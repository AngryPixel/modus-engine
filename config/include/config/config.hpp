/*
 * Copyright 2019-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/string_slice.hpp>
#include <core/io/byte_stream.hpp>
#include <core/pointers.hpp>

#include <config/module_config.hpp>

namespace modus::config {

    class ConfigPrivate;

    enum class ConfigError {
        InvalidPath,
        PathNotFound,
        InvalidType
    };

    template<typename T>
    using ConfigResult = Result<T, ConfigError>;

    /// Configuration file access and serialization.
    ///
    /// Configuration data can be accessed via path queries such as
    /// "/foo/bar/value" via any of the query/story methods.
    ///
    /// Use paths such as "/foo/N" where N is an integer to access/store values
    /// in array "Foo".
    ///
    class MODUS_CONFIG_EXPORT Config {
        private:
           PimplPtr<ConfigPrivate> m_impl;

        public:
           Config();

           ~Config() = default;

           MODUS_CLASS_DISABLE_COPY_MOVE(Config);

           Result<void, IOError> load_from(ISeekableReader& reader);

           Result<void, IOError> save_to(IByteWriter& writer);

           ConfigResult<StringSlice> query_string(const StringSlice& path) const;

           ConfigResult<bool> query_bool(const StringSlice& path) const;

           ConfigResult<i32> query_i32(const StringSlice& path) const;

           ConfigResult<f64> query_f64(const StringSlice& path) const;

           ConfigResult<void> store_string(const StringSlice& path, const StringSlice& str, const bool create = false);

           ConfigResult<void> store_bool(const StringSlice& path, const bool value, const bool create = false);

           ConfigResult<void> store_i32(const StringSlice& path, const i32 value, const bool create = false);

           ConfigResult<void> store_f64(const StringSlice& path, const f64 value, const bool create = false);

           ConfigResult<void> erase(const StringSlice& path);

           bool has_changed() const;
    };

}
