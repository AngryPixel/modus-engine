/*
 * Copyright 2019-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>
#include <config_private.hpp>

#include <core/io/memory_stream.hpp>

namespace modus::config {

struct StringSliceReader {
    using Ch = char;

    const StringSlice& m_stream;
    usize offset = 0;

    StringSliceReader(const StringSlice& slice) : m_stream(slice) {}

    [[nodiscard]] Ch Peek() const {
        if (offset < m_stream.size()) {
            return m_stream[offset];
        }
        return '\0';
    }
    [[nodiscard]] Ch Take() {
        if (offset < m_stream.size()) {
            return m_stream[offset++];
        }
        return '\0';
    }

    [[nodiscard]] size_t Tell() const { return offset;}

    [[nodiscard]] Ch* PutBegin() { modus_assert(false); return nullptr; }
    void Put(Ch) { modus_assert(false); }
    void Flush() { modus_assert(false); }
    size_t PutEnd(Ch*) { modus_assert(false); return 0; }
};


static inline rapidjson::Pointer string_slice_to_pointer(const StringSlice& slice) {
    return rapidjson::Pointer(slice.data(), slice.size());
}

ConfigPrivate::ConfigPrivate()
    : m_document(),
      m_has_changed(false) {

}

Result<void, IOError> ConfigPrivate::load_from(ISeekableReader& reader) {
    RamStream stream;
    if (!stream.reserve(512)) {
        return Error(IOError::Unknown);
    }
    auto read_result = stream.populate_from_stream(reader);
    if (!read_result) {
        return Error(IOError::Unknown);
    }

    const StringSlice doc_slice = stream.as_string_slice().sub_slice(0, read_result.value());
    StringSliceReader rjstream(doc_slice);
    const rapidjson::ParseResult parse_result = m_document.ParseStream(rjstream);
    if (!parse_result) {
        return Error(IOError::Unknown);
    }

    return Ok<>();
}

Result<void, IOError> ConfigPrivate::save_to(IByteWriter& writer) {
    MODUS_UNUSED(writer);

    // Generate a in memory version first since rapidjson doesn't validate
    // io memory failures

    rapidjson::StringBuffer buffer(nullptr, 1024);
    rapidjson::PrettyWriter<rapidjson::StringBuffer> rjwriter(buffer);

    if (!m_document.Accept(rjwriter)) {
        return Error(IOError::Unknown);
    }

    const StringSlice generated = StringSlice(buffer.GetString(), buffer.GetSize());
    auto write_result = writer.write(generated.as_bytes());
    if (!write_result) {
        return Error(write_result.error());
    }

    if (write_result.value() != generated.size()) {
        return Error(IOError::Unknown);
    }

    m_has_changed = false;

    return Ok<>();
}

ConfigResult<StringSlice> ConfigPrivate::query_string(const StringSlice& path) const {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    const rapidjson::Value* rjvalue = pointer.Get(m_document);
    if (rjvalue == nullptr) {
        return Error(ConfigError::PathNotFound);
    }

    if (!rjvalue->IsString()) {
        return Error(ConfigError::InvalidType);
    }

    return Ok(StringSlice(rjvalue->GetString()));
}

ConfigResult<bool> ConfigPrivate::query_bool(const StringSlice& path) const {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    const rapidjson::Value* rjvalue = pointer.Get(m_document);
    if (rjvalue == nullptr) {
        return Error(ConfigError::PathNotFound);
    }

    if (!rjvalue->IsBool()) {
        return Error(ConfigError::InvalidType);
    }

    return Ok(rjvalue->GetBool());
}

ConfigResult<i32> ConfigPrivate::query_i32(const StringSlice& path) const {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    const rapidjson::Value* rjvalue = pointer.Get(m_document);
    if (rjvalue == nullptr) {
        return Error(ConfigError::PathNotFound);
    }

    if (!rjvalue->IsInt()) {
        return Error(ConfigError::InvalidType);
    }

    return Ok(rjvalue->GetInt());
}

ConfigResult<f64> ConfigPrivate::query_f64(const StringSlice& path) const {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    const rapidjson::Value* rjvalue = pointer.Get(m_document);
    if (rjvalue == nullptr) {
        return Error(ConfigError::PathNotFound);
    }

    if (!rjvalue->IsDouble()) {
        return Error(ConfigError::InvalidType);
    }

    return Ok(rjvalue->GetDouble());
}

ConfigResult<void> ConfigPrivate::store_string(const StringSlice& path, const StringSlice& str, const bool create) {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    if (create) {
        pointer.Set(m_document,rapidjson::Value(str.data(), str.size()));
    } else {
        rapidjson::Value* rjvalue = pointer.Get(m_document);
        if (rjvalue == nullptr) {
            return Error(ConfigError::PathNotFound);
        }
        rjvalue->SetString(str.data(), str.size());
    }
    m_has_changed = true;
    return Ok<>();
}

ConfigResult<void> ConfigPrivate::store_bool(const StringSlice& path, const bool value, const bool create) {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    if (create) {
        pointer.Set(m_document,rapidjson::Value(value));
    } else {
        rapidjson::Value* rjvalue = pointer.Get(m_document);
        if (rjvalue == nullptr) {
            return Error(ConfigError::PathNotFound);
        }
        rjvalue->SetBool(value);
    }
    m_has_changed = true;
    return Ok<>();
}

ConfigResult<void> ConfigPrivate::store_i32(const StringSlice& path, const i32 value, const bool create) {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    if (create) {
        pointer.Set(m_document,rapidjson::Value(value));
    } else {
        rapidjson::Value* rjvalue = pointer.Get(m_document);
        if (rjvalue == nullptr) {
            return Error(ConfigError::PathNotFound);
        }
        rjvalue->SetBool(value);
    }
    m_has_changed = true;
    return Ok<>();
}

ConfigResult<void> ConfigPrivate::store_f64(const StringSlice& path, const f64 value, const bool create) {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }
    if (create) {
        pointer.Set(m_document,rapidjson::Value(value));
    } else {
        rapidjson::Value* rjvalue = pointer.Get(m_document);
        if (rjvalue == nullptr) {
            return Error(ConfigError::PathNotFound);
        }
        rjvalue->SetDouble(value);
    }
    m_has_changed = true;
    return Ok<>();
}

ConfigResult<void> ConfigPrivate::erase(const StringSlice& path) {
    rapidjson::Pointer pointer = string_slice_to_pointer(path);
    if (!pointer.IsValid()){
        return Error(ConfigError::InvalidPath);
    }

    if (!pointer.Erase(m_document)) {
        return Error(ConfigError::PathNotFound);
    }
    m_has_changed = true;
    return Ok<>();
}

}
