/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>
#include <config/config.hpp>

#include <config_private.hpp>

namespace modus::config {

Config::Config()
    : m_impl(make_pimpl_ptr<ConfigPrivate>()) {

}

Result<void, IOError> Config::load_from(ISeekableReader& reader) {
    return m_impl->load_from(reader);
}

Result<void, IOError> Config::save_to(IByteWriter& writer) {
    return m_impl->save_to(writer);
}

ConfigResult<StringSlice> Config::query_string(const StringSlice& path) const {
    return m_impl->query_string(path);
}

ConfigResult<bool> Config::query_bool(const StringSlice& path) const {
    return m_impl->query_bool(path);
}

ConfigResult<i32> Config::query_i32(const StringSlice& path) const {
    return m_impl->query_i32(path);
}

ConfigResult<f64> Config::query_f64(const StringSlice& path) const {
    return m_impl->query_f64(path);
}

ConfigResult<void> Config::store_string(const StringSlice& path, const StringSlice& str, const bool create) {
    return m_impl->store_string(path, str, create);
}

ConfigResult<void> Config::store_bool(const StringSlice& path, const bool value, const bool create) {
    return m_impl->store_bool(path, value, create);
}

ConfigResult<void> Config::store_i32(const StringSlice& path, const i32 value, const bool create) {
    return m_impl->store_i32(path, value, create);
}

ConfigResult<void> Config::store_f64(const StringSlice& path, const f64 value, const bool create){
    return m_impl->store_f64(path, value, create);
}

ConfigResult<void> Config::erase(const StringSlice& path) {
    return m_impl->erase(path);
}

bool Config::has_changed() const {
    return m_impl->has_changed();
}

}
