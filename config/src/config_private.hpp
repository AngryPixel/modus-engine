/*
 * Copyright 2019-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <config/config.hpp>

namespace modus::config {
// TODO: Replace rapid json with something that supports none global allocators
class ConfigPrivate {
    private:
        rapidjson::Document m_document;
        bool m_has_changed;
    public:
        ConfigPrivate();

        ~ConfigPrivate() = default;

       MODUS_CLASS_DISABLE_COPY_MOVE(ConfigPrivate);

       Result<void, IOError> load_from(ISeekableReader& reader);

       Result<void, IOError> save_to(IByteWriter& writer);

       ConfigResult<StringSlice> query_string(const StringSlice& path) const;

       ConfigResult<bool> query_bool(const StringSlice& path) const;

       ConfigResult<i32> query_i32(const StringSlice& path) const;

       ConfigResult<f64> query_f64(const StringSlice& path) const;

       ConfigResult<void> store_string(const StringSlice& path, const StringSlice& str, const bool create);

       ConfigResult<void> store_bool(const StringSlice& path, const bool value, const bool create);

       ConfigResult<void> store_i32(const StringSlice& path, const i32 value, const bool create);

       ConfigResult<void> store_f64(const StringSlice& path, const f64 value, const bool create);

       ConfigResult<void> erase(const StringSlice& path);

       inline bool has_changed() const {
           return m_has_changed;
       }

    };
}
