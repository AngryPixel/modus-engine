/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <config/config_pch.h>
#include <config/config.hpp>
#include <core/io/memory_stream.hpp>
#include <catch2/catch.hpp>

using namespace modus;
using namespace modus::config;

TEST_CASE("Create and Read Values", "[Config]") {

    Config cfg;
    String value_str = "Hello World!";
    bool value_bool = true;
    i32 value_i32 = -65791;
    f64 value_f64 = f64(value_i32) * 2.5;

    const char* path_str = "/str";
    const char* path_bool = "/bool";
    const char* path_i32 = "/myfancypath";
    const char* path_f64 = "/this";

    // Create Str
    {
        auto store_result = cfg.store_string(path_str, value_str, true);
        REQUIRE(store_result);
        auto read_result = cfg.query_string(path_str);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == StringSlice(value_str));
    }
    // Create bool
    {
        // try to store value which doesn't exist
        auto store_result = cfg.store_bool(path_bool, value_bool);
        REQUIRE_FALSE(store_result);
        REQUIRE(store_result.error() == ConfigError::PathNotFound);
        store_result = cfg.store_bool(path_bool, value_bool, true);
        REQUIRE(store_result);
        auto read_result = cfg.query_bool(path_bool);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == value_bool);
    }

    // Create i32
    {
        auto store_result = cfg.store_i32(path_i32, value_i32, true);
        REQUIRE(store_result);
        auto read_result = cfg.query_i32(path_i32);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == value_i32);
    }

    // Create f64
    {
        auto store_result = cfg.store_f64(path_f64, value_f64, true);
        REQUIRE(store_result);
        auto read_result = cfg.query_f64(path_f64);
        REQUIRE(read_result);
        REQUIRE(read_result.value() <= value_f64);
    }
}

TEST_CASE("Create and Read from nested section and erase", "[Config]") {

    Config cfg;
    i32 value_i32 = 54623;

    {
        const char* path_i32 = "/nested/path/to/bar";
        const char* path_i32_invalid = "/nested/path/to";
        auto store_result = cfg.store_i32(path_i32, value_i32, true);
        REQUIRE(store_result);
        auto read_result = cfg.query_i32(path_i32);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == value_i32);

        // Read invalid type
        read_result = cfg.query_i32(path_i32_invalid);
        REQUIRE_FALSE(read_result);
        REQUIRE(read_result.error() == ConfigError::InvalidType);

        // Erase whole section
        auto erase_result = cfg.erase("/nested");
        REQUIRE(erase_result);
    }

}

TEST_CASE("Test serialize", "[Config]") {

    String value_str = "Hello World!";
    bool value_bool = true;
    i32 value_i32 = -65791;
    f64 value_f64 = f64(value_i32) * 2.5;

    const char* path_str = "/str";
    const char* path_bool = "/bool";
    const char* path_i32 = "/myfancypath";
    const char* path_f64 = "/this";

    RamStream stream;
    {
        Config cfg;
        // Create Str
        {
            auto store_result = cfg.store_string(path_str, value_str, true);
            REQUIRE(store_result);
        }
        // Create bool
        {
            // try to store value which doesn't exist
            auto store_result = cfg.store_bool(path_bool, value_bool, true);
            REQUIRE(store_result);
        }

        // Create i32
        {
            auto store_result = cfg.store_i32(path_i32, value_i32, true);
            REQUIRE(store_result);
        }

        // Create f64
        {
            auto store_result = cfg.store_f64(path_f64, value_f64, true);
            REQUIRE(store_result);
        }

        auto save_result = cfg.save_to(stream);
        REQUIRE(save_result);
    }
    REQUIRE(stream.seek(0));
    {
        Config cfg;
        auto load_result = cfg.load_from(stream);
        REQUIRE(load_result);

    // Create Str
    {
        auto read_result = cfg.query_string(path_str);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == StringSlice(value_str));
    }
    // Create bool
    {
        // try to store value which doesn't exist
        auto read_result = cfg.query_bool(path_bool);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == value_bool);
    }

    // Create i32
    {
        auto read_result = cfg.query_i32(path_i32);
        REQUIRE(read_result);
        REQUIRE(read_result.value() == value_i32);
    }

    // Create f64
    {
        auto read_result = cfg.query_f64(path_f64);
        REQUIRE(read_result);
        REQUIRE(read_result.value() <= value_f64);
    }
    }
}
