/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <vfs/vfs_pch.h>

#include <os/path.hpp>
#include <vfs/vfs.hpp>

#include <catch2/catch.hpp>

using namespace modus;
using namespace modus::vfs;

TEST_CASE("VirtualPath from tag and path", "[VFSVirtualPath]") {
    {
        auto result = VirtualPath::from("foo", "bar");
        REQUIRE(result);
    }

    {
        // Tag too big
        auto result = VirtualPath::from("ThisTagIsWayTooBigYaKnow", "bar");
        REQUIRE_FALSE(result);
    }
    {
        // tag not alpha
        auto result = VirtualPath::from("tag0", "bar");
        REQUIRE_FALSE(result);
        result = VirtualPath::from("t:g", "bar");
        REQUIRE_FALSE(result);
    }

    {
        // Invalid path
        auto result = VirtualPath::from("foo", "bar:d");
        REQUIRE_FALSE(result);
    }

    {
        // relative path
        auto result = VirtualPath::from("foo", "bar/../d");
        REQUIRE_FALSE(result);
    }

    {
        // empty path
        auto result = VirtualPath::from("foo", "");
        REQUIRE_FALSE(result);
    }
}

TEST_CASE("VirtualPath from string", "[VFSVirtualPath]") {
    {
        auto result = VirtualPath::from("foo:bar");
        REQUIRE(result);
    }

    {
        // Tag too big
        auto result = VirtualPath::from("ThisTagIsWayTooBigYaKnow:bar");
        REQUIRE_FALSE(result);
    }
    {
        // tag not alpha
        auto result = VirtualPath::from("tag0:bar");
        REQUIRE_FALSE(result);
    }

    {
        // Invalid path
        auto result = VirtualPath::from("foo", "bar:d");
        REQUIRE_FALSE(result);
    }

    {
        // relative path
        auto result = VirtualPath::from("foo:bar/../d");
        REQUIRE_FALSE(result);
    }

    {
        // empty path
        auto result = VirtualPath::from("foo:");
        REQUIRE_FALSE(result);
    }
    {
        // no tag separator
        auto result = VirtualPath::from("foobar");
        REQUIRE_FALSE(result);
    }
}
