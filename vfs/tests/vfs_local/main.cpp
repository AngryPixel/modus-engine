/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <vfs/vfs_pch.h>
#include <core/io/byte_stream.hpp>
#include <os/path.hpp>
#include <vfs/vfs.hpp>

#include <catch2/catch.hpp>

using namespace modus;
using namespace modus::vfs;
using namespace modus::os;

TEST_CASE("Tag test", "[VFSLocal]") {
    VirtualFileSystem vfs;

    {
        VFSMountParams params;
        params.tag = "test";
        params.path = MODUS_TEST_SOURCE_DIR;
        params.read_only = true;
        auto mount_result = vfs.mount_path(params);
        REQUIRE(mount_result);

        auto vpath_result = VirtualPath::from("test:test_data/dir/file2.txt");
        REQUIRE(vpath_result);
        auto exists_result = vfs.exists(vpath_result.value());
        REQUIRE(exists_result);

        // Double mount
        auto mount_result2 = vfs.mount_path(params);
        REQUIRE_FALSE(mount_result2);

        auto unmount_result = vfs.unmount_path("test", MODUS_TEST_SOURCE_DIR);
        REQUIRE(unmount_result);
    }

    // Invalid path
    {
        VFSMountParams invalid;
        invalid.tag = "test";
        invalid.path = "/I/Do/No/Exist!";
        auto mount_result = vfs.mount_path(invalid);
        REQUIRE_FALSE(mount_result);
    }

    // Invalid tag
    {
        VFSMountParams invalid;
        invalid.tag = "test:0";
        invalid.path = "/I/Do/No/Exist!";
        auto mount_result = vfs.mount_path(invalid);
        REQUIRE_FALSE(mount_result);
    }

    // Shutdown
    auto unmount_result = vfs.unmount_all();
    REQUIRE(unmount_result);
}

static const StringSlice k_tmp_dir_name = "modus_vfs_instance_local_test";

struct VFSTestScope {
    VirtualFileSystem& vfs;
    VFSTestScope(VirtualFileSystem& v) : vfs(v) {}

    ~VFSTestScope() {
        auto r = vfs.unmount_all();
        REQUIRE(r);
    }
};

TEST_CASE("Make directory + FileIO", "[VFSInstanceLocal]") {
    auto tmp_dir_result = Path::os_temp_path();
    REQUIRE(tmp_dir_result);
    const String tmp_dir = tmp_dir_result.release_value();
    String test_dir;
    Path::join_inplace(test_dir, tmp_dir, k_tmp_dir_name);
    REQUIRE(Path::make_directory(test_dir));

    VirtualFileSystem vfs;
    VFSTestScope scope_gaurd(vfs);
    VFSMountParams params;
    params.tag = "tmp";
    params.path = test_dir;
    params.read_only = false;
    auto mount_result = vfs.mount_path(params);
    REQUIRE(mount_result);

    auto vpath_foo_result = VirtualPath::from("tmp:foo");
    REQUIRE(vpath_foo_result);

    SECTION("Make simple directory") {
        String dir_path;
        Path::join_inplace(dir_path, test_dir, "foo");
        auto result = vfs.make_directory(vpath_foo_result.value());
        REQUIRE(result);
        REQUIRE(Path::is_directory(dir_path));
    }

    SECTION("Move directory") {
        String dir_path;
        Path::join_inplace(dir_path, test_dir, "foo");
        String new_dir_path;
        auto result = vfs.make_directory(vpath_foo_result.value());
        REQUIRE(result);
        Path::join_inplace(new_dir_path, test_dir, "bar");
        result = vfs.rename(vpath_foo_result.value(), "bar");
        REQUIRE(result);
        REQUIRE(Path::is_directory(new_dir_path));
        REQUIRE_FALSE(Path::exists(dir_path));
    }

    SECTION("Write & Read from file") {
        StringSlice string_in = "Hello World!!";

        auto vpath_result = VirtualPath::from("tmp:write_test.txt");
        REQUIRE(vpath_result);
        const VirtualPath file_path = vpath_result.value();

        // Create file
        {
            auto open_result = vfs.open_writable(file_path, false, true);
            REQUIRE(open_result);

            auto f = open_result.release_value();

            auto write_result = f->write(string_in.as_bytes());
            REQUIRE(write_result);
            REQUIRE(write_result.value() == string_in.size());
            {
                auto pos_result = f->position();
                REQUIRE(pos_result);
                REQUIRE(pos_result.value() == string_in.size());
            }
            REQUIRE(f->size() == string_in.size());

            {
                auto seek_result = f->seek(0);
                REQUIRE(seek_result);
                REQUIRE(seek_result.value() == 0);
                auto pos_result = f->position();
                REQUIRE(pos_result);
                REQUIRE(pos_result.value() == 0);
            }

            char buffer[32];
            SliceMut<char> buffer_slice(buffer);
            auto read_result = f->read(buffer_slice.as_bytes());
            REQUIRE(read_result);
            REQUIRE(read_result.value() == string_in.size());
            auto sub_slice = buffer_slice.sub_slice(string_in.size());
            REQUIRE(sub_slice.as_bytes() == string_in.as_bytes());

            read_result = f->read(buffer_slice.as_bytes());
            REQUIRE(read_result);
            REQUIRE(read_result.value() == 0);
            f->flush();
        }
    }

    auto remove_result = Path::remove(test_dir);
    REQUIRE(remove_result);
    REQUIRE_FALSE(Path::exists(test_dir));
}

TEST_CASE("Priority", "[VFSInstanceLocal]") {
    String test_dir1, test_dir2;
    Path::join_inplace(test_dir1, MODUS_TEST_SOURCE_DIR, "test_data/dir0");
    Path::join_inplace(test_dir2, MODUS_TEST_SOURCE_DIR, "test_data/dir1");

    char buffer[3];

    VirtualFileSystem vfs;
    VFSTestScope scope_gaurd(vfs);
    VFSMountParams params;
    params.tag = "test";
    params.path = test_dir1;
    params.read_only = false;
    auto mount_result = vfs.mount_path(params);
    REQUIRE(mount_result);

    auto vpath_foo_result = VirtualPath::from("test:foo.txt");
    REQUIRE(vpath_foo_result);

    {
        auto reader_result = vfs.open_readable(vpath_foo_result.value());
        REQUIRE(reader_result);
        auto reader = reader_result.release_value();
        auto read_result = reader->read(SliceMut(buffer).as_bytes());
        REQUIRE(read_result);
        REQUIRE(read_result.value() == 3);
        const char* expected = "bar";
        REQUIRE(Slice(buffer) == StringSlice(expected));
    }

    /// mount higher priority file
    params.priority = 0;
    params.path = test_dir2;
    REQUIRE(vpath_foo_result);

    auto mount_result2 = vfs.mount_path(params);
    REQUIRE(mount_result2);

    {
        auto reader_result = vfs.open_readable(vpath_foo_result.value());
        REQUIRE(reader_result);
        auto reader = reader_result.release_value();
        auto read_result = reader->read(SliceMut(buffer).as_bytes());
        REQUIRE(read_result);
        REQUIRE(read_result.value() == 3);
        const char* expected = "baz";
        REQUIRE(Slice(buffer) == StringSlice(expected));
    }

    auto unmount_result = vfs.unmount_all();
    REQUIRE(unmount_result);
}

TEST_CASE("Empty Unmount", "[VFSLocal]") {
    VirtualFileSystem vfs;
    auto result = vfs.unmount_all();
    REQUIRE(result);
    REQUIRE_FALSE(!result);
}
