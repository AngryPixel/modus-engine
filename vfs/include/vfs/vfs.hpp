/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/pointers.hpp>
#include <core/string_slice.hpp>

#include <vfs/module_config.hpp>

namespace modus {
class ISeekableReader;
class ISeekableWriter;
}    // namespace modus

namespace modus::vfs {

enum class VFSError {
    ReadOnlyFileSystem,
    InvalidUrl,
    UrlDoesNotExist,
    Permissions,
    FileSystem,
    NotAFile,
    Exists,
    Unknown
};

enum class VFSMountError {
    AlreadyMounted,
    PathIsNotADirectory,
    ArchiveNotFound,
    ArchiveCorrupt,
    TagSizeExceeded,
    TagInvalid,
    InUse,
    PathNotMounted
};

MODUS_VFS_EXPORT StringSlice vfs_mount_error_to_string(const VFSMountError error);

MODUS_VFS_EXPORT StringSlice vfs_error_to_string(const VFSError error);

using VFSMountResult = Result<void, VFSMountError>;

template <typename T>
using VFSResult = Result<T, VFSError>;

using VFSRWPtr = std::unique_ptr<ISeekableWriter>;
using VFSROPtr = std::unique_ptr<ISeekableReader>;

struct VFSMountParams {
    StringSlice tag;
    StringSlice path;
    bool read_only = true;
    u64 priority = std::numeric_limits<u64>::max();
};

class MODUS_VFS_EXPORT VirtualPath {
   private:
    StringSlice m_tag;
    StringSlice m_path;

   private:
    VirtualPath(const StringSlice& tag, const StringSlice& path);

   public:
    static Result<VirtualPath, void> from(const StringSlice& virtual_path);
    static Result<VirtualPath, void> from(const StringSlice& tag, const StringSlice& path);

    VirtualPath() = default;
    VirtualPath(const VirtualPath&) = default;
    VirtualPath(VirtualPath&&) = default;
    VirtualPath& operator=(const VirtualPath&) = default;
    VirtualPath& operator=(VirtualPath&&) = default;
    ~VirtualPath() = default;

    inline StringSlice tag() const { return m_tag; }

    inline StringSlice path() const { return m_path; }
};

class VirtualFileSystemPrivate;
class MODUS_VFS_EXPORT VirtualFileSystem {
   private:
    PimplPtr<VirtualFileSystemPrivate> m_impl;

   public:
    VirtualFileSystem();

    ~VirtualFileSystem();

    VFSMountResult mount_path(const VFSMountParams& params);

    VFSMountResult unmount_path(StringSlice tag, StringSlice path);

    VFSMountResult mount_archive(const VFSMountParams& ms);

    VFSMountResult unmount_archive(StringSlice tag, StringSlice path);

    VFSMountResult unmount_all();

    VFSResult<VFSROPtr> open_readable(const VirtualPath& vpath);

    VFSResult<VFSRWPtr> open_writable(const VirtualPath& vpath,
                                      const bool append,
                                      const bool create);

    VFSResult<void> exists(const VirtualPath& vpath) const;

    VFSResult<void> make_directory(const VirtualPath& vpath);

    VFSResult<void> remove(const VirtualPath& vpath);

    VFSResult<void> rename(const VirtualPath& vpath, const StringSlice& new_path);
};

}    // namespace modus::vfs
