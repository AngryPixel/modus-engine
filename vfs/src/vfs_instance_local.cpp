/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/file.hpp>
#include <os/path.hpp>
#include <vfs_instance_local.hpp>

namespace modus::vfs {

static inline VFSError path_error_to_vfs_error(const os::PathError error) {
    switch (error) {
        case os::PathError::Permissions:
            return VFSError::Permissions;
        case os::PathError::NotFound:
            return VFSError::UrlDoesNotExist;
        case os::PathError::Exists:
            return VFSError::Exists;
        case os::PathError::InvalidPath:
        case os::PathError::PathConversion:
        case os::PathError::PathTooLong:
            return VFSError::FileSystem;
        case os::PathError::ReadOnlyPath:
            return VFSError::ReadOnlyFileSystem;
        case os::PathError::NotAFile:
            return VFSError::NotAFile;
        default:
            return VFSError::Unknown;
    }
}

Result<VFSInstance*, VFSMountError> VFSInstanceLocal::create(StringSlice path,
                                                             const u32 priority,
                                                             const bool read_only) {
    if (!os::Path::is_directory(path)) {
        return Error(VFSMountError::PathIsNotADirectory);
    }

    VFSInstance* ptr = AllocatorTraits<VFSAllocator>::allocate_and_construct<VFSInstanceLocal>(
        path, priority, read_only);
    return Ok(ptr);
}

VFSInstanceLocal::VFSInstanceLocal(StringSlice path, const u32 priority, const bool read_only)
    : VFSInstance(path, priority, read_only), m_path(path.data(), path.size()) {}

VFSResult<VFSROPtr> VFSInstanceLocal::open_readable(StringSlice path) {
    auto result = to_real_path(path);
    if (!result) {
        return Error(result.error());
    }

    os::FileBuilder builder;
    os::PathResult<os::File> file_result = builder.read().binary().open(result.value());

    if (!file_result) {
        return Error(path_error_to_vfs_error(file_result.error()));
    }

    return Ok<VFSROPtr>(modus::make_unique<os::File>(file_result.release_value()));
}

VFSResult<VFSRWPtr> VFSInstanceLocal::open_writable(StringSlice path,
                                                    const bool append,
                                                    const bool create) {
    if (is_read_only()) {
        return Error(VFSError::ReadOnlyFileSystem);
    }

    auto result = to_real_path(path);
    if (!result) {
        return Error(result.error());
    }

    os::FileBuilder builder;
    if (append) {
        builder.append();
    }
    if (create) {
        builder.create();
    }
    os::PathResult<os::File> file_result = builder.read().write().binary().open(result.value());

    if (!file_result) {
        return Error(path_error_to_vfs_error(file_result.error()));
    }

    return Ok<VFSRWPtr>(modus::make_unique<os::File>(file_result.release_value()));
}

VFSResult<void> VFSInstanceLocal::exists(StringSlice path) const {
    auto result = to_real_path(path);
    if (!result) {
        return Error(result.error());
    }
    return Ok<>();
}

VFSResult<void> VFSInstanceLocal::make_directory(StringSlice path) {
    if (is_read_only()) {
        return Error(VFSError::ReadOnlyFileSystem);
    }

    auto result = to_real_path(path);
    if (!result) {
        return Error(result.error());
    }

    auto mkdir_result = os::Path::make_directory(result.value());
    if (!mkdir_result) {
        return Error(path_error_to_vfs_error(mkdir_result.error()));
    }
    return Ok<>();
}

VFSResult<void> VFSInstanceLocal::remove(StringSlice path) {
    if (is_read_only()) {
        return Error(VFSError::ReadOnlyFileSystem);
    }

    auto result = to_real_path(path);
    if (!result) {
        return Error(result.error());
    }

    auto remove_result = os::Path::remove(result.value());
    if (!remove_result) {
        return Error(path_error_to_vfs_error(remove_result.error()));
    }
    return Ok<>();
}

VFSResult<void> VFSInstanceLocal::rename(StringSlice path_from, StringSlice path_to) {
    if (is_read_only()) {
        return Error(VFSError::ReadOnlyFileSystem);
    }

    auto result = to_real_path(path_from);
    if (!result) {
        return Error(result.error());
    }

    auto result_to = to_real_path(path_to);
    if (!result_to) {
        return Error(result.error());
    }

    auto rename_result = os::Path::rename(result.value(), result_to.value());
    if (!rename_result) {
        return Error(path_error_to_vfs_error(rename_result.error()));
    }

    return Ok<>();
}

Result<String, VFSError> VFSInstanceLocal::to_real_path(StringSlice path) const {
    // Should have been detected by caller
    if (path.find("..").is_value()) {
        return Error(VFSError::InvalidUrl);
    }

    String joined_path;
    os::Path::join_inplace(joined_path, m_path, path);
    return Ok(joined_path);
}

}    // namespace modus::vfs
