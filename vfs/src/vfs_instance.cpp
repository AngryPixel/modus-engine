/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <vfs_instance.hpp>

namespace modus::vfs {

VFSInstance::VFSInstance(StringSlice path, const u32 priority, const bool read_only)
    : m_path(path.data(), path.size()), m_priority(priority), m_read_only(read_only) {}

VFSResult<VFSRWPtr> VFSInstance::open_writable(StringSlice, const bool, const bool) {
    return Error(VFSError::ReadOnlyFileSystem);
}

VFSResult<void> VFSInstance::make_directory(StringSlice) {
    return Error(VFSError::ReadOnlyFileSystem);
}

VFSResult<void> VFSInstance::remove(StringSlice) {
    return Error(VFSError::ReadOnlyFileSystem);
}

VFSResult<void> VFSInstance::rename(StringSlice, StringSlice) {
    return Error(VFSError::ReadOnlyFileSystem);
}

VFSMountResult VFSInstance::unmount() {
    return Ok<>();
}

}    // namespace modus::vfs
