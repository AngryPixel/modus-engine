/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vfs_private.hpp>

namespace modus::vfs {

class VFSInstance {
   protected:
    VFSString m_path;
    const u32 m_priority;
    const bool m_read_only;

   public:
    VFSInstance(StringSlice path, const u32 priority, const bool read_only);

    virtual ~VFSInstance() {}

    inline bool is_read_only() const { return m_read_only; }

    u32 priority() const { return m_priority; }

    inline StringSlice path() const { return m_path; }

    virtual VFSResult<VFSROPtr> open_readable(StringSlice path) = 0;

    virtual VFSResult<VFSRWPtr> open_writable(StringSlice path,
                                              const bool append,
                                              const bool create);

    virtual VFSResult<void> exists(StringSlice path) const = 0;

    virtual VFSResult<void> make_directory(StringSlice path);

    virtual VFSResult<void> remove(StringSlice path);

    virtual VFSResult<void> rename(StringSlice path_from, StringSlice path_to);

    virtual VFSMountResult unmount();
};

}    // namespace modus::vfs
