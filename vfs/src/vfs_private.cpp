/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <vfs_instance_local.hpp>
#include <vfs_private.hpp>

namespace modus::vfs {

static constexpr usize k_max_tag_size = 16;
static constexpr char k_tag_separator = ':';

static inline bool is_valid_tag(const StringSlice& tag) {
    if (tag.size() > k_max_tag_size) {
        return false;
    }

    for (auto& c : tag) {
        if (!std::isalpha(c)) {
            return false;
        }
    }

    return true;
}

static inline bool is_valid_path(const StringSlice& path) {
    return path.size() > 0 && path.find("..").is_error() && path.find(k_tag_separator).is_error();
}

VirtualPath::VirtualPath(const StringSlice& tag, const StringSlice& path)
    : m_tag(tag), m_path(path) {
    modus_assert(tag.size() != 0 && tag.size() < k_max_tag_size);
    modus_assert(path.size() != 0);
}

Result<VirtualPath, void> VirtualPath::from(const StringSlice& virtual_path) {
    auto find_result = virtual_path.find(k_tag_separator);
    if (!find_result || find_result.value() > k_max_tag_size) {
        return Error<>();
    }
    const StringSlice tag = virtual_path.sub_slice(0, find_result.value());
    const StringSlice path = virtual_path.sub_slice(find_result.value() + 1, virtual_path.size());
    return VirtualPath::from(tag, path);
}

Result<VirtualPath, void> VirtualPath::from(const StringSlice& tag, const StringSlice& path) {
    if (!is_valid_tag(tag)) {
        return Error<>();
    }
    if (!is_valid_path(path)) {
        return Error<>();
    }
    return Ok(VirtualPath(tag, path));
}

VirtualFileSystemPrivate::VirtualFileSystemPrivate()
    : m_fileSystems(HashMapType()), m_shutdown(false) {
    // random magic number yay!
    auto accessor = m_fileSystems.lock_write();
    accessor->reserve(16);
}

VirtualFileSystemPrivate::~VirtualFileSystemPrivate() {
#if defined(MODUS_ENABLE_ASSERTS)
    auto accessor = m_fileSystems.lock_read();
    modus_assert(accessor->empty());
#endif
}

VFSMountResult VirtualFileSystemPrivate::mount_path(const VFSMountParams& params) {
    if (params.tag.size() > k_max_tag_size) {
        return Error(VFSMountError::TagSizeExceeded);
    }

    if (!is_valid_tag(params.tag)) {
        return Error(VFSMountError::TagInvalid);
    }

    VFSString key(params.tag.data(), params.tag.size());
    {
        auto accessor = m_fileSystems.lock_write();
        HashMapType& file_systems = *accessor;
        auto it = file_systems.find(key);
        if (it == file_systems.end()) {
            HashMapValue value;
            value.reserve(8);
            it = file_systems.insert({std::move(key), std::move(value)}).first;
        } else {
            if (is_vfs_mounted(it->second, params.path)) {
                return Error(VFSMountError::AlreadyMounted);
            }
        }
        Result<VFSInstance*, VFSMountError> vfs_result =
            VFSInstanceLocal::create(params.path, params.priority, params.read_only);

        if (!vfs_result) {
            return Error(vfs_result.error());
        }
        priority_insert(it->second, vfs_result.value());
    }
    return Ok<>();
}

VFSMountResult VirtualFileSystemPrivate::unmount_path(StringSlice tag, StringSlice path) {
    if (tag.size() > k_max_tag_size) {
        return Error(VFSMountError::TagSizeExceeded);
    }

    if (!is_valid_tag(tag)) {
        return Error(VFSMountError::TagInvalid);
    }

    const VFSString key(tag.data(), tag.size());
    {
        auto accessor = m_fileSystems.lock_write();
        auto it = accessor->find(key);
        if (it == accessor->end()) {
            return Error(VFSMountError::PathNotMounted);
        }

        auto scan_it = std::find_if(it->second.begin(), it->second.end(),
                                    [&path](const VFSInstance* i) { return i->path() == path; });
        if (scan_it == it->second.end()) {
            return Error(VFSMountError::PathNotMounted);
        }
        AllocatorTraits<VFSAllocator>::destroy_and_free(*scan_it);
        it->second.erase(scan_it);
    }
    return Ok<>();
}

VFSMountResult VirtualFileSystemPrivate::mount_archive(const VFSMountParams& params) {
    if (params.tag.size() > k_max_tag_size) {
        return Error(VFSMountError::TagSizeExceeded);
    }
    return Error(VFSMountError::ArchiveNotFound);
}

VFSMountResult VirtualFileSystemPrivate::unmount_archive(StringSlice, StringSlice) {
    return Error(VFSMountError::ArchiveNotFound);
}

VFSMountResult VirtualFileSystemPrivate::unmount_all() {
    m_shutdown = true;
    {
        auto accessor = m_fileSystems.lock_write();
        for (auto& kv : *accessor) {
            for (auto& instance : kv.second) {
                if (instance != nullptr) {
                    auto unmount_result = instance->unmount();
                    if (!unmount_result) {
                        return unmount_result;
                    }
                    AllocatorTraits<VFSAllocator>::destroy_and_free(instance);
                    instance = nullptr;
                }
            }
        }
        accessor->clear();
    }
    return Ok<>();
}

VFSResult<VFSROPtr> VirtualFileSystemPrivate::open_readable(const VirtualPath& vpath) {
    VFSResult<VFSROPtr> result = Error(VFSError::UrlDoesNotExist);
    if (m_shutdown) {
        return result;
    }
    auto lambda = [&result](VFSInstance& instance, const StringSlice& path) -> bool {
        auto vfs_result = instance.open_readable(path);
        if (vfs_result) {
            result = std::move(vfs_result);
            return true;
        }
        return false;
    };
    for_each_vfs_in_tag(lambda, vpath);
    return result;
}

VFSResult<VFSRWPtr> VirtualFileSystemPrivate::open_writable(const VirtualPath& vpath,
                                                            const bool append,
                                                            const bool create) {
    VFSResult<VFSRWPtr> result = Error(VFSError::UrlDoesNotExist);
    if (m_shutdown) {
        return result;
    }
    auto lambda = [&result, append, create](VFSInstance& instance,
                                            const StringSlice& path) -> bool {
        auto vfs_result = instance.open_writable(path, append, create);
        if (vfs_result) {
            result = std::move(vfs_result);
            return true;
        }
        return false;
    };
    for_each_vfs_in_tag(lambda, vpath);
    return result;
}

VFSResult<void> VirtualFileSystemPrivate::exists(const VirtualPath& vpath) const {
    VFSResult<void> result = Error(VFSError::UrlDoesNotExist);
    if (m_shutdown) {
        return result;
    }
    auto lambda = [&result](VFSInstance& instance, const StringSlice& path) -> bool {
        auto vfs_result = instance.exists(path);
        if (vfs_result) {
            result = std::move(vfs_result);
            return true;
        }
        return false;
    };
    for_each_vfs_in_tag(lambda, vpath);
    return result;
}

VFSResult<void> VirtualFileSystemPrivate::make_directory(const VirtualPath& vpath) {
    VFSResult<void> result = Error(VFSError::FileSystem);
    if (m_shutdown) {
        return result;
    }
    auto lambda = [&result](VFSInstance& instance, const StringSlice path) -> bool {
        auto vfs_result = instance.make_directory(path);
        if (vfs_result) {
            result = std::move(vfs_result);
            return true;
        }
        return false;
    };
    for_each_vfs_in_tag(lambda, vpath);
    return result;
}

VFSResult<void> VirtualFileSystemPrivate::remove(const VirtualPath& vpath) {
    VFSResult<void> result = Error(VFSError::FileSystem);
    if (m_shutdown) {
        return result;
    }
    auto lambda = [&result](VFSInstance& instance, const StringSlice& path) -> bool {
        auto vfs_result = instance.remove(path);
        if (vfs_result) {
            result = std::move(vfs_result);
            return true;
        }
        return false;
    };
    for_each_vfs_in_tag(lambda, vpath);
    return result;
}

VFSResult<void> VirtualFileSystemPrivate::rename(const VirtualPath& vpath_from,
                                                 const StringSlice& new_path) {
    if (new_path.find("../").is_value() || new_path.find(':').is_value()) {
        return Error(VFSError::InvalidUrl);
    }
    VFSResult<void> result = Error(VFSError::FileSystem);
    if (m_shutdown) {
        return result;
    }
    auto lambda = [&result, &new_path](VFSInstance& instance, const StringSlice& path) -> bool {
        auto vfs_result = instance.rename(path, new_path);
        if (vfs_result) {
            result = std::move(vfs_result);
            return true;
        }
        return false;
    };
    for_each_vfs_in_tag(lambda, vpath_from);
    return result;
}

bool VirtualFileSystemPrivate::is_vfs_mounted(const HashMapValue& instances,
                                              const StringSlice path) const {
    for (const auto& instance : instances) {
        if (instance->path() == path) {
            return true;
        }
    }
    return false;
}

void VirtualFileSystemPrivate::priority_insert(HashMapValue& values, VFSInstance* instance) {
    auto it = values.begin();
    for (; it != values.end(); ++it) {
        if (instance->priority() < (*it)->priority()) {
            values.insert(it, instance);
            return;
        }
    }
    values.push_back(instance);
}

template <typename T>
void VirtualFileSystemPrivate::for_each_vfs_in_tag(T& fn, const VirtualPath& vpath) {
    const StringSlice tag = vpath.tag();
    const VFSString key(tag.data(), tag.size());
    {
        auto accessor = m_fileSystems.lock_read();
        auto it = accessor->find(key);
        if (it != accessor->end()) {
            for (auto& instance : it->second) {
                if (fn(*instance, vpath.path())) {
                    break;
                }
            }
        }
    }
}

template <typename T>
void VirtualFileSystemPrivate::for_each_vfs_in_tag(T& fn, const VirtualPath& vpath) const {
    const StringSlice tag = vpath.tag();
    const VFSString key(tag.data(), tag.size());
    {
        auto accessor = m_fileSystems.lock_read();
        auto it = accessor->find(key);
        if (it != accessor->end()) {
            for (auto& instance : it->second) {
                if (fn(*instance, vpath.path())) {
                    break;
                }
            }
        }
    }
}
}    // namespace modus::vfs
