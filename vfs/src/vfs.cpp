/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <vfs/vfs.hpp>
#include <vfs_private.hpp>

namespace modus::vfs {

StringSlice vfs_mount_error_to_string(const VFSMountError error) {
    switch (error) {
        case VFSMountError::AlreadyMounted:
            return "The supplied path is already mounted";
        case VFSMountError::PathIsNotADirectory:
            return "The supplied path is not a directory";

        case VFSMountError::ArchiveNotFound:
            return "Could not locate archive with the given path";
        case VFSMountError::ArchiveCorrupt:
            return "Archive is corrupt";
        case VFSMountError::TagSizeExceeded:
            return "Tag length exceeded, use a smaller tag";
        case VFSMountError::TagInvalid:
            return "Tag contains invalid characters";
        case VFSMountError::InUse:
            return "The current filesystem is still in use";
        case VFSMountError::PathNotMounted:
            return "Failed to mount the supplied path";

        default:
            return "Unknown error code";
    }
}

StringSlice vfs_error_to_string(const VFSError error) {
    switch (error) {
        case VFSError::ReadOnlyFileSystem:
            return "Read only file system, writing is not allowed";
        case VFSError::InvalidUrl:
            return "The url is not valid. This could be due to an invalid tag "
                   "or path component";
        case VFSError::UrlDoesNotExist:
            return "Could not find any file matching the supplied url";
        case VFSError::Permissions:
            return "Failed to access file to due to user permissions "
                   "restrictions";
        case VFSError::FileSystem:
            return "An error occurred on the filesystem level";
        case VFSError::NotAFile:
            return "The url is not a file";
        case VFSError::Exists:
            return "The url is pointing to a file that already exists";
        case VFSError::Unknown:
        default:
            return "Unknown error code";
    }
}

VirtualFileSystem::VirtualFileSystem() : m_impl(make_pimpl_ptr<VirtualFileSystemPrivate>()) {}

VirtualFileSystem::~VirtualFileSystem() {}

VFSMountResult VirtualFileSystem::mount_path(const VFSMountParams& params) {
    return m_impl->mount_path(params);
}

VFSMountResult VirtualFileSystem::unmount_path(StringSlice tag, StringSlice path) {
    return m_impl->unmount_path(tag, path);
}

VFSMountResult VirtualFileSystem::mount_archive(const VFSMountParams& params) {
    return m_impl->mount_archive(params);
}

VFSMountResult VirtualFileSystem::unmount_archive(StringSlice tag, StringSlice path) {
    return m_impl->unmount_archive(tag, path);
}

VFSMountResult VirtualFileSystem::unmount_all() {
    return m_impl->unmount_all();
}

VFSResult<VFSROPtr> VirtualFileSystem::open_readable(const VirtualPath& vpath) {
    return m_impl->open_readable(vpath);
}

VFSResult<VFSRWPtr> VirtualFileSystem::open_writable(const VirtualPath& vpath,
                                                     const bool append,
                                                     const bool create) {
    return m_impl->open_writable(vpath, append, create);
}

VFSResult<void> VirtualFileSystem::exists(const VirtualPath& vpath) const {
    return m_impl->exists(vpath);
}

VFSResult<void> VirtualFileSystem::make_directory(const VirtualPath& vpath) {
    return m_impl->make_directory(vpath);
}

VFSResult<void> VirtualFileSystem::remove(const VirtualPath& vpath) {
    return m_impl->remove(vpath);
}

VFSResult<void> VirtualFileSystem::rename(const VirtualPath& vpath, const StringSlice& new_path) {
    return m_impl->rename(vpath, new_path);
}

}    // namespace modus::vfs
