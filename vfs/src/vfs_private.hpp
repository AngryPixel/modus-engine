/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/io/byte_stream.hpp>
#include <os/locks.hpp>
#include <vfs/vfs.hpp>

namespace modus::vfs {

using VFSAllocator = DefaultAllocator;

using VFSString = StringWithAllocator<VFSAllocator>;

class VFSInstance;
class MODUS_VFS_EXPORT VirtualFileSystemPrivate {
   private:
    using HashMapKey = VFSString;
    using HashMapValue = Vector<VFSInstance*, VFSAllocator>;
    using HashMapType = HashMapWithAllocator<HashMapKey, HashMapValue, VFSAllocator>;

    mutable os::RWLock<HashMapType> m_fileSystems;
    volatile bool m_shutdown;

   public:
    VirtualFileSystemPrivate();

    ~VirtualFileSystemPrivate();

    VFSMountResult mount_path(const VFSMountParams& params);

    VFSMountResult unmount_path(StringSlice tag, StringSlice path);

    VFSMountResult mount_archive(const VFSMountParams& ms);

    VFSMountResult unmount_archive(StringSlice tag, StringSlice path);

    VFSMountResult unmount_all();

    VFSResult<VFSROPtr> open_readable(const VirtualPath& vpath);

    VFSResult<VFSRWPtr> open_writable(const VirtualPath& vpath,
                                      const bool append,
                                      const bool create);

    VFSResult<void> exists(const VirtualPath& vpath) const;

    VFSResult<void> make_directory(const VirtualPath& vpath);

    VFSResult<void> remove(const VirtualPath& vpath);

    VFSResult<void> rename(const VirtualPath& vpath_from, const StringSlice& new_path);

   private:
    bool is_vfs_mounted(const HashMapValue& instances, const StringSlice path) const;

    void priority_insert(HashMapValue& values, VFSInstance* instance);
    Result<StringSlice, VFSError> decode_tag(const VirtualPath& vpath) const;

    template <typename T>
    void for_each_vfs_in_tag(T& fn, const VirtualPath& vpath);

    template <typename T>
    void for_each_vfs_in_tag(T& fn, const VirtualPath& vpath) const;
};

}    // namespace modus::vfs
