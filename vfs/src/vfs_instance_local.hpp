/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vfs_instance.hpp>

namespace modus::vfs {

/// Local file system implementation
class VFSInstanceLocal final : public VFSInstance {
   private:
    VFSString m_path;

   public:
    VFSInstanceLocal(StringSlice path, const u32 priority, const bool read_only);

    static Result<VFSInstance*, VFSMountError> create(StringSlice path,
                                                      const u32 priority,
                                                      const bool read_only);

    VFSResult<VFSROPtr> open_readable(StringSlice path) override;

    VFSResult<VFSRWPtr> open_writable(StringSlice path,
                                      const bool append,
                                      const bool create) override;

    VFSResult<void> exists(StringSlice path) const override;

    VFSResult<void> make_directory(StringSlice path) override;

    VFSResult<void> remove(StringSlice path) override;

    VFSResult<void> rename(StringSlice path_from, StringSlice path_to) override;

   private:
    Result<String, VFSError> to_real_path(StringSlice path) const;
};

}    // namespace modus::vfs
