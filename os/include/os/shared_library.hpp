/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/os.hpp>

namespace modus::os {

class MODUS_OS_EXPORT SharedLibrary {
   private:
    void* m_impl;
    String m_path;

   public:
    enum OpenFlags { Local, Lazy };

    SharedLibrary();

    ~SharedLibrary() { (void)close(); }

    SharedLibrary(SharedLibrary&& rhs) noexcept;

    SharedLibrary& operator=(SharedLibrary&& rhs) noexcept;

    MODUS_CLASS_DISABLE_COPY(SharedLibrary);

    Result<void, String> open(const StringSlice path, const u32 flags = 0);

    Result<> close();

    Result<void*> resolve(const char* symbol) const;

    StringSlice library_path() const { return m_path; }

    static Result<void*> resolve_global(const char* symbol);

    static Slice<StringSlice> valid_library_extensions();

    static bool is_valid_library_name(const StringSlice slice);
};

}    // namespace modus::os
