/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/os.hpp>

namespace modus::os {

class MODUS_OS_EXPORT Timer {
    i64 m_start_counter;

   public:
    Timer();

    void reset();

    INanosec elapsed_nanoseconds() const;

    IMicrosec elapsed_microseconds() const;

    DMicrosec elapsed_microseconds_f64() const;

    DMilisec elapsed_miliseconds_f64() const;

    IMilisec elapsed_miliseconds() const;

    DSeconds elapsed_seconds() const;
};

MODUS_OS_EXPORT void sleep_sec(const ISeconds seconds);

MODUS_OS_EXPORT void sleep_ms(const IMilisec ms);
}    // namespace modus::os
