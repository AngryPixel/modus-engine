/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/os.hpp>

namespace modus::os {

namespace lock_impl {
class MODUS_OS_EXPORT Mutex final {
    friend class Condition;

   private:
    void* m_data;

   public:
    Mutex();
    ~Mutex();
    MODUS_CLASS_DISABLE_COPY_MOVE(Mutex);
    void acquire();
    void release();
};

class MODUS_OS_EXPORT RWLock final {
   private:
    void* m_data;

   public:
    RWLock();
    ~RWLock();
    MODUS_CLASS_DISABLE_COPY_MOVE(RWLock);

    void acquire_read();
    void acquire_write();
    void release_read();
    void release_write();
};

class MODUS_OS_EXPORT Condition final {
   private:
    void* m_data;

   public:
    Condition();
    ~Condition();
    MODUS_CLASS_DISABLE_COPY_MOVE(Condition);

    void wait(Mutex& mutex);

    void signal_one();
    void signal_all();
};

}    // namespace lock_impl

template <typename T>
class MODUS_OS_EXPORT Mutex final {
    friend class Condition;

   private:
    lock_impl::Mutex m_impl;
    T m_value;

   private:
    inline lock_impl::Mutex& impl() { return m_impl; }

   private:
    inline T* data() { return &m_value; }

   public:
    using value_type = T;
    using this_type = Mutex<T>;

    class MODUS_OS_EXPORT Accessor {
        friend class Condition;

       private:
        Mutex<T>* m_mutex;

        lock_impl::Mutex& impl() { return m_mutex->impl(); }

       public:
        Accessor() = delete;
        inline explicit Accessor(Mutex<T>& mutex) : m_mutex(&mutex) { m_mutex->m_impl.acquire(); }
        inline ~Accessor() { m_mutex->m_impl.release(); }
        MODUS_CLASS_DISABLE_COPY_MOVE(Accessor);
        inline T& operator*() { return *m_mutex->data(); }
        inline T* operator->() { return m_mutex->data(); }
    };

    Mutex() : m_value() {}
    Mutex(const T& value) : m_value(value) {}
    Mutex(T&& value) : m_value(std::forward<T>(value)) {}
    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(Mutex, Mutex<T>);

    inline Accessor lock() { return Accessor(*this); }
};

template <typename T>
class MODUS_OS_EXPORT RWLock final {
   private:
    lock_impl::RWLock m_impl;
    T m_value;

   private:
    inline T* data() { return &m_value; }

   public:
    using value_type = T;
    using this_type = RWLock<T>;

    class MODUS_OS_EXPORT AccessorRead {
       private:
        RWLock<T>* m_mutex;

       public:
        AccessorRead() = delete;
        inline AccessorRead(RWLock<T>& mutex) : m_mutex(&mutex) { m_mutex->m_impl.acquire_read(); }
        inline ~AccessorRead() { m_mutex->m_impl.release_read(); }
        MODUS_CLASS_DISABLE_COPY_MOVE(AccessorRead);
        inline T& operator*() { return *m_mutex->data(); }
        inline T* operator->() { return m_mutex->data(); }
    };

    class MODUS_OS_EXPORT AccessorWrite {
       private:
        RWLock<T>* m_mutex;

       public:
        AccessorWrite() = delete;
        inline AccessorWrite(RWLock<T>& mutex) : m_mutex(&mutex) {
            m_mutex->m_impl.acquire_write();
        }
        inline ~AccessorWrite() { m_mutex->m_impl.release_write(); }
        MODUS_CLASS_DISABLE_COPY_MOVE(AccessorWrite);
        inline T& operator*() { return *m_mutex->data(); }
        inline T* operator->() { return m_mutex->data(); }
    };

    RWLock() = delete;
    RWLock(const T& value) : m_value(value) {}
    RWLock(T&& value) : m_value(std::forward<T>(value)) {}
    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(RWLock, RWLock<T>);

    inline AccessorRead lock_read() { return *this; }
    inline AccessorWrite lock_write() { return *this; }
};

class MODUS_OS_EXPORT Condition final {
   private:
    lock_impl::Condition m_impl;

   public:
    Condition() = default;
    MODUS_CLASS_DISABLE_COPY_MOVE(Condition);

    template <typename T>
    inline void wait(typename T::Accessor& mutex_accessor) {
        m_impl.wait(mutex_accessor.impl());
    }

    inline void signal_one() { m_impl.signal_one(); }

    inline void signal_all() { m_impl.signal_all(); }
};

}    // namespace modus::os
