/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/os.hpp>

#include <core/io/byte_stream.hpp>
#include <os/path.hpp>

namespace modus::os {

struct MODUS_CORE_EXPORT FileHandle {
    u64 data;
};

class MODUS_OS_EXPORT File final : public ISeekableWriter {
   private:
    FileHandle m_handle;
    usize m_size;
    usize m_offset;

   public:
    File(FileHandle handle, const usize size);

    File(const File&) = delete;

    File(File&&) noexcept;

    File& operator=(const File&) = delete;

    File& operator=(File&&) noexcept;

    virtual ~File() override;

    IOResult<usize> read(SliceMut<u8> slice) override final;

    IOResult<usize> write(const Slice<u8> slice) override final;

    IOResult<usize> seek(const usize position) override final;

    IOResult<usize> seek_end() override final;

    IOResult<usize> position() const override final;

    usize size() const override final;

    void flush() override final;
};

class MODUS_OS_EXPORT FileBuilder {
   private:
    u32 m_flags = 0;

   public:
    /// Create the file if it doesn't exist
    FileBuilder& create();
    /// Append to the file, will truncate otherwise
    FileBuilder& append();
    /// Open in read mode
    FileBuilder& read();
    /// Open in write mode
    FileBuilder& write();
    /// Open in text mode
    FileBuilder& text();
    /// Open in binary mode
    FileBuilder& binary();
    PathResult<File> open(const StringSlice& path) const;
};
}    // namespace modus::os
