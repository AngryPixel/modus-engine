/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/os.hpp>

#include <core/strong_type.hpp>

namespace modus::os {

MODUS_DECLARE_STRONG_TYPE(ThreadId, u64, 0);

MODUS_OS_EXPORT u32 os_cpu_count();

class MODUS_OS_EXPORT Thread {
    friend class PrivateThreadAccessor;

   public:
    enum class Priority { RealTime, VeryHigh, High, Default, Low, VeryLow, Batch };

   private:
    void* m_data;

   protected:
    Thread();

   public:
    using ThreadFn = void (*)(void*);

    static ThreadId current_thread_id();

    static void yield();

    static Result<> set_current_thread_name(const StringSlice& name);

    static Result<> set_current_thread_priority(const Priority priority);

    MODUS_CLASS_DISABLE_COPY(Thread);

    Thread(Thread&& rhs) noexcept;

    Thread& operator=(Thread&& rhs) noexcept;

    virtual ~Thread();

    Result<> spawn();

    void join();

    ThreadId id() const;

   protected:
    virtual void run() = 0;
};

#if defined(MODUS_DEBUG) && !defined(MODUS_OS_ENABLE_THREAD_WATCHER)
#define MODUS_OS_ENABLE_THREAD_WATCHER
#endif

class MODUS_OS_EXPORT ThreadWatcher {
   private:
#if defined(MODUS_OS_ENABLE_THREAD_WATCHER)
    ThreadId m_id;
#endif
   public:
#if defined(MODUS_OS_ENABLE_THREAD_WATCHER)
    ThreadWatcher() : m_id(Thread::current_thread_id()) {}
#endif

    inline void set_to_current_thread() {
#if defined(MODUS_OS_ENABLE_THREAD_WATCHER)
        m_id = Thread::current_thread_id();
#endif
    }

    inline bool validate() const {
#if defined(MODUS_OS_ENABLE_THREAD_WATCHER)
        const ThreadId current_thread_id = Thread::current_thread_id();
        return m_id == current_thread_id;
#else
        return true;
#endif
    }
};

}    // namespace modus::os

template <>
struct fmt::formatter<modus::os::ThreadId> : fmt::formatter<modus::u64> {
    template <typename FormatContext>
    auto format(const modus::os::ThreadId id, FormatContext& ctx) {
        return fmt::formatter<modus::u64>::format(id.value(), ctx);
    }
};
