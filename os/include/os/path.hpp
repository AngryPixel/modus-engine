/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/os.hpp>

namespace modus::os {

enum class PathError {
    Permissions,
    NotFound,
    Exists,
    InvalidPath,
    ReadOnlyPath,
    PathConversion,
    PathTooLong,
    NotAFile,
    InvalidOperation,
    Unknwon,
};

MODUS_OS_EXPORT StringSlice path_error_to_str(const PathError error);

template <typename T>
using PathResult = Result<T, PathError>;

class MODUS_OS_EXPORT PathInfo {
   private:
    usize m_size = 0;
    usize m_flags = 0;
    u64 m_last_mod_time_sec = 0;

   public:
    PathInfo() = default;
    PathInfo(const usize flags, const usize size, const u64 last_mod_time_sec);
    PathInfo(const PathInfo&) = default;
    PathInfo(PathInfo&&) = default;
    PathInfo& operator=(const PathInfo&) = default;
    PathInfo& operator=(PathInfo&&) = default;

    inline usize size_bytes() const { return m_size; }

    bool is_file() const;
    bool is_directory() const;
    bool is_symlink() const;
    bool is_executable() const;

    // Return modification time since unix epoch in seconds
    u64 last_mod_time_sec() const { return m_last_mod_time_sec; }
};

class MODUS_OS_EXPORT Path {
   public:
    static constexpr char k_path_separator = '/';

    static void join_inplace(String& output, const StringSlice p1, const StringSlice p2);

    static void join_inplace(String& output, const StringSlice p1);

    inline static String join(const StringSlice p1, const StringSlice p2) {
        String r;
        join_inplace(r, p1, p2);
        return r;
    }

    static StringSlice get_extension(const StringSlice path);

    static StringSlice remove_extension(const StringSlice path);

    static StringSlice get_basename(const StringSlice path);

    static StringSlice get_path(const StringSlice path);

    static PathResult<String> get_real_path(const StringSlice path);

    static PathResult<void> make_directory(const StringSlice path);

    static PathResult<PathInfo> path_info(const StringSlice path);

    static PathResult<void> remove(const StringSlice path);

    static PathResult<void> rename(const StringSlice oldPath, const StringSlice newPath);

    static PathResult<void> cd(const StringSlice path);

    static PathResult<String> cwd();

    static bool is_absolute(const StringSlice path);

    static inline bool is_file(const StringSlice path) {
        auto result = path_info(path);
        if (result.is_value()) {
            return result.value().is_file();
        }
        return false;
    }

    static inline bool is_directory(const StringSlice path) {
        auto result = path_info(path);
        if (result.is_value()) {
            return result.value().is_directory();
        }
        return false;
    }

    static inline bool is_executable(const StringSlice path) {
        auto result = path_info(path);
        if (result) {
            return result->is_file() && result->is_executable();
        }
        return false;
    }

    static inline bool exists(const StringSlice path) { return path_info(path).is_value(); }

    static Result<String, void> os_temp_path();

    static Result<String, void> os_user_path();

    static Result<String, void> os_user_pref_config_path();

    static Result<String, void> os_user_pref_data_path();

    static Result<String, void> os_user_cache_path();

    static Result<String, void> executable_path();

    static Result<String, void> executable_directory();

    struct ListDirectoryCallback {
        virtual void on_list(const StringSlice path) = 0;
    };

    struct DefaultListDirectoryCallback final : public ListDirectoryCallback {
        Vector<String> m_paths;

        DefaultListDirectoryCallback();
        void on_list(const StringSlice path) override;
    };

    // This fucntion currently does not follow symlinks
    static PathResult<void> list_directory(ListDirectoryCallback& callback,
                                           const StringSlice path,
                                           const bool recursive,
                                           const bool include_directories);
};

}    // namespace modus::os
