/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <file_unix.hpp>
#include <os/file.hpp>
#include <path_internal.hpp>

namespace modus::os {

static Error<IOError> errno_to_io_error(const int error) {
    IOError err = IOError::Unknown;
    switch (error) {
        case EBADF:
            err = IOError::BadHandle;
            break;
        case EINTR:
            err = IOError::Interrupted;
            break;
        case EIO:
        case EINVAL:
        case EFAULT:
        case EPIPE:
            err = IOError::IO;
            break;
        default:
            break;
    }
    return Error(err);
}

IOResult<usize> os_file_read(OSFileHandle& handle, SliceMut<u8>& slice) {
    const isize result = ::read(handle, slice.data(), slice.size());
    if (result >= 0) {
        return Ok(usize(result));
    }
    return errno_to_io_error(errno);
}

IOResult<usize> os_file_write(OSFileHandle& handle, const Slice<u8>& slice) {
    const isize result = ::write(handle, slice.data(), slice.size());
    if (result >= 0) {
        return Ok(usize(result));
    }
    return errno_to_io_error(errno);
}

void os_file_close(OSFileHandle& handle) {
    const int result = ::close(handle);
    (void)result;
#if defined(MODUS_DEBUG)
    if (result != 0) {
        const int error = errno;
        fprintf(stderr, "Failed to close file hanlde (%u): %s", error, strerror(error));
    }
#endif
}

void os_file_flush(OSFileHandle& handle) {
    const int result = ::fsync(handle);
    (void)result;
#if defined(MODUS_DEBUG)
    if (result != 0) {
        const int error = errno;
        fprintf(stderr, "Failed to flush file hanlde (%u): %s", error, strerror(error));
    }
#endif
}

IOResult<usize> os_file_seek(OSFileHandle& handle, const usize position) {
    const off64_t result = ::lseek64(handle, position, SEEK_SET);
    if (result == off64_t(-1)) {
        return Error(IOError::SeekFailed);
    }
    return Ok(usize(result));
}

IOResult<usize> os_file_seek_end(OSFileHandle& handle) {
    const off64_t result = ::lseek64(handle, 0, SEEK_END);
    if (result == off64_t(-1)) {
        return Error(IOError::SeekFailed);
    }
    return Ok(usize(result));
}

IOResult<usize> os_file_position(OSFileHandle& handle) {
    const off64_t result = ::lseek64(handle, 0, SEEK_CUR);
    if (result == off64_t(-1)) {
        return Error(IOError::SeekFailed);
    }
    return Ok(usize(result));
}

PathResult<File> os_file_open(const StringSlice& path,
                              const bool create,
                              const bool append,
                              const bool read,
                              const bool write,
                              const bool binary) {
    (void)binary;

    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }

    PathResult<PathInfo> r_path_info = path_info_native(os_path.data());
    usize size = 0;
    // doesn't exist but can't create
    if (!r_path_info) {
        if (r_path_info.error() != PathError::NotFound) {
            return Error(r_path_info.error());
        } else if (!create) {
            return Error(r_path_info.error());
        }
    } else {
        const PathInfo& path_info = r_path_info.value();
        if (!path_info.is_file()) {
            return Error(PathError::NotAFile);
        }
        size = path_info.size_bytes();
    }

    const mode_t create_mode = S_IWUSR | S_IRUSR;
    int flags = 0;
    // Handle the case where a file already exists on disk and we want
    // to recreate it. Set O_TRUNC to just truncate the file to simulate a
    // create.
    if (create) {
        if (!r_path_info) {
            flags |= O_CREAT | O_EXCL;
        } else {
            flags |= O_TRUNC;
        }
    }

    if (write) {
        if (append) {
            flags |= O_APPEND;
            flags |= O_WRONLY;
        } else {
            flags |= O_TRUNC;
            size = 0;
        }
    }

    if (read && write) {
        flags |= O_RDWR;
    } else if (read) {
        flags |= O_RDONLY;
    } else {
        flags |= O_WRONLY;
    }

    int open_result = ::open(os_path.data(), flags, create_mode);
    if (open_result == -1) {
        return errno_to_path_error(errno);
    }
    return Ok(File(os_file_handle_to_file_handle(open_result), size));
}
}    // namespace modus::os
