/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/file.hpp>

namespace modus::os {

using OSFileHandle = HANDLE;
static_assert(sizeof(OSFileHandle) <= sizeof(FileHandle));

inline bool os_file_hanlde_is_valid(const OSFileHandle& handle) {
    return handle != INVALID_HANDLE_VALUE;
}

inline OSFileHandle file_hanlde_to_os_file_handle(const FileHandle& handle) {
    return reinterpret_cast<OSFileHandle>(handle.data);
}

inline FileHandle os_file_handle_to_file_handle(const OSFileHandle& handle) {
    FileHandle file_handle = {reinterpret_cast<u64>(handle)};
    return file_handle;
}

inline FileHandle invalid_file_handle() {
    return os_file_handle_to_file_handle(INVALID_HANDLE_VALUE);
}

IOResult<usize> os_file_read(OSFileHandle& handle, SliceMut<u8>& slice);

IOResult<usize> os_file_write(OSFileHandle& handle, const Slice<u8>& slice);

IOResult<usize> os_file_seek(OSFileHandle& handle, const usize position);

IOResult<usize> os_file_seek_end(OSFileHandle& handle);

void os_file_close(OSFileHandle& handle);

void os_file_flush(OSFileHandle& handle);

IOResult<usize> os_file_position(OSFileHandle& handle);

PathResult<File> os_file_open(const StringSlice& path,
                              const bool create,
                              const bool append,
                              const bool read,
                              const bool write,
                              const bool binary);
}    // namespace modus::os
