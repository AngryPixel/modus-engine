/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/path.hpp>
#include <path_internal.hpp>

#if defined(MODUS_OS_UNIX)
#include <dirent.h>
#endif

#if defined(MODUS_OS_WIN32)
#include <ctime>
#endif

namespace modus::os {

static constexpr char k_extension_separator = '.';

StringSlice path_error_to_str(const PathError error) {
    switch (error) {
        case PathError::Permissions:
            return "Lack of permission to access/modify path";
        case PathError::NotFound:
            return "Path does not exist";
        case PathError::Exists:
            return "Path already exists";
        case PathError::ReadOnlyPath:
            return "Path is read only";
        case PathError::PathConversion:
            return "There was an issue converting the path it's native "
                   "representation";
        case PathError::NotAFile:
            return "Path is not a file";
        case PathError::InvalidOperation:
            return "Invalid operation";
        case PathError::Unknwon:
            return "Unknown path error occurred";
        default:
            modus_assert_message(false, "Should not be reached");
            return StringSlice();
    }
}

#if defined(MODUS_OS_WIN32)
Error<PathError> last_err_to_path_error(const DWORD error) {
    PathError err = PathError::Unknwon;
    switch (error) {
        case ERROR_PATH_NOT_FOUND:
        case ERROR_FILE_NOT_FOUND:
            err = PathError::NotFound;
            break;
        case ERROR_ACCESS_DENIED:
            err = PathError::Permissions;
            break;
        case ERROR_FILE_EXISTS:
        case ERROR_ALREADY_EXISTS:
            err = PathError::Exists;
            break;
        case ERROR_INVALID_PARAMETER:
            err = PathError::InvalidOperation;
            break;
            // case ERROR_IS_SUBST_TARGET
            //     err = PathError::InvalidPath;
            //     break;
        /*case EROFS:
            err = PathError::ReadOnlyPath;
            break;
        case ERROR_INVALID__:
            err = PathError::PathTooLong;
            break;*/
        default:
#if defined(MODUS_DEBUG)
            fprintf(stderr, "errno_to_path_error: Unhandle error code (%d)\n", error);
#endif
            break;
    }
    return Error(err);
}
#endif

Error<PathError> errno_to_path_error(const int error) {
    PathError err = PathError::Unknwon;
    switch (error) {
        case ENOENT:
            err = PathError::NotFound;
            break;
        case EACCES:
        case EPERM:
            err = PathError::Permissions;
            break;
        case EEXIST:
            err = PathError::Exists;
            break;
        case EINVAL:
            err = PathError::InvalidPath;
            break;
        case EROFS:
            err = PathError::ReadOnlyPath;
            break;
        case ENAMETOOLONG:
            err = PathError::PathTooLong;
            break;
        default:
#if defined(MODUS_DEBUG)
            fprintf(stderr, "errno_to_path_error: Unhandle error code (%d): %s\n", error,
                    strerror(error));
#endif
            break;
    }
    return Error(err);
}

void Path::join_inplace(String& output, const StringSlice p1, const StringSlice p2) {
    output.reserve(p1.size() + p2.size() + 1);
    output.append(p1.data(), p1.size());
    if (p1.size() != 0 && p1[p1.size() - 1] != k_path_separator) {
        output += k_path_separator;
    }
    output.append(p2.data(), p2.size());
}

void Path::join_inplace(String& output, const StringSlice p1) {
    if (!output.empty() && output[output.size() - 1] != k_path_separator) {
        output += k_path_separator;
    }
    output.append(p1.data(), p1.size());
}

StringSlice Path::get_extension(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    auto find_result = path.rfind(k_extension_separator);
    if (find_result.is_value()) {
        return path.sub_slice(find_result.value() + 1, path.size());
    }
    return StringSlice();
}

StringSlice Path::remove_extension(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    auto find_result = path.rfind(k_extension_separator);
    if (find_result.is_value()) {
        return path.sub_slice(0, find_result.value());
    }
    return path;
}

StringSlice Path::get_basename(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    auto find_result = path.rfind(k_path_separator);
    if (find_result.is_value()) {
        return path.sub_slice(find_result.value() + 1, path.size());
    }
    return path;
}

StringSlice Path::get_path(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    auto find_result = path.rfind(k_path_separator);
    if (find_result.is_value()) {
        return path.sub_slice(0, find_result.value());
    }
    return path;
}

PathResult<String> Path::get_real_path(const StringSlice path) {
    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }
#if defined(MODUS_OS_UNIX)
    OSPathBuffer fullPath;
    char* result = realpath(os_path.data(), fullPath.data());
    if (result != nullptr) {
        return Ok<String>(fullPath.data());
    }
    return errno_to_path_error(errno);
#else
    OSPathBuffer output;
    wchar_t* result = _wfullpath(os_path.data(), output.data(), output.size());
    if (result != nullptr) {
        String result;
        if (to_path(result, output)) {
            return Ok(std::move(result));
        }
        return Error(PathError::PathConversion);
    }
    return last_err_to_path_error(GetLastError());
#endif
}

#if defined(MODUS_OS_UNIX)
static inline int modus_mkdir(const OSPathCharType* path) {
    return ::mkdir(path, (S_IWUSR | S_IRUSR | S_IXUSR));
}

static inline OSPathCharType* modus_strchr(OSPathCharType* str, const OSPathCharType ch) {
    return strchr(str, ch);
}
#elif defined(MODUS_OS_WIN32)
static inline int modus_mkdir(const OSPathCharType* path) {
    return _wmkdir(path);
}

static inline OSPathCharType* modus_strchr(OSPathCharType* str, const OSPathCharType ch) {
    return wcschr(str, ch);
}
#else
#error Missing implementation of mkdir
#endif

static PathResult<void> make_directory_recursive(OSPathCharType* path, OSPathCharType* offset) {
    errno = 0;
    OSPathCharType* char_loc = modus_strchr(offset, k_os_path_sep);
    if (char_loc) {
        *char_loc = '\0';
        const int errCode = modus_mkdir(path);
        PathResult<PathInfo> result_info = path_info_native(path);
        *char_loc = k_os_path_sep;
        if (errCode == -1) {
            const bool is_dir = result_info.is_value() ? result_info.value().is_directory() : false;
            const int error = errno;
            if (is_dir && error != EEXIST) {
                return errno_to_path_error(error);
            } else if (!is_dir) {
                return errno_to_path_error(error);
            }
        }
        *char_loc = k_os_path_sep;
        return make_directory_recursive(path, char_loc + 1);
    } else {
        if (modus_mkdir(path) == -1) {
            PathResult<PathInfo> result_info = path_info_native(path);
            const int error = errno;
            const bool is_dir = result_info.is_value() ? result_info.value().is_directory() : false;
            if (is_dir && error != EEXIST) {
                return errno_to_path_error(error);
            } else if (!is_dir) {
                return errno_to_path_error(error);
            }
        }
        return Ok<>();
    }
}

PathResult<void> Path::make_directory(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }
    OSPathCharType* offset = os_path.data();
#if defined(MODUS_OS_UNIX)
    if (os_path[0] == k_path_separator) {
        offset++;
    }
#else
    if (!PathIsRelativeW(os_path.data())) {
        while (*offset != L'\0' && *offset != L':') {
            offset++;
        }
        offset++;
        if (*offset != L'\0') {
            modus_assert(*offset == L'\\');
            offset++;
        }
    }
#endif
    return make_directory_recursive(os_path.data(), offset);
}

#if defined(MODUS_OS_UNIX)
#define modus_stat stat
#define modus_statfn stat
#define modus_remove ::remove
#define modus_rename ::rename
#define S_ISEXEC(m) ((m & S_IXUSR) == S_IXUSR)
#elif defined(MODUS_OS_WIN32)
#define modus_stat _stat64
#define modus_statfn _wstat64
#define S_ISREG(m) ((m & _S_IFREG) == _S_IFREG)
#define S_ISDIR(m) ((m & _S_IFDIR) == _S_IFDIR)
#define S_ISEXEC(m) ((m & _S_IEXEC) == _S_IEXEC)
#define S_ISLNK(m) (false)
#define modus_remove _wremove
#define modus_rename _wrename
#else
#error "Unknown platform"
#endif

enum PathInfoFlags { k_file = 1 << 0, k_dir = 1 << 1, k_symlink = 1 << 2, k_executable = 1 << 3 };

PathInfo::PathInfo(const usize flags, const usize size, const u64 last_mod_time_sec)
    : m_size(size), m_flags(flags), m_last_mod_time_sec(last_mod_time_sec) {}

bool PathInfo::is_file() const {
    return m_flags & PathInfoFlags::k_file;
}

bool PathInfo::is_directory() const {
    return m_flags & PathInfoFlags::k_dir;
}

bool PathInfo::is_symlink() const {
    return m_flags & PathInfoFlags::k_symlink;
}

bool PathInfo::is_executable() const {
    return m_flags & PathInfoFlags::k_executable;
}

PathResult<PathInfo> path_info_native(const OSPathCharType* os_path) {
    modus_assert(os_path != nullptr);
    struct modus_stat file_info;
    if (modus_statfn(os_path, &file_info) != 0) {
        return errno_to_path_error(errno);
    }

    usize file_size = 0;
    usize flags = 0;
    if (S_ISREG(file_info.st_mode)) {
        flags |= PathInfoFlags::k_file;
        file_size = file_info.st_size;
    }

    if (S_ISDIR(file_info.st_mode)) {
        flags |= PathInfoFlags::k_dir;
    }

    if (S_ISLNK(file_info.st_mode)) {
        flags |= PathInfoFlags::k_symlink;
    }

    if (S_ISEXEC(file_info.st_mode)) {
        flags |= PathInfoFlags::k_executable;
    }

#if defined(MODUS_OS_UNIX)
    return Ok(PathInfo(flags, file_size, static_cast<u64>(file_info.st_mtim.tv_sec)));
#else
    return Ok(PathInfo(flags, file_size, static_cast<u64>(file_info.st_mtime)));
#endif
}

PathResult<PathInfo> Path::path_info(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }
    return path_info_native(os_path.data());
}

#if defined(MODUS_OS_LINUX)
static int ftw_handler(const char* path, const struct stat*, int, struct FTW*) {
    return remove(path);
}

static PathResult<void> remove_recursive(const OSPathCharType* os_path) {
    const int result = nftw(os_path, ftw_handler, 32, FTW_DEPTH | FTW_PHYS);
    if (result != 0) {
        return errno_to_path_error(errno);
    }
    return Ok<>();
}
PathResult<void> Path::remove(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    return remove_recursive(path.data());
}

#else
PathResult<void> Path::remove(const StringSlice path) {
    modus_assert(path.data() != nullptr);
    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }
    const size_t len = lstrlenW(os_path.data());
    if (len + 1 < os_path.size()) {
        os_path[len] = '\0';
        os_path[len + 1] = '\0';
    }
    SHFILEOPSTRUCTW file_op = {NULL,
                               FO_DELETE,
                               os_path.data(),
                               L"",
                               FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT,
                               false,
                               0,
                               L""};
    const int ret = SHFileOperationW(&file_op);
    if (ret != 0) {
        return Error(PathError::Unknwon);
    }
    return Ok<>();
}
#endif

PathResult<void> Path::rename(const StringSlice oldPath, const StringSlice newPath) {
    modus_assert(oldPath.data() != nullptr && newPath != nullptr);
    OSPathBuffer osPathOld;
    if (!to_os_path(osPathOld, oldPath)) {
        return Error(PathError::PathConversion);
    }
    OSPathBuffer osPathNew;
    if (!to_os_path(osPathNew, newPath)) {
        return Error(PathError::PathConversion);
    }

#if defined(MODUS_OS_WIN32)
    // if the target file exists, we have to delete it first before we
    // can move it.
    if (path_info_native(osPathNew.data())) {
        if (auto r = Path::remove(newPath); !r) {
            return r;
        }
    }
#endif

    if (modus_rename(osPathOld.data(), osPathNew.data()) != 0) {
        return errno_to_path_error(errno);
    }
    return Ok<>();
}

PathResult<void> Path::cd(const StringSlice path) {
    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }
#if defined(MODUS_OS_UNIX)
    const int result = ::chdir(os_path.data());
    if (result != 0) {
        return errno_to_path_error(errno);
    }
    return Ok<>();
#else
    const int result = _wchdir(os_path.data());
    if (result != 0) {
        return errno_to_path_error(errno);
    }
    return Ok<>();
#endif
}

PathResult<String> Path::cwd() {
#if defined(MODUS_OS_UNIX)
    OSPathBuffer os_path;
    const char* result = getcwd(os_path.data(), os_path.size());
    if (result == nullptr) {
        return errno_to_path_error(errno);
    }
    return Ok(String(os_path.data()));
#else
    OSPathBuffer os_path;
    const wchar_t* result = _wgetcwd(os_path.data(), os_path.size());
    if (result == nullptr) {
        return errno_to_path_error(errno);
    }
    String path_utf8;
    if (to_path(path_utf8, os_path)) {
        return Ok(std::move(path_utf8));
    }
    return Error(PathError::PathConversion);
#endif
}

bool Path::is_absolute(const StringSlice path) {
#if defined(MODUS_OS_UNIX)
    return !path.is_empty() ? path[0] == k_path_separator : false;
#else
    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        modus_assert_message(false, "Failed to convert path!");
        return false;
    }
    return !PathIsRelative(os_path.data());
#endif
}

Result<String, void> Path::os_temp_path() {
#if defined(MODUS_OS_LINUX)
    return Ok(String("/tmp"));
#else
    OSPathBuffer os_path;
    const DWORD size = GetTempPathW(os_path.size(), os_path.data());
    if (size == 0) {
        return Error<>();
    }
    String result;
    if (!to_path(result, os_path)) {
        return Error<>();
    }
    return Ok(std::move(result));
#endif
}

Result<String, void> Path::os_user_path() {
#if defined(MODUS_OS_UNIX)
    const char* home = getenv("HOME");
    if (home != nullptr) {
        return Ok(String(home));
    }

    struct passwd* pwuid = getpwuid(getuid());
    if (pwuid != nullptr) {
        return Ok(String(pwuid->pw_dir));
    }
    return Error<>();
#else
    OSPathBuffer os_path;
    if (FAILED(SHGetFolderPathW(NULL, CSIDL_PROFILE, NULL, 0, os_path.data()))) {
        return Error<>();
    }
    String result;
    if (!to_path(result, os_path)) {
        return Error<>();
    }
    return Ok(std::move(result));
#endif
}

Result<String, void> Path::os_user_pref_config_path() {
#if defined(MODUS_OS_LINUX)
    const char* config_path = getenv("XDG_CONFIG_HOME");
    if (config_path != nullptr) {
        return Ok(String(config_path));
    }

    auto home_result = os_user_path();
    if (!home_result) {
        return home_result;
    }
    String path = home_result.release_value();
    Path::join_inplace(path, ".config");
    return Ok(path);
#else
    OSPathBuffer os_path;
    if (FAILED(SHGetFolderPathW(NULL, CSIDL_APPDATA, NULL, 0, os_path.data()))) {
        return Error<>();
    }
    String result;
    if (!to_path(result, os_path)) {
        return Error<>();
    }
    return Ok(std::move(result));
#endif
}

Result<String, void> Path::os_user_pref_data_path() {
#if defined(MODUS_OS_LINUX)
    const char* config_path = getenv("XDG_DATA_HOME");
    if (config_path != nullptr) {
        return Ok(String(config_path));
    }

    auto home_result = os_user_path();
    if (!home_result) {
        return home_result;
    }
    String path = home_result.release_value();
    Path::join_inplace(path, ".local/share");
    return Ok(path);
#else
    OSPathBuffer os_path;
    if (FAILED(SHGetFolderPathW(NULL, CSIDL_APPDATA, 0, 0, os_path.data()))) {
        return Error<>();
    }
    String result;
    if (!to_path(result, os_path)) {
        return Error<>();
    }
    return Ok(std::move(result));
#endif
}

Result<String, void> Path::os_user_cache_path() {
#if defined(MODUS_OS_LINUX)
    const char* config_path = getenv("XDG_CACHE_HOME");
    if (config_path != nullptr) {
        return Ok(String(config_path));
    }

    auto home_result = os_user_path();
    if (!home_result) {
        return home_result;
    }
    String path = home_result.release_value();
    Path::join_inplace(path, ".cache");
    return Ok(path);
#else
    OSPathBuffer os_path;
    if (FAILED(SHGetFolderPathW(NULL, CSIDL_APPDATA, 0, 0, os_path.data()))) {
        return Error<>();
    }
    String result;
    if (!to_path(result, os_path)) {
        return Error<>();
    }
    return Ok(std::move(result));
#endif
}

Result<String, void> Path::executable_path() {
#if defined(MODUS_OS_LINUX)
    OSPathBuffer ospath;
    const ssize_t r = readlink("/proc/self/exe", ospath.data(), ospath.size());
    if (r < 0) {
        return Error<>();
    }
    return Ok(String(ospath.data(), r));
#else
    OSPathBuffer ospath;
    if (GetModuleFileName(nullptr, ospath.data(), ospath.size()) == 0) {
        return Error<>();
    }
    String result;
    if (!to_path(result, ospath)) {
        return Error<>();
    }
    return Ok(std::move(result));
#endif
}

Result<String, void> Path::executable_directory() {
    auto r_exec = executable_path();
    if (!r_exec) {
        return r_exec;
    }
    return Ok(Path::get_path(*r_exec).to_str());
}

PathResult<void> Path::list_directory(Path::ListDirectoryCallback& callback,
                                      const StringSlice path,
                                      const bool recursive,
                                      const bool include_directories) {
#if defined(MODUS_OS_UNIX)
    struct DirScope {
        DIR* m_dir;
        DirScope(DIR* d) : m_dir(d) {}
        ~DirScope() { closedir(m_dir); }
    };

    constexpr size_t kInitialDirQueueCapacity = 32;
    Vector<String> dir_queue;
    dir_queue.reserve(kInitialDirQueueCapacity);
    dir_queue.push_back(path.to_str());

    String path_buffer;
    while (!dir_queue.empty()) {
        const String cur_path = std::move(dir_queue.back());
        dir_queue.erase(dir_queue.begin() + dir_queue.size() - 1);
        DIR* current_dir = opendir(cur_path.c_str());
        if (current_dir == nullptr) {
            return errno_to_path_error(errno);
        }
        DirScope dir_scope(current_dir);
        struct dirent* current_dirent = nullptr;
        while ((current_dirent = readdir(current_dir)) != nullptr) {
            StringSlice item_name = current_dirent->d_name;

            if (item_name.starts_with(".") || item_name.starts_with("..")) {
                continue;
            }
            if (current_dirent->d_type == DT_REG) {
                path_buffer = cur_path;
                Path::join_inplace(path_buffer, item_name);
                callback.on_list(path_buffer);
            } else if (current_dirent->d_type == DT_DIR && (include_directories || recursive)) {
                path_buffer = cur_path;
                Path::join_inplace(path_buffer, item_name);
                if (include_directories) {
                    callback.on_list(path_buffer);
                }
                if (recursive) {
                    dir_queue.push_back(path_buffer);
                }
            }
        }
    }
    return Ok<>();
#elif defined(MODUS_OS_WIN32)

    struct DirScope {
        HANDLE m_handle;
        DirScope(HANDLE h) : m_handle(h) {}
        ~DirScope() { FindClose(m_handle); }
    };

    OSPathBuffer ospath;
    if (!to_os_path(ospath, path)) {
        return Error(PathError::PathConversion);
    }

    constexpr size_t kInitialDirQueueCapacity = 32;
    Vector<std::wstring> dir_queue;
    dir_queue.reserve(kInitialDirQueueCapacity);
    dir_queue.push_back(std::wstring(ospath.data()));

    std::wstring path_buffer;
    String utf8_path_buffer;
    WIN32_FIND_DATA find_data;
    HANDLE find_handle;

    auto wpath_join = [](std::wstring& path, const wchar_t* other) -> void {
        if (!path.empty() && path[path.size() - 1] != L'\\') {
            path += L'\\';
        }
        path += other;
    };

    while (!dir_queue.empty()) {
        const std::wstring cur_path = std::move(dir_queue.back());
        dir_queue.erase(dir_queue.begin() + dir_queue.size() - 1);

        // We need to append \\*.* to the current path so that FindFirst file
        // list all the files inside the directory
        path_buffer = cur_path;
        wpath_join(path_buffer, L"*.*");
        find_handle = FindFirstFile(path_buffer.c_str(), &find_data);
        if (find_handle == INVALID_HANDLE_VALUE) {
            return Error(last_err_to_path_error(GetLastError()));
        }
        DirScope dir_scope(find_handle);
        do {
            if (wcscmp(find_data.cFileName, L".") == 0 || wcscmp(find_data.cFileName, L"..") == 0) {
                continue;
            }
            if ((find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
                (include_directories || recursive)) {
                path_buffer = cur_path;
                wpath_join(path_buffer, find_data.cFileName);
                if (include_directories) {
                    utf8_path_buffer.clear();
                    if (!to_path(utf8_path_buffer, path_buffer)) {
                        return Error(PathError::PathConversion);
                    }
                    callback.on_list(utf8_path_buffer);
                }
                if (recursive) {
                    dir_queue.push_back(path_buffer);
                }
            } else {
                path_buffer = cur_path;
                wpath_join(path_buffer, find_data.cFileName);
                utf8_path_buffer.clear();
                if (!to_path(utf8_path_buffer, path_buffer)) {
                    return Error(PathError::PathConversion);
                }
                callback.on_list(utf8_path_buffer);
            }
        } while (FindNextFile(find_handle, &find_data) != 0);

        const int last_error = GetLastError();
        if (last_error != ERROR_NO_MORE_FILES) {
            return Error(last_err_to_path_error(last_error));
        }
    }
    return Ok<>();
#else
#error "Missing implementation"
#endif
}

Path::DefaultListDirectoryCallback::DefaultListDirectoryCallback() {
    m_paths.reserve(8);
}

void Path::DefaultListDirectoryCallback::on_list(const StringSlice path) {
    m_paths.push_back(path.to_str());
}

}    // namespace modus::os
