/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/path.hpp>
namespace modus::os {

#if defined(MODUS_OS_UNIX)
using OSPathCharType = char;
static constexpr char k_os_path_sep = '/';
static const StringSlice k_os_path_sep_str = "/";
static constexpr size_t GetMaxPathSize() {
    return PATH_MAX;
}
#elif defined(MODUS_OS_WIN32)
using OSPathCharType = wchar_t;
static constexpr OSPathCharType k_os_path_sep = L'\\';
static const StringSlice k_os_path_sep_str = "\\";
static constexpr size_t GetMaxPathSize() {
    return MAX_PATH;
}
#else
#error "Unsupport OS paltform"
#endif

using OSPathBuffer = std::array<OSPathCharType, GetMaxPathSize()>;

static inline bool to_os_path(OSPathBuffer& os_path, const StringSlice& path) {
#if defined(MODUS_OS_UNIX)
    path.to_cstr(os_path.data(), GetMaxPathSize());
    return true;
#elif defined(MODUS_OS_WIN32)

    const int n_len_wide = MultiByteToWideChar(CP_UTF8, 0, path.data(), path.size(), nullptr, 0);

    if (n_len_wide == 0 || static_cast<size_t>(n_len_wide + 1) >= os_path.size()) {
        return false;
    }

    const int result =
        MultiByteToWideChar(CP_UTF8, 0, path.data(), path.size(), os_path.data(), os_path.size());

    os_path[n_len_wide] = L'\0';
    if (result != 0) {
        std::replace(os_path.begin(), os_path.begin() + n_len_wide, L'/', k_os_path_sep);
    }
    return result != 0;
#else
#error "Unsupport OS platform"
#endif
}

static inline bool to_path(String& path, const OSPathBuffer& os_path) {
    path.clear();
#if defined(MODUS_OS_UNIX)
    path.assign(os_path.data());
    return true;
#elif defined(MODUS_OS_WIN32)
    const size_t os_path_size = lstrlenW(os_path.data());
    const int n_len_wide =
        WideCharToMultiByte(CP_UTF8, 0, os_path.data(), os_path_size, nullptr, 0, nullptr, nullptr);

    if (n_len_wide == 0) {
        return false;
    }
    path.resize(n_len_wide, '\0');

    const int result = WideCharToMultiByte(CP_UTF8, 0, os_path.data(), os_path_size, path.data(),
                                           n_len_wide, nullptr, nullptr);
    if (result != 0) {
        std::replace(path.begin(), path.begin() + n_len_wide, '\\', '/');
    }
    path = StringSlice(path).replace("//", "/");
    return result != 0;
#else
#error "Unsupported OS platform"
#endif
}

#if defined(MODUS_OS_WIN32)
static inline bool to_path(String& path, const std::wstring& os_path) {
    const size_t os_path_size = os_path.size();
    const int n_len_wide =
        WideCharToMultiByte(CP_UTF8, 0, os_path.data(), os_path_size, nullptr, 0, nullptr, nullptr);

    if (n_len_wide == 0) {
        return false;
    }
    path.resize(n_len_wide, '\0');

    const int result = WideCharToMultiByte(CP_UTF8, 0, os_path.data(), os_path_size, path.data(),
                                           n_len_wide, nullptr, nullptr);
    if (result != 0) {
        std::replace(path.begin(), path.begin() + n_len_wide, '\\', '/');
    }
    path = StringSlice(path).replace("//", "/");
    return result != 0;
}
#endif

Error<PathError> errno_to_path_error(const int error);

#if defined(MODUS_OS_WIN32)
Error<PathError> last_err_to_path_error(const DWORD error);
#endif

PathResult<PathInfo> path_info_native(const OSPathCharType* os_path);

}    // namespace modus::os
