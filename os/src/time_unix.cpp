/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <ctime>
#include <os/time.hpp>

namespace modus::os {

static i64 get_elapsed_nanoseconds() {
    i64 elapsed_nano;
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    elapsed_nano = now.tv_sec;
    elapsed_nano *= 1000000000;
    elapsed_nano += now.tv_nsec;
    return elapsed_nano;
}

Timer::Timer() : m_start_counter(get_elapsed_nanoseconds()) {}

void Timer::reset() {
    m_start_counter = get_elapsed_nanoseconds();
}

INanosec Timer::elapsed_nanoseconds() const {
    return INanosec(get_elapsed_nanoseconds() - m_start_counter);
}

IMicrosec Timer::elapsed_microseconds() const {
    return IMicrosec((get_elapsed_nanoseconds() - m_start_counter) / 1000);
}

DMicrosec Timer::elapsed_microseconds_f64() const {
    return DMicrosec(elapsed_nanoseconds());
}

DMilisec Timer::elapsed_miliseconds_f64() const {
    return DMilisec(elapsed_nanoseconds());
}

IMilisec Timer::elapsed_miliseconds() const {
    return IMilisec((get_elapsed_nanoseconds() - m_start_counter) / 1000000);
}

DSeconds Timer::elapsed_seconds() const {
    return DSeconds(elapsed_nanoseconds());
}

void sleep_sec(const ISeconds seconds) {
    ::sleep(seconds.count());
}

void sleep_ms(const IMilisec ms) {
    ::usleep(IMicrosec(ms).count());
}

}    // namespace modus::os
