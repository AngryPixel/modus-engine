/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <ctime>
#include <os/time.hpp>

namespace modus::os {

static i64 get_performance_counter() {
    LARGE_INTEGER counter;
    QueryPerformanceCounter(&counter);
    return counter.QuadPart;
}

static i64 get_performance_frequency() {
    static i64 kPerformanceFerquency = []() -> i64 {
        LARGE_INTEGER frequency;
        QueryPerformanceFrequency(&frequency);
        return frequency.QuadPart;
    }();
    return kPerformanceFerquency;
}

Timer::Timer() : m_start_counter(get_performance_counter()) {}

void Timer::reset() {
    m_start_counter = get_performance_counter();
}

INanosec Timer::elapsed_nanoseconds() const {
    const i64 diff = (get_performance_counter() - m_start_counter) * 1000000000;
    return INanosec(diff / get_performance_frequency());
}

IMicrosec Timer::elapsed_microseconds() const {
    const i64 diff = (get_performance_counter() - m_start_counter) * 1000000;
    return IMicrosec(diff / get_performance_frequency());
}

DMicrosec Timer::elapsed_microseconds_f64() const {
    const i64 diff = (get_performance_counter() - m_start_counter) * 1000000;
    return DMicrosec(f64(diff) / f64(get_performance_frequency()));
}

DMilisec Timer::elapsed_miliseconds_f64() const {
    const i64 diff = (get_performance_counter() - m_start_counter) * 1000;
    return DMilisec(f64(diff) / f64(get_performance_frequency()));
}

IMilisec Timer::elapsed_miliseconds() const {
    const i64 diff = (get_performance_counter() - m_start_counter) * 1000;
    return IMilisec(diff / get_performance_frequency());
}

DSeconds Timer::elapsed_seconds() const {
    const i64 diff = get_performance_counter() - m_start_counter;
    return DSeconds(f64(diff) / f64(get_performance_frequency()));
}

/*
    inline u64 elapsed_nanoseconds() const {
        const u64 current = time::get_performance_counter();
        return (1000000000 * (current - m_start_counter)) / time::get_performance_frequency();
    }

    u64 elapsed_microseconds() const {
        const u64 current = time::get_performance_counter();
        return (1000 * (current - m_start_counter)) / time::get_performance_frequency();
    }

    f64 elapsed_microseconds_f64() const {
        const u64 current = time::get_performance_counter();
        return f64(1000000 * (current - m_start_counter)) / f64(time::get_performance_frequency());
    }

    f64 elapsed_miliseconds_f64() const;

    u64 elapsed_miliseconds() const {
        const u64 current = time::get_performance_counter();
        return (1000 * (current - m_start_counter)) / time::get_performance_frequency();
    }

    f64 elapsed_seconds() const {
        const u64 current = time::get_performance_counter();
        return f64(current - m_start_counter) / f64(time::get_performance_frequency());
    }
    */
void sleep_sec(const ISeconds seconds) {
    ::_sleep(seconds.count());
}

void sleep_ms(const IMilisec ms) {
    Sleep(ms.count());
}

}    // namespace modus::os
