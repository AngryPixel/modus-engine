/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/guid_gen.hpp>

#if defined(MODUS_OS_LINUX)
#include <uuid/uuid.h>
#endif

namespace modus::os::guid {

MODUS_OS_EXPORT Result<modus::GID, void> generate() {
#if defined(MODUS_OS_LINUX)
    static_assert(sizeof(GID::ByteStorageType) == sizeof(uuid_t));
    GID::ByteStorageType guid_bytes;
    uuid_generate(guid_bytes.data());
    return Ok(GID(guid_bytes));
#else
    ::GUID os_guid;
    const HRESULT result = CoCreateGuid(&os_guid);
    if (FAILED(result)) {
        return Error<>();
    }
    modus::GID::ByteStorageType* guid_bytes =
        reinterpret_cast<modus::GID::ByteStorageType*>(&os_guid);
    return Ok(modus::GID(*guid_bytes));
#endif
}

}    // namespace modus::os::guid
