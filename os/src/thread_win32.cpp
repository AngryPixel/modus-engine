/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/thread.hpp>
#include <path_internal.hpp>

namespace modus::os {

u32 os_cpu_count() {
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    return u32(sysinfo.dwNumberOfProcessors);
}
static_assert(sizeof(ThreadId::value_type) >= sizeof(HANDLE));

static constexpr inline HANDLE handle_from_data(void* data) {
    return HANDLE(data);
}

class PrivateThreadAccessor {
   public:
    static DWORD WINAPI win_thread_fn(LPVOID input) {
        Thread* thread = reinterpret_cast<Thread*>(input);
        thread->run();
        return TRUE;
    }
};

Result<void, void> Thread::set_current_thread_name(const StringSlice& name) {
    OSPathBuffer wname;
    if (!to_os_path(wname, name)) {
        return Error<void>();
    }
    const HRESULT result = SetThreadDescription(GetCurrentThread(), wname.data());
    if (FAILED(result)) {
        return Error<void>();
    }
    return Ok<void>();
}

Result<void, void> Thread::spawn() {
    if (m_data != nullptr) {
        return Error<void>();
    }

    HANDLE h = CreateThread(nullptr, 0, PrivateThreadAccessor::win_thread_fn, this, 0, nullptr);

    if (h == INVALID_HANDLE_VALUE) {
        return Error<void>();
    }
    m_data = h;
    return Ok<void>();
}

ThreadId Thread::current_thread_id() {
    return ThreadId(GetCurrentThreadId());
}

void Thread::yield() {
    // Win32 doesn't have a yield option
    Sleep(0);
}

Thread::Thread() : m_data(nullptr) {}

Thread::~Thread() {
    modus_assert_message(m_data == nullptr,
                         "Thread was not joined. This will cause memory "
                         "corruption with the shared thread state.");
}

Thread::Thread(Thread&& rhs) noexcept : m_data(rhs.m_data) {
    rhs.m_data = nullptr;
}

Thread& Thread::operator=(Thread&& rhs) noexcept {
    if (this != &rhs) {
        if (m_data != nullptr) {
            join();
        }
        m_data = rhs.m_data;
        rhs.m_data = nullptr;
    }
    return *this;
}

void Thread::join() {
    if (m_data != nullptr) {
        HANDLE h = handle_from_data(m_data);
        WaitForSingleObject(h, INFINITE);
        CloseHandle(h);
        m_data = nullptr;
    }
}

ThreadId Thread::id() const {
    return ThreadId(GetThreadId(handle_from_data(m_data)));
}

Result<void, void> Thread::set_current_thread_priority(const Thread::Priority priority) {
    int thread_priority = THREAD_PRIORITY_NORMAL;
    switch (priority) {
        case Thread::Priority::RealTime: {
            thread_priority = THREAD_PRIORITY_TIME_CRITICAL;
            break;
        }
        case Thread::Priority::VeryHigh: {
            thread_priority = THREAD_PRIORITY_HIGHEST;
            break;
        }
        case Thread::Priority::High: {
            thread_priority = THREAD_PRIORITY_ABOVE_NORMAL;
            break;
        }
        case Thread::Priority::Default: {
            break;
        }
        case Thread::Priority::Low: {
            thread_priority = THREAD_PRIORITY_BELOW_NORMAL;
            break;
        }
        case Thread::Priority::VeryLow: {
            thread_priority = THREAD_PRIORITY_LOWEST;
            break;
        }
        case Thread::Priority::Batch: {
            // NOT supported!
            return Error<void>();
        }
        default:
            modus_assert_message(false, "Should not be reached!");
            return Error<void>();
    }

    if (SetThreadPriority(GetCurrentThread(), thread_priority) != TRUE) {
        Error<void>();
    }
    return Ok<void>();
}

}    // namespace modus::os
