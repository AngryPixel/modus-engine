/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/file.hpp>
#include <os/path.hpp>

#if defined(MODUS_OS_UNIX)
#include <file_unix.hpp>
#elif defined(MODUS_OS_WIN32)
#include <file_win32.hpp>
#else
#error "File not implemented for current OS";
#endif

namespace modus::os {

File::File(FileHandle handle, const usize size) : m_handle(handle), m_size(size), m_offset(size) {}

File::~File() {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    if (os_file_hanlde_is_valid(os_hdl)) {
        os_file_close(os_hdl);
    }
}

File::File(File&& rhs) noexcept
    : m_handle(rhs.m_handle), m_size(rhs.m_size), m_offset(rhs.m_offset) {
    rhs.m_handle = invalid_file_handle();
    rhs.m_size = 0;
    rhs.m_offset = 0;
}

File& File::operator=(File&& rhs) noexcept {
    if (this != &rhs) {
        m_handle = rhs.m_handle;
        rhs.m_handle = invalid_file_handle();
        rhs.m_size = 0;
        rhs.m_offset = 0;
    }
    return *this;
}

IOResult<usize> File::read(SliceMut<u8> slice) {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    modus_assert(os_file_hanlde_is_valid(os_hdl));
    return os_file_read(os_hdl, slice);
}

IOResult<usize> File::write(const Slice<u8> slice) {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    modus_assert(os_file_hanlde_is_valid(os_hdl));
    auto result = os_file_write(os_hdl, slice);
    if (result && result.value() > 0) {
        m_size = std::max(m_offset + result.value(), m_size);
    }
    return result;
}

IOResult<usize> File::seek(const usize position) {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    modus_assert(os_file_hanlde_is_valid(os_hdl));
    auto result = os_file_seek(os_hdl, position);
    if (result) {
        m_offset = result.value();
    }
    return result;
}

IOResult<usize> File::seek_end() {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    modus_assert(os_file_hanlde_is_valid(os_hdl));
    auto result = os_file_seek_end(os_hdl);
    if (result) {
        m_offset = result.value();
    }
    return result;
}

IOResult<usize> File::position() const {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    modus_assert(os_file_hanlde_is_valid(os_hdl));
    return os_file_position(os_hdl);
}

usize File::size() const {
    return m_size;
}

void File::flush() {
    OSFileHandle os_hdl = file_hanlde_to_os_file_handle(m_handle);
    modus_assert(os_file_hanlde_is_valid(os_hdl));
    return os_file_flush(os_hdl);
}

enum FileBuilderFlags {
    k_append = 1 << 0,
    k_read = 1 << 1,
    k_write = 1 << 2,
    k_binary = 1 << 3,
    k_text = 1 << 4,
    k_create = 1 << 5
};

FileBuilder& FileBuilder::create() {
    m_flags |= FileBuilderFlags::k_create;
    return *this;
}

FileBuilder& FileBuilder::append() {
    m_flags |= FileBuilderFlags::k_append;
    return *this;
}

FileBuilder& FileBuilder::read() {
    m_flags |= FileBuilderFlags::k_read;
    return *this;
}

FileBuilder& FileBuilder::write() {
    m_flags |= FileBuilderFlags::k_write;
    return *this;
}

FileBuilder& FileBuilder::binary() {
    m_flags |= FileBuilderFlags::k_binary;
    m_flags &= ~(FileBuilderFlags::k_text);
    return *this;
}

FileBuilder& FileBuilder::text() {
    m_flags |= FileBuilderFlags::k_text;
    m_flags &= ~(FileBuilderFlags::k_binary);
    return *this;
}

PathResult<File> FileBuilder::open(const StringSlice& path) const {
    return os_file_open(path, (m_flags & FileBuilderFlags::k_create) == FileBuilderFlags::k_create,
                        (m_flags & FileBuilderFlags::k_append) == FileBuilderFlags::k_append,
                        (m_flags & FileBuilderFlags::k_read) == FileBuilderFlags::k_read,
                        (m_flags & FileBuilderFlags::k_write) == FileBuilderFlags::k_write,
                        (m_flags & FileBuilderFlags::k_binary) == FileBuilderFlags::k_binary);
}

}    // namespace modus::os
