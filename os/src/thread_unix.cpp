/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/thread.hpp>

namespace modus::os {

u32 os_cpu_count() {
    const int num_cpu = sysconf(_SC_NPROCESSORS_ONLN);
    if (num_cpu < 0) {
        return 1u;
    }
    return u32(num_cpu);
}

static_assert(sizeof(ThreadId::value_type) >= sizeof(pthread_t));

static inline pthread_t* pthread_from_data(void* data) {
    return reinterpret_cast<pthread_t*>(data);
}

Thread::Thread(Thread&& rhs) noexcept : m_data(rhs.m_data) {
    rhs.m_data = nullptr;
}

Thread& Thread::operator=(Thread&& rhs) noexcept {
    if (&rhs != this) {
        if (m_data != nullptr) {
            join();
        }
        m_data = rhs.m_data;
        rhs.m_data = nullptr;
    }
    return *this;
}

class PrivateThreadAccessor {
   public:
    static void* pthread_function(void* input) {
        Thread* thread = reinterpret_cast<Thread*>(input);
        thread->run();
        return nullptr;
    }
};

Result<> Thread::set_current_thread_name(const StringSlice& name) {
    static constexpr usize k_max_thread_name = 32;
    char thread_name[k_max_thread_name];
    name.to_cstr(thread_name, k_max_thread_name);
    if (pthread_setname_np(pthread_self(), thread_name) != 0) {
        return Error<>();
    }
    return Ok<>();
}

Result<> Thread::spawn() {
    if (m_data != nullptr) {
        return Error<>();
    }

    m_data = MODUS_NEW pthread_t;
    if (m_data == nullptr) {
        modus_assert_message(false, "Out of memory");
        return Error<>();
    }

    pthread_t* pthread = pthread_from_data(m_data);
    pthread_attr_t thread_attr;
    pthread_attr_init(&thread_attr);
    // pthread_attr_setstacksize(&thread_attr,stackSize);
    pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
    const int result =
        pthread_create(pthread, &thread_attr, PrivateThreadAccessor::pthread_function, this);
    pthread_attr_destroy(&thread_attr);
    if (result != 0) {
        return Error<>();
    }
    return Ok<>();
}

ThreadId Thread::current_thread_id() {
    return ThreadId(pthread_self());
}

void Thread::yield() {
    if (pthread_yield() != 0) {
        modus_panic("Failed to yield current thread");
    }
}

Thread::Thread() : m_data(nullptr) {}

Thread::~Thread() {
    modus_assert_message(m_data == nullptr,
                         "Thread was not joined. This will cause memory "
                         "corruption with the shared thread state.");
}

void Thread::join() {
    if (m_data != nullptr) {
        pthread_t* pthread = pthread_from_data(m_data);
        void* return_result;
        pthread_join(*pthread, &return_result);
        m_data = nullptr;
        delete pthread;
    }
}

ThreadId Thread::id() const {
    return ThreadId(*pthread_from_data(m_data));
}

Result<> Thread::set_current_thread_priority(const Thread::Priority priority) {
    sched_param params;
    int queue_type = 0;
    params.sched_priority = 0;
    const int fifo_min_priority = sched_get_priority_min(SCHED_FIFO);
    const int fifo_max_priority = sched_get_priority_max(SCHED_FIFO);
    const int diff = fifo_max_priority - fifo_min_priority;
    switch (priority) {
        case Thread::Priority::RealTime: {
            queue_type = SCHED_RR;
            const int rr_max_priority = sched_get_priority_max(SCHED_RR);
            params.sched_priority = rr_max_priority;
            break;
        }
        case Thread::Priority::VeryHigh: {
            queue_type = SCHED_FIFO;
            params.sched_priority = fifo_max_priority;
            break;
        }
        case Thread::Priority::High: {
            queue_type = SCHED_FIFO;
            params.sched_priority = fifo_min_priority + int(diff * 0.80);
            break;
        }
        case Thread::Priority::Default: {
            queue_type = SCHED_OTHER;
            break;
        }
        case Thread::Priority::Low: {
            queue_type = SCHED_FIFO;
            params.sched_priority = fifo_min_priority + int(diff * 0.25);
            break;
        }
        case Thread::Priority::VeryLow: {
            queue_type = SCHED_IDLE;
            break;
        }
        case Thread::Priority::Batch: {
            queue_type = SCHED_BATCH;
            break;
        }
        default:
            modus_assert_message(false, "Should not be reached!");
            return Error<>();
    }

    if (pthread_setschedparam(pthread_self(), queue_type, &params) != 0) {
        Error<>();
    }
    return Ok<>();
}

}    // namespace modus::os
