/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/locks.hpp>

namespace modus::os {

namespace lock_impl {

static inline pthread_mutex_t* pthread_mutex_from_data(void* data) {
    return reinterpret_cast<pthread_mutex_t*>(data);
}

Mutex::Mutex() {
    pthread_mutexattr_t mutex_attr;
    pthread_mutexattr_init(&mutex_attr);
    pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
    MODUS_CHECKED_NEW(m_data, pthread_mutex_t());
    pthread_mutex_t* mutex = pthread_mutex_from_data(m_data);
    const int create_result = pthread_mutex_init(mutex, &mutex_attr);
    pthread_mutexattr_destroy(&mutex_attr);

    if (create_result != 0) {
        modus_panic("Failed to create mutex");
    }
}

Mutex::~Mutex() {
    pthread_mutex_t* mutex = pthread_mutex_from_data(m_data);
    const int result = pthread_mutex_destroy(mutex);
    delete mutex;
    if (result != 0) {
        modus_panic("Failed to destroy mutex");
    }
}

void Mutex::acquire() {
    pthread_mutex_t* mutex = pthread_mutex_from_data(m_data);
    if (pthread_mutex_lock(mutex) != 0) {
        modus_panic("Failed to lock mutex");
    }
}

void Mutex::release() {
    pthread_mutex_t* mutex = pthread_mutex_from_data(m_data);
    if (pthread_mutex_unlock(mutex) != 0) {
        modus_panic("Failed to lock mutex");
    }
}

static inline pthread_rwlock_t* pthread_rwlock_from_data(void* data) {
    return reinterpret_cast<pthread_rwlock_t*>(data);
}

RWLock::RWLock() {
    MODUS_CHECKED_NEW(m_data, pthread_rwlock_t());
    pthread_rwlock_t* rwlock = pthread_rwlock_from_data(m_data);
    if (pthread_rwlock_init(rwlock, nullptr) != 0) {
        modus_panic("Failed to init mutexrw");
    }
}

RWLock::~RWLock() {
    pthread_rwlock_t* rwlock = pthread_rwlock_from_data(m_data);
    if (pthread_rwlock_destroy(rwlock) != 0) {
        modus_panic("Failed to destroy mutexrw");
    }
    delete rwlock;
}

void RWLock::acquire_read() {
    pthread_rwlock_t* rwlock = pthread_rwlock_from_data(m_data);
    if (pthread_rwlock_rdlock(rwlock) != 0) {
        modus_panic("Failed to acquire read lock");
    }
}

void RWLock::acquire_write() {
    pthread_rwlock_t* rwlock = pthread_rwlock_from_data(m_data);
    if (pthread_rwlock_rdlock(rwlock) != 0) {
        modus_panic("Failed to acquire write lock");
    }
}

void RWLock::release_read() {
    pthread_rwlock_t* rwlock = pthread_rwlock_from_data(m_data);
    if (pthread_rwlock_unlock(rwlock) != 0) {
        modus_panic("Failed to unlock read lock");
    }
}

void RWLock::release_write() {
    pthread_rwlock_t* rwlock = pthread_rwlock_from_data(m_data);
    if (pthread_rwlock_unlock(rwlock) != 0) {
        modus_panic("Failed to unlock write lock");
    }
}

static inline pthread_cond_t* pthread_cond_from_data(void* data) {
    return reinterpret_cast<pthread_cond_t*>(data);
}

Condition::Condition() {
    MODUS_CHECKED_NEW(m_data, pthread_cond_t());
    pthread_cond_t* condition = pthread_cond_from_data(m_data);
    if (pthread_cond_init(condition, nullptr) != 0) {
        modus_panic("Failed to create condition");
    }
}

Condition::~Condition() {
    pthread_cond_t* condition = pthread_cond_from_data(m_data);
    if (pthread_cond_destroy(condition) != 0) {
        modus_panic("Failed to destroy condition");
    }
    delete condition;
}

void Condition::wait(Mutex& mutex) {
    pthread_cond_t* condition = pthread_cond_from_data(m_data);
    pthread_mutex_t* pthread_mutex = pthread_mutex_from_data(mutex.m_data);
    if (pthread_cond_wait(condition, pthread_mutex) != 0) {
        modus_panic("Failed to wait on mutex");
    }
}

void Condition::signal_one() {
    pthread_cond_t* condition = pthread_cond_from_data(m_data);
    if (pthread_cond_signal(condition) != 0) {
        modus_panic("Failed to signal one");
    }
}

void Condition::signal_all() {
    pthread_cond_t* condition = pthread_cond_from_data(m_data);
    if (pthread_cond_broadcast(condition) != 0) {
        modus_panic("Failed to signal one");
    }
}

}    // namespace lock_impl

}    // namespace modus::os
