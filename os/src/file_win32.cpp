/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <file_win32.hpp>
#include <os/file.hpp>
#include <path_internal.hpp>

namespace modus::os {

static Error<IOError> error_to_io_error(const DWORD error) {
    IOError err = IOError::Unknown;
    switch (error) {
        case ERROR_INVALID_HANDLE:
            err = IOError::BadHandle;
            break;
        case EINTR:
            err = IOError::Interrupted;
            break;
        case EIO:
        case EINVAL:
        case EFAULT:
        case EPIPE:
            err = IOError::IO;
            break;
        default:
            break;
    }
    return Error(err);
}

IOResult<usize> os_file_read(OSFileHandle& handle, SliceMut<u8>& slice) {
    DWORD num_bytes_read = 0;
    const BOOL result = ReadFile(handle, slice.data(), slice.size(), &num_bytes_read, nullptr);
    if (result == TRUE) {
        return Ok(usize(num_bytes_read));
    }
    return error_to_io_error(GetLastError());
}

IOResult<usize> os_file_write(OSFileHandle& handle, const Slice<u8>& slice) {
    DWORD num_bytes_written = 0;
    const BOOL result = WriteFile(handle, slice.data(), slice.size(), &num_bytes_written, nullptr);
    if (result == TRUE) {
        return Ok(usize(num_bytes_written));
    }
    return error_to_io_error(GetLastError());
}

void os_file_close(OSFileHandle& handle) {
    const BOOL result = CloseHandle(handle);
    (void)result;
#if defined(MODUS_DEBUG)
    if (result != TRUE) {
        const DWORD error = GetLastError();
        fprintf(stderr, "Failed to close file handle (%u)\n", error);
    }
#endif
}

void os_file_flush(OSFileHandle& handle) {
    const BOOL result = FlushFileBuffers(handle);
    (void)result;
#if defined(MODUS_DEBUG)
    if (result != TRUE) {
        const DWORD error = GetLastError();
        fprintf(stderr, "Failed to flush file handle (%u)\n", error);
    }
#endif
}

IOResult<usize> os_file_seek(OSFileHandle& handle, const usize position) {
    LARGE_INTEGER new_pos = {0};
    LARGE_INTEGER set_position;
    set_position.QuadPart = position;
    const BOOL result = SetFilePointerEx(handle, set_position, &new_pos, FILE_BEGIN);
    if (result != TRUE) {
        return Error(IOError::SeekFailed);
    }
    return Ok(usize(new_pos.QuadPart));
}

IOResult<usize> os_file_seek_end(OSFileHandle& handle) {
    LARGE_INTEGER new_pos = {0};
    LARGE_INTEGER set_position{0};
    const BOOL result = SetFilePointerEx(handle, set_position, &new_pos, FILE_END);
    if (result != TRUE) {
        return Error(IOError::SeekFailed);
    }
    return Ok(usize(new_pos.QuadPart));
}

IOResult<usize> os_file_position(OSFileHandle& handle) {
    LARGE_INTEGER new_pos = {0};
    LARGE_INTEGER set_position{0};
    const BOOL result = SetFilePointerEx(handle, set_position, &new_pos, FILE_CURRENT);
    if (result != TRUE) {
        return Error(IOError::SeekFailed);
    }
    return Ok(usize(new_pos.QuadPart));
}

PathResult<File> os_file_open(const StringSlice& path,
                              const bool create,
                              const bool append,
                              const bool read,
                              const bool write,
                              const bool binary) {
    (void)binary;

    OSPathBuffer os_path;
    if (!to_os_path(os_path, path)) {
        return Error(PathError::PathConversion);
    }

    PathResult<PathInfo> r_path_info = path_info_native(os_path.data());
    usize size = 0;
    // doesn't exist but can't create
    if (!r_path_info) {
        if (r_path_info.error() != PathError::NotFound) {
            return Error(r_path_info.error());
        } else if (!create) {
            return Error(r_path_info.error());
        }
    } else {
        const PathInfo& path_info = r_path_info.value();
        if (!path_info.is_file()) {
            return Error(PathError::NotAFile);
        }
        size = path_info.size_bytes();
    }

    DWORD flags = 0;
    DWORD create_flags = 0;
    // Handle the case where a file already exists on disk and we want
    // to recreate it.
    if (create) {
        if (!r_path_info) {
            create_flags |= CREATE_NEW;
        } else {
            create_flags |= TRUNCATE_EXISTING;
        }
    } else {
        create_flags |= OPEN_EXISTING;
    }

    if (write) {
        flags |= GENERIC_WRITE;
        if (!append) {
            flags = flags & (~FILE_APPEND_DATA);
            size = 0;
        } else if (!create) {
            create_flags |= TRUNCATE_EXISTING;
        }
    }

    if (read) {
        flags |= GENERIC_READ;
    }

    DWORD share_flags = 0;

    HANDLE h = CreateFile2(os_path.data(), flags, share_flags, create_flags, nullptr);
    if (h == INVALID_HANDLE_VALUE) {
        return last_err_to_path_error(GetLastError());
    }
    return Ok(File(os_file_handle_to_file_handle(h), size));
}
}    // namespace modus::os
