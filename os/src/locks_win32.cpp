/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <os/locks.hpp>

namespace modus::os {

namespace lock_impl {

static inline CRITICAL_SECTION* cr_from_data(void* data) {
    return reinterpret_cast<CRITICAL_SECTION*>(data);
}

Mutex::Mutex() {
    MODUS_CHECKED_NEW(m_data, CRITICAL_SECTION());
    const BOOL result = InitializeCriticalSectionAndSpinCount(cr_from_data(m_data), 0x0400);
    if (result != TRUE) {
        modus_panic("Failed to create critical section");
    }
}

Mutex::~Mutex() {
    DeleteCriticalSection(cr_from_data(m_data));
}

void Mutex::acquire() {
    EnterCriticalSection(cr_from_data(m_data));
}

void Mutex::release() {
    LeaveCriticalSection(cr_from_data(m_data));
}

static inline SRWLOCK* srwlock_from_data(void* data) {
    return reinterpret_cast<SRWLOCK*>(data);
}

RWLock::RWLock() {
    MODUS_CHECKED_NEW(m_data, SRWLOCK());
    InitializeSRWLock(srwlock_from_data(m_data));
}

RWLock::~RWLock() {
    delete srwlock_from_data(m_data);
}

void RWLock::acquire_read() {
    AcquireSRWLockShared(srwlock_from_data(m_data));
}

void RWLock::acquire_write() {
    AcquireSRWLockExclusive(srwlock_from_data(m_data));
}

void RWLock::release_read() {
    ReleaseSRWLockShared(srwlock_from_data(m_data));
}

void RWLock::release_write() {
    ReleaseSRWLockExclusive(srwlock_from_data(m_data));
}

static inline CONDITION_VARIABLE* cv_from_data(void* data) {
    return reinterpret_cast<CONDITION_VARIABLE*>(data);
}

Condition::Condition() {
    MODUS_CHECKED_NEW(m_data, CONDITION_VARIABLE());
    InitializeConditionVariable(cv_from_data(m_data));
}

Condition::~Condition() {
    delete cv_from_data(m_data);
}

void Condition::wait(Mutex& mutex) {
    CRITICAL_SECTION* cs = cr_from_data(mutex.m_data);
    CONDITION_VARIABLE* cv = cv_from_data(m_data);
    const BOOL result = SleepConditionVariableCS(cv, cs, INFINITE);
    if (result != TRUE) {
        modus_panic("Failed to sleep on condition variable");
    }
}

void Condition::signal_one() {
    CONDITION_VARIABLE* cv = cv_from_data(m_data);
    WakeConditionVariable(cv);
}

void Condition::signal_all() {
    CONDITION_VARIABLE* cv = cv_from_data(m_data);
    WakeAllConditionVariable(cv);
}

}    // namespace lock_impl

}    // namespace modus::os
