/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <os/shared_library.hpp>
#include <path_internal.hpp>

#include <dlfcn.h>

namespace modus::os {

SharedLibrary::SharedLibrary() : m_impl(nullptr) {}

SharedLibrary::SharedLibrary(SharedLibrary&& rhs) noexcept
    : m_impl(rhs.m_impl), m_path(std::move(rhs.m_path)) {
    rhs.m_impl = nullptr;
}

SharedLibrary& SharedLibrary::operator=(SharedLibrary&& rhs) noexcept {
    if (this != &rhs) {
        (void)close();
        m_impl = rhs.m_impl;
        m_path = std::move(rhs.m_path);
        rhs.m_impl = nullptr;
    }
    return *this;
}

Result<void, String> SharedLibrary::open(const StringSlice path, const u32 flags) {
    if (!close()) {
        return Error<String>(modus::format("Failed to close open library:{}", m_path));
    }
    OSPathBuffer native_path;
    if (!to_os_path(native_path, path)) {
        return Error<String>(modus::format("Failed to convert path '{}' to native type.", path));
    }

    (void)dlerror();

    int dl_flags = 0;

    if (flags & OpenFlags::Lazy) {
        dl_flags |= RTLD_LAZY;
    } else {
        dl_flags |= RTLD_NOW;
    }

    if (flags & OpenFlags::Local) {
        dl_flags |= RTLD_LOCAL;
    } else {
        dl_flags |= RTLD_GLOBAL;
    }

    void* result = dlopen(native_path.data(), dl_flags);
    if (result == nullptr) {
        return Error<String>(dlerror());
    }

    m_impl = result;
    m_path = path.to_str();
    return Ok<>();
}

Result<> SharedLibrary::close() {
    if (m_impl != nullptr) {
        const int r = dlclose(m_impl);
        if (r != 0) {
            return Error<>();
        }
        m_impl = nullptr;
        m_path.clear();
        return Ok<>();
    }
    return Ok<>();
}

Result<void*, void> SharedLibrary::resolve(const char* symbol) const {
    if (m_impl == nullptr) {
        return Error<>();
    }
    void* sym = dlsym(m_impl, symbol);
    if (sym == nullptr) {
        return Error<>();
    }
    return Ok(sym);
}

Result<void*> SharedLibrary::resolve_global(const char* symbol) {
    void* sym = dlsym(RTLD_DEFAULT, symbol);
    if (sym == nullptr) {
        return Error<>();
    }
    return Ok(sym);
}

Slice<StringSlice> SharedLibrary::valid_library_extensions() {
    static const StringSlice kValidExtensions[] = {".so"};
    return Slice(kValidExtensions);
}

bool SharedLibrary::is_valid_library_name(const StringSlice slice) {
    for (auto& ext : valid_library_extensions()) {
        if (slice.ends_with(ext)) {
            return true;
        }
    }
    return false;
}

}    // namespace modus::os
