/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <os/env.hpp>
#include <os/path.hpp>

namespace modus::os {

Result<String> get_env(const char* env_var) {
    const char* var = getenv(env_var);
    if (var == nullptr) {
        return Error<>();
    }
    return Ok<String>(var);
}

Result<String> find_executable(const StringSlice in_executable) {
    auto r_env = get_env("PATH");
    if (!r_env) {
        return Error<>();
    }

    StringSlice paths = *r_env;
#if (MODUS_OS_UNIX)
    const char path_sep = ':';
#else
    const char path_sep = ';';
#endif
    size_t prev_pos = 0;
    size_t cur_pos = 0;

    String exec_path;
#if defined(MODUS_OS_WIN32)
    String executable = in_executable.to_str();
    if (!StringSlice(executable).ends_with(".exe")) {
        executable += ".exe";
    }
#else
    const StringSlice executable = in_executable;
#endif
    while (true) {
        auto r_find = paths.find(path_sep, cur_pos);
        StringSlice sub_str;
        if (!r_find) {
            sub_str = paths.sub_slice(prev_pos, paths.size() - prev_pos);
        } else {
            sub_str = paths.sub_slice(prev_pos, *r_find - prev_pos);
            cur_pos = *r_find + 1;
            prev_pos = cur_pos;
        }
        exec_path.clear();
        os::Path::join_inplace(exec_path, sub_str, executable);
        auto r_path_info = os::Path::path_info(exec_path);
        if (r_path_info && r_path_info->is_file() && r_path_info->is_executable()) {
#if defined(MODUS_OS_WIN32)
            for (auto& ch : exec_path) {
                if (ch == '\\') {
                    ch = '/';
                }
            }
#endif
            return Ok(std::move(exec_path));
        }
        if (!r_find) {
            break;
        };
    }
    return Error<>();
}

}    // namespace modus::os
