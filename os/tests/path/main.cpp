/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <os/os_pch.h>
#include <iostream>

#include <os/env.hpp>
#include <os/file.hpp>
#include <os/path.hpp>

#include <catch2/catch.hpp>

using namespace modus;
using namespace modus::os;

TEST_CASE("Find executable", "[Path]") {
#if defined(MODUS_OS_UNIX)
    auto r_find = os::find_executable("ls");
    REQUIRE(r_find);
    REQUIRE((*r_find == "/usr/bin/ls" || *r_find == "/bin/ls"));
#else
    auto r_find = os::find_executable("ping.exe");
    REQUIRE(r_find);
    REQUIRE(*r_find == "C:/Windows/system32/ping.exe");
#endif
}

TEST_CASE("Find non_existent executable", "[Path]") {
    auto r_find = os::find_executable("YIBBERISH_I_DO_NOT_EXIST");
    REQUIRE_FALSE(r_find);
}

TEST_CASE("Path string queries", "[Path]") {
#if defined(MODUS_OS_WIN32)
    StringSlice path = "C:/this/fancy/file.txt";
    const StringSlice expected_no_ext = "C:/this/fancy/file";
    const StringSlice expected_path = "C:/this/fancy";
#else
    StringSlice path = "/this/fancy/file.txt";
    const StringSlice expected_no_ext = "/this/fancy/file";
    const StringSlice expected_path = "/this/fancy";
#endif

    REQUIRE(Path::is_absolute(path));
    REQUIRE(Path::get_extension(path) == "txt");
    REQUIRE(Path::get_basename(path) == "file.txt");
    REQUIRE(Path::get_path(path) == expected_path);
    REQUIRE(Path::remove_extension(path) == expected_no_ext);

    String join_path;
    Path::join_inplace(join_path, "/foo");
    REQUIRE(join_path == "/foo");
    Path::join_inplace(join_path, "/foo/", "bar");
    REQUIRE(join_path == "/foo/foo/bar");
}

TEST_CASE("Dir queries", "[Path]") {
    String file_path;
    Path::join_inplace(file_path, MODUS_TEST_SOURCE_DIR, "test_data/dir/file2.txt");
    String dir_path;
    Path::join_inplace(dir_path, MODUS_TEST_SOURCE_DIR, "test_data/dir");

    PathResult<PathInfo> result_file = Path::path_info(file_path);
    PathResult<PathInfo> result_dir = Path::path_info(dir_path);
    REQUIRE(result_file.is_value());
    REQUIRE(result_dir.is_value());
    REQUIRE(result_file.value().is_file());
    REQUIRE(result_dir.value().is_directory());
}

static const StringSlice k_tmp_dir_name = "modus_path_test";

TEST_CASE("Make directory", "[Path]") {
    auto tmp_dir_result = Path::os_temp_path();
    REQUIRE(tmp_dir_result);
    const String tmp_dir = tmp_dir_result.release_value();
    String test_dir = Path::join(tmp_dir, k_tmp_dir_name);
    REQUIRE(Path::make_directory(test_dir));

    SECTION("Make simple directory") {
        String dir_path = Path::join(test_dir, "foo");
        auto result = Path::make_directory(dir_path);
        REQUIRE(result);

        REQUIRE(Path::is_directory(dir_path));

        // make directory for the same path should not fail
        result = Path::make_directory(dir_path);
        REQUIRE(result);
    }

    SECTION("Move directory") {
        String dir_path = Path::join(test_dir, "foo");
        String new_dir_path;
        auto result = Path::make_directory(dir_path);
        REQUIRE(result);
        Path::join_inplace(new_dir_path, test_dir, "bar");
        result = Path::rename(dir_path, new_dir_path);
        REQUIRE(result);

        REQUIRE(Path::is_directory(new_dir_path));
        REQUIRE_FALSE(Path::exists(dir_path));
    }

    SECTION("Write & Read from file") {
        StringSlice string_in = "Hello World!!";
        String file_path;
        Path::join_inplace(file_path, test_dir, "test.txt");

        // Create file
        {
            FileBuilder builder;
            auto open_result = builder.write().read().create().binary().open(file_path);
            if (!open_result) {
                const StringSlice err_str = path_error_to_str(open_result.error());
                std::cout << fmt::format("Failed to open file: {}", err_str) << std::endl;
            }
            REQUIRE(open_result);

            File f = open_result.release_value();

            auto write_result = f.write(string_in.as_bytes());
            REQUIRE(write_result);
            REQUIRE(write_result.value() == string_in.size());
            {
                auto pos_result = f.position();
                REQUIRE(pos_result);
                REQUIRE(pos_result.value() == string_in.size());
            }
            REQUIRE(f.size() == string_in.size());

            {
                auto seek_result = f.seek(0);
                REQUIRE(seek_result);
                REQUIRE(seek_result.value() == 0);
                auto pos_result = f.position();
                REQUIRE(pos_result);
                REQUIRE(pos_result.value() == 0);
            }

            char buffer[32];
            SliceMut<char> buffer_slice(buffer);
            auto read_result = f.read(buffer_slice.as_bytes());
            REQUIRE(read_result);
            REQUIRE(read_result.value() == string_in.size());
            auto sub_slice = buffer_slice.sub_slice(string_in.size());
            REQUIRE(sub_slice.as_bytes() == string_in.as_bytes());

            read_result = f.read(buffer_slice.as_bytes());
            REQUIRE(read_result);
            REQUIRE(read_result.value() == 0);
            f.flush();
        }

        // Open in append mode
        {
            FileBuilder builder;
            auto open_result = builder.read().append().binary().open(file_path);
            if (!open_result) {
                const StringSlice err_str = path_error_to_str(open_result.error());
                std::cout << fmt::format("Failed to open file: {}", err_str) << std::endl;
            }
            REQUIRE(open_result);
            File f = open_result.release_value();
            auto pos_result = f.position();
            REQUIRE(pos_result);
            // Append will only switch position on the first write
            REQUIRE((pos_result.value() == string_in.size() || pos_result.value() == 0));
            REQUIRE(f.size() == string_in.size());
        }

        // Truncate file
        {
            FileBuilder builder;
            auto open_result = builder.write().read().binary().open(file_path);
            if (!open_result) {
                const StringSlice err_str = path_error_to_str(open_result.error());
                std::cout << fmt::format("Failed to open file: {}", err_str) << std::endl;
            }
            REQUIRE(open_result);
            File f = open_result.release_value();
            auto pos_result = f.position();
            REQUIRE(pos_result);
            REQUIRE(pos_result.value() == 0);
            REQUIRE(f.size() == 0);
        }
    }

    SECTION("List directory") {
        String directory_path;
        Path::join_inplace(directory_path, test_dir, "dir");

        REQUIRE(Path::make_directory(directory_path));
        String file_path;
        Path::join_inplace(file_path, directory_path, "foo.txt");

        {
            auto r_file = FileBuilder().create().write().open(file_path);
            REQUIRE(r_file);
        }

        Path::DefaultListDirectoryCallback callback;
        auto r_list = Path::list_directory(callback, test_dir, true, true);
        if (!r_list) {
            const StringSlice err_str = path_error_to_str(r_list.error());
            std::cout << fmt::format("Failed to list_path: {}", err_str) << std::endl;
        }
        REQUIRE(r_list);
        REQUIRE(callback.m_paths.size() == 2);
        REQUIRE(callback.m_paths[0] == directory_path);
        REQUIRE(callback.m_paths[1] == file_path);
    }

    auto remove_result = Path::remove(test_dir);
    REQUIRE(remove_result);
    REQUIRE_FALSE(Path::exists(test_dir));
}
