/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
#include <os/os_pch.h>
#include <iostream>

#include <catch2/catch.hpp>
#include <os/shared_library.hpp>

using namespace modus;
using namespace modus::os;

// These tests are only here to test compilation errors

#if defined(MODUS_OS_UNIX)
static constexpr const char* kLibPath = "./libtest_module.so";
#else
static constexpr const char* kLibPath = ".\\test_module.dll";
#endif

using fn_type = int (*)();

TEST_CASE("Module load", "[Shared Library]") {
    SharedLibrary lib;

    auto r = lib.open(kLibPath);
    if (!r) {
        std::cerr << fmt::format("Failed to open '{}': {}\n", kLibPath, r.error());
    }
    REQUIRE(r);

    auto r_sym = lib.resolve("do_foo");
    REQUIRE(r_sym);

    fn_type fn = reinterpret_cast<fn_type>(*r_sym);
    REQUIRE(fn() == 1024);

    REQUIRE_FALSE(lib.resolve("do_bar"));

    REQUIRE(lib.close());
}
