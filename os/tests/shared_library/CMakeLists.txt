#
# Lock tests
#


modus_add_test(test_shared_library NAME SharedLibrary)

target_sources(test_shared_library PRIVATE
    test_shared_library.cpp
)

target_link_libraries(test_shared_library PRIVATE os)

add_library(test_module MODULE module.cpp)
modus_apply_common_target_flags(test_module)

get_property(is_multi_config GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
if (is_multi_config)
    set_target_properties(test_module PROPERTIES
        LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests
    )
else()
    set_target_properties(test_module PROPERTIES
        LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests
    )
endif()

add_dependencies(test_shared_library test_module)
