/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
#include <os/os_pch.h>
#include <iostream>

#include <catch2/catch.hpp>
#include <os/locks.hpp>
#include <os/thread.hpp>
#include <os/time.hpp>

using namespace modus;
using namespace modus::os;

// These tests are only here to test compilation errors

struct Foo {
    int f;
};

TEST_CASE("Test Mutex", "[Locks]") {
    Mutex<Foo> mutex(Foo{30});
    {
        auto accessor = mutex.lock();
        REQUIRE(accessor->f == 30);
    }
}

TEST_CASE("Test RWLock", "[Locks]") {
    RWLock<Foo> rwlock(Foo{30});
    {
        auto accessor = rwlock.lock_read();
        REQUIRE(accessor->f == 30);
    }
    {
        auto accessor = rwlock.lock_write();
        REQUIRE(accessor->f == 30);
    }
}

class TestThread final : public Thread {
   public:
    Condition condition;

    void run() override {
        os::sleep_sec(ISeconds(1));
        const char* thread_name = "test";
        auto result = Thread::set_current_thread_name(thread_name);
        REQUIRE(result);
        condition.signal_one();
    }
};

TEST_CASE("Test Condition", "[Locks]") {
    Mutex<int> mutex(30);
    TestThread thread;
    {
        auto accessor = mutex.lock();
        auto result = thread.spawn();
        REQUIRE(result);
        thread.condition.wait<decltype(mutex)>(accessor);
    }
    thread.join();
}

class TestThreadPriority final : public Thread {
    void run() override {
        Thread::Priority m_priorities[] = {Thread::Priority::RealTime, Thread::Priority::VeryHigh,
                                           Thread::Priority::High,     Thread::Priority::Low,
                                           Thread::Priority::VeryLow,  Thread::Priority::Default};

        for (auto& priority : m_priorities) {
            auto result = Thread::set_current_thread_priority(priority);
            REQUIRE(result);
        }
    }
};

TEST_CASE("Test Thread Priority", "[Thread]") {
    TestThreadPriority thread;
    auto result = thread.spawn();
    REQUIRE(result);
    thread.join();
}
