/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <bullet_collision_shape.hpp>
#include <bullet_rigid_body.hpp>
#include <physics/instance.hpp>
namespace modus::physics {

class BulletInstance final : public Instance {
   private:
    BulletCollisionShapeDB m_shape_db;
    BulletRigidBodyDB m_rigid_body_db;

   public:
    BulletInstance();

    std::unique_ptr<PhysicsWorld> create_world() override;
    Result<> initialize() override;
    Result<> shutdown() override;
    Result<CollisionShapeHandle, void> create_collision_shape_sphere(const f32 radius) override;
    Result<CollisionShapeHandle, void> create_collision_shape_box(const glm::vec3& radius) override;
    Result<CollisionShapeHandle, void> create_collision_shape_plane(const glm::vec3& direction,
                                                                    const f32 offset) override;
    Result<CollisionShapeHandle, void> create_collision_shape_convex_triangle_mesh(
        const TriangleMeshShapeCreateParams& params) override;
    Result<CollisionShapeHandle, void> create_collision_shape_static_triangle_mesh(
        const TriangleMeshShapeCreateParams& params) override;
    Result<CollisionShapeHandle, void> create_collision_shape_convex_hull(
        const Slice<f32> points,
        const size_t num_points,
        const size_t stride_bytes,
        const glm::vec3& scale) override;
    Result<CollisionShapeHandle, void> create_collision_shape_convex_hull(
        const TriangleMeshShapeCreateParams& params) override;
    Result<> destroy_collision_shape(const CollisionShapeHandle handle) override;

    Result<RigidBodyHandle, void> create_rigid_body(const RigidBodyCreateParams& params) override;
    Result<> destroy_rigid_body(const RigidBodyHandle handle) override;
    Result<math::Transform, void> rigid_body_transform(const RigidBodyHandle handle) const override;
    Result<glm::vec3> rigid_body_linear_velocity(const RigidBodyHandle handle) const override;
    Result<> rigid_body_apply_force(const RigidBodyHandle handle,
                                    const glm::vec3& force,
                                    const glm::vec3& relative_position) override;
    Result<> rigid_body_apply_relative_force(const RigidBodyHandle handle,
                                             const glm::vec3& force,
                                             const glm::vec3& relative_position) override;
    Result<> rigid_body_apply_impulse(const RigidBodyHandle handle,
                                      const glm::vec3& force,
                                      const glm::vec3& relative_position) override;
    Result<> rigid_body_apply_torque_impulse(const RigidBodyHandle handle,
                                             const glm::vec3& torque) override;
    Result<> rigid_body_apply_relative_impulse(const RigidBodyHandle handle,
                                               const glm::vec3& force,
                                               const glm::vec3& relative_position) override;
    Result<> rigid_body_apply_torque(const RigidBodyHandle handle, const glm::vec3& force) override;
    Result<> rigid_body_apply_relative_torque(const RigidBodyHandle handle,
                                              const glm::vec3& force) override;
    Result<> rigid_body_set_linear_velocity(const RigidBodyHandle handle,
                                            const glm::vec3& velocity) override;
    Result<> rigid_body_set_angular_velocity(const RigidBodyHandle handle,
                                             const glm::vec3& velocity) override;
    Result<> rigid_body_set_transform(const RigidBodyHandle handle,
                                      const math::Transform& transform) override;
    Result<RigidBodyState, void> rigid_body_state(const RigidBodyHandle handle) override;
    Result<math::bv::AABB, void> rigid_body_aabb(const RigidBodyHandle handle) const override;
    Result<f32> rigid_body_angular_damping(const RigidBodyHandle handle) const override;
    Result<f32> rigid_body_linear_damping(const RigidBodyHandle handle) const override;
    Result<glm::vec3> rigid_body_angular_factor(const RigidBodyHandle handle) const override;
    Result<glm::vec3> rigid_body_linear_factor(const RigidBodyHandle handle) const override;
    Result<> set_rigid_body_angular_factor(const RigidBodyHandle handle,
                                           const glm::vec3& factor) override;
    Result<> set_rigid_body_linear_factor(const RigidBodyHandle handle,
                                          const glm::vec3& factor) override;
};

}    // namespace modus::physics
