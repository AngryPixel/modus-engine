/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/handle_pool.hpp>
#include <physics/world.hpp>

namespace modus::physics {
class BulletRigidBodyDB;
struct WorldCreateParams;

struct CollisionPair {
    struct {
        RigidBodyHandle handle;
        NotMyPtr<void> user_data;
    } object1;
    struct {
        RigidBodyHandle handle;
        NotMyPtr<void> user_data;
    } object2;

    bool operator==(const CollisionPair& rhs) const {
        return object1.handle == rhs.object1.handle && object2.handle == rhs.object2.handle;
    }

    bool operator!=(const CollisionPair& rhs) const {
        return !(object1.handle == rhs.object1.handle && object2.handle == rhs.object2.handle);
    }

    bool operator<(const CollisionPair& rhs) const {
        if (object1.handle == rhs.object1.handle) {
            return object2.handle < rhs.object2.handle;
        }
        return object1.handle < rhs.object1.handle;
    }
};

class BulletWorld final : public PhysicsWorld {
   private:
    BulletRigidBodyDB& m_rigid_body_db;
    std::unique_ptr<btDefaultCollisionConfiguration> m_collision_config;
    std::unique_ptr<btCollisionDispatcher> m_collision_dispatcher;
    std::unique_ptr<btBroadphaseInterface> m_broad_phase_interface;
    std::unique_ptr<btSequentialImpulseConstraintSolver> m_constraint_solver;
    std::unique_ptr<btDynamicsWorld> m_world;
    physics::Vector<CollisionPair> m_collisions;
    physics::Vector<CollisionPair> m_prev_collisions;
    physics::Vector<CollisionPair> m_removed_pairs;
    CollisionBeginDelegate m_callback_begin;
    CollisionEndDelegate m_callback_end;
    Vector<glm::vec3> m_contact_points_vec;

   private:
    static void tick_callback(btDynamicsWorld* const world, const btScalar time_step);

   public:
    BulletWorld(BulletRigidBodyDB& rigid_body_db);

    Result<> initialize(const WorldCreateParams& parmas) override;

    Result<> shutdown() override;

    void set_gravity(const glm::vec3& gravity) override;

    glm::vec3 gravity() const override;

    void update(const f32 tick_seconds) override;

    Result<> add_rigid_body(const RigidBodyHandle handle) override;

    Result<> remove_rigid_body(const RigidBodyHandle handle) override;

    void set_collision_callbacks(CollisionBeginDelegate on_collision_begin,
                                 CollisionEndDelegate on_collision_end) override;

    void debug_draw(IDebugDrawer& drawer) override;
};
}    // namespace modus::physics
