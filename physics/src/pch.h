/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
// clang-format off
#if (MODUS_OS_WIN32)
#define BT_USE_SSE_IN_API 1
#endif
// clang-format on
#include <bullet/btBulletDynamicsCommon.h>
#include <log/log_pch.h>
#include <physics/physics_pch.h>

#include <log/log.hpp>
#include <profiler/profiler.hpp>
