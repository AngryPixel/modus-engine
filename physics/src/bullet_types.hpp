/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <math/transform.hpp>
#include <physics/physics.hpp>

namespace modus::physics {

inline btVector3 make_btvec3(const glm::vec3& v) {
    return btVector3(v.x, v.y, v.z);
}

inline glm::vec3 make_vec3(const btVector3& v) {
    return glm::vec3(v.x(), v.y(), v.z());
}

inline btQuaternion make_btquat(const glm::quat& q) {
    return btQuaternion(q.x, q.y, q.z, q.w);
}

inline glm::quat make_quat(const btQuaternion& q) {
    return glm::quat(q.w(), q.x(), q.y(), q.z());
}

inline btTransform make_bttransform(const math::Transform& t) {
    btTransform bt;
    bt.setOrigin(make_btvec3(t.m_position));
    bt.setRotation(make_btquat(t.m_rotation));
    return bt;
}

inline math::Transform make_transform(const btTransform bt) {
    math::Transform t;
    t.set_translation(make_vec3(bt.getOrigin()));
    t.set_rotation(make_quat(bt.getRotation()));
    return t;
}

}    // namespace modus::physics
