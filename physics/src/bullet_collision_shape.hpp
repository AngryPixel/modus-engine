/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/allocator/pool_allocator.hpp>
#include <core/handle_pool.hpp>
#include <physics/collision_shape.hpp>

namespace modus::physics {

class CollisionShapeBase {
   public:
    virtual ~CollisionShapeBase() = default;
    virtual NotMyPtr<btCollisionShape> collision_shape() = 0;
};

class BulletCollisionShapeData {
   public:
    NotMyPtr<CollisionShapeBase> m_shape;
    u32 m_ref_count = 1;
    CollisionShapeType m_type;

    BulletCollisionShapeData(NotMyPtr<CollisionShapeBase> shape, const CollisionShapeType type)
        : m_shape(shape), m_type(type) {}

    virtual ~BulletCollisionShapeData() = default;
};
class MeshCollisionShape;
class StaticMeshCollisionShape;
class SphereCollisionShape;
class BoxCollisionShape;
class PlaneCollisionShape;
class ConvexHullCollisionShape;

class MODUS_PHYSICS_EXPORT BulletCollisionShapeDB final {
   public:
    using Type = BulletCollisionShapeData;

   private:
    using PoolType = HandlePool<Type, PhysicsAllocator, CollisionShapeHandle>;
    PoolType m_shapes;
    PoolAllocatorTyped<MeshCollisionShape, PhysicsAllocator> m_mesh_shapes;
    PoolAllocatorTyped<StaticMeshCollisionShape, PhysicsAllocator> m_static_mesh_shapes;
    PoolAllocatorTyped<ConvexHullCollisionShape, PhysicsAllocator> m_convex_hull_shapes;
    PoolAllocatorTyped<SphereCollisionShape, PhysicsAllocator> m_sphere_shapes;
    PoolAllocatorTyped<BoxCollisionShape, PhysicsAllocator> m_box_shapes;
    PoolAllocatorTyped<PlaneCollisionShape, PhysicsAllocator> m_plane_shapes;
    PoolAllocatorTyped<btTriangleIndexVertexArray, PhysicsAllocator> m_mesh_descriptions;

   public:
    BulletCollisionShapeDB();

    ~BulletCollisionShapeDB();

    Result<CollisionShapeHandle, void> create_sphere(const f32 radius);

    Result<CollisionShapeHandle, void> create_box(const glm::vec3& radius);

    Result<CollisionShapeHandle, void> create_plane(const glm::vec3& direction, const f32 offset);

    Result<CollisionShapeHandle, void> create_convex_triangle_mesh(
        const TriangleMeshShapeCreateParams& params);

    Result<CollisionShapeHandle, void> create_static_triangle_mesh(
        const TriangleMeshShapeCreateParams& params);

    Result<CollisionShapeHandle, void> create_convex_hull(const Slice<f32> points,
                                                          const size_t num_points,
                                                          const size_t stride_bytes,
                                                          const glm::vec3& scale);

    Result<CollisionShapeHandle, void> create_convex_hull(
        const TriangleMeshShapeCreateParams& params);

    Result<> destroy(const CollisionShapeHandle);

    Result<NotMyPtr<Type>, void> get(const CollisionShapeHandle handle) {
        return m_shapes.get(handle);
    }

    Result<NotMyPtr<btCollisionShape>, void> add_ref(const CollisionShapeHandle handle);

    void rem_ref(const CollisionShapeHandle handle);
};

}    // namespace modus::physics
