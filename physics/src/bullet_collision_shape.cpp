/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <bullet_collision_shape.hpp>
#include <bullet_types.hpp>
namespace modus::physics {

class SphereCollisionShape final : public CollisionShapeBase {
   public:
    btSphereShape m_shape;
    static constexpr CollisionShapeType kType = CollisionShapeType::Sphere;

   public:
    SphereCollisionShape(const f32 radius) : m_shape(radius) {}
    NotMyPtr<btCollisionShape> collision_shape() override { return &m_shape; }
};

class BoxCollisionShape final : public CollisionShapeBase {
   public:
    btBoxShape m_shape;
    static constexpr CollisionShapeType kType = CollisionShapeType::Box;

   public:
    BoxCollisionShape(const btVector3& extent) : m_shape(extent) {}
    NotMyPtr<btCollisionShape> collision_shape() override { return &m_shape; }
};

class PlaneCollisionShape final : public CollisionShapeBase {
   public:
    btStaticPlaneShape m_shape;
    static constexpr CollisionShapeType kType = CollisionShapeType::Plane;

   public:
    PlaneCollisionShape(const btVector3& direction, const f32 offset)
        : m_shape(direction, offset) {}
    NotMyPtr<btCollisionShape> collision_shape() override { return &m_shape; }
};

class MeshCollisionShape final : public CollisionShapeBase {
   public:
    NotMyPtr<btTriangleIndexVertexArray> m_mesh_data;
    btConvexTriangleMeshShape m_shape;
    static constexpr CollisionShapeType kType = CollisionShapeType::TriangleMesh;

   public:
    MeshCollisionShape(NotMyPtr<btTriangleIndexVertexArray> mesh_data)
        : m_mesh_data(mesh_data), m_shape(mesh_data.get()) {}
    NotMyPtr<btCollisionShape> collision_shape() override { return &m_shape; }
};

class StaticMeshCollisionShape final : public CollisionShapeBase {
   public:
    NotMyPtr<btTriangleIndexVertexArray> m_mesh_data;
    btBvhTriangleMeshShape m_shape;
    static constexpr CollisionShapeType kType = CollisionShapeType::StaticTriangleMesh;

   public:
    StaticMeshCollisionShape(NotMyPtr<btTriangleIndexVertexArray> mesh_data)
        : m_mesh_data(mesh_data), m_shape(mesh_data.get(), true) {}
    NotMyPtr<btCollisionShape> collision_shape() override { return &m_shape; }
};

class ConvexHullCollisionShape final : public CollisionShapeBase {
   public:
    btConvexHullShape m_shape;
    static constexpr CollisionShapeType kType = CollisionShapeType::ConvexHull;

   public:
    ConvexHullCollisionShape(const btScalar* points = 0,
                             const int numPoints = 0,
                             const int stride = sizeof(btVector3))
        : m_shape(points, numPoints, stride) {}
    NotMyPtr<btCollisionShape> collision_shape() override { return &m_shape; }
};

PHY_ScalarType index_type_to_bt_type(const TriangleMeshShapeCreateParams::IndexType type) {
    switch (type) {
        case TriangleMeshShapeCreateParams::IndexType::U8:
            return PHY_ScalarType::PHY_UCHAR;
        case TriangleMeshShapeCreateParams::IndexType::U16:
            // NOTE: This will come back to haunt me
            return PHY_ScalarType::PHY_SHORT;
        case TriangleMeshShapeCreateParams::IndexType::U32:
            // NOTE: This will come back to haunt me
            return PHY_ScalarType::PHY_INTEGER;
        default:
            modus_assert_message(false, "Should not be reached\n");
            return PHY_ScalarType::PHY_UCHAR;
    }
}

static constexpr usize kItemPerBlockCount = 32;

BulletCollisionShapeDB::BulletCollisionShapeDB()
    : m_shapes(kItemPerBlockCount),
      m_mesh_shapes(kItemPerBlockCount),
      m_static_mesh_shapes(kItemPerBlockCount),
      m_convex_hull_shapes(kItemPerBlockCount),
      m_sphere_shapes(kItemPerBlockCount),
      m_box_shapes(kItemPerBlockCount),
      m_plane_shapes(kItemPerBlockCount),
      m_mesh_descriptions(kItemPerBlockCount) {}

BulletCollisionShapeDB::~BulletCollisionShapeDB() {
#if defined(MODUS_ENABLE_ASSERTS)
    m_shapes.for_each([](const Type& shape) -> void {
        modus_assert_message(shape.m_ref_count == 1,
                             "This shape is still in use somewhere, expect a crash!");
    });
#endif
}

Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_sphere(const f32 radius) {
    SphereCollisionShape* shape = m_sphere_shapes.construct(radius);
    if (shape == nullptr) {
        return Error<>();
    }
    auto r_add = m_shapes.create(BulletCollisionShapeData(shape, SphereCollisionShape::kType));
    if (!r_add) {
        m_sphere_shapes.destroy(shape);
        return Error<>();
    }
    return Ok(r_add->first);
}

Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_box(const glm::vec3& radius) {
    BoxCollisionShape* shape = m_box_shapes.construct(make_btvec3(radius));
    if (shape == nullptr) {
        return Error<>();
    }
    auto r_add = m_shapes.create(BulletCollisionShapeData(shape, BoxCollisionShape::kType));
    if (!r_add) {
        m_box_shapes.destroy(shape);
        return Error<>();
    }
    return Ok(r_add->first);
}

Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_plane(const glm::vec3& direction,
                                                                        const f32 offset) {
    PlaneCollisionShape* shape = m_plane_shapes.construct(make_btvec3(direction), offset);
    if (shape == nullptr) {
        return Error<>();
    }
    auto r_add = m_shapes.create(BulletCollisionShapeData(shape, PlaneCollisionShape::kType));
    if (!r_add) {
        m_plane_shapes.destroy(shape);
        return Error<>();
    }
    return Ok(r_add->first);
}

Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_convex_triangle_mesh(
    const TriangleMeshShapeCreateParams& params) {
    // Must be divisible by three;
    if (params.vertices.is_empty() || params.triangle_count == 0) {
        return Error<>();
    }

    NotMyPtr<btTriangleIndexVertexArray> vertex_data = m_mesh_descriptions.construct();
    if (!vertex_data) {
        return Error<>();
    }
    btIndexedMesh indexed_mesh;
    indexed_mesh.m_numTriangles = params.triangle_count;
    if (params.index_type != TriangleMeshShapeCreateParams::IndexType::None &&
        !params.indices.is_empty()) {
        indexed_mesh.m_indexType = index_type_to_bt_type(params.index_type);
        indexed_mesh.m_triangleIndexStride = params.indices_stride;
        indexed_mesh.m_triangleIndexBase = params.indices.data();
    }
    indexed_mesh.m_vertexType = PHY_ScalarType::PHY_FLOAT;
    indexed_mesh.m_vertexBase = params.vertices.as_bytes().data();
    indexed_mesh.m_numVertices = params.num_vertices;
    indexed_mesh.m_vertexStride = params.vertices_stride;

    vertex_data->addIndexedMesh(indexed_mesh);
    MeshCollisionShape* mesh_shape = m_mesh_shapes.construct(vertex_data);
    if (mesh_shape == nullptr) {
        m_mesh_descriptions.destroy(vertex_data.get());
        return Error<>();
    }

    mesh_shape->m_shape.setLocalScaling(make_btvec3(params.scale));
    auto r_add = m_shapes.create(BulletCollisionShapeData(mesh_shape, MeshCollisionShape::kType));
    if (!r_add) {
        m_mesh_descriptions.destroy(vertex_data.get());
        m_mesh_shapes.destroy(mesh_shape);
        return Error<>();
    }
    return Ok(r_add->first);
}
Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_static_triangle_mesh(
    const TriangleMeshShapeCreateParams& params) {
    // Must be divisible by three;
    if (params.vertices.is_empty() || params.triangle_count == 0) {
        return Error<>();
    }

    NotMyPtr<btTriangleIndexVertexArray> vertex_data = m_mesh_descriptions.construct();
    if (!vertex_data) {
        return Error<>();
    }
    btIndexedMesh indexed_mesh;
    indexed_mesh.m_numTriangles = params.triangle_count;
    if (params.index_type != TriangleMeshShapeCreateParams::IndexType::None &&
        !params.indices.is_empty()) {
        indexed_mesh.m_indexType = index_type_to_bt_type(params.index_type);
        indexed_mesh.m_triangleIndexStride = params.indices_stride;
        indexed_mesh.m_triangleIndexBase = params.indices.data();
    }
    indexed_mesh.m_vertexType = PHY_ScalarType::PHY_FLOAT;
    indexed_mesh.m_vertexBase = params.vertices.as_bytes().data();
    indexed_mesh.m_numVertices = params.num_vertices;
    indexed_mesh.m_vertexStride = params.vertices_stride;

    vertex_data->addIndexedMesh(indexed_mesh);
    StaticMeshCollisionShape* mesh_shape = m_static_mesh_shapes.construct(vertex_data);
    if (mesh_shape == nullptr) {
        m_mesh_descriptions.destroy(vertex_data.get());
        return Error<>();
    }

    mesh_shape->m_shape.setLocalScaling(make_btvec3(params.scale));
    auto r_add =
        m_shapes.create(BulletCollisionShapeData(mesh_shape, StaticMeshCollisionShape::kType));
    if (!r_add) {
        m_mesh_descriptions.destroy(vertex_data.get());
        m_static_mesh_shapes.destroy(mesh_shape);
        return Error<>();
    }
    return Ok(r_add->first);
}

Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_convex_hull(
    const Slice<f32> points,
    const size_t num_points,
    const size_t stride_bytes,
    const glm::vec3& scale) {
    // Must be divisible by three;
    if (points.is_empty() || num_points == 0) {
        return Error<>();
    }
    ConvexHullCollisionShape* mesh_shape = m_convex_hull_shapes.construct(
        points.data(), static_cast<int>(num_points), static_cast<int>(stride_bytes));
    if (mesh_shape == nullptr) {
        return Error<>();
    }
    mesh_shape->m_shape.setLocalScaling(make_btvec3(scale));
    auto r_add =
        m_shapes.create(BulletCollisionShapeData(mesh_shape, ConvexHullCollisionShape::kType));
    if (!r_add) {
        m_convex_hull_shapes.destroy(mesh_shape);
        return Error<>();
    }
    return Ok(r_add->first);
}

Result<CollisionShapeHandle, void> BulletCollisionShapeDB::create_convex_hull(
    const TriangleMeshShapeCreateParams& params) {
    // Must be divisible by three;
    if (params.vertices.is_empty() || params.num_vertices == 0) {
        return Error<>();
    }
    ConvexHullCollisionShape* mesh_shape = m_convex_hull_shapes.construct(
        params.vertices.data(), params.num_vertices, params.vertices_stride);
    if (mesh_shape == nullptr) {
        return Error<>();
    }
    mesh_shape->m_shape.setLocalScaling(make_btvec3(params.scale));
    auto r_add =
        m_shapes.create(BulletCollisionShapeData(mesh_shape, ConvexHullCollisionShape::kType));
    if (!r_add) {
        m_convex_hull_shapes.destroy(mesh_shape);
        return Error<>();
    }
    return Ok(r_add->first);
}

Result<> BulletCollisionShapeDB::destroy(const CollisionShapeHandle handle) {
    auto r_get = m_shapes.get(handle);
    if (!r_get) {
        return Ok<>();
    }
    modus_assert_message((*r_get)->m_ref_count == 1,
                         "Attempting to delete shape that is still in use!");
    if ((*r_get)->m_ref_count != 1) {
        return Error<>();
    }
    NotMyPtr<CollisionShapeBase> shape = (*r_get)->m_shape;
    switch ((*r_get)->m_type) {
        case CollisionShapeType::Sphere:
            shape->~CollisionShapeBase();
            m_sphere_shapes.deallocate(shape.get());
            break;
        case CollisionShapeType::Box:
            shape->~CollisionShapeBase();
            m_box_shapes.deallocate(shape.get());
            break;
        case CollisionShapeType::Plane:
            shape->~CollisionShapeBase();
            m_plane_shapes.deallocate(shape.get());
            break;
        case CollisionShapeType::TriangleMesh: {
            MeshCollisionShape* mesh_shape = static_cast<MeshCollisionShape*>(shape.get());
            NotMyPtr<btTriangleIndexVertexArray> mesh_desc = mesh_shape->m_mesh_data;
            m_mesh_shapes.destroy(mesh_shape);
            m_mesh_descriptions.destroy(mesh_desc.get());
            break;
        }
        case CollisionShapeType::StaticTriangleMesh: {
            StaticMeshCollisionShape* mesh_shape =
                static_cast<StaticMeshCollisionShape*>(shape.get());
            NotMyPtr<btTriangleIndexVertexArray> mesh_desc = mesh_shape->m_mesh_data;
            m_static_mesh_shapes.destroy(mesh_shape);
            m_mesh_descriptions.destroy(mesh_desc.get());
            break;
        }
        case CollisionShapeType::ConvexHull:
            shape->~CollisionShapeBase();
            m_convex_hull_shapes.deallocate(shape.get());
            break;

        default:
            modus_assert_message(false, "Unknown type");
            break;
    }
    return m_shapes.erase(handle);
}

Result<NotMyPtr<btCollisionShape>, void> BulletCollisionShapeDB::add_ref(
    const CollisionShapeHandle handle) {
    auto r_get = m_shapes.get(handle);
    if (!r_get) {
        return Error<>();
    }

    // Increment ref
    (*r_get)->m_ref_count++;
    return Ok((*r_get)->m_shape->collision_shape());
}

void modus::physics::BulletCollisionShapeDB::rem_ref(const CollisionShapeHandle handle) {
    auto r_get = m_shapes.get(handle);
    if (!r_get) {
        return;
    }

    // Increment ref
    (*r_get)->m_ref_count--;
    modus_assert_message((*r_get)->m_ref_count >= 1, "The ref count can't go lower than 1");
}

}    // namespace modus::physics
