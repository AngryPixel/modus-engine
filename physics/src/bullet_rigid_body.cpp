/*
 * Copyright 2020-2021 by Leander Beernaert

 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <bullet_rigid_body.hpp>
// clang-format on

#include <bullet_collision_shape.hpp>
#include <bullet_types.hpp>

namespace modus::physics {
static constexpr u32 kRigidBodiesPerChunk = 32;

BulletRigidBodyDB::BulletRigidBodyDB(BulletCollisionShapeDB& shape_db)
    : m_shape_db(shape_db),
      m_rigid_bodies(kRigidBodiesPerChunk),
      m_rigid_bodies_pool(kRigidBodiesPerChunk) {}

void BulletRigidBodyDB::shutdown() {
    MODUS_PROFILE_BLOCK("Destroy Rigid Bodies", profiler::color::Amber400);
    m_rigid_bodies.for_each([this](BulletRigidBody& object) {
        // m_world->removeRigidBody(object.rigid_body.get());
        m_shape_db.rem_ref(object.collision_shape_handle);
    });
    m_rigid_bodies.clear();
}

Result<NotMyPtr<BulletRigidBody>> BulletRigidBodyDB::rigid_body(const RigidBodyHandle handle) {
    return m_rigid_bodies.get(handle);
}

Result<NotMyPtr<const BulletRigidBody>> BulletRigidBodyDB::rigid_body(
    const RigidBodyHandle handle) const {
    return m_rigid_bodies.get(handle);
}

Result<RigidBodyHandle, void> BulletRigidBodyDB::create_rigid_body(
    const RigidBodyCreateParams& params) {
    auto r_shape = m_shape_db.add_ref(params.shape_handle);
    if (!r_shape) {
        return Error<>();
    }

    BulletRigidBody object;
    object.collision_shape_handle = params.shape_handle;
    object.user_data = params.user_data;
    object.shape = (*r_shape);
    if (!object.shape) {
        return Error<>();
    }

    btTransform t = make_bttransform(params.transform);

    if (!params.kinematic) {
        btScalar mass = params.mass;
        btVector3 local_inertia(0.f, 0.f, 0.f);
        object.shape->calculateLocalInertia(mass, local_inertia);

        btRigidBody::btRigidBodyConstructionInfo rb_info(mass, nullptr, object.shape.get(),
                                                         local_inertia);
        rb_info.m_friction = params.friction;
        rb_info.m_restitution = params.restitution;
        rb_info.m_angularDamping = params.angular_damping;
        rb_info.m_linearDamping = params.linear_damping;
        rb_info.m_startWorldTransform = t;
        object.rigid_body = m_rigid_bodies_pool.construct(rb_info);
        if (object.rigid_body == nullptr) {
            return Error<>();
        }

        if (params.disable_collision_response) {
            object.rigid_body->setFlags(object.rigid_body->getFlags() |
                                        btCollisionObject::CF_NO_CONTACT_RESPONSE);
        }
        object.rigid_body->setLinearFactor(make_btvec3(params.linear_factor));
        object.rigid_body->setAngularFactor(make_btvec3(params.angular_factor));

    } else {
        object.rigid_body = m_rigid_bodies_pool.construct(0.f, nullptr, object.shape.get());
        if (object.rigid_body == nullptr) {
            return Error<>();
        }
        object.rigid_body->setWorldTransform(t);
        object.rigid_body->setFlags(object.rigid_body->getFlags() |
                                    btCollisionObject::CF_KINEMATIC_OBJECT);
        object.rigid_body->setActivationState(DISABLE_DEACTIVATION);
    }

    if (params.continuous_collision_detection) {
        object.rigid_body->setCcdSweptSphereRadius(
            params.continuous_collision_detection->sphere_radius);
        object.rigid_body->setCcdMotionThreshold(
            params.continuous_collision_detection->motion_threshold);
    }

    auto r_create = m_rigid_bodies.create(std::move(object));
    if (!r_create) {
        m_rigid_bodies_pool.destroy(object.rigid_body.get());
        return Error<>();
    }
    BulletRigidBody& phys_object = *r_create.value().second;
    phys_object.handle = r_create.value().first;
    phys_object.rigid_body->setUserPointer(r_create.value().second.get());

    return Ok(r_create.value().first);
}

Result<> BulletRigidBodyDB::destroy_rigid_body(const RigidBodyHandle handle) {
    auto r_object = m_rigid_bodies.get(handle);
    if (r_object) {
        m_shape_db.rem_ref((*r_object)->collision_shape_handle);
        if (r_object.value()->rigid_body->isInWorld()) {
            (*r_object)->world->removeCollisionObject((*r_object)->rigid_body.get());
        }
    }
    return m_rigid_bodies.erase(handle);
}
Result<math::Transform, void> BulletRigidBodyDB::transform(const RigidBodyHandle handle) const {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btTransform object_transform = r_object.value()->rigid_body->getWorldTransform();
    return Ok<math::Transform>(make_transform(object_transform));
}

Result<> BulletRigidBodyDB::apply_force(const RigidBodyHandle handle,
                                        const glm::vec3& force,
                                        const glm::vec3& relative_position) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate();
    rigid_body.applyForce(make_btvec3(force), make_btvec3(relative_position));
    return Ok<>();
}

Result<> BulletRigidBodyDB::apply_relative_force(const RigidBodyHandle handle,
                                                 const glm::vec3& force,
                                                 const glm::vec3& relative_position) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    btTransform rb_transform = r_object.value()->rigid_body->getWorldTransform();
    rigid_body.activate();
    rigid_body.applyForce(rb_transform.getBasis() * make_btvec3(force),
                          make_btvec3(relative_position));
    return Ok<>();
}

Result<> BulletRigidBodyDB::apply_impulse(const RigidBodyHandle handle,
                                          const glm::vec3& force,
                                          const glm::vec3& relative_position) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate();
    rigid_body.applyImpulse(make_btvec3(force), make_btvec3(relative_position));
    return Ok<>();
}

Result<> BulletRigidBodyDB::apply_relative_impulse(const RigidBodyHandle handle,
                                                   const glm::vec3& force,
                                                   const glm::vec3& relative_position) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    btTransform rb_transform = r_object.value()->rigid_body->getWorldTransform();
    rigid_body.activate();
    rigid_body.applyImpulse(rb_transform.getBasis() * make_btvec3(force),
                            make_btvec3(relative_position));
    return Ok<>();
}

Result<> BulletRigidBodyDB::apply_torque(const RigidBodyHandle handle, const glm::vec3& torque) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate();
    rigid_body.applyTorque(make_btvec3(torque));
    return Ok<>();
}

Result<> BulletRigidBodyDB::apply_torque_impulse(const RigidBodyHandle handle,
                                                 const glm::vec3& torque) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate();
    rigid_body.applyTorqueImpulse(make_btvec3(torque));
    return Ok<>();
}

Result<> BulletRigidBodyDB::apply_relative_torque(const RigidBodyHandle handle,
                                                  const glm::vec3& torque) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    btTransform rb_transform = r_object.value()->rigid_body->getWorldTransform();
    rigid_body.activate();
    rigid_body.applyTorque(rb_transform.getBasis() * make_btvec3(torque));
    return Ok<>();
}

Result<> BulletRigidBodyDB::set_linear_velocity(const RigidBodyHandle handle,
                                                const glm::vec3& velocity) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }
    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate();
    rigid_body.setLinearVelocity(make_btvec3(velocity));
    return Ok<>();
}

Result<> BulletRigidBodyDB::set_angular_velocity(const RigidBodyHandle handle,
                                                 const glm::vec3& velocity) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }
    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate();
    rigid_body.setAngularVelocity(make_btvec3(velocity));
    return Ok<>();
}

Result<> BulletRigidBodyDB::set_transform(const RigidBodyHandle handle,
                                          const math::Transform& transform) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }
    btRigidBody& rigid_body = *r_object.value()->rigid_body;
    rigid_body.activate(true);
    btTransform bt_transform = make_bttransform(transform);
    rigid_body.setWorldTransform(bt_transform);
    // rigid_body.getMotionState()->setWorldTransform(transform);
    return Ok<>();
}

Result<RigidBodyState, void> BulletRigidBodyDB::rigid_body_state(const RigidBodyHandle handle) {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    RigidBodyState state;

    const btRigidBody& rb = *(*r_object)->rigid_body;
    btTransform object_transform = r_object.value()->rigid_body->getWorldTransform();
    state.tranform = make_transform(object_transform);
    state.linear_velocity = make_vec3(rb.getLinearVelocity());
    state.angular_velocity = make_vec3(rb.getAngularVelocity());
    state.total_force = make_vec3(rb.getTotalForce());
    state.total_torque = make_vec3(rb.getTotalTorque());
    return Ok(state);
}

Result<math::bv::AABB, void> BulletRigidBodyDB::aabb(const RigidBodyHandle handle) const {
    auto r_object = m_rigid_bodies.get(handle);
    if (!r_object) {
        return Error<>();
    }

    btVector3 min, max;
    (*r_object)->rigid_body->getAabb(min, max);
    return Ok(math::bv::make_aabb(make_vec3(min), make_vec3(max)));
}

}    // namespace modus::physics
