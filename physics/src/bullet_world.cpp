/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <bullet_world.hpp>

// clang-format on
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>
#include <bullet/LinearMath/btIDebugDraw.h>

#include <bullet_rigid_body.hpp>
#include <bullet_types.hpp>
#include <physics/debug_drawer.hpp>

namespace modus::physics {

class DebugDrawer final : public btIDebugDraw {
   public:
    IDebugDrawer& m_drawer;
    int m_debug_mode;

    DebugDrawer(IDebugDrawer& drawer) : m_drawer(drawer), m_debug_mode(0) {
        m_debug_mode |= btIDebugDraw::DBG_DrawWireframe;
        m_debug_mode |= btIDebugDraw::DBG_DrawContactPoints;
        m_debug_mode |= btIDebugDraw::DBG_DrawAabb;
        m_debug_mode |= btIDebugDraw::DBG_DrawConstraints;
        m_debug_mode |= btIDebugDraw::DBG_FastWireframe;
    }

    void drawContactPoint(const btVector3& PointOnB,
                          const btVector3& normalOnB,
                          btScalar distance,
                          int lifeTime,
                          const btVector3& color) override {
        // May be hard to see.
        // TODO: Fade over time?
        MODUS_UNUSED(lifeTime);
        btVector3 const startPoint = PointOnB;
        btVector3 const endPoint = PointOnB + normalOnB * distance;
        drawLine(startPoint, endPoint, color);
    }

    void reportErrorWarning(const char* warning) override {
        MODUS_LOGW("BulletWorld: {}", warning);
    }

    void draw3dText(const btVector3& location, const char* textString) override {
        m_drawer.draw_text3d(make_vec3(location), textString);
    }

    void drawLine(const btVector3& from, const btVector3& to, const btVector3& lineColor) override {
        m_drawer.draw_line(make_vec3(from), make_vec3(to), make_vec3(lineColor),
                           make_vec3(lineColor));
    }

    void drawLine(const btVector3& from,
                  const btVector3& to,
                  const btVector3& fromColor,
                  const btVector3& toColor) override {
        m_drawer.draw_line(make_vec3(from), make_vec3(to), make_vec3(fromColor),
                           make_vec3(toColor));
    }

    void setDebugMode(int mode) override { m_debug_mode = mode; }

    int getDebugMode() const override { return m_debug_mode; }
};

BulletWorld::BulletWorld(BulletRigidBodyDB& rigid_body_db) : m_rigid_body_db(rigid_body_db) {}

Result<> BulletWorld::initialize(const WorldCreateParams& params) {
    /// collision configuration contains default setup for memory, collision
    /// setup. Advanced users can create their own configuration.
    m_collision_config.reset(MODUS_NEW btDefaultCollisionConfiguration());

    /// use the default collision dispatcher. For parallel processing you can
    /// use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_collision_dispatcher.reset(MODUS_NEW btCollisionDispatcher(m_collision_config.get()));

    /// btDbvtBroadphase is a good general purpose broadphase. You can also try
    /// out btAxis3Sweep.
    m_broad_phase_interface.reset(MODUS_NEW btDbvtBroadphase());

    /// the default constraint solver. For parallel processing you can use a
    /// different solver (see Extras/BulletMultiThreaded)
    m_constraint_solver.reset(new btSequentialImpulseConstraintSolver());

    m_world.reset(new btDiscreteDynamicsWorld(m_collision_dispatcher.get(),
                                              m_broad_phase_interface.get(),
                                              m_constraint_solver.get(), m_collision_config.get()));

    m_world->setGravity(make_btvec3(params.gravity));

    m_world->setInternalTickCallback(tick_callback);
    m_world->setWorldUserInfo(this);

    m_callback_begin = params.callback_collision_begin;
    m_callback_end = params.callback_collision_end;

    return Ok<>();
}

Result<> BulletWorld::shutdown() {
    // Remove rigid bodies from world
    {
        const int num_collision_objects = m_world->getNumCollisionObjects();
        auto& collision_objects = m_world->getCollisionObjectArray();
        for (int i = 0; i < num_collision_objects; i++) {
            btCollisionObject* bt_object = collision_objects[i];
            if (bt_object->getInternalType() == btCollisionObject::CO_RIGID_BODY) {
                auto rigid_body = reinterpret_cast<BulletRigidBody*>(bt_object->getUserPointer());
                rigid_body->world.reset();
            }
        }
    }

    {
        MODUS_PROFILE_BLOCK("BulletWorld::Destroy", profiler::color::Amber300)
        m_world.reset();
    }

    m_constraint_solver.reset();
    m_broad_phase_interface.reset();
    m_constraint_solver.reset();
    m_collision_dispatcher.reset();
    m_collision_config.reset();
    m_callback_begin = CollisionBeginDelegate();
    m_callback_end = CollisionEndDelegate();
    return Ok<>();
}

void BulletWorld::tick_callback(btDynamicsWorld* const world, const btScalar time_step) {
    MODUS_PROFILE_BLOCK("BulletWorld::tick_callback", profiler::color::Amber200);
    MODUS_UNUSED(world);
    MODUS_UNUSED(time_step);
    modus_assert(world != nullptr && world->getWorldUserInfo() != nullptr);

    BulletWorld* const bt_world = static_cast<BulletWorld*>(world->getWorldUserInfo());
    MODUS_UNUSED(bt_world);

    // increment physics_tick
    if (!bt_world->m_callback_begin) {
        return;
    }

    // get all contacts to detect collisions which are represented as manifolds
    // in bullet
    btDispatcher* const dispatcher = world->getDispatcher();
    const int num_manifolds = dispatcher->getNumManifolds();
    bt_world->m_collisions.reserve(num_manifolds);
    for (int manifold_idx = 0; manifold_idx < num_manifolds; ++manifold_idx) {
        const btPersistentManifold* const manifold =
            dispatcher->getManifoldByIndexInternal(manifold_idx);
        modus_assert(manifold != nullptr);

        if (manifold->getNumContacts() == 0) {
            continue;
        }

        const btCollisionObject* const object1 = manifold->getBody0();
        const btCollisionObject* const object2 = manifold->getBody1();

        const bool swap = object1 < object2;

        const btCollisionObject* const object1_sorted = swap ? object2 : object1;
        const btCollisionObject* const object2_sorted = swap ? object1 : object2;

        // NOTE: the same objects can appear in multiple manifolds
        BulletRigidBody* physics_object1 =
            reinterpret_cast<BulletRigidBody*>(object1_sorted->getUserPointer());
        BulletRigidBody* physics_object2 =
            reinterpret_cast<BulletRigidBody*>(object2_sorted->getUserPointer());

        modus_assert(physics_object1 != nullptr);
        modus_assert(physics_object2 != nullptr);

        // If the tick is equal to the world tick, it means the collision for
        // this object was already handled.

        CollisionPair cp = {{physics_object1->handle, physics_object1->user_data},
                            {physics_object2->handle, physics_object2->user_data}};

        auto it =
            std::find(bt_world->m_prev_collisions.begin(), bt_world->m_prev_collisions.end(), cp);
        if (it != bt_world->m_prev_collisions.end()) {
            // already handled
            continue;
        }

        // track current collision
        bt_world->m_collisions.push_back(cp);

        CollisionBeginInfo collision_info;
        collision_info.object1.handle = physics_object1->handle;
        collision_info.object1.user_data = physics_object1->user_data;
        collision_info.object2.handle = physics_object2->handle;
        collision_info.object2.user_data = physics_object2->user_data;

        auto& contatct_point_vector = bt_world->m_contact_points_vec;
        contatct_point_vector.clear();
        contatct_point_vector.reserve(manifold->getNumContacts());
        for (int i = 0; i < manifold->getNumContacts(); ++i) {
            const btManifoldPoint& point = manifold->getContactPoint(i);
            contatct_point_vector.push_back(make_vec3(point.getPositionWorldOnB()));
            collision_info.sum_normal_force +=
                make_vec3(point.m_combinedRestitution * point.m_normalWorldOnB);
            collision_info.sum_friction_force +=
                make_vec3(point.m_combinedFriction * point.m_lateralFrictionDir1);
        }
        collision_info.contact_points = make_slice(contatct_point_vector);
        bt_world->m_callback_begin(collision_info);
    }

    // Data going into set_difference must be sorted
    std::sort(bt_world->m_collisions.begin(), bt_world->m_collisions.end());
    auto& removed_pairs = bt_world->m_removed_pairs;
    std::set_difference(bt_world->m_prev_collisions.begin(), bt_world->m_prev_collisions.end(),
                        bt_world->m_collisions.begin(), bt_world->m_collisions.end(),
                        std::inserter(removed_pairs, removed_pairs.begin()));

    if (bt_world->m_callback_end) {
        for (const auto& cp : removed_pairs) {
            CollisionEndInfo collision_info{{cp.object1.handle, cp.object1.user_data},
                                            {cp.object2.handle, cp.object2.user_data}};
            bt_world->m_callback_end(collision_info);
        }
    }

    std::swap(bt_world->m_prev_collisions, bt_world->m_collisions);
    bt_world->m_collisions.clear();
    removed_pairs.clear();
}

void BulletWorld::set_gravity(const glm::vec3& gravity) {
    m_world->setGravity(make_btvec3(gravity));
}

glm::vec3 BulletWorld::gravity() const {
    return make_vec3(m_world->getGravity());
}

void BulletWorld::update(const f32 tick_seconds) {
    MODUS_PROFILE_BLOCK("BulletWorld::update", profiler::color::Amber100);
    /*static f64 accumlator = 0.0f;
    accumlator += f64(engine.tick_sec());
    static constexpr f64 timestep = 1.0/60.0;
    while(accumlator >= timestep) {
        m_world->stepSimulation(timestep);
        accumlator -= timestep;
    }*/
    // const f32 t = accumlator/timestep;
    // lerp previous state with current state
    // Bullet takes care of this ^
    // m_world->stepSimulation(tick_seconds, 4);
    m_world->stepSimulation(tick_seconds, 0);
}

Result<> BulletWorld::add_rigid_body(const RigidBodyHandle handle) {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (!r_body) {
        return Error<>();
    }

    if ((*r_body)->world) {
        return Error<>();
    }
    (*r_body)->world = m_world.get();
    m_world->addRigidBody((*r_body)->rigid_body.get());
    return Ok<>();
}

Result<> BulletWorld::remove_rigid_body(const RigidBodyHandle handle) {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (!r_body) {
        return Error<>();
    }

    if ((*r_body)->world != m_world.get()) {
        return Error<>();
    }
    (*r_body)->world.reset();
    m_world->removeRigidBody((*r_body)->rigid_body.get());
    return Ok<>();
}

void BulletWorld::set_collision_callbacks(CollisionBeginDelegate on_collision_begin,
                                          CollisionEndDelegate on_collision_end) {
    m_callback_begin = on_collision_begin;
    m_callback_end = on_collision_end;
}

void BulletWorld::debug_draw(IDebugDrawer& drawer) {
    DebugDrawer debug_drawer(drawer);
    m_world->setDebugDrawer(&debug_drawer);
    m_world->debugDrawWorld();
    m_world->setDebugDrawer(nullptr);
}

}    // namespace modus::physics
