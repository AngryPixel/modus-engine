/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <bullet_instance.hpp>
#include <bullet_types.hpp>
#include <bullet_world.hpp>

namespace modus::physics {

BulletInstance::BulletInstance() : m_rigid_body_db(m_shape_db) {}

Result<> BulletInstance::initialize() {
    return Ok<>();
}

Result<> BulletInstance::shutdown() {
    m_rigid_body_db.shutdown();
    return Ok<>();
}

std::unique_ptr<PhysicsWorld> BulletInstance::create_world() {
    return modus::make_unique<BulletWorld>(m_rigid_body_db);
}
Result<> BulletInstance::destroy_collision_shape(const CollisionShapeHandle handle) {
    return m_shape_db.destroy(handle);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_sphere(const f32 radius) {
    return m_shape_db.create_sphere(radius);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_box(
    const glm::vec3& radius) {
    return m_shape_db.create_box(radius);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_plane(
    const glm::vec3& direction,
    const f32 offset) {
    return m_shape_db.create_plane(direction, offset);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_convex_triangle_mesh(
    const TriangleMeshShapeCreateParams& params) {
    return m_shape_db.create_convex_triangle_mesh(params);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_static_triangle_mesh(
    const TriangleMeshShapeCreateParams& params) {
    return m_shape_db.create_static_triangle_mesh(params);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_convex_hull(
    const Slice<f32> points,
    const size_t num_points,
    const size_t stride_bytes,
    const glm::vec3& scale) {
    return m_shape_db.create_convex_hull(points, num_points, stride_bytes, scale);
}
Result<CollisionShapeHandle, void> BulletInstance::create_collision_shape_convex_hull(
    const TriangleMeshShapeCreateParams& params) {
    return m_shape_db.create_convex_hull(params);
}
Result<RigidBodyHandle, void> BulletInstance::create_rigid_body(
    const RigidBodyCreateParams& params) {
    return m_rigid_body_db.create_rigid_body(params);
}
Result<> BulletInstance::destroy_rigid_body(const RigidBodyHandle handle) {
    return m_rigid_body_db.destroy_rigid_body(handle);
}
Result<math::Transform, void> BulletInstance::rigid_body_transform(
    const RigidBodyHandle handle) const {
    return m_rigid_body_db.transform(handle);
}

Result<glm::vec3> BulletInstance::rigid_body_linear_velocity(const RigidBodyHandle handle) const {
    if (auto r_body = m_rigid_body_db.rigid_body(handle); !r_body) {
        return Error<>();
    } else {
        return Ok(make_vec3((*r_body)->rigid_body->getLinearVelocity()));
    }
}
Result<> BulletInstance::rigid_body_apply_force(const RigidBodyHandle handle,
                                                const glm::vec3& force,
                                                const glm::vec3& relative_position) {
    return m_rigid_body_db.apply_force(handle, force, relative_position);
}
Result<> BulletInstance::rigid_body_apply_relative_force(const RigidBodyHandle handle,
                                                         const glm::vec3& force,
                                                         const glm::vec3& relative_position) {
    return m_rigid_body_db.apply_relative_force(handle, force, relative_position);
}
Result<> BulletInstance::rigid_body_apply_impulse(const RigidBodyHandle handle,
                                                  const glm::vec3& force,
                                                  const glm::vec3& relative_position) {
    return m_rigid_body_db.apply_impulse(handle, force, relative_position);
}
Result<> BulletInstance::rigid_body_apply_torque_impulse(const RigidBodyHandle handle,
                                                         const glm::vec3& torque) {
    return m_rigid_body_db.apply_torque_impulse(handle, torque);
}
Result<> BulletInstance::rigid_body_apply_relative_impulse(const RigidBodyHandle handle,
                                                           const glm::vec3& force,
                                                           const glm::vec3& relative_position) {
    return m_rigid_body_db.apply_relative_impulse(handle, force, relative_position);
}
Result<> BulletInstance::rigid_body_apply_torque(const RigidBodyHandle handle,
                                                 const glm::vec3& force) {
    return m_rigid_body_db.apply_torque(handle, force);
}
Result<> BulletInstance::rigid_body_apply_relative_torque(const RigidBodyHandle handle,
                                                          const glm::vec3& force) {
    return m_rigid_body_db.apply_relative_torque(handle, force);
}
Result<> BulletInstance::rigid_body_set_linear_velocity(const RigidBodyHandle handle,
                                                        const glm::vec3& velocity) {
    return m_rigid_body_db.set_linear_velocity(handle, velocity);
}
Result<> BulletInstance::rigid_body_set_angular_velocity(const RigidBodyHandle handle,
                                                         const glm::vec3& velocity) {
    return m_rigid_body_db.set_angular_velocity(handle, velocity);
}
Result<> BulletInstance::rigid_body_set_transform(const RigidBodyHandle handle,
                                                  const math::Transform& transform) {
    return m_rigid_body_db.set_transform(handle, transform);
}
Result<RigidBodyState, void> BulletInstance::rigid_body_state(const RigidBodyHandle handle) {
    return m_rigid_body_db.rigid_body_state(handle);
}
Result<math::bv::AABB, void> BulletInstance::rigid_body_aabb(const RigidBodyHandle handle) const {
    return m_rigid_body_db.aabb(handle);
}
Result<f32> BulletInstance::rigid_body_angular_damping(const RigidBodyHandle handle) const {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (r_body) {
        return Ok((*r_body)->rigid_body->getAngularDamping());
    }
    return Error<>();
}
Result<f32> BulletInstance::rigid_body_linear_damping(const RigidBodyHandle handle) const {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (r_body) {
        return Ok((*r_body)->rigid_body->getLinearDamping());
    }
    return Error<>();
}
Result<glm::vec3> BulletInstance::rigid_body_angular_factor(const RigidBodyHandle handle) const {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (r_body) {
        return Ok(make_vec3((*r_body)->rigid_body->getAngularFactor()));
    }
    return Error<>();
}
Result<glm::vec3> BulletInstance::rigid_body_linear_factor(const RigidBodyHandle handle) const {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (r_body) {
        return Ok(make_vec3((*r_body)->rigid_body->getLinearFactor()));
    }
    return Error<>();
}
Result<> BulletInstance::set_rigid_body_angular_factor(const RigidBodyHandle handle,
                                                       const glm::vec3& factor) {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (r_body) {
        (*r_body)->rigid_body->setAngularFactor(make_btvec3(factor));
        return Ok();
    }
    return Error<>();
}
Result<> BulletInstance::set_rigid_body_linear_factor(const RigidBodyHandle handle,
                                                      const glm::vec3& factor) {
    auto r_body = m_rigid_body_db.rigid_body(handle);
    if (r_body) {
        (*r_body)->rigid_body->setLinearFactor(make_btvec3(factor));
        return Ok();
    }
    return Error<>();
}
}    // namespace modus::physics
