/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/allocator/pool_allocator.hpp>
#include <core/handle_pool.hpp>
#include <physics/rigid_body.hpp>

namespace modus::physics {

class BulletCollisionShapeDB;
struct BulletRigidBody {
    NotMyPtr<btRigidBody> rigid_body;
    NotMyPtr<btCollisionShape> shape;
    NotMyPtr<void> user_data;
    RigidBodyHandle handle;
    CollisionShapeHandle collision_shape_handle;
    NotMyPtr<btDynamicsWorld> world;
};

class BulletRigidBodyDB final {
   private:
    using RigidBodyPoolType = HandlePool<BulletRigidBody, PhysicsAllocator, RigidBodyHandle>;
    BulletCollisionShapeDB& m_shape_db;
    RigidBodyPoolType m_rigid_bodies;
    PoolAllocatorTyped<btRigidBody, PhysicsAllocator> m_rigid_bodies_pool;

   public:
    BulletRigidBodyDB(BulletCollisionShapeDB& shape_db);

    void shutdown();

    Result<NotMyPtr<BulletRigidBody>> rigid_body(const RigidBodyHandle handle);

    Result<NotMyPtr<const BulletRigidBody>> rigid_body(const RigidBodyHandle handle) const;

    Result<RigidBodyHandle, void> create_rigid_body(const RigidBodyCreateParams& params);

    Result<> destroy_rigid_body(const RigidBodyHandle handle);

    Result<math::Transform, void> transform(const RigidBodyHandle handle) const;

    Result<> apply_force(const RigidBodyHandle handle,
                         const glm::vec3& force,
                         const glm::vec3& relative_position);

    Result<> apply_relative_force(const RigidBodyHandle handle,
                                  const glm::vec3& force,
                                  const glm::vec3& relative_position);

    Result<> apply_impulse(const RigidBodyHandle handle,
                           const glm::vec3& force,
                           const glm::vec3& relative_position);

    Result<> apply_relative_impulse(const RigidBodyHandle handle,
                                    const glm::vec3& force,
                                    const glm::vec3& relative_position);

    Result<> apply_torque_impulse(const RigidBodyHandle handle, const glm::vec3& torque);

    Result<> apply_torque(const RigidBodyHandle handle, const glm::vec3& torque);

    Result<> apply_relative_torque(const RigidBodyHandle handle, const glm::vec3& force);

    Result<> set_linear_velocity(const RigidBodyHandle handle, const glm::vec3& velocity);

    Result<> set_angular_velocity(const RigidBodyHandle handle, const glm::vec3& velocity);

    Result<> set_transform(const RigidBodyHandle handle, const math::Transform& transform);

    Result<RigidBodyState, void> rigid_body_state(const RigidBodyHandle handle);

    Result<math::bv::AABB, void> aabb(const RigidBodyHandle handle) const;
};
}    // namespace modus::physics
