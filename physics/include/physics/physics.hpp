/*
 * Copyright 2020-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <core/core.hpp>
#include <core/handle_pool_shared.hpp>
#include <math/math.hpp>
#include <physics/module_config.hpp>

namespace modus::physics {

using PhysicsAllocator = DefaultAllocator;

MODUS_DECLARE_HANDLE_TYPE(RigidBodyHandle, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(CollisionShapeHandle, std::numeric_limits<u32>::max());

template <typename T>
using Vector = modus::Vector<T, PhysicsAllocator>;

}    // namespace modus::physics
