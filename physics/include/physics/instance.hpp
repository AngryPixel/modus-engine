/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <physics/collision_shape.hpp>
#include <physics/rigid_body.hpp>

namespace modus::physics {

class PhysicsWorld;

/// Physics Engine Instance through which physics worlds and resources
/// can be created.
class MODUS_PHYSICS_EXPORT Instance {
   public:
    virtual ~Instance() = default;

    virtual std::unique_ptr<PhysicsWorld> create_world() = 0;

    virtual Result<> initialize() = 0;

    virtual Result<> shutdown() = 0;

    virtual Result<> destroy_collision_shape(const CollisionShapeHandle) = 0;

    virtual Result<CollisionShapeHandle, void> create_collision_shape_sphere(const f32 radius) = 0;

    virtual Result<CollisionShapeHandle, void> create_collision_shape_box(
        const glm::vec3& radius) = 0;

    virtual Result<CollisionShapeHandle, void> create_collision_shape_plane(
        const glm::vec3& direction,
        const f32 offset) = 0;
    virtual Result<CollisionShapeHandle, void> create_collision_shape_convex_triangle_mesh(
        const TriangleMeshShapeCreateParams& params) = 0;

    virtual Result<CollisionShapeHandle, void> create_collision_shape_static_triangle_mesh(
        const TriangleMeshShapeCreateParams& params) = 0;

    virtual Result<CollisionShapeHandle, void> create_collision_shape_convex_hull(
        const Slice<f32> points,
        const size_t num_points,
        const size_t stride_bytes,
        const glm::vec3& scale) = 0;
    virtual Result<CollisionShapeHandle, void> create_collision_shape_convex_hull(
        const TriangleMeshShapeCreateParams& params) = 0;

    // --- Rigid Body ---------------------------------------------------------------------------
    virtual Result<RigidBodyHandle, void> create_rigid_body(
        const RigidBodyCreateParams& params) = 0;

    virtual Result<> destroy_rigid_body(const RigidBodyHandle handle) = 0;

    virtual Result<math::Transform, void> rigid_body_transform(
        const RigidBodyHandle handle) const = 0;

    virtual Result<glm::vec3> rigid_body_linear_velocity(const RigidBodyHandle handle) const = 0;

    virtual Result<> rigid_body_apply_force(const RigidBodyHandle handle,
                                            const glm::vec3& force,
                                            const glm::vec3& relative_position) = 0;

    virtual Result<> rigid_body_apply_relative_force(const RigidBodyHandle handle,
                                                     const glm::vec3& force,
                                                     const glm::vec3& relative_position) = 0;

    virtual Result<> rigid_body_apply_impulse(const RigidBodyHandle handle,
                                              const glm::vec3& force,
                                              const glm::vec3& relative_position) = 0;

    virtual Result<> rigid_body_apply_torque_impulse(const RigidBodyHandle handle,
                                                     const glm::vec3& torque) = 0;

    virtual Result<> rigid_body_apply_relative_impulse(const RigidBodyHandle handle,
                                                       const glm::vec3& force,
                                                       const glm::vec3& relative_position) = 0;

    virtual Result<> rigid_body_apply_torque(const RigidBodyHandle handle,
                                             const glm::vec3& force) = 0;

    virtual Result<> rigid_body_apply_relative_torque(const RigidBodyHandle handle,
                                                      const glm::vec3& force) = 0;

    virtual Result<> rigid_body_set_linear_velocity(const RigidBodyHandle handle,
                                                    const glm::vec3& velocity) = 0;

    virtual Result<> rigid_body_set_angular_velocity(const RigidBodyHandle handle,
                                                     const glm::vec3& velocity) = 0;

    virtual Result<> rigid_body_set_transform(const RigidBodyHandle handle,
                                              const math::Transform& transform) = 0;

    virtual Result<RigidBodyState, void> rigid_body_state(const RigidBodyHandle handle) = 0;

    virtual Result<math::bv::AABB, void> rigid_body_aabb(const RigidBodyHandle handle) const = 0;

    virtual Result<f32> rigid_body_angular_damping(const RigidBodyHandle handle) const = 0;

    virtual Result<f32> rigid_body_linear_damping(const RigidBodyHandle handle) const = 0;

    virtual Result<glm::vec3> rigid_body_angular_factor(const RigidBodyHandle handle) const = 0;

    virtual Result<glm::vec3> rigid_body_linear_factor(const RigidBodyHandle handle) const = 0;

    virtual Result<> set_rigid_body_angular_factor(const RigidBodyHandle handle,
                                                   const glm::vec3& factor) = 0;

    virtual Result<> set_rigid_body_linear_factor(const RigidBodyHandle handle,
                                                  const glm::vec3& factor) = 0;
};

MODUS_PHYSICS_EXPORT std::unique_ptr<Instance> create_default_instance();

}    // namespace modus::physics
