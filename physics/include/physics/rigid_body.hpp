/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <math/bounding_volumes.hpp>
#include <math/transform.hpp>
#include <physics/physics.hpp>

namespace modus::physics {

struct MODUS_PHYSICS_EXPORT RigidBodyCreateParams {
    struct CCD {
        f32 motion_threshold = 1e-7f;
        f32 sphere_radius = 0.5f;
    };
    static constexpr f32 kMassStatic = 0.0f;
    CollisionShapeHandle shape_handle;
    math::Transform transform = math::Transform::kIdentity;
    f32 mass = kMassStatic;
    f32 restitution = 0.0f;
    f32 friction = 0.0f;
    f32 angular_damping = 0.0f;
    f32 linear_damping = 0.0f;
    glm::vec3 angular_factor = glm::vec3(1.0f);
    glm::vec3 linear_factor = glm::vec3(1.0f);
    NotMyPtr<void> user_data;
    Optional<CCD> continuous_collision_detection;
    bool disable_collision_response = false;
    bool kinematic = false;
};

struct MODUS_PHYSICS_EXPORT RigidBodyState {
    math::Transform tranform;
    glm::vec3 linear_velocity;
    glm::vec3 angular_velocity;
    glm::vec3 total_force;
    glm::vec3 total_torque;
};
}    // namespace modus::physics
