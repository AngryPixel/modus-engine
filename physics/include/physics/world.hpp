/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <physics/physics.hpp>

#include <core/delegate.hpp>

namespace modus::physics {

class IDebugDrawer;

struct CollisionBeginInfo {
    Slice<glm::vec3> contact_points;
    glm::vec3 sum_normal_force;
    glm::vec3 sum_friction_force;
    struct {
        RigidBodyHandle handle;
        NotMyPtr<void> user_data;
    } object1;
    struct {
        RigidBodyHandle handle;
        NotMyPtr<void> user_data;
    } object2;
};

struct CollisionEndInfo {
    struct {
        RigidBodyHandle handle;
        NotMyPtr<void> user_data;
    } object1;
    struct {
        RigidBodyHandle handle;
        NotMyPtr<void> user_data;
    } object2;
};

using CollisionBeginDelegate = Delegate<void(CollisionBeginInfo&)>;
using CollisionEndDelegate = Delegate<void(CollisionEndInfo&)>;

struct WorldCreateParams {
    glm::vec3 gravity = glm::vec3(0.f, -10.0f, 0.f);
    // Debug Drawer
    CollisionBeginDelegate callback_collision_begin;
    CollisionEndDelegate callback_collision_end;
};

class MODUS_PHYSICS_EXPORT PhysicsWorld {
   public:
    virtual ~PhysicsWorld() = default;

    virtual Result<> initialize(const WorldCreateParams& params) = 0;

    virtual Result<> shutdown() = 0;

    virtual glm::vec3 gravity() const = 0;

    virtual void set_gravity(const glm::vec3& gravity) = 0;

    virtual void update(const f32 tick_seconds) = 0;

    virtual Result<> add_rigid_body(const RigidBodyHandle handle) = 0;

    virtual Result<> remove_rigid_body(const RigidBodyHandle handle) = 0;

    virtual void set_collision_callbacks(CollisionBeginDelegate on_collision_begin,
                                         CollisionEndDelegate on_collision_end) = 0;

    virtual void debug_draw(IDebugDrawer& drawer) = 0;
};

}    // namespace modus::physics
