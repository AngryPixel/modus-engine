/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <physics/physics.hpp>

namespace modus::physics {

enum class CollisionShapeType { Sphere, Box, Plane, TriangleMesh, StaticTriangleMesh, ConvexHull };

// NOTE: Slice Data Needs to be kept alive for as long as
// the shape is in use. Here be dragons?
struct MODUS_PHYSICS_EXPORT TriangleMeshShapeCreateParams {
    enum class IndexType : u8 { None, U8, U16, U32 };
    Slice<f32> vertices;
    ByteSlice indices;
    u32 vertices_stride = 0;
    u32 indices_stride = 0;
    u32 triangle_count = 0;
    u32 num_vertices = 0;
    IndexType index_type = IndexType::None;
    glm::vec3 scale = glm::vec3(1.0f);
};

}    // namespace modus::physics
