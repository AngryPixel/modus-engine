/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <physics/physics.hpp>

namespace modus::physics {

class MODUS_PHYSICS_EXPORT IDebugDrawer {
   public:
    virtual ~IDebugDrawer() = default;

    virtual void draw_line(const glm::vec3& from,
                           const glm::vec3& to,
                           const glm::vec3& colour_from,
                           const glm::vec3& colour_to) = 0;

    virtual void draw_text3d(const glm::vec3& pos, const StringSlice text) = 0;
};

}    // namespace modus::physics
