/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus::profiler {

using Color = unsigned int;
namespace color {
// Taken From easy_profiler profile_colors.h

#if defined(MODUS_PROFILER_USE_TRACY)
#define MODUS_MAKE_COLOR(X) X
#else
#define MODUS_MAKE_COLOR(X) (0xff000000 | X)
#endif

static constexpr Color Red50 = MODUS_MAKE_COLOR(0xffebee);
static constexpr Color Red100 = MODUS_MAKE_COLOR(0xffcdd2);
static constexpr Color Red200 = MODUS_MAKE_COLOR(0xef9a9a);
static constexpr Color Red300 = MODUS_MAKE_COLOR(0xe57373);
static constexpr Color Red400 = MODUS_MAKE_COLOR(0xef5350);
static constexpr Color Red500 = MODUS_MAKE_COLOR(0xf44336);
static constexpr Color Red600 = MODUS_MAKE_COLOR(0xe53935);
static constexpr Color Red700 = MODUS_MAKE_COLOR(0xd32f2f);
static constexpr Color Red800 = MODUS_MAKE_COLOR(0xc62828);
static constexpr Color Red900 = MODUS_MAKE_COLOR(0xb71c1c);
static constexpr Color RedA100 = MODUS_MAKE_COLOR(0xff8a80);
static constexpr Color RedA200 = MODUS_MAKE_COLOR(0xff5252);
static constexpr Color RedA400 = MODUS_MAKE_COLOR(0xff1744);
static constexpr Color RedA700 = MODUS_MAKE_COLOR(0xd50000);

static constexpr Color Pink50 = MODUS_MAKE_COLOR(0xfce4ec);
static constexpr Color Pink100 = MODUS_MAKE_COLOR(0xf8bbd0);
static constexpr Color Pink200 = MODUS_MAKE_COLOR(0xf48fb1);
static constexpr Color Pink300 = MODUS_MAKE_COLOR(0xf06292);
static constexpr Color Pink400 = MODUS_MAKE_COLOR(0xec407a);
static constexpr Color Pink500 = MODUS_MAKE_COLOR(0xe91e63);
static constexpr Color Pink600 = MODUS_MAKE_COLOR(0xd81b60);
static constexpr Color Pink700 = MODUS_MAKE_COLOR(0xc2185b);
static constexpr Color Pink800 = MODUS_MAKE_COLOR(0xad1457);
static constexpr Color Pink900 = MODUS_MAKE_COLOR(0x880e4f);
static constexpr Color PinkA100 = MODUS_MAKE_COLOR(0xff80ab);
static constexpr Color PinkA200 = MODUS_MAKE_COLOR(0xff4081);
static constexpr Color PinkA400 = MODUS_MAKE_COLOR(0xf50057);
static constexpr Color PinkA700 = MODUS_MAKE_COLOR(0xc51162);

static constexpr Color Purple50 = MODUS_MAKE_COLOR(0xf3e5f5);
static constexpr Color Purple100 = MODUS_MAKE_COLOR(0xe1bee7);
static constexpr Color Purple200 = MODUS_MAKE_COLOR(0xce93d8);
static constexpr Color Purple300 = MODUS_MAKE_COLOR(0xba68c8);
static constexpr Color Purple400 = MODUS_MAKE_COLOR(0xab47bc);
static constexpr Color Purple500 = MODUS_MAKE_COLOR(0x9c27b0);
static constexpr Color Purple600 = MODUS_MAKE_COLOR(0x8e24aa);
static constexpr Color Purple700 = MODUS_MAKE_COLOR(0x7b1fa2);
static constexpr Color Purple800 = MODUS_MAKE_COLOR(0x6a1b9a);
static constexpr Color Purple900 = MODUS_MAKE_COLOR(0x4a148c);
static constexpr Color PurpleA100 = MODUS_MAKE_COLOR(0xea80fc);
static constexpr Color PurpleA200 = MODUS_MAKE_COLOR(0xe040fb);
static constexpr Color PurpleA400 = MODUS_MAKE_COLOR(0xd500f9);
static constexpr Color PurpleA700 = MODUS_MAKE_COLOR(0xaa00ff);

static constexpr Color DeepPurple50 = MODUS_MAKE_COLOR(0xede7f6);
static constexpr Color DeepPurple100 = MODUS_MAKE_COLOR(0xd1c4e9);
static constexpr Color DeepPurple200 = MODUS_MAKE_COLOR(0xb39ddb);
static constexpr Color DeepPurple300 = MODUS_MAKE_COLOR(0x9575cd);
static constexpr Color DeepPurple400 = MODUS_MAKE_COLOR(0x7e57c2);
static constexpr Color DeepPurple500 = MODUS_MAKE_COLOR(0x673ab7);
static constexpr Color DeepPurple600 = MODUS_MAKE_COLOR(0x5e35b1);
static constexpr Color DeepPurple700 = MODUS_MAKE_COLOR(0x512da8);
static constexpr Color DeepPurple800 = MODUS_MAKE_COLOR(0x4527a0);
static constexpr Color DeepPurple900 = MODUS_MAKE_COLOR(0x311b92);
static constexpr Color DeepPurpleA100 = MODUS_MAKE_COLOR(0xb388ff);
static constexpr Color DeepPurpleA200 = MODUS_MAKE_COLOR(0x7c4dff);
static constexpr Color DeepPurpleA400 = MODUS_MAKE_COLOR(0x651fff);
static constexpr Color DeepPurpleA700 = MODUS_MAKE_COLOR(0x6200ea);

static constexpr Color Indigo50 = MODUS_MAKE_COLOR(0xe8eaf6);
static constexpr Color Indigo100 = MODUS_MAKE_COLOR(0xc5cae9);
static constexpr Color Indigo200 = MODUS_MAKE_COLOR(0x9fa8da);
static constexpr Color Indigo300 = MODUS_MAKE_COLOR(0x7986cb);
static constexpr Color Indigo400 = MODUS_MAKE_COLOR(0x5c6bc0);
static constexpr Color Indigo500 = MODUS_MAKE_COLOR(0x3f51b5);
static constexpr Color Indigo600 = MODUS_MAKE_COLOR(0x3949ab);
static constexpr Color Indigo700 = MODUS_MAKE_COLOR(0x303f9f);
static constexpr Color Indigo800 = MODUS_MAKE_COLOR(0x283593);
static constexpr Color Indigo900 = MODUS_MAKE_COLOR(0x1a237e);
static constexpr Color IndigoA100 = MODUS_MAKE_COLOR(0x8c9eff);
static constexpr Color IndigoA200 = MODUS_MAKE_COLOR(0x536dfe);
static constexpr Color IndigoA400 = MODUS_MAKE_COLOR(0x3d5afe);
static constexpr Color IndigoA700 = MODUS_MAKE_COLOR(0x304ffe);

static constexpr Color Blue50 = MODUS_MAKE_COLOR(0xe3f2fd);
static constexpr Color Blue100 = MODUS_MAKE_COLOR(0xbbdefb);
static constexpr Color Blue200 = MODUS_MAKE_COLOR(0x90caf9);
static constexpr Color Blue300 = MODUS_MAKE_COLOR(0x64b5f6);
static constexpr Color Blue400 = MODUS_MAKE_COLOR(0x42a5f5);
static constexpr Color Blue500 = MODUS_MAKE_COLOR(0x2196f3);
static constexpr Color Blue600 = MODUS_MAKE_COLOR(0x1e88e5);
static constexpr Color Blue700 = MODUS_MAKE_COLOR(0x1976d2);
static constexpr Color Blue800 = MODUS_MAKE_COLOR(0x1565c0);
static constexpr Color Blue900 = MODUS_MAKE_COLOR(0x0d47a1);
static constexpr Color BlueA100 = MODUS_MAKE_COLOR(0x82b1ff);
static constexpr Color BlueA200 = MODUS_MAKE_COLOR(0x448aff);
static constexpr Color BlueA400 = MODUS_MAKE_COLOR(0x2979ff);
static constexpr Color BlueA700 = MODUS_MAKE_COLOR(0x2962ff);

static constexpr Color LightBlue50 = MODUS_MAKE_COLOR(0xe1f5fe);
static constexpr Color LightBlue100 = MODUS_MAKE_COLOR(0xb3e5fc);
static constexpr Color LightBlue200 = MODUS_MAKE_COLOR(0x81d4fa);
static constexpr Color LightBlue300 = MODUS_MAKE_COLOR(0x4fc3f7);
static constexpr Color LightBlue400 = MODUS_MAKE_COLOR(0x29b6f6);
static constexpr Color LightBlue500 = MODUS_MAKE_COLOR(0x03a9f4);
static constexpr Color LightBlue600 = MODUS_MAKE_COLOR(0x039be5);
static constexpr Color LightBlue700 = MODUS_MAKE_COLOR(0x0288d1);
static constexpr Color LightBlue800 = MODUS_MAKE_COLOR(0x0277bd);
static constexpr Color LightBlue900 = MODUS_MAKE_COLOR(0x01579b);
static constexpr Color LightBlueA100 = MODUS_MAKE_COLOR(0x80d8ff);
static constexpr Color LightBlueA200 = MODUS_MAKE_COLOR(0x40c4ff);
static constexpr Color LightBlueA400 = MODUS_MAKE_COLOR(0x00b0ff);
static constexpr Color LightBlueA700 = MODUS_MAKE_COLOR(0x0091ea);

static constexpr Color Cyan50 = MODUS_MAKE_COLOR(0xe0f7fa);
static constexpr Color Cyan100 = MODUS_MAKE_COLOR(0xb2ebf2);
static constexpr Color Cyan200 = MODUS_MAKE_COLOR(0x80deea);
static constexpr Color Cyan300 = MODUS_MAKE_COLOR(0x4dd0e1);
static constexpr Color Cyan400 = MODUS_MAKE_COLOR(0x26c6da);
static constexpr Color Cyan500 = MODUS_MAKE_COLOR(0x00bcd4);
static constexpr Color Cyan600 = MODUS_MAKE_COLOR(0x00acc1);
static constexpr Color Cyan700 = MODUS_MAKE_COLOR(0x0097a7);
static constexpr Color Cyan800 = MODUS_MAKE_COLOR(0x00838f);
static constexpr Color Cyan900 = MODUS_MAKE_COLOR(0x006064);
static constexpr Color CyanA100 = MODUS_MAKE_COLOR(0x84ffff);
static constexpr Color CyanA200 = MODUS_MAKE_COLOR(0x18ffff);
static constexpr Color CyanA400 = MODUS_MAKE_COLOR(0x00e5ff);
static constexpr Color CyanA700 = MODUS_MAKE_COLOR(0x00b8d4);

static constexpr Color Teal50 = MODUS_MAKE_COLOR(0xe0f2f1);
static constexpr Color Teal100 = MODUS_MAKE_COLOR(0xb2dfdb);
static constexpr Color Teal200 = MODUS_MAKE_COLOR(0x80cbc4);
static constexpr Color Teal300 = MODUS_MAKE_COLOR(0x4db6ac);
static constexpr Color Teal400 = MODUS_MAKE_COLOR(0x26a69a);
static constexpr Color Teal500 = MODUS_MAKE_COLOR(0x009688);
static constexpr Color Teal600 = MODUS_MAKE_COLOR(0x00897b);
static constexpr Color Teal700 = MODUS_MAKE_COLOR(0x00796b);
static constexpr Color Teal800 = MODUS_MAKE_COLOR(0x00695c);
static constexpr Color Teal900 = MODUS_MAKE_COLOR(0x004d40);
static constexpr Color TealA100 = MODUS_MAKE_COLOR(0xa7ffeb);
static constexpr Color TealA200 = MODUS_MAKE_COLOR(0x64ffda);
static constexpr Color TealA400 = MODUS_MAKE_COLOR(0x1de9b6);
static constexpr Color TealA700 = MODUS_MAKE_COLOR(0x00bfa5);

static constexpr Color Green50 = MODUS_MAKE_COLOR(0xe8f5e9);
static constexpr Color Green100 = MODUS_MAKE_COLOR(0xc8e6c9);
static constexpr Color Green200 = MODUS_MAKE_COLOR(0xa5d6a7);
static constexpr Color Green300 = MODUS_MAKE_COLOR(0x81c784);
static constexpr Color Green400 = MODUS_MAKE_COLOR(0x66bb6a);
static constexpr Color Green500 = MODUS_MAKE_COLOR(0x4caf50);
static constexpr Color Green600 = MODUS_MAKE_COLOR(0x43a047);
static constexpr Color Green700 = MODUS_MAKE_COLOR(0x388e3c);
static constexpr Color Green800 = MODUS_MAKE_COLOR(0x2e7d32);
static constexpr Color Green900 = MODUS_MAKE_COLOR(0x1b5e20);
static constexpr Color GreenA100 = MODUS_MAKE_COLOR(0xb9f6ca);
static constexpr Color GreenA200 = MODUS_MAKE_COLOR(0x69f0ae);
static constexpr Color GreenA400 = MODUS_MAKE_COLOR(0x00e676);
static constexpr Color GreenA700 = MODUS_MAKE_COLOR(0x00c853);

static constexpr Color LightGreen50 = MODUS_MAKE_COLOR(0xf1f8e9);
static constexpr Color LightGreen100 = MODUS_MAKE_COLOR(0xdcedc8);
static constexpr Color LightGreen200 = MODUS_MAKE_COLOR(0xc5e1a5);
static constexpr Color LightGreen300 = MODUS_MAKE_COLOR(0xaed581);
static constexpr Color LightGreen400 = MODUS_MAKE_COLOR(0x9ccc65);
static constexpr Color LightGreen500 = MODUS_MAKE_COLOR(0x8bc34a);
static constexpr Color LightGreen600 = MODUS_MAKE_COLOR(0x7cb342);
static constexpr Color LightGreen700 = MODUS_MAKE_COLOR(0x689f38);
static constexpr Color LightGreen800 = MODUS_MAKE_COLOR(0x558b2f);
static constexpr Color LightGreen900 = MODUS_MAKE_COLOR(0x33691e);
static constexpr Color LightGreenA100 = MODUS_MAKE_COLOR(0xccff90);
static constexpr Color LightGreenA200 = MODUS_MAKE_COLOR(0xb2ff59);
static constexpr Color LightGreenA400 = MODUS_MAKE_COLOR(0x76ff03);
static constexpr Color LightGreenA700 = MODUS_MAKE_COLOR(0x64dd17);

static constexpr Color Lime50 = MODUS_MAKE_COLOR(0xf9ebe7);
static constexpr Color Lime100 = MODUS_MAKE_COLOR(0xf0f4c3);
static constexpr Color Lime200 = MODUS_MAKE_COLOR(0xe6ee9c);
static constexpr Color Lime300 = MODUS_MAKE_COLOR(0xdce775);
static constexpr Color Lime400 = MODUS_MAKE_COLOR(0xd4e157);
static constexpr Color Lime500 = MODUS_MAKE_COLOR(0xcddc39);
static constexpr Color Lime600 = MODUS_MAKE_COLOR(0xc0ca33);
static constexpr Color Lime700 = MODUS_MAKE_COLOR(0xafb42b);
static constexpr Color Lime800 = MODUS_MAKE_COLOR(0x9e9d24);
static constexpr Color Lime900 = MODUS_MAKE_COLOR(0x827717);
static constexpr Color LimeA100 = MODUS_MAKE_COLOR(0xf4ff81);
static constexpr Color LimeA200 = MODUS_MAKE_COLOR(0xeeff41);
static constexpr Color LimeA400 = MODUS_MAKE_COLOR(0xc6ff00);
static constexpr Color LimeA700 = MODUS_MAKE_COLOR(0xaeea00);

static constexpr Color Yellow50 = MODUS_MAKE_COLOR(0xfffde7);
static constexpr Color Yellow100 = MODUS_MAKE_COLOR(0xfff9c4);
static constexpr Color Yellow200 = MODUS_MAKE_COLOR(0xfff59d);
static constexpr Color Yellow300 = MODUS_MAKE_COLOR(0xfff176);
static constexpr Color Yellow400 = MODUS_MAKE_COLOR(0xffee58);
static constexpr Color Yellow500 = MODUS_MAKE_COLOR(0xffeb3b);
static constexpr Color Yellow600 = MODUS_MAKE_COLOR(0xfdd835);
static constexpr Color Yellow700 = MODUS_MAKE_COLOR(0xfbc02d);
static constexpr Color Yellow800 = MODUS_MAKE_COLOR(0xf9a825);
static constexpr Color Yellow900 = MODUS_MAKE_COLOR(0xf57f17);
static constexpr Color YellowA100 = MODUS_MAKE_COLOR(0xffff8d);
static constexpr Color YellowA200 = MODUS_MAKE_COLOR(0xffff00);
static constexpr Color YellowA400 = MODUS_MAKE_COLOR(0xffea00);
static constexpr Color YellowA700 = MODUS_MAKE_COLOR(0xffd600);

static constexpr Color Amber50 = MODUS_MAKE_COLOR(0xfff8e1);
static constexpr Color Amber100 = MODUS_MAKE_COLOR(0xffecb3);
static constexpr Color Amber200 = MODUS_MAKE_COLOR(0xffe082);
static constexpr Color Amber300 = MODUS_MAKE_COLOR(0xffd54f);
static constexpr Color Amber400 = MODUS_MAKE_COLOR(0xffca28);
static constexpr Color Amber500 = MODUS_MAKE_COLOR(0xffc107);
static constexpr Color Amber600 = MODUS_MAKE_COLOR(0xffb300);
static constexpr Color Amber700 = MODUS_MAKE_COLOR(0xffa000);
static constexpr Color Amber800 = MODUS_MAKE_COLOR(0xff8f00);
static constexpr Color Amber900 = MODUS_MAKE_COLOR(0xff6f00);
static constexpr Color AmberA100 = MODUS_MAKE_COLOR(0xffe57f);
static constexpr Color AmberA200 = MODUS_MAKE_COLOR(0xffd740);
static constexpr Color AmberA400 = MODUS_MAKE_COLOR(0xffc400);
static constexpr Color AmberA700 = MODUS_MAKE_COLOR(0xffab00);

static constexpr Color Orange50 = MODUS_MAKE_COLOR(0xfff3e0);
static constexpr Color Orange100 = MODUS_MAKE_COLOR(0xffe0b2);
static constexpr Color Orange200 = MODUS_MAKE_COLOR(0xffcc80);
static constexpr Color Orange300 = MODUS_MAKE_COLOR(0xffb74d);
static constexpr Color Orange400 = MODUS_MAKE_COLOR(0xffa726);
static constexpr Color Orange500 = MODUS_MAKE_COLOR(0xff9800);
static constexpr Color Orange600 = MODUS_MAKE_COLOR(0xfb8c00);
static constexpr Color Orange700 = MODUS_MAKE_COLOR(0xf57c00);
static constexpr Color Orange800 = MODUS_MAKE_COLOR(0xef6c00);
static constexpr Color Orange900 = MODUS_MAKE_COLOR(0xe65100);
static constexpr Color OrangeA100 = MODUS_MAKE_COLOR(0xffd180);
static constexpr Color OrangeA200 = MODUS_MAKE_COLOR(0xffab40);
static constexpr Color OrangeA400 = MODUS_MAKE_COLOR(0xff9100);
static constexpr Color OrangeA700 = MODUS_MAKE_COLOR(0xff6d00);

static constexpr Color DeepOrange50 = MODUS_MAKE_COLOR(0xfbe9e7);
static constexpr Color DeepOrange100 = MODUS_MAKE_COLOR(0xffccbc);
static constexpr Color DeepOrange200 = MODUS_MAKE_COLOR(0xffab91);
static constexpr Color DeepOrange300 = MODUS_MAKE_COLOR(0xff8a65);
static constexpr Color DeepOrange400 = MODUS_MAKE_COLOR(0xff7043);
static constexpr Color DeepOrange500 = MODUS_MAKE_COLOR(0xff5722);
static constexpr Color DeepOrange600 = MODUS_MAKE_COLOR(0xf4511e);
static constexpr Color DeepOrange700 = MODUS_MAKE_COLOR(0xe64a19);
static constexpr Color DeepOrange800 = MODUS_MAKE_COLOR(0xd84315);
static constexpr Color DeepOrange900 = MODUS_MAKE_COLOR(0xbf360c);
static constexpr Color DeepOrangeA100 = MODUS_MAKE_COLOR(0xff9e80);
static constexpr Color DeepOrangeA200 = MODUS_MAKE_COLOR(0xff6e40);
static constexpr Color DeepOrangeA400 = MODUS_MAKE_COLOR(0xff3d00);
static constexpr Color DeepOrangeA700 = MODUS_MAKE_COLOR(0xdd2c00);

static constexpr Color Brown50 = MODUS_MAKE_COLOR(0xefebe9);
static constexpr Color Brown100 = MODUS_MAKE_COLOR(0xd7ccc8);
static constexpr Color Brown200 = MODUS_MAKE_COLOR(0xbcaaa4);
static constexpr Color Brown300 = MODUS_MAKE_COLOR(0xa1887f);
static constexpr Color Brown400 = MODUS_MAKE_COLOR(0x8d6e63);
static constexpr Color Brown500 = MODUS_MAKE_COLOR(0x795548);
static constexpr Color Brown600 = MODUS_MAKE_COLOR(0x6d4c41);
static constexpr Color Brown700 = MODUS_MAKE_COLOR(0x5d4037);
static constexpr Color Brown800 = MODUS_MAKE_COLOR(0x4e342e);
static constexpr Color Brown900 = MODUS_MAKE_COLOR(0x3e2723);

static constexpr Color Grey50 = MODUS_MAKE_COLOR(0xfafafa);
static constexpr Color Grey100 = MODUS_MAKE_COLOR(0xf5f5f5);
static constexpr Color Grey200 = MODUS_MAKE_COLOR(0xeeeeee);
static constexpr Color Grey300 = MODUS_MAKE_COLOR(0xe0e0e0);
static constexpr Color Grey400 = MODUS_MAKE_COLOR(0xbdbdbd);
static constexpr Color Grey500 = MODUS_MAKE_COLOR(0x9e9e9e);
static constexpr Color Grey600 = MODUS_MAKE_COLOR(0x757575);
static constexpr Color Grey700 = MODUS_MAKE_COLOR(0x616161);
static constexpr Color Grey800 = MODUS_MAKE_COLOR(0x424242);
static constexpr Color Grey900 = MODUS_MAKE_COLOR(0x212121);

static constexpr Color BlueGrey50 = MODUS_MAKE_COLOR(0xeceff1);
static constexpr Color BlueGrey100 = MODUS_MAKE_COLOR(0xcfd8dc);
static constexpr Color BlueGrey200 = MODUS_MAKE_COLOR(0xb0bec5);
static constexpr Color BlueGrey300 = MODUS_MAKE_COLOR(0x90a4ae);
static constexpr Color BlueGrey400 = MODUS_MAKE_COLOR(0x78909c);
static constexpr Color BlueGrey500 = MODUS_MAKE_COLOR(0x607d8b);
static constexpr Color BlueGrey600 = MODUS_MAKE_COLOR(0x546e7a);
static constexpr Color BlueGrey700 = MODUS_MAKE_COLOR(0x455a64);
static constexpr Color BlueGrey800 = MODUS_MAKE_COLOR(0x37474f);
static constexpr Color BlueGrey900 = MODUS_MAKE_COLOR(0x263238);

#if defined(MODUS_PROFILER_USE_TRACY)
static constexpr Color Black = MODUS_MAKE_COLOR(0x000001);
#else
static constexpr Color Black = MODUS_MAKE_COLOR(0x000000);
#endif
static constexpr Color White = MODUS_MAKE_COLOR(0xffffff);
static constexpr Color Null = 0x00000000;

static constexpr Color Red = Red500;
static constexpr Color DarkRed = Red900;
static constexpr Color Coral = Red200;
static constexpr Color RichRed = MODUS_MAKE_COLOR(0xff0000);
static constexpr Color Pink = Pink500;
static constexpr Color Rose = PinkA100;
static constexpr Color Purple = Purple500;
static constexpr Color Magenta = PurpleA200;
static constexpr Color DarkMagenta = PurpleA700;
static constexpr Color DeepPurple = DeepPurple500;
static constexpr Color Indigo = Indigo500;
static constexpr Color Blue = Blue500;
static constexpr Color DarkBlue = Blue900;
static constexpr Color RichBlue = MODUS_MAKE_COLOR(0x0000ff);
static constexpr Color LightBlue = LightBlue500;
static constexpr Color SkyBlue = LightBlueA100;
static constexpr Color Navy = LightBlue800;
static constexpr Color Cyan = Cyan500;
static constexpr Color DarkCyan = Cyan900;
static constexpr Color Teal = Teal500;
static constexpr Color DarkTeal = Teal900;
static constexpr Color Green = Green500;
static constexpr Color DarkGreen = Green900;
static constexpr Color RichGreen = MODUS_MAKE_COLOR(0x00ff00);
static constexpr Color LightGreen = LightGreen500;
static constexpr Color Mint = LightGreen900;
static constexpr Color Lime = Lime500;
static constexpr Color Olive = Lime900;
static constexpr Color Yellow = Yellow500;
static constexpr Color RichYellow = YellowA200;
static constexpr Color Amber = Amber500;
static constexpr Color Gold = Amber300;
static constexpr Color PaleGold = AmberA100;
static constexpr Color Orange = Orange500;
static constexpr Color Skin = Orange100;
static constexpr Color DeepOrange = DeepOrange500;
static constexpr Color Brick = DeepOrange900;
static constexpr Color Brown = Brown500;
static constexpr Color DarkBrown = Brown900;
static constexpr Color CreamWhite = Orange50;
static constexpr Color Wheat = Amber100;
static constexpr Color Grey = Grey500;
static constexpr Color Dark = Grey900;
static constexpr Color Silver = Grey300;
static constexpr Color BlueGrey = BlueGrey500;

static constexpr Color Default = Wheat;
}    // namespace color
}    // namespace modus::profiler

#undef MODUS_MAKE_COLOR

#if defined(MODUS_PROFILING)

#if defined(MODUS_PROFILER_USE_TRACY)

#if !defined(TRACY_ENABLE)
#error "TRACY_ENABLE has not been defined, profiler will not work!"
#endif
#define TRACY_NO_BROADCAST
#include <Tracy.hpp>

#define MODUS_PROFILE_SCOPE(color) ZoneScopedC(color)

#define MODUS_PROFILE_BLOCK(name, color) ZoneScopedNC(name, color)

#define MODUS_PROFILE_BLOCK_NAMED(varName, name, color) ZoneNamedNC(varName, name, color, true)

#define MODUS_PROFILE_ENABLE

#define MODUS_PROFILE_DISABLE

#define MODUS_PROFILE_MARK_MAIN_THREAD tracy::SetThreadName("Main")

#define MODUS_PROFILE_MARK_THREAD(NAME) tracy::SetThreadName(NAME)

#define MODUS_PROFILE_START_LISTENING(port)

#define MODUS_PROFILE_DUMP_TO_FILE(file)

#define MODUS_PROFILE_MARK_FRAME FrameMark

#define MODUS_PROFILE_MARK_FRAME_START(NAME) FrameMarkStart(NAME)

#define MODUS_PROFILE_MARK_FRAME_END(NAME) FrameMarkEnd(NAME)

#define MODUS_PROFILE_PLOT(name, value) TracyPlot(name, modus::i64(value))

#define MODUS_PROFILE_ALLOC(ptr, size) TracyAlloc(ptr, size)

#define MODUS_PROFILE_FREE(ptr) TracyFree(ptr)

#endif
#else

#define MODUS_PROFILE_SCOPE(color)

#define MODUS_PROFILE_BLOCK(name, color)

#define MODUS_PROFILE_BLOCK_NAMED(varName, name, color)

#define MODUS_PROFILE_FUNCTION(...)

#define MODUS_PROFILE_ENABLE

#define MODUS_PROFILE_DISABLE

#define MODUS_PROFILE_MARK_MAIN_THREAD

#define MODUS_PROFILE_MARK_THREAD(NAME)

#define MODUS_PROFILE_MARK_FRAME_START(NAME)

#define MODUS_PROFILE_MARK_FRAME_END(NAME)

#define MODUS_PROFILE_START_LISTENING(x)

#define MODUS_PROFILE_DUMP_TO_FILE(file) return 0

#define MODUS_PROFILE_MARK_FRAME

#define MODUS_PROFILE_PLOT(name, value)

#define MODUS_PROFILE_ALLOC(ptr, size)

#define MODUS_PROFILE_FREE(ptr)

#endif
