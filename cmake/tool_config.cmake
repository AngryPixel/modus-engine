#
# Tools setup
#

function(modus_add_tool target)

    add_executable(${target})

    modus_apply_output_dir(${target})

    modus_apply_common_target_flags(${target})

    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/src/pch.h)
        modus_apply_pch(${target})
    endif()
endfunction()
