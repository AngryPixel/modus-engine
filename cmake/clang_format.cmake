#
# clang format util
#

find_program(clang_format_bin clang-format)

if (clang_format_bin)
    message(STATUS "Found clang-format: ${clang_format_bin}, enabeling clangformat target")
    # TODO: improve
    # get all project files
    set(ALL_SOURCE_FILES "")
    set(module_list
        core
        log
        os
        app
        threed
        engine
        engine_modules
        math
        vfs
        tools
        physics
        profiler
        game
        audio
        package
        ui
        jobsys
    )

    foreach(module IN LISTS module_list)
        file(GLOB_RECURSE file_list
            ${module}/*.cpp
            ${module}/*.hpp
            ${module}/*.h
            ${module}/*.c
        )
        list(APPEND ALL_SOURCE_FILES ${file_list})
    endforeach()

    add_custom_target(
            clangformat
            COMMAND "${clang_format_bin}"
            -i
            --verbose
            ${ALL_SOURCE_FILES}
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMENT "Running clang-format"
    )
endif()
