#
# Test setup
#
enable_testing()

find_package(Catch2 CONFIG REQUIRED)

set(catch_main_src "${CMAKE_BINARY_DIR}/catch_main.cpp")

if (NOT EXISTS "${catch_main_src}")
    file(WRITE ${catch_main_src}
        "#define CATCH_CONFIG_MAIN\n#include <catch2/catch.hpp>\n"
    )
endif()

# MSVC doesn't like it when the main is defined in another static library, use object library
# to get around this
add_library(modus_catch_main OBJECT ${catch_main_src})
modus_apply_common_target_flags(modus_catch_main)
target_link_libraries(modus_catch_main PRIVATE Catch2::Catch2)

function(modus_add_test target)

    cmake_parse_arguments(arg "SKIP_CATCH" "NAME" "" ${ARGN})

    add_executable(${target})
    get_property(is_multi_config GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
    if (is_multi_config)
        set_target_properties(${target} PROPERTIES
            LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests/$<CONFIG>
            RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests/$<CONFIG>
        )
    else()
        set_target_properties(${target} PROPERTIES
            LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests
            RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests
        )
    endif()

    modus_apply_common_target_flags(${target})

    if (NOT arg_SKIP_CATCH)
        target_link_libraries(${target} PRIVATE modus_catch_main Catch2::Catch2)
    endif()

    add_test(NAME ${arg_NAME}
        COMMAND ${target}
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin_tests
     )

    target_compile_definitions(${target}
        PRIVATE MODUS_TEST_SOURCE_DIR="${CMAKE_CURRENT_SOURCE_DIR}"
        PRIVATE MODUS_TEST_BINARY_DIR="${CMAKE_CURRENT_BINARY_DIR}"
    )

    # Enable exceptions for test frameworks
    if (NOT MSVC)
        target_compile_options(${target} PRIVATE -fexceptions)
    else()
	target_compile_options(${target} PRIVATE /EHsc)
    endif()
endfunction()
