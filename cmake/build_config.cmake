#
# Build Configuration Setup
#

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Attempt to auto dectect raspberry pi
if (EXISTS "/opt/vc/include/bcm_host.h")
    message(STATUS "Rasperberry PI detecteted")
    set(MODUS_RASPBERRY_PI4 TRUE)
endif()

if (UNIX AND NOT APPLE)
    set(LINUX TRUE CACHE INTERNAL "Linux OS")
endif()

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    find_program(lld_binary NAMES lld ld.lld)
    if (lld_binary)
        message(STATUS "Found ldd linker, overriding default linker...")
        set(MODUS_LINKER_OVERRIDE_COMPILE_FLAGS "" CACHE INTERNAL "Use ld.lld as linker")
        set(MODUS_LINKER_OVERRIDE_LINK_FLAGS "-Wl,-fuse-ld=lld" CACHE INTERNAL "Use ld.lld as linker")
    endif()
endif()

#if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
#    find_program(gold_binary NAMES ld.gold)
#    if (gold_binary)
#        message(STATUS "Found gold linker, overriding default linker...")
#        set(MODUS_LINKER_OVERRIDE_COMPILE_FLAGS "-fuse-ld=gold" CACHE INTERNAL "Use ld.lld as linker")
#        set(MODUS_LINKER_OVERRIDE_LINK_FLAGS "-Wl,-fuse-ld=gold" CACHE INTERNAL "Use ld.lld as linker")
#    endif()
#endif()

if (MODUS_RASPBERRY_PI4)
    add_compile_options(
        -march=native
        -mcpu=native
        -mtune=native
        #-mfpu=neon
        -mfpu=neon-fp-armv8
        #-mneon-for-64bits
        -mfloat-abi=hard
    )
endif()


function(modus_apply_pch target)
    if (MODUS_USE_PCH)
        if (NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/src/pch.h")
            message(FATAL_ERROR "Please create src/pch.h for ${target} under ${CMAKE_CURRENT_SOURCE_DIR}")
        endif()

        if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/src/pch.cpp")
            message(FATAL_ERROR "${CMAKE_CURRENT_SOURCE_DIR}/src/pch.cpp is not required!")
        endif()

        if (CMAKE_VERSION GREATER_EQUAL 3.16.0)
            target_precompile_headers(${target} PRIVATE
                "$<$<COMPILE_LANGUAGE:CXX>:${CMAKE_CURRENT_SOURCE_DIR}/src/pch.h>"
            )
        endif()
        target_compile_definitions(${target} PUBLIC MODUS_USE_PCH)
    endif()
    target_sources(${target} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/pch.h)
endfunction()

function(modus_generate_module_config_header target
        version_major version_minor version_patch)
    string(TOUPPER ${target} MODULE_NAME)
    set(MODULE_VERSION_MAJOR ${version_major})
    set(MODULE_VERSION_MINOR ${version_minor})
    set(MODULE_VERSION_PATCH ${version_patch})
    set(MODULE_VERSION_STRING "${version_major}.${version_minor}.${version_patch}")
    get_target_property(binary_dir ${target} BINARY_DIR)
    set(generated_dir ${binary_dir}/gen_config/)
    set(generated_file ${generated_dir}/${target}/module_config.hpp)
    configure_file(${CMAKE_SOURCE_DIR}/cmake/module_config.hpp.in
        ${generated_file}
    )
    target_include_directories(${target} PUBLIC ${generated_dir})
    target_sources(${target} PRIVATE ${generated_file})
    set_target_properties(${target} PROPERTIES VERSION ${MODULE_VERSION_STRING})
    if (MODUS_BUILD_SHARED)
    target_compile_definitions(${target}
    PUBLIC MODUS_SHARED_LIBRARY_BUILD=1
    )
    endif()
    target_compile_definitions(${target}
        PRIVATE MODUS_${MODULE_NAME}_BUILDING=1
    )
endfunction()

function(modus_generate_binary_config_header target
        version_major version_minor version_patch)
    string(TOUPPER ${target} MODULE_NAME)
    set(MODULE_VERSION_MAJOR ${version_major})
    set(MODULE_VERSION_MINOR ${version_minor})
    set(MODULE_VERSION_PATCH ${version_patch})
    set(MODULE_VERSION_STRING "${version_major}.${version_minor}.${version_patch}")
    get_target_property(binary_dir ${target} BINARY_DIR)
    set(generated_dir ${binary_dir}/gen_config/)
    set(generated_file ${generated_dir}/${target}/binary_config.hpp)
    configure_file(${CMAKE_SOURCE_DIR}/cmake/binary_config.hpp.in
        ${generated_file}
    )
    target_include_directories(${target} PUBLIC ${generated_dir})
    target_sources(${target} PRIVATE ${generated_file})
    set_target_properties(${target} PROPERTIES VERSION ${MODULE_VERSION_STRING})
endfunction()

function(modus_apply_output_dir target)
    get_property(is_multi_config GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
    if (is_multi_config)
        set_target_properties(${target} PROPERTIES
            LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/$<CONFIG>
            RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/$<CONFIG>
            ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/$<CONFIG>
        )
    else()
        set_target_properties(${target} PROPERTIES
            LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
            RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
            ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib
        )
    endif()
endfunction()

function(modus_apply_common_target_flags target)

    cmake_parse_arguments(arg "NO_ERROR_ON_WARNING" "" "" ${ARGN})

    get_target_property(target_type ${target} TYPE)

    set_target_properties(${target}
        PROPERTIES
            C_STANDARD 99
            CXX_STANDARD 17
            CXX_EXTENSIONS OFF
            VISIBILITY_INLINES_HIDDEN TRUE
            CXX_VISIBILITY_PRESET hidden
            C_VISIBILITY_PRESET hidden
    )
    if (MSVC)
        target_compile_definitions(${target} PUBLIC -DUNICODE -D_UNICODE)
    endif()

    target_compile_definitions(${target}
        PUBLIC
            $<$<CONFIG:Debug>:MODUS_DEBUG>
            $<$<NOT:$<CONFIG:Debug>>: NDEBUG>
            $<$<CONFIG:Release>: MODUS_RELEASE>
    )

    if (MODUS_PROFILE)
        target_compile_definitions(${target} PUBLIC
            MODUS_PROFILE
        )
    endif()

    if (MODUS_RTM)
        target_compile_definitions(${target} PUBLIC
            MODUS_RTM
        )
    endif()

    # Platform Defines
    target_compile_definitions(${target}
        PUBLIC
            $<$<BOOL:${UNIX}>:MODUS_OS_UNIX>
            $<$<BOOL:${LINUX}>:MODUS_OS_LINUX>
            $<$<BOOL:${APPLE}>:MODUS_OS_DARWIN>
            $<$<BOOL:${WIN32}>:MODUS_OS_WIN32>
            $<$<EQUAL:${CMAKE_SIZEOF_VOID_P},8>:MODUS_64BIT>
            $<$<EQUAL:${CMAKE_SIZEOF_VOID_P},4>:MODUS_32BIT>
     )

    # General include paths
    target_include_directories(${target}
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
        PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src
    )

    # Compiler settings
    if (NOT MSVC)
        # clang & gcc
        target_compile_options(${target}
            PRIVATE
                -pedantic
                $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>
                $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
                $<$<CONFIG:Debug>:-fno-inline -O0>
                -fPIC
        )

        if (NOT MODUS_RTM)
            target_compile_options(${target}
                PRIVATE
                    -g3
                    -fno-omit-frame-pointer
            )
        endif()

        # Warning flags
        target_compile_options(${target}
            PRIVATE
                -Wall
                -Wextra
                $<$<COMPILE_LANG_AND_ID:CXX,GNU>:-Wduplicated-cond>
                $<$<COMPILE_LANG_AND_ID:CXX,GNU>:-Wduplicated-branches>
                $<$<COMPILE_LANG_AND_ID:CXX,GNU>:-Wlogical-op>
                $<$<COMPILE_LANG_AND_ID:CXX,GNU>:-Wrestrict>
                $<$<COMPILE_LANG_AND_ID:CXX,GNU>:-Wno-maybe-uninitialized>
                #$<$<COMPILE_LANG_AND_ID:CXX,GNU>:-Wuseless-cast>
                $<$<COMPILE_LANG_AND_ID:C,GNU>:-Wjump-misses-init>
                $<$<COMPILE_LANG_AND_ID:CXX,Clang>:-Wnull-dereference>
                -Wdouble-promotion
                -Wshadow
                -Wformat=2
                -Wcast-align
                -Wsign-compare
                -Wno-float-equal
                -Wreturn-type
                -Wunused-variable
                -Wno-error=attributes
        )

        # GCC 8.3 has some false positives that pop up
        if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU"
            AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "9.0.0")
            target_compile_options(${target}
                PRIVATE
                    -Wnull-dereference
            )
        endif()

        if (NOT MODUS_RASPBERRY_PI4)
            target_compile_options(${target} PRIVATE
                -msse2
                -msse3
                -msse4
                -msse4.1
                -msse4.2
            )
        endif()
        if (NOT arg_NO_ERROR_ON_WARNING)
            target_compile_options(${target} PRIVATE -Werror)
        endif()
    else()
        # MSVC
        target_compile_definitions(${target}
            PUBLIC -D_HAS_EXCEPTIONS=0 -DNOMINMAX
        )
        target_compile_options(${target}
            PRIVATE
                /W2
                /EHs-c-
                /Oi
        )

        target_compile_options(${target}
            PRIVATE
                /WX
                /permissive-
                $<$<CONFIG:Release>: /O2 /Oy- /Gm- /GS /MD /Ob2 /Gd>
                $<$<NOT:$<CONFIG:Debug>>: /Oy- /Od /Ob0 /Gm- /RTC1 /Zi>
        )
        if (NOT MODUS_RTM)
            target_compile_options(${target} PUBLIC /Zi)
            if (target_type STREQUAL "EXECUTABLE" OR
                    target_type STREQUAL "SHARED_LIBRARY")
                target_link_options(${target} PRIVATE /DEBUG)
            endif()
        endif()
    endif()

    if (MODUS_LINKER_OVERRIDE_COMPILE_FLAGS)
        target_compile_options(${target} PRIVATE ${MODUS_LINKER_OVERRIDE_COMPILE_FLAGS})
    endif()
    if (MODUS_LINKER_OVERRIDE_LINK_FLAGS)
        target_link_options(${target} PRIVATE ${MODUS_LINKER_OVERRIDE_LINK_FLAGS})
    endif()

    if (MODUS_RTM)
        target_compile_definitions(${target} PUBLIC MODUS_RTM)
    endif()
endfunction()


function(modus_add_module target)

    cmake_parse_arguments(arg
        "NO_ERROR_ON_WARNING;STATIC"
        "VERSION_MAJOR;VERSION_MINOR;VERSION_PATCH"
        ""
        ${ARGN}
    )

    if (arg_STATIC OR NOT MODUS_BUILD_SHARED)
        add_library(${target} STATIC)
    else()
        add_library(${target} SHARED)
    endif()

    modus_apply_output_dir(${target})

    modus_generate_module_config_header(${target}
        ${arg_VERSION_MAJOR}
        ${arg_VERSION_MINOR}
        ${arg_VERSION_PATCH}
    )

    modus_apply_common_target_flags(${target})

    modus_apply_pch(${target})

    # Enable tests
    if (MODUS_BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests/CMakeLists.txt)
        add_subdirectory(tests)
    endif()

    # Enable tools
    if (MODUS_BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools/CMakeLists.txt)
        add_subdirectory(tools)
    endif()

    add_library(${target}_private INTERFACE)
    target_include_directories(${target}_private INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/src)
    target_link_libraries(${target}_private INTERFACE ${target})
endfunction()

function(modus_add_game target)

    cmake_parse_arguments(arg
        "NO_ERROR_ON_WARNING"
        "VERSION_MAJOR;VERSION_MINOR;VERSION_PATCH"
        ""
        ${ARGN}
    )

    add_executable(${target})

    modus_apply_output_dir(${target})

    modus_apply_common_target_flags(${target})

    modus_generate_binary_config_header(${target}
        ${arg_VERSION_MAJOR}
        ${arg_VERSION_MINOR}
        ${arg_VERSION_PATCH}
    )

    modus_apply_pch(${target})

    if (MODUS_BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests/CMakeLists.txt)
        add_subdirectory(tests)
    endif()

    # Enable tools
    if (MODUS_BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools/CMakeLists.txt)
        add_subdirectory(tools)
    endif()

endfunction()
