#
# Add profile and rtm configurations
#

get_property(is_multi_config GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
if (NOT is_multi_config)
    set(valid_build_types Debug Release)
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "${valid_build_types}")
    if (NOT CMAKE_BUILD_TYPE)
        set(CMAKE_BUILD_TYPE Release CACHE STRING "" FORCE)
    elseif(NOT CMAKE_BUILD_TYPE IN_LIST valid_build_types)
        message(FATAL_ERROR "Invalid build type: ${CMAKE_BUILD_TYPE}")
    endif()
endif()
if (MSVC)
    string (REGEX REPLACE "/EHsc *" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
endif()
