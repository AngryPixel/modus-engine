/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format
#include <pch.h>
// clang-format
#include <ui/constraint.hpp>
#include <ui/object.hpp>

namespace modus::ui {

ConstraintParams constraint_params_from_object(ConstObjectPtr object) {
    ConstraintParams params;
    UIRefPtr<Object> parent = object->parent();
    params.parent_size = parent ? parent->size() : object->size();
    params.flex_factor = params.parent_size;
    return params;
}

ConstraintValue::ConstraintValue() : m_type(Type::Constant) {
    m_value = 0.f;
}

ConstraintValue::ConstraintValue(const ConstantValue value) {
    m_type = Type::Constant;
    m_value = value.value();
}
ConstraintValue& ConstraintValue::operator=(const ConstantValue value) {
    m_type = Type::Constant;
    m_value = value.value();
    return *this;
}

ConstraintValue::ConstraintValue(const FlexValue value) {
    m_type = Type::Flex;
    m_value = value.value();
    modus_assert(m_value >= 1.0f);
}
ConstraintValue& ConstraintValue::operator=(const FlexValue value) {
    m_type = Type::Flex;
    m_value = value.value();
    modus_assert(m_value >= 1.0f);
    return *this;
}

ConstraintValue::ConstraintValue(const IncomingValue value) {
    m_type = Type::Incoming;
    m_value = value.value();
}
ConstraintValue& ConstraintValue::operator=(const IncomingValue value) {
    m_type = Type::Incoming;
    m_value = value.value();
    return *this;
}

f32 ConstraintValue::transform(const f32 v) const {
    if (!is_constant()) {
        return m_value * v;
    } else {
        return m_value;
    }
}

Size Constraint::resolve(const ConstraintParams& params) const {
    modus_assert_message(!min_height.is_flex() && !min_width.is_flex(),
                         "Min width and height can't be flex constraints");
    Size size{0.f, 0.f};
    const f32 _min_height = min_height.transform(params.parent_size.height);
    const f32 _min_width = min_width.transform(params.parent_size.width);
    const f32 _max_height = !max_height.is_flex() ? max_height.transform(params.parent_size.height)
                                                  : max_height.transform(params.flex_factor.height);
    const f32 _max_width = !max_width.is_flex() ? max_width.transform(params.parent_size.width)
                                                : max_width.transform(params.flex_factor.width);
    size.width = std::max(_min_width, _max_width);
    size.height = std::max(_min_height, _max_height);
    return size;
}

bool is_flex_constraint(const Constraint& c) {
    return c.min_width.is_constant() && c.min_height.is_constant() &&
           (c.max_width.is_flex() || c.max_height.is_flex());
}

}    // namespace modus::ui
