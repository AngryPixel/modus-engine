/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/object.hpp>
#include <ui/context.hpp>

namespace modus::ui {

Object::Object(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : m_context(ctx), m_parent(parent), m_size({0, 0}), m_visible(true) {
    m_constraint.min_width = ConstantValue(10);
    m_constraint.min_height = ConstantValue(10);
    m_constraint.max_width = IncomingValue();
    m_constraint.max_height = IncomingValue();
}

void Object::set_visible(const bool value) {
    m_visible = value;
}

bool Object::on_event(const Event&, const Offset) {
    return false;
}

void Object::eval_constraints(const ConstraintParams& params) {
    m_size = m_constraint.resolve(params);
}

void Object::set_parent(const UIWeakRefPtr<Object>& parent) {
    m_parent = parent;
}

}    // namespace modus::ui
