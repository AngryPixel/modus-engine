/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format off

#include <ui/timer.hpp>
#include <ui/context.hpp>

namespace modus::ui {

    Timer::Timer(ContextPtr ctx) :
        m_context(ctx),
        m_elapsed_ms(0),
        m_target_ms(0) {
    }

    Timer::~Timer() {
        if (m_handle) {
            m_context->tickable_manager().destroy(m_handle);
        }
    }

    void Timer::on_tick(const IMilisec tick_ms){
        m_elapsed_ms += tick_ms;
        if (finished()) {
            cancel();
            if (m_on_trigger) {
                m_on_trigger(*this);
            }
        }
    }

    void Timer::reset() {
        m_elapsed_ms = IMilisec(0);
        start();
    }

    void Timer::cancel() {
        m_elapsed_ms = IMilisec(0);
        if (m_handle) {
            m_context->tickable_manager().destroy(m_handle);
            m_handle = TickableHandle();
        }
    }

    void Timer::repeat() {
        if (m_elapsed_ms > m_target_ms) {
            m_elapsed_ms -= m_target_ms;
        } else {
            m_elapsed_ms = IMilisec(0);
        }
        start();
    }

    void Timer::start() {
        if (!m_handle) {
            TickableDelegate delegate;
            delegate.bind<Timer, &Timer::on_tick>(this);
            m_handle = m_context->tickable_manager().create(delegate);
        }
    }
}

