
/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <ui/color.hpp>

namespace modus::ui {

// From https://unlimited3d.wordpress.com/2020/01/08/srgb-color-space-in-opengl/
static inline f32 norm_srgb_to_linear(const f32 srgb) {
    return srgb < 0.04045f ? srgb / 12.92f : glm::pow((srgb + 0.055f) / 1.055f, 2.4f);
}

static inline f32 norm_linear_to_srgb(const f32 linear) {
    return linear <= 0.0031308f ? linear * 12.92f : glm::pow(linear, 1.0f / 2.4f) * 1.055f - 0.055f;
}

Color Color::srgb_to_linear() const {
    glm::vec4 norm = normalize();
    norm.r = norm_srgb_to_linear(norm.r);
    norm.g = norm_srgb_to_linear(norm.g);
    norm.b = norm_srgb_to_linear(norm.b);
    return make_color(norm);
}

Color Color::linear_to_srgb() const {
    glm::vec4 norm = normalize();
    norm.r = norm_linear_to_srgb(norm.r);
    norm.g = norm_linear_to_srgb(norm.g);
    norm.b = norm_linear_to_srgb(norm.b);
    return make_color(norm);
}

glm::vec4 Color::normalize() const {
    glm::vec4 vec;
    vec.r = f32(this->v.r) / 255.f;
    vec.g = f32(this->v.g) / 255.f;
    vec.b = f32(this->v.b) / 255.f;
    vec.a = f32(this->v.a) / 255.f;
    return glm::clamp(vec, glm::vec4(0.f), glm::vec4(1.f));
}

Color Color::interpolate(const Color& from, const Color& to, const f32 interval) {
    const i16 diff_r = to.v.r - from.v.r;
    const i16 diff_g = to.v.g - from.v.g;
    const i16 diff_b = to.v.b - from.v.b;
    const i16 diff_a = to.v.a - from.v.a;
    Color r;
    r.v.r = from.v.r + u8(f32(diff_r) * f32(interval));
    r.v.g = from.v.g + u8(f32(diff_g) * f32(interval));
    r.v.b = from.v.b + u8(f32(diff_b) * f32(interval));
    r.v.a = from.v.a + u8(f32(diff_a) * f32(interval));
    return r;
}

Color make_color(const u8 r, const u8 g, const u8 b) {
    Color c;
    c.v.r = r;
    c.v.g = g;
    c.v.b = b;
    c.v.a = 255;
    return c;
}

Color make_color(const u8 r, const u8 g, const u8 b, const u8 a) {
    Color c;
    c.v.r = r;
    c.v.g = g;
    c.v.b = b;
    c.v.a = a;
    return c;
}

Color make_color(const glm::vec4& color) {
    Color c;
    c.v.r = static_cast<u8>(color.r * 225.0f);
    c.v.g = static_cast<u8>(color.g * 225.0f);
    c.v.b = static_cast<u8>(color.b * 225.0f);
    c.v.a = static_cast<u8>(color.a * 225.0f);
    return c;
}

Color make_color(const u32 hex) {
    Color c;
    c.u = hex;
    return c;
}

}    // namespace modus::ui
