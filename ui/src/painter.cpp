/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/painter.hpp>
#include <ui/context.hpp>
#include <ui/text/font.hpp>
#include <ui/text/text_utils.hpp>
#include <ui/color.hpp>
#include <math/packing.hpp>

namespace modus::ui {
static_assert(std::numeric_limits<PainterIndexType>::max() >= kPainterBufferQuadCount,
              "Index data type can't all the vertices from the data buffer");

ScissorScope::ScissorScope(Painter& painter, const Rect scissor_rect) : m_painter(painter) {
    m_painter.push_clip_rect(scissor_rect);
}
ScissorScope::~ScissorScope() {
    m_painter.pop_clip_rect();
}

Painter::Painter(ContextPtr context, NotMyPtr<PainterDevice> device)
    : m_context(context),
      m_painter_device(device),
      m_buffers(),
      m_commands(),
      m_scissor_stack(),
      m_buffer_index(0) {
    m_buffers.push_back(m_context->alloc<PainterBuffer>());
    m_commands.reserve(8);
    m_scissor_stack.reserve(8);
}

Result<> Painter::initialize() {
    return m_painter_device->initialize(m_context);
}

void Painter::shutdown() {
    return m_painter_device->shutdown(m_context);
}

void Painter::begin_frame(const bool clear) {
    m_early_clip_rect = make_rect(Offset{0, 0}, m_context->drawable_size());
    m_painter_device->begin_frame(clear);
}

void Painter::end_frame() {
    modus_assert_message(m_scissor_stack.empty(), "Scissor stack is not properlly cleared");
    m_painter_device->execute_commands(make_slice(m_buffers), make_slice(m_commands));
    m_painter_device->end_frame();
    for (auto& buffer : m_buffers) {
        buffer->vertices.clear();
        buffer->indices.clear();
    }
    m_commands.clear();
    m_buffer_index = 0;
}

void Painter::draw_rect(const Rect& rect, const u32 zvalue, const Color& color) {
    if (!m_early_clip_rect.intersects(rect)) {
        return;
    }
    MODUS_UNUSED(zvalue);
    PainterBuffer& buffer = get_or_allocate_buffer(4);

    const u32 buffer_index = m_buffer_index;
    const u16 index_offset = static_cast<u16>(buffer.vertices.size());
    const u32 index_buffer_offset = buffer.indices.size();

    const f32 top = f32(rect.y);
    const f32 left = f32(rect.x);
    const f32 right = f32(rect.x + rect.width);
    const f32 bottom = f32(rect.y + rect.height);

    const PainterVertexData data[] = {
        {{left, top}, math::pack_unorm_1616({0.f, 1.0f}), color.u},
        {{left, bottom}, math::pack_unorm_1616({0.f, 0.f}), color.u},
        {{right, bottom}, math::pack_unorm_1616({1.f, 0.f}), color.u},
        {{right, top}, math::pack_unorm_1616({1.f, 1.f}), color.u},
    };

    const PainterIndexType indices[] = {
        index_offset,
        static_cast<u16>(index_offset + u16(1)),
        static_cast<u16>(index_offset + u16(2)),
        index_offset,
        static_cast<u16>(index_offset + u16(2)),
        static_cast<u16>(index_offset + u16(3)),
    };

    for (auto& d : data) {
        buffer.vertices.push_back(d).expect("Failed to push back data");
    }

    for (auto& i : indices) {
        buffer.indices.push_back(i).expect("Failed to push back indices");
    }

    const PainterCommand cmd{buffer_index, index_buffer_offset * u32(sizeof(PainterIndexType)), 6,
                             nullptr, current_scissor_rect()};
    m_commands.push_back(cmd);
}

void Painter::draw_border(const Rect& rect,
                          const u32 zvalue,
                          const Color& color,
                          const f32 thickness) {
    // Early out
    if (!m_early_clip_rect.intersects(rect)) {
        return;
    }
    if (thickness <= 0) {
        return;
    }
    // top
    draw_rect({rect.x, rect.y, rect.width, thickness}, zvalue, color);
    // bottom
    draw_rect({rect.x, rect.y + rect.height - thickness, rect.width, thickness}, zvalue, color);
    // left
    draw_rect({rect.x, rect.y + thickness, thickness, rect.height - thickness * 2.f}, zvalue,
              color);
    // right
    draw_rect({rect.x + rect.width - thickness, rect.y + thickness, thickness,
               rect.height - thickness * 2.f},
              zvalue, color);
}

void Painter::draw_text(const PaintTextParams& params, const StringSlice text) {
    // Early out
    if (!m_early_clip_rect.intersects(params.rect)) {
        return;
    }

    const u32 quad_count = params.generator.quad_count(params.font, text);
    const u32 required_size = 4 * quad_count;
    PainterBuffer& buffer = get_or_allocate_buffer(required_size);

    const u32 index_buffer_offset = buffer.indices.size();
    const u32 buffer_index = m_buffer_index;

    params.generator.generate(buffer, {params.rect.x, params.rect.y}, params.color, params.font,
                              text);

    const PainterCommand cmd{buffer_index, index_buffer_offset * u32(sizeof(PainterIndexType)),
                             6 * quad_count, params.font.font_atlas(), current_scissor_rect()};
    m_commands.push_back(cmd);
}

PainterBuffer& Painter::get_or_allocate_buffer(const u32 required_size) {
    auto& current_buffer = m_buffers[m_buffer_index];
    if (current_buffer->vertices.size() + required_size > current_buffer->vertices.capacity()) {
        m_buffer_index++;
        if (m_buffer_index >= m_buffers.size()) {
            m_buffers.push_back(m_context->alloc<PainterBuffer>());
        }
    }
    return *m_buffers[m_buffer_index];
}

Result<UIRefPtr<PainterBitmap2D>> Painter::create_bitmap2d(
    const PainterBitmap2DCreateParams& params) {
    return m_painter_device->create_bitmap2d(m_context, params);
}

Optional<Rect> Painter::current_scissor_rect() const {
    return !m_scissor_stack.empty() ? m_scissor_stack.back() : Optional<Rect>();
}

void Painter::push_clip_rect(const Rect& r) {
    m_scissor_stack.push_back(r);
    m_early_clip_rect = r;
}

void Painter::pop_clip_rect() {
    if (!m_scissor_stack.empty()) {
        auto& stack = m_scissor_stack;
        stack.erase(stack.begin() + stack.size() - 1);
    }
    if (m_scissor_stack.empty()) {
        m_early_clip_rect = make_rect(Offset{0, 0}, m_context->drawable_size());
    }
}

}    // namespace modus::ui
