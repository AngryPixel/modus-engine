/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format off

#include <ui/animation.hpp>
#include <ui/context.hpp>

namespace modus::ui {

Animator::Animator(ContextPtr context) :
    m_context(context),
    m_elapsed_ms(0),
    m_duration_ms(1000),
    m_reversed(false) {

}

Animator::~Animator() {
    stop();
}

void Animator::start(const bool reversed) {
    modus_assert(m_animation);
    if (!m_animation) {
        return;
    }
    m_elapsed_ms = IMilisec(0);
    m_reversed = reversed;
    if (!m_handle) {
        TickableDelegate delegate;
        delegate.bind<Animator, &Animator::on_tick>(this);
        m_handle = m_context->tickable_manager().create(delegate);
    }
}

void Animator::start(UIRefPtr<Animation> animation, const bool reversed) {
    m_animation = animation;
    start(reversed);
}

void Animator::stop() {
    if (m_handle) {
        m_context->tickable_manager().destroy(m_handle);
        m_handle = TickableHandle();
    }
}

void Animator::set_reversed(const bool value) {
    if (m_reversed != value) {
        m_reversed = true;
        if (m_elapsed_ms < m_duration_ms) {
            m_elapsed_ms = m_duration_ms - m_elapsed_ms;
        }
    }
}

void Animator::on_tick(const IMilisec tick_ms) {
    m_elapsed_ms += tick_ms;
    f64 interval = std::min(f64(m_elapsed_ms.count())/f64(m_duration_ms.count()), 1.0);
    if (m_reversed) {
        interval = 1.0 - interval;
    }
    m_animation->animate(interval);
    if (m_elapsed_ms>= m_duration_ms) {
        stop();
        if (m_on_finished) {
            m_on_finished(*this);
        }
    }
}
}
