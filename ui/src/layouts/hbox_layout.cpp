/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/layouts/hbox_layout.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>

namespace modus::ui {
HBoxLayout::HBoxLayout(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_children(), m_margin_inner(0), m_margin_outer(0) {
    m_children.reserve(4);
}

void HBoxLayout::add_child(const UIRefPtr<Object>& child) {
    auto it = std::find_if(m_children.begin(), m_children.end(),
                           [ptr = child.get()](const auto& v) { return v.get() == ptr; });

    if (it == m_children.end()) {
        m_children.push_back(child);
    }
}

void HBoxLayout::remove_child(ConstObjectPtr child) {
    auto it = std::find_if(m_children.begin(), m_children.end(),
                           [child](const auto& v) { return v.get() == child.get(); });
    if (it != m_children.end()) {
        m_children.erase(it);
    }
}

ObjectPtr HBoxLayout::child(const size_t index) {
    if (index >= m_children.size()) {
        return ObjectPtr();
    }
    return m_children[index].get();
}

ConstObjectPtr HBoxLayout::child(const size_t index) const {
    if (index >= m_children.size()) {
        return ConstObjectPtr();
    }
    return m_children[index].get();
}

void HBoxLayout::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
    if (m_children.empty()) {
        return;
    }

    const f32 total_margin_size =
        (m_margin_outer * 2.f) + (m_margin_inner * f32(m_children.size() - 1));

    ConstraintParams child_params;
    child_params.parent_size.width = std::max(m_size.width - total_margin_size, 0.f);
    child_params.parent_size.height = std::max(m_size.height - (m_margin_outer * 2.f), 0.f);
    child_params.flex_factor = {0.f, 0.f};

    f32 total_child_width = 0.f;
    f32 total_flex_factors = 0.f;

    for (auto& child : m_children) {
        if (!child->constraint().max_width.is_flex()) {
            child->eval_constraints(child_params);
            total_child_width += child->size().width;
        } else {
            total_flex_factors += child->constraint().max_width.flex_value();
        }
    }

    if (total_flex_factors > 0) {
        const f32 free_width = child_params.parent_size.width -
                               std::min(total_child_width, child_params.parent_size.width);
        const f32 flex_value = free_width / total_flex_factors;
        child_params.flex_factor.width = flex_value;
        child_params.flex_factor.height = m_size.height;

        for (auto& child : m_children) {
            if (child->constraint().max_width.is_flex()) {
                child->eval_constraints(child_params);
            }
        }
    }

    if (is_inifinte(m_size.width)) {
        m_size.width = total_child_width + total_margin_size;
    }
}

bool HBoxLayout::on_event(const Event& event, const Offset offset) {
    Offset local_offset = offset + Offset{m_margin_outer, m_margin_outer};
    for (auto& child : m_children) {
        if (child->on_event(event, local_offset)) {
            return true;
        }
        local_offset.x += child->size().width + m_margin_inner;
    }
    return false;
}

void HBoxLayout::paint(Painter& painter, const PaintParams& params) const {
    PaintParams local_params = params.new_scope();
    local_params.offset.y += m_margin_outer;
    local_params.offset.x += m_margin_outer;
    for (auto& child : m_children) {
        child->paint(painter, local_params);
        local_params.offset.x += child->size().width + m_margin_inner;
    }
}

void HBoxLayout::set_margin_inner(const f32 margin_px) {
    modus_assert(margin_px >= 0.f);
    m_margin_inner = std::max(0.f, margin_px);
}

void HBoxLayout::set_margin_outer(const f32 margin_px) {
    modus_assert(margin_px >= 0.f);
    m_margin_outer = std::max(0.f, margin_px);
}

}    // namespace modus::ui
