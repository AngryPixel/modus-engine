/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/layouts/vscroll_layout.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/event.hpp>

namespace modus::ui {

VScrollLayout::VScrollLayout(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_scroll_bar(ctx), m_child(), m_scroll_offset({0, 0}) {
    m_scroll_bar.m_on_scroll.bind<VScrollLayout, &VScrollLayout::on_scroll>(this);
    m_scroll_bar.set_visible(false);
}

VScrollLayout::~VScrollLayout() {}

void VScrollLayout::set_child(const UIRefPtr<Object>& object) {
    m_child = object;
    m_scroll_offset = {0.f, 0.f};
}

void VScrollLayout::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
    if (m_child) {
        ConstraintParams child_params;
        child_params.parent_size = {m_size.width, kInfiniteValue};
        child_params.flex_factor = {0.f, 0.f};
        m_child->eval_constraints(child_params);
        m_scroll_bar.eval_constraints(params);
        m_scroll_bar.set_area_percentage(m_size.height / f32(m_child->size().height));
        update_scroll_offset();
    }
}

bool VScrollLayout::on_event(const Event& event, const Offset offset) {
    if (!m_child) {
        return false;
    }

    const Rect object_rect = make_rect(offset, m_size);
    if (event.type == EventType::MouseMove &&
        object_rect.contains(event.mouse_move.x, event.mouse_move.y)) {
        m_scroll_bar.set_visible(true);
    }

    const Offset scroll_bar_offset = {offset.x + m_size.width - m_scroll_bar.size().width,
                                      offset.y};
    if (m_scroll_bar.on_event(event, scroll_bar_offset)) {
        return true;
    }

    const Offset local_offset = offset + m_scroll_offset;
    if (m_child->on_event(event, local_offset)) {
        return true;
    }
    return false;
}

void VScrollLayout::paint(Painter& painter, const PaintParams& params) const {
    if (m_child) {
        ScissorScope scissor_scope(painter, make_rect(params.offset, m_size));
        PaintParams local_params = params.new_scope();
        local_params.offset = local_params.offset + m_scroll_offset;
        m_child->paint(painter, local_params);
        PaintParams scroll_bar_params = params.new_scope();
        scroll_bar_params.offset.x += m_size.width - m_scroll_bar.size().width;
        if (m_scroll_bar.is_visible()) {
            m_scroll_bar.paint(painter, scroll_bar_params);
        }
    }
}

void VScrollLayout::on_scroll(const f32 scroll_percentage) {
    if (m_child) {
        update_scroll_offset();
        if (m_on_scrolled) {
            m_on_scrolled(*this, scroll_percentage);
        }
    }
}

void VScrollLayout::update_scroll_offset() {
    const f32 max_scroll_height = m_child->size().height - m_size.height;
    m_scroll_offset.y = -(max_scroll_height * m_scroll_bar.scroll_percentage());
}

}    // namespace modus::ui
