/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/context.hpp>
#include <ui/object.hpp>
#include <ui/view.hpp>
#include <ui/event.hpp>
#include <ui/text/font.hpp>

namespace modus::ui {

Context::Context(NotMyPtr<PainterDevice> painter_device)
    : m_view_controller(this),
      m_drawable_size({0, 0}),
      m_painter(this, painter_device),
      m_tickable_manager(),
      m_visible(true) {}

Context::~Context() {
    m_root_view.reset();
    m_view_controller.clear();
}

Result<> Context::initialize() {
    return m_painter.initialize();
}

void Context::shutdown() {
    m_root_view.reset();
    m_view_controller.clear();
    m_painter.shutdown();
}

void Context::set_root_view(const UIRefPtr<View>& view) {
    m_view_controller.clear();
    m_root_view = view;
    m_view_controller.push_view(view);
}

void Context::update(const IMilisec elapsed_ms) {
    m_tickable_manager.update(elapsed_ms);
    m_view_controller.update();
    if (m_visible) {
        m_view_controller.eval_constraints();
    }
}

void Context::paint(const bool clear) const {
    if (m_visible) {
        m_painter.begin_frame(clear);
        m_view_controller.paint(m_painter);
        m_painter.end_frame();
    }
}

bool Context::on_event(const Event& evt) {
    if (evt.type == EventType::ContextResize) {
        m_drawable_size.width = evt.context_resize.width;
        m_drawable_size.height = evt.context_resize.height;
    } else if (evt.type == EventType::MouseMove) {
        m_mouse_location.x = f32(evt.mouse_move.x);
        m_mouse_location.y = f32(evt.mouse_move.y);
    }
    return m_view_controller.on_event(evt);
}

void Context::set_drawable_size(const Size& size) {
    Event evt;
    evt.type = EventType::ContextResize;
    evt.context_resize.width = size.width;
    evt.context_resize.height = size.height;
    on_event(evt);
}

void Context::set_default_font(const UIRefPtr<Font>& font) {
    modus_assert(font);
    m_default_font = font;
}

void Context::set_visible(const bool visible) {
    m_visible = visible;
    Event evt;
    evt.type = visible ? EventType::ContextMouseEnter : EventType::ContextMouseLeave;
    on_event(evt);
}

}    // namespace modus::ui
