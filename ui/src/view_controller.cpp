/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <ui/view_controller.hpp>
#include <ui/view.hpp>
#include <ui/painter.hpp>
#include <ui/context.hpp>

namespace modus::ui {

class ViewAnimation final : public Animation {
   public:
    Offset from;
    Offset to;
    Offset current;

    void animate(const f64 interval) override {
        Offset delta = to - from;
        delta.x = f32(f64(delta.x) * interval);
        delta.y = f32(f64(delta.y) * interval);
        current = from + delta;
    }
};

ViewController::ViewController(ContextPtr context)
    : m_views(), m_animator(context), m_animation(context->alloc<ViewAnimation>()) {
    m_animator.set_duration_ms(IMilisec(250));
    m_operations.reserve(4);
}

ViewController::~ViewController() {
    modus_assert_message(m_views.empty(), "Not all views have been popped!");
}

void ViewController::push_view(const ViewPtr& view) {
    modus_assert(view);
    m_operations.push_back({view, PendingOperationType::Push});
}

void ViewController::push_view_animated(const ViewPtr& view) {
    modus_assert(view);
    m_operations.push_back({view, PendingOperationType::PushAnimated});
}

void ViewController::pop_view() {
    ViewPtr view = top_view();
    if (!view) {
        return;
    }
    m_operations.push_back({nullptr, PendingOperationType::Pop});
}

void ViewController::pop_view_animated() {
    ViewPtr view = top_view();
    if (!view) {
        return;
    }
    m_operations.push_back({nullptr, PendingOperationType::PopAnimated});
}

void ViewController::pop_to_root() {
    if (m_views.size() > 1) {
        m_operations.push_back({nullptr, PendingOperationType::PopToRoot});
    }
}

ViewController::ViewPtr ViewController::top_view() const {
    return !m_views.empty() ? m_views.back() : ViewPtr();
}

void ViewController::clear() {
    m_operations.clear();
    while (!m_views.empty()) {
        do_pop();
    }
}

void ViewController::paint(Painter& painter) const {
    PaintParams params;
    for (auto& view : m_views) {
        if (is_animating() && view == m_views.back()) {
            params.offset = m_animation->current;
        } else {
            params.offset = Offset{0, 0};
        }
        view->paint(painter, params);
        // TODO: Find a better way to generate higher zindices
        params.z_index += 1000;
    }
}

bool ViewController::on_event(const Event& evt) {
    if (is_animating()) {
        return false;
    }
    if (!m_views.empty()) {
        auto& top = m_views.back();
        return top->on_event(evt, Offset{0, 0});
    }
    return false;
}

void ViewController::eval_constraints() {
    for (auto& view : m_views) {
        view->eval_constraints();
    }
}

void ViewController::do_pop() {
    if (!m_views.empty()) {
        m_views.back()->on_exit();
        m_views.erase(m_views.begin() + (m_views.size() - 1));
    }
    if (!m_views.empty()) {
        m_views.back()->on_enter();
    }
}

void ViewController::update() {
    bool animate = false;
    const usize pending_op_count = m_operations.size();
    if (is_animating()) {
        return;
    }
    usize i = 0;
    for (; i < pending_op_count; ++i) {
        PendingOperation operation = m_operations[i];
        switch (operation.type) {
            case PendingOperationType::PopAnimated: {
                modus_assert(!m_views.empty());
                const Size drawable_size = m_views.back()->context()->drawable_size();
                m_animation->to = Offset{drawable_size.width, 0};
                m_animation->from = Offset{0, 0};
                m_animator.m_on_finished
                    .bind<ViewController, &ViewController::on_animated_pop_finish>(this);
                m_animator.start(m_animation, false);
                i++;
                goto exit;
                break;
            }
            case PendingOperationType::Pop: {
                do_pop();
                break;
            }
            case PendingOperationType::PopToRoot: {
                while (m_views.size() > 1) {
                    do_pop();
                }
                break;
            }
            case PendingOperationType::PushAnimated: {
                animate = true;
                [[fallthrough]];
            }
            case PendingOperationType::Push: {
                modus_assert(operation.view);
                if (!m_views.empty()) {
                    m_views.back()->on_exit();
                }
                m_views.push_back(operation.view);
                const Size drawable_size = operation.view->context()->drawable_size();
                operation.view->set_size(drawable_size);
                operation.view->on_enter();
                if (animate) {
                    m_animation->from = Offset{drawable_size.width, 0};
                    m_animation->to = Offset{0, 0};
                    m_animator.m_on_finished
                        .bind<ViewController, &ViewController::on_animated_push_finish>(this);
                    m_animator.start(m_animation, false);
                    ++i;
                    goto exit;
                }
                break;
            }
            default:
                break;
        }
    }
exit:
    m_operations.erase(m_operations.begin(), m_operations.begin() + i);
}    // namespace modus::engine

void ViewController::on_animated_push_finish(Animator& animator) {
    animator.m_on_finished.reset();
}

void ViewController::on_animated_pop_finish(Animator& animator) {
    animator.m_on_finished.reset();
    do_pop();
}

bool ViewController::is_animating() const {
    return m_animator.is_animating();
}
}    // namespace modus::ui
