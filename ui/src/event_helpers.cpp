/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <ui/event_helpers.hpp>
#include <ui/event.hpp>

namespace modus::ui {

bool MouseClickHelper::handle_event(const Event& event, const Rect& rect) {
    switch (event.type) {
        case ui::EventType::MouseMove: {
            if (rect.contains(event.mouse_move.x, event.mouse_move.y)) {
                if (m_state < State::Hover) {
                    m_state = State::Hover;
                    if (m_on_mouse_enter) {
                        m_on_mouse_enter();
                    }
                }
            } else if (m_state >= State::Hover) {
                m_state = State::Default;
                if (m_on_mouse_leave) {
                    m_on_mouse_leave();
                }
            }
            return false;
        }
        case ui::EventType::MouseButton: {
            if (rect.contains(event.mouse_button.x, event.mouse_button.y)) {
                if (event.mouse_button.is_down) {
                    m_state = State::ButtonDown;
                    if (m_on_mouse_down) {
                        m_on_mouse_down();
                    }
                } else if (m_state == State::ButtonDown) {
                    if (m_on_mouse_up) {
                        m_on_mouse_up();
                    }
                    m_state = State::Hover;
                }
                return true;
            } else {
                if (m_state == State::Hover && m_on_mouse_leave) {
                    m_on_mouse_leave();
                }
                m_state = State::Default;
                return false;
            }
        }
        case ui::EventType::ContextMouseLeave: {
            if (m_state != State::Default) {
                m_state = State::Default;
                if (m_on_mouse_leave) {
                    m_on_mouse_leave();
                }
            }
            return false;
        }
        default:
            return false;
    }
}
}    // namespace modus::ui
