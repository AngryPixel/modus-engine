/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <threed/threed_pch.h>
// clang-format on

#include <ui/painters/threed/threed_painter.hpp>
#include <ui/text/font.hpp>
#include <threed/device.hpp>
#include <threed/pipeline.hpp>
#include <threed/compositor.hpp>
#include <threed/device.hpp>
#include <threed/buffer.hpp>
#include <threed/drawable.hpp>
#include <threed/program.hpp>
#include <threed/sampler.hpp>
#include <threed/effect.hpp>
#include <threed/texture.hpp>
#include <ui/context.hpp>

namespace modus::ui {

static const char* kVertShader = R"R(
#version 310 es
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif
layout(location = 0) in vec2 POS;
layout(location = 1) in vec2 UV;
layout(location = 2) in vec4 COLOR;
layout(location = 0) uniform mat4 u_proj;
out vec2 var_uv;
out vec4 var_color;
void main() {
    var_uv = UV;
    var_color = COLOR;
    gl_Position = u_proj * vec4(POS.xy, 0.0, 1.0);
}
)R";

static const char* kFragShader = R"R(
#version 310 es
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif
in vec2 var_uv;
in vec4 var_color;
layout(location = 0) out vec4 out_color;
layout(binding = 0) uniform sampler2D u_sampler;
layout(location = 1) uniform int u_is_text;
void main() {
    vec4 sampler_color = texture(u_sampler, var_uv);
    if (u_is_text == 1) {
        sampler_color = vec4(1.0, 1.0, 1.0, sampler_color.r);
    }
    out_color = var_color * sampler_color;
}
)R";

struct TriBuffer {
    threed::BufferHandle m_data_buffer;
    threed::BufferHandle m_index_buffer;
    threed::DrawableHandle m_drawable;
};
constexpr u32 kProjConstantLoc = 0;
constexpr u32 kIsFontConstantLoc = 1;
constexpr u32 kSamplerLoc = 0;

Result<TriBuffer> create_buffer(threed::Device& device) {
    TriBuffer result;

    {
        threed::BufferCreateParams params;
        params.type = threed::BufferType::Data;
        params.data = nullptr;
        params.size = sizeof(PainterVertexData) * kPainterVertexBufferSize;
        params.usage = threed::BufferUsage::Stream;

        auto r = device.create_buffer(params);
        if (!r) {
            goto failure;
        }
        result.m_data_buffer = *r;
    }

    {
        threed::BufferCreateParams params;
        params.type = threed::BufferType::Indices;
        params.data = nullptr;
        params.size = sizeof(PainterIndexType) * kPainterIndexBufferSize;
        params.usage = threed::BufferUsage::Stream;

        auto r = device.create_buffer(params);
        if (!r) {
            goto failure;
        }
        result.m_index_buffer = *r;
    }

    {
        threed::DrawableCreateParams params;
        params.count = 0;
        params.start = 0;
        params.primitive = threed::Primitive::Triangles;
        params.index_type = threed::IndicesType::U16;
        params.index_buffer.buffer = result.m_index_buffer;
        params.index_buffer.offset = 0;
        params.data_buffers[0].buffer = result.m_data_buffer;
        params.data_buffers[0].stride = sizeof(PainterVertexData);
        params.data_buffers[0].offset = 0;

        {
            threed::DrawableDataParams& data_param = params.data[0];
            data_param.data_type = threed::DataType::Vec2F32;
            data_param.offset = 0;
            data_param.normalized = false;
            data_param.buffer_index = 0;
        }
        {
            threed::DrawableDataParams& data_param = params.data[1];
            data_param.data_type = threed::DataType::Vec2U16;
            data_param.offset = sizeof(glm::vec2);
            data_param.normalized = true;
            data_param.buffer_index = 0;
        }
        {
            threed::DrawableDataParams& data_param = params.data[2];
            data_param.data_type = threed::DataType::Vec4U8;
            data_param.offset = sizeof(glm::vec2) + sizeof(u32);
            data_param.normalized = true;
            data_param.buffer_index = 0;
        }

        auto r = device.create_drawable(params);
        if (!r) {
            goto failure;
        }
        result.m_drawable = *r;
    }
    return Ok(result);

failure:
    if (result.m_drawable) {
        device.destroy_drawable(result.m_drawable);
    }
    if (result.m_data_buffer) {
        device.destroy_buffer(result.m_data_buffer);
    }
    if (result.m_index_buffer) {
        device.destroy_buffer(result.m_index_buffer);
    }

    return Error<>();
}

ThreedPainterDeviceBase::ThreedPainterDeviceBase(NotMyPtr<threed::Device> device,
                                                 NotMyPtr<threed::Compositor> compositor)
    : m_device(device), m_compositor(compositor) {}

ThreedPainterDeviceBase::~ThreedPainterDeviceBase() {}

Result<> ThreedPainterDeviceBase::initialize(ContextPtr) {
    {
        threed::ProgramCreateParams params;
        params.vertex = StringSlice(kVertShader).as_bytes();
        params.fragment = StringSlice(kFragShader).as_bytes();

        threed::ProgramConstantInputParam constant;
        constant.name = "u_proj";
        constant.data_type = threed::DataType::Mat4F32;

        threed::ProgramConstantInputParam constant_is_font;
        constant_is_font.name = "u_is_text";
        constant_is_font.data_type = threed::DataType::I32;

        threed::ProgramSamplerInputParam sampler;
        sampler.name = "u_sampler";
        params.samplers.push_back(sampler).expect("Failed to add sampler");
        params.constants.push_back(constant).expect("Failed to add constant");
        params.constants.push_back(constant_is_font).expect("Failed to add constant");

        auto r = m_device->create_program(params);
        if (!r) {
            return Error<>();
        }
        m_program = *r;
    }

    {
        threed::EffectCreateParams params;
        params.state.depth_stencil.depth.enabled = false;
        params.state.depth_stencil.stencil.enabled = false;
        params.state.blend.targets[0].enabled = true;
        params.state.blend.targets[0].eq_source = threed::BlendEquation::SourceAlpha;
        params.state.blend.targets[0].eq_destination = threed::BlendEquation::OneMinusSourceAlpha;
        params.state.raster.cull_enabled = true;

        if (auto r = m_device->create_effect(params); !r) {
            return Error<>();
        } else {
            m_effect = *r;
        }

        threed::SamplerCreateParams sampler_params;
        sampler_params.filter_mag = threed::SamplerFilter::Linear;
        sampler_params.filter_min = threed::SamplerFilter::Linear;
        sampler_params.wrap_r = threed::SamplerWrapMode::ClampToEdge;
        sampler_params.wrap_s = threed::SamplerWrapMode::ClampToEdge;
        sampler_params.wrap_t = threed::SamplerWrapMode::ClampToEdge;

        if (auto r = m_device->create_sampler(sampler_params); !r) {
            return Error<>();
        } else {
            m_sampler = *r;
        }
    }

    {
        threed::TextureCreateParams params;
        params.type = threed::TextureType::T2D;
        params.format = threed::TextureFormat::R8G8B8A8;
        params.width = 2;
        params.height = 2;
        params.mip_map_levels = 1;
        params.depth = 1;

        threed::TextureData tex_data;
        tex_data.width = 2;
        tex_data.height = 2;
        tex_data.mip_map_level = 0;
        tex_data.unpack_alignment = 1;

        const u8 data[] = {
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
        };

        tex_data.data = ByteSlice(data);

        auto r = m_device->create_texture(params, Slice(&tex_data, 1));
        if (!r) {
            return Error<>();
        }
        m_default_texture = *r;
    }

    auto r_buffer = create_buffer(*m_device);
    if (!r_buffer) {
        return Error<>();
    }
    m_allocated_buffers.push_back(*r_buffer);
    m_current_buffer_index = 0;
    return Ok<>();
}

void ThreedPainterDeviceBase::shutdown(ContextPtr) {
    if (m_default_texture.is_valid()) {
        m_device->destroy_texture(m_default_texture);
    }
    if (m_effect.is_valid()) {
        m_device->destroy_effect(m_effect);
    }
    if (m_program.is_valid()) {
        m_device->destroy_program(m_program);
    }

    if (m_sampler) {
        m_device->destroy_sampler(m_sampler);
    }

    for (auto& buffer : m_allocated_buffers) {
        m_device->destroy_drawable(buffer.m_drawable);
        m_device->destroy_buffer(buffer.m_data_buffer);
        m_device->destroy_buffer(buffer.m_index_buffer);
    }
}

class ThreedBitmap2D final : public PainterBitmap2D {
   public:
    threed::TextureHandle handle;
    NotMyPtr<threed::Device> device;

    ThreedBitmap2D(threed::TextureHandle h, NotMyPtr<threed::Device> d) : handle(h), device(d) {
        modus_assert(device);
    }

    ~ThreedBitmap2D() { device->destroy_texture(handle); }
};

Result<UIRefPtr<PainterBitmap2D>> ThreedPainterDeviceBase::create_bitmap2d(
    ContextPtr context,
    const PainterBitmap2DCreateParams& params) {
    threed::TextureFormat tex_format = threed::TextureFormat::Total;

    switch (params.format) {
        case PainterBitmapFormat::R8:
            tex_format = threed::TextureFormat::R8;
            break;
        case PainterBitmapFormat::RGB8:
            tex_format = threed::TextureFormat::R8G8B8;
            break;
        case PainterBitmapFormat::RGBA8:
            tex_format = threed::TextureFormat::R8G8B8A8;
            break;
        default:
            modus_assert_message(false, "Unsupported/unhandled texture type");
            return Error<>();
    }
    threed::TextureCreateParams tex_params;
    tex_params.type = threed::TextureType::T2D;
    tex_params.format = tex_format;
    tex_params.depth = 1;
    tex_params.width = params.width;
    tex_params.height = params.height;
    tex_params.mip_map_levels = 1;

    threed::TextureData tex_data;
    tex_data.width = params.width;
    tex_data.height = params.height;
    tex_data.data = Slice<u8>(reinterpret_cast<const u8*>(params.data), params.data_size);

    auto r = m_device->create_texture(tex_params, Slice(&tex_data, 1));
    if (!r) {
        return Error<>();
    }
    return Ok<UIRefPtr<PainterBitmap2D>>(context->alloc<ThreedBitmap2D>(*r, m_device));
}

void ThreedPainterDeviceBase::execute_commands(Slice<UIRefPtr<PainterBuffer>> buffers,
                                               Slice<PainterCommand> commands) {
    // Upload Last buffer to GPU
    for (u32 i = 0; i < buffers.size(); i++) {
        TriBuffer& buffer = create_or_get_buffer(i);
        if (!m_device->update_buffer(buffer.m_data_buffer,
                                     make_slice(buffers[i]->vertices).as_bytes(), 0, true)) {
            modus_assert_message(false, "failed to update data buffer");
        }
        if (!m_device->update_buffer(buffer.m_index_buffer,
                                     make_slice(buffers[i]->indices).as_bytes(), 0, true)) {
            modus_assert_message(false, "failed to update index buffer");
        }
    }

    // Create projection matrix
    glm::mat4 proj_matrix = glm::ortho(0.f, f32(m_pass->viewport.width),
                                       f32(m_pass->viewport.height), 0.f, -1.0f, 1.0f);
    auto proj_constant = m_device->upload_constant(proj_matrix);

    threed::ConstantHandle is_font_true_constant = m_device->upload_constant(i32(1));
    threed::ConstantHandle is_font_false_constant = m_device->upload_constant(i32(0));

    for (const auto& cmd : commands) {
        threed::Command* tcmd = m_device->allocate_command(*m_pass);
        modus_assert(cmd.buffer_index < m_allocated_buffers.size());
        const TriBuffer& cmd_buffer = m_allocated_buffers[cmd.buffer_index];
        tcmd->effect = m_effect;
        tcmd->program = m_program;
        tcmd->drawable = cmd_buffer.m_drawable;
        tcmd->draw.type = threed::DrawType::CustomRange;
        tcmd->draw.range_start = cmd.draw_offset;
        tcmd->draw.range_count = cmd.draw_count;

        if (cmd.scissor_rect) {
            const Rect scissor_rect = cmd.scissor_rect.value();
            tcmd->scissor.x = u32(std::round(scissor_rect.x));
            tcmd->scissor.y = u32(std::round(scissor_rect.y));
            tcmd->scissor.width = u32(std::round(scissor_rect.width));
            tcmd->scissor.height = u32(std::round(scissor_rect.height));
            tcmd->scissor.enabled = true;
        } else {
            tcmd->scissor.enabled = false;
        }

        threed::TextureHandle tex_handle = m_default_texture;
        if (cmd.bitmap) {
            tex_handle = static_cast<ThreedBitmap2D*>(cmd.bitmap.get())->handle;
        }
        tcmd->inputs.samplers[kSamplerLoc] = {tex_handle, m_sampler};
        tcmd->inputs.constants[kIsFontConstantLoc] =
            tex_handle == m_default_texture ? is_font_false_constant : is_font_true_constant;
        tcmd->inputs.constants[kProjConstantLoc] = proj_constant;
    }

    // reset values
    m_current_buffer_index = 0;
}

TriBuffer& ThreedPainterDeviceBase::create_or_get_buffer(const u32 index) {
    if (index >= m_allocated_buffers.size()) {
        auto r = create_buffer(*m_device);
        modus_panic("Failed to create new device buffer");
        m_allocated_buffers.push_back(*r);
    }
    return m_allocated_buffers[index];
}

ThreedPainterDeviceStandalone::ThreedPainterDeviceStandalone(
    NotMyPtr<threed::Device> device,
    NotMyPtr<threed::Compositor> compositor)
    : ThreedPainterDeviceBase(device, compositor) {}
void ThreedPainterDeviceStandalone::begin_frame(const bool clear) {
    if (m_compositor->requires_update()) {
        if (!m_compositor->update(*m_device)) {
            return;
        }
    }
    m_device->begin_frame();
    m_pipeline = m_device->allocate_pipeline("ThreedPainterDeviceBase");

    auto pass = m_device->allocate_pass(*m_pipeline, "ui_pass");
    pass->viewport.x = 0;
    pass->viewport.y = 0;
    pass->viewport.width = m_compositor->width();
    pass->viewport.height = m_compositor->height();
    pass->frame_buffer = m_compositor->frame_buffer();
    if (clear) {
        pass->state.clear.colour[0].clear = true;
        pass->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.f);
    }
    m_pass = pass;
}

void ThreedPainterDeviceStandalone::end_frame() {
    m_device->execute_pipeline(*m_pipeline);
    m_device->end_frame();
    m_compositor->swap_buffers();
    m_pipeline.reset();
    m_current_buffer_index = 0;
}

ThreedPainterDeviceEmbedded::ThreedPainterDeviceEmbedded(NotMyPtr<threed::Device> device,
                                                         NotMyPtr<threed::Compositor> compositor)
    : ThreedPainterDeviceBase(device, compositor) {}

void ThreedPainterDeviceEmbedded::set_pipeline(NotMyPtr<threed::Pipeline> pipeline) {
    modus_assert(pipeline);
    m_pipeline = pipeline;
}

void ThreedPainterDeviceEmbedded::begin_frame(const bool clear) {
    modus_assert(m_pipeline);
    auto pass = m_device->allocate_pass(*m_pipeline, "ui_pass");
    pass->viewport.x = 0;
    pass->viewport.y = 0;
    pass->viewport.width = m_compositor->width();
    pass->viewport.height = m_compositor->height();
    pass->frame_buffer = m_compositor->frame_buffer();
    if (clear) {
        pass->state.clear.colour[0].clear = true;
        pass->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.f);
    }
    m_pass = pass;
}

void ThreedPainterDeviceEmbedded::end_frame() {
    m_current_buffer_index = 0;
}
}    // namespace modus::ui
