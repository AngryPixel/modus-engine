/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <ui/painters/null_painter.hpp>
#include <ui/context.hpp>

namespace modus::ui {

class NullPainterDeviceBitmap2D final : public PainterBitmap2D {};

NullPainterDevice::NullPainterDevice() {}

void NullPainterDevice::begin_frame(const bool) {}

void NullPainterDevice::end_frame() {}

Result<UIRefPtr<PainterBitmap2D>> NullPainterDevice::create_bitmap2d(
    ContextPtr context,
    const PainterBitmap2DCreateParams&) {
    return Ok<UIRefPtr<PainterBitmap2D>>(context->alloc<NullPainterDeviceBitmap2D>());
}

void NullPainterDevice::execute_commands(Slice<UIRefPtr<PainterBuffer>>, Slice<PainterCommand>) {}

}    // namespace modus::ui
