/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format off

#include <ui/tickable.hpp>

namespace modus::ui {

    constexpr const u32 kEntriesPerBlock = 32;
    TickableManager::TickableManager():
        m_tickables(kEntriesPerBlock) {
    }

    TickableHandle TickableManager::create(TickableDelegate delegate) {
        modus_assert(delegate);
        return m_tickables.create(delegate).value_or_panic("Failed to create tickable").first;
    }

    void TickableManager::destroy(TickableHandle handle) {
        m_tickables.erase(handle).expect("Failed to destroy tickable");
    }

    void TickableManager::update(const IMilisec tick_ms) {
        m_tickables.for_each([elapsed_ms = tick_ms](Tickable& t){
            t.m_on_tick(elapsed_ms);
        });
    }
}
