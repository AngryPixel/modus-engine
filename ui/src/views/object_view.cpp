/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/views/object_view.hpp>
#include <ui/event.hpp>
#include <ui/context.hpp>

namespace modus::ui {

ObjectView::ObjectView(ContextPtr ctx, const UIRefPtr<Object>& object)
    : View(ctx), m_object(object) {
    set_size(ctx->drawable_size());
}

void ObjectView::eval_constraints(const ConstraintParams& params) {
    m_size = m_constraint.resolve(params);
    if (m_object) {
        ConstraintParams child_params{m_size, m_size};
        m_object->eval_constraints(child_params);
    }
}

void ObjectView::paint(Painter& painter, const PaintParams& params) const {
    if (m_object) {
        m_object->paint(painter, params);
    }
}

bool ObjectView::on_event(const Event& event, const Offset offset) {
    if (event.type == EventType::ContextResize) {
        m_size.width = event.context_resize.width;
        m_size.height = event.context_resize.height;
        return false;
    }

    if (m_object) {
        return m_object->on_event(event, offset);
    }
    return false;
}

void ObjectView::set_object(const UIRefPtr<Object>& object) {
    m_object = object;
}

}    // namespace modus::ui
