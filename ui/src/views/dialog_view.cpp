/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/views/dialog_view.hpp>
#include <ui/event.hpp>
#include <ui/context.hpp>

namespace modus::ui {

DialogView::DialogView(ContextPtr ctx, const UIRefPtr<Object>& object)
    : View(ctx), m_object(object) {
    set_size(ctx->drawable_size());
    m_color = make_color(0, 0, 0, 0);
}

void DialogView::eval_constraints(const ConstraintParams& params) {
    m_size = m_constraint.resolve(params);
    if (m_object) {
        ConstraintParams child_params;
        child_params.parent_size = {m_size.width / 2, m_size.height / 2};
        child_params.flex_factor = child_params.parent_size;
        m_object->eval_constraints(child_params);
    }
}

void DialogView::paint(Painter& painter, const PaintParams& params) const {
    PaintParams local_params = params;
    if (m_color.v.a > 0) {
        local_params = params.new_scope();
        painter.draw_rect(make_rect(local_params.offset, m_size), local_params.z_index, m_color);
    }

    if (m_object) {
        PaintParams object_params = local_params;
        const Offset object_offset = calculate_object_offset();
        ScissorScope scissor_scope(painter, make_rect(object_offset, m_object->size()));
        object_params.offset = object_params.offset + object_offset;
        m_object->paint(painter, object_params);
    }
}

bool DialogView::on_event(const Event& event, const Offset offset) {
    if (event.type == EventType::ContextResize) {
        m_size.width = event.context_resize.width;
        m_size.height = event.context_resize.height;
        return false;
    }

    if (m_object) {
        const Offset object_offset = offset + calculate_object_offset();
        return m_object->on_event(event, object_offset);
    }
    return false;
}

void DialogView::set_object(const UIRefPtr<Object>& object) {
    m_object = object;
}

Offset DialogView::calculate_object_offset() const {
    const Size object_size = m_object->size();
    const Offset margin_offset{m_size.width / 4, m_size.height / 4};
    const Offset center_offset{((m_size.width / 2) - object_size.width) / 2,
                               ((m_size.height / 2) - object_size.height) / 2};
    return margin_offset + center_offset;
}

}    // namespace modus::ui
