/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <thread>
#include <ui/objects/combo_box.hpp>
#include <ui/objects/button.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/event.hpp>
#include <ui/views/dialog_view.hpp>
#include <ui/layouts/vscroll_layout.hpp>
#include <ui/layouts/vbox_layout.hpp>
#include <ui/text/font.hpp>

namespace modus::ui {

class ComboBoxButton final : public Button {
   public:
    const u32 m_index;
    ComboBoxButton(ContextPtr context, u32 index) : Button(context), m_index(index) {}
};

class ComboBoxDialog final : public DialogView {
   private:
    ComboBox& m_combo_box;
    UIRefPtr<VScrollLayout> m_vscroll_layout;
    UIRefPtr<VBoxLayout> m_vbox_layout;
    Optional<u32> m_selected_index;

   public:
    ComboBoxDialog(ContextPtr context, ComboBox& combo_box)
        : DialogView(context), m_combo_box(combo_box) {
        m_vbox_layout = context->create_object<VBoxLayout>();
        m_vscroll_layout = context->create_object<VScrollLayout>();
        m_vscroll_layout->constraint().max_width = IncomingValue();
        m_vscroll_layout->constraint().max_height = IncomingValue();
        m_vbox_layout->constraint().max_width = IncomingValue();
        m_vbox_layout->constraint().max_height = ConstantValue(kInfiniteValue);
        m_vscroll_layout->set_child(m_vbox_layout);
        set_object(m_vscroll_layout);

        set_background_color(make_color(0, 0, 0, 200));
    }

    void on_enter() override {
        m_vbox_layout->clear();
        const auto& data = m_combo_box.data();
        const u32 child_count = data->item_count();
        for (u32 i = 0; i < child_count; ++i) {
            m_vbox_layout->add_child(create_button(data->item_display(i), i));
        }
    }

    void on_exit() override {
        if (m_selected_index && m_selected_index.value() != m_combo_box.selected_index()) {
            m_combo_box.set_selected_index(m_selected_index.value());
        }
    }

   private:
    void on_clicked(Button& button) {
        ComboBoxButton& cbbtn = static_cast<ComboBoxButton&>(button);
        m_selected_index = cbbtn.m_index;
        context()->view_controller().pop_view();
    }

    UIRefPtr<ComboBoxButton> create_button(const StringSlice text, const u32 index) {
        const ComboBoxStyle& cb_style = m_combo_box.style();
        UIRefPtr<ComboBoxButton> btn = context()->create_object<ComboBoxButton>(index);
        btn->set_text(text);
        btn->constraint().max_width = IncomingValue();
        btn->constraint().max_height = ConstantValue(cb_style.selection_item_height);
        ButtonStyle bt_style;
        if (index != m_combo_box.selected_index()) {
            bt_style.color = cb_style.color;
            bt_style.label_color = cb_style.label_color;
        } else {
            bt_style.color = cb_style.color_hover;
            bt_style.label_color = cb_style.label_color_hover;
        }
        bt_style.font = cb_style.font;
        bt_style.color_hover = cb_style.color_hover;
        bt_style.color_down = cb_style.color_hover;
        bt_style.label_color_hover = cb_style.label_color_hover;
        bt_style.label_color_down = cb_style.label_color_hover;
        btn->set_style(bt_style);
        btn->set_label_margin(cb_style.selection_item_margin);
        btn->m_on_clicked.bind<ComboBoxDialog, &ComboBoxDialog::on_clicked>(this);
        return btn;
    }
};

ComboBox::ComboBox(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_label(ctx), m_style(), m_data(), m_selected_index(0) {
    m_style.font = ctx->default_font();
    m_style.color = make_color(0, 0, 0);
    m_style.color_hover = make_color(255, 255, 255);
    m_style.label_color = make_color(255, 255, 255);
    m_style.label_color_hover = make_color(0, 0, 0);
    m_style.selection_item_height = 30;
    m_style.selection_item_margin = 2;
    constraint().min_height = ConstantValue(20.f);
    constraint().min_width = ConstantValue(20.f);
    constraint().max_height = ConstantValue(20.f);
    constraint().max_width = ConstantValue(20.f);
    m_helper.m_on_mouse_up.bind<ComboBox, &ComboBox::on_mouse_click>(this);
    m_helper.m_on_mouse_enter.bind<ComboBox, &ComboBox::on_mouse_enter>(this);
    m_helper.m_on_mouse_leave.bind<ComboBox, &ComboBox::on_mouse_leave>(this);
    m_color = m_style.color;
    m_label.set_color(m_style.label_color);
    m_label.set_font(m_style.font);
    m_label.set_vertical_alignment(VerticalAlignment::Center);
    m_label.set_horizontal_alignment(HorizontalAlignment::Center);
    m_dialog = ctx->create_object<ComboBoxDialog>(*this);
}

ComboBox::~ComboBox() {}

void ComboBox::set_style(const ComboBoxStyle& style) {
    m_style = style;
}

void ComboBox::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
    ConstraintParams label_params;
    label_params.parent_size = m_size;
    label_params.flex_factor = label_params.parent_size;
    m_label.eval_constraints(label_params);
}

bool ComboBox::on_event(const Event& event, const Offset offset) {
    const Rect rect = make_rect(offset, m_size);
    return m_helper.handle_event(event, rect);
}

void ComboBox::paint(Painter& painter, const PaintParams& params) const {
    // Draw border
    PaintParams local_params = params.new_scope();
    painter.draw_rect(make_rect(local_params.offset, m_size), local_params.z_index, m_color);
    if (m_data && m_data->item_count() != 0) {
        PaintParams label_params = local_params.new_scope();
        m_label.paint(painter, label_params);
    }
}

void ComboBox::set_selected_index(const u32 index) {
    if (m_data && index < m_data->item_count()) {
        m_selected_index = index;
        m_label.set_text(m_data->item_display(index));
        if (m_on_selected) {
            m_on_selected(*this);
        }
    }
}

void ComboBox::set_data(UIRefPtr<ComboBoxData> data) {
    m_data = data;
    m_selected_index = 0;
    if (m_data) {
        m_label.set_text(data->item_display(0));
    }
}

void ComboBox::on_mouse_click() {
    if (m_data) {
        context()->view_controller().push_view(m_dialog);
    }
}

void ComboBox::on_mouse_enter() {
    m_label.set_color(m_style.label_color_hover);
    m_color = m_style.color_hover;
}

void ComboBox::on_mouse_leave() {
    m_label.set_color(m_style.label_color);
    m_color = m_style.color;
}

}    // namespace modus::ui
