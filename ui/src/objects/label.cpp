/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/objects/label.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/text/font.hpp>
#include <ui/text/text_utils.hpp>

namespace modus::ui {
Label::Label(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent),
      m_text(),
      m_text_size({0.f, 0.f}),
      m_halignment(HorizontalAlignment::Left),
      m_valignment(VerticalAlignment::Center) {
    m_font = ctx->default_font();
}

Label::~Label() {}

void Label::set_text(const StringSlice text) {
    m_text.clear();
    m_text.append(text.data(), text.size());
    update_text_size();
}

void Label::set_font(const UIRefPtr<Font>& font) {
    m_font = font;
    update_text_size();
}

void Label::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
}

void Label::paint(Painter& painter, const PaintParams& params) const {
    if (!m_font) {
        return;
    }
    const f32 text_width = std::min(m_size.width, m_text_size.width);
    const f32 text_height = std::min(m_size.height, m_text_size.height);

    TextVertexDataGenerator generator;
    generator.max_text_width = text_width;
    generator.max_text_height = text_height;
    generator.allow_multiline = false;

    Rect rect = make_rect(params.offset, {text_width, text_height});
    rect.x += get_alignment_offset(m_halignment, text_width, m_size.width);
    rect.y += get_alignment_offset(m_valignment, text_height, m_size.height);
    ui::PaintTextParams paint_params{generator, *m_font, rect, m_color};
    painter.draw_text(paint_params, m_text);
    // painter.draw_rect(make_rect(params.offset, m_size), params.z_index + 1,
    // make_color(128,128,128));
}

void Label::update_text_size() {
    if (m_font) {
        if (m_text.empty()) {
            m_text_size = {0.f, 0.f};
            return;
        }
        TextSizeCaculator calculator;
        calculator.allow_multiline = false;
        m_text_size = calculator.text_size(*m_font, m_text);
    }
}
}    // namespace modus::ui
