/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <thread>
#include <ui/objects/check_box.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/event.hpp>

namespace modus::ui {

CheckBox::CheckBox(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_style(), m_checked(false) {
    m_style.border_color = make_color(255, 255, 255);
    m_style.center_color = make_color(255, 255, 255);
    m_style.center_margin = 2.0f;
    m_style.border_thickness = 2.0f;
    constraint().min_height = ConstantValue(20.f);
    constraint().min_width = ConstantValue(20.f);
    constraint().max_height = ConstantValue(20.f);
    constraint().max_width = ConstantValue(20.f);
    m_helper.m_on_mouse_up.bind<CheckBox, &CheckBox::on_mouse_click>(this);
}

CheckBox::~CheckBox() {}

void CheckBox::set_checked(const bool value) {
    if (m_checked != value) {
        m_checked = value;
        if (m_on_clicked) {
            m_on_clicked(*this);
        }
    }
}

void CheckBox::set_style(const CheckBoxStyle& style) {
    m_style = style;
    modus_assert(style.border_thickness >= 0.f);
    modus_assert(style.center_margin >= 0.f);
}

bool CheckBox::on_event(const Event& event, const Offset offset) {
    const Rect rect = make_rect(offset, m_size);
    return m_helper.handle_event(event, rect);
}

void CheckBox::paint(Painter& painter, const PaintParams& params) const {
    // Draw border
    PaintParams local_params = params.new_scope();
    painter.draw_border(make_rect(local_params.offset, m_size), local_params.z_index,
                        m_style.border_color, m_style.border_thickness);

    if (m_checked) {
        // Draw center
        Rect center_rect;
        modus_assert(m_style.border_thickness + m_style.center_margin * 2 < m_size.width);
        modus_assert(m_style.border_thickness + m_style.center_margin * 2 < m_size.height);

        const f32 center_offset = m_style.border_thickness + m_style.center_margin;
        center_rect.x = local_params.offset.x + center_offset;
        center_rect.y = local_params.offset.y + center_offset;
        center_rect.width = m_size.width - center_offset * 2.f;
        center_rect.height = m_size.height - center_offset * 2.f;
        painter.draw_rect(center_rect, local_params.z_index, m_style.center_color);
    }
}

void CheckBox::on_mouse_click() {
    m_checked = !m_checked;
    if (m_on_clicked) {
        m_on_clicked(*this);
    }
}

}    // namespace modus::ui
