/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/objects/null_object.hpp>

namespace modus::ui {

NullObject::NullObject(ContextPtr ctx, const UIWeakRefPtr<Object>& parent) : Object(ctx, parent) {}

void NullObject::paint(Painter&, const PaintParams&) const {}

void NullObject::force_size(const Size& size) {
    m_size.width = size.width;
    m_size.height = size.height;
}

}    // namespace modus::ui
