/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/objects/button.hpp>
#include <ui/objects/label.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/event.hpp>
#include <ui/text/font.hpp>

namespace modus::ui {

static inline Offset get_label_offset(const Offset offset, const f32 margin) {
    return Offset{offset.x + margin, offset.y + margin};
}

static inline Size get_label_size(const Size size, const f32 margin) {
    const f32 marginx2 = margin * 2.f;
    return Size{size.width - marginx2, size.height - marginx2};
}

Button::Button(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_label(ctx), m_label_margin(0) {
    m_style.color = make_color(0, 0, 0, 0);
    m_style.label_color = make_color(255, 255, 255);
    m_style.color_hover = make_color(255, 255, 255);
    m_style.label_color_hover = make_color(0, 0, 0);
    m_style.label_color_down = make_color(0, 0, 0);
    m_style.color_down = make_color(128, 128, 128);
    m_style.font = ctx->default_font();

    m_label.constraint().max_width = IncomingValue();
    m_label.constraint().max_height = IncomingValue();
    m_label.constraint().min_height = ConstantValue(20);
    m_label.constraint().min_width = ConstantValue(20);
    m_label.set_horizontal_alignment(ui::HorizontalAlignment::Center);
    m_label.set_vertical_alignment(ui::VerticalAlignment::Center);
    m_color = m_style.color;
    m_label.set_color(m_style.label_color);

    m_helper.m_on_mouse_enter.bind<Button, &Button::on_mouse_enter>(this);
    m_helper.m_on_mouse_leave.bind<Button, &Button::on_mouse_leave>(this);
    m_helper.m_on_mouse_down.bind<Button, &Button::on_mouse_down>(this);
    m_helper.m_on_mouse_up.bind<Button, &Button::on_mouse_up>(this);
}

Button::~Button() {}

void Button::set_text(const StringSlice text) {
    m_label.set_text(text);
}

void Button::set_style(const ButtonStyle& style) {
    modus_assert(style.font);
    m_style = style;
    m_label.set_font(style.font);
    switch (m_helper.m_state) {
        case MouseClickHelper::State::Default:
            m_label.set_color(m_style.label_color);
            m_color = m_style.color;
            break;
        case MouseClickHelper::State::Hover:
            m_label.set_color(m_style.label_color_hover);
            m_color = m_style.color_hover;
            break;
        case MouseClickHelper::State::ButtonDown:
            m_label.set_color(m_style.label_color_down);
            m_color = m_style.color_down;
            break;
    }
}

void Button::set_label_margin(const f32 margin) {
    modus_assert(margin >= 0.f);
    m_label_margin = margin;
}

void Button::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
    ConstraintParams label_params;
    label_params.parent_size = get_label_size(m_size, m_label_margin);
    label_params.flex_factor = label_params.parent_size;
    m_label.eval_constraints(label_params);
}

bool Button::on_event(const Event& event, const Offset offset) {
    Offset label_offset = get_label_offset(offset, m_label_margin);
    const Rect rect = make_rect(offset, m_size);
    if (!m_label.on_event(event, label_offset)) {
        return m_helper.handle_event(event, rect);
    }
    return false;
}

void Button::paint(Painter& painter, const PaintParams& params) const {
    // Draw background
    PaintParams local_params = params.new_scope();
    painter.draw_rect(make_rect(local_params.offset, m_size), local_params.z_index, m_color);

    // Center align label
    const Size label_size = get_label_size(m_size, m_label_margin);
    const Offset alignment_offset = {
        get_alignment_offset(HorizontalAlignment::Center, m_label.size().width, label_size.width),
        get_alignment_offset(VerticalAlignment::Center, m_label.size().height, label_size.height)};

    // Draw label
    PaintParams label_params = local_params.new_scope();
    label_params.offset = get_label_offset(label_params.offset, m_label_margin) + alignment_offset;
    m_label.paint(painter, label_params);
}

void Button::on_mouse_enter() {
    m_color = m_style.color_hover;
    m_label.set_color(m_style.label_color_hover);
}

void Button::on_mouse_leave() {
    m_color = m_style.color;
    m_label.set_color(m_style.label_color);
}

void Button::on_mouse_down() {
    m_color = m_style.color_down;
    m_label.set_color(m_style.label_color_down);
}

void Button::on_mouse_up() {
    m_color = m_style.color_hover;
    m_label.set_color(m_style.label_color_hover);
    if (m_on_clicked) {
        m_on_clicked(*this);
    }
}

class ButtonAnimation final : public Animation {
   public:
    AnimatedButton& m_button;

    ButtonAnimation(AnimatedButton& button) : m_button(button) {}

    void animate(const f64 interval) override {
        const Color from = m_button.m_style.color;
        const Color to = m_button.m_style.color_hover;
        const Color label_from = m_button.m_style.label_color;
        const Color label_to = m_button.m_style.label_color_hover;
        m_button.m_color = Color::interpolate(from, to, interval);
        m_button.m_label.set_color(Color::interpolate(label_from, label_to, interval));
    }
};

AnimatedButton::AnimatedButton(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Button(ctx, parent), m_animator(ctx) {
    m_animator.set_duration_ms(IMilisec(250));
    auto animation = ctx->alloc<ButtonAnimation>(*this);
    m_animator.set_animation(animation);
    m_helper.m_on_mouse_enter.bind<AnimatedButton, &AnimatedButton::on_mouse_enter>(this);
    m_helper.m_on_mouse_leave.bind<AnimatedButton, &AnimatedButton::on_mouse_leave>(this);
}

void AnimatedButton::set_animation_duration_ms(const IMilisec duration_ms) {
    m_animator.set_duration_ms(duration_ms);
}

void AnimatedButton::on_mouse_enter() {
    if (m_animator.is_animating()) {
        m_animator.set_reversed(false);
    } else {
        m_animator.start(false);
    }
    m_label.set_color(m_style.label_color_hover);
}

void AnimatedButton::on_mouse_leave() {
    if (m_animator.is_animating()) {
        m_animator.set_reversed(true);
    } else {
        m_animator.start(true);
    }
    m_label.set_color(m_style.label_color);
}

}    // namespace modus::ui
