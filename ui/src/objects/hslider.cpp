/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/objects/hslider.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/event.hpp>

namespace modus::ui {

HSlider::HSlider(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_slide_percentage(0.f), m_dragging(false) {
    constraint().min_height = ConstantValue(40.f);
    constraint().max_height = IncomingValue();
    constraint().min_width = ConstantValue(10.f);
    constraint().max_width = IncomingValue();

    m_style.border_thickness = 2.f;
    m_style.margin = 2.f;
    m_style.border_color = make_color(255, 255, 255);
    m_style.slider_color = make_color(255, 255, 255);
}

bool HSlider::on_event(const Event& event, const Offset offset) {
    if (!is_visible()) {
        return false;
    }
    const Rect slider_rect = make_rect(offset, m_size);
    switch (event.type) {
        case EventType::MouseButton: {
            if (!is_visible()) {
                return false;
            }
            if (event.mouse_button.is_down) {
                if (slider_rect.contains(event.mouse_button.x, event.mouse_button.y) &&
                    !m_dragging) {
                    m_dragging = true;
                    set_slide_percentage((f32(event.mouse_button.x) - slider_rect.x) /
                                         f32(m_size.width));
                    return true;
                };
            } else if (m_dragging) {
                m_dragging = false;
                return true;
            }
            return false;
        }
        case EventType::MouseMove: {
            if (m_dragging) {
                set_slide_percentage((f32(event.mouse_move.x) - slider_rect.x) / f32(m_size.width));
                return true;
            }
            return false;
        }
        case EventType::MouseScroll: {
            if (!m_dragging && slider_rect.contains(context()->mouse_location().x,
                                                    context()->mouse_location().y)) {
                set_slide_percentage(m_slide_percentage + (f32(event.mouse_scroll.y) * 0.1f));
                return true;
            }
            return false;
        }
        default:
            return false;
    }
}
void HSlider::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
}

void HSlider::paint(Painter& painter, const PaintParams& params) const {
    const PaintParams slider_params = params.new_scope();
    const Rect slider_rect = calculate_slider_rect(slider_params.offset);
    painter.draw_rect(slider_rect, slider_params.z_index, m_style.slider_color);

    const PaintParams local_params = slider_params.new_scope();
    painter.draw_border(make_rect(local_params.offset, m_size), local_params.z_index,
                        m_style.border_color, m_style.border_thickness);
}

void HSlider::set_slide_percentage(const f32 percentage) {
    m_slide_percentage = std::max(0.f, std::min(percentage, 1.0f));
    if (m_on_slide) {
        m_on_slide(*this);
    }
}

void HSlider::set_style(const SliderStyle& style) {
    m_style = style;
}

Rect HSlider::calculate_slider_rect(const Offset offset) const {
    modus_assert(m_size.width >= m_style.border_thickness * 2.f + m_style.margin * 2.f);
    modus_assert(m_size.height >= m_style.border_thickness * 2.f + m_style.margin * 2.f);
    return {
        offset.x + m_style.border_thickness + m_style.margin,
        offset.y + m_style.border_thickness + m_style.margin,
        (m_size.width * m_slide_percentage) -
            (m_style.border_thickness * 2.f + m_style.margin * 2.f),
        m_size.height - (m_style.border_thickness * 2.f) - (m_style.margin * 2.f),
    };
}

}    // namespace modus::ui
