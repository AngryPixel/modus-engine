/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/objects/text_box.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/text/font.hpp>
#include <ui/text/text_utils.hpp>

namespace modus::ui {

TextBox::TextBox(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent), m_text(), m_text_size({0.f, 0.f}) {
    m_style.background_color = make_color(255, 255, 255);
    m_style.text_color = make_color(0, 0, 0);
    m_style.text_margin = 10;
    m_style.font = ctx->default_font();
}

TextBox::~TextBox() {}

void TextBox::set_text(const StringSlice text) {
    m_text.clear();
    m_text.append(text.data(), text.size());
    update_text_size();
}

void TextBox::set_style(const TextBoxStyle& style) {
    m_style = style;
}

void TextBox::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
}

void TextBox::paint(Painter& painter, const PaintParams& params) const {
    if (!m_style.font) {
        return;
    }

    ScissorScope scissor_scope(painter, make_rect(params.offset, m_size));
    PaintParams box_params = params;
    if (m_style.background_color.v.a > 0) {
        box_params = params.new_scope();
        const Rect box_rect = make_rect(params.offset, size());
        painter.draw_rect(box_rect, box_params.z_index, m_style.background_color);
    }

    const f32 text_width = std::min(m_size.width - m_style.text_margin * 2.f, m_text_size.width);
    const f32 text_height = std::min(m_size.height - m_style.text_margin * 2.f, m_text_size.height);

    TextVertexDataGenerator generator;
    generator.max_text_width = text_width;
    generator.max_text_height = text_height;
    generator.allow_multiline = true;

    const Rect text_rect =
        make_rect(params.offset + Offset{m_style.text_margin, m_style.text_margin},
                  {text_width, text_height});
    ui::PaintTextParams paint_params{generator, *m_style.font, text_rect, m_style.text_color};
    painter.draw_text(paint_params, m_text);
}

void TextBox::update_text_size() {
    if (m_style.font) {
        if (m_text.empty()) {
            m_text_size = {0.f, 0.f};
            return;
        }
        TextSizeCaculator calculator;
        calculator.allow_multiline = true;
        m_text_size = calculator.text_size(*m_style.font, m_text);
    }
}
}    // namespace modus::ui
