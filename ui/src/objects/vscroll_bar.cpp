/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <ui/objects/vscroll_bar.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <ui/event.hpp>
#include <iostream>

namespace modus::ui {

VScrollBar::VScrollBar(ContextPtr ctx, const UIWeakRefPtr<Object>& parent)
    : Object(ctx, parent),
      m_scroll_percentage(0.f),
      m_area_percentage(1.f),
      m_bar_height(0.f),
      m_scroll_height(0.f),
      m_bar_offset_value(0.f),
      m_timer(ctx),
      m_dragging(false) {
    MODUS_UNUSED(m_scroll_percentage);
    MODUS_UNUSED(m_area_percentage);

    constraint().min_height = ConstantValue(40.f);
    constraint().max_height = IncomingValue();
    constraint().min_width = ConstantValue(20.f);
    constraint().max_width = IncomingValue(0.01f);

    m_style.margin_inner = 2.f;
    m_style.margin_outer = 2.f;
    m_style.color_overlay = make_color(0, 0, 0, 128);
    m_style.color_bar = make_color(255, 255, 255, 200);
    m_timer.set_duration_ms(ISeconds(1));
    m_timer.m_on_trigger.bind<VScrollBar, &VScrollBar::on_hide_timer_trigger>(this);
}

bool VScrollBar::on_event(const Event& event, const Offset offset) {
    if (!is_visible()) {
        return false;
    }
    const Offset bar_offset = {0.f, m_bar_offset_value};
    const Rect bar_rect = calculate_bar_rect(offset + bar_offset);
    const Rect area_react = calculate_overlay_rect(offset);
    switch (event.type) {
        case EventType::MouseButton: {
            if (!is_visible()) {
                return false;
            }
            if (event.mouse_button.is_down) {
                if (bar_rect.contains(event.mouse_button.x, event.mouse_button.y) && !m_dragging) {
                    m_timer.cancel();
                    m_dragging = true;
                    m_last_mouse_drag_pos = event.mouse_button.y;
                    return true;
                };
            } else {
                if (m_dragging) {
                    m_timer.reset();
                    m_dragging = false;
                    return true;
                }
                if (area_react.contains(event.mouse_button.x, event.mouse_button.y)) {
                    m_timer.reset();
                    apply_scroll_offset(event.mouse_button.y - bar_rect.y);
                    return true;
                }
            }
            return false;
        }
        case EventType::MouseMove: {
            if (m_dragging) {
                const f32 drag_offset = f32(event.mouse_move.y) - m_last_mouse_drag_pos;
                m_last_mouse_drag_pos = f32(event.mouse_move.y);
                apply_scroll_offset(drag_offset);
                return true;
            }
            return false;
        }
        case EventType::MouseScroll: {
            if (!m_dragging) {
                set_visible(true);
                m_timer.reset();
                apply_scroll_offset(f32(event.mouse_scroll.y * 2));
                return true;
            }
            return false;
        }
        case EventType::ContextMouseLeave: {
            m_dragging = false;
            m_timer.reset();
            return false;
        }
        default:
            return false;
    }
}
void VScrollBar::eval_constraints(const ConstraintParams& params) {
    m_size = constraint().resolve(params);
    recalculate();
}

void VScrollBar::paint(Painter& painter, const PaintParams& params) const {
    const PaintParams local_params = params.new_scope();
    const Rect overlay_rect = calculate_overlay_rect(params.offset);
    painter.draw_rect(overlay_rect, local_params.z_index, m_style.color_overlay);
    const PaintParams bar_params = local_params.new_scope();
    const Offset bar_offset = bar_params.offset + Offset{0.f, m_bar_offset_value};
    const Rect bar_rect = calculate_bar_rect(bar_offset);
    painter.draw_rect(bar_rect, bar_params.z_index, m_style.color_bar);
}

void VScrollBar::set_scroll_percentage(const f32 percentage) {
    m_scroll_percentage = std::max(0.f, std::min(percentage, 1.0f));
    m_bar_offset_value = m_scroll_height * m_scroll_percentage;
}

void VScrollBar::set_area_percentage(const f32 percentage) {
    m_area_percentage = std::max(0.f, std::min(percentage, 1.0f));
    recalculate();
}

void VScrollBar::set_style(const ScrollBarStyle& style) {
    m_style = style;
    recalculate();
}

void VScrollBar::set_visible(const bool value) {
    if (m_area_percentage < 1.0f) {
        Object::set_visible(value);
        if (value && !m_dragging) {
            m_timer.reset();
        }
    } else {
        Object::set_visible(false);
    }
}

Rect VScrollBar::calculate_bar_rect(const Offset offset) const {
    modus_assert(m_size.width >= m_style.margin_outer * 2.f + m_style.margin_inner * 2.f);
    return {
        offset.x + m_style.margin_outer + m_style.margin_inner,
        offset.y + m_style.margin_inner + m_style.margin_outer,
        m_size.width - (m_style.margin_outer * 2.f + m_style.margin_inner * 2.f),
        m_bar_height,
    };
}

Rect VScrollBar::calculate_overlay_rect(const Offset offset) const {
    modus_assert(m_size.width >= m_style.margin_outer * 2.f + m_style.margin_inner * 2.f);
    return {
        offset.x + m_style.margin_outer,
        offset.y + m_style.margin_outer,
        m_size.width - (m_style.margin_outer * 2.f),
        m_size.height - (m_style.margin_outer * 2.f),
    };
}

void VScrollBar::recalculate() {
    m_bar_height = (f32(m_size.height) * m_area_percentage) - (m_style.margin_inner * 2.f) -
                   (m_style.margin_outer * 2.f);
    m_scroll_height =
        m_size.height - m_bar_height - (m_style.margin_outer * 2.f) - (m_style.margin_inner * 2.f);
    m_bar_offset_value = m_scroll_height * m_scroll_percentage;
}

void VScrollBar::apply_scroll_offset(const f32 offset) {
    f32 new_offset = m_bar_offset_value + offset;
    new_offset = std::max(0.f, std::min(new_offset, m_scroll_height));
    if (new_offset != m_bar_offset_value) {
        m_bar_offset_value = new_offset;
        m_scroll_percentage = m_bar_offset_value / m_scroll_height;
        if (m_on_scroll) {
            m_on_scroll(m_scroll_percentage);
        }
    }
}

void VScrollBar::on_hide_timer_trigger(Timer&) {
    set_visible(false);
}

}    // namespace modus::ui
