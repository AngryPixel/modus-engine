/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <ui/text/text_utils.hpp>
#include <ui/painter.hpp>
#include <ui/text/font.hpp>
#include <ui/color.hpp>
#include <math/packing.hpp>

namespace modus::ui {

struct NullEmmiter {};

struct EvalParams {
    f32 line_spacing = 0;
    f32 max_text_width = 0;
    f32 max_text_height = 0;
    bool allow_multiline = false;
    u8 tab_size = 4;
};

enum class ParseResult { Word, Newline, Tab, Space, End };

struct ParseState {
    size_t offset = 0;
    size_t word_begin_offset = 0;
};

static StringSlice get_word(const ParseState& state, const StringSlice text) {
    return text.sub_slice(state.word_begin_offset, state.offset - state.word_begin_offset);
}

static ParseResult parse(ParseState& state, const StringSlice text) {
    while (state.offset < text.size()) {
        const char ch = text[state.offset];
        state.offset++;
        if (ch == '\n') {
            return ParseResult::Newline;
        } else if (ch == '\t') {
            return ParseResult::Tab;
        } else if (ch == ' ') {
            return ParseResult::Space;
        } else if (ch >= 33) {
            state.word_begin_offset = state.offset - 1;
            while (state.offset < text.size()) {
                if (text[state.offset] < 33) {
                    return ParseResult::Word;
                }
                state.offset++;
            }
            return ParseResult::Word;
        }
    }
    return ParseResult::End;
}

static Size eval_text_size(const EvalParams& eval_params,
                           const Font& font,
                           const StringSlice text) {
    Size size{0, font.line_height_px()};
    const GlyphInfo& space_glyph = font.glyph_info_for_char(' ');

    bool done = false;
    ParseState state;
    f32 current_x = 0.f;
    while (!done) {
        const ParseResult r = parse(state, text);
        switch (r) {
            case ParseResult::Word: {
                const StringSlice word = get_word(state, text);
                for (const auto& ch : word) {
                    current_x += font.glyph_info_for_char(ch).advance_x_px;
                }
                break;
            }
            case ParseResult::Newline:
                if (!eval_params.allow_multiline) {
                    done = true;
                } else {
                    size.width = std::max(current_x, size.width);
                    size.height += font.line_height_px() + eval_params.line_spacing;
                    current_x = 0;
                }
                break;
            case ParseResult::Space:
                current_x += f32(space_glyph.advance_x_px);
                break;
            case ParseResult::Tab:
                current_x += f32(space_glyph.advance_x_px * eval_params.tab_size);
                break;
            case ParseResult::End:
                done = true;
                break;
        }
    }
    size.width = std::max(current_x, size.width);
    return size;
}

Size TextSizeCaculator::text_size(const Font& font, const StringSlice text) const {
    EvalParams params;
    params.line_spacing = line_spacing;
    params.tab_size = tab_size;
    params.allow_multiline = allow_multiline;
    params.max_text_width = max_text_width.value_or(std::numeric_limits<f32>::max());
    params.max_text_height = max_text_height.value_or(std::numeric_limits<f32>::max());
    return eval_text_size(params, font, text);
}

u32 TextVertexDataGenerator::quad_count(const Font& font, const StringSlice text) const {
    u32 quad_count = 0;
    const GlyphInfo& space_glyph = font.glyph_info_for_char(' ');
    bool done = false;
    ParseState state;
    f32 current_x = 0;
    f32 currenty_y = 0;
    const f32 text_width_limit = max_text_width.value_or(std::numeric_limits<f32>::max());
    const f32 text_height_limit = max_text_height.value_or(std::numeric_limits<f32>::max());

    while (!done) {
        const ParseResult r = parse(state, text);
        switch (r) {
            case ParseResult::Word: {
                const StringSlice word = get_word(state, text);

                // calculate word width
                f32 word_advance = 0;
                for (const auto& ch : word) {
                    word_advance += f32(font.glyph_info_for_char(ch).advance_x_px);
                }

                // check if within borders
                if (current_x + word_advance > text_width_limit) {
                    currenty_y += font.line_height_px() + f32(line_spacing);
                    current_x = 0.f;
                }

                if (currenty_y > text_height_limit) {
                    done = true;
                } else {
                    current_x += word_advance;
                    quad_count += word.size();
                }
                break;
            }
            case ParseResult::Newline:
                if (!allow_multiline) {
                    done = true;
                } else {
                    currenty_y += font.line_height_px() + f32(line_spacing);
                    if (currenty_y > text_height_limit) {
                        done = true;
                    }
                    current_x = 0.f;
                }
                break;
            case ParseResult::Space:
                current_x += f32(space_glyph.advance_x_px);
                break;
            case ParseResult::Tab:
                current_x += f32(space_glyph.advance_x_px * tab_size);
                break;
            case ParseResult::End:
                done = true;
                break;
        }
    }
    return quad_count;
}

void TextVertexDataGenerator::generate(PainterBuffer& out_buffer,
                                       const Offset& offset,
                                       const Color& color,
                                       const Font& font,
                                       const StringSlice text) const {
    auto& out_vertices = out_buffer.vertices;
    auto& out_indices = out_buffer.indices;

    const GlyphInfo& space_glyph = font.glyph_info_for_char(' ');

    bool done = false;
    ParseState state;
    f32 current_x = 0;
    f32 currenty_y = 0;
    const f32 text_width_limit = max_text_width.value_or(std::numeric_limits<f32>::max());
    const f32 text_height_limit = max_text_height.value_or(std::numeric_limits<f32>::max());

    while (!done) {
        const ParseResult r = parse(state, text);
        switch (r) {
            case ParseResult::Word: {
                const StringSlice word = get_word(state, text);

                // calculate word width
                f32 word_advance = 0;
                for (const auto& ch : word) {
                    word_advance += font.glyph_info_for_char(ch).advance_x_px;
                }

                // check if within borders
                if (current_x + word_advance > text_width_limit) {
                    currenty_y += font.line_height_px() + line_spacing;
                    current_x = 0;
                }

                if (currenty_y > text_height_limit) {
                    done = true;
                } else {
                    for (const auto& ch : word) {
                        const GlyphInfo& info = font.glyph_info_for_char(ch);
                        const GlyphAtlasCoord& gc = font.glyph_atlas_coord_for_char(ch);
                        const u16 index_offset = static_cast<u16>(out_vertices.size());

                        // TODO: For some reason the text does not render correctly if the offset
                        // doesn't start on a height with decimal part
                        const f32 left =
                            f32(std::round(offset.x) + current_x + f32(info.bearing_x_px));
                        const f32 top = f32(std::round(offset.y) + currenty_y) +
                                        f32(font.line_height_px()) - f32(info.bearing_y_px) -
                                        f32(font.max_bearing_y_diff_px());
                        const f32 right = left + f32(info.width_px);
                        const f32 bottom = top + f32(info.height_px);

                        const PainterVertexData data[] = {
                            {{left, top}, math::pack_unorm_1616({gc.left, gc.top}), color.u},
                            {{left, bottom}, math::pack_unorm_1616({gc.left, gc.bottom}), color.u},
                            {{right, bottom},
                             math::pack_unorm_1616({gc.right, gc.bottom}),
                             color.u},
                            {{right, top}, math::pack_unorm_1616({gc.right, gc.top}), color.u},
                        };

                        const PainterIndexType indices[] = {
                            index_offset,
                            static_cast<u16>(index_offset + u16(1)),
                            static_cast<u16>(index_offset + u16(2)),
                            index_offset,
                            static_cast<u16>(index_offset + u16(2)),
                            static_cast<u16>(index_offset + u16(3)),
                        };

                        for (auto& d : data) {
                            out_vertices.push_back(d).expect("Failed to push back data");
                        }

                        for (auto& i : indices) {
                            out_indices.push_back(i).expect("Failed to push back indices");
                        }

                        current_x += info.advance_x_px;
                    }
                }
                break;
            }
            case ParseResult::Newline:
                if (!allow_multiline) {
                    done = true;
                } else {
                    currenty_y += font.line_height_px() + line_spacing;
                    if (currenty_y > text_height_limit) {
                        done = true;
                    }
                    current_x = 0.f;
                }
                break;
            case ParseResult::Space:
                current_x += f32(space_glyph.advance_x_px);
                break;
            case ParseResult::Tab:
                current_x += f32(space_glyph.advance_x_px * tab_size);
                break;
            case ParseResult::End:
                done = true;
                break;
        }
    }
}

}    // namespace modus::ui
