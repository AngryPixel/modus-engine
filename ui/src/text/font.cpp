/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <ui/text/font.hpp>
#include <ui/context.hpp>
#include <ui/painter.hpp>
#include <core/io/memory_stream.hpp>

#include <ui/font_generated.h>

namespace modus::ui {

Result<UIRefPtr<Font>, FontError> Font::create(ContextPtr ctx, IByteReader& reader) {
    RamStream<UIAllocator> ram_stream;

    if (!ram_stream.populate_from_stream(reader)) {
        return Error(FontError::IO);
    }

    const ByteSlice font_slice = ram_stream.as_bytes();
    flatbuffers::Verifier v(font_slice.data(), font_slice.size());
    if (!fbs::VerifyFontBuffer(v)) {
        return Error(FontError::Format);
    }

    const auto fbs_font = fbs::GetFont(font_slice.data());
    const auto fbs_header = fbs_font->header();
    const auto fbs_meta_data = fbs_font->meta_data();
    if (fbs_header == nullptr || fbs_meta_data == nullptr) {
        return Error(FontError::Content);
    }

    const auto fbs_glyph_info = fbs_font->meta_data()->info();
    if (fbs_glyph_info->size() != kGlyphInfoCount) {
        return Error(FontError::Content);
    }

    const auto fbs_bitmap = fbs_font->data();
    if (fbs_bitmap == nullptr) {
        return Error(FontError::Content);
    }

    Painter& painter = ctx->painter();
    UIRefPtr<PainterBitmap2D> bitmap;

    PainterBitmap2DCreateParams bitmap_params;
    bitmap_params.width = fbs_header->atlas_width_px();
    bitmap_params.height = fbs_header->atlas_height_px();
    bitmap_params.format = PainterBitmapFormat::R8;
    bitmap_params.data = fbs_bitmap->data();
    bitmap_params.data_size = fbs_bitmap->size();

    if (auto r = painter.create_bitmap2d(bitmap_params); !r) {
        return Error(FontError::Painter);
    } else {
        bitmap = *r;
    }

    UIRefPtr<Font> font = ctx->alloc<Font>(ctx);
    font->m_monospaced = fbs_header->is_monospace();
    font->m_font_size_px = f32(fbs_header->font_size_px());
    font->m_line_height_px = f32(fbs_header->line_height_px());
    font->m_max_bearing_y_diff_px = f32(fbs_header->max_bearing_y_diff_px());
    font->m_bitmap = bitmap;

    for (i32 i = 0; i < kGlyphInfoCount; ++i) {
        auto& gi = font->m_glyph_info[i];
        auto& gc = font->m_glyph_coords[i];
        const auto& fbs_gi = fbs_glyph_info->Get(i);

        gi.width_px = f32(fbs_gi->width());
        gi.height_px = f32(fbs_gi->height());
        gi.advance_x_px = f32(fbs_gi->advance_x());
        gi.bearing_x_px = f32(fbs_gi->bearing_x());
        gi.bearing_y_px = f32(fbs_gi->bearing_y());

        gc.top = fbs_gi->tex_top();
        gc.bottom = fbs_gi->tex_bottom();
        gc.right = fbs_gi->tex_right();
        gc.left = fbs_gi->tex_left();
    }

    return Ok(std::move(font));
}

Font::Font(ContextPtr context) : m_context(context), m_line_height_px(0), m_monospaced(false) {}

Font::~Font() {}

const GlyphInfo& Font::glyph_info_for_char(const char c) const {
    if (c < 32) {
        modus_panic("Invalid ascii char");
    }
    const i32 index = c - 32;
    return m_glyph_info[index];
}

const GlyphAtlasCoord& Font::glyph_atlas_coord_for_char(const char c) const {
    if (c < 32) {
        modus_panic("Invalid ascii char");
    }
    const i32 index = c - 32;
    return m_glyph_coords[index];
}

}    // namespace modus::ui
