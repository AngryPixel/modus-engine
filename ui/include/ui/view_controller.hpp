/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <ui/animation.hpp>

namespace modus::ui {

class ViewAnimation;
class MODUS_UI_EXPORT ViewController {
    friend class Context;

   public:
    using ViewPtr = UIRefPtr<View>;

   private:
    enum class PendingOperationType {
        Pop,
        PopToRoot,
        Push,
        PushAnimated,
        PopAnimated,
    };
    struct PendingOperation {
        ViewPtr view;
        PendingOperationType type;
    };

    Vector<ViewPtr> m_views;
    Vector<PendingOperation> m_operations;
    Animator m_animator;
    UIRefPtr<ViewAnimation> m_animation;

   public:
    ViewController(ContextPtr context);
    ~ViewController();
    ViewController(const ViewController&) = delete;
    ViewController(ViewController&&) = delete;
    ViewController& operator=(const ViewController&) = delete;
    ViewController& operator=(ViewController&&) = delete;

    void push_view(const ViewPtr& view);

    void push_view_animated(const ViewPtr& view);

    void pop_view();

    void pop_view_animated();

    void pop_to_root();

    inline bool is_empty() const { return m_views.empty(); }

    ViewPtr top_view() const;

    void clear();

    void paint(Painter& painter) const;

    bool on_event(const Event& evt);

    void eval_constraints();

   private:
    void do_pop();

    void update();

    void on_animated_push_finish(Animator& animator);

    void on_animated_pop_finish(Animator& animator);

    bool is_animating() const;
};
}    // namespace modus::ui
