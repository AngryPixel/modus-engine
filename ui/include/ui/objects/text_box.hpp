/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <ui/color.hpp>
#include <ui/object.hpp>

namespace modus::ui {

struct MODUS_UI_EXPORT TextBoxStyle {
    UIRefPtr<Font> font;
    Color background_color;
    Color text_color;
    f32 text_margin;
};

class MODUS_UI_EXPORT TextBox final : public Object {
   private:
    String m_text;
    Size m_text_size;
    TextBoxStyle m_style;

   public:
    TextBox(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    ~TextBox();

    StringSlice text() const { return m_text; }

    void set_text(const StringSlice text);

    void set_style(const TextBoxStyle& style);

    void eval_constraints(const ConstraintParams& params) override;

    void paint(Painter& painter, const PaintParams& params) const override;

   private:
    void update_text_size();
};

}    // namespace modus::ui
