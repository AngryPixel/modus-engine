/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <ui/color.hpp>
#include <ui/object.hpp>

namespace modus::ui {

/// Text Label
class MODUS_UI_EXPORT Label final : public Object {
   private:
    String m_text;
    Size m_text_size;
    UIRefPtr<Font> m_font;
    Color m_color;
    HorizontalAlignment m_halignment;
    VerticalAlignment m_valignment;

   public:
    Label(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    ~Label();

    StringSlice text() const { return m_text; }

    void set_text(const StringSlice text);

    HorizontalAlignment horizontal_alignment() const { return m_halignment; }

    VerticalAlignment vertical_alignment() const { return m_valignment; }

    void set_horizontal_alignment(const HorizontalAlignment alignment) { m_halignment = alignment; }

    void set_vertical_alignment(const VerticalAlignment alignment) { m_valignment = alignment; }

    UIRefPtr<Font> font() const { return m_font; }

    void set_font(const UIRefPtr<Font>& font);

    void set_color(const Color color) { m_color = color; }

    Color color() const { return m_color; }

    void eval_constraints(const ConstraintParams& params) override;

    void paint(Painter& painter, const PaintParams& params) const override;

   private:
    void update_text_size();
};

}    // namespace modus::ui
