/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <core/delegate.hpp>
#include <ui/color.hpp>
#include <ui/object.hpp>
#include <ui/timer.hpp>

namespace modus::ui {

struct MODUS_UI_EXPORT SliderStyle {
    f32 margin;
    f32 border_thickness;
    Color border_color;
    Color slider_color;
};

class MODUS_UI_EXPORT HSlider final : public Object {
   private:
    SliderStyle m_style;
    f32 m_slide_percentage;
    bool m_dragging;

   public:
    Delegate<void(HSlider&)> m_on_slide;

   public:
    HSlider(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

    void eval_constraints(const ConstraintParams& params) override;

    void set_slide_percentage(const f32 percentage);

    void set_style(const SliderStyle& style);

    f32 slide_percentage() const { return m_slide_percentage; }

   private:
    Rect calculate_slider_rect(const Offset offset) const;
};

}    // namespace modus::ui
