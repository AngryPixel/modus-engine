/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <core/delegate.hpp>
#include <ui/animation.hpp>
#include <ui/color.hpp>
#include <ui/event_helpers.hpp>
#include <ui/object.hpp>
#include <ui/objects/label.hpp>
namespace modus::ui {

class Font;

struct MODUS_UI_EXPORT ButtonStyle {
    UIRefPtr<Font> font;
    Color color;
    Color color_hover;
    Color color_down;
    Color label_color;
    Color label_color_down;
    Color label_color_hover;
};

class MODUS_UI_EXPORT Button : public Object {
   protected:
    Label m_label;
    MouseClickHelper m_helper;
    f32 m_label_margin;
    Color m_color;
    ButtonStyle m_style;

   public:
    Delegate<void(Button&)> m_on_clicked;

   public:
    Button(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    ~Button();

    void set_text(const StringSlice text);

    StringSlice text() const { return m_label.text(); }

    void set_style(const ButtonStyle& style);

    void set_label_margin(const f32 margin);

    void eval_constraints(const ConstraintParams& params) override;

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

   private:
    void on_mouse_enter();

    void on_mouse_leave();

    void on_mouse_down();

    void on_mouse_up();
};

/// Same as Button but with animated color transitions between the hover and regular states.
class MODUS_UI_EXPORT AnimatedButton : public Button {
    friend class ButtonAnimation;

   private:
    Animator m_animator;

   public:
    AnimatedButton(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    void set_animation_duration_ms(const IMilisec duration_ms);

   private:
    void on_mouse_enter();

    void on_mouse_leave();
};

}    // namespace modus::ui
