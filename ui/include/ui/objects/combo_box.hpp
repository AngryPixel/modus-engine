/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <ui/color.hpp>
#include <ui/event_helpers.hpp>
#include <ui/objects/label.hpp>

#include <core/delegate.hpp>
#include <core/string_slice.hpp>

namespace modus::ui {

struct MODUS_UI_EXPORT ComboBoxStyle {
    UIRefPtr<Font> font;
    Color color;
    Color color_hover;
    Color label_color;
    Color label_color_hover;
    f32 selection_item_height;
    f32 selection_item_margin;
};

class MODUS_UI_EXPORT ComboBoxData {
   public:
    virtual ~ComboBoxData() = default;

    virtual u32 item_count() const = 0;

    virtual StringSlice item_display(const u32 index) const = 0;
};

class ComboBoxDialog;
class MODUS_UI_EXPORT ComboBox final : public Object {
   private:
    Label m_label;
    MouseClickHelper m_helper;
    ComboBoxStyle m_style;
    UIRefPtr<ComboBoxData> m_data;
    UIRefPtr<ComboBoxDialog> m_dialog;
    Color m_color;
    u32 m_selected_index;

   public:
    Delegate<void(ComboBox&)> m_on_selected;

    ComboBox(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    ~ComboBox();

    void set_style(const ComboBoxStyle& style);

    const ComboBoxStyle& style() const { return m_style; }

    void eval_constraints(const ConstraintParams& params) override;

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

    u32 selected_index() const { return m_selected_index; }

    void set_selected_index(const u32 index);

    void set_data(UIRefPtr<ComboBoxData> data);

    const UIRefPtr<ComboBoxData>& data() const { return m_data; }

   private:
    void on_mouse_click();

    void on_mouse_leave();

    void on_mouse_enter();
};

}    // namespace modus::ui
