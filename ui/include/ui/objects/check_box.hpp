/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/delegate.hpp>
#include <ui/color.hpp>
#include <ui/event_helpers.hpp>
#include <ui/object.hpp>

namespace modus::ui {

struct MODUS_UI_EXPORT CheckBoxStyle {
    Color border_color;
    Color center_color;
    f32 center_margin;
    f32 border_thickness;
};

class MODUS_UI_EXPORT CheckBox final : public Object {
   private:
    MouseClickHelper m_helper;
    CheckBoxStyle m_style;
    bool m_checked;

   public:
    Delegate<void(CheckBox&)> m_on_clicked;

    CheckBox(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    ~CheckBox();

    bool checked() const { return m_checked; }

    void set_checked(const bool value);

    void set_style(const CheckBoxStyle& style);

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

   private:
    void on_mouse_click();
};

}    // namespace modus::ui
