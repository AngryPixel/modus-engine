/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <core/delegate.hpp>
#include <ui/color.hpp>
#include <ui/object.hpp>
#include <ui/timer.hpp>

namespace modus::ui {

struct MODUS_UI_EXPORT ScrollBarStyle {
    f32 margin_outer;
    f32 margin_inner;
    Color color_overlay;
    Color color_bar;
};

class MODUS_UI_EXPORT VScrollBar final : public Object {
   private:
    ScrollBarStyle m_style;
    f32 m_scroll_percentage;
    f32 m_area_percentage;
    f32 m_bar_height;
    f32 m_scroll_height;
    f32 m_bar_offset_value;
    f32 m_last_mouse_drag_pos;
    Timer m_timer;
    bool m_dragging;

   public:
    Delegate<void(const f32)> m_on_scroll;

   public:
    VScrollBar(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

    void eval_constraints(const ConstraintParams& params) override;

    void set_scroll_percentage(const f32 percentage);

    void set_area_percentage(const f32 percentage);

    void set_style(const ScrollBarStyle& style);

    void set_visible(const bool value) override;

    f32 scroll_percentage() const { return m_scroll_percentage; }

   private:
    Rect calculate_bar_rect(const Offset offset) const;

    Rect calculate_overlay_rect(const Offset offset) const;

    void recalculate();

    void apply_scroll_offset(const f32 offset);

    void on_hide_timer_trigger(Timer& timer);
};

}    // namespace modus::ui
