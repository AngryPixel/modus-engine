/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/painter.hpp>

namespace modus::ui {

class MODUS_UI_EXPORT NullPainterDevice final : public PainterDevice {
   public:
    NullPainterDevice();

    void begin_frame(const bool) override;

    void end_frame() override;

    Result<UIRefPtr<PainterBitmap2D>> create_bitmap2d(ContextPtr context,
                                                      const PainterBitmap2DCreateParams&) override;

    void execute_commands(Slice<UIRefPtr<PainterBuffer>> buffers,
                          Slice<PainterCommand> commands) override;
};

}    // namespace modus::ui
