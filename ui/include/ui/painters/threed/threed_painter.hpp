/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <threed/threed.hpp>
#include <ui/painter.hpp>

namespace modus::threed {
class Device;
struct Pipeline;
class Compositor;
struct Pass;
}    // namespace modus::threed

namespace modus::ui {

class ThreedResources;
struct TriBuffer;
class MODUS_UI_EXPORT ThreedPainterDeviceBase : public PainterDevice {
   protected:
    NotMyPtr<threed::Device> m_device;
    NotMyPtr<threed::Compositor> m_compositor;
    NotMyPtr<threed::Pipeline> m_pipeline;
    NotMyPtr<threed::Pass> m_pass;
    threed::ProgramHandle m_program;
    threed::EffectHandle m_effect;
    threed::SamplerHandle m_sampler;
    threed::TextureHandle m_default_texture;
    Vector<TriBuffer> m_allocated_buffers;
    u32 m_current_buffer_index;

   protected:
    ThreedPainterDeviceBase(NotMyPtr<threed::Device> device,
                            NotMyPtr<threed::Compositor> compositor);

   public:
    ~ThreedPainterDeviceBase();

    Result<> initialize(ContextPtr contex) override final;

    void shutdown(ContextPtr) override final;

    Result<UIRefPtr<PainterBitmap2D>> create_bitmap2d(
        ContextPtr context,
        const PainterBitmap2DCreateParams& params) override final;

    void execute_commands(Slice<UIRefPtr<PainterBuffer>> buffers,
                          Slice<PainterCommand> commands) override final;

   private:
    TriBuffer& create_or_get_buffer(const u32 index);
};

/// Standalone, will update the compositor and create a pipeline
class MODUS_UI_EXPORT ThreedPainterDeviceStandalone final : public ThreedPainterDeviceBase {
   public:
    ThreedPainterDeviceStandalone(NotMyPtr<threed::Device> device,
                                  NotMyPtr<threed::Compositor> compositor);

    void begin_frame(const bool clear) override;

    void end_frame() override;
};

/// Embeddable, only craetes passes and expects someone to provide the pipeline object
class MODUS_UI_EXPORT ThreedPainterDeviceEmbedded final : public ThreedPainterDeviceBase {
   public:
    ThreedPainterDeviceEmbedded(NotMyPtr<threed::Device> device,
                                NotMyPtr<threed::Compositor> compositor);

    void set_pipeline(NotMyPtr<threed::Pipeline> pipeline);

    void begin_frame(const bool clear) override;

    void end_frame() override;
};

}    // namespace modus::ui
