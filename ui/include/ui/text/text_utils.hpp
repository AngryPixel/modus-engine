/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <core/string_slice.hpp>

namespace modus::ui {

class Font;
struct PainterBuffer;

struct MODUS_UI_EXPORT TextSizeCaculator {
    f32 line_spacing = 0;
    Optional<f32> max_text_width;
    Optional<f32> max_text_height;
    bool allow_multiline = false;
    u8 tab_size = 4;

    Size text_size(const Font& font, const StringSlice text) const;
};

struct MODUS_UI_EXPORT TextVertexDataGenerator {
    f32 line_spacing = 0;
    Optional<f32> max_text_width;
    Optional<f32> max_text_height;
    bool allow_multiline = false;
    u8 tab_size = 4;

    u32 quad_count(const Font& font, const StringSlice) const;

    void generate(PainterBuffer& out_buffer,
                  const Offset& offset,
                  const Color& color,
                  const Font& font,
                  const StringSlice text) const;
};

}    // namespace modus::ui
