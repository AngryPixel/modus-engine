/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

namespace modus {
class IByteReader;
}

namespace modus::ui {

struct MODUS_UI_EXPORT GlyphInfo {
    f32 width_px = 0;        // Glyph Width
    f32 height_px = 0;       // Glyph Height
    f32 bearing_x_px = 0;    // X Offset from origin
    f32 bearing_y_px = 0;    // Y Offset from origin
    f32 advance_x_px = 0;    // Amount to advance for next char
};

struct MODUS_UI_EXPORT GlyphAtlasCoord {
    f32 top;
    f32 bottom;
    f32 right;
    f32 left;
};

enum class FontError : u8 {
    IO,
    Format,
    Content,
    Painter,
};

/// Contains information required to display a font.
/// Currently only supports ascii characters
class MODUS_UI_EXPORT Font final {
   private:
    // Covers the visible ascii range only
    static constexpr i32 kGlyphInfoCount = 96;

    ContextPtr m_context;
    Array<GlyphInfo, kGlyphInfoCount> m_glyph_info;
    Array<GlyphAtlasCoord, kGlyphInfoCount> m_glyph_coords;
    f32 m_line_height_px;
    f32 m_font_size_px;
    f32 m_max_bearing_y_diff_px;
    UIRefPtr<PainterBitmap2D> m_bitmap;
    bool m_monospaced;

   public:
    static Result<UIRefPtr<Font>, FontError> create(ContextPtr ctx, IByteReader& reader);

    Font(ContextPtr ctx);
    ~Font();
    MODUS_CLASS_DISABLE_COPY_MOVE(Font);

    const GlyphInfo& glyph_info_for_char(const char c) const;

    const GlyphAtlasCoord& glyph_atlas_coord_for_char(const char c) const;

    f32 font_size_px() const { return m_font_size_px; }

    f32 max_bearing_y_diff_px() const { return m_max_bearing_y_diff_px; }

    f32 line_height_px() const { return m_line_height_px; }

    bool is_mono_spaced() const { return m_monospaced; }

    UIRefPtr<PainterBitmap2D> font_atlas() const { return m_bitmap; }

    // Generate draw instruction for character
};

}    // namespace modus::ui
