/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/tickable.hpp>
#include <ui/ui.hpp>

namespace modus::ui {

class MODUS_UI_EXPORT Animation {
   public:
    virtual ~Animation() = default;

    /// Perform animation calculations based on the given interval [0-1].
    virtual void animate(const f64 interval) = 0;
};

class MODUS_UI_EXPORT Animator {
   private:
    ContextPtr m_context;
    IMilisec m_elapsed_ms;
    IMilisec m_duration_ms;
    TickableHandle m_handle;
    bool m_reversed;
    UIRefPtr<Animation> m_animation;

   public:
    Delegate<void(Animator&)> m_on_finished;

    Animator(ContextPtr m_context);

    virtual ~Animator();

    void start(UIRefPtr<Animation> animation, const bool reversed = false);

    void start(const bool reversed = false);

    void stop();

    void set_reversed(const bool value);

    bool is_reversed() const { return m_reversed; }

    IMilisec elapsed_ms() const { return m_elapsed_ms; }

    IMilisec duration_ms() const { return m_duration_ms; }

    void set_duration_ms(const IMilisec duration_ms) { m_duration_ms = duration_ms; }

    UIRefPtr<Animation>& animation() { return m_animation; }

    const UIRefPtr<Animation>& animation() const { return m_animation; }

    void set_animation(const UIRefPtr<Animation>& animation) { m_animation = animation; }

    bool is_animating() const { return m_handle.is_valid(); }

   private:
    void on_tick(const IMilisec tick_ms);
};
}    // namespace modus::ui
