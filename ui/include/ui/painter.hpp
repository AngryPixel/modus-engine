/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <core/string_slice.hpp>
#include <ui/color.hpp>
#include <ui/constraint.hpp>

namespace modus::ui {

class Font;

enum class PainterBitmapFormat { R8, RGB8, RGBA8 };

struct MODUS_UI_EXPORT PainterBitmap2DCreateParams {
    PainterBitmapFormat format;
    u32 width;
    u32 height;
    const void* data;
    size_t data_size;
};

class MODUS_UI_EXPORT PainterBitmap2D {
   public:
    virtual ~PainterBitmap2D() = default;
};

struct MODUS_UI_EXPORT PaintTextParams {
    const TextVertexDataGenerator& generator;
    const Font& font;
    Rect rect;
    Color color;
};

struct MODUS_UI_EXPORT PaintParams {
    Offset offset = {0, 0};
    u32 z_index = 0;

    PaintParams new_scope() const { return PaintParams{offset, z_index + 1}; }
};

struct MODUS_UI_EXPORT alignas(4) PainterVertexData {
    glm::vec2 pos;
    u32 uv;
    u32 color;
};

using PainterIndexType = u16;
constexpr i32 kPainterBufferQuadCount = 4096;
constexpr i32 kPainterVertexBufferSize = kPainterBufferQuadCount * 4;
constexpr i32 kPainterIndexBufferSize = kPainterBufferQuadCount * 6;

struct MODUS_UI_EXPORT PainterBuffer {
    FixedVector<PainterVertexData, kPainterVertexBufferSize> vertices;
    FixedVector<PainterIndexType, kPainterIndexBufferSize> indices;
};

struct MODUS_UI_EXPORT PainterCommand {
    u32 buffer_index;
    u32 draw_offset;
    u32 draw_count;
    UIRefPtr<PainterBitmap2D> bitmap;
    Optional<Rect> scissor_rect;
};

class MODUS_UI_EXPORT PainterDevice {
   public:
    virtual ~PainterDevice() = default;

    virtual Result<> initialize(ContextPtr) { return Ok<>(); }

    virtual void shutdown(ContextPtr) {}

    virtual void begin_frame(const bool clear) = 0;

    virtual void end_frame() = 0;

    virtual Result<UIRefPtr<PainterBitmap2D>> create_bitmap2d(
        ContextPtr context,
        const PainterBitmap2DCreateParams& params) = 0;

    virtual void execute_commands(Slice<UIRefPtr<PainterBuffer>> buffers,
                                  Slice<PainterCommand> commands) = 0;
};

class Painter;

class MODUS_UI_EXPORT ScissorScope {
   private:
    Painter& m_painter;

   public:
    ScissorScope(Painter& painter, const Rect scissor_rect);
    ~ScissorScope();
    MODUS_CLASS_DISABLE_COPY_MOVE(ScissorScope);
};

class MODUS_UI_EXPORT Painter {
    friend class ScissorScope;

   private:
    ContextPtr m_context;
    NotMyPtr<PainterDevice> m_painter_device;
    Vector<UIRefPtr<PainterBuffer>> m_buffers;
    Vector<PainterCommand> m_commands;
    Vector<Rect> m_scissor_stack;
    Rect m_early_clip_rect;
    u32 m_buffer_index;

   public:
    Painter(ContextPtr context, NotMyPtr<PainterDevice> device);

    Result<> initialize();

    void shutdown();

    void begin_frame(const bool clear);

    void end_frame();

    void draw_rect(const Rect& rect, const u32 zvalue, const Color& color);

    void draw_border(const Rect& rect, const u32 zvalue, const Color& color, const f32 thickness);

    void draw_text(const PaintTextParams& params, const StringSlice text);

    Result<UIRefPtr<PainterBitmap2D>> create_bitmap2d(const PainterBitmap2DCreateParams& params);

   private:
    PainterBuffer& get_or_allocate_buffer(const u32 required_size);

    Optional<Rect> current_scissor_rect() const;

    void push_clip_rect(const Rect& r);

    void pop_clip_rect();
};

}    // namespace modus::ui
