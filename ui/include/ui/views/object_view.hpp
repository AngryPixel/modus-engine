/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <ui/view.hpp>

namespace modus::ui {

class MODUS_UI_EXPORT ObjectView : public View {
   private:
    UIRefPtr<Object> m_object;

   public:
    ObjectView(ContextPtr ctx, const UIRefPtr<Object>& object = UIRefPtr<Object>());

    void eval_constraints(const ConstraintParams& params) override final;

    void paint(Painter& painter, const PaintParams& params) const override;

    bool on_event(const Event& event, const Offset offset) override;

    void set_object(const UIRefPtr<Object>& object);

    const UIRefPtr<Object>& object() const { return m_object; }
};

}    // namespace modus::ui
