/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// clang-format off
#include <core/core.hpp>
#include <core/strong_type.hpp>
#include <core/ref_pointer.hpp>
#include <core/containers.hpp>
#include <core/string_slice.hpp>

#include <ui/module_config.hpp>
#include <ui/ui_types.hpp>
// clang-fornmat on

namespace modus::ui {
using UIAllocator = DefaultAllocator;

template <typename T>
using Vector = modus::Vector<T, UIAllocator>;

using String = StringWithAllocator<UIAllocator>;

template <typename T>
using UIRefPtr = RefPtr<T, UIAllocator>;

template <typename T>
using UIWeakRefPtr = WeakRefPtr<T, UIAllocator>;
}    // namespace modus::ui
