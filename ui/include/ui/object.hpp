/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <ui/ui.hpp>

#include <ui/constraint.hpp>

namespace modus::ui {

class MODUS_UI_EXPORT Object {
    friend struct ObjectRectUpdater;
    friend class Context;
    friend class ConstraintSolver;

   private:
    ContextPtr m_context;
    UIWeakRefPtr<Object> m_parent;

   protected:
    Constraint m_constraint;
    Size m_size;
    bool m_visible;

   public:
    Object(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    virtual ~Object() = default;

    ContextPtr context() const { return m_context; }

    UIWeakRefPtr<Object> parent() const { return m_parent; }

    Constraint& constraint() { return m_constraint; }
    const Constraint& constraint() const { return m_constraint; }

    const Size size() const { return m_size; }

    bool is_visible() const { return m_visible; }

    virtual void set_visible(const bool value);

    /// Generate draw commands for this obect
    virtual void paint(Painter& painter, const PaintParams& params) const = 0;

    void eval_constraints() { eval_constraints(constraint_params_from_object(this)); }

    virtual void eval_constraints(const ConstraintParams& params);

    /// React to Input Event
    /// Return true if event was consumed
    virtual bool on_event(const Event& event, const Offset offset);

    void set_parent(const UIWeakRefPtr<Object>& parent);
};

}    // namespace modus::ui
