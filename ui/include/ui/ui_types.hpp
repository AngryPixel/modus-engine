/*
 * Copyright 2020-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <core/fixed_vector.hpp>
namespace modus::ui {

struct MODUS_UI_EXPORT Size {
    f32 width;
    f32 height;
};

struct MODUS_UI_EXPORT Offset {
    f32 x;
    f32 y;
};

struct MODUS_UI_EXPORT Rect {
    f32 x;
    f32 y;
    f32 width;
    f32 height;

    bool contains(const f32 px, const f32 py) const {
        return px >= x && py >= y && px <= x + width && py <= y + height;
    }

    bool intersects(const Rect& rect) const {
        //   l +----+
        //     |    |
        //     +----+ r
        const Offset l1 = {x, y};
        const Offset r1 = {x + width, y + height};
        const Offset l2 = {rect.x, rect.y};
        const Offset r2 = {rect.x + rect.width, rect.y + rect.height};

        if (l1.x >= r2.x || l2.x >= r1.x) {
            return false;
        }

        if (l1.y >= r2.y || l2.y >= r1.y) {
            return false;
        }
        return true;
    }
};

inline Offset operator+(const Offset o1, const Offset o2) {
    return {o1.x + o2.x, o1.y + o2.y};
}

inline Offset operator-(const Offset o1, const Offset o2) {
    return {o1.x - o2.x, o1.y - o2.y};
}

inline Rect make_rect(const Offset& offset, const Size& size) {
    return {offset.x, offset.y, size.width, size.height};
}

class Object;
class Context;
struct Constraint;
class ConstantValue;
class FlexValue;
class IncomingValue;

using ContextPtr = NotMyPtr<Context>;
using ObjectPtr = NotMyPtr<Object>;
using ConstObjectPtr = NotMyPtr<const Object>;
struct Event;
class View;

struct Color;
class Painter;
struct PaintParams;
class PainterBitmap2D;

class Font;
struct TextSizeCalculator;
struct TextVertexDataGenerator;

enum class HorizontalAlignment : u8 { Left, Center, Right };
enum class VerticalAlignment : u8 { Top, Center, Bottom };

static inline f32 get_alignment_offset(const HorizontalAlignment alignment,
                                       const f32 text_size,
                                       const f32 total_size) {
    if (text_size >= total_size) {
        return 0.f;
    }
    if (alignment == HorizontalAlignment::Right) {
        return total_size - text_size;
    } else if (alignment == HorizontalAlignment::Center) {
        return (total_size - text_size) / 2.f;
    } else {
        return 0.f;
    }
}

static inline f32 get_alignment_offset(const VerticalAlignment alignment,
                                       const f32 text_size,
                                       const f32 total_size) {
    if (text_size >= total_size) {
        return 0.f;
    }
    if (alignment == VerticalAlignment::Bottom) {
        return total_size - text_size;
    } else if (alignment == VerticalAlignment::Center) {
        return (total_size - text_size) / 2.0f;
    } else {
        return 0.f;
    }
}

}    // namespace modus::ui
