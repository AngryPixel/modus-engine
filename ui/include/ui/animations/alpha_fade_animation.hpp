/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/animation.hpp>

namespace modus::ui {

class MODUS_UI_EXPORT AlphaFadeOutAnimation final : public Animation {
   public:
    u8 m_value;
    void animate(const f64 interval) override { m_value = 255 - static_cast<u8>(255.0 * interval); }
};

class MODUS_UI_EXPORT AlphaFadeInAnimation final : public Animation {
   public:
    u8 m_value;
    void animate(const f64 interval) override { m_value = static_cast<u8>(255.0 * interval); }
};
}    // namespace modus::ui
