/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

namespace modus::ui {

enum class EventType {
    ContextResize,
    MouseMove,
    MouseButton,
    MouseScroll,
    ContextMouseLeave,
    ContextMouseEnter
};

struct MODUS_UI_EXPORT ContextResizeEvent {
    f32 width;
    f32 height;
};

struct MODUS_UI_EXPORT MouseMoveEvent {
    f32 x;
    f32 y;
};

struct MODUS_UI_EXPORT MouseScrollEvent {
    i32 x;
    i32 y;
};

enum class MouseButton : u8 { Left, Right, Middle };

struct MODUS_UI_EXPORT MouseButtonEvent {
    MouseButton button;
    bool is_down;
    u8 padding__;
    u8 padding2__;
    f32 x;
    f32 y;
};

struct MODUS_UI_EXPORT Event {
    EventType type;
    union {
        ContextResizeEvent context_resize;
        MouseMoveEvent mouse_move;
        MouseButtonEvent mouse_button;
        MouseScrollEvent mouse_scroll;
    };
};
}    // namespace modus::ui
