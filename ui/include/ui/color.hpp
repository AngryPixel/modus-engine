/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

namespace modus::ui {

struct MODUS_UI_EXPORT RGBA {
    u8 r;
    u8 g;
    u8 b;
    u8 a;
};
struct MODUS_UI_EXPORT Color {
    union {
        RGBA v;
        u32 u;
    };

    Color srgb_to_linear() const;
    Color linear_to_srgb() const;
    glm::vec4 normalize() const;

    /// Interpolate two colors.
    /// @param interval Interpolation interval [0.0-1.0]
    static Color interpolate(const Color& from, const Color& to, const f32 interval);
};

MODUS_UI_EXPORT Color make_color(const u8 r, const u8 g, const u8 b);
MODUS_UI_EXPORT Color make_color(const u8 r, const u8 g, const u8 b, const u8 a);
MODUS_UI_EXPORT Color make_color(const glm::vec4& color);
// From hex for 0xRRGGBBAA
MODUS_UI_EXPORT Color make_color(const u32 hex);
}    // namespace modus::ui

template <>
struct fmt::formatter<modus::ui::Color> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::ui::Color& color, FormatContext& ctx) {
        return format_to(ctx.out(), "Color{{ r:{}, g:{}, b:{}, a:{} }}", color.v.r, color.v.g,
                         color.v.b, color.v.a);
    }
};
