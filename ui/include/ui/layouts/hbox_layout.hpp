/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <ui/object.hpp>

namespace modus::ui {

/// Sort chidren in a horizontal box.
/// Inner margin is the space between each child and Outer margin is the space the box layout should
/// maintain relative to its parent.
class MODUS_UI_EXPORT HBoxLayout final : public Object {
   private:
    Vector<UIRefPtr<Object>> m_children;
    f32 m_margin_inner;
    f32 m_margin_outer;

   public:
    HBoxLayout(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    void add_child(const UIRefPtr<Object>& child);

    void remove_child(ConstObjectPtr child);

    void eval_constraints(const ConstraintParams& params) override;

    ObjectPtr child(const size_t index);

    ConstObjectPtr child(const size_t index) const;

    size_t child_count() const { return m_children.size(); }

    void clear() { m_children.clear(); }

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

    void set_margin_inner(const f32 margin_px);

    void set_margin_outer(const f32 margin_px);
};

}    // namespace modus::ui
