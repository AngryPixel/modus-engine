/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <core/delegate.hpp>
#include <ui/objects/vscroll_bar.hpp>

namespace modus::ui {

/// Vertical Scrollable layout
class MODUS_UI_EXPORT VScrollLayout final : public Object {
   private:
    VScrollBar m_scroll_bar;
    UIRefPtr<Object> m_child;
    Offset m_scroll_offset;

   public:
    Delegate<void(VScrollLayout&, const f32)> m_on_scrolled;

    VScrollLayout(ContextPtr ctx, const UIWeakRefPtr<Object>& parent = UIWeakRefPtr<Object>());

    ~VScrollLayout();

    UIRefPtr<Object> child() const { return m_child; }

    void set_child(const UIRefPtr<Object>& object);

    void eval_constraints(const ConstraintParams& params) override;

    bool on_event(const Event& event, const Offset offset) override;

    void paint(Painter& painter, const PaintParams& params) const override;

   private:
    void on_scroll(const f32 scroll_percentage);

    void update_scroll_offset();
};

}    // namespace modus::ui
