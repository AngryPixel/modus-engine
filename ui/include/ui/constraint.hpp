/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <core/strong_type.hpp>

namespace modus::ui {

constexpr const f32 kInfiniteValue = std::numeric_limits<f32>::max();
MODUS_DECLARE_STRONG_TYPE(ConstantValue, f32, 0.0f);
MODUS_DECLARE_STRONG_TYPE(FlexValue, f32, 1.0f);
MODUS_DECLARE_STRONG_TYPE(IncomingValue, f32, 1.0f);

inline bool is_inifinte(const ConstantValue& v) {
    return !(v.value() < kInfiniteValue);
}
inline bool is_inifinte(const f32 v) {
    return !(v < kInfiniteValue);
}

struct MODUS_UI_EXPORT ConstraintParams {
    Size parent_size;
    Size flex_factor;
};

MODUS_UI_EXPORT ConstraintParams constraint_params_from_object(ConstObjectPtr object);

class MODUS_UI_EXPORT ConstraintValue {
   private:
    enum class Type : u8 { Constant, Flex, Incoming };
    f32 m_value;
    Type m_type;

   public:
    static const ConstantValue kInfiniteValue;

    ConstraintValue();

    ConstraintValue(const ConstantValue value);
    ConstraintValue& operator=(const ConstantValue value);

    ConstraintValue(const FlexValue value);
    ConstraintValue& operator=(const FlexValue value);

    ConstraintValue(const IncomingValue);
    ConstraintValue& operator=(const IncomingValue);

    bool is_flex() const { return m_type == Type::Flex; }
    bool is_incoming() const { return m_type == Type::Incoming; }
    bool is_constant() const { return m_type == Type::Constant; }

    f32 flex_value() const {
        modus_assert(is_flex());
        return m_value;
    }
    f32 incoming_value() const {
        modus_assert(is_incoming());
        return m_value;
    }
    f32 constant_value() const {
        modus_assert(is_constant());
        return m_value;
    }

    f32 transform(const f32 incoming) const;
};

struct MODUS_UI_EXPORT Constraint {
    ConstraintValue min_width = ConstantValue();
    ConstraintValue min_height = ConstantValue();
    ConstraintValue max_width = IncomingValue();
    ConstraintValue max_height = IncomingValue();

    Size resolve(const ConstraintParams& params) const;
};

MODUS_UI_EXPORT bool is_flex_constraint(const Constraint& c);

}    // namespace modus::ui
