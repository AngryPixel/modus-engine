/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <ui/tickable.hpp>

namespace modus::ui {

class Timer;
using TimerDelegate = Delegate<void(Timer&)>;

class MODUS_UI_EXPORT Timer {
   private:
    ContextPtr m_context;
    IMilisec m_elapsed_ms;
    IMilisec m_target_ms;
    TickableHandle m_handle;

   public:
    Delegate<void(Timer&)> m_on_trigger;

    Timer(ContextPtr ctx);

    ~Timer();

    bool finished() const { return m_elapsed_ms >= m_target_ms; }

    void reset();

    void set_duration_ms(const IMilisec target_ms) { m_target_ms = target_ms; }

    IMilisec duration_ms() const { return m_target_ms; }

    void cancel();

    void repeat();

    void start();

    IMilisec elapsed_ms() const { return m_elapsed_ms; }

   private:
    void on_tick(const IMilisec tick_ms);
};
}    // namespace modus::ui
