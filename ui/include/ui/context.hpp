/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <ui/painter.hpp>
#include <ui/tickable.hpp>
#include <ui/view_controller.hpp>
namespace modus::ui {

class Font;
class MODUS_UI_EXPORT Context {
   private:
    ViewController m_view_controller;
    UIRefPtr<View> m_root_view;
    Size m_drawable_size;
    mutable Painter m_painter;
    Offset m_mouse_location;
    TickableManager m_tickable_manager;
    UIRefPtr<Font> m_default_font;
    bool m_visible;

   public:
    Context(NotMyPtr<PainterDevice> painter_device);

    ~Context();
    MODUS_CLASS_DISABLE_COPY_MOVE(Context);

    Result<> initialize();

    void shutdown();

    template <typename T, typename... Args>
    UIRefPtr<T> create_object(Args&&... args) {
        static_assert(std::is_base_of_v<Object, T>);
        return make_refptr_with_allocator<T, UIAllocator>(this, std::forward<Args>(args)...);
    }

    template <typename T, typename... Args>
    UIRefPtr<T> alloc(Args&&... args) {
        return make_refptr_with_allocator<T, UIAllocator>(std::forward<Args>(args)...);
    }

    ViewController& view_controller() { return m_view_controller; }

    void set_root_view(const UIRefPtr<View>& view);

    void update(const IMilisec elapsed_ms);

    void paint(const bool clear) const;

    bool on_event(const Event& evt);

    Size drawable_size() const { return m_drawable_size; }

    void set_drawable_size(const Size& size);

    Painter& painter() { return m_painter; }

    Offset mouse_location() const { return m_mouse_location; }

    TickableManager& tickable_manager() { return m_tickable_manager; }

    const UIRefPtr<Font>& default_font() const { return m_default_font; }

    void set_default_font(const UIRefPtr<Font>& font);

    void set_visible(const bool visible);

    bool visible() const { return m_visible; }
};

}    // namespace modus::ui
