/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/ui.hpp>

#include <core/delegate.hpp>
#include <core/handle_pool.hpp>

namespace modus::ui {

using TickableDelegate = Delegate<void(const IMilisec)>;
struct Tickable {
    TickableDelegate m_on_tick;
    Tickable() = default;
    Tickable(TickableDelegate delegate) : m_on_tick(delegate) {}
};

MODUS_DECLARE_HANDLE_TYPE(TickableHandle, std::numeric_limits<u32>::max());
class MODUS_UI_EXPORT TickableManager {
   private:
    using PoolType = HandlePool<Tickable, UIAllocator, TickableHandle>;
    PoolType m_tickables;

   public:
    TickableManager();

    TickableHandle create(TickableDelegate delegate);

    void destroy(TickableHandle handle);

    void update(const IMilisec tick_ms);
};
}    // namespace modus::ui
