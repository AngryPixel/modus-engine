/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <os/os_pch.h>
#include <ui/ui_pch.h>
// clang-format on

#include <ui/font_generated.h>
#include <freetype/ftglyph.h>
#include <ft2build.h>

#include <core/getopt.hpp>
#include <iostream>
#include <os/file.hpp>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

using namespace modus;
using namespace modus::ui;

struct RectScope {
    i32 x = 0;
    i32 y = 0;
    i32 w = 0;
    i32 h = 0;
    i32 dw = 0;
    i32 dh = 0;
};

// Get value inside the rect scope.
u8 rect_scope_get(const RectScope& rs, Slice<u8> data, const i32 x, const i32 y) {
    modus_assert(x < rs.w);
    modus_assert(y < rs.h);
    const i32 data_x = rs.x + x;
    const i32 data_y = rs.y + y;
    return data[data_y * rs.dw + data_x];
}

void rect_scope_set(const RectScope& rs, SliceMut<u8> data, const i32 x, const i32 y, const u8 v) {
    modus_assert(x < rs.w);
    modus_assert(y < rs.h);
    const i32 data_x = rs.x + x;
    const i32 data_y = rs.y + y;
    data[data_y * rs.dw + data_x] = v;
}

class FreeTypeScope {
   private:
    FT_Library& m_ft;

   public:
    FreeTypeScope(FT_Library& ft) : m_ft(ft) {}

    ~FreeTypeScope() { FT_Done_FreeType(m_ft); }
};

class FreeTypeFaceScope {
   private:
    FT_Face& m_ft;

   public:
    FreeTypeFaceScope(FT_Face& ft) : m_ft(ft) {}

    ~FreeTypeFaceScope() { FT_Done_Face(m_ft); }
};

struct FontOptions {
    i32 padding_px = 2;
    i32 font_size_px = 12;
};

struct GlypInfo {
    i32 index;                 // sorting index / asci character
    i32 width;                 // glyph width
    i32 height;                // glyph height
    i32 bearing_x;             // distance form origin left
    i32 bearing_y;             // height from the base line
    i32 advance_x;             // distance to next character
    f32 tex_top;               // Texture Rect  top
    f32 tex_left;              // Texture Rect left
    f32 tex_right;             // Texture Rect right
    f32 tex_bottom;            // Texture Rect bottom
    std::vector<u8> buffer;    // stored glyph images
};

static bool fits_into_atlas_size(const u32 atlas_size_px,
                                 const u32 padding_px,
                                 const Slice<GlypInfo> glyph_infos) {
    u32 current_x = 0;
    u32 current_y = 0;
    u32 current_height = glyph_infos[0].height;
    for (auto& glyph_info : glyph_infos) {
        const u32 width = glyph_info.width;
        const u32 height = glyph_info.height;
        // check if we need to move to the next row
        if (current_x + width + padding_px * 2 >= atlas_size_px) {
            current_x = 0;
            current_y += padding_px * 2 + current_height;
            current_height = height;
        }
        // Calculate Glyph rect in buffer
        u32 left = current_x;
        u32 top = current_y;
        left += padding_px;
        top += padding_px;
        u32 right = left + width;
        u32 bottom = top + height;
        if (right >= atlas_size_px || bottom >= atlas_size_px) {
            return false;
        }
        current_x += width + padding_px * 2;
    }
    return true;
}

u32 calculate_atlas_size(const u32 padding_px, const Slice<GlypInfo> glyph_infos) {
    u32 atlas_size_px = 32;
    while (!fits_into_atlas_size(atlas_size_px, padding_px, glyph_infos)) {
        atlas_size_px <<= 1;
    }
    return atlas_size_px;
}

static const StringSlice kIntro = "modus_mk_font -i input -o output";
int main(const int argc, const char** argv) {
    CmdOptionString opt_input("-i", "--input", "Input font file", "");
    CmdOptionString opt_output("-o", "--output", "Path to the generated file", "");
    CmdOptionU32 opt_atlas_font_size("-f", "--font-size-px", "Font size in pixels. Default = 12",
                                     12);
    CmdOptionU32 opt_atlas_padding("-p", "--font-padding-px",
                                   "Padding in pixels between glyphs on the atlas. Default = 2", 2);
    CmdOptionString opt_debug_image(
        "", "--save-atlas-image", "Path to where the generate atlas image should be generated", "");
    CmdOptionParser opt_parser;

    opt_parser.add(opt_input).expect("failed to add option to parser");
    opt_parser.add(opt_output).expect("failed to add option to parser");
    opt_parser.add(opt_atlas_font_size).expect("failed to add option to parser");
    opt_parser.add(opt_atlas_padding).expect("failed to add option to parser");
    opt_parser.add(opt_debug_image).expect("failed to add option to parser");

    if (!opt_parser.parse(argc, argv) || !opt_input.parsed() || !opt_output.parsed()) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    // Init Freetype
    FT_Library ft;
    if (FT_Init_FreeType(&ft) != 0) {
        std::cerr << "Failed to initialize Freetype \n";
    }
    // Scope guard
    FreeTypeScope ft_scope(ft);

    // open font
    FT_Face face;
    if (FT_New_Face(ft, opt_input.value_cstr(), 0, &face) != 0) {
        std::cerr << fmt::format("Failed to open font {}\n", opt_input.value());
        return EXIT_FAILURE;
    }
    FreeTypeFaceScope face_scope(face);

    FontOptions font_options;
    font_options.font_size_px = opt_atlas_font_size.value();
    font_options.padding_px = opt_atlas_padding.value();

    modus::Vector<GlypInfo> glyph_infos;
    modus::Vector<RectScope> sdf_rects;

    // Set height in pixels for font
    FT_Set_Pixel_Sizes(face, 0, font_options.font_size_px);

    u32 max_height = 0;
    u32 max_width = 0;
    const u32 line_height = (face->size->metrics.ascender - face->size->metrics.descender) >> 6;
    // Load all chars from 32 to 128
    glyph_infos.reserve(127 - 32);
    sdf_rects.reserve(127 - 32);

    i32 max_bearing_y_diff = 0;
    FT_Int32 ft_load_flags = FT_LOAD_RENDER;
    for (i32 i = 32; i < 127; ++i) {
        const FT_UInt char_index = FT_Get_Char_Index(face, i);
        if (char_index == 0) {
            std::cerr << fmt::format("Font does not have a valid glyphy for char: '{}':{:02}\n",
                                     char(i), i);
            return EXIT_FAILURE;
        }
        if (FT_Load_Glyph(face, char_index, ft_load_flags) != 0) {
            return EXIT_FAILURE;
        }

        GlypInfo glyph_info;
        glyph_info.index = i;
        glyph_info.height = face->glyph->bitmap.rows;
        glyph_info.width = face->glyph->bitmap.width;
        glyph_info.bearing_x = face->glyph->bitmap_left;
        glyph_info.bearing_y = face->glyph->bitmap_top;
        glyph_info.advance_x = face->glyph->advance.x >>
                               6;    // advance.x is 1/64, needs to be dived to get the right value

        max_height = std::max(max_height, face->glyph->bitmap.rows);
        max_width = std::max(max_width, face->glyph->bitmap.width);
        max_bearing_y_diff =
            std::max(max_bearing_y_diff, (glyph_info.height - glyph_info.bearing_y));
        // copy glyph data into memory because it seems to be overwritten/lost
        // later
        glyph_info.buffer.resize(glyph_info.width * glyph_info.height, '0');
        memcpy(glyph_info.buffer.data(), face->glyph->bitmap.buffer,
               face->glyph->bitmap.rows * face->glyph->bitmap.width);
        glyph_infos.push_back(std::move(glyph_info));
    }

    // check if monospace
    bool is_monospace = true;
    for (size_t i = 0; i < glyph_infos.size() - 1 && is_monospace; ++i) {
        is_monospace = is_monospace && (glyph_infos[i].advance_x == glyph_infos[i + 1].advance_x);
    }

    // sort glyphs by height
    std::sort(glyph_infos.begin(), glyph_infos.end(),
              [](const GlypInfo& g1, const GlypInfo& g2) { return g1.height > g2.height; });

    const u32 atlas_size_px =
        calculate_atlas_size(font_options.padding_px, make_slice(glyph_infos));
    const u32 atlas_buffer_size = atlas_size_px * atlas_size_px;
    std::unique_ptr<u8> atlas_buffer_ptr(new u8[atlas_buffer_size]);
    ByteSliceMut atlas_buffer(atlas_buffer_ptr.get(), atlas_buffer_size);
    // Set everything to 0
    memset(atlas_buffer_ptr.get(), 0, atlas_buffer_size);

    // Generate singe row font atlas
    u32 current_x = 0;
    u32 current_y = 0;
    u32 current_height = glyph_infos[0].height;
    for (auto& glyph_info : glyph_infos) {
        const u32 width = glyph_info.width;
        const u32 height = glyph_info.height;

        // check if we need to move to the next row
        if (current_x + width + font_options.padding_px * 2 >= atlas_size_px) {
            current_x = 0;
            current_y += font_options.padding_px * 2 + current_height;
            current_height = height;
        }

        // Calculate Glyph rect in buffer
        u32 left = current_x;
        u32 top = current_y;
        left += font_options.padding_px;
        top += font_options.padding_px;
        u32 right = left + width;
        u32 bottom = top + height;

        // Covert to relative location for texture coordinates
        const f32 tex_left = f32(left) / f32(atlas_size_px);
        const f32 tex_right = f32(right) / f32(atlas_size_px);
        const f32 tex_top = f32(top) / f32(atlas_size_px);
        const f32 tex_bottom = f32(bottom) / f32(atlas_size_px);

        glyph_info.tex_top = 1 - tex_top;
        glyph_info.tex_bottom = 1 - tex_bottom;
        glyph_info.tex_right = tex_right;
        glyph_info.tex_left = tex_left;

        /*
        std::cout << fmt::format("'{}' :: l:{} r:{} t:{} b:{}\n",
                char(i),
                tex_left,
                tex_right,
                tex_top,
                tex_bottom);*/

        for (u32 y = 0; y < height; ++y) {
            for (u32 x = 0; x < width; ++x) {
                const u32 buffer_x = current_x + x + font_options.padding_px;
                const u32 buffer_y = current_y + y + font_options.padding_px;
                const u32 glyph_index = (y * width) + x;
                const u32 buffer_index = (buffer_y * atlas_size_px) + buffer_x;
                atlas_buffer[buffer_index] = glyph_info.buffer[glyph_index];
            }
        }
        current_x += width + font_options.padding_px * 2;
    }

    // Write debug image if requested
    if (opt_debug_image.parsed()) {
        // write png output
        if (!stbi_write_png(opt_debug_image.value_cstr(), atlas_size_px, atlas_size_px, 1,
                            atlas_buffer.data(), 0)) {
            std::cerr << fmt::format("Could not write atlas image to '{}'\n",
                                     opt_debug_image.value_cstr());
            return EXIT_SUCCESS;
        }
    }

    // flip image for Renderer
    const u32 width_in_bytes = atlas_size_px;
    const u32 half_height = atlas_size_px / 2;

    for (u32 row = 0; row < half_height; row++) {
        u8* top = atlas_buffer.data() + row * width_in_bytes;
        u8* bottom = atlas_buffer.data() + (atlas_size_px - row - 1) * width_in_bytes;
        for (u32 col = 0; col < width_in_bytes; col++) {
            std::swap(*top, *bottom);
            top++;
            bottom++;
        }
    }

    ui::fbs::FontMetaData wrapper;
    auto fbs_render_info = wrapper.mutable_info();

    for (const auto& glyph_info : glyph_infos) {
        const u32 info_index = glyph_info.index - 32;
        auto render_info = fbs_render_info->GetMutablePointer(info_index);
        const auto& glyph_info_i = glyph_info;
        render_info->mutate_width(glyph_info_i.width);
        render_info->mutate_height(glyph_info_i.height);
        render_info->mutate_bearing_x(glyph_info_i.bearing_x);
        render_info->mutate_bearing_y(glyph_info_i.bearing_y);
        render_info->mutate_advance_x(glyph_info_i.advance_x);
        render_info->mutate_tex_top(glyph_info_i.tex_top);
        render_info->mutate_tex_bottom(glyph_info_i.tex_bottom);
        render_info->mutate_tex_left(glyph_info_i.tex_left);
        render_info->mutate_tex_right(glyph_info_i.tex_right);
    }

    // Prepare font header
    ui::fbs::FontHeader fbs_font_header;
    fbs_font_header.mutate_font_size_px(font_options.font_size_px);
    fbs_font_header.mutate_atlas_width_px(atlas_size_px);
    fbs_font_header.mutate_atlas_height_px(atlas_size_px);
    fbs_font_header.mutate_max_width_px(max_width);
    fbs_font_header.mutate_max_height_px(max_height);
    fbs_font_header.mutate_line_height_px(line_height);
    fbs_font_header.mutate_max_bearing_y_diff_px(max_bearing_y_diff);
    fbs_font_header.mutate_is_monospace(true);

    // Fill up GUID
    flatbuffers::FlatBufferBuilder fbs_builder;
    auto fbs_data = fbs_builder.CreateVector(atlas_buffer.data(), atlas_buffer.size());
    auto fbs_font = ui::fbs::CreateFont(fbs_builder, &fbs_font_header, &wrapper, fbs_data);

    fbs_builder.Finish(fbs_font, "UIFT");

    auto r_file = os::FileBuilder().write().create().binary().open(opt_output.value_cstr());
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {} for writing.\n", opt_output.value());
        return EXIT_FAILURE;
    }

    os::File file = r_file.release_value();

    const ByteSlice data_slice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = file.write(data_slice);
    if (!r_write || r_write.value() != data_slice.size()) {
        std::cerr << fmt::format("Failed to write data to {} .\n", opt_output.value());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
