/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <os/os_pch.h>
#include <ui/ui_pch.h>
#include <app/app_pch.h>
#include <threed/threed_pch.h>
// clang-format on

#include <ui/painters/threed/threed_painter.hpp>
#include <ui/event.hpp>
#include <ui/context.hpp>

#include "app/input.hpp"
#include "common.hpp"
#include <app/app.hpp>
#include <app/event.hpp>
#include <threed/device.hpp>
#include <threed/compositor.hpp>
#include <threed/pipeline.hpp>
#include <iostream>

namespace modus::ui {

UITestApp::UITestApp() {
    m_app = modus::app::create_app();
}

UITestApp::~UITestApp() {
    if (m_context) {
        m_context->shutdown();
    }

    if (m_device) {
        m_device->shutdown();
    }

    if (m_app) {
        (void)m_app->shutdown();
    }
}

Result<> UITestApp::initialize(StringSlice name, const i32 width, const i32 height) {
    app::AppInitParams init_params;
    const String app_pref_name = "modus.test.ui." + name.to_str();
    init_params.name = name;
    init_params.name_preferences = app_pref_name;
    init_params.event_handler.bind<UITestApp, &UITestApp::on_event>(this);

    if (!m_app->initialize(init_params)) {
        std::cerr << "Failed to initialize app";
        return Error<>();
    }

    app::WindowCreateParams window_params;
    window_params.title = name;
    window_params.width = width;
    window_params.height = height;
    window_params.framebuffer.red_bits = 8;
    window_params.framebuffer.green_bits = 8;
    window_params.framebuffer.blue_bits = 8;
    window_params.framebuffer.depth_bits = 24;

    if (auto r = m_app->create_window(window_params); !r) {
        std::cerr << "Failed to create window\n";
        return Error<>();
    } else {
        m_window = *r;
    }

    m_device = m_app->threed_instance().create_device();
    if (!m_device->initialize()) {
        std::cerr << "Failed to initialize device\n";
        return Error<>();
    }

    if (auto r = m_window->create_compositor(*m_device, false); !r) {
        std::cerr << "Failed to create compositor\n";
        return Error<>();
    } else {
        m_compositor = *r;
    }

    m_painter_device =
        modus::make_unique<ThreedPainterDeviceStandalone>(m_device.get(), m_compositor);
    m_context = modus::make_unique<Context>(m_painter_device.get());

    if (!m_context->initialize()) {
        std::cerr << "Failed to init compositor\n";
        return Error<>();
    }

    m_context->set_drawable_size({f32(width), f32(height)});
    m_app_name = name.to_str();
    m_timer.reset();
    return Ok<>();
}

void UITestApp::run() {
    m_fps_tick = IMilisec(0);
    m_fps = 0.0f;
    m_frame_count = 0;
    m_prev_ms = m_timer.elapsed_miliseconds();
    while (true) {
        const IMilisec elapsed_ms = m_timer.elapsed_miliseconds();
        const IMilisec tick_ms = elapsed_ms - m_prev_ms;
        m_prev_ms = elapsed_ms;
        m_fps_tick += tick_ms;
        if (m_fps_tick > IMilisec(500)) {
            m_fps = 2.0 * f64(m_frame_count) / (f64(m_fps_tick.count()) / 500.0);
            m_frame_count = 0;
            m_fps_tick = IMilisec(0);
            m_window->set_title(modus::format("{}: {:03.2f}", m_app_name, m_fps));
        }
        m_frame_count++;
        if (m_app->tick() == app::TickResult::Quit) {
            break;
        }
        m_context->update(tick_ms);
        m_context->paint(true);
    }
}

bool UITestApp::on_event(const app::Event& event) {
    MODUS_UNUSED(event);
    ui::Event ui_event;
    switch (event.type) {
        case app::EventType::Window: {
            switch (event.window.type) {
                case app::WindowEventType::Resize: {
                    ui_event.type = EventType::ContextResize;
                    ui_event.context_resize.width = event.window.resize.width;
                    ui_event.context_resize.height = event.window.resize.height;
                    return m_context->on_event(ui_event);
                }
                case app::WindowEventType::Enter: {
                    ui_event.type = EventType::ContextMouseEnter;
                    return m_context->on_event(ui_event);
                }
                case app::WindowEventType::Leave: {
                    ui_event.type = EventType::ContextMouseLeave;
                    return m_context->on_event(ui_event);
                }
                default:
                    return false;
            }
        }

        case app::EventType::Input: {
            switch (event.input.type) {
                case app::InputEventType::MouseMove: {
                    ui_event.type = EventType::MouseMove;
                    ui_event.mouse_move.x = event.input.mouse_move.x;
                    ui_event.mouse_move.y = event.input.mouse_move.y;
                    return m_context->on_event(ui_event);
                }
                case app::InputEventType::MouseButton: {
                    switch (event.input.mouse_button.button) {
                        case app::MouseButton::Left:
                            ui_event.mouse_button.button = MouseButton::Left;
                            break;
                        case app::MouseButton::Middle:
                            ui_event.mouse_button.button = MouseButton::Middle;
                            break;
                        case app::MouseButton::Right:
                            ui_event.mouse_button.button = MouseButton::Right;
                        default:
                            return false;
                    }
                    ui_event.type = EventType::MouseButton;
                    ui_event.mouse_button.x = event.input.mouse_button.x;
                    ui_event.mouse_button.y = event.input.mouse_button.y;
                    ui_event.mouse_button.is_down = event.input.mouse_button.is_mouse_down();
                    return m_context->on_event(ui_event);
                }
                case app::InputEventType::MouseScroll: {
                    ui_event.type = EventType::MouseScroll;
                    ui_event.mouse_scroll.x = event.input.mouse_scroll.xvalue;
                    ui_event.mouse_scroll.y = event.input.mouse_scroll.yvalue;
                    return m_context->on_event(ui_event);
                }
                default:
                    return false;
            }
        }

        default:
            return false;
    }

    // m_context->on_event()
}

}    // namespace modus::ui
