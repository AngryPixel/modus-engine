/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <os/time.hpp>

namespace modus::app {
class App;
struct Event;
class Window;
}    // namespace modus::app

namespace modus::threed {
class Compositor;
class Device;
}    // namespace modus::threed

namespace modus::ui {
class PainterDevice;
class Context;
class UITestApp {
   protected:
    String m_app_name;
    std::unique_ptr<app::App> m_app;
    NotMyPtr<app::Window> m_window;
    std::unique_ptr<threed::Device> m_device;
    NotMyPtr<threed::Compositor> m_compositor;
    std::unique_ptr<PainterDevice> m_painter_device;
    std::unique_ptr<Context> m_context;
    os::Timer m_timer;
    IMilisec m_prev_ms;
    IMilisec m_fps_tick;
    f64 m_fps;
    u32 m_frame_count;

   public:
    UITestApp();

    ~UITestApp();

    Result<> initialize(StringSlice name, const i32 width, const i32 height);

    ui::Context& context() { return *m_context; }

    void run();

   protected:
    bool on_event(const app::Event& event);
};

}    // namespace modus::ui
