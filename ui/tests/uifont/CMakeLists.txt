
modus_add_test(test_uifont NAME UIFont)

target_sources(test_uifont PRIVATE
    test_uifont.cpp
)

target_compile_definitions(test_uifont PRIVATE
    ${test_font_define}
)
add_dependencies(test_uifont gen_test_uifont)

target_link_libraries(test_uifont PRIVATE ui_tests_common)

