/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <os/os_pch.h>
#include <ui/ui_pch.h>

#include <os/file.hpp>
#include <ui/context.hpp>
#include <ui/painters/null_painter.hpp>
#include <ui/text/font.hpp>
#include <ui/text/text_utils.hpp>

#include <catch2/catch.hpp>

using namespace modus;

TEST_CASE("Load Font", "[Font]") {
    auto painter_device = std::make_unique<ui::NullPainterDevice>();
    modus::ui::Context context(painter_device.get());
    REQUIRE(context.initialize());
    {
        auto r_file = os::FileBuilder().read().open(TEST_FONT_PATH);
        REQUIRE(r_file);
        auto r = ui::Font::create(&context, *r_file);
        REQUIRE(r);
        ui::UIRefPtr<ui::Font> font = r.release_value();

        CHECK(font->is_mono_spaced());
        CHECK(font->font_size_px() == 14);
    }
    context.shutdown();
}

TEST_CASE("Text Length", "[Font]") {
    auto painter_device = std::make_unique<ui::NullPainterDevice>();
    modus::ui::Context context(painter_device.get());
    REQUIRE(context.initialize());
    {
        auto r_file = os::FileBuilder().read().open(TEST_FONT_PATH);
        REQUIRE(r_file);
        auto r = ui::Font::create(&context, *r_file);
        REQUIRE(r);
        ui::UIRefPtr<ui::Font> font = r.release_value();

        const f32 space_size_px = font->glyph_info_for_char(' ').advance_x_px;

        {
            ui::TextSizeCaculator calculator;
            const StringSlice text = "Hello!";
            const ui::Size size = calculator.text_size(*font, text);
            CHECK(size.width == f32(space_size_px * text.size()));
            CHECK(size.height == font->line_height_px());
        }
        {
            // multi line disabled
            ui::TextSizeCaculator calculator;
            const StringSlice text = "Hello!\nWorld";
            const ui::Size size = calculator.text_size(*font, text);
            CHECK(size.width == space_size_px * 6);
            CHECK(size.height == font->line_height_px());
        }
        {
            // multi line enabled
            ui::TextSizeCaculator calculator;
            calculator.allow_multiline = true;
            const StringSlice text = "Hello!\nWorld";
            const ui::Size size = calculator.text_size(*font, text);
            CHECK(size.width == space_size_px * 6);
            CHECK(size.height == font->line_height_px() * 2);
        }
        /* temporarily disabled since the logic is not shared
        {
            // multi line enabled + with wrapping
            ui::TextSizeCaculator calculator;
            calculator.allow_multiline = true;
            calculator.max_text_width = space_size_px * 6;
            const StringSlice text = "Hello! World!";
            const ui::Size size = calculator.text_size(*font, text);
            CHECK(size.width == space_size_px * 6);    // 1st line 'Hello!'
            CHECK(size.height == font->line_height_px() * 3);
        }
        {
            // multi line enabled + with wrapping + height limit
            ui::TextSizeCaculator calculator;
            calculator.allow_multiline = true;
            calculator.max_text_width = space_size_px * 6;
            calculator.max_text_height = u32(font->line_height_px() * 1.5);
            const StringSlice text = "Hello! World!";
            const ui::Size size = calculator.text_size(*font, text);
            CHECK(size.width == space_size_px * 6);    // 1st line 'Hello!'
            CHECK(size.height == font->line_height_px());
        }*/
    }
    context.shutdown();
}
