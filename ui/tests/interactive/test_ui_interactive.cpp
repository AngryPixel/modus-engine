/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <os/os_pch.h>
#include <log/log_pch.h>
#include <ui/ui_pch.h>
// clang-format on
#include <common.hpp>
#include <ui/context.hpp>
#include <ui/objects/null_object.hpp>
#include <ui/layouts/hbox_layout.hpp>
#include <ui/layouts/vbox_layout.hpp>
#include <ui/layouts/vscroll_layout.hpp>
#include <ui/views/object_view.hpp>
#include <ui/views/dialog_view.hpp>
#include <ui/objects/label.hpp>
#include <ui/objects/button.hpp>
#include <ui/objects/check_box.hpp>
#include <ui/text/text_utils.hpp>
#include <ui/objects/hslider.hpp>
#include <ui/objects/text_box.hpp>
#include <ui/objects/combo_box.hpp>
#include <ui/color.hpp>
#include <ui/painter.hpp>
#include <ui/text/font.hpp>
#include <ui/event.hpp>
#include <ui/timer.hpp>
#include <iostream>
#include <log/log.hpp>
#include <os/file.hpp>
#include <ui/animations/alpha_fade_animation.hpp>

#include <core/delegate.hpp>
#include "ui/animation.hpp"
#include "ui/constraint.hpp"
#include "ui/ui_types.hpp"
#include "ui/view.hpp"

using namespace modus;

static const char* kLongText = R"R(
sudo allows a permitted user to execute a command as the superuser or another user, as specified by the security policy.  The invoking user's real (not effective) user-ID is used to
determine the user name with which to query the security policy.

sudo supports a plugin architecture for security policies and input/output logging.  Third parties can develop and distribute their own policy and I/O logging plugins to work seam‐
lessly with the sudo front end.  The default security policy is sudoers, which is configured via the file /etc/sudoers, or via LDAP.  See the Plugins section for more information.

The security policy determines what privileges, if any, a user has to run sudo.  The policy may require that users authenticate themselves with a password or another authentication
mechanism.  If authentication is required, sudo will exit if the user's password is not entered within a configurable time limit.  This limit is policy-specific; the default password
prompt timeout for the sudoers security policy is 5 minutes.

Security policies may support credential caching to allow the user to run sudo again for a period of time without requiring authentication.  By default, the sudoers policy caches cre‐
dentials on a per-terminal basis for 5 minutes.  See the timestamp_type and timestamp_timeout options in sudoers(5) for more information.  By running sudo with the -v option, a user
can update the cached credentials without running a command.

When invoked as sudoedit, the -e option (described below), is implied.

Security policies may log successful and failed attempts to use sudo.  If an I/O plugin is configured, the running command's input and output may be logged as well.

The options are as follows)R";

using MouseStateTrackerDelegate = modus::Delegate<void()>;

struct MouseStateTracker {
    ui::MouseButton m_button = ui::MouseButton::Left;
    bool m_is_down = false;
    bool m_is_within_area = false;

    MouseStateTrackerDelegate m_on_mouse_enter;
    MouseStateTrackerDelegate m_on_mouse_leave;
    MouseStateTrackerDelegate m_on_mouse_click;

    bool update(const ui::Event& evt, const ui::Rect& rect, ui::ContextPtr context) {
        switch (evt.type) {
            case ui::EventType::MouseScroll: {
                const ui::Offset mouse_loc = context->mouse_location();
                if (!(mouse_loc.x >= rect.x && mouse_loc.x <= rect.x + rect.width &&
                      mouse_loc.y >= rect.y && mouse_loc.y <= rect.y + rect.height)) {
                    if (m_is_within_area) {
                        if (m_on_mouse_leave) {
                            m_on_mouse_leave();
                        }
                        m_is_within_area = false;
                        m_is_down = false;
                    }
                } else {
                    if (!m_is_within_area) {
                        if (m_on_mouse_enter) {
                            m_on_mouse_enter();
                        }
                        m_is_within_area = true;
                    }
                }
                return false;
            }
            case ui::EventType::MouseMove: {
                if (evt.mouse_move.x >= rect.x && evt.mouse_move.x <= rect.x + rect.width &&
                    evt.mouse_move.y >= rect.y && evt.mouse_move.y <= rect.y + rect.height) {
                    if (!m_is_within_area) {
                        if (m_on_mouse_enter) {
                            m_on_mouse_enter();
                        }
                        m_is_within_area = true;
                    }
                } else if (m_is_within_area) {
                    if (m_on_mouse_leave) {
                        m_on_mouse_leave();
                    }
                    m_is_within_area = false;
                    m_is_down = false;
                }
                return false;
            }
            case ui::EventType::MouseButton: {
                if (evt.mouse_button.x >= rect.x && evt.mouse_button.x <= rect.x + rect.width &&
                    evt.mouse_button.y >= rect.y && evt.mouse_button.y <= rect.y + rect.height) {
                    if (evt.mouse_button.is_down) {
                        if (m_is_down == false) {
                            m_is_down = true;
                        }
                    } else if (m_is_down) {
                        if (m_on_mouse_click) {
                            m_on_mouse_click();
                        }
                        m_is_down = false;
                    }
                    return true;
                } else {
                    m_is_down = false;
                    return false;
                }
            }

            case ui::EventType::ContextMouseLeave: {
                if (m_is_within_area) {
                    if (m_on_mouse_leave) {
                        m_on_mouse_leave();
                    }
                    m_is_within_area = false;
                    m_is_down = false;
                }
                return false;
            }
            default:
                return false;
        }
    }
};

class TestView final : public ui::DialogView {
   public:
    TestView(ui::ContextPtr ctx) : ui::DialogView(ctx) {
        auto button = ctx->create_object<ui::Button>();
        button->set_label_margin(4);
        button->set_text("Ok");
        button->constraint().max_width = ui::IncomingValue();
        button->constraint().max_height = ui::IncomingValue();
        button->m_on_clicked.bind<TestView, &TestView::on_button_clicked>(this);
        ui::DialogView::set_object(button);
        ui::DialogView::set_background_color(ui::make_color(0, 0, 0, 128));
    }

    void on_enter() override { std::cout << "Test view on enter\n"; }

    void on_exit() override { std::cout << "Test view on exit\n"; }

   private:
    void on_button_clicked(ui::Button&) { context()->view_controller().pop_view_animated(); }
};

void on_animation_finished(ui::Animator& anim) {
    anim.start(!anim.is_reversed());
}

void on_check_box_clicked(ui::CheckBox& cb) {
    std::cout << fmt::format("CheckBox: {}\n", cb.checked());
}

class TestObject final : public ui::Object {
   private:
    ui::Color m_color;
    ui::Color m_color_inactive;
    ui::Color m_color_hover;
    MouseStateTracker m_tracker;
    ui::UIRefPtr<TestView> m_test_view;
    ui::Animator m_animator;
    ui::UIRefPtr<ui::AlphaFadeInAnimation> m_animation;

   public:
    TestObject(ui::ContextPtr ctx, const ui::Color& color)
        : ui::Object(ctx),
          m_color(color),
          m_color_inactive(color),
          m_color_hover(ui::make_color(color.normalize() * glm::vec4(0.5f, 0.5f, 0.5f, 1.0f))),
          m_animator(ctx) {
        m_tracker.m_on_mouse_enter.bind<TestObject, &TestObject::on_mouse_enter>(this);
        m_tracker.m_on_mouse_leave.bind<TestObject, &TestObject::on_mouse_leave>(this);
        m_tracker.m_on_mouse_click.bind<TestObject, &TestObject::on_mouse_down>(this);
        m_test_view = ctx->create_object<TestView>();
        m_animator.m_on_finished.bind<on_animation_finished>();
        m_animation = ctx->alloc<ui::AlphaFadeInAnimation>();
        m_animator.start(m_animation);
    }

    void paint(ui::Painter& painter, const ui::PaintParams& params) const override {
        ui::Color c = m_color;
        c.v.a = m_animation->m_value;
        painter.draw_rect(ui::Rect{params.offset.x, params.offset.y, m_size.width, m_size.height},
                          params.z_index, c);
    }

    void on_mouse_enter() { m_color = m_color_hover; }

    void on_mouse_leave() { m_color = m_color_inactive; }

    void on_mouse_down() {
        std::cout << fmt::format("Clicked {}\n", m_color);
        context()->view_controller().push_view_animated(m_test_view);
    }

    bool on_event(const ui::Event& evt, const ui::Offset offset) override {
        return m_tracker.update(evt, ui::make_rect(offset, m_size), context());
    }
};

void on_click(ui::Button& b) {
    std::cout << "Button clicked:" << b.text().to_str() << '\n';
}

void on_timer_trigger(ui::Timer& timer) {
    // std::cout << "Timer triggered\n";
    timer.repeat();
}

struct TestComboBoxData final : public ui::ComboBoxData {
    std::vector<std::string> m_values;

    TestComboBoxData() {
        for (u32 i = 0; i < 100; ++i) {
            m_values.push_back(fmt::to_string(i));
        }
    }

    u32 item_count() const override { return m_values.size(); }

    StringSlice item_display(const u32 index) const override {
        return StringSlice(m_values[index]);
    }
};

static void setup_layout(ui::Context& ctx) {
    auto view = ctx.create_object<ui::ObjectView>();
    auto layout = ctx.create_object<ui::HBoxLayout>();
    layout->set_margin_outer(4);
    layout->set_margin_inner(4);
    view->set_object(layout);
    ctx.set_root_view(view);
    ui::UIRefPtr<ui::Font> font;
    auto file = os::FileBuilder().read().open(TEST_FONT_PATH).value_or_panic("Failed to open font");
    font = ui::Font::create(&ctx, file).value_or_panic("Failed to load font");
    ctx.set_default_font(font);

    // Left side
    {
        auto vscroll = ctx.create_object<ui::VScrollLayout>();

        auto vlayout = ctx.create_object<ui::VBoxLayout>();
        vlayout->set_margin_inner(4);
        vlayout->constraint().min_height = ui::ConstantValue(40);
        vlayout->constraint().min_width = ui::ConstantValue(40);
        vlayout->constraint().max_height = ui::ConstantValue(ui::kInfiniteValue);
        vlayout->constraint().max_width = ui::IncomingValue();

        auto child1 = ctx.create_object<TestObject>(ui::make_color(0xbb, 0xd1, 0x96));
        child1->constraint().min_height = ui::ConstantValue(40);
        child1->constraint().min_width = ui::ConstantValue(40);
        child1->constraint().max_height = ui::ConstantValue(1024);
        child1->constraint().max_width = ui::IncomingValue();
        auto child2 = ctx.create_object<TestObject>(ui::make_color(0xbb, 255, 0x96));
        child2->constraint().min_height = ui::ConstantValue(40);
        child2->constraint().min_width = ui::ConstantValue(40);
        child2->constraint().max_height = ui::ConstantValue(1024);
        child2->constraint().max_width = ui::IncomingValue();

        vlayout->add_child(child1);
        vlayout->add_child(child2);
        vscroll->set_child(vlayout);
        layout->add_child(vscroll);
    }
    {
        auto vlayout = ctx.create_object<ui::VBoxLayout>();
        vlayout->set_margin_inner(4);
        vlayout->add_child(ctx.create_object<TestObject>(ui::make_color(0x56, 0x55, 0x6e)));

        auto combo_box = ctx.create_object<ui::ComboBox>();
        combo_box->constraint().max_width = ui::IncomingValue();
        combo_box->constraint().max_height = ui::ConstantValue(30);

        combo_box->set_data(ctx.alloc<TestComboBoxData>());
        vlayout->add_child(combo_box);
        layout->add_child(vlayout);
    }

    {
        auto vlayout = ctx.create_object<ui::VBoxLayout>();
        vlayout->set_margin_inner(4);

        auto vobject1 = ctx.create_object<ui::TextBox>();
        vobject1->constraint().min_height = ui::ConstantValue(30);
        vobject1->constraint().max_height = ui::FlexValue();
        vobject1->constraint().min_width = ui::ConstantValue(30);
        vobject1->constraint().max_width = ui::IncomingValue();
        vobject1->set_text(kLongText);

        auto button = ctx.create_object<ui::AnimatedButton>();

        button->set_text("Good Morning jiet!");

        button->m_on_clicked.bind<on_click>();
        button->constraint().min_width = ui::ConstantValue(40);
        button->constraint().min_height = ui::ConstantValue(20);
        button->constraint().max_height = ui::ConstantValue(50);
        button->constraint().max_width = ui::ConstantValue(200);
        button->set_label_margin(4);

        ui::ButtonStyle button_style;
        button_style.font = font;
        button_style.color = ui::make_color(0x0f, 0x4c, 0x75);
        button_style.color_hover = ui::make_color(0x00, 0xB7, 0xC2);
        button_style.color_down = ui::make_color(0x1b, 0x26, 0x2c);
        button_style.label_color = ui::make_color(0x00, 0xB7, 0xC2);
        button_style.label_color_hover = ui::make_color(0xfd, 0xcb, 0x9e);
        button_style.label_color_down = ui::make_color(0xFF, 0xFF, 0xFF);
        button->set_style(button_style);

        auto checkbox = ctx.create_object<ui::CheckBox>();
        checkbox->m_on_clicked.bind<on_check_box_clicked>();

        auto slider = ctx.create_object<ui::HSlider>();
        slider->constraint().max_height = ui::ConstantValue(40);
        vlayout->add_child(button);
        vlayout->add_child(checkbox);
        vlayout->add_child(slider);
        vlayout->add_child(vobject1);
        layout->add_child(vlayout);
    }

    auto object1 = layout->child(0);
    auto object2 = layout->child(1);
    auto object3 = layout->child(2);

    object1->constraint().min_height = ui::ConstantValue(30);
    object1->constraint().max_height = ui::IncomingValue();
    object1->constraint().min_width = ui::ConstantValue(30);
    object1->constraint().max_width = ui::FlexValue(2);

    object2->constraint().min_height = ui::ConstantValue(30);
    object2->constraint().max_height = ui::IncomingValue(0.3f);
    object2->constraint().min_width = ui::ConstantValue(30);
    object2->constraint().max_width = ui::FlexValue(4);

    object3->constraint().min_height = ui::ConstantValue(30);
    object3->constraint().max_height = ui::IncomingValue();
    object3->constraint().min_width = ui::ConstantValue(30);
    object3->constraint().max_width = ui::FlexValue(3);
}

int main() {
    {
#if !defined(MODUS_LOG_USE_SPDLOG)
        log::LogService::set(log::make_basic_stdout_logger());
#endif
        ui::UITestApp app;

        if (!app.initialize("UI Interactive App", 1280, 720)) {
            return EXIT_FAILURE;
        }

        ui::Timer timer(&app.context());
        timer.m_on_trigger.bind<on_timer_trigger>();
        timer.set_duration_ms(IMilisec(1000));
        timer.start();
        setup_layout(app.context());
        { ui::Timer timer2(&app.context()); }
        app.run();
    }
    return 0;
}
