/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ui/ui_pch.h>
#include <catch2/catch.hpp>
#include <ui/context.hpp>
#include <ui/objects/null_object.hpp>
#include <ui/painters/null_painter.hpp>

using namespace modus;

TEST_CASE("Basic Checks", "[UI Constraint]") {
    ui::Size size;

    auto painter_device = std::make_unique<ui::NullPainterDevice>();
    modus::ui::Context context(painter_device.get());
    REQUIRE(context.initialize());
    {
        auto object = context.create_object<modus::ui::NullObject>();

        object->constraint().min_height = ui::ConstantValue(300);
        object->constraint().max_height = ui::ConstantValue(ui::kInfiniteValue);
        object->constraint().min_width = ui::ConstantValue(300);
        object->constraint().max_width = ui::IncomingValue();

        auto parent_object = context.create_object<modus::ui::NullObject>();

        // Test againt Parent larger than child
        parent_object->force_size({800, 600});
        object->eval_constraints(modus::ui::constraint_params_from_object(parent_object.get()));
        size = object->size();
        CHECK(size.width == parent_object->size().width);
        CHECK(ui::is_inifinte(size.height));

        // Test against parent smaller than child
        parent_object->force_size({200, 600});
        object->eval_constraints(modus::ui::constraint_params_from_object(parent_object.get()));
        size = object->size();
        CHECK(size.width == 300);
        CHECK(ui::is_inifinte(size.height));

        // Test for 50% of parent's height and width == min constraint
        object->constraint().max_height = ui::IncomingValue(0.5f);
        object->constraint().max_height = ui::IncomingValue(0.5f);
        parent_object->force_size({300, 600});
        object->eval_constraints(modus::ui::constraint_params_from_object(parent_object.get()));
        size = object->size();
        CHECK(size.width == 300);
        CHECK(size.height == 300);

        // Test for 50% of parent's height and 200% width
        object->constraint().max_width = ui::IncomingValue(2.f);
        object->constraint().max_height = ui::IncomingValue(0.5f);
        parent_object->force_size({800, 600});
        object->eval_constraints(modus::ui::constraint_params_from_object(parent_object.get()));
        size = object->size();
        CHECK(size.width == 1600);
        CHECK(size.height == 300);
    }
    context.shutdown();
}
