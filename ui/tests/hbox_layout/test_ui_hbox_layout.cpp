/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <ui/ui_pch.h>
#include <catch2/catch.hpp>
#include <ui/context.hpp>
#include <ui/layouts/hbox_layout.hpp>
#include <ui/objects/null_object.hpp>
#include <ui/painters/null_painter.hpp>
#include <ui/views/object_view.hpp>
#include "ui/constraint.hpp"
#include "ui/event.hpp"

using namespace modus;

constexpr u32 kViewWidth = 800;
constexpr u32 kViewHeight = 600;

auto create_hbox_layout(ui::Context& ctx) {
    auto view = ctx.create_object<ui::ObjectView>();
    auto layout = ctx.create_object<ui::HBoxLayout>();
    view->set_object(layout);
    layout->constraint().max_width = ui::IncomingValue();
    layout->constraint().max_height = ui::IncomingValue();
    ctx.set_root_view(view);

    ui::Event evt;
    evt.type = ui::EventType::ContextResize;
    evt.context_resize.width = kViewWidth;
    evt.context_resize.height = kViewHeight;
    ctx.on_event(evt);

    return layout;
}

TEST_CASE("Basic Checks", "[UI HBox]") {
    auto painter_device = std::make_unique<ui::NullPainterDevice>();
    modus::ui::Context context(painter_device.get());
    REQUIRE(context.initialize());
    {
        auto layout = create_hbox_layout(context);

        for (int i = 0; i < 3; i++) {
            auto object = context.create_object<modus::ui::NullObject>();
            layout->add_child(object);
        }

        auto object1 = layout->child(0);
        auto object2 = layout->child(1);
        auto object3 = layout->child(2);

        REQUIRE(object1);
        REQUIRE(object2);
        REQUIRE(object3);

        object1->constraint().min_height = ui::ConstantValue(30);
        object1->constraint().max_height = ui::IncomingValue();
        object1->constraint().min_width = ui::ConstantValue(30);
        object1->constraint().max_width = ui::IncomingValue(0.25f);

        object2->constraint().min_height = ui::ConstantValue(30);
        object2->constraint().max_height = ui::IncomingValue();
        object2->constraint().min_width = ui::ConstantValue(30);
        object2->constraint().max_width = ui::IncomingValue(0.4f);

        object3->constraint().min_height = ui::ConstantValue(30);
        object3->constraint().max_height = ui::IncomingValue();
        object3->constraint().min_width = ui::ConstantValue(30);
        object3->constraint().max_width = ui::IncomingValue(0.35f);

        context.update(IMilisec(0));

        CHECK(object1->size().width == 200);
        CHECK(object2->size().width == 320);
        CHECK(object3->size().width == 280);
    }
    context.shutdown();
}

TEST_CASE("Flex Factor [Flex|Fixed|Flex]", "[UI HBox]") {
    auto painter_device = std::make_unique<ui::NullPainterDevice>();
    modus::ui::Context context(painter_device.get());
    REQUIRE(context.initialize());
    {
        auto layout = create_hbox_layout(context);

        for (int i = 0; i < 3; i++) {
            auto object = context.create_object<modus::ui::NullObject>();
            layout->add_child(std::move(object));
        }

        auto object1 = layout->child(0);
        auto object2 = layout->child(1);
        auto object3 = layout->child(2);

        REQUIRE(object1);
        REQUIRE(object2);
        REQUIRE(object3);

        object1->constraint().min_height = ui::ConstantValue(30);
        object1->constraint().max_height = ui::IncomingValue();
        object1->constraint().min_width = ui::ConstantValue(30);
        object1->constraint().max_width = ui::FlexValue(2);

        object2->constraint().min_height = ui::ConstantValue(30);
        object2->constraint().max_height = ui::IncomingValue();
        object2->constraint().min_width = ui::ConstantValue(30);
        object2->constraint().max_width = ui::IncomingValue(0.4f);

        object3->constraint().min_height = ui::ConstantValue(30);
        object3->constraint().max_height = ui::IncomingValue();
        object3->constraint().min_width = ui::ConstantValue(30);
        object3->constraint().max_width = ui::FlexValue(1);

        context.update(IMilisec(0));

        CHECK(object1->size().width == 320);
        CHECK(object2->size().width == 320);
        CHECK(object3->size().width == 160);
    }
    context.shutdown();
}

TEST_CASE("Flex Factor [Flex|Flex|Flex]", "[UI HBox]") {
    auto painter_device = std::make_unique<ui::NullPainterDevice>();
    modus::ui::Context context(painter_device.get());
    REQUIRE(context.initialize());
    {
        auto layout = create_hbox_layout(context);

        for (int i = 0; i < 3; i++) {
            auto object = context.create_object<modus::ui::NullObject>();
            layout->add_child(std::move(object));
        }

        auto object1 = layout->child(0);
        auto object2 = layout->child(1);
        auto object3 = layout->child(2);

        REQUIRE(object1);
        REQUIRE(object2);
        REQUIRE(object3);

        object1->constraint().min_height = ui::ConstantValue(30);
        object1->constraint().max_height = ui::IncomingValue();
        object1->constraint().min_width = ui::ConstantValue(30);
        object1->constraint().max_width = ui::FlexValue(2);

        object2->constraint().min_height = ui::ConstantValue(30);
        object2->constraint().max_height = ui::IncomingValue();
        object2->constraint().min_width = ui::ConstantValue(30);
        object2->constraint().max_width = ui::FlexValue(5);

        object3->constraint().min_height = ui::ConstantValue(30);
        object3->constraint().max_height = ui::IncomingValue();
        object3->constraint().min_width = ui::ConstantValue(30);
        object3->constraint().max_width = ui::FlexValue(1);

        context.update(IMilisec(0));

        CHECK(object1->size().width == 200);
        CHECK(object2->size().width == 500);
        CHECK(object3->size().width == 100);
    }
    context.shutdown();
}
