
modus_add_test(test_ui_hbox_layout NAME UIHBoxLayout)

target_sources(test_ui_hbox_layout PRIVATE
    test_ui_hbox_layout.cpp
)

target_link_libraries(test_ui_hbox_layout PRIVATE ui_tests_common)

