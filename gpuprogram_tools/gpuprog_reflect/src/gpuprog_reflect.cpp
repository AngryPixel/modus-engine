/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <app/app_pch.h>
#include <threed/threed_pch.h>
// clang-format on

#include <app/app.hpp>
#include <core/getopt.hpp>
#include <core/io/memory_stream.hpp>
#include <threed/instance.hpp>
#include <threed/device.hpp>
#include <threed/program.hpp>
#include <os/file.hpp>

#include <iostream>

#include "constants_json.hpp"

using namespace modus;

// Until the raspberry PI4 does not support OpenGLES 3.1 we have to extract the
// information from an active GL context. Afterwards we can use spirv-cross to
// extract that without creating an OpenGL context.

static Result<> do_reflect(
    const threed::ProgramCreateParams& program_params,
    threed::ProgramReflection& reflection) {
    MODUS_UNUSED(program_params);
    // Create APP
    std::unique_ptr<app::App> app;
    app = modus::app::create_app();
    app::AppInitParams init_params;
    init_params.name = "GPUProgram Reflection";
    init_params.name_preferences = "modus.tools.gpuprog_reflect";

    if (!app->initialize(init_params)) {
        std::cerr << "Failed to create app\n";
        return Error<>();
    }

    app::WindowCreateParams window_params;
    window_params.title = "GPUProgram Reflect";
    window_params.width = 64;
    window_params.height = 64;
    window_params.hidden = true;
    app::AppResult<NotMyPtr<app::Window>> result =
        app->create_window(window_params);
    if (!result) {
        std::cerr << fmt::format("Failed to create window: {}\n",
                                 result.error());
        return Error<>();
    }

    auto device = app->threed_instance().create_device();
    if (!device->initialize()) {
        std::cerr << "Failed to create threed device\n";
        (void)app->shutdown();
        return Error<>();
    }

    auto r_program = device->create_program(program_params);
    if (!r_program) {
        std::cerr << fmt::format("Failed to create threed program: {}\n",
                                 r_program.error());
        (void)device->shutdown();
        (void)app->shutdown();
        return Error<>();
    }

    auto r_reflect = device->reflect_program(*r_program, reflection);
    (void)device->shutdown();
    (void)app->shutdown();
    return r_reflect;
}

static Result<RamStream<>, void> read_file_to_memory(const StringSlice path) {
    auto r_file = os::FileBuilder().read().binary().open(path);
    if (!r_file) {
        return Error<>();
    }

    RamStream<> stream;
    auto r_read = stream.populate_from_stream(r_file.value());
    if (!r_read || r_read.value() != r_file.value().size()) {
        return Error<>();
    }

    return Ok(std::move(stream));
}

static Result<> write_cpp_header(
    const StringSlice path,
    const threed::ProgramReflection& reflection,
    const StringSlice cpp_namespace,
    const StringSlice cpp_namespace_name) {
    auto r_file = os::FileBuilder().write().create().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", path);
        return Error<>();
    }

#define WRITE(x)                                  \
    if (!write(x)) {                              \
        std::cerr << "Failed to write to file\n"; \
        return Error<>();                     \
    }

    auto write = [&r_file](const StringSlice slice) -> bool {
        return r_file->write_exactly(slice.as_bytes()).is_value();
    };

    WRITE("// Auto generated header, do not modify\n\n")
    WRITE("// clang-format off\n")
    WRITE("#pragma once\n\n")
    WRITE("#include <threed/types.hpp>\n")
    WRITE("#include <threed/pipeline.hpp>\n")

    WRITE(modus::format("\nnamespace {}::{} {{\n\n", cpp_namespace, cpp_namespace_name))
    usize index = 0;
    for (usize i = 0; i < reflection.constants.size(); ++i) {
        const auto& c = reflection.constants[i];
        WRITE(
            format("inline void set_constant_{}(\n\tmodus::threed::Command& "
                   "cmd,\n\tconst modus::threed::ConstantHandle& h) {{\n",
                   c.name))
        WRITE(format("\tcmd.inputs.constants[{}] = h;\n", index))
        WRITE("}\n\n")
        index++;
    }

    for (usize i = 0; i < reflection.buffers.size(); ++i) {
        const auto& c = reflection.buffers[i];
        WRITE(
            format("inline void set_constant_{}(\n\tmodus::threed::Command& "
                   "cmd,\n\tconst modus::threed::ConstantBufferHandle& h) {{\n",
                   c.name))
        WRITE(format("\tcmd.inputs.constants[{}] = h;\n", index))
        WRITE("}\n\n")
        index++;
    }

    for (usize i = 0; i < reflection.samplers.size(); ++i) {
        const auto& c = reflection.samplers[i];
        WRITE(format("static constexpr modus::u32 kSamplerIndex_{} = {};\n", c.name, i))
        WRITE(
            format("inline void set_sampler_{}(\n\tmodus::threed::Command& "
                   "cmd,\n\tconst modus::threed::TextureHandle& h) {{\n",
                   c.name))
        WRITE(format("\tcmd.inputs.textures[{}] = h;\n", i))
        WRITE("}\n\n")
    }

    WRITE("}\n")

#undef WRITE
    return Ok<>();
}

static const StringSlice kIntro =
    "Extract constants out of a gpuprogram program\n Usage: "
    "modus_gpuprog_reflect -o "
    "output --vert ... --frag ...";

int main(const int argc, const char** argv) {
    MODUS_UNUSED(argc);
    MODUS_UNUSED(argv);

    CmdOptionString opt_vert("", "--vert", "Path to vertex shader", "");
    CmdOptionString opt_frag("", "--frag", "Path to fragment shader", "");
    CmdOptionString opt_cpp("", "--cpp-header", "Path to generate cpp header",
                            "");
    CmdOptionString opt_cpp_namespace(
        "", "--cpp-namespace",
        "cpp namespace for generated hcpp namespace for generated header. "
        "Default = gpureflect.",
        "gpureflect");
    CmdOptionString opt_cpp_namespace_name(
        "", "--cpp-namespace-name",
        "cpp namespace name to append to --cpp-namespace. By default it is the filename without the extension passed into --cpp-header. ",
        "");
    CmdOptionString opt_output("-o", "--output",
                               "Path to generated constants.json file", "");
    CmdOptionEmpty opt_verbose("-v", "--verbose", "Enable Verbose output");

    CmdOptionParser opt_parser;
    opt_parser.add(opt_output).expect("Failed to add option");
    opt_parser.add(opt_vert).expect("Failed to add option");
    opt_parser.add(opt_frag).expect("Failed to add option");
    opt_parser.add(opt_cpp).expect("Failed to add option");
    opt_parser.add(opt_cpp_namespace).expect("Failed to add option");
    opt_parser.add(opt_cpp_namespace_name).expect("Failed to add option");
    opt_parser.add(opt_verbose).expect("Failed to add option");

    if (!opt_parser.parse(argc, argv) || !opt_vert.parsed() ||
        !opt_output.parsed()) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    threed::ProgramCreateParams params;

    auto r_vertex = read_file_to_memory(opt_vert.value());
    if (!r_vertex) {
        std::cerr << fmt::format("Failed to read {} into memory\n",
                                 opt_vert.value());
        return EXIT_FAILURE;
    }

    params.vertex = r_vertex->as_bytes();

    Result<RamStream<>, void> r_frag = Error<>();
    if (opt_frag.parsed()) {
        r_frag = read_file_to_memory(opt_frag.value());
        if (!r_frag) {
            std::cerr << fmt::format("Failed to read {} into memory\n",
                                     opt_frag.value());
            return EXIT_FAILURE;
        }
        params.fragment = r_frag->as_bytes();
    }

    threed::ProgramReflection reflection;
    if (auto r = do_reflect(params, reflection); !r) {
        return EXIT_FAILURE;
    }

    if (opt_verbose.parsed()) {
        std::cout << "Constants:\n";
        for (const auto& c : reflection.constants) {
            std::cout << fmt::format("\t[{:02}] name={} type={}\n",
                                     c.location_hint, c.name, c.data_type);
        }
        std::cout << "Buffers:\n";
        for (const auto& b : reflection.buffers) {
            std::cout << fmt::format("\t{}\n", b.name);
        }

        std::cout << "Samplers:\n";
        for (const auto& c : reflection.samplers) {
            std::cout << fmt::format("\t[{:02}] name={} type={}\n",
                                     c.location_hint, c.name, c.type);
        }
    }

    if (!write_constant_file(opt_output.value(), reflection)) {
        return EXIT_FAILURE;
    }

    if (opt_cpp.parsed()) {
        StringSlice namespace_name;
        if (opt_cpp_namespace_name.parsed()) {
            namespace_name = opt_cpp_namespace_name.value();
        } else {
            namespace_name = os::Path::get_basename(opt_cpp.value());
            while(namespace_name.find('.')) {
                namespace_name = os::Path::remove_extension(namespace_name);
            }
        }
        if (!write_cpp_header(opt_cpp.value(), reflection,
                              opt_cpp_namespace.value(), namespace_name)) {
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}
