/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include "constants_json.hpp"

#include <core/core_pch.h>
#include <os/os_pch.h>
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>

#include <core/io/memory_stream.hpp>
#include <iostream>
#include <os/file.hpp>
#include <threed/program.hpp>

static const char* k_str_data_types[] = {
    "I8",
    "U8",
    "I16",
    "U16",
    "I32",
    "U32",
    "F32",
    "Vec2I8",
    "Vec2U8",
    "Vec2I16",
    "Vec2U16",
    "Vec2I32",
    "Vec2U32",
    "Vec2F32",
    "Vec3I8",
    "Vec3U8",
    "Vec3I16",
    "Vec3U16",
    "Vec3I32",
    "Vec3U32",
    "Vec3F32",
    "Vec4I8",
    "Vec4U8",
    "Vec4I16",
    "Vec4U16",
    "Vec4I32",
    "Vec4U32",
    "Vec4F32",
    "Mat2F32",
    "Mat3F32",
    "Mat4F32",
    "U32_2_3x10_REV",
    "I32_2_3x10_REV",
};

static_assert(modus::array_size(k_str_data_types) ==
              usize(threed::DataType::Total));

static Result<threed::DataType, void> data_type_from_string(
    const StringSlice str) {
    for (u32 i = 0; i < u32(threed::DataType::Total); ++i) {
        if (str == k_str_data_types[i]) {
            return Ok(static_cast<threed::DataType>(i));
        }
    }
    return Error<>();
}

static Result<const char*, void> data_type_to_str(const threed::DataType dt) {
    if (dt >= threed::DataType::Total) {
        return Error<>();
    }
    return Ok(k_str_data_types[u32(dt)]);
}

static constexpr const char* kKeyConstants = "constants";
static constexpr const char* kKeySamplers = "samplers";
static constexpr const char* kKeyIsBuffer = "is_buffer";
static constexpr const char* kKeyDataType = "data_type";
static constexpr const char* kKeyName = "name";

Result<> read_constant_file(const StringSlice path,
                                      Vector<Constant>& constant_list,
                                      Vector<Sampler>& sampler_list) {
    using namespace rapidjson;
    RamStream<> m_json_data;

    {
        auto r_file = os::FileBuilder().read().open(path);
        if (!r_file) {
            std::cerr << fmt::format("Failed to open {}\n", path);
            return Error<>();
        }

        auto r_read = m_json_data.populate_from_stream(*r_file);
        if (!r_read || r_read.value() != r_file->size()) {
            std::cerr << fmt::format("Failed to read into memory\n");
            return Error<>();
        }
    }
    const StringSlice doc_slice = m_json_data.as_string_slice();
    rapidjson::Document doc;
    doc.Parse(doc_slice.data(), doc_slice.size());

    if (doc.GetParseError() != ParseErrorCode::kParseErrorNone) {
        // Failed to parse document
        std::cerr << fmt::format("File {} is not a valid json document\n",
                                 path);
        return Error<>();
    }

    if (!doc.HasMember(kKeyConstants)) {
        std::cerr << fmt::format("Missing root {} object in document: {}\n",
                                 kKeyConstants, path);
        return Error<>();
    }

    const auto& constants = doc[kKeyConstants];
    if (!constants.IsArray()) {
        std::cerr << fmt::format("{} is not an array object in document: {}\n",
                                 kKeyConstants, path);
        return Error<>();
    }

    // Read Constants
    const auto& constants_array = constants.GetArray();
    constant_list.reserve(constants.Size());
    usize index = 0;
    for (const auto& constant : constants_array) {
        Constant cur_constant;
        if (!constant.IsObject()) {
            std::cerr << fmt::format(
                "Constant {} is not a json object in document: {}\n", index,
                path);
            return Error<>();
        }
        const auto& constant_object = constant.GetObject();

        if (!constant_object.HasMember(kKeyName)) {
            std::cerr << fmt::format(
                "Constant {} does not have field '{}' in document: {}\n", index,
                kKeyName, path);
            return Error<>();
        }
        if (!constant_object.HasMember(kKeyIsBuffer)) {
            std::cerr << fmt::format(
                "Constant {} does not have field '{}' in document: {}\n", index,
                kKeyIsBuffer, path);
            return Error<>();
        }

        {
            const auto& name_value = constant_object[kKeyName];
            if (!name_value.IsString()) {
                std::cerr << fmt::format(
                    "Constant {}'s field '{}' is not a string in document: "
                    "{}\n",
                    index, kKeyName, path);
                return Error<>();
            }
            cur_constant.name = name_value.GetString();
        }

        {
            const auto& is_buffer_value = constant_object[kKeyIsBuffer];
            if (!is_buffer_value.IsBool()) {
                std::cerr << fmt::format(
                    "Constant {}'s field '{}' is not a boolean in document: "
                    "{}\n",
                    index, kKeyIsBuffer, path);
                return Error<>();
            }
            cur_constant.is_buffer = is_buffer_value.GetBool();
        }

        if (!cur_constant.is_buffer) {
            if (!constant_object.HasMember(kKeyDataType)) {
                std::cerr << fmt::format(
                    "Constant {} does not have field '{}' in document: {}\n",
                    index, kKeyDataType, path);
                return Error<>();
            }
            const auto& data_type_value = constant_object[kKeyDataType];
            if (!data_type_value.IsString()) {
                std::cerr << fmt::format(
                    "Constant {}'s field '{}' is not a string in document: "
                    "{}\n",
                    index, kKeyDataType, path);
                return Error<>();
            }

            const char* data_type_str = data_type_value.GetString();
            if (auto r = data_type_from_string(data_type_str); !r) {
                std::cerr << fmt::format(
                    "Constant {}'s data type '{}' is not a valid data type in "
                    "document: {}\n",
                    index, data_type_str, path);
                return Error<>();
            } else {
                cur_constant.data_type = *r;
            }
        }
        constant_list.push_back(cur_constant);
        index++;
    }

    if (!doc.HasMember(kKeyConstants)) {
        std::cerr << fmt::format("Missing root {} object in document: {}\n",
                                 kKeySamplers, path);
        return Error<>();
    }

    const auto& samplers_value = doc[kKeySamplers];
    if (!samplers_value.IsArray()) {
        std::cerr << fmt::format("{} is not an array object in document: {}\n",
                                 kKeySamplers, path);
        return Error<>();
    }

    const auto& samplers = samplers_value.GetArray();
    sampler_list.reserve(samplers.Size());
    index = 0;

    for (const auto& sampler : samplers) {
        Sampler cur_sampler;
        if (!sampler.IsString()) {
            std::cerr << fmt::format(
                "Sampler {} is not a string in document: {}\n", index, path);
            return Error<>();
        }
        cur_sampler.name = sampler.GetString();
        sampler_list.push_back(cur_sampler);
        index++;
    }
    return Ok<>();
}

Result<> write_constant_file(
    const StringSlice path,
    const threed::ProgramReflection& reflection) {
    using namespace rapidjson;
    MODUS_UNUSED(path);
    MODUS_UNUSED(reflection);

    using StrRef = Value::StringRefType;

    Document doc;
    doc.SetObject();

    // create constants
    {
        Value constants;
        constants.SetArray();

        for (const auto& c : reflection.constants) {
            Value constant;
            constant.SetObject();
            Value name;
            name.SetString(StrRef(c.name.data(), c.name.size()),
                           doc.GetAllocator());
            constant.AddMember(StrRef(kKeyName), name, doc.GetAllocator());
            constant.AddMember(StrRef(kKeyIsBuffer), false, doc.GetAllocator());
            auto r = data_type_to_str(c.data_type);
            if (!r) {
                std::cerr << fmt::format(
                    "Unknown data type in constant {}:{}\n", c.name,
                    c.data_type);
                return Error<>();
            }
            Value dt;
            dt.SetString(StrRef(*r), doc.GetAllocator());
            constant.AddMember(StrRef(kKeyDataType), dt, doc.GetAllocator());
            constants.PushBack(constant, doc.GetAllocator());
        }

        for (const auto& c : reflection.buffers) {
            Value constant;
            constant.SetObject();
            Value name;
            name.SetString(StrRef(c.name.data(), c.name.size()),
                           doc.GetAllocator());
            constant.AddMember(StrRef(kKeyName), name, doc.GetAllocator());
            constant.AddMember(StrRef(kKeyIsBuffer), true, doc.GetAllocator());
            constants.PushBack(constant, doc.GetAllocator());
        }

        doc.AddMember(StrRef(kKeyConstants), constants, doc.GetAllocator());
    }
    // create samplers
    {
        Value samplers;
        samplers.SetArray();
        for (const auto& c : reflection.samplers) {
            Value name;
            name.SetString(StrRef(c.name.data(), c.name.size()),
                           doc.GetAllocator());
            samplers.PushBack(name, doc.GetAllocator());
        }
        doc.AddMember(StrRef(kKeySamplers), samplers, doc.GetAllocator());
    }

    rapidjson::StringBuffer buffer(0, 65 * 1024 * 1024);
    rapidjson::PrettyWriter<rapidjson::StringBuffer> rjwriter(buffer);

    if (!doc.Accept(rjwriter)) {
        std::cerr << "Failed to write json to memory buffer\n";
        return Error<>();
    }

    auto r_file = os::FileBuilder().write().create().open(path);
    if (!r_file) {
        std::cerr << format("Failed to open {} for writing\n", path);
        return Error<>();
    }

    const StringSlice generated =
        StringSlice(buffer.GetString(), buffer.GetSize());
    auto write_result = r_file->write(generated.as_bytes());
    if (!write_result || write_result.value() != generated.size()) {
        std::cerr << "Failed to write data to file\n";
        return Error<>();
    }
    return Ok<>();
}
