/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core_pch.h>
#include <threed/threed_pch.h>

#include <core/fixed_stack_string.hpp>
#include <threed/types.hpp>

namespace modus::threed {
struct ProgramReflection;
}

using namespace modus;

struct Constant {
    FixedStackString<64> name;
    threed::DataType data_type;
    bool is_buffer;
};

struct Sampler {
    FixedStackString<64> name;
};

Result<> read_constant_file(const StringSlice path,
                                      Vector<Constant>& constant_list,
                                      Vector<Sampler>& sampler_list);

Result<> write_constant_file(
    const StringSlice path,
    const threed::ProgramReflection& reflection);
