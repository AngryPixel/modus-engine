#
# Embedd these source files in the current library
#

set(songho_source_directory ${CMAKE_CURRENT_LIST_DIR}/src CACHE INTERNAL "")

function(modus_add_songho_sources target)

    target_sources(${target} PRIVATE
        ${songho_source_directory}/Icosphere.h
        ${songho_source_directory}/Icosphere.cpp
        ${songho_source_directory}/Icosahedron.h
        ${songho_source_directory}/Icosahedron.cpp
    )

    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU"
        OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        set_source_files_properties(
            ${songho_source_directory}/Icosphere.cpp
            ${songho_source_directory}/Icosahedron.cpp
            PROPERTIES
                COMPILE_FLAGS "-Wno-shadow"
        )
    endif()
    target_include_directories(${target} PRIVATE ${songho_source_directory})
endfunction()

