/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <audio/audio_device.hpp>
#include <audio/codecs/oggvorbis_codec.hpp>
#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/engine.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/entity_template_asset.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/image_asset.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/mesh_asset.hpp>
#include <engine/graphics/model_asset.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <gameplay/components/asteroid_info_component.hpp>
#include <gameplay/components/flight_component.hpp>
#include <gameplay/components/health_component.hpp>
#include <gameplay/components/laser_gun_component.hpp>
#include <modus_assets.hpp>
#include <physics/instance.hpp>
#include <physics/rigid_body.hpp>
#include <physics/world.hpp>

namespace modus::game {

static constexpr const char* kAssetPathAsteroidTemplate1 =
    "game:modus/entity_templates/asteroids/asteroid1.etpl";
static constexpr const char* kAssetPathAsteroidTemplate2 =
    "game:modus/entity_templates/asteroids/asteroid2.etpl";
static constexpr const char* kAssetPathAsteroidTemplate3 =
    "game:modus/entity_templates/asteroids/asteroid3.etpl";
static constexpr const char* kAssetPathLaserTemplate =
    "game:modus/entity_templates/laser/laser.etpl";
static constexpr const char* kAssetPathShipTemplate = "game:modus/entity_templates/ship/ship.etpl";
static constexpr const char* kAssetPathCageTemplate = "game:modus/entity_templates/cage/cage.etpl";

Result<> ModusAssets::preload_async(engine::Engine& engine) {
    engine::assets::AsyncLoadParams params;
    params.path = "game:modus/models/paletted/cage.model";
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_cube_mesh_asset = *r;
    }

    params.path = kAssetPathAsteroidTemplate1;
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_asteroid_templates[0] = *r;
    }
    params.path = kAssetPathAsteroidTemplate2;
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_asteroid_templates[1] = *r;
    }
    params.path = kAssetPathAsteroidTemplate3;
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_asteroid_templates[2] = *r;
    }
    params.path = kAssetPathLaserTemplate;
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_laser_template = *r;
    }
    params.path = kAssetPathShipTemplate;
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_ship_template = *r;
    }
    params.path = kAssetPathCageTemplate;
    if (auto r = engine.load_startup_asset(params); !r) {
        return Error<>();
    } else {
        m_cage_template = *r;
    }
    return Ok<>();
}

Result<> ModusAssets::load_immediate(engine::Engine& engine) {
    if (!setup_asteroid_info(engine)) {
        MODUS_LOGE("Failed to initialize astroid info table");
        return Error<>();
    }

    if (!setup_laser_info(engine)) {
        MODUS_LOGE("Failed to initialize laser info");
        return Error<>();
    }

    return Ok<>();
}

Result<> ModusAssets::destroy(engine::Engine& engine) {
    auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();
    asset_loader.destroy_async(m_cubemap_asset);
    asset_loader.destroy_async(m_cubemap_asset);
    asset_loader.destroy_async(m_laser_template);
    asset_loader.destroy_async(m_ship_template);
    asset_loader.destroy_async(m_cage_template);

    for (auto& asset : m_asteroid_templates) {
        asset_loader.destroy_async(asset);
    }
    return Ok<>();
}

Result<engine::gameplay::EntityId, void> ModusAssets::spawn_asteroid(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    physics::PhysicsWorld& physics_world,
    const u32 type,
    const u32 level,
    const math::Transform& tranform,
    const glm::vec3& direction,
    const glm::vec3& angular_velocity) const {
    modus_assert(type < kNumAsteroidTypes);
    modus_assert(level < kNumAsteroidLevels);
    const AsteroidInfo& info = m_asteroid_info[type][level];
    const auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();

    auto r_asset = asset_loader.resolve_typed<engine::gameplay::EntityTemplateAsset>(
        m_asteroid_templates[level]);
    if (!r_asset) {
        MODUS_LOGE("ModusAsset: Failed to locate asteroid for level {}", level);
        return Error<>();
    }

    auto r_entity = (*r_asset)->spawn(engine, entity_manager);
    if (!r_entity) {
        MODUS_LOGE("ModusAssets: Failed to spawn asteroid");
        return Error<>();
    }
    entity_manager.update_entity_tag(*r_entity, kEntityTagAsteroid).expect();

    auto transform_component =
        entity_manager.component<engine::gameplay::TransformComponent>(*r_entity).value_or_panic();
    transform_component->set_translation(tranform.translation());
    transform_component->set_rotation(tranform.rotation());

    auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();
    auto physics_component =
        entity_manager.component<engine::physics::PhysicsComponent>(*r_entity).value_or_panic();
    if (!physics_instance.rigid_body_set_transform(physics_component->m_handle, tranform)) {
        MODUS_LOGE("ModusAssets: Failed to set transform ");
    }

    if (!physics_instance.rigid_body_set_linear_velocity(physics_component->m_handle,
                                                         direction * info.velocity)) {
        MODUS_LOGE("ModusAssets: Failed to set linear velocity");
    }

    if (!physics_instance.rigid_body_set_angular_velocity(physics_component->m_handle,
                                                          angular_velocity)) {
        MODUS_LOGE("ModusAssets: Failed to set angular velocity");
    }

    if (!physics_world.add_rigid_body(physics_component->m_handle)) {
        MODUS_LOGE("ModusAssets: Failed to add rigid body to world");
    }

    return Ok(*r_entity);
}

Result<engine::gameplay::EntityId, void> ModusAssets::spawn_ship(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    physics::PhysicsWorld& physics_world,
    const glm::vec3& position) const {
    const auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();
    auto r_asset =
        asset_loader.resolve_typed<engine::gameplay::EntityTemplateAsset>(m_ship_template);
    if (!r_asset) {
        MODUS_LOGE("ModusAssets: Could not locate ship asset");
        return Error<>();
    }

    auto r_entity = (*r_asset)->spawn(engine, entity_manager);
    if (!r_entity) {
        MODUS_LOGE("ModusAssets: Failed to create entity");
        return Error<>();
    }
    entity_manager.update_entity_tag(*r_entity, kEntityTagPlayer).expect();

    /*
        auto r_animator = animation_module->manager().create((*r_mesh)->animation_catlog);
        if (!r_animator) {
            MODUS_LOGE("ModusAssets: Failed to create animator");
            return Error<>();
        }

        engine::animation::PlayAnimationParams play_params;
        play_params.loop = true;
        play_params.animation_name = "EngineSpin";

        if (!animation_module->manager().play(*r_animator, play_params)) {
            MODUS_LOGE("ModusAssets: Failed to play ship engine animation");
            return Error<>();
        }

        play_params.animation_name = "WingsMove";
        if (!animation_module->manager().play(*r_animator, play_params)) {
            MODUS_LOGE("ModusAssets: Failed to play ship wing animation");
            return Error<>();
        }*/

    if (auto r_listener =
            entity_manager.add_component<engine::audio::AudioListenerComponent>(*r_entity);
        !r_listener) {
        return Error<>();
    }

    auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();

    auto pc =
        entity_manager.component<engine::physics::PhysicsComponent>(*r_entity).value_or_panic();
    math::Transform t = math::Transform::kIdentity;
    t.set_translation(position);
    physics_instance.rigid_body_set_transform(pc->m_handle, t).expect();

    if (!physics_world.add_rigid_body(pc->m_handle)) {
        MODUS_LOGE("ModusAssets: Failed to add asset to world");
        return Error<>();
    }

    // Spawn ship thruster_lights
    /*
    {

        auto r_engine = entity_manager.create("Engine1");
        if (!r_engine) {
            MODUS_LOGE("ModusAssets: Failed to create engine light");
            return Error<>();
        }

        if (auto r_tc =
    entity_manager.add_component<engine::gameplay::TransformComponent>(*r_engine);
            !r_tc) {
            MODUS_LOGE("ModusAssets: Failed to create engine light transform
    component"); return Error<>(); } else {
            if(!(*r_tc)->attach(entity_manager, *r_entity)) {
                MODUS_LOGE("ModusAssets: Failed to attach engine light");
            }
            (*r_tc)->set_translation(2.75f, 2.5f,-9.5f);
        }

        if (auto r_lc =
    entity_manager.add_component<engine::graphics::LightComponent>(*r_engine);
    !r_lc) { MODUS_LOGE("ModusAssets: Failed to create engine light light
    component"); return Error<>(); } else { auto& light = (*r_lc)->light;
            light.set_type(engine::graphics::LightType::Point);
            light.set_diffuse_color(glm::vec3(32.f/255.f, 46.0f/255.0f, 59.0f/255.0f));
            //light.set_diffuse_color(glm::vec3(1.0f));
            light.set_diffuse_intensity(3.0f);
            light.set_point_light_radius(5.0f);
        }

    }*/
    return Ok(*r_entity);
}

const Optional<ModusAssets::AsteroidInfo> ModusAssets::asteroid_info(const u32 type,
                                                                     const u32 level) const {
    if (type >= kNumAsteroidTypes || level >= kNumAsteroidLevels) {
        return Optional<ModusAssets::AsteroidInfo>();
    }
    return m_asteroid_info[type][level];
}

/*
Result<engine::gameplay::EntityId, void> ModusAssets::spawn_cubemap(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager) const {
    const auto& asset_manager =
        engine.module<engine::ModuleAssets>(
            engine.modules().assets())
            ->manager();
    auto r_mesh_asset =
        asset_manager.get_typed<engine::graphics::MeshAsset>(m_cube_mesh_asset);
    if (!r_mesh_asset) {
        MODUS_LOGE("ModusAssets: Could not locate cube mesh asset");
        return Error<>();
    }

    auto r_cubemap_asset =
        asset_manager.get_typed<engine::graphics::ImageAsset>(m_cubemap_asset);
    if (!r_cubemap_asset) {
        MODUS_LOGE("ModusAssets: Could not locate cubemap asset");
        return Error<>();
    }

    auto r_entity = entity_manager.create("CubeMap");
    ;
    if (!r_entity) {
        MODUS_LOGE("ModusAssets: Failed to create entity");
        return Error<>();
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>(
        engine.modules().graphics());
    const auto& mesh_db = graphics_module->mesh_db();

    auto r_mesh_instance = mesh_db.new_instance((*r_mesh_asset)->m_mesh_handle);
    if (!r_mesh_instance) {
        MODUS_LOGE("ModusAssets: Failed to create mesh instance");
        return Error<>();
    }

    if (auto r =
            entity_manager.add_component<engine::gameplay::TransformComponent>(
                *r_entity);
        !r) {
        return Error<>();
    }

    auto r_graphics =
        entity_manager.add_component<engine::graphics::GraphicsComponent>(
            *r_entity);
    if (!r_graphics) {
        return Error<>();
    }
    (*r_graphics)->mesh_instance = *r_mesh_instance;
    (*r_graphics)->tag = engine::graphics::GraphicsComponentTag::SkyBox;

    const GUID guid = GUID::from_string("58aaf618-0919-4a62-a8c8-8e06c59ef335")
                          .value_or_panic("Invalid GUID");
    if (auto r_mat_instance =
            graphics_module->material_db().create_instance(guid);
        !r_mat_instance) {
        MODUS_LOGE("ModusAssets: Failed to create cubemap material");
    } else {
        (void)(*r_graphics)->mesh_instance.materials.push_back(*r_mat_instance);
        (*r_graphics)->material_instance = *r_mat_instance;
        (*r_graphics)->tag = engine::graphics::GraphicsComponentTag::SkyBox;
        auto r_instance_ptr =
            graphics_module->material_db().instance(*r_mat_instance);
        if (!(*r_instance_ptr)
                 ->set_property("texture", (*r_cubemap_asset)->m_handle)) {
            MODUS_LOGE("ModusAssets: Failed to set cubemap texture property");
        }
    }
    return Ok(*r_entity);
}*/

Result<engine::gameplay::EntityId, void> ModusAssets::spawn_cage(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    physics::PhysicsWorld& physics_world,
    const f32 radius) const {
    MODUS_UNUSED(radius);
    const auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();
    auto r_asset =
        asset_loader.resolve_typed<engine::gameplay::EntityTemplateAsset>(m_cage_template);
    if (!r_asset) {
        MODUS_LOGE("ModusAssets: Could not locate cube mesh asset");
        return Error<>();
    }

    auto r_entity = (*r_asset)->spawn(engine, entity_manager);
    if (!r_entity) {
        MODUS_LOGE("ModusAssets: Failed to create cage entity");
        return Error<>();
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    [[maybe_unused]] const auto& mesh_db = graphics_module->mesh_db();

    auto r_graphics = entity_manager.component<engine::graphics::GraphicsComponent>(*r_entity);
    if (!r_graphics) {
        return Error<>();
    }
    (*r_graphics)->z_distance = f32(7000.f);

    auto r_physics = entity_manager.component<engine::physics::PhysicsComponent>(*r_entity);
    if (!r_physics) {
        return Error<>();
    }

    if (!physics_world.add_rigid_body((*r_physics)->m_handle)) {
        return Error<>();
    }

    return Ok(*r_entity);
}

Result<> ModusAssets::setup_asteroid_info(engine::Engine& engine) {
    MODUS_UNUSED(engine);
    auto fill_asteroid_info = [this](const u32 asteroid_index) -> Result<> {
        MODUS_UNUSED(this);
        modus_assert(asteroid_index < kNumAsteroidTypes);
        constexpr f32 kAsteroidVelocity[kNumAsteroidLevels] = {300.0f, 400.0f, 500.0f};
        for (u32 i = 0; i < kNumAsteroidLevels; ++i) {
            AsteroidInfo& info = m_asteroid_info[asteroid_index][i];
            info.velocity = kAsteroidVelocity[i];
        }
        return Ok<>();
    };

    if (!fill_asteroid_info(0)) {
        return Error<>();
    }

    return Ok<>();
}

Result<> ModusAssets::setup_laser_info(engine::Engine&) {
    m_laser_info.velocity = 2000.f;
    return Ok<>();
}

Result<> ModusAssets::load_sounds(engine::Engine& engine) {
    audio::AudioContext& ctx = engine.module<engine::ModuleAudio>()->context();
    auto fn_load = [&engine, &ctx](const StringSlice path, const StringSlice name,
                                   const modus::audio::UserId id,
                                   const bool stream) -> Result<modus::audio::SoundHandle, void> {
        auto r_path = vfs::VirtualPath::from(path);
        if (!r_path) {
            MODUS_LOGE("ModusAssets: Invalid audio path: {}", path);
            return Error<>();
        }

        auto r_file = engine.module<engine::ModuleFileSystem>()->vfs().open_readable(*r_path);
        if (!r_file) {
            MODUS_LOGE("ModusAssets: Count not open Audio file: {}", path);
            return Error<>();
        }

        modus::audio::SoundCreateParams sound_params;
        sound_params.name = name;
        sound_params.codec = modus::make_unique<modus::audio::OggVorbisCodec>();
        sound_params.byte_stream = r_file.release_value();
        sound_params.id = id;
        sound_params.stream = stream;

        return ctx.create_sound(sound_params);
    };
    // Load audio assets

    using AudioId = modus::audio::UserId;

    if (auto r_sound = fn_load("game:audio/ship/alert.ogg", "ShipAlert", AudioId(0), false);
        r_sound) {
        m_audio_assets.ship.alert = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create ship alert sound");
        return Error<>();
    }

    if (auto r_sound = fn_load("game:audio/ship/laser1.ogg", "ShipLaser1", AudioId(1), false);
        r_sound) {
        m_audio_assets.ship.laser1 = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create ship laser1 sound");
        return Error<>();
    }

    if (auto r_sound = fn_load("game:audio/ship/laser2.ogg", "ShipLaser2", AudioId(2), false);
        r_sound) {
        m_audio_assets.ship.laser2 = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create ship laser2 sound");
        return Error<>();
    }

    if (auto r_sound = fn_load("game:audio/ship/engine.ogg", "ShipEngine", AudioId(3), false);
        r_sound) {
        m_audio_assets.ship.engine = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create ship engine sound");
        return Error<>();
    }

    if (auto r_sound = fn_load("game:audio/ship/death.ogg", "ShipDeath", AudioId(300), false);
        r_sound) {
        m_audio_assets.ship.death = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create ship death sound");
        return Error<>();
    }

    if (auto r_sound = fn_load("game:audio/ambient/main.ogg", "AmbientMain", AudioId(4), true);
        r_sound) {
        m_audio_assets.ambient.main = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create ambient sound");
        return Error<>();
    }

    if (auto r_sound =
            fn_load("game:audio/asteroid/impact.ogg", "AsteroidCollision", AudioId(5), false);
        r_sound) {
        m_audio_assets.asteroid.impact = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create asteroid impact sound");
        return Error<>();
    }

    if (auto r_sound =
            fn_load("game:audio/asteroid/glass-break1.ogg", "AsteroidShatter1", AudioId(6), false);
        r_sound) {
        m_audio_assets.asteroid.shatter1 = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create asteroid shatter 1 sound");
        return Error<>();
    }

    if (auto r_sound =
            fn_load("game:audio/asteroid/glass-break2.ogg", "AsteroidShatter2", AudioId(7), false);
        r_sound) {
        m_audio_assets.asteroid.shatter2 = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create asteroid shatter 2 sound");
        return Error<>();
    }

    if (auto r_sound =
            fn_load("game:audio/asteroid/glass-break3.ogg", "AsteroidShatter3", AudioId(8), false);
        r_sound) {
        m_audio_assets.asteroid.shatter3 = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create asteroid shatter 3 sound");
        return Error<>();
    }

    if (auto r_sound =
            fn_load("game:audio/asteroid/glass-break4.ogg", "AsteroidShatter4", AudioId(9), false);
        r_sound) {
        m_audio_assets.asteroid.shatter4 = *r_sound;
    } else {
        MODUS_LOGE("Modus::Assets Failed to create asteroid shatter  sound");
        return Error<>();
    }
    return Ok<>();
}

Result<> ModusAssets::destroy_sounds(engine::Engine& engine) {
    audio::AudioContext& ctx = engine.module<engine::ModuleAudio>()->context();
    auto fn_destroy = [&ctx](const modus::audio::SoundHandle h) {
        if (h && !ctx.destroy_sound(h)) {
            MODUS_LOGW("Failed to destroy sound");
        }
    };

    fn_destroy(m_audio_assets.ambient.main);
    fn_destroy(m_audio_assets.ship.alert);
    fn_destroy(m_audio_assets.ship.laser1);
    fn_destroy(m_audio_assets.ship.laser2);
    fn_destroy(m_audio_assets.ship.engine);
    fn_destroy(m_audio_assets.ship.death);
    fn_destroy(m_audio_assets.asteroid.impact);
    fn_destroy(m_audio_assets.asteroid.shatter1);
    fn_destroy(m_audio_assets.asteroid.shatter2);
    fn_destroy(m_audio_assets.asteroid.shatter3);
    fn_destroy(m_audio_assets.asteroid.shatter4);

    return Ok<>();
}
}    // namespace modus::game
