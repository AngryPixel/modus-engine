/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <input/player_input_context.hpp>
#include <input/action_binders.hpp>
#include <gameplay/commands/flight_commands.hpp>
#include <gameplay/commands/fire_commands.hpp>
#include <engine/input/default_input_priorities.hpp>

namespace modus::game {

PlayerInputContext::PlayerInputContext(engine::gameplay::GameWorld& game_world)
    : engine::input::MappedInputContext(engine::input::kGameInputPriorityMin),
      m_game_world(game_world) {}

StringSlice PlayerInputContext::name() const {
    return "PlayerInputContext";
}

Result<> PlayerInputContext::initialize(engine::Engine&) {
    // Placeholder
    InputSettings settings;
    GameplayCallbacks callbacks;

    callbacks.on_yaw =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_yaw_action>(this);

    callbacks.on_roll =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_roll_action>(this);

    callbacks.on_pitch =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_pitch_action>(this);

    callbacks.on_vertical =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_vertical_action>(this);

    callbacks.on_thrust =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_thrust_action>(this);

    callbacks.on_fire_begin =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_fire_begin_action>(this);

    callbacks.on_fire_end =
        engine::input::AxisMap::Callback::build<PlayerInputContext,
                                                &PlayerInputContext::on_fire_end_action>(this);

    auto& input_mapper = this->input_mapper();
    bind_gameplay(settings, input_mapper, callbacks);
    return Ok<>();
}

void PlayerInputContext::shutdown(engine::Engine&) {}

void PlayerInputContext::reset() {
    input_mapper().reset_input_state();
}

void PlayerInputContext::on_yaw_action(engine::Engine&, const engine::input::AxisMap& mapping) {
    m_game_world.push_command(FlightYawCommand(m_player_id, mapping.value_normalized()));
}

void PlayerInputContext::on_vertical_action(engine::Engine&,
                                            const engine::input::AxisMap& mapping) {
    m_game_world.push_command(
        FlightVerticalThrusterCommand(m_player_id, mapping.value_normalized()));
}

void PlayerInputContext::on_pitch_action(engine::Engine&, const engine::input::AxisMap& mapping) {
    m_game_world.push_command(FlightPitchCommand(m_player_id, mapping.value_normalized()));
}

void PlayerInputContext::on_roll_action(engine::Engine&, const engine::input::AxisMap& mapping) {
    m_game_world.push_command(FlightRollCommand(m_player_id, mapping.value_normalized()));
}

void PlayerInputContext::on_thrust_action(engine::Engine&, const engine::input::AxisMap& mapping) {
    m_game_world.push_command(FlightThrusterCommand(m_player_id, mapping.value_normalized()));
}

void PlayerInputContext::on_fire_begin_action(engine::Engine&, const engine::input::AxisMap&) {
    m_game_world.push_command(FireBeginCommand(m_player_id));
}

void PlayerInputContext::on_fire_end_action(engine::Engine&, const engine::input::AxisMap&) {
    m_game_world.push_command(FireEndCommand(m_player_id));
}
}    // namespace modus::game
