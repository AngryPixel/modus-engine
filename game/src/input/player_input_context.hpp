/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/ecs_types.hpp>
#include <engine/input/input_mapper.hpp>

namespace modus::game {

class PlayerInputContext final : public engine::input::MappedInputContext {
   private:
    engine::gameplay::GameWorld& m_game_world;
    engine::gameplay::EntityId m_player_id;

   public:
    PlayerInputContext(engine::gameplay::GameWorld& game_world);

    StringSlice name() const override;

    void set_player_entity_id(const engine::gameplay::EntityId player_id) {
        m_player_id = player_id;
    }

    Result<> initialize(engine::Engine& engine);

    void shutdown(engine::Engine& engine);

    void reset();

   private:
    void on_yaw_action(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_vertical_action(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_pitch_action(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_roll_action(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_thrust_action(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_fire_begin_action(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_fire_end_action(engine::Engine&, const engine::input::AxisMap& mapping);
};

}    // namespace modus::game
