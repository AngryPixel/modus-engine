/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/input/input_mapper.hpp>

namespace modus::game {

// place holder
class InputSettings {};

struct GameplayCallbacks {
    engine::input::AxisMap::Callback on_vertical;
    engine::input::AxisMap::Callback on_vertical_begin;
    engine::input::AxisMap::Callback on_vertical_end;
    engine::input::AxisMap::Callback on_yaw;
    engine::input::AxisMap::Callback on_yaw_begin;
    engine::input::AxisMap::Callback on_yaw_end;
    engine::input::AxisMap::Callback on_roll;
    engine::input::AxisMap::Callback on_roll_begin;
    engine::input::AxisMap::Callback on_roll_end;
    engine::input::AxisMap::Callback on_pitch;
    engine::input::AxisMap::Callback on_pitch_begin;
    engine::input::AxisMap::Callback on_pitch_end;
    engine::input::AxisMap::Callback on_thrust;
    engine::input::AxisMap::Callback on_thrust_begin;
    engine::input::AxisMap::Callback on_thrust_end;
    engine::input::AxisMap::Callback on_fire_begin;
    engine::input::AxisMap::Callback on_fire_end;
};

void bind_gameplay(const InputSettings&,
                   engine::input::InputMapper& mapper,
                   const GameplayCallbacks& callbacks);

}    // namespace modus::game
