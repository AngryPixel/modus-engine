/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat off

#include <input/action_binders.hpp>
#include <input/actions.hpp>

namespace modus::game {

static constexpr f32 kDeadZone = 0.3f;

void bind_gameplay(const modus::game::InputSettings&,
                   engine::input::InputMapper& input_mapper,
                   const modus::game::GameplayCallbacks& callbacks) {
    {
        auto r_action = input_mapper.action(u32(GameplayActions::Vertical));
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Veritcal Thrust");
        action->bind_positive_button(app::InputKey::KeyW);
        action->bind_negative_button(app::InputKey::KeyS);
        action->bind_axis(engine::input::AxisBindingType::GameControllerLeftY);
        action->set_dead_zone(kDeadZone);
        action->bind_on_button_down(callbacks.on_vertical_begin);
        action->bind_on_update(callbacks.on_vertical);
        action->bind_on_button_down(callbacks.on_vertical_end);
        action->set_enabled(true);
    }
    {
        auto r_action = input_mapper.action(u32(GameplayActions::Yaw));
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Yaw");
        action->set_dead_zone(kDeadZone);
        action->bind_positive_button(app::InputKey::KeyD);
        action->bind_negative_button(app::InputKey::KeyA);
        action->bind_axis(engine::input::AxisBindingType::GameControllerLeftX);
        action->set_enabled(true);
        action->bind_on_button_down(callbacks.on_yaw_begin);
        action->bind_on_update(callbacks.on_yaw);
        action->bind_on_button_up(callbacks.on_yaw_end);
    }
    {
        auto r_action = input_mapper.action(u32(GameplayActions::Pitch));
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Pitch");
        action->bind_positive_button(app::InputKey::KeyUp);
        action->bind_negative_button(app::InputKey::KeyDown);
        action->bind_axis(engine::input::AxisBindingType::GameControllerRightY);
        action->set_enabled(true);
        action->set_dead_zone(kDeadZone);
        action->set_inverted(true);
        action->bind_on_button_down(callbacks.on_pitch_begin);
        action->bind_on_update(callbacks.on_pitch);
        action->bind_on_button_up(callbacks.on_pitch_end);
    }
    {
        auto r_action = input_mapper.action(u32(GameplayActions::Roll));
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Roll");
        action->bind_positive_button(app::InputKey::KeyRight);
        action->bind_negative_button(app::InputKey::KeyLeft);
        action->bind_axis(engine::input::AxisBindingType::GameControllerRightX);
        action->set_dead_zone(kDeadZone);
        action->set_enabled(true);
        action->bind_on_button_down(callbacks.on_roll_begin);
        action->bind_on_update(callbacks.on_roll);
        action->bind_on_button_up(callbacks.on_roll_end);
    }
    {
        auto r_action = input_mapper.action(u32(GameplayActions::Thruster));
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Thrusters");
        action->bind_positive_button(app::InputKey::KeySpace);
        action->bind_axis(engine::input::AxisBindingType::GameControllerRightTrigger);
        action->set_enabled(true);
        action->bind_on_button_down(callbacks.on_thrust_begin);
        action->bind_on_update(callbacks.on_thrust);
        action->bind_on_button_up(callbacks.on_thrust_end);
    }

    {
        auto r_action = input_mapper.action(u32(GameplayActions::Fire));
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Fire Weapens");
        action->bind_positive_button(app::InputKey::KeyLeftShift);
        action->bind_negative_button(app::InputKey::KeyRightCtrl);
        action->bind_axis(engine::input::AxisBindingType::GameControllerLeftTrigger);
        action->set_enabled(true);
        action->bind_on_button_down(callbacks.on_fire_begin);
        action->bind_on_button_up(callbacks.on_fire_end);
    }
}

}    // namespace modus::game
