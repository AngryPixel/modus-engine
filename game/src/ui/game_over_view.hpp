/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/views/dialog_view.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::ui {
class Button;
class Label;
}    // namespace modus::ui

namespace modus::game {

class GameOverView final : public modus::ui::DialogView {
   private:
    modus::ui::UIRefPtr<modus::ui::Label> m_score_label;
    modus::ui::UIRefPtr<modus::ui::Label> m_accuracy_label;
    Optional<bool> m_exit_value;

   public:
    GameOverView(ui::ContextPtr context);

    ~GameOverView();

    void create_ui();

    void on_enter() override;

    void on_exit() override;

    void set_score_text(const StringSlice text);

    void set_accuracy_text(const StringSlice text);

    Optional<bool> exit_value() const { return m_exit_value; }

   private:
    void on_replay_clicked(ui::Button&);

    void on_exit_clicked(ui::Button&);
};

}    // namespace modus::game
