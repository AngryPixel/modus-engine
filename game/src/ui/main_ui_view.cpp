/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include "ui/main_ui_view.hpp"
#include <ui/layouts/hbox_layout.hpp>
#include <ui/layouts/vbox_layout.hpp>
#include <ui/objects/null_object.hpp>
#include <ui/objects/button.hpp>
#include <ui/context.hpp>
#include <ui/text/font.hpp>
#include <modus_game.hpp>
#include <gameplay/modus_world.hpp>

#include <engine/modules/module_gameplay.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::game {

MainUIView::MainUIView(ui::ContextPtr context, engine::Engine& engine)
    : ui::ObjectView(context), m_engine(engine), m_begin_clicked(false) {}

void MainUIView::create_ui() {
    ui::ContextPtr context = this->context();

    auto hlayout = context->create_object<ui::HBoxLayout>();
    {
        auto& constraint = hlayout->constraint();
        constraint.max_width = ui::IncomingValue(0.3f);
        constraint.max_height = ui::IncomingValue();
        hlayout->set_margin_inner(10);
        hlayout->set_margin_outer(10);
    }

    auto vlayout = context->create_object<ui::VBoxLayout>();
    {
        auto& constraint = vlayout->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::IncomingValue();
        vlayout->set_margin_inner(30);
    }

    auto begin_button = context->create_object<ui::Button>();
    {
        auto& constraint = begin_button->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::ConstantValue(40);
        begin_button->set_text("Start");
        begin_button->m_on_clicked.bind<MainUIView, &MainUIView::on_begin_clicked>(this);
    }

    auto exit_button = context->create_object<ui::Button>();
    {
        auto& constraint = exit_button->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::ConstantValue(40);
        exit_button->set_text("Quit");
        exit_button->m_on_clicked.bind<MainUIView, &MainUIView::on_exit_clicked>(this);
    }

    auto spacer = context->create_object<ui::NullObject>();
    {
        auto& constraint = spacer->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::FlexValue();
    }

    vlayout->add_child(spacer);
    vlayout->add_child(begin_button);
    vlayout->add_child(exit_button);

    hlayout->add_child(vlayout);
    set_object(hlayout);
}

void MainUIView::on_exit() {
    if (m_begin_clicked) {
        auto gameplay_module = m_engine.module<engine::ModuleGameplay>();
        ModusGame& game = static_cast<ModusGame&>(m_engine.game());
        if (!game.m_assets.load_immediate(m_engine)) {
            MODUS_LOGE("Failed to initialize assets");
            m_engine.quit();
            return;
        }
        if (!game.m_default_world->initialize(m_engine)) {
            MODUS_LOGE("Failed to initialize world");
            m_engine.quit();
            return;
        }
        gameplay_module->set_world(m_engine, game.m_default_world.get());
        m_begin_clicked = false;
    }
}

void MainUIView::on_begin_clicked(ui::Button&) {
    context()->view_controller().pop_view();
    m_begin_clicked = true;
}

void MainUIView::on_exit_clicked(ui::Button&) {
    m_engine.quit();
}

}    // namespace modus::game
