/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include "ui/game_over_view.hpp"
#include <ui/layouts/hbox_layout.hpp>
#include <ui/layouts/vbox_layout.hpp>
#include <ui/objects/null_object.hpp>
#include <ui/objects/label.hpp>
#include <ui/objects/button.hpp>
#include <ui/context.hpp>
#include <ui/text/font.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::game {

static modus::ui::UIRefPtr<modus::ui::Label> create_label(ui::VBoxLayout& layout,
                                                          const StringSlice text) {
    ui::ContextPtr ctx = layout.context();

    auto hlayout = ctx->create_object<ui::HBoxLayout>();
    hlayout->constraint().max_width = ui::IncomingValue();
    hlayout->constraint().max_height = ui::ConstantValue(40);
    hlayout->set_margin_inner(10);

    auto desc_label = ctx->create_object<ui::Label>();
    desc_label->set_text(text);
    desc_label->set_color(ui::make_color(255, 255, 255));
    desc_label->constraint().max_height = ui::IncomingValue();
    desc_label->constraint().max_width = ui::FlexValue(4);
    desc_label->set_horizontal_alignment(ui::HorizontalAlignment::Left);

    auto input_label = ctx->create_object<ui::Label>();
    input_label->set_color(ui::make_color(255, 255, 255));
    input_label->constraint().max_height = ui::IncomingValue();
    input_label->constraint().max_width = ui::FlexValue(2);
    input_label->set_horizontal_alignment(ui::HorizontalAlignment::Left);

    hlayout->add_child(desc_label);
    hlayout->add_child(input_label);
    layout.add_child(hlayout);
    return input_label;
}

GameOverView::GameOverView(ui::ContextPtr context) : ui::DialogView(context) {
    set_background_color(ui::make_color(0, 0, 0, 200));
}

GameOverView::~GameOverView() {}

void GameOverView::create_ui() {
    ui::ContextPtr context = this->context();

    // Main layout
    auto vlayout = context->create_object<ui::VBoxLayout>();
    {
        auto& constraint = vlayout->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::IncomingValue();
        vlayout->set_margin_inner(20);
        vlayout->set_margin_outer(20);
    }

    m_score_label = create_label(*vlayout, "Final Score");
    m_accuracy_label = create_label(*vlayout, "Accuracy");

    auto vspacer = context->create_object<ui::NullObject>();
    {
        auto& constraint = vspacer->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::FlexValue();
    }

    auto begin_button = context->create_object<ui::Button>();
    {
        auto& constraint = begin_button->constraint();
        constraint.max_width = ui::FlexValue();
        constraint.max_height = ui::ConstantValue(40);
        begin_button->set_text("Replay");
        begin_button->m_on_clicked.bind<GameOverView, &GameOverView::on_replay_clicked>(this);
    }

    auto exit_button = context->create_object<ui::Button>();
    {
        auto& constraint = exit_button->constraint();
        constraint.max_width = ui::FlexValue();
        constraint.max_height = ui::ConstantValue(40);
        exit_button->set_text("Quit");
        exit_button->m_on_clicked.bind<GameOverView, &GameOverView::on_exit_clicked>(this);
    }

    auto hlayout = context->create_object<ui::HBoxLayout>();
    {
        auto& constraint = hlayout->constraint();
        constraint.max_width = ui::IncomingValue();
        constraint.max_height = ui::ConstantValue(50);
        hlayout->set_margin_inner(20);
    }

    hlayout->add_child(begin_button);
    hlayout->add_child(exit_button);

    vlayout->add_child(vspacer);
    vlayout->add_child(hlayout);
    set_object(vlayout);
}

void GameOverView::on_enter() {
    m_exit_value.reset();
}

void GameOverView::on_exit() {}

void GameOverView::set_score_text(const StringSlice text) {
    m_score_label->set_text(text);
}

void GameOverView::set_accuracy_text(const StringSlice text) {
    m_accuracy_label->set_text(text);
}

void GameOverView::on_replay_clicked(ui::Button&) {
    m_exit_value = true;
}

void GameOverView::on_exit_clicked(ui::Button&) {
    m_exit_value = false;
}

}    // namespace modus::game
