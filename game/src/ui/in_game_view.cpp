/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <ui/in_game_view.hpp>
// clang-format on

#include <ui/context.hpp>
#include <ui/text/font.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>

#include <gameplay/modus_world.hpp>

namespace modus::game {

constexpr f32 reticle_size_ratio = 0.02f;

InGameView::InGameView(ui::ContextPtr ctx) : ObjectView(ctx) {}

void InGameView::paint(ui::Painter& painter, const ui::PaintParams& params) const {
    if (m_world) {
        // TODO: Store last5 points and draw all of them
        const engine::gameplay::EntityId player_id = m_world->player_entity_id();
        const engine::gameplay::EntityId camera_id = m_world->main_camera();
        const auto& entity_manager = m_world->entity_manager();
        auto r_camera = entity_manager.component<engine::graphics::CameraComponent>(camera_id);
        auto r_transform =
            entity_manager.component<engine::gameplay::TransformComponent>(player_id);
        if (!r_camera || !r_transform) {
            MODUS_LOGE("Failed to retrieve camera ID");
        } else {
            const math::Transform& world_transform = (*r_transform)->world_transform();
            const glm::vec3 point = world_transform.translation() +
                                    (world_transform.rotation() * glm::vec3(0., 0, 10000.0f));
            const glm::vec2 screen_center = (*r_camera)->m_camera.point_to_screen_coord(
                point, u32(m_size.width), u32(m_size.height));
            const f32 width = reticle_size_ratio * m_size.height;
            const f32 height = width;
            const ui::Rect rect = ui::make_rect(
                {screen_center.x - width / 2.0f, screen_center.y - height / 2.f}, {width, height});
            painter.draw_border(rect, params.z_index, ui::make_color(200, 200, 200, 255), 4.f);
        }
    }
    ObjectView::paint(painter, params);
}

}    // namespace modus::game
