/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ui/views/object_view.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::ui {
class Button;
}

namespace modus::game {

class MainUIView final : public modus::ui::ObjectView {
   private:
    engine::Engine& m_engine;
    bool m_begin_clicked;

   public:
    MainUIView(ui::ContextPtr context, engine::Engine& engine);

    void create_ui();

    void on_exit() override;

   private:
    void on_begin_clicked(ui::Button&);

    void on_exit_clicked(ui::Button&);
};

}    // namespace modus::game
