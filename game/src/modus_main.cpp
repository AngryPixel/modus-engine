/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <modus_game.hpp>

using namespace modus;

int main(const int argc, const char** argv) {
    engine::Engine engine;
    game::ModusGame game;

    auto r_parse = engine.parse_command_options(argc, argv, game);
    if (!r_parse) {
        return EXIT_FAILURE;
    } else if (r_parse.value()) {
        return EXIT_SUCCESS;
    }

    if (!engine.initialize(game)) {
        fprintf(stderr,
                "Modus: Failed to initialize engine. Check the log for more "
                "details\n");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr,
                "Modus: Failed to shutdown engine. Check the log for more "
                "details\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
