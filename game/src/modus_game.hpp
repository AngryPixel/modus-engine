/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <core/getopt.hpp>
#include <engine/igame.hpp>
#include <engine/input/basic_input_contexts.hpp>
#include <gameplay/entity_templates/entity_templates.hpp>
#include <graphics/materials/modus_materials.hpp>
#include <graphics/modus_pipeline.hpp>
#include <modus_assets.hpp>

namespace modus::engine {
class View;
}

namespace modus::gameplay {
class GameWorld;
}

namespace modus::game {

class ModusGame final : public engine::IGame {
    friend class MainUIView;

   private:
    struct Options {
        CmdOptionString world;
        Options();
    };

    struct EngineModules;
    std::unique_ptr<engine::gameplay::GameWorld> m_default_world;
    std::unique_ptr<engine::gameplay::GameWorld> m_main_world;
    std::unique_ptr<EngineModules> m_engine_modules;
    ModusPipeline m_pipeline;
    engine::input::CtrlAltQuitInputContext m_quit_input_context;
    Options m_cmd_opts;
    ModusAssets m_assets;
    ModusMaterials m_materials;
    EntityTemplates m_entity_templates;

   public:
    ModusGame();

    ~ModusGame();

    Result<> register_modules(engine::ModuleRegistrator& registrator) override;

    Result<> add_cmd_options(modus::CmdOptionParser&) override;

    StringSlice name() const override;

    StringSlice name_preferences() const override;

    Result<> initialize(engine::Engine& engine) override;

    Result<> shutdown(engine::Engine& engine) override;

    void tick(engine::Engine& engine) override;

    const ModusAssets& assets() const { return m_assets; }

    ModusAssets& assets() { return m_assets; }

    ModusPipeline& pipeline() { return m_pipeline; }
};

}    // namespace modus::game
