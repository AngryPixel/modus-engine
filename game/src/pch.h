/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// clang-format off
#include <engine/modules/module_gameplay_pch.h>
#include <engine/modules/module_physics_pch.h>
#include <engine/modules/module_audio_pch.h>
#include <engine/modules/module_graphics_pch.h>
#include <engine/modules/module_ui_pch.h>
#include <modus_pch.hpp>
// clang-format on

#define MODUS_PROFILE_GAME_BLOCK(name) MODUS_PROFILE_BLOCK(name, modus::profiler::color::Indigo)
#define MODUS_PROFILE_GAME_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::Indigo)
