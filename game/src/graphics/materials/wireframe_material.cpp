/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <graphics/materials/wireframe_material.hpp>
#include <engine/engine.hpp>

#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/pipeline.hpp>
#include <threed/buffer.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_animation.hpp>

#include <engine/modules/module_graphics.hpp>
#include <graphics/modus_pipeline.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <threed/program_gen/shader_generator.hpp>

namespace modus::game {

WireframeMaterialInstance::WireframeMaterialInstance(const engine::graphics::MaterialHandle h,
                                                     const u32 flags)
    : MaterialInstance(h, flags),
      m_prop_diffuse(kPropNameDiffuse, glm::vec4(0.2f, 0.2f, 0.2f, 1.0f)),
      m_prop_edge_color(kPropNameEdgeColor, glm::vec4(1.0f)),
      m_prop_animated(kPropNameAnimated, false) {}

void WireframeMaterialInstance::apply(engine::Engine& engine,
                                      engine::graphics::MaterialApplyParams& params) const {
    MODUS_UNUSED(engine);
    modus_assert(params.base_material.material_handle() == material_handle());
    const WireframeMaterial& material_type =
        static_cast<const WireframeMaterial&>(params.base_material);
    modus_assert(material_type.effect_handle());

    const glm::mat4 mvp =
        params.frame_globals.constant_block().mat_project_view * params.renderable_transform;
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    params.command.effect = material_type.effect_handle();

    material_type.m_mvp_binder = mvp;
    material_type.m_model_matrix_binder = params.renderable_transform;
    material_type.m_edge_color_binder = m_prop_edge_color.value();
    material_type.m_diffuse_binder = m_prop_diffuse.value();
    if (m_prop_animated.value()) {
        params.command.program = material_type.m_prog_animated;
        auto& animation_cache = graphics_module->animation_buffer_cache();
        if (params.animator) {
            if (!m_cbuffer) {
                if (auto r = animation_cache.create_buffer(device); r) {
                    m_cbuffer = *r;
                }
            }
            auto animation_module = engine.module<engine::ModuleAnimation>();
            auto r_data = animation_module->manager().animation_data(params.animator);
            if (r_data) {
                auto r_buffer = animation_cache.update_buffer(device, m_cbuffer, *r_data);
                if (r_buffer) {
                    material_type.m_anim_cb_binder = m_cbuffer;
                }
            }
        } else {
            material_type.m_anim_cb_binder = animation_cache.identity_buffer();
        }
        material_type.m_binders_animated.bind(params.command, device);
    } else {
        params.command.program = material_type.forward_program_handle();
        material_type.m_binders.bind(params.command, device);
    }
}

void WireframeMaterialInstance::apply_instanced(
    engine::Engine& engine,
    engine::graphics::MaterialInstancedApplyParams& params) const {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(params);
}

Result<NotMyPtr<engine::graphics::MaterialProperty>, void> WireframeMaterialInstance::find_property(
    const StringSlice name) {
    if (name == kPropNameDiffuse) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_diffuse);
    } else if (name == kPropNameEdgeColor) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_edge_color);
    } else if (name == kPropNameAnimated) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_animated);
    }
    return Error<>();
}

Result<NotMyPtr<const engine::graphics::MaterialProperty>, void>
WireframeMaterialInstance::find_property(const StringSlice name) const {
    if (name == kPropNameDiffuse) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_diffuse);
    } else if (name == kPropNameEdgeColor) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_edge_color);
    } else if (name == kPropNameAnimated) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_animated);
    }
    return Error<>();
}

static constexpr u32 kChunksPerBlock = 12;
WireframeMaterial::WireframeMaterial()
    : MaterialHelper<WireframeMaterialInstance>(kGUID, "Wireframe", kChunksPerBlock) {
    // m_flags |= engine::graphics::kMaterialFlagSupportsInstancing;
}

static const char* kFragmentShaderMain = R"R(
void main() {
    vec4 diffuse_color = u_diffuse;
    vec4 edge_color = u_edge_color;
    float min_dist = min(min(v_barycentric.x, v_barycentric.y), v_barycentric.z);
    float edge_intensity = 1.0 - step(0.025, min_dist);
    vec4 diffuse = (1.0 - edge_intensity) * diffuse_color;
    vec4 emissive= edge_intensity * edge_color;

    vec3 normal = normalize(v_normal);
    OUT_COLOR = emissive + (diffuse * modus_calculate_light(normal, 0.0));
}
)R";

Result<> WireframeMaterial::initialize(engine::Engine& engine) {
    MODUS_UNUSED(engine);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    const auto& shader_generator = device.shader_generator();
    const auto& shader_snippet_db = graphics_module->shader_snippet_db();

    threed::dynamic::ProgramGenerator generator;
    threed::dynamic::TypedDefaultConstantInput<glm::mat4> mvp_input("u_mvp", &m_mvp_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::mat4> model_matrix_input(
        "u_model_matrix", &m_model_matrix_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::vec4> diffuse_input("u_diffuse",
                                                                        &m_diffuse_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::vec4> edge_color_input("u_edge_color",
                                                                           &m_edge_color_binder);
    engine::graphics::AnimationCBufferInput anim_cb_input(&m_anim_cb_binder);
    // non-animated
    {
        threed::dynamic::ProgramGenParams params;

        if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                     "engine.frame_globals")) {
            MODUS_LOGE("Default Material: Failed to locate frame_globals shader snippet");
            return Error<>();
        }

        if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                     "modus.lighting")) {
            MODUS_LOGE("Default Material: Failed to locate lighting shader snippet");
            return Error<>();
        }

        params.varying
            .push_back({"v_position", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_normal", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Flat,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_uv", threed::DataType::Vec2F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_barycentric", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_vertex_input())
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_normal_input())
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_uv_input())
            .expect();
        params.vertex_stage.constants.push_back(&mvp_input).expect();
        params.vertex_stage.constants.push_back(&model_matrix_input).expect();
        params.vertex_stage.main = R"R(
void main() {
    const vec3 barycentric_coords[3] = vec3[] (
        vec3(1, 0, 0),
        vec3(0, 1, 0),
        vec3(0, 0, 1)
    );
    v_normal = NORMAL;
    v_position = vec3(u_model_matrix * VERTEX);
    v_uv = UV;
    v_barycentric = barycentric_coords[gl_VertexID % 3];
    gl_Position = u_mvp * VERTEX;
})R";
        params.fragment_stage.fragment_outputs
            .push_back(engine::graphics::shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.constants.push_back(&diffuse_input).expect();
        params.fragment_stage.constants.push_back(&edge_color_input).expect();
        params.fragment_stage.main = kFragmentShaderMain;
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
            return Error<>();
        } else {
            m_prog_forward_handle = *r;
        }

        generator.reset();
    }

    // Animated
    {
        threed::dynamic::ProgramGenParams params;

        if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                     "engine.frame_globals")) {
            MODUS_LOGE("Default Material: Failed to locate frame_globals shader snippet");
            return Error<>();
        }

        if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                     "modus.lighting")) {
            MODUS_LOGE("Default Material: Failed to locate lighting shader snippet");
            return Error<>();
        }

        params.varying
            .push_back({"v_position", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_normal", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Flat,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_uv", threed::DataType::Vec2F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_barycentric", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_vertex_input())
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_normal_input())
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_uv_input())
            .expect();
        params.vertex_stage.inputs
            .push_back(engine::graphics::shader_desc::default_bone_indices_input())
            .expect();
        params.vertex_stage.inputs
            .push_back(engine::graphics::shader_desc::default_bone_weights_input())
            .expect();
        params.vertex_stage.constants.push_back(&mvp_input).expect();
        params.vertex_stage.constants.push_back(&model_matrix_input).expect();
        params.vertex_stage.constant_buffers.push_back(&anim_cb_input).expect();
        params.vertex_stage.main = R"R(
void main() {
    const vec3 barycentric_coords[3] = vec3[] (
        vec3(1, 0, 0),
        vec3(0, 1, 0),
        vec3(0, 0, 1)
    );
    mat4 bone_transform = get_bone_transform();
    vec4 position = bone_transform * VERTEX;
    vec3 normal = (bone_transform * vec4(NORMAL, 0)).xyz;
    v_normal = normalize(normal);
    v_position = vec3(u_model_matrix * position);
    v_uv = UV;
    v_barycentric = barycentric_coords[gl_VertexID % 3];
    gl_Position = u_mvp * position;
})R";
        params.fragment_stage.fragment_outputs
            .push_back(engine::graphics::shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.constants.push_back(&diffuse_input).expect();
        params.fragment_stage.constants.push_back(&edge_color_input).expect();
        params.fragment_stage.main = kFragmentShaderMain;
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_binders_animated = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
            return Error<>();
        } else {
            m_prog_animated = *r;
        }

        generator.reset();
    }

    // Effect
    threed::EffectCreateParams effect_params;
    effect_params.state.depth_stencil.depth.enabled = true;

    effect_params.state.raster.cull_mode = threed::CullMode::Back;
    effect_params.state.raster.cull_enabled = true;
    effect_params.state.raster.face_counter_clockwise = true;

    auto effect_create_result = device.create_effect(effect_params);
    if (!effect_create_result) {
        MODUS_LOGE("WireframeMaterial: Failed to create effect: {}", effect_create_result.error());
        return Error<>();
    }
    m_effect_handle = effect_create_result.release_value();
    return Ok<>();
}

Result<> WireframeMaterial::shutdown(engine::Engine& engine) {
    MODUS_UNUSED(engine);
    if (m_effect_handle.is_valid()) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();
        device.destroy_effect(m_effect_handle);
        m_effect_handle = threed::EffectHandle();
    }
    return Ok<>();
}

}    // namespace modus::game
