/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <graphics/materials/cage_material.hpp>
#include <engine/engine.hpp>

#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/pipeline.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <threed/program_gen/shader_generator.hpp>

namespace modus::game {

CageMaterialInstance::CageMaterialInstance(const engine::graphics::MaterialHandle h,
                                           const u32 flags)
    : engine::graphics::MaterialInstance(h, flags),
      m_prop_color_line(kPropertyNameColorLine, glm::vec4(1.f)),
      m_prop_color_background(kPropertyNameColorBackground, glm::vec4(0.3f, 0.3f, 0.3f, 1.f)) {}

void CageMaterialInstance::apply(engine::Engine& engine,
                                 engine::graphics::MaterialApplyParams& params) const {
    MODUS_UNUSED(engine);
    modus_assert(params.base_material.material_handle() == material_handle());
    modus_assert(params.base_material.effect_handle());

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    const CageMaterial& material_type = static_cast<const CageMaterial&>(params.base_material);
    params.command.effect = material_type.effect_handle();
    params.command.program = material_type.forward_program_handle();

    const glm::mat4 mvp =
        params.frame_globals.constant_block().mat_project_view * params.renderable_transform;
    material_type.m_mvp_binder = mvp;
    material_type.m_color_solid_binder = m_prop_color_background.value();
    material_type.m_color_line_binder = m_prop_color_line.value();
    material_type.m_binders.bind(params.command, device);
}

Result<NotMyPtr<engine::graphics::MaterialProperty>, void> CageMaterialInstance::find_property(
    const StringSlice name) {
    if (name == kPropertyNameColorLine) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_color_line);
    } else if (name == kPropertyNameColorBackground) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_color_background);
    }
    return Error<>();
}

Result<NotMyPtr<const engine::graphics::MaterialProperty>, void>
CageMaterialInstance::find_property(const StringSlice name) const {
    if (name == kPropertyNameColorLine) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_color_line);
    } else if (name == kPropertyNameColorBackground) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_color_background);
    }
    return Error<>();
}

static constexpr u32 kChunksPerBlock = 8;

CageMaterial::CageMaterial()
    : MaterialHelper<CageMaterialInstance>("f7f3be3f-de9a-448f-b75d-cf177edd295e",
                                           "Cage",
                                           kChunksPerBlock) {
    m_flags |= engine::graphics::kMaterialFlagTransparent;
}

Result<> CageMaterial::initialize(engine::Engine& engine) {
    MODUS_UNUSED(engine);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    const auto& shader_generator = device.shader_generator();

    threed::dynamic::ProgramGenerator generator;
    threed::dynamic::ProgramGenParams params;

    threed::dynamic::TypedDefaultConstantInput<glm::mat4> mvp_input("u_mvp", &m_mvp_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::vec4> color_line_input("u_color_line",
                                                                           &m_color_line_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::vec4> color_solid_input("u_color_solid",
                                                                            &m_color_solid_binder);

    params.varying
        .push_back({"v_uv", threed::DataType::Vec2F32, threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Medium})
        .expect();
    params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_vertex_input())
        .expect();
    params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_uv_input())
        .expect();
    params.vertex_stage.constants.push_back(&mvp_input).expect();
    params.vertex_stage.main = R"R(
void main() {
    v_uv = UV;
    gl_Position = u_mvp * VERTEX;
})R";
    params.fragment_stage.fragment_outputs
        .push_back(engine::graphics::shader_desc::default_frag_output())
        .expect();
    params.fragment_stage.constants.push_back(&color_line_input).expect();
    params.fragment_stage.constants.push_back(&color_solid_input).expect();
    params.fragment_stage.main = R"R(
float random (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}
void main() {
    vec4 diffuse_color = u_color_solid;
    vec4 edge_color = u_color_line;
    vec2 st = v_uv * 50.0; // Scale the coordinate system by 10
    vec2 ipos = floor(st);  // get the integer coords
    //vec2 fpos = fract(st);  // get the fractional coords
    float step_result = step(0.8, random(ipos));
    // Assign a random value based on the integer coord
    vec4 bg_color = u_color_line * step_result + u_color_solid * (1.0 - step_result);
    // calculate borders
    // bottom-left
    vec2 bl = step(vec2(0.025),v_uv);
    float pct = bl.x * bl.y;
    // top-right
    vec2 tr = step(vec2(0.025),1.0-v_uv);
    pct *= tr.x * tr.y;
    OUT_COLOR = pct * bg_color + (1.0 - pct) * u_color_line;
}
)R";
    params.skip_instanced_variant = true;

    if (auto r = generator.generate(params, shader_generator); !r) {
        MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
        return Error<>();
    }

    auto result = generator.result(0).value_or_panic();
    m_binders = result->binders;

    if (auto r = device.create_program(result->create_params); !r) {
        MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
        return Error<>();
    } else {
        m_prog_forward_handle = *r;
    }
    // Effect
    threed::EffectCreateParams effect_params;
    effect_params.state.depth_stencil.depth.enabled = true;
    effect_params.state.raster.cull_enabled = true;
    effect_params.state.raster.cull_mode = threed::CullMode::Front;

    auto effect_create_result = device.create_effect(effect_params);
    if (!effect_create_result) {
        MODUS_LOGE("CageMaterial: Failed to create effect: {}", effect_create_result.error());
        return Error<>();
    }
    m_effect_handle = effect_create_result.release_value();
    return Ok<>();
}

Result<> CageMaterial::shutdown(engine::Engine& engine) {
    MODUS_UNUSED(engine);
    if (m_effect_handle.is_valid()) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();
        device.destroy_effect(m_effect_handle);
        m_effect_handle = threed::EffectHandle();
    }

    return Ok<>();
}

}    // namespace modus::game
