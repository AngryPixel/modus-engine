/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/assets/asset_types.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/material_properties.hpp>
#include <threed/program_gen/default_inputs.hpp>

namespace modus::game {

class MODUS_ENGINE_EXPORT PalettedMaterialInstance final
    : public engine::graphics::MaterialInstance {
   private:
    engine::graphics::TextureHandleMaterialProperty m_prop_palette;

   public:
    static constexpr const char* kPropNamePalette = "palette";

    PalettedMaterialInstance(const engine::graphics::MaterialHandle h, const u32 flags);

    void apply(engine::Engine& engine,
               engine::graphics::MaterialApplyParams& params) const override;

    void apply_instanced(engine::Engine& engine,
                         engine::graphics::MaterialInstancedApplyParams& params) const override;

   protected:
    Result<NotMyPtr<engine::graphics::MaterialProperty>, void> find_property(
        const StringSlice name) override;
    Result<NotMyPtr<const engine::graphics::MaterialProperty>, void> find_property(
        const StringSlice name) const override;
};

class MODUS_ENGINE_EXPORT PalettedMaterial final
    : public engine::graphics::MaterialHelper<PalettedMaterialInstance> {
    friend class PalettedMaterialInstance;

   private:
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_mvp_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_model_matrix_binder;
    mutable threed::dynamic::DefaultSamplerInputBinder m_pallet_binder;
    threed::SamplerHandle m_sampler;

   public:
    static constexpr const char* kGUID = "6ed5ebe8-b976-4ea5-b775-7d7a214df5ed";

    PalettedMaterial();

    Result<> initialize(engine::Engine& engine) override;

    Result<> shutdown(engine::Engine& engine) override;
};

}    // namespace modus::game
