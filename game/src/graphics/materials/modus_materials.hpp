/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <graphics/materials/cage_material.hpp>
#include <graphics/materials/laser_material.hpp>
#include <graphics/materials/paletted_material.hpp>
#include <graphics/materials/wireframe_material.hpp>

namespace modus::engine::graphics {
class MaterialDB;
}

namespace modus::game {

class MODUS_ENGINE_EXPORT ModusMaterials {
   private:
    LaserMaterial m_material_laser;
    CageMaterial m_material_cage;
    WireframeMaterial m_material_wireframe;
    PalettedMaterial m_material_paletted;

   public:
    Result<> initialize(engine::Engine& engine, engine::graphics::MaterialDB& db);

    Result<> shutdown(engine::Engine& engine, engine::graphics::MaterialDB& db);
};

}    // namespace modus::game
