/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <graphics/materials/modus_materials.hpp>
#include <engine/graphics/material.hpp>
#include <engine/engine.hpp>

namespace modus::game {

Result<> ModusMaterials::initialize(engine::Engine& engine, engine::graphics::MaterialDB& db) {
#define ADD_MATERIAL(m)                                                            \
    if (!m.initialize(engine)) {                                                   \
        MODUS_LOGE("ModusMaterials: Failed to initialize material: {}", m.name()); \
        return Error<>();                                                          \
    }                                                                              \
    if (!db.add(&m)) {                                                             \
        MODUS_LOGE("ModusMaterials: Failed to add material: {}", m.name());        \
        return Error<>();                                                          \
    }

    ADD_MATERIAL(m_material_laser);
    ADD_MATERIAL(m_material_cage);
    ADD_MATERIAL(m_material_wireframe);
    ADD_MATERIAL(m_material_paletted);

    return Ok<>();
#undef ADD_MATERIAL
}

Result<> ModusMaterials::shutdown(engine::Engine& engine, engine::graphics::MaterialDB& db) {
#define REM_MATERIAL(m)                                                          \
    db.erase(&m);                                                                \
    if (!m.shutdown(engine)) {                                                   \
        MODUS_LOGE("ModusMaterials: Failed to shutdown material: {}", m.name()); \
    }

    REM_MATERIAL(m_material_paletted);
    REM_MATERIAL(m_material_wireframe);
    REM_MATERIAL(m_material_laser);
    REM_MATERIAL(m_material_cage);
    return Ok<>();
#undef REM_MATERIAL
}

}    // namespace modus::game
