/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <graphics/materials/paletted_material.hpp>
// clang-format on

#include <engine/engine.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/pipeline.hpp>
#include <threed/sampler.hpp>
#include <threed/buffer.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_graphics.hpp>
#include <graphics/modus_pipeline.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <threed/program_gen/shader_generator.hpp>

namespace modus::game {

PalettedMaterialInstance::PalettedMaterialInstance(const engine::graphics::MaterialHandle h,
                                                   const u32 flags)
    : MaterialInstance(h, flags), m_prop_palette(kPropNamePalette) {}

void PalettedMaterialInstance::apply(engine::Engine& engine,
                                     engine::graphics::MaterialApplyParams& params) const {
    MODUS_UNUSED(engine);
    modus_assert(params.base_material.material_handle() == material_handle());
    const PalettedMaterial& material_type =
        static_cast<const PalettedMaterial&>(params.base_material);
    modus_assert(material_type.effect_handle());

    const glm::mat4 mvp =
        params.frame_globals.constant_block().mat_project_view * params.renderable_transform;
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    params.command.effect = material_type.effect_handle();
    params.command.program = material_type.forward_program_handle();
    material_type.m_mvp_binder = mvp;
    material_type.m_model_matrix_binder = params.renderable_transform;
    material_type.m_pallet_binder = m_prop_palette.value();
    material_type.m_binders.bind(params.command, device);
}

void PalettedMaterialInstance::apply_instanced(
    engine::Engine& engine,
    engine::graphics::MaterialInstancedApplyParams& params) const {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(params);
}

Result<NotMyPtr<engine::graphics::MaterialProperty>, void> PalettedMaterialInstance::find_property(
    const StringSlice name) {
    if (name == kPropNamePalette) {
        return Ok(NotMyPtr<engine::graphics::MaterialProperty>(&m_prop_palette));
    }
    return Error<>();
}

Result<NotMyPtr<const engine::graphics::MaterialProperty>, void>
PalettedMaterialInstance::find_property(const StringSlice name) const {
    if (name == kPropNamePalette) {
        return Ok(NotMyPtr<const engine::graphics::MaterialProperty>(&m_prop_palette));
    }
    return Error<>();
}

static constexpr u32 kChunksPerBlock = 12;
PalettedMaterial::PalettedMaterial()
    : MaterialHelper<PalettedMaterialInstance>(kGUID, "Paletted", kChunksPerBlock) {
    // m_flags |= engine::graphics::kMaterialFlagSupportsInstancing;
}

static const char* kFragmentShaderMain = R"R(
void main() {
    vec4 diffuse_color = texture(s_palette, v_uv);
    vec3 normal = normalize(v_normal);
    OUT_COLOR = (diffuse_color * modus_calculate_light(normal, 0.0));
}
)R";

Result<> PalettedMaterial::initialize(engine::Engine& engine) {
    MODUS_UNUSED(engine);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    const auto& shader_generator = device.shader_generator();
    const auto& shader_snippet_db = graphics_module->shader_snippet_db();

    threed::dynamic::ProgramGenerator generator;
    threed::dynamic::TypedDefaultConstantInput<glm::mat4> mvp_input("u_mvp", &m_mvp_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::mat4> model_matrix_input(
        "u_model_matrix", &m_model_matrix_binder);
    threed::dynamic::DefaultSamplerInput pallet_input("s_palette", &m_pallet_binder);
    // non-animated
    {
        threed::dynamic::ProgramGenParams params;

        if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                     "engine.frame_globals")) {
            MODUS_LOGE("Default Material: Failed to locate frame_globals shader snippet");
            return Error<>();
        }

        if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                     "modus.lighting")) {
            MODUS_LOGE("Default Material: Failed to locate lighting shader snippet");
            return Error<>();
        }

        params.varying
            .push_back({"v_position", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_normal", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Flat,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.varying
            .push_back({"v_uv", threed::DataType::Vec2F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Default})
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_vertex_input())
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_normal_input())
            .expect();
        params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_uv_input())
            .expect();
        params.vertex_stage.constants.push_back(&mvp_input).expect();
        params.vertex_stage.constants.push_back(&model_matrix_input).expect();
        params.vertex_stage.main = R"R(
void main() {
    v_normal = vec3(u_model_matrix* vec4(NORMAL,0));
    v_position = vec3(u_model_matrix * VERTEX);
    v_uv = UV;
    gl_Position = u_mvp * VERTEX;
})R";
        params.fragment_stage.fragment_outputs
            .push_back(engine::graphics::shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.samplers.push_back(&pallet_input).expect();
        params.fragment_stage.main = kFragmentShaderMain;
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
            return Error<>();
        } else {
            m_prog_forward_handle = *r;
        }

        generator.reset();
    }

    // Effect
    threed::EffectCreateParams effect_params;
    effect_params.state.depth_stencil.depth.enabled = true;
    effect_params.state.raster.cull_mode = threed::CullMode::Back;
    effect_params.state.raster.cull_enabled = true;
    effect_params.state.raster.face_counter_clockwise = true;

    auto effect_create_result = device.create_effect(effect_params);
    if (!effect_create_result) {
        MODUS_LOGE("PalettedMaterial: Failed to create effect: {}", effect_create_result.error());
        return Error<>();
    }
    m_effect_handle = effect_create_result.release_value();

    threed::SamplerCreateParams sampler_params;
    sampler_params.filter_mag = threed::SamplerFilter::Nearest;
    sampler_params.filter_min = threed::SamplerFilter::Nearest;
    sampler_params.wrap_r = threed::SamplerWrapMode::ClampToEdge;
    sampler_params.wrap_s = threed::SamplerWrapMode::ClampToEdge;
    sampler_params.wrap_t = threed::SamplerWrapMode::ClampToEdge;

    if (auto r = device.create_sampler(sampler_params); !r) {
        MODUS_LOGE("PalettedMaterial: Failed to sampler effect: {}", r.error());
        return Error<>();
    } else {
        m_sampler = *r;
        m_pallet_binder.m_sampler = *r;
    }
    return Ok<>();
}

Result<> PalettedMaterial::shutdown(engine::Engine& engine) {
    MODUS_UNUSED(engine);
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    if (graphics_module) {
        auto& device = graphics_module->device();
        if (m_effect_handle.is_valid()) {
            device.destroy_effect(m_effect_handle);
            m_effect_handle = threed::EffectHandle();
        }
        if (m_sampler) {
            device.destroy_sampler(m_sampler);
            m_sampler = threed::SamplerHandle();
        }
    }
    return Ok<>();
}

}    // namespace modus::game
