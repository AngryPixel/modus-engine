/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <graphics/modus_pipeline.hpp>
#include <threed/compositor.hpp>

namespace modus::game {

#if !defined(MODUS_ENGINE_TARGET_MOBILE)
//#define USE_MULTISAMPLING
#endif

Result<> ModusPipeline::initialize(engine::Engine& engine) {
#if defined(USE_MULTISAMPLING)
    auto graphics_module = engine.module<engine::ModuleGraphics>(engine.modules().graphics());
    const threed::Compositor& compositor = graphics_module->default_compositor();
    threed::FramebufferCompositorCreateParams params;
    params.width = compositor.width();
    params.height = compositor.height();
    // Depth 16F doesn't work properly on RPI4
    params.depth_format = threed::TextureFormat::Depth32F;
    params.colour_format = threed::TextureFormat::R8G8B8;
    params.sample_count = 4;

    if (!m_ms_compositor.initialize(params, graphics_module->device(), 2)) {
        MODUS_LOGE("GamePipeline: Failed to setup MS Compositior");
        return Error<>();
    }
#endif
    if (!m_modus_light_manager.initialize(engine)) {
        MODUS_LOGE("GamePipeline: Failed to initialize light manager");
        return Error<>();
    }
    return Ok<>();
}

Result<> ModusPipeline::shutdown(engine::Engine& engine) {
#if defined(USE_MULTISAMPLING)
    auto graphics_module = engine.module<engine::ModuleGraphics>(engine.modules().graphics());
    m_ms_compositor.shutdown(graphics_module->device());
#endif
    m_modus_light_manager.shutdown(engine);
    return Ok<>();
}

Result<> ModusPipeline::build(engine::Engine& engine, threed::PipelinePtr pipeline) {
    // Check if there's anything to render
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    NotMyPtr<const engine::gameplay::GameWorld> world = gameplay_module->world();
    if (world.get() == nullptr) {
        return Error<>();
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    // Update pipeline state
    m_renderables.clear();
    m_transparent.clear();
    m_opaque.clear();

    const auto& compositor = graphics_module->default_compositor();

#if defined(USE_MULTISAMPLING)
    // Update MS compositor
    if (!m_ms_compositor.update(graphics_module->device())) {
        MODUS_LOGE("GamePipeline: Failed to update MS Compositor");
        return threed::PipelinePtr();
    }
#endif

    const engine::gameplay::EntityManager& entity_manager = world->entity_manager();
    auto r_camera =
        entity_manager.component<engine::graphics::CameraComponent>(world->main_camera());
    if (!r_camera) {
        // No camera found, assume ui
        auto pass = device.allocate_pass(*pipeline, "DefaultClear");
        pass->viewport.width = compositor.width();
        pass->viewport.height = compositor.height();
        pass->frame_buffer = compositor.frame_buffer();
        pass->color_targets = threed::kFramebufferColorTarget0;
        pass->state.clear.colour[0].clear = true;
        pass->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
        pass->state.clear.clear_depth = true;
        pass->state.clear.clear_stencil = true;
        return Ok<>();
    }
    const engine::graphics::Camera& world_camera = (*r_camera)->m_camera;
    {
        MODUS_PROFILE_GAME_BLOCK("Collect Visibles");
        m_renderables.reserve(entity_manager.entity_count());
        engine::graphics::FrustumEvaluator evaluator(world_camera);
        const auto& mesh_db = graphics_module->mesh_db();
        engine::gameplay::EntityManagerView<const engine::gameplay::TransformComponent,
                                            const engine::graphics::GraphicsComponent>
            view(entity_manager);
        view.for_each([this, &mesh_db, &evaluator, &world_camera](
                          const engine::gameplay::EntityId,
                          const engine::gameplay::TransformComponent& tc,
                          const engine::graphics::GraphicsComponent& gc) {
            if (is_component_visible(gc, evaluator)) {
                component_to_renderable(m_renderables, mesh_db, tc, world_camera, gc);
            }
        });
    }

    // Update light & shadow manager
    {
        if (!m_modus_light_manager.update(engine, entity_manager)) {
            MODUS_LOGE("Failed to update light manager");
            return Error<>();
        }
    }

    MODUS_PROFILE_PLOT("Renderables", m_renderables.size());

    // Split into opaque non-opaque
    {
        MODUS_PROFILE_GAME_BLOCK("Categorize");
        m_opaque.reserve(m_renderables.size());
        m_transparent.reserve(m_renderables.size() / 3);
        for (auto& renderable : m_renderables) {
            if (!renderable.material_instance.has_flag(
                    engine::graphics::kMaterialFlagTransparent)) {
                if (renderable.material_instance.has_flag(engine::graphics::KMaterialFlagSkyBox)) {
                    // m_skybox = renderable;
                } else {
                    m_opaque.push_back(renderable);
                }
            } else {
                m_transparent.push_back(renderable);
            }
        }
    }

    // Retrieve all visible objects from the game world
    const auto frame_globals = graphics_module->frame_globals();
    if (!frame_globals) {
        MODUS_LOGE("No Frame Globals avaialble");
        return Error<>();
    }

    // Draw
    {
        MODUS_PROFILE_GAME_BLOCK("Prepare Commands");
        threed::Pass* forward_pass = device.allocate_pass(*pipeline, "Forward");
#if defined(USE_MULTISAMPLING)
        forward_pass->viewport.width = m_ms_compositor.width();
        forward_pass->viewport.height = m_ms_compositor.height();
        forward_pass->frame_buffer = m_ms_compositor.frame_buffer();
#else
        forward_pass->viewport.width = compositor.width();
        forward_pass->viewport.height = compositor.height();
        forward_pass->frame_buffer = compositor.frame_buffer();
#endif
        forward_pass->color_targets = threed::kFramebufferColorTarget0;
        forward_pass->state.clear.colour[0].clear = true;
        forward_pass->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
        forward_pass->state.clear.clear_depth = true;
        forward_pass->state.clear.clear_stencil = true;
        forward_pass->color_targets = threed::kFramebufferColorTarget0;

#if defined(USE_MULTISAMPLING)
        threed::PassFramebufferCopy fbo_copy;
        fbo_copy.fast = true;
        fbo_copy.pass.x = 0;
        fbo_copy.pass.y = 0;
        fbo_copy.pass.width = m_ms_compositor.width();
        fbo_copy.pass.height = m_ms_compositor.height();
        fbo_copy.other.x = 0;
        fbo_copy.other.y = 0;
        fbo_copy.other.width = compositor.width();
        fbo_copy.other.height = compositor.height();
        fbo_copy.other.handle = compositor.frame_buffer();
        fbo_copy.copy_color = true;
        // fbo_copy.copy_depth = true;
        forward_pass->framebuffer_copy.post_pass = fbo_copy;
#endif

        {
            MODUS_PROFILE_GAME_BLOCK("Forward");
            generate_commands(engine, this, *forward_pass, **frame_globals,
                              make_slice_mut(m_opaque), false);
        }
        /*
        if (m_skybox.has_value()) {
            MODUS_PROFILE_BLOCK("SkyBox", profiler::color::Red900);
            generate_commands(engine, this, *forward_pass, **frame_globals,
                              SliceMut(&m_skybox.value(), 1), false);
        }*/
        {
            MODUS_PROFILE_GAME_BLOCK("Transparent");
            std::sort(
                m_transparent.begin(), m_transparent.end(),
                [](const engine::graphics::Renderable& r1, const engine::graphics::Renderable& r2) {
                    const f32 d1 = r1.z_distance;
                    const f32 d2 = r2.z_distance;
                    return d1 > d2;
                });

            generate_commands(engine, this, *forward_pass, **frame_globals,
                              make_slice_mut(m_transparent), true);
        }
    }

#if defined(USE_MULTISAMPLING)
    m_ms_compositor.swap_buffers();
#endif
    return Ok<>();
}

void ModusPipeline::on_compositor_update(engine::Engine&, const threed::Compositor& compositor) {
    MODUS_UNUSED(compositor);
#if defined(USE_MULTISAMPLING)
    m_ms_compositor.set_dimensions(compositor.width(), compositor.height());
#endif
}

}    // namespace modus::game
