/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::gameplay {
class EntityManager;
}

namespace modus::game {

/// Simplified Light Manager, only holds one directional light
class ModusLightManager {
   private:
    struct ShaderSnippet;
    struct alignas(threed::kConstantBufferDataAlignment) LightBlock {
        glm::vec4 ambient;
        glm::vec3 diffuse_color;
        f32 diffuse_intensity;
        glm::vec3 direction;
        f32 padding__;
    };

   private:
    std::unique_ptr<ShaderSnippet> m_shader_snippet;
    LightBlock m_block;
    threed::ConstantBufferHandle m_buffer;

   public:
    ModusLightManager();

    ~ModusLightManager();

    Result<> initialize(engine::Engine& engine);

    void shutdown(engine::Engine& engine);

    Result<> update(engine::Engine& engine, const engine::gameplay::EntityManager& entity_manager);

    threed::ConstantBufferHandle constant_buffer() const { return m_buffer; }
};

}    // namespace modus::game
