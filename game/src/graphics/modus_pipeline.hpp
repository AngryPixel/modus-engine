/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/light.hpp>
#include <engine/graphics/pipeline/pipeline.hpp>
#include <graphics/modus_light_manager.hpp>
#include <threed/framebuffer_compositor.hpp>

namespace modus::game {
class ModusWorld;
class ModusPipeline : public engine::graphics::IPipeline {
   private:
    Vector<engine::graphics::Renderable> m_renderables;
    Vector<engine::graphics::Renderable> m_opaque;
    Vector<engine::graphics::Renderable> m_transparent;
    threed::FramebufferCompositor m_ms_compositor;
    ModusLightManager m_modus_light_manager;

   public:
    MODUS_ENGINE_DECLARE_PIPELINE_ID(2);

    ModusPipeline() : IPipeline(kPipelineId) {}

    Result<> initialize(engine::Engine& engine);

    Result<> shutdown(engine::Engine& engine);

    Result<> build(engine::Engine& engine, threed::PipelinePtr pipeline) override;

    void on_compositor_update(engine::Engine& engine,
                              const threed::Compositor& compositor) override;

    const ModusLightManager& light_manager() const { return m_modus_light_manager; }
};

}    // namespace modus::game
