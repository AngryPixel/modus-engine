/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <graphics/modus_light_manager.hpp>
#include <engine/engine.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <threed/buffer.hpp>
#include <threed/program_gen/shader_snippet_db.hpp>
#include <threed/program_gen/program_gen.hpp>

namespace modus::game {

static const char* kLightShaderCode = R"R(
vec4 modus_do_lighting(in vec3 color,
    in vec3 normal,
    in vec3 light_dir,
    float diffuse_intensity,
    float specular_factor) {

    float n_dot_l = dot(normal, light_dir);
    float diffuse_factor = n_dot_l;

    // calculate diffuse
    vec4 diffuse_color = vec4(color
                * diffuse_intensity
                * diffuse_factor, 1.0);
    return diffuse_color;
}

vec4 modus_calculate_light(in vec3 normal, float specular_factor) {
    vec4 final_color = modus_do_lighting(u_light_color,
        normal,
        u_light_direction,
        u_light_intensity,
        specular_factor);
    vec4 ambient_color = vec4(u_light_ambient.xyz * u_light_ambient.w, 1.0);
    return ambient_color + final_color;
})R";

struct LightCBInput final : public threed::dynamic::DefaultConstantBufferInput {
    LightCBInput(NotMyPtr<const threed::dynamic::InputBinder> binder)
        : threed::dynamic::DefaultConstantBufferInput("ModusLights", binder) {
        m_desc.buffer_members.push_back({"u_light_ambient", threed::DataType::Vec4F32});
        m_desc.buffer_members.push_back({"u_light_color", threed::DataType::Vec3F32});
        m_desc.buffer_members.push_back({"u_light_intensity", threed::DataType::F32});
        m_desc.buffer_members.push_back({"u_light_direction", threed::DataType::Vec3F32});
        m_desc.buffer_members.push_back({"paddingxxxxx", threed::DataType::F32});
    }

    Result<threed::dynamic::BindableInputResult> generate_code_snippet(
        threed::dynamic::GenerateParams& params) const override {
        if (!params.writer->write_exactly(StringSlice(kLightShaderCode).as_bytes())) {
            return Error<>();
        }
        return Ok(threed::dynamic::BindableInputResult::Generated);
    }
};

struct ModusLightManager::ShaderSnippet final : public threed::dynamic::ShaderSnippet {
    threed::dynamic::DefaultConstantBufferInputBinder cb_binder;
    LightCBInput cb_input;

    ShaderSnippet() : threed::dynamic::ShaderSnippet("modus.lighting"), cb_input(&cb_binder) {}

    Result<> apply_common(threed::dynamic::ShaderStageCommon& stage) const {
        if (!stage.constant_buffers.push_back(&cb_input)) {
            MODUS_LOGE("Failed to add lighting cb to stage");
            return Error<>();
        }
        return Ok<>();
    }

    Result<> apply(threed::dynamic::ShaderVertexStage& vertex_stage) const override {
        return apply_common(vertex_stage);
    }

    Result<> apply(threed::dynamic::ShaderFragmentStage& fragment_stage) const override {
        return apply_common(fragment_stage);
    }
};

ModusLightManager::ModusLightManager() {
    m_shader_snippet = modus::make_unique<ShaderSnippet>();
}

ModusLightManager::~ModusLightManager() {}

Result<> ModusLightManager::initialize(engine::Engine& engine) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    threed::ConstantBufferCreateParams params;
    params.usage = threed::BufferUsage::Stream;
    params.size = sizeof(LightBlock);
    params.data = nullptr;

    if (auto r_buffer = device.create_constant_buffer(params); !r_buffer) {
        return Error<>();
    } else {
        m_buffer = *r_buffer;
    }
    m_shader_snippet->cb_binder = m_buffer;

    if (!graphics_module->shader_snippet_db().register_snippet(m_shader_snippet.get())) {
        MODUS_LOGE("ModusLightManager: Failed to register shader snippet");
        return Error<void>();
    }
    return Ok<>();
}

void ModusLightManager::shutdown(engine::Engine& engine) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    if (graphics_module && m_buffer) {
        graphics_module->shader_snippet_db().unregister_snippet(m_shader_snippet.get());
        auto& device = graphics_module->device();
        device.destroy_constant_buffer(m_buffer);
        m_buffer = threed::ConstantBufferHandle();
    }
}

Result<> ModusLightManager::update(engine::Engine& engine,
                                   const engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("ModusLightManager::Update");
    m_block.direction = glm::vec3(1.f);
    m_block.diffuse_color = glm::vec4(1.f);
    m_block.diffuse_intensity = 0.0f;
    m_block.ambient = glm::vec4(0.f, 0.f, 0.f, 1.0);

    {
        MODUS_PROFILE_GAME_BLOCK("Collect Lights");
        // collect directional light
        bool light_found = false;
        engine::gameplay::EntityManagerView<const engine::graphics::LightComponent> view(
            entity_manager);
        for (auto& entity : view) {
            const engine::graphics::LightComponent& lc =
                view.get<const engine::graphics::LightComponent>(entity);
            if (lc.light.type() == engine::graphics::LightType::Ambient) {
                m_block.ambient +=
                    glm::vec4(lc.light.diffuse_color(), lc.light.diffuse_intensity());
            }
            if (light_found) {
                continue;
            }

            if (lc.light.type() != engine::graphics::LightType::Directional) {
                continue;
            }

            m_block.direction = glm::normalize(lc.light.position());
            m_block.diffuse_color = lc.light.diffuse_color();
            m_block.diffuse_intensity = lc.light.diffuse_intensity();
        };
    }
    // Update buffer
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    if (!device.update_constant_buffer(m_buffer, m_block)) {
        MODUS_LOGE("ModusLightManager: Failed to update constant block");
        return Error<>();
    }
    return Ok<>();
}

}    // namespace modus::game
