/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gameplay/entity_manager.hpp>

#include <audio/audio.hpp>
#include <engine/assets/asset_types.hpp>
#include <physics/physics.hpp>

namespace modus::physics {
class PhysicsWorld;
}

namespace modus::audio {
class AudioContext;
}

namespace modus::engine {
class Engine;
}

namespace modus::game {

class ModusAssets {
   public:
    using AssetHandle = engine::assets::AssetHandleV2;
    static constexpr u32 kNumAsteroidTypes = 1;
    static constexpr u32 kNumAsteroidLevels = 3;

    struct AsteroidInfo {
        f32 velocity;
    };

    struct LaserInfo {
        f32 velocity;
    };

    struct AudioAssets {
        struct {
            modus::audio::SoundHandle alert;
            modus::audio::SoundHandle laser1;
            modus::audio::SoundHandle laser2;
            modus::audio::SoundHandle engine;
            modus::audio::SoundHandle death;
        } ship;
        struct {
            modus::audio::SoundHandle main;
        } ambient;
        struct {
            modus::audio::SoundHandle impact;
            modus::audio::SoundHandle shatter1;
            modus::audio::SoundHandle shatter2;
            modus::audio::SoundHandle shatter3;
            modus::audio::SoundHandle shatter4;
        } asteroid;
    };

   private:
    AssetHandle m_cubemap_asset;
    AssetHandle m_cube_mesh_asset;
    AssetHandle m_asteroid_templates[kNumAsteroidLevels];
    AssetHandle m_laser_template;
    AssetHandle m_ship_template;
    AssetHandle m_cage_template;
    AsteroidInfo m_asteroid_info[kNumAsteroidTypes][kNumAsteroidLevels];
    LaserInfo m_laser_info;
    AudioAssets m_audio_assets;

   public:
    ModusAssets() = default;

    Result<> load_immediate(engine::Engine& engine);

    Result<> preload_async(engine::Engine& engine);

    Result<> destroy(engine::Engine& engine);

    AssetHandle ship_template() const { return m_ship_template; }

    AssetHandle laser_template_asset() const { return m_laser_template; }

    AssetHandle cage_template_asset() const { return m_cage_template; }

    AssetHandle cubemap_asset() const { return m_cubemap_asset; }

    Result<engine::gameplay::EntityId, void> spawn_asteroid(
        modus::engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        modus::physics::PhysicsWorld& physics_world,
        const u32 type,
        const u32 level,
        const math::Transform& tranform,
        const glm::vec3& direction,
        const glm::vec3& angular_velocity) const;

    Result<engine::gameplay::EntityId, void> spawn_ship(
        modus::engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        modus::physics::PhysicsWorld& physics_world,
        const glm::vec3& position) const;

    Result<engine::gameplay::EntityId, void> spawn_cage(
        engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        physics::PhysicsWorld& physics_world,
        const f32 radius) const;

    const LaserInfo& laser_info() const { return m_laser_info; }

    const Optional<AsteroidInfo> asteroid_info(const u32 type, const u32 level) const;

    const AudioAssets& audio_assets() const { return m_audio_assets; }

    Result<> load_sounds(engine::Engine& engine);

    Result<> destroy_sounds(engine::Engine&);

   private:
    Result<> setup_asteroid_info(engine::Engine& engine);

    Result<> setup_laser_info(engine::Engine& engine);
};

}    // namespace modus::game
