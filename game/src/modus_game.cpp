/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gameplay/modus_world.hpp>
#include <modus_game.hpp>
#include <os/path.hpp>
#if !defined(MODUS_RTM) || defined(MODUS_PROFILE)
#include <test_worlds/test_worlds_view.hpp>
#endif
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <engine/modules/module_physics.hpp>
#include <engine/modules/module_ui.hpp>
#include <gameplay/main_ui_world.hpp>

namespace modus::game {

struct ModusGame::EngineModules {
    engine::ModuleAudio m_audio;
    engine::ModulePhysics m_physics;
    engine::ModuleGraphics m_graphics;
    engine::ModuleGameplay m_gameplay;
    engine::ModuleAnimation m_animation;
    engine::ModuleUI m_ui;
};

ModusGame::Options::Options()
    : world("", "--game-world", "Select different game world to load other than default", "") {}

ModusGame::ModusGame() {
    m_engine_modules = modus::make_unique<EngineModules>();
}

ModusGame::~ModusGame() {}

Result<> ModusGame::register_modules(engine::ModuleRegistrator& registrator) {
#define REGISTER_MODULE(module)                                               \
    if (!registrator.register_module<decltype(m_engine_modules->m_##module)>( \
            &m_engine_modules->m_##module)) {                                 \
        MODUS_LOGE("Failed to register gameplay " #module);                   \
        return Error<>();                                                     \
    }

    REGISTER_MODULE(gameplay)
    REGISTER_MODULE(audio)
    REGISTER_MODULE(physics)
    REGISTER_MODULE(graphics)
    REGISTER_MODULE(animation)
    REGISTER_MODULE(ui)

#undef REGISTER_MODULE
    return Ok<>();
}

StringSlice ModusGame::name() const {
    return "Modus";
}

StringSlice ModusGame::name_preferences() const {
    return "modus.game";
}

Result<> ModusGame::add_cmd_options(modus::CmdOptionParser& parser) {
    MODUS_UNUSED(parser);
#if !defined(MODUS_RTM) || defined(MODUS_PROFILE)
    if (!parser.add(m_cmd_opts.world)) {
        return Error<>();
    }
#endif
    return Ok<>();
}

Result<> ModusGame::initialize(engine::Engine& engine) {
    MODUS_PROFILE_GAME_BLOCK("Game::initialize");
    MODUS_LOGI("Initialiazing Modus v{}", MODUS_MODUS_VERSION_STRING);

#if !defined(MODUS_RTM) || defined(MODUS_PROFILE)
    if (m_cmd_opts.world.parsed()) {
        auto r = create_test_world(m_cmd_opts.world.value());
        if (!r) {
            MODUS_LOGE("Unknown value specified for --game-world");
            return Error<>();
        }
        m_default_world = r.release_value();
    } else {
        m_default_world = modus::make_unique<ModusWorld>();
    }
#else
    m_default_world = modus::make_unique<ModusWorld>();
#endif
    m_main_world = modus::make_unique<MainUIWorld>();

    auto& vfs = engine.module<engine::ModuleFileSystem>()->vfs();
    // Mount Data Dir
    {
        const String data_path = os::Path::join(MODUS_GAME_ASSET_BIN_DIR, "modus-data");
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "game";
        vfs_params.path = data_path;

        if (auto r_vfs = vfs.mount_path(vfs_params); !r_vfs) {
            MODUS_LOGE("Failed to mount game filesystem at: {}", data_path);
            return Error<>();
        } else {
            MODUS_LOGD("Mounted game path: {}", data_path);
        }
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    // Mount Image Dir
    /*
    const auto device_limits = graphics_module->device().device_limits();

    {
        StringSlice image_type_dir;
        if (true) {
            image_type_dir = "etc2_low";
        } else if (device_limits.compressed_texture_support.etc2) {
            image_type_dir = "etc2";
        } else {
            MODUS_LOGE(
                "No suitable compressed texture format support has been "
                "found");
            return Error<>();
        }
        String data_path =
            os::Path::join(MODUS_GAME_ASSET_BIN_DIR, "modus-images");
        os::Path::join(data_path, image_type_dir);
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "game";
        vfs_params.path = data_path;

        if (auto r_vfs = vfs.mount_path(vfs_params); !r_vfs) {
            MODUS_LOGE("Failed to mount game filesystem at: {}", data_path);
            return Error<>();
        } else {
            MODUS_LOGD("Mounted game path: {}", data_path);
        }
    }*/
    {
        // Mount non-compressed images
        String data_path = os::Path::join(MODUS_GAME_ASSET_BIN_DIR, "modus-images");
        os::Path::join_inplace(data_path, "default");
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "game";
        vfs_params.path = data_path;

        if (auto r_vfs = vfs.mount_path(vfs_params); !r_vfs) {
            MODUS_LOGE("Failed to mount game filesystem at: {}", data_path);
            return Error<>();
        } else {
            MODUS_LOGD("Mounted game path: {}", data_path);
        }
    }

    // Mount Audio dir
    {
        const String data_path = os::Path::join(MODUS_GAME_ASSET_BIN_DIR, "modus-audio");
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "game";
        vfs_params.path = data_path;
        vfs_params.path = data_path;

        if (auto r_vfs = vfs.mount_path(vfs_params); !r_vfs) {
            MODUS_LOGE("Failed to mount game filesystem at: {}", data_path);
            return Error<>();
        } else {
            MODUS_LOGD("Mounted game path: {}", data_path);
        }
    }
    if (!m_pipeline.initialize(engine)) {
        MODUS_LOGE("Failed to load pipline");
        return Error<>();
    }

    if (!m_materials.initialize(engine, graphics_module->material_db())) {
        MODUS_LOGE("Failed to initialize materials");
        return Error<>();
    }

    if (!m_entity_templates.initialize(engine)) {
        MODUS_LOGE("Failed to initialize entity templates");
        return Error<>();
    }

    graphics_module->set_pipeline(&m_pipeline);

    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().add_context(&m_quit_input_context);

    if (!m_assets.preload_async(engine)) {
        MODUS_LOGE("Failed to pre-load game assets");
        return Error<>();
    }
    if (!m_main_world->initialize(engine)) {
        MODUS_LOGE("Failed to initialize world");
        return Error<>();
    }

    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    gameplay_module->set_world(engine, m_main_world.get());
    return Ok<>();
}

Result<> ModusGame::shutdown(engine::Engine& engine) {
    MODUS_PROFILE_GAME_BLOCK("Game::shutdown");
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    gameplay_module->set_world(engine, NotMyPtr<engine::gameplay::GameWorld>());

    if (m_main_world) {
        (void)m_main_world->shutdown(engine);
        m_main_world.reset();
    }

    if (m_default_world) {
        (void)m_default_world->shutdown(engine);
        m_default_world.reset();
    }

    if (!m_assets.destroy(engine)) {
        MODUS_LOGW("Modus: Failed to destroy assets");
    }
    /*
        if (!m_assets2.destroy(engine)) {
            MODUS_LOGW("Modus: Failed to destroy assets");
        }*/

    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().remove_context(&m_quit_input_context);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    graphics_module->unset_pipeline();

    m_entity_templates.shutdown(engine);

    if (!m_materials.shutdown(engine, graphics_module->material_db())) {
        MODUS_LOGE("Failed to shutdown materials");
    }

    if (!m_pipeline.shutdown(engine)) {
        MODUS_LOGW("Modus: Failed to shutdown graphics pipeline");
    }

    MODUS_UNUSED(engine);

    return Ok<>();
}

void ModusGame::tick(engine::Engine& engine) {
    MODUS_UNUSED(engine);
}

}    // namespace modus::game
