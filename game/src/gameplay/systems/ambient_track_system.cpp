/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <audio/audio_device.hpp>
#include <gameplay/systems/ambient_track_system.hpp>
#include <modus_game.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/engine.hpp>

namespace modus::game {

AmbientTrackSystem::AmbientTrackSystem() : engine::gameplay::System("AmbientTrack") {}

Result<> AmbientTrackSystem::initialize(engine::Engine& engine,
                                        engine::gameplay::GameWorld&,
                                        engine::gameplay::EntityManager&) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());

    m_ambient_track = game.assets().audio_assets().ambient.main;
    if (!m_ambient_track) {
        MODUS_LOGE("AmbientTrackSystem: Ambient sound has invalid handle");
        return Error<>();
    }

    auto audio_module = engine.module<engine::ModuleAudio>();
    if (!audio_module) {
        MODUS_LOGE("AudioSystem: Audio module {} is not in module list",
                   engine::module_guid<engine::ModuleAudio>());
        return Error<>();
    }

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();

    modus::audio::AudioSourceCreateParams source_params;
    source_params.gain = 0.1f;
    source_params.looping = true;
    source_params.max_distance = std::numeric_limits<f32>::max();
    source_params.position = glm::vec3(0.f);

    if (auto r_source = audio_context.create_source(source_params); !r_source) {
        MODUS_LOGE("AmbientTrackSystem: Failed to create audio source");
        return Error<>();
    } else {
        m_audio_source = *r_source;
    }

    if (!audio_context.set_source_sound(m_audio_source, m_ambient_track)) {
        MODUS_LOGE("AmbientTrackSystem: Failed to set sound on audio source");
        return Error<>();
    }
    return Ok<>();
}

void AmbientTrackSystem::shutdown(engine::Engine& engine,
                                  engine::gameplay::GameWorld&,
                                  engine::gameplay::EntityManager&) {
    if (m_audio_source) {
        auto& audio_context = engine.module<engine::ModuleAudio>()->context();
        if (!audio_context.destroy_source(m_audio_source)) {
            MODUS_LOGE("AmbientTrackSystem: Failed to destroy audio source");
        }
    }
}

void AmbientTrackSystem::update(engine::Engine&,
                                const IMilisec,
                                engine::gameplay::GameWorld&,
                                engine::gameplay::EntityManager&) {
    // Nothing to do at the moment
    // Ideas:
    // * change track based on health left and/or engaging with enemies
}

void AmbientTrackSystem::update_threaded(engine::ThreadSafeEngineAccessor&,
                                         const IMilisec,
                                         engine::gameplay::ThreadSafeGameWorldAccessor&,
                                         engine::gameplay::ThreadSafeEntityManagerAccessor&) {
    // Nothing to do at the moment
    // Ideas:
    // * change track based on health left and/or engaging with enemies
}

void AmbientTrackSystem::pause(engine::Engine& engine, engine::gameplay::GameWorld&) {
    if (!m_audio_source) {
        return;
    }

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();
    if (!audio_context.pause(m_audio_source)) {
        MODUS_LOGE("AmbientTrackSystem: Failed to pause audio source");
    }
}

void AmbientTrackSystem::resume(engine::Engine& engine, engine::gameplay::GameWorld&) {
    if (!m_audio_source) {
        return;
    }
    auto& audio_context = engine.module<engine::ModuleAudio>()->context();
    if (!audio_context.play(m_audio_source)) {
        MODUS_LOGE("AmbientTrackSystem: Failed to resume/play audio source");
    }
}

}    // namespace modus::game
