/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <audio/audio_device.hpp>
#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/gameplay/ecs/components/transform_animation_component.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>
#include <gameplay/components/asteroid_info_component.hpp>
#include <gameplay/components/health_component.hpp>
#include <gameplay/components/lifetime_component.hpp>
#include <gameplay/modus_world.hpp>
#include <gameplay/systems/asteroid_spawner_system.hpp>
#include <gameplay/systems/cage_system.hpp>
#include <math/easing_functions.hpp>
#include <modus_game.hpp>
#include <physics/world.hpp>

#include <game/ecs/components/asteroid_level_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::game::AsteroidInfoComponent)
namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<modus::game::AsteroidInfoComponent>;
Result<typename Traits::IntermediateType> Traits::load(Engine&, const StorageType& storage) {
    IntermediateType result;
    result.m_level = storage.level();
    return Ok(result);
}
}    // namespace modus::engine::gameplay

namespace modus::game {

AsteroidSpawnerSystem::AsteroidSpawnerSystem(const CageSystem& cage_system)
    : engine::gameplay::System("AsteroidSpawner"),
      m_max_active_asteroid_count(10),
      m_current_active_asteroid_count(0),
      m_max_total_asteroid_spawn_count(100),
      m_total_asteroid_spawn_count(0),
      m_spawn_interval_sec(5.f),
      m_elapsed_spawn_interval_sec(m_spawn_interval_sec),
      m_cage_system(cage_system) {}

void AsteroidSpawnerSystem::update(engine::Engine& engine,
                                   const IMilisec tick,
                                   engine::gameplay::GameWorld& game_world,
                                   engine::gameplay::EntityManager& entity_manager) {
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("AsteroidSpawnerSystem: No physics world provided!");
        return;
    }
    ModusWorld& modus_world = static_cast<ModusWorld&>(*gameplay_module->world());

    {
        MODUS_PROFILE_GAME_BLOCK("Asteroid Spawner Spawn");
        // TODO: Pass Rng into Asteroid Spawner instead
        // Hope for the best?
        // Spawn new asteroids
        if (m_total_asteroid_spawn_count < m_max_total_asteroid_spawn_count) {
            if (m_current_active_asteroid_count < m_max_active_asteroid_count) {
                m_elapsed_spawn_interval_sec += FSeconds(tick);
                if (m_elapsed_spawn_interval_sec >= m_spawn_interval_sec) {
                    if (!spawn_asteroid_random(engine, entity_manager, modus_world)) {
                        MODUS_LOGE("Failed to spawn asteroid!");
                        m_total_asteroid_spawn_count++;
                    }
                    m_elapsed_spawn_interval_sec -= m_spawn_interval_sec;
                }
            }
        }
    }

    {
        MODUS_PROFILE_GAME_BLOCK("Asteroid Spawner Dead check");
        // Check if there are dead asteroids
        engine::gameplay::EntityManagerView<const engine::gameplay::TransformComponent,
                                            const AsteroidInfoComponent>
            view(entity_manager);
        view.for_each([this, &engine, &modus_world, &entity_manager](
                          const engine::gameplay::EntityId id,
                          const engine::gameplay::TransformComponent& tc,
                          const AsteroidInfoComponent& aic) {
            if (entity_manager.is_alive(id)) {
                return;
            }

            const u32 asteroid_level = aic.m_level;
            modus_assert(asteroid_level < ModusAssets::kNumAsteroidLevels);
            if (asteroid_level == 0) {
                m_current_active_asteroid_count--;
            } else if (asteroid_level == 2) {
                return;
            }
            constexpr u32 kNumAsteroidsToSpwanPerLevel[] = {2, 4, 0};
            constexpr u32 kAsteroidsChildSpwanLevel[] = {1, 2, 0};
            static_assert(modus::array_size(kNumAsteroidsToSpwanPerLevel) ==
                          ModusAssets::kNumAsteroidLevels);

            // TODO: Improve scatter pattern based on lazer impact?

            for (u32 i = 0; i < kNumAsteroidsToSpwanPerLevel[asteroid_level]; ++i) {
                if (!spawn_from_impact(engine, entity_manager, modus_world,
                                       tc.world_transform().translation(),
                                       kAsteroidsChildSpwanLevel[asteroid_level])) {
                    MODUS_LOGE("Failed to spawn asteroid from impact");
                }
            }
        });
    }

    {
        MODUS_PROFILE_GAME_BLOCK("Asteroid Spawner Collision");
        // Handle Asteroid collisions
        engine::gameplay::EntityManagerView<const engine::physics::CollisionComponent,
                                            const AsteroidInfoComponent>
            view(entity_manager);
        view.for_each_with_tag(
            kEntityTagAsteroid, [&entity_manager](const engine::gameplay::EntityId id,
                                                  const engine::physics::CollisionComponent& cc,
                                                  const AsteroidInfoComponent& aic) {
                for (const auto& collision : cc.m_collisions) {
                    auto r_other_tag = entity_manager.entity_tag(collision.m_other_id);
                    if (!r_other_tag || (*r_other_tag != kEntityTagLaser)) {
                        return;
                    }

                    // TODO: This needs to be added to the entity template
                    auto r_tac =
                        entity_manager.add_component<engine::gameplay::TransformAnimationComponent>(
                            id);
                    if (!r_tac) {
                        MODUS_LOGE("Failed to add transform animation component to asteroid");
                        return;
                    }
                    engine::gameplay::TransformAnimationComponent::AnimationFn anim_fn = nullptr;

                    switch (aic.m_level) {
                        case 0:
                            anim_fn = [](engine::gameplay::TransformComponent& tc,
                                         const FSeconds elapsed, const FSeconds duration) -> void {
                                const FSeconds half_duration = duration * 0.5f;
                                if (elapsed < half_duration) {
                                    tc.set_scale(math::ease_elastic_out(
                                        elapsed.count(), 150.0f, 110.f, half_duration.count()));
                                } else {
                                    tc.set_scale(math::ease_elastic_out(
                                        (elapsed - half_duration).count(), 110.0f, 150.0f,
                                        half_duration.count()));
                                }
                            };
                            break;
                        case 1:
                            anim_fn = [](engine::gameplay::TransformComponent& tc,
                                         const FSeconds elapsed, const FSeconds duration) -> void {
                                const FSeconds half_duration = duration * 0.5f;
                                if (elapsed < half_duration) {
                                    tc.set_scale(math::ease_elastic_out(
                                        elapsed.count(), 70.0f, 50.f, half_duration.count()));
                                } else {
                                    tc.set_scale(math::ease_elastic_out(
                                        (elapsed - half_duration).count(), 50.0f, 70.0f,
                                        half_duration.count()));
                                }
                            };
                            break;
                        case 2:
                            anim_fn = [](engine::gameplay::TransformComponent& tc,
                                         const FSeconds elapsed, const FSeconds duration) -> void {
                                const FSeconds half_duration = duration * 0.5f;
                                if (elapsed < half_duration) {
                                    tc.set_scale(math::ease_elastic_out(
                                        elapsed.count(), 30.0f, 20.f, half_duration.count()));
                                } else {
                                    tc.set_scale(math::ease_elastic_out(
                                        (elapsed - half_duration).count(), 20.0f, 30.0f,
                                        half_duration.count()));
                                }
                            };
                            break;

                        default:
                            MODUS_LOGE("AsteroidSpwanerSystem: Unknown level: {}", aic.m_level);
                            return;
                    }

                    auto& tac = **r_tac;
                    tac.m_loop = false;
                    tac.m_paused = false;
                    tac.m_duration_sec = FSeconds(0.5f);
                    if (tac.m_elapsed_sec > FSeconds(0.25f)) {
                        tac.m_elapsed_sec = FSeconds(0.25f);
                    }
                    tac.m_animation = anim_fn;
                }
            });
    }
}

void AsteroidSpawnerSystem::update_threaded(engine::ThreadSafeEngineAccessor&,
                                            const IMilisec,
                                            engine::gameplay::ThreadSafeGameWorldAccessor&,
                                            engine::gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("Asteroid Spawner System can't run threaded");
}

void AsteroidSpawnerSystem::debug_info(engine::Engine& engine,
                                       const engine::gameplay::EntityManager& entity_manager) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& debug_drawer = graphics_module->debug_drawer();

    Array<u32, ModusAssets::kNumAsteroidTypes> asteroid_count;
    asteroid_count.fill(0);
    engine::gameplay::EntityManagerView<const AsteroidInfoComponent> view(entity_manager);
    view.for_each(
        [&asteroid_count](const engine::gameplay::EntityId, const AsteroidInfoComponent& aic) {
            modus_assert(aic.m_level < asteroid_count.size());
            asteroid_count[aic.m_level]++;
        });

    debug_drawer.draw_text_2d(
        modus::format("Active Asteroids LVL0        : {:04}", m_current_active_asteroid_count), 20,
        100, 1.0f, glm::vec3(1.0f));
    debug_drawer.draw_text_2d(
        modus::format("Active Asteroids LVL1        : {:04}", asteroid_count[1]), 20, 80, 1.0f,
        glm::vec3(1.0f));
    debug_drawer.draw_text_2d(
        modus::format("Active Asteroids LVL2        : {:04}", asteroid_count[2]), 20, 60, 1.0f,
        glm::vec3(1.0f));
    debug_drawer.draw_text_2d(
        modus::format("Total Asteroids Spawned LVL0 : {}", m_total_asteroid_spawn_count), 20, 40,
        1.0f, glm::vec3(1.0f));
}

void AsteroidSpawnerSystem::reset() {
    m_current_active_asteroid_count = 0;
    m_total_asteroid_spawn_count = 0;
    m_elapsed_spawn_interval_sec = m_spawn_interval_sec;
}

static constexpr u32 kAngularMagnitude = 5;

Result<engine::gameplay::EntityId, void> AsteroidSpawnerSystem::spawn_asteroid_random(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    ModusWorld& modus_world) {
    auto& random_generator = modus_world.random_generator();
    const u32 asteroid_type = u32(random_generator.generate() % ModusAssets::kNumAsteroidTypes);
    const u32 asteroid_level = 0;

    // Random Position
    auto angle_1 = glm::radians(f32(random_generator.generate() % 360));
    auto angle_2 = glm::radians(f32(random_generator.generate() % 360));

    glm::vec3 position = glm::vec3(0.f);
    /*
    position.x = m_cage_system.cage_radius().x * glm::cos(angle_1) * glm::cos(angle_2);
    position.y = m_cage_system.cage_radius().y * glm::sin(angle_1) * glm::cos(angle_2);
    position.z = m_cage_system.cage_radius().z * glm::sin(angle_2);
    */

    // Random Direction + or - 45 degrees along each axis
    angle_1 = glm::radians(f32(-25 + random_generator.generate() % 50));
    angle_2 = glm::radians(f32(-25 + random_generator.generate() % 50));

    glm::vec3 direction_point;
    direction_point.x = m_cage_system.cage_radius().x * glm::cos(angle_1) * glm::cos(angle_2);
    direction_point.y = m_cage_system.cage_radius().y * glm::sin(angle_1) * glm::cos(angle_2);
    direction_point.z = m_cage_system.cage_radius().z * glm::sin(angle_2);

    // Random asteroid rotations
    const f32 angle = glm::radians(f32(random_generator.generate() % 360));
    const f32 x_axis = f32(random_generator.generate() % 360) / 360.0f;
    const f32 y_axis = f32(random_generator.generate() % 360) / 360.0f;
    const f32 z_axis = f32(random_generator.generate() % 360) / 360.0f;
    const glm::quat rotation = glm::angleAxis(angle, glm::vec3(x_axis, y_axis, z_axis));

    const glm::vec3 direction = glm::normalize(direction_point - position);

    math::Transform t = math::Transform::kIdentity;
    t.set_translation(position);
    MODUS_UNUSED(rotation);
    t.set_rotation(rotation);

    const f32 angular_speed = f32(random_generator.generate() % kAngularMagnitude);
    const glm::vec3 angular_velocity =
        glm::normalize(glm::vec3(x_axis, y_axis, z_axis)) * angular_speed;

    auto r = spawn_asteroid(engine, entity_manager, modus_world, asteroid_type, asteroid_level, t,
                            direction, angular_velocity);
    if (r) {
        m_current_active_asteroid_count++;
        m_total_asteroid_spawn_count++;
    }
    return r;
}

Result<engine::gameplay::EntityId, void> AsteroidSpawnerSystem::spawn_from_impact(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    ModusWorld& modus_world,
    const glm::vec3& position,
    const u32 level) {
    auto& random_generator = modus_world.random_generator();
    const u32 asteroid_type = u32(random_generator.generate() % ModusAssets::kNumAsteroidTypes);

    const ModusGame& game = static_cast<ModusGame&>(engine.game());
    const auto asteroid_info_opt = game.assets().asteroid_info(asteroid_type, level);
    if (!asteroid_info_opt) {
        MODUS_LOGE("Failed to retrieve asteroid info for type={} level={}", asteroid_type, level);
        return Error<>();
    }

    // Spawn new asteroids slightly apart from each other so they don't collide
    // immediatley after being spawned
    const f32 offset_radius = (ModusAssets::kNumAsteroidLevels - level) * 10.f;

    // Random direction angle
    const f32 angle_1 = glm::radians(f32(random_generator.generate() % 360));
    const f32 angle_2 = glm::radians(f32(random_generator.generate() % 360));

    glm::vec3 direction_point;
    direction_point.x = offset_radius * glm::cos(angle_1) * glm::cos(angle_2);
    direction_point.y = offset_radius * glm::sin(angle_1) * glm::cos(angle_2);
    direction_point.z = offset_radius * glm::sin(angle_2);
    const glm::vec3 direction = glm::normalize(direction_point);

    // Random asteroid rotations
    const f32 angle = glm::radians(f32(random_generator.generate() % 360));
    const f32 x_axis = f32(random_generator.generate() % 360) / 360.0f;
    const f32 y_axis = f32(random_generator.generate() % 360) / 360.0f;
    const f32 z_axis = f32(random_generator.generate() % 360) / 360.0f;
    const glm::quat rotation = glm::angleAxis(angle, glm::vec3(x_axis, y_axis, z_axis));

    const glm::vec3 spawn_position = direction_point + position;
    math::Transform t;
    t.set_scale(1.f);
    t.set_translation(spawn_position);
    t.set_rotation(rotation);

    const f32 angular_speed = f32(random_generator.generate() % kAngularMagnitude);
    const glm::vec3 angular_velocity =
        glm::normalize(glm::vec3(x_axis, y_axis, z_axis)) * angular_speed;

    return spawn_asteroid(engine, entity_manager, modus_world, asteroid_type, level, t, direction,
                          angular_velocity);
}

static void on_asteroid_death(engine::Engine& engine, const engine::gameplay::EntityId id) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    ModusWorld& world = static_cast<ModusWorld&>(*gameplay_module->world());
    engine::gameplay::EntityManager& em = world.entity_manager();

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();

    auto r_transform = em.component<engine::gameplay::TransformComponent>(id);
    if (!r_transform) {
        return;
    }

    auto r_entity = em.create("AsteroidDeath");
    if (!r_entity) {
        MODUS_LOGE("OnAsteroidDeath: Failed to create entity");
        return;
    }

    if (auto r_lifetime = em.add_component<LifetimeComponent>(*r_entity); r_lifetime) {
        (*r_lifetime)->m_max_time_sec = FSeconds(5.f);
    } else {
        MODUS_LOGE("OnAsteroidDeath: Failed to create life time component");
        em.destroy(*r_entity);
        return;
    }

    auto r_audio_source_comp = em.add_component<engine::audio::AudioSourceComponent>(*r_entity);
    if (!r_audio_source_comp) {
        MODUS_LOGE("OnAsteroidDeath: Failed to create audio source component");
        em.destroy(*r_entity);
        return;
    }

    modus::audio::AudioSourceCreateParams source_params;
    source_params.gain = 0.8f;
    source_params.max_distance = 1000.f;
    source_params.looping = false;
    source_params.direction = glm::vec3(0.f);
    source_params.position = (*r_transform)->world_transform().translation();

    auto r_audio_source = audio_context.create_source(source_params);
    if (!r_audio_source) {
        MODUS_LOGE("OnAsteroidDeath: Failed to create audio source");
        em.destroy(*r_entity);
        return;
    }

    (*r_audio_source_comp)->audio_source = *r_audio_source;
    (*r_audio_source_comp)->directionless = true;

    const ModusAssets& assets = game.assets();
    const modus::audio::SoundHandle shatter_sounds[] = {
        assets.audio_assets().asteroid.shatter1,
        assets.audio_assets().asteroid.shatter2,
        assets.audio_assets().asteroid.shatter3,
        assets.audio_assets().asteroid.shatter4,
    };

    const u32 sound_index = world.random_generator().generate() % modus::array_size(shatter_sounds);
    if (!audio_context.play(*r_audio_source, shatter_sounds[sound_index])) {
        MODUS_LOGE("OnAsteroidDeath: Failed to play shatter sound");
        em.destroy(*r_entity);
    }
}

Result<engine::gameplay::EntityId, void> AsteroidSpawnerSystem::spawn_asteroid(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    ModusWorld& modus_word,
    const u32 type,
    const u32 level,
    const math::Transform& transform,
    const glm::vec3& direction,
    const glm::vec3& angular_velocity) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());
    auto r = game.assets().spawn_asteroid(engine, entity_manager, *modus_word.physics_world(), type,
                                          level, transform, direction, angular_velocity);
    if (r) {
        auto r_health = entity_manager.component<HealthComponent>(*r);
        if (!r_health) {
            entity_manager.destroy(*r);
            return Error<>();
        }
        (*r_health)->m_on_death_fn = on_asteroid_death;

        auto r_collision = entity_manager.add_component<engine::physics::CollisionComponent>(*r);
        if (!r_collision) {
            entity_manager.destroy(*r);
            return Error<>();
        }
    }
    return r;
}

}    // namespace modus::game
