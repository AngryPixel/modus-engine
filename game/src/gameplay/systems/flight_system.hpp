/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/event/event_types.hpp>
#include <gameplay/entity_manager.hpp>

namespace modus::engine::event {
class Event;
}

namespace modus::audio {
class AudioContext;
}
namespace modus::game {
class ModusGame;

class MODUS_ENGINE_EXPORT FlightSystem final : public engine::gameplay::System {
   private:
    engine::event::ListenerHandle m_on_entity_destroyed_listener;

   public:
    FlightSystem();

    Result<> initialize(engine::Engine&,
                        engine::gameplay::GameWorld&,
                        engine::gameplay::EntityManager& entity_manager) override;

    void shutdown(engine::Engine& engine,
                  engine::gameplay::GameWorld&,
                  engine::gameplay::EntityManager& entity_manager) override;

    void update(engine::Engine& engine,
                const IMilisec tick,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec tick,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void post_update(engine::Engine& engine,
                     engine::gameplay::GameWorld&,
                     engine::gameplay::EntityManager& entity_manager) override;

   private:
    Result<engine::gameplay::EntityId, void> create_audio_source(
        engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        const engine::gameplay::EntityId owner_id,
        modus::audio::AudioContext& audio_context);

    void on_entity_destroyed(engine::Engine&,
                             const engine::gameplay::EntityId id,
                             engine::gameplay::EntityManager& entity_manager);
};

}    // namespace modus::game
