/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gameplay/entity_manager.hpp>

namespace modus::engine::gameplay {
struct TransformComponent;
}

namespace modus::game {

class ScoreSystem final : public engine::gameplay::System {
   private:
    u64 m_total_score;
    IMilisec m_time_since_last_kill_ms;
    u32 m_combo_multiplier;
    engine::gameplay::EntityId m_player_id;

   public:
    ScoreSystem();

    void update(engine::Engine& engine,
                const IMilisec tick,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec tick,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void reset(const engine::gameplay::EntityId m_player_id);

    u64 total_score() const { return m_total_score; }

   private:
    using ViewType =
        engine::gameplay::EntityManagerView<const engine::gameplay::TransformComponent>;
    template <typename EntityManagerType>
    void do_system_update(EntityManagerType& entity_manager,
                          const IMilisec tick_ms,
                          ViewType& view);
};

}    // namespace modus::game
