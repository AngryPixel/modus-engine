/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/audio.hpp>
#include <engine/event/event_types.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <gameplay/entity_manager.hpp>
#include <math/transform.hpp>

namespace modus::physics {
class PhysicsWorld;
}

namespace modus::audio {
class AudioContext;
}

namespace modus::game {
class ModusGame;
struct CollisionInfo;
struct LaserGunComponent;

class LaserGunSystem final : public engine::gameplay::System {
   private:
    struct Collision {
        glm::vec3 contact_point;
        engine::gameplay::EntityId laser_id;
        engine::gameplay::EntityId other_id;
        u32 other_tag;
    };

    modus::audio::SoundHandle m_sound_handle;
    u64 m_shots_fired;
    u64 m_shots_hit;
    u64 m_shots_hit_self;
    engine::event::ListenerHandle m_on_entity_destroyed_listener;
    bool m_fire = false;

   public:
    LaserGunSystem();

    Result<> initialize(engine::Engine& engine,
                        engine::gameplay::GameWorld&,
                        engine::gameplay::EntityManager& entity_manager) override;

    void shutdown(engine::Engine& engine,
                  engine::gameplay::GameWorld&,
                  engine::gameplay::EntityManager& entity_manager) override;

    void update(engine::Engine& engine,
                const IMilisec tick,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec tick,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    Result<> spawn_laser_bolt_at(engine::Engine& engine,
                                 engine::gameplay::EntityManager& entity_manager,
                                 const math::Transform& transform);

    Result<> reset(engine::gameplay::EntityManager& entity_manager);

    f64 accuracy() const {
        if (m_shots_fired == 0) {
            return 0.0;
        }
        return f64(m_shots_hit) / f64(m_shots_fired) * 100.0;
    }

    u64 n_time_shot_self() const { return m_shots_hit_self; }

   private:
    Result<> spawn_laser_bolt(engine::Engine& engine,
                              engine::gameplay::EntityManager& entity_manager,
                              const engine::gameplay::EntityId ship_id,
                              LaserGunComponent& lgc,
                              modus::physics::PhysicsWorld& physics_world,
                              modus::audio::AudioContext& audio_context);

    Result<> create_audio_sources(engine::gameplay::EntityManager& entity_manager,
                                  const engine::gameplay::EntityId owner_id,
                                  LaserGunComponent& lgc,
                                  modus::audio::AudioContext& audio_context);

    void on_entity_destroyed(engine::Engine&,
                             const engine::gameplay::EntityId id,
                             engine::gameplay::EntityManager& entity_manager);

    void on_collision(const engine::gameplay::EntityManager& em, const CollisionInfo& collision);

    void spawn_collision_sound(engine::Engine&,
                               engine::gameplay::GameWorld& game_world,
                               engine::gameplay::EntityManager& em,
                               const glm::vec3& position);
};

}    // namespace modus::game
