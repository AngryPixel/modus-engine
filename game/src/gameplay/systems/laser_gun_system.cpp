/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gameplay/systems/laser_gun_system.hpp>
// clang-format on

#include <audio/audio_device.hpp>
#include <gameplay/modus_world.hpp>
#include <gameplay/components/laser_gun_component.hpp>
#include <gameplay/components/health_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <modus_game.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_physics.hpp>
#include <physics/world.hpp>
#include <physics/instance.hpp>
#include <physics/rigid_body.hpp>
#include <gameplay/components/lifetime_component.hpp>

#include <game/ecs/components/laser_gun_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::game::LaserGunComponent)

namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<modus::game::LaserGunComponent>;
Result<typename Traits::IntermediateType> Traits::load(Engine&, const StorageType& storage) {
    IntermediateType result;
    result.m_damage = storage.damage();
    result.m_fire_interval_sec = FSeconds(storage.fire_interval_sec());
    const auto* fbs_spawn_offset = storage.spawn_offset();
    if (fbs_spawn_offset != nullptr) {
        result.m_spawn_offset =
            glm::vec3(fbs_spawn_offset->x(), fbs_spawn_offset->y(), fbs_spawn_offset->z());
    } else {
        result.m_spawn_offset = glm::vec3(0.f);
    }
    result.m_elapsed_seconds_since_last_shot = FSeconds(0.0f);
    result.m_current_audio_source = 0;
    result.m_is_firing = false;
    return Ok(result);
}
}    // namespace modus::engine::gameplay
namespace modus::game {

LaserGunSystem::LaserGunSystem() : engine::gameplay::System("LaserGun") {}

Result<> LaserGunSystem::initialize(engine::Engine& engine,
                                    engine::gameplay::GameWorld&,
                                    engine::gameplay::EntityManager& entity_manager) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());

    m_sound_handle = game.assets().audio_assets().ship.laser1;
    if (!m_sound_handle) {
        MODUS_LOGE("LaserGunSystem: Laser sound has invalid handle");
        return Error<>();
    }

    engine::gameplay::EntityDestroyedDelegate callback;
    callback.bind<LaserGunSystem, &LaserGunSystem::on_entity_destroyed>(this);
    entity_manager.add_entity_destroyed_callback(callback);
    return Ok<>();
}

void LaserGunSystem::shutdown(engine::Engine& engine,
                              engine::gameplay::GameWorld&,
                              engine::gameplay::EntityManager& entity_manager) {
    MODUS_UNUSED(engine);
    engine::gameplay::EntityDestroyedDelegate callback;
    callback.bind<LaserGunSystem, &LaserGunSystem::on_entity_destroyed>(this);
    entity_manager.remove_entity_destroyed_callback(callback);
}

void LaserGunSystem::update(engine::Engine& engine,
                            const IMilisec tick,
                            engine::gameplay::GameWorld& game_world,
                            engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Laser Gun System Update");
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("LaserGunSystem: Physics world has not been set!");
        return;
    }

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();

    auto animation_module = engine.module<engine::ModuleAnimation>();

    // Spawn laser bolts
    engine::gameplay::EntityManagerViewMut<LaserGunComponent> view(entity_manager);
    view.for_each([this, tick_sec = FSeconds(tick), &engine, &audio_context, &physics_world,
                   &animation_module,
                   &entity_manager](const engine::gameplay::EntityId id, LaserGunComponent& lgc) {
        // create audio sources if entity has none
        if (lgc.m_audio_sources.is_empty()) {
            if (!this->create_audio_sources(entity_manager, id, lgc, audio_context)) {
                MODUS_LOGE("LaserGunSystem: Failed to create audio sources for {}", id);
                return;
            }
        }

        if (lgc.m_is_firing) {
            lgc.m_elapsed_seconds_since_last_shot += tick_sec;
            if (lgc.m_elapsed_seconds_since_last_shot >= lgc.m_fire_interval_sec) {
                lgc.m_elapsed_seconds_since_last_shot -= lgc.m_fire_interval_sec;
                if (!spawn_laser_bolt(engine, entity_manager, id, lgc, *physics_world,
                                      audio_context)) {
                    MODUS_LOGE("Failed to spawn laser bolt!");
                }
                if (auto r_gc = entity_manager.component<engine::graphics::GraphicsComponent>(id);
                    r_gc && (*r_gc)->animator) {
                    engine::animation::PlayAnimationParams play_params;
                    play_params.loop = false;
                    play_params.animation_name = "Shoot";
                    if (!animation_module->manager().play((*r_gc)->animator, play_params)) {
                        MODUS_LOGE("LaserGunSystem: Failed to play shoot animation");
                    }
                }
                m_shots_fired++;
            }
        }
    });

    {
        MODUS_PROFILE_GAME_BLOCK("Laser collision");
        // Laser Collision Handling
        engine::gameplay::EntityManagerView<const engine::physics::CollisionComponent>
            collision_view(entity_manager);
        collision_view.for_each_with_tag(
            kEntityTagLaser, [this, &game_world, &engine, &entity_manager](
                                 const engine::gameplay::EntityId id,
                                 const engine::physics::CollisionComponent& cc) {
                for (const auto& collision : cc.m_collisions) {
                    auto r_other_tag = entity_manager.entity_tag(collision.m_other_id);
                    if (!r_other_tag) {
                        return;
                    }

                    if (*r_other_tag != kEntityTagAsteroid) {
                        if (*r_other_tag == kEntityTagPlayer) {
                            m_shots_hit_self++;
                        }
                        return;
                    }
                    if (!cc.m_collisions.is_empty()) {
                        entity_manager.destroy(id);
                    }
                    if (auto r_health =
                            entity_manager.component<HealthComponent>(collision.m_other_id);
                        r_health) {
                        (*r_health)->m_health -= 25.0f;
                    }
                    spawn_collision_sound(engine, game_world, entity_manager,
                                          collision.m_info->contact_point);
                    m_shots_hit++;
                }
            });
    }
}

void LaserGunSystem::update_threaded(engine::ThreadSafeEngineAccessor&,
                                     const IMilisec,
                                     engine::gameplay::ThreadSafeGameWorldAccessor&,
                                     engine::gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("LaserGunSystem can't run threaded at the moment");
}

Result<> LaserGunSystem::spawn_laser_bolt_at(engine::Engine&,
                                             engine::gameplay::EntityManager&,
                                             const math::Transform&) {
    return Error<>();
}

Result<> LaserGunSystem::reset(engine::gameplay::EntityManager& entity_manager) {
    MODUS_UNUSED(entity_manager);
    m_shots_fired = 0;
    m_shots_hit = 0;
    m_shots_hit_self = 0;
    m_fire = false;
    return Ok<>();
}

Result<> LaserGunSystem::spawn_laser_bolt(engine::Engine& engine,
                                          engine::gameplay::EntityManager& entity_manager,
                                          const engine::gameplay::EntityId ship_id,
                                          LaserGunComponent& lgc,
                                          physics::PhysicsWorld& physics_world,
                                          modus::audio::AudioContext& audio_context) {
    math::Transform transform;
    glm::vec3 ship_velocity;
    auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();
    // Extract transform from ship
    if (auto r_physics = entity_manager.component<engine::physics::PhysicsComponent>(ship_id);
        !r_physics) {
        MODUS_LOGE("Failed to retreive physics component from ship!");
        return Error<>();
    } else {
        auto r_state = physics_instance.rigid_body_state((*r_physics)->m_handle);
        if (!r_state) {
            MODUS_LOGE("Failed to retreive rigid body state from ship");
            return Error<>();
        }
        transform = r_state->tranform;
        ship_velocity = r_state->linear_velocity;
    }

    const ModusGame& game = static_cast<const ModusGame&>(engine.game());
    const auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();

    auto r_asset = asset_loader.resolve_typed<engine::gameplay::EntityTemplateAsset>(
        game.assets().laser_template_asset());
    if (!r_asset) {
        MODUS_LOGE("Failed to get laser template asset");
        return Error<>();
    }

    auto r_entity = (*r_asset)->spawn(engine, entity_manager);
    if (!r_entity) {
        MODUS_LOGE("Failed to spawn laser entity");
        return Error<>();
    }
    entity_manager.update_entity_tag(*r_entity, kEntityTagLaser).expect();

    const glm::vec3 direction = transform.rotation() * glm::vec3(0.f, 0.f, 1.f);

    // Make sure bolt spawns in front of the ship!
    math::Transform spawn_transform;
    constexpr f32 spawn_offset_distance = 10.f;

    const glm::vec3 bolt_spawn_offset =
        transform.translation() + (direction * spawn_offset_distance);
    spawn_transform.set_translation(bolt_spawn_offset);
    spawn_transform.set_rotation(transform.rotation());
    spawn_transform.set_scale(1.0f);

    auto physics_component =
        entity_manager.component<engine::physics::PhysicsComponent>(*r_entity).value_or_panic();

    physics_instance.rigid_body_set_transform(physics_component->m_handle, spawn_transform)
        .expect();

    if (!physics_instance.rigid_body_set_linear_velocity(physics_component->m_handle,
                                                         (direction * 2500.f) + ship_velocity)) {
        MODUS_LOGE("LaserGunSystem: Failed to set linear velocity");
        return Error<>();
    }
    if (!physics_world.add_rigid_body(physics_component->m_handle)) {
        MODUS_LOGE("LaserGunSystem: Failed to add object to physics world");
        return Error<>();
    }
    /*
    auto r_asset =
        asset_loader.resolve_typed<engine::graphics::ModelAsset>(game.assets().laser_asset());
    if (!r_asset) {
        MODUS_LOGE("ModusWorld: Could not locate laser asset");
        return Error<>();
    }

    auto r_entity = entity_manager.create("Laser", kEntityTagLaser);
    if (!r_entity) {
        MODUS_LOGE("ModusWorld: Failed to create entity");
        return Error<>();
    }

    // Lifetime
    if (auto r_lifetime = entity_manager.add_component<LifetimeComponent>(*r_entity); !r_lifetime) {
        return Error<>();
    } else {
        constexpr f32 kMaxLaserBoltLifetime = 2.5f;
        (*r_lifetime)->m_max_time_sec = kMaxLaserBoltLifetime;
    }

    // Collision response
    if (auto r_collision =
            entity_manager.add_component<engine::physics::CollisionComponent>(*r_entity);
        !r_collision) {
        return Error<>();
    } else {
        (*r_collision)->m_response_callback =
            engine::physics::CollisionComponent::ResponseDelegate::build<
                LaserGunSystem, &LaserGunSystem::on_collision>(this);
    }

    // Add Transform Component
    if (!entity_manager.add_component<engine::gameplay::TransformComponent>(*r_entity)) {
        return Error<>();
    }

    // Add Graphics component
    if (auto r_graphics =
            entity_manager.add_component<engine::graphics::GraphicsComponent>(*r_entity);
        !r_graphics) {
        return Error<>();
    } else {
        auto graphics_module =
            engine.module<engine::ModuleGraphics>(engine.modules().graphics());
        const auto& mesh_db = graphics_module->mesh_db();
        auto r_mesh_instance = mesh_db.new_instance((*r_asset)->m_mesh_handle);
        if (!r_mesh_instance) {
            MODUS_LOGE("ModusWorld: Failed to create laser mesh instance");
            return Error<>();
        }
        if ((*r_asset)->m_material_assets.size() == 0) {
            (*r_graphics)->material_instance = graphics_module->material_db().default_instance();
        } else {
            auto material_asset = (*r_asset)->m_material_assets[0];
            auto r_material =
                asset_loader.resolve_typed<engine::graphics::MaterialAsset>(material_asset);
            if (!r_material) {
                MODUS_LOGE("ModusWorld: Could not locate material [{:02}]", 0);
            } else {
                if (!r_mesh_instance->materials.push_back((*r_material)->m_material_ref)) {
                    MODUS_LOGE("ModusWorld: Could not insert mateiral [{:02}]", 0);
                }
            }
        }
        (*r_graphics)->mesh_instance = *r_mesh_instance;
    }

    // Add Light component
    if (auto r_lc = entity_manager.add_component<engine::graphics::LightComponent>(*r_entity);
        r_lc) {
        auto& light = (*r_lc)->light;
        light.set_type(engine::graphics::LightType::Point);
        light.set_diffuse_color(glm::vec3(1.0f, 0.0f, 0.0f));
        light.set_diffuse_intensity(1.0f);
        light.set_diffuse_intensity(3.0f);
        light.set_point_light_radius(40.f);
        // light.set_attenuation(1.0f, .4f, 0.1f);
    } else {
        return Error<>();
    }

    // Add physics component
    if (auto r_physics_component =
            entity_manager.add_component<engine::physics::PhysicsComponent>(*r_entity);
        !r_physics_component) {
        return Error<>();
    } else {
        const glm::vec3 direction = transform.rotation() * glm::vec3(0.f, 0.f, 1.f);

        // Make sure bolt spawns in front of the ship!
        math::Transform spawn_transform;
        constexpr f32 spawn_offset_distance = 10.f;

        const glm::vec3 bolt_spawn_offset =
            transform.translation() + (direction * spawn_offset_distance);
        spawn_transform.set_translation(bolt_spawn_offset);
        spawn_transform.set_rotation(transform.rotation());

        modus::physics::RigidBodyCreateParams rigid_body_params;
        const auto& laser_info = game.assets().laser_info();
        rigid_body_params.shape_handle = laser_info.collision_shape_handle;
        rigid_body_params.mass = laser_info.mass;
        rigid_body_params.friction = 0.0f;
        rigid_body_params.transform = spawn_transform;
        void* user_ptr = reinterpret_cast<void*>(usize(r_entity->value()));
        rigid_body_params.user_data = user_ptr;

        auto r_physics_object = physics_instance.create_rigid_body(rigid_body_params);
        if (!r_physics_object) {
            MODUS_LOGE("LaserGunSystem: Failed to create physics object");
            return Error<>();
        }
        (*r_physics_component)->m_handle = *r_physics_object;

        if (!physics_instance.rigid_body_set_linear_velocity(
                *r_physics_object, (direction * 2500.f) + ship_velocity)) {
            MODUS_LOGE("LaserGunSystem: Failed to set linear velocity");
            return Error<>();
        }
        if (!physics_world.add_rigid_body((*r_physics_component)->m_handle)) {
            MODUS_LOGE("LaserGunSystem: Failed to add object to physics world");
            return Error<>();
        }
    }*/

    // Rewind a play fire sound
    if (auto r_audio_source_component =
            entity_manager.component<engine::audio::AudioSourceComponent>(
                lgc.m_audio_sources[lgc.m_current_audio_source]);
        !r_audio_source_component) {
        MODUS_LOGE("LaserGunSystem: Failed to get audio source component");
        return Error<>();
    } else {
        if (!audio_context.rewind_and_play((*r_audio_source_component)->audio_source)) {
            MODUS_LOGE("LaserGunSystem: Failed to play firing sound");
        }
        lgc.m_current_audio_source =
            (lgc.m_current_audio_source + 1) % LaserGunComponent::kMaxAudioSources;
    }

    return Ok<>();
}

Result<> LaserGunSystem::create_audio_sources(engine::gameplay::EntityManager& entity_manager,
                                              const engine::gameplay::EntityId owner_id,
                                              LaserGunComponent& lgc,
                                              audio::AudioContext& audio_context) {
    lgc.m_current_audio_source = 0;
    while (lgc.m_audio_sources.size() < LaserGunComponent::kMaxAudioSources) {
        engine::gameplay::EntityId entity_id;

        if (auto r_entity = entity_manager.create("ShipFireAudioSource"); !r_entity) {
            MODUS_LOGE("LaserGunSystem: Failed to create audio source entity");
            return Error<>();
        } else {
            entity_id = *r_entity;
            lgc.m_audio_sources.push_back(entity_id).expect(
                "Failed to push back audio source, should not happen");
        }

        if (auto r_transform =
                entity_manager.add_component<engine::gameplay::TransformComponent>(entity_id);
            !r_transform) {
            MODUS_LOGE("LaserGunSystem: Failed to create transform component");
            return Error<>();
        } else {
            if (!(*r_transform)->attach(entity_manager, owner_id)) {
                MODUS_LOGE("LaserGunSystem: Failed to attach entity {} to owner {}", entity_id,
                           owner_id);
            }
        }

        if (auto r_source =
                entity_manager.add_component<engine::audio::AudioSourceComponent>(entity_id);
            !r_source) {
            MODUS_LOGE("LaserGunSystem: Failed to create audio source component");
            return Error<>();
        } else {
            modus::audio::AudioSourceCreateParams source_params;
            source_params.gain = 0.1f;
            source_params.max_distance = 20.0f;
            source_params.looping = false;

            auto r_audio_source = audio_context.create_source(source_params);
            if (!r_audio_source) {
                MODUS_LOGE("FireSystem: Failed to create audio source");
                return Error<>();
            }
            (*r_source)->audio_source = *r_audio_source;
            (*r_source)->directionless = false;

            if (!audio_context.set_source_sound(*r_audio_source, m_sound_handle)) {
                MODUS_LOGE(
                    "LaserGunSystem: Failed to attach audio stream to audio "
                    "source");
                return Error<>();
            }
        }
    }

    return Ok<>();
}

void LaserGunSystem::on_entity_destroyed(engine::Engine&,
                                         const engine::gameplay::EntityId entity_id,
                                         engine::gameplay::EntityManager& entity_manager) {
    auto r_lgc = entity_manager.component<LaserGunComponent>(entity_id);
    if (r_lgc) {
        for (auto& id : (*r_lgc)->m_audio_sources) {
            entity_manager.destroy(id);
        }
    }
}

void LaserGunSystem::spawn_collision_sound(engine::Engine& engine,
                                           engine::gameplay::GameWorld&,
                                           engine::gameplay::EntityManager& em,
                                           const glm::vec3& position) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();
    auto r_entity = em.create("LaserCollision");
    if (!r_entity) {
        MODUS_LOGE("LaserGunSystem: Failed to create entity");
        return;
    }

    if (auto r_lifetime = em.add_component<LifetimeComponent>(*r_entity); r_lifetime) {
        (*r_lifetime)->m_max_time_sec = FSeconds(5.f);
    } else {
        MODUS_LOGE("LaserGunSystem: Failed to create life time component");
        em.destroy(*r_entity);
        return;
    }

    auto r_audio_source_comp = em.add_component<engine::audio::AudioSourceComponent>(*r_entity);
    if (!r_audio_source_comp) {
        MODUS_LOGE("LaserGunSystem: Failed to create audio source component");
        em.destroy(*r_entity);
        return;
    }

    modus::audio::AudioSourceCreateParams source_params;
    source_params.gain = 0.8f;
    source_params.max_distance = 800.f;
    source_params.looping = false;
    source_params.direction = glm::vec3(0.f);
    source_params.position = position;

    auto r_audio_source = audio_context.create_source(source_params);
    if (!r_audio_source) {
        MODUS_LOGE("LaserGunSystem: Failed to create audio source");
        em.destroy(*r_entity);
        return;
    }

    (*r_audio_source_comp)->audio_source = *r_audio_source;
    (*r_audio_source_comp)->directionless = true;

    const ModusAssets& assets = game.assets();
    if (!audio_context.play(*r_audio_source, assets.audio_assets().asteroid.impact)) {
        MODUS_LOGE("LaserGunSystem: Failed to play shatter sound");
        em.destroy(*r_entity);
    }
}

}    // namespace modus::game
