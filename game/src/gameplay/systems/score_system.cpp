/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <gameplay/modus_world.hpp>
#include <gameplay/systems/score_system.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>

namespace modus::game {

ScoreSystem::ScoreSystem()
    : engine::gameplay::System("Score"),
      m_total_score(0),
      m_time_since_last_kill_ms(0),
      m_combo_multiplier(1) {}

static constexpr u64 kAstroidKillBaseScore = 50;
static constexpr u64 kAstroidKillProxmimityScoreFactor = 1000;
static constexpr IMilisec kComboMultiplierResetMs = IMilisec(2000);

void ScoreSystem::update(engine::Engine&,
                         const IMilisec tick,
                         engine::gameplay::GameWorld&,
                         engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Player Score System Update");
    ViewType view(entity_manager);
    do_system_update(entity_manager, tick, view);
}

void ScoreSystem::update_threaded(
    engine::ThreadSafeEngineAccessor&,
    const IMilisec tick,
    engine::gameplay::ThreadSafeGameWorldAccessor&,
    engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Player Score System Update");
    ViewType view(entity_manager);
    do_system_update(entity_manager, tick, view);
}

void ScoreSystem::reset(const engine::gameplay::EntityId player_id) {
    m_total_score = 0;
    m_time_since_last_kill_ms = IMilisec(0);
    m_combo_multiplier = 1;
    m_player_id = player_id;
}

template <typename EntityManagerType>
void ScoreSystem::do_system_update(EntityManagerType& entity_manager,
                                   const IMilisec tick_ms,
                                   ScoreSystem::ViewType& view) {
    m_time_since_last_kill_ms += tick_ms;
    if (m_time_since_last_kill_ms >= kComboMultiplierResetMs) {
        m_combo_multiplier = 1;
    }
    view.for_each_with_tag(kEntityTagAsteroid, [this, &entity_manager](
                                                   const engine::gameplay::EntityId id,
                                                   const engine::gameplay::TransformComponent& tc) {
        if (entity_manager.is_alive(id)) {
            return;
        }

        auto r_player_tc =
            entity_manager.template component<engine::gameplay::TransformComponent>(m_player_id);
        if (!r_player_tc) {
            return;
        }

        const f32 dist = glm::distance((*r_player_tc)->world_transform().translation(),
                                       tc.world_transform().translation());

        const u64 score = kAstroidKillBaseScore +
                          (kAstroidKillBaseScore * u64(kAstroidKillProxmimityScoreFactor / dist));
        m_total_score += m_combo_multiplier * score;
        m_combo_multiplier++;
        m_time_since_last_kill_ms = IMilisec(0);
    });
}

}    // namespace modus::game
