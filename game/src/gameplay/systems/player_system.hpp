/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/physics/ecs/components/collision_component.hpp>
#include <gameplay/entity_manager.hpp>
namespace modus::game {

class PlayerSystem final : public engine::gameplay::System {
   private:
    IMilisec m_time_since_last_collision_ms;
    engine::gameplay::EntityId m_player_id;

   public:
    PlayerSystem();

    Result<> initialize(engine::Engine&,
                        engine::gameplay::GameWorld&,
                        engine::gameplay::EntityManager&) override;

    void shutdown(engine::Engine&,
                  engine::gameplay::GameWorld&,
                  engine::gameplay::EntityManager&) override;

    void update(engine::Engine& engine,
                const IMilisec tick,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec tick,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    Result<> reset(engine::Engine& engine,
                   engine::gameplay::GameWorld& game_world,
                   engine::gameplay::EntityManager& entity_manager);

    engine::gameplay::EntityId player_id() const { return m_player_id; }

   private:
    Result<engine::gameplay::EntityId, void> spawn_ship(
        engine::Engine& engine,
        engine::gameplay::GameWorld& game_world,
        engine::gameplay::EntityManager& entity_manager);
};

}    // namespace modus::game
