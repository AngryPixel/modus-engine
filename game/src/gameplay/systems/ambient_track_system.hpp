/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gameplay/entity_manager.hpp>

namespace modus::audio {
class AudioContext;
}

namespace modus::game {

class AmbientTrackSystem final : public engine::gameplay::System {
   private:
    modus::audio::SoundHandle m_ambient_track;
    modus::audio::AudioSourceHandle m_audio_source;

   public:
    AmbientTrackSystem();

    Result<> initialize(engine::Engine& engine,
                        engine::gameplay::GameWorld&,
                        engine::gameplay::EntityManager& entity_manager) override;

    void shutdown(engine::Engine& engine,
                  engine::gameplay::GameWorld&,
                  engine::gameplay::EntityManager& entity_manager) override;

    void update(engine::Engine& engine,
                const IMilisec,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void pause(engine::Engine&, engine::gameplay::GameWorld&);

    void resume(engine::Engine&, engine::gameplay::GameWorld&);
};

}    // namespace modus::game
