/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gameplay/systems/flight_system.hpp>
// clang-format on

#include <modus_game.hpp>
#include <audio/audio_device.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/modules/module_physics.hpp>
#include <gameplay/components/flight_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/modules/module_animation.hpp>
#include <physics/world.hpp>
#include <physics/instance.hpp>

#include <game/ecs/components/flight_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::game::FlightComponent)

namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<modus::game::FlightComponent>;
Result<typename Traits::IntermediateType> Traits::load(Engine&, const StorageType& storage) {
    IntermediateType result;
    result.m_angular_magnitude_pitch = storage.angular_magnitude_pitch();
    result.m_angular_magnitude_roll = storage.angular_magnitude_roll();
    result.m_angular_magnitude_yaw = storage.angular_magnitude_yaw();
    result.m_max_velocity = storage.max_velocity();
    result.m_max_velocity_up = storage.max_velocity_up();
    result.m_pitch_amount = 0.f;
    result.m_yaw_amount = 0.f;
    result.m_roll_amount = 0.f;
    result.m_thrust_amount = 0.f;
    result.m_vertical_thrust_amount = 0.f;
    result.m_dir_forward = glm::vec3(0.f, 0.f, 1.0f);
    result.m_dir_up = glm::vec3(0.f, 1.0f, 0.f);
    result.m_audio_source_entity = engine::gameplay::EntityId();
    return Ok(result);
}
}    // namespace modus::engine::gameplay

namespace modus::game {

static inline glm::vec3 acclerate_to(const glm::vec3& cur_velocity,
                                     const glm::vec3& target_velocity,
                                     const f32 max_accel,
                                     const f32 time_step_sec) {
    const glm::vec3 delta_v = target_velocity - cur_velocity;
    glm::vec3 accel = delta_v / time_step_sec;

    if (glm::length2(accel) > max_accel * max_accel) {
        accel = glm::normalize(accel) * max_accel;
    }
    return accel;
}

FlightSystem::FlightSystem() : engine::gameplay::System("Flight") {}

Result<> FlightSystem::initialize(engine::Engine&,
                                  engine::gameplay::GameWorld&,
                                  engine::gameplay::EntityManager& entity_manager) {
    // Register on entity destroyed event
    engine::gameplay::EntityDestroyedDelegate callback;
    callback.bind<FlightSystem, &FlightSystem::on_entity_destroyed>(this);
    entity_manager.add_entity_destroyed_callback(callback);
    return Ok<>();
}

void FlightSystem::shutdown(engine::Engine&,
                            engine::gameplay::GameWorld&,
                            engine::gameplay::EntityManager& entity_manager) {
    engine::gameplay::EntityDestroyedDelegate callback;
    callback.bind<FlightSystem, &FlightSystem::on_entity_destroyed>(this);
    entity_manager.remove_entity_destroyed_callback(callback);
}

void FlightSystem::update(engine::Engine& engine,
                          const IMilisec tick,
                          engine::gameplay::GameWorld& game_world,
                          engine::gameplay::EntityManager& entity_manager) {
    auto physics_module = engine.module<engine::ModulePhysics>();
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("FlightSystem: Physics world has not been set!");
        return;
    }
    auto& physics_instance = physics_module->instance();

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();

    auto animation_module = engine.module<engine::ModuleAnimation>();
    modus_assert(physics_world);

    MODUS_PROFILE_GAME_BLOCK("Update Flight components");
    engine::gameplay::EntityManagerViewMut<FlightComponent, const engine::physics::PhysicsComponent,
                                           const engine::graphics::GraphicsComponent>
        view(entity_manager);
    view.for_each([this, &engine, &audio_context, &animation_module, &physics_instance,
                   &entity_manager, tick_sec = FSeconds(tick)](
                      const engine::gameplay::EntityId id, FlightComponent& fc,
                      const engine::physics::PhysicsComponent& pc,
                      const engine::graphics::GraphicsComponent& gc) {
        if (!fc.m_audio_source_entity) {
            auto r_audio_source = create_audio_source(engine, entity_manager, id, audio_context);
            if (!r_audio_source) {
                MODUS_LOGE(
                    "FlightSystem: Failed to create audio source for "
                    "Entity {}",
                    id);
                return;
            }
            fc.m_audio_source_entity = *r_audio_source;
        }

        // Apply torque
        glm::vec3 torque = glm::vec3(0.f);
        torque.x = -fc.m_angular_magnitude_pitch * fc.m_pitch_amount;
        torque.y = -fc.m_angular_magnitude_yaw * fc.m_yaw_amount;
        torque.z = fc.m_angular_magnitude_roll * fc.m_roll_amount;
        if (!physics_instance.rigid_body_apply_relative_torque(pc.m_handle, torque)) {
            MODUS_LOGE("FlightSystem: Failed to apply torque to Entity {}", id);
        }

        // Apply Force
        auto r_rigid_body_state = physics_instance.rigid_body_state(pc.m_handle);
        if (!r_rigid_body_state) {
            MODUS_LOGE(
                "FlightSystem: Failed to get rigid body state for "
                "Entity {}",
                id);
            return;
        }

        glm::vec3 force = glm::vec3(0.f);
        // Vertical acceleration
        force += acclerate_to((*r_rigid_body_state).linear_velocity,
                              ((*r_rigid_body_state).tranform.rotation() * fc.m_dir_up) *
                                  fc.m_max_velocity_up,
                              1000.f, tick_sec.count()) *
                 fc.m_vertical_thrust_amount;

        // forward acceleration
        force += acclerate_to((*r_rigid_body_state).linear_velocity,
                              ((*r_rigid_body_state).tranform.rotation() * fc.m_dir_forward) *
                                  fc.m_max_velocity,
                              2000.f, tick_sec.count()) *
                 fc.m_thrust_amount;

        if (!physics_instance.rigid_body_apply_impulse(pc.m_handle, force, glm::vec3(0.f))) {
            MODUS_LOGE("FlightSystem: Failed to apply impluse to Entity {}", id);
        }

        // update audio source pitch based on engine thruster
        if (auto r_audio_source = entity_manager.component<engine::audio::AudioSourceComponent>(
                fc.m_audio_source_entity);
            r_audio_source) {
            const float pitch = std::min(1.5f, 1.0f + fc.m_thrust_amount);
            if (!audio_context.set_source_pitch((*r_audio_source)->audio_source, pitch)) {
                MODUS_LOGE("FlightSystem: Failed to update engine audio pitch");
            }
        } else {
            MODUS_LOGE(
                "FlightSystem: Failed to retreive audio source component "
                "for entity {} with attached etnity {}",
                id, fc.m_audio_source_entity);
        }

        // update engine rotation animation speed
        if (gc.animator) {
            if (!animation_module->manager().set_animation_speed(gc.animator, "EngineSpin",
                                                                 0.5f + fc.m_thrust_amount)) {
                MODUS_LOGE("FlightSystem: Failed to set animation speed for entity {}", id);
            }
        }
    });
}

void FlightSystem::update_threaded(engine::ThreadSafeEngineAccessor&,
                                   const IMilisec,
                                   engine::gameplay::ThreadSafeGameWorldAccessor&,
                                   engine::gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("Can't run flight system threaded");
}

void FlightSystem::post_update(engine::Engine&,
                               engine::gameplay::GameWorld&,
                               engine::gameplay::EntityManager& entity_manager) {
    engine::gameplay::EntityManagerViewMut<FlightComponent> view(entity_manager);
    view.for_each([](const engine::gameplay::EntityId, FlightComponent& fc) { fc.reset_state(); });
}

Result<engine::gameplay::EntityId, void> FlightSystem::create_audio_source(
    engine::Engine& engine,
    engine::gameplay::EntityManager& entity_manager,
    const engine::gameplay::EntityId owner_id,
    modus::audio::AudioContext& audio_context) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());

    auto r_entity = entity_manager.create("ShipEngine");
    if (!r_entity) {
        MODUS_LOGE("FlightSystem: Failed to create engine sound system");
        return Error<>();
    }
    const engine::gameplay::EntityId entity_id = *r_entity;

    auto r_owner_transform =
        entity_manager.component<engine::gameplay::TransformComponent>(owner_id);
    if (!r_owner_transform) {
        MODUS_LOGE(
            "FlightSystem: Owner entity {} does not have a "
            "transform component",
            owner_id);
        return Error<>();
    }

    if (auto r_transform =
            entity_manager.add_component<engine::gameplay::TransformComponent>(entity_id);
        !r_transform) {
        MODUS_LOGE("Flightystem: Failed to create transform component");
        return Error<>();
    } else {
        if (!(*r_transform)->attach(entity_manager, owner_id)) {
            MODUS_LOGE("FlightSystem: Failed to attach entity {} to owner {}", entity_id, owner_id);
        }
    }

    if (auto r_source =
            entity_manager.add_component<engine::audio::AudioSourceComponent>(entity_id);
        !r_source) {
        MODUS_LOGE("FlightSystem: Failed to create audio source component");
        return Error<>();
    } else {
        modus::audio::AudioSourceCreateParams source_params;
        source_params.gain = 1.0f;
        source_params.max_distance = 20.0f;
        source_params.looping = true;
        source_params.direction = glm::vec3(0);

        auto r_audio_source = audio_context.create_source(source_params);
        if (!r_audio_source) {
            return Error<>();
        }

        if (!audio_context.set_source_sound(*r_audio_source,
                                            game.assets().audio_assets().ship.engine)) {
            return Error<>();
        }

        audio_context.play(*r_audio_source).expect("Failed to play audio source");
        (*r_source)->audio_source = *r_audio_source;
        (*r_source)->directionless = true;
    }
    return Ok(entity_id);
}

void FlightSystem::on_entity_destroyed(engine::Engine&,
                                       const engine::gameplay::EntityId entity_id,
                                       engine::gameplay::EntityManager& entity_manager) {
    auto r_fc = entity_manager.component<FlightComponent>(entity_id);
    if (r_fc) {
        entity_manager.destroy((*r_fc)->m_audio_source_entity);
    }
}

}    // namespace modus::game
