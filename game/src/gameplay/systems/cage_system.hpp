/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gameplay/entity_manager.hpp>
#include <math/bounding_volumes.hpp>

namespace modus::physics {
class PhysicsWorld;
}

namespace modus::game {

class CageSystem final : public engine::gameplay::System {
   private:
    math::bv::AABB m_cage_aabb;
    f32 m_min_cage_radius;
    f32 m_cage_shrink_rate;
    FSeconds m_cage_shrink_tick_sec;
    FSeconds m_sec_since_last_shrink;
    engine::gameplay::EntityId m_cage_entity;
    bool m_enable_shrink;

   public:
    CageSystem();

    const glm::vec3& cage_radius() const { return m_cage_aabb.extends; }

    Result<> initialize(engine::Engine& engine,
                        engine::gameplay::GameWorld&,
                        engine::gameplay::EntityManager& type) override;

    void update(engine::Engine& engine,
                const IMilisec tick,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec tick,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void reset(const glm::vec3& cage_radius);
};

}    // namespace modus::game
