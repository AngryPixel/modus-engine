/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gameplay/systems/lifetime_system.hpp>
// clang-format on

#include <gameplay/components/lifetime_component.hpp>
#include <game/ecs/components/lifetime_component_generated.h>
#include <engine/gameplay/gameworld.hpp>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::game::LifetimeComponent)

namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<modus::game::LifetimeComponent>;
Result<typename Traits::IntermediateType> Traits::load(Engine&, const StorageType& storage) {
    IntermediateType result;
    result.m_elapsed_time_sec = FSeconds(0.f);
    result.m_max_time_sec = FSeconds(storage.duration_sec());
    return Ok(result);
}
}    // namespace modus::engine::gameplay

namespace modus::game {

template <typename EM>
static inline void do_system_update(EM& entity_manager, const FSeconds fixed_tick_sec) {
    engine::gameplay::EntityManagerViewMut<LifetimeComponent> view(entity_manager);
    view.for_each([&entity_manager, fixed_tick_sec](const engine::gameplay::EntityId id,
                                                    LifetimeComponent& lc) {
        lc.m_elapsed_time_sec += fixed_tick_sec;
        if (lc.m_elapsed_time_sec >= lc.m_max_time_sec) {
            entity_manager.destroy(id);
        }
    });
}

LifetimeSystem::LifetimeSystem() : engine::gameplay::System("Lifetime") {}
void LifetimeSystem::update(engine::Engine&,
                            const IMilisec tick,
                            engine::gameplay::GameWorld&,
                            engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Update Lifetime components");
    do_system_update(entity_manager, FSeconds(tick));
}
void LifetimeSystem::update_threaded(
    engine::ThreadSafeEngineAccessor&,
    const IMilisec tick,
    engine::gameplay::ThreadSafeGameWorldAccessor&,
    engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Update Lifetime components");
    do_system_update(entity_manager, FSeconds(tick));
}
}    // namespace modus::game
