/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <audio/audio_device.hpp>
#include <gameplay/systems/player_system.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <modus_game.hpp>
#include <gameplay/modus_world.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/audio/ecs/components/audio_components.hpp>
#include <gameplay/components/health_component.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <physics/world.hpp>
#include <engine/modules/module_physics.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <gameplay/components/lifetime_component.hpp>
#include <physics/instance.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>

namespace modus::game {

static constexpr IMilisec kCollisionResetTimeMs = IMilisec(500);

PlayerSystem::PlayerSystem()
    : engine::gameplay::System("Player"), m_time_since_last_collision_ms(kCollisionResetTimeMs) {}

Result<> PlayerSystem::initialize(engine::Engine& engine,
                                  engine::gameplay::GameWorld&,
                                  engine::gameplay::EntityManager&) {
    auto audio_module = engine.module<engine::ModuleAudio>();
    if (!audio_module) {
        MODUS_LOGE("PlayerSystem: Audio module {} is not in module list",
                   engine::module_guid<engine::ModuleAudio>());
        return Error<>();
    }
    return Ok<>();
}

void PlayerSystem::shutdown(engine::Engine&,
                            engine::gameplay::GameWorld&,
                            engine::gameplay::EntityManager&) {}

void PlayerSystem::update(engine::Engine& engine,
                          const IMilisec tick,
                          engine::gameplay::GameWorld& game_world,
                          engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Player Collision");
    m_time_since_last_collision_ms += tick;

    auto physics_module = engine.module<engine::ModulePhysics>();
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("CageSystem: No physics world provided!");
        return;
    }
    auto& physics_instance = physics_module->instance();
    auto r_physics = entity_manager.component<engine::physics::PhysicsComponent>(m_player_id);
    if (m_time_since_last_collision_ms > kCollisionResetTimeMs) {
        if (r_physics && !physics_instance.set_rigid_body_angular_factor((*r_physics)->m_handle,
                                                                         glm::vec3(1.0))) {
            MODUS_LOGE("Failed to set angular factor on player");
        }
    }

    // Check asteroid collisions
    if (auto r_collision_component =
            entity_manager.component<engine::physics::CollisionComponent>(m_player_id);
        r_collision_component) {
        for (const auto& collision : (*r_collision_component)->m_collisions) {
            if (m_time_since_last_collision_ms > kCollisionResetTimeMs) {
                if (!physics_instance.set_rigid_body_angular_factor((*r_physics)->m_handle,
                                                                    glm::vec3(0.1f))) {
                    MODUS_LOGE("Failed to set angular factor on player");
                }
                m_time_since_last_collision_ms = IMilisec(0);
                if (auto r_tag = entity_manager.entity_tag(collision.m_other_id);
                    !r_tag || (*r_tag) != kEntityTagAsteroid) {
                    continue;
                }
                if (auto r_health = entity_manager.component<HealthComponent>(m_player_id);
                    r_health) {
                    (*r_health)->m_health -= 26.0f;
                }
            }
        }
    }
}

void PlayerSystem::update_threaded(
    engine::ThreadSafeEngineAccessor&,
    const IMilisec tick,
    engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
    engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Player Collision");
    m_time_since_last_collision_ms += tick;

    // Check asteroid collisions
    if (auto r_collision_component =
            entity_manager.component<engine::physics::CollisionComponent>(m_player_id);
        r_collision_component) {
        for (const auto& collision : (*r_collision_component)->m_collisions) {
            if (auto r_tag = entity_manager.entity_tag(collision.m_other_id);
                !r_tag || (*r_tag) != kEntityTagAsteroid) {
                return;
            }
            if (m_time_since_last_collision_ms > kCollisionResetTimeMs) {
                game_world.push_command(
                    [player_id = m_player_id](engine::Engine&, engine::gameplay::GameWorld& world) {
                        if (auto r_health =
                                world.entity_manager().component<HealthComponent>(player_id);
                            r_health) {
                            (*r_health)->m_health -= 26.0f;
                        }
                    });
                m_time_since_last_collision_ms = IMilisec(0);
            }
        }
    }
}

Result<> PlayerSystem::reset(engine::Engine& engine,
                             engine::gameplay::GameWorld& game_world,
                             engine::gameplay::EntityManager& entity_manager) {
    m_player_id = engine::gameplay::EntityId();
    m_time_since_last_collision_ms = kCollisionResetTimeMs;
    auto r_spawn = spawn_ship(engine, game_world, entity_manager);
    if (!r_spawn) {
        return Error<>();
    }
    m_player_id = *r_spawn;
    return Ok<>();
}

static void on_player_death(engine::Engine& engine, const engine::gameplay::EntityId id) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    ModusWorld& world = static_cast<ModusWorld&>(*gameplay_module->world());
    engine::gameplay::EntityManager& em = world.entity_manager();

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();

    auto r_transform = em.component<engine::gameplay::TransformComponent>(id);
    if (!r_transform) {
        return;
    }

    auto r_entity = em.create("AsteroidDeath");
    if (!r_entity) {
        MODUS_LOGE("OnPlayerDeath: Failed to create entity");
        return;
    }

    if (auto r_lifetime = em.add_component<LifetimeComponent>(*r_entity); r_lifetime) {
        (*r_lifetime)->m_max_time_sec = FSeconds(5.f);
    } else {
        MODUS_LOGE("OnPlayerDeath: Failed to create life time component");
        em.destroy(*r_entity);
        return;
    }

    auto r_audio_source_comp = em.add_component<engine::audio::AudioSourceComponent>(*r_entity);
    if (!r_audio_source_comp) {
        MODUS_LOGE("OnPlayerDeath: Failed to create audio source component");
        em.destroy(*r_entity);
        return;
    }

    modus::audio::AudioSourceCreateParams source_params;
    source_params.gain = 0.8f;
    source_params.max_distance = 100.f;
    source_params.looping = false;
    source_params.direction = glm::vec3(0.f);
    source_params.position = (*r_transform)->world_transform().translation();

    auto r_audio_source = audio_context.create_source(source_params);
    if (!r_audio_source) {
        MODUS_LOGE("OnPlayerDeath: Failed to create audio source");
        em.destroy(*r_entity);
        return;
    }

    (*r_audio_source_comp)->audio_source = *r_audio_source;
    (*r_audio_source_comp)->directionless = true;

    const ModusAssets& assets = game.assets();
    if (!audio_context.play(*r_audio_source, assets.audio_assets().ship.death)) {
        MODUS_LOGE("OnPlayerDeath: Failed to play death sound");
        em.destroy(*r_entity);
    }
}

Result<engine::gameplay::EntityId, void> PlayerSystem::spawn_ship(
    engine::Engine& engine,
    engine::gameplay::GameWorld& game_world,
    engine::gameplay::EntityManager& entity_manager) {
    const ModusGame& game = static_cast<ModusGame&>(engine.game());

    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("PlayerSystem: Physics world has not been set!");
        return Error<>();
    }

    auto r_spawn = game.assets().spawn_ship(engine, entity_manager, *physics_world, glm::vec3(0.f));
    if (!r_spawn) {
        return Error<>();
    }

    auto r_health = entity_manager.component<HealthComponent>(*r_spawn);
    if (!r_health) {
        entity_manager.destroy(*r_spawn);
        return Error<>();
    }
    (*r_health)->m_on_death_fn = on_player_death;

    // Collision response
    if (auto r_collision =
            entity_manager.add_component<engine::physics::CollisionComponent>(*r_spawn);
        !r_collision) {
        entity_manager.destroy(*r_spawn);
        return Error<>();
    }
    return r_spawn;
}

}    // namespace modus::game
