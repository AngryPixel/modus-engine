/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_physics.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>
#include <gameplay/modus_world.hpp>
#include <gameplay/systems/cage_system.hpp>
#include <modus_game.hpp>
#include <physics/debug_drawer.hpp>
#include <physics/instance.hpp>
#include <physics/rigid_body.hpp>
#include <physics/world.hpp>

namespace modus::game {

CageSystem::CageSystem()
    : engine::gameplay::System("Cage"),
      m_cage_aabb(),
      m_min_cage_radius(500.0f),
      m_cage_shrink_rate(0.75f),
      m_cage_shrink_tick_sec(60.0f),
      m_sec_since_last_shrink(0.f),
      m_enable_shrink(true) {
    m_cage_aabb.center = glm::vec3(0.f);
    m_cage_aabb.extends = glm::vec3(1000.0f);
}

Result<> CageSystem::initialize(engine::Engine& engine,
                                engine::gameplay::GameWorld& game_world,
                                engine::gameplay::EntityManager& entity_manager) {
    const f32 cage_radius = 3000.0f;
    m_cage_aabb.extends = glm::vec3(cage_radius);
    const ModusGame& game = (static_cast<ModusGame&>(engine.game()));
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("CageSystem: Physics World has not been set!");
        return Error<>();
    }

    if (auto r = game.assets().spawn_cage(engine, entity_manager, *physics_world, cage_radius);
        !r) {
        MODUS_LOGE("CageSystem: Failed to spawn cage");
        return Error<>();
    } else {
        m_cage_entity = *r;
    }
    return Ok<>();
}

void CageSystem::update(engine::Engine& engine,
                        const IMilisec tick,
                        engine::gameplay::GameWorld& game_world,
                        engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Cage System Update");
    auto physics_module = engine.module<engine::ModulePhysics>();
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("CageSystem: No physics world provided!");
        return;
    }
    // auto& physics_instance = physics_module->instance();

    MODUS_UNUSED(entity_manager);
    MODUS_UNUSED(engine);
    MODUS_UNUSED(physics_module);
    // Void shrink cage over time
    if (m_enable_shrink && m_cage_aabb.extends.x > m_min_cage_radius) {
        m_sec_since_last_shrink += FSeconds(tick);
        if (m_sec_since_last_shrink >= m_cage_shrink_tick_sec) {
            m_sec_since_last_shrink -= m_cage_shrink_tick_sec;
            m_cage_aabb.extends *= m_cage_shrink_rate;
        }
        /*
                if (auto r_tc =
                        entity_manager.component<engine::gameplay::TransformComponent>(m_cage_entity);
                    r_tc) {
                    (*r_tc)->set_scale(m_cage_aabb.extends.x);
                } else {
                    MODUS_LOGE(
                        "CageSystem: Could not find cage entity's transform "
                        "component");
                }*/
    }
    /*
        auto bounds_fn = [&entity_manager, &physics_instance, this](
                             engine::gameplay::EntityId entity,
                             const engine::gameplay::TransformComponent& tc,
                             const engine::physics::PhysicsComponent& pc) -> void {
            auto tag =
                entity_manager.entity_tag(entity).value_or_panic("Failed to retrieve entity tag");

            if (tag == kEntityTagPlayer) {
                // Player impulse
                const glm::vec3 position = tc.world_transform().translation();
                const glm::vec3 impulse_dir = -glm::normalize(position);
                const glm::vec3 position_abs = glm::abs(position);
                const glm::vec3 distance = -(position_abs - m_cage_aabb.extends);
                static constexpr f32 kImpulseStrength = 10000;
                static constexpr f32 kMinDistance = 100;
                const glm::vec3 distance_norm = distance / kMinDistance;

                bool apply_impulse = false;
                glm::vec3 impulse = glm::vec3(0.f);
                if (distance_norm.x <= 1.0f) {
                    impulse.x =
                        impulse_dir.x * kImpulseStrength * (1.0f - std::min(0.5f, distance_norm.x));
                    apply_impulse = true;
                }

                if (distance_norm.y <= 1.0f) {
                    impulse.y =
                        impulse_dir.y * kImpulseStrength * (1.0f - std::min(.5f, distance_norm.y));
                    apply_impulse = true;
                }

                if (distance_norm.z <= 1.0f) {
                    impulse.z =
                        impulse_dir.z * kImpulseStrength * (1.0f - std::min(.5f, distance_norm.z));
                    apply_impulse = true;
                }

                if (apply_impulse) {
                    if (!physics_instance.rigid_body_apply_impulse(pc.m_handle, impulse,
                                                                   glm::vec3(0))) {
                        MODUS_LOGE("CageSystem: Failed to add impulse to ship");
                    }
                }

            } else {
                // Asteroids and/or lasers
                auto r_aabb = physics_instance.rigid_body_aabb(pc.m_handle);
                if (!r_aabb) {
                    return;
                }

                if (math::bv::intersects(m_cage_aabb, *r_aabb)) {
                    return;
                }

                const glm::vec3 abs_pos = glm::abs(tc.world_transform().translation());
                math::bv::Ray ray;
                const f32 distance = glm::abs(glm::distance(abs_pos, glm::vec3(0.))) -
                                     (glm::length(r_aabb->extends) * 0.5f);
                ray.origin = tc.world_transform().translation();
                ray.direction = glm::normalize(glm::vec3(0.0f) - ray.origin);
                const glm::vec3 new_position = ray.point_at(distance * 2.0f);
                math::Transform t;
                t.set_scale(1.0f);
                t.set_translation(new_position);
                t.set_rotation(tc.world_transform().rotation());
                if (!physics_instance.rigid_body_set_transform(pc.m_handle, t)) {
                    MODUS_LOGE("Failed to set relocate entity {} to new position", entity.value());
                }
            }
        };

        engine::gameplay::EntityManagerView<const engine::gameplay::TransformComponent,
                                            const engine::physics::PhysicsComponent>
            view(entity_manager);
        view.for_each(bounds_fn);*/
}

void CageSystem::update_threaded(engine::ThreadSafeEngineAccessor&,
                                 const IMilisec,
                                 engine::gameplay::ThreadSafeGameWorldAccessor&,
                                 engine::gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("Cage System can't currently be run threaded");
}

void CageSystem::reset(const glm::vec3& cage_radius) {
    m_cage_aabb.extends = cage_radius;
    m_sec_since_last_shrink = FSeconds(0.f);
}

}    // namespace modus::game
