/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/physics/ecs/components/collision_component.hpp>
#include <gameplay/entity_manager.hpp>

namespace modus::physics {
class PhysicsWorld;
}

namespace modus::game {
class ModusWorld;
class CageSystem;
class AsteroidSpawnerSystem final : public engine::gameplay::System {
   private:
    u32 m_max_active_asteroid_count;
    u32 m_current_active_asteroid_count;
    u32 m_max_total_asteroid_spawn_count;
    u32 m_total_asteroid_spawn_count;
    FSeconds m_spawn_interval_sec;
    FSeconds m_elapsed_spawn_interval_sec;
    const CageSystem& m_cage_system;

   public:
    AsteroidSpawnerSystem(const CageSystem& cage_system);

    void update(engine::Engine& engine,
                const IMilisec,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void debug_info(engine::Engine& engine, const engine::gameplay::EntityManager& entity_manager);

    void reset();

   private:
    Result<engine::gameplay::EntityId, void> spawn_asteroid_random(
        engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        ModusWorld& modus_world);

    Result<engine::gameplay::EntityId, void> spawn_from_impact(
        engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        ModusWorld& modus_world,
        const glm::vec3& position,
        const u32 level);

    Result<engine::gameplay::EntityId, void> spawn_asteroid(
        modus::engine::Engine& engine,
        engine::gameplay::EntityManager& entity_manager,
        ModusWorld& modus_word,
        const u32 type,
        const u32 level,
        const math::Transform& transform,
        const glm::vec3& direction,
        const glm::vec3& angular_velocity);
};

}    // namespace modus::game
