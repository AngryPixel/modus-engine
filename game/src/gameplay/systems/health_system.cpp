/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gameplay/systems/health_system.hpp>
// clang-format on

#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <gameplay/components/health_component.hpp>
#include <game/ecs/components/health_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::game::HealthComponent)

namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<modus::game::HealthComponent>;
Result<typename Traits::IntermediateType> Traits::load(Engine&, const StorageType& storage) {
    IntermediateType result;
    result.m_health = storage.health();
    return Ok(result);
}
}    // namespace modus::engine::gameplay

namespace modus::game {

HealthSystem::HealthSystem() : engine::gameplay::System("Health") {}

void HealthSystem::update(engine::Engine& engine,
                          const IMilisec,
                          engine::gameplay::GameWorld&,
                          engine::gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Update Health components");
    engine::gameplay::EntityManagerViewMut<HealthComponent> view(entity_manager);
    view.for_each(
        [&entity_manager, &engine](const engine::gameplay::EntityId id, HealthComponent& hc) {
            if (hc.m_health <= 0.0f) {
                entity_manager.destroy(id);
                if (hc.m_on_death_fn != nullptr) {
                    hc.m_on_death_fn(engine, id);
                }
            }
        });
}

void HealthSystem::update_threaded(
    engine::ThreadSafeEngineAccessor&,
    const IMilisec,
    engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
    engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GAME_BLOCK("Update Health components");
    engine::gameplay::EntityManagerViewMut<HealthComponent> view(entity_manager);
    view.for_each([&entity_manager, &game_world](const engine::gameplay::EntityId id,
                                                 HealthComponent& hc) {
        if (hc.m_health <= 0.0f) {
            entity_manager.destroy(id);
            game_world.push_command([id](engine::Engine& eg, engine::gameplay::GameWorld& world) {
                auto& em = world.entity_manager();
                auto r_hc = em.component<HealthComponent>(id);
                if (r_hc && (*r_hc)->m_on_death_fn != nullptr) {
                    (*r_hc)->m_on_death_fn(eg, id);
                }
            });
        }
    });
}

}    // namespace modus::game
