/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/gameworld.hpp>
#include <ui/ui.hpp>

namespace modus::game {

class MainUIView;

class MainUIWorld final : public engine::gameplay::GameWorld {
   private:
    modus::ui::UIRefPtr<MainUIView> m_ui_view;
    bool m_ui_created;

   public:
    MainUIWorld();

    ~MainUIWorld();

    Result<> initialize(engine::Engine&) override;

    Result<> shutdown(engine::Engine&) override;

    void on_enter(engine::Engine&) override;

    void on_exit(engine::Engine&) override;

    void tick(engine::Engine& engine, const IMilisec tick) override;

    void tick_fixed(engine::Engine& engine, const IMilisec tick) override;

    engine::gameplay::EntityId main_camera() const override;
};

}    // namespace modus::game
