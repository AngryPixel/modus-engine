/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <audio/audio_device.hpp>
#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/gameplay/ecs/components/transform_animation_component.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/components/velocity_component.hpp>
#include <engine/gameplay/fsm/fsm.hpp>
#include <engine/graphics/debug_drawers/physics_debug_drawer.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <engine/modules/module_physics.hpp>
#include <engine/modules/module_ui.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>
#include <gameplay/components/asteroid_info_component.hpp>
#include <gameplay/components/flight_component.hpp>
#include <gameplay/components/health_component.hpp>
#include <gameplay/components/laser_gun_component.hpp>
#include <gameplay/components/lifetime_component.hpp>
#include <gameplay/modus_world.hpp>
#include <modus_game.hpp>
#include <physics/world.hpp>

#include <ui/context.hpp>
#include <ui/game_over_view.hpp>
#include <ui/in_game_view.hpp>

namespace modus::game {

enum class ModusGameStates : u32 {
    Playing,
    PlayerDead,
};

class GameStatePlaying;
class GameStateDead final : public engine::gameplay::FSMState {
   private:
    modus::ui::UIRefPtr<GameOverView> m_ui_view;
    const ModusWorld& m_world;
    bool m_should_exit;

   public:
    GameStateDead(const ModusWorld& world) : m_world(world), m_should_exit(false) {}

    void on_enter(engine::Engine& engine) override {
        if (!m_ui_view) {
            auto ui_module = engine.module<engine::ModuleUI>();
            auto& ui_context = ui_module->context();
            m_ui_view = ui_context.create_object<GameOverView>();
            m_ui_view->create_ui();
        }
        m_should_exit = false;
        MODUS_LOGD("State Dead Enter");
        m_ui_view->set_score_text(modus::format("{}", m_world.score_system().total_score()));
        m_ui_view->set_accuracy_text(modus::format("{:02.2f}", m_world.fire_system().accuracy()));
        auto ui_module = engine.module<engine::ModuleUI>();
        ui_module->context().view_controller().push_view(m_ui_view);
        ui_module->show(engine, true);
    }

    Optional<u32> update(engine::Engine& engine) override {
        if (!m_ui_view->exit_value()) {
            return Optional<u32>();
        }
        m_should_exit = m_ui_view->exit_value().value_or(false);
        if (m_should_exit) {
            return u32(ModusGameStates::Playing);
        } else {
            auto ui_module = engine.module<engine::ModuleUI>();
            ui_module->context().view_controller().pop_view();
            ui_module->hide(engine);
            engine.quit();
        }
        return Optional<u32>();
    }

    void on_exit(engine::Engine& engine) override {
        MODUS_LOGD("State Dead Exit");
        auto ui_module = engine.module<engine::ModuleUI>();
        ui_module->context().view_controller().pop_view();
        ui_module->hide(engine);
    }
};

class GameStatePlaying final : public engine::gameplay::FSMState {
   private:
    ModusWorld& m_world;
    ui::UIRefPtr<InGameView> m_ui_view;

   public:
    GameStatePlaying(ModusWorld& world) : m_world(world) {}

    void on_enter(engine::Engine& engine) override {
        MODUS_LOGD("State Playing Enter");
        m_world.reset(engine).expect("Failed to reset world");
        auto ui_module = engine.module<engine::ModuleUI>();
        auto& ui_context = ui_module->context();
        if (!m_ui_view) {
            m_ui_view = ui_context.create_object<InGameView>();
        }
        m_ui_view->set_world(&m_world);
        ui_context.view_controller().push_view(m_ui_view);
        ui_module->show(engine, false);
    }

    Optional<u32> update(engine::Engine&) override {
        const auto player_id = m_world.player_entity_id();
        if (m_world.entity_manager().is_alive(player_id)) {
            return Optional<u32>();
        }
        return u32(ModusGameStates::PlayerDead);
    }

    void on_exit(engine::Engine& engine) override {
        MODUS_LOGD("State Playing Exit");
        auto ui_module = engine.module<engine::ModuleUI>();
        auto& ui_context = ui_module->context();
        if (m_ui_view) {
            m_ui_view->set_world(NotMyPtr<ModusWorld>());
        }
        ui_context.view_controller().pop_view();
        ui_module->hide(engine);
    }
};

class ModusGameFSM final : public engine::gameplay::FSM {
   private:
    GameStatePlaying m_state_playing;
    GameStateDead m_state_dead;

   public:
    ModusGameFSM(ModusWorld& world) : m_state_playing(world), m_state_dead(world) {
        set_state(u32(ModusGameStates::Playing), &m_state_playing);
        set_state(u32(ModusGameStates::PlayerDead), &m_state_dead);
    }

    Result<> initialize(engine::Engine&) override {
        this->set_initial_state(u32(ModusGameStates::Playing));
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        reset(engine);
        return Ok<>();
    }
};

Result<> ModusWorld::register_components(engine::gameplay::EntityManager& em) {
#define REGISTER(C)                                        \
    if (!em.register_component<C>()) {                     \
        MODUS_LOGE("Failed to register component " #C ""); \
        return Error<>();                                  \
    }

    REGISTER(engine::gameplay::TransformComponent)
    REGISTER(engine::gameplay::VelocityComponent)
    REGISTER(engine::gameplay::TransformAnimationComponent)
    REGISTER(engine::graphics::GraphicsComponent)
    REGISTER(engine::graphics::LightComponent)
    REGISTER(engine::graphics::CameraComponent)
    REGISTER(engine::physics::PhysicsComponent)
    REGISTER(engine::physics::CollisionComponent);
    REGISTER(engine::audio::AudioSourceComponent)
    REGISTER(engine::audio::AudioListenerComponent)
    REGISTER(LifetimeComponent)
    REGISTER(HealthComponent)
    REGISTER(AsteroidInfoComponent)
    REGISTER(FlightComponent)
    REGISTER(LaserGunComponent)
#undef REGISTER
    return Ok<>();
}
ModusWorld::ModusWorld()
    : m_cage_system(),
      m_spawner_system(m_cage_system),
      m_audio_system(true),
      m_player_input_context(*this) {
    m_debug_opt.draw_sphere_bounds = true;
}

ModusWorld::~ModusWorld() {}

Result<> ModusWorld::initialize(engine::Engine& engine) {
    MODUS_PROFILE_GAME_BLOCK("ModusWorld Initialize");

    m_random_generator.init(257);

    if (!initialize_command_queue(1024 * 1024)) {
        MODUS_LOGE("Failed to initialize command queue");
        return Error<>();
    }

    if (!register_components(m_entity_manager)) {
        return Error<>();
    }

    if (!m_entity_manager.initialize(engine, 1024)) {
        MODUS_LOGE("Failed to initialize entity manager");
        return Error<>();
    }

    // setup physics
    auto physics_module = engine.module<engine::ModulePhysics>();
    if (!physics_module) {
        MODUS_LOGE("Physics module {} is not in module list",
                   engine::module_guid<engine::ModulePhysics>());
        return Error<>();
    }
    modus::physics::WorldCreateParams world_params;
    world_params.gravity = glm::vec3(0.0f);

    m_physics_world = physics_module->create_world();
    if (!m_physics_world || !m_physics_world->initialize(world_params)) {
        MODUS_LOGE("Failed to initialize physics world ");
        return Error<>();
    }

    set_storage<modus::physics::PhysicsWorld>(m_physics_world.get());

    ModusGame& game = static_cast<ModusGame&>(engine.game());
    if (!game.assets().load_sounds(engine)) {
        return Error<>();
    }

#if !defined(MODUS_WORLD_USE_THREADED_SYSTEM_RUNNER)
    const GUID transform_system_dep[] = {m_transform_system.guid()};
    const GUID physics_system_dep[] = {m_physics_system.guid()};
    const GUID flight_system_dep[] = {m_flight_system.guid()};
    const GUID transform_physiscs_dep[] = {m_transform_system.guid(), m_physics_system.guid()};
    const GUID transform_health_dep[] = {m_transform_system.guid(), m_health_system.guid()};
    const GUID transform_score_dep[] = {m_transform_system.guid(), m_score_system.guid()};
    const GUID phsyics_transform_anim_dep[] = {m_physics_system.guid(),
                                               m_transform_animation_system.guid()};

    engine::gameplay::DefaultSystemRunner::Registration registrations[] = {
        {&m_flight_system, Slice<GUID>()},
        {&m_physics_system, flight_system_dep},
        {&m_collision_system, transform_physiscs_dep},
        {&m_transform_animation_system, physics_system_dep},
        {&m_transform_system, phsyics_transform_anim_dep},
        {&m_fire_system, transform_system_dep},
        {&m_player_system, transform_system_dep},
        {&m_graphics_system, transform_system_dep},
        {&m_camera_system, transform_system_dep},
        {&m_lifetime_system, Slice<GUID>()},
        {&m_health_system, Slice<GUID>()},
        {&m_light_system, transform_system_dep},
        {&m_spawner_system, transform_health_dep},
        {&m_score_system, transform_health_dep},
        {&m_cage_system, transform_score_dep},
        {&m_audio_system, transform_system_dep},
        {&m_ambient_track_system, Slice<GUID>()},
    };

    if (!m_system_runner.register_systems(registrations)) {
        MODUS_LOGE("ModusGame: Failed to register systems");
        return Error<>();
    }
#else
    // clang-format off
    using TJob = engine::gameplay::ThreadedSystemRunnerJob;
    m_system_runner.set_job(TJob(&m_flight_system, true).chain_parallel({
        TJob(&m_ambient_track_system),
        TJob(&m_lifetime_system),
        TJob(&m_physics_system, true).chain(
            TJob(&m_collision_system).chain(
                TJob(&m_transform_animation_system).chain(
                    TJob(&m_transform_system, true).chain(
                        TJob(&m_fire_system, true).chain_parallel({
                            TJob(&m_graphics_system),
                            TJob(&m_camera_system),
                            TJob(&m_light_system),
                            TJob(&m_health_system).chain(
                                    TJob(&m_player_system, true).chain(
                                    TJob(&m_spawner_system, true).chain(
                                        TJob(&m_cage_system, true).chain(
                                            TJob(&m_score_system, true).chain(
                                                TJob(&m_audio_system, true)
                                            )
                                        )
                                    )
                                )
                            )
                        })
                    )
                )
            )
        )
    }));
    // clang-format on
#endif

    if (!m_system_runner.initialize(engine, *this, m_entity_manager)) {
        MODUS_LOGE("ModusGame: Failed to initialize systems");
        return Error<>();
    }

    if (auto r_entity = m_entity_manager.create("GlobalLight"); !r_entity) {
        MODUS_LOGE("ModusWorld: Failed to create light entity");
        return Error<>();
    } else {
        if (auto r_lc = m_entity_manager.add_component<engine::graphics::LightComponent>(*r_entity);
            r_lc) {
            auto& light = (*r_lc)->light;
            light.set_type(engine::graphics::LightType::Directional);
            light.set_position(glm::vec3(0.f, 2000.f, 0.f));
            light.set_diffuse_intensity(1.0f);
            light.set_directional_light_area(4000, 4000);
            light.set_directional_light_near_far(1.0f, 10000.f);
            light.set_diffuse_color(glm::vec3(1.0, 1.0, 1.0));
            light.set_emits_shadow(true);
        } else {
            MODUS_LOGE("ModusWorld: Failed to create light component");
            return Error<>();
        }
    }

    if (auto r_ambient_light = m_entity_manager.create("AmbientLight"); !r_ambient_light) {
        return Error<>();
    } else {
        if (auto r_lc =
                m_entity_manager.add_component<engine::graphics::LightComponent>(*r_ambient_light);
            r_lc) {
            auto& light = (*r_lc)->light;
            light.set_type(engine::graphics::LightType::Ambient);
            light.set_diffuse_color(glm::vec3(1.f));
            light.set_diffuse_intensity(0.1f);
            (*r_lc)->enabled = true;
        }
    }

    if (!m_player_input_context.initialize(engine)) {
        MODUS_LOGE("ModusWorld: Failed to initialize player input context");
        return Error<>();
    }

    m_state = modus::make_unique<ModusGameFSM>(*this);
    if (!m_state->initialize(engine)) {
        MODUS_LOGE("ModusWorld: Failed to initialize state machine");
        return Error<>();
    }
    return Ok<>();
}

Result<> ModusWorld::shutdown(engine::Engine& engine) {
    if (m_state) {
        if (!m_state->shutdown(engine)) {
            MODUS_LOGW("ModusWorld: Failed to shutdown state machine");
        }
        m_state.reset();
    }
    {
        m_player_input_context.shutdown(engine);

        // Shutdown entity manager to make sure all resources held by entities
        // are released
        if (!m_entity_manager.shutdown(engine)) {
            MODUS_LOGE("Failed to shutdown entity manager");
        }

        // Shut down systems
        m_system_runner.shutdown(engine, *this, m_entity_manager);

        ModusGame& game = static_cast<ModusGame&>(engine.game());
        if (!game.assets().destroy_sounds(engine)) {
            MODUS_LOGE("Failed to destroy sounds");
        }
    }

    if (m_physics_world) {
        if (!m_physics_world->shutdown()) {
            MODUS_LOGE("Failed to shutdown physics world");
        }
        m_physics_world.reset();
    }
    reset_storage<modus::physics::PhysicsWorld>();
    return Ok<>();
}

void ModusWorld::on_enter(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().add_context(&m_player_input_context);

    m_ambient_track_system.resume(engine, *this);
    m_state->start(engine).expect("Failed to start state machine");
}

void ModusWorld::on_exit(engine::Engine& engine) {
    m_ambient_track_system.pause(engine, *this);
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().remove_context(&m_player_input_context);
}

void ModusWorld::tick(engine::Engine& engine, const IMilisec) {
    MODUS_PROFILE_GAME_BLOCK("ModusWorld::tick");
    // Upadate state machine
    m_state->update(engine);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& debug_drawer = graphics_module->debug_drawer();
#if !defined(MODUS_RTM)

    // engine::graphics::PhysicsDebugDrawer physics_debug_drawer(engine);
    // m_physics_world->debug_draw(physics_debug_drawer);
    debug_drawer.draw_text_2d(modus::format("Radius: {}", m_cage_system.cage_radius()), 10, 60,
                              1.0f, glm::vec3(1.0f));
#endif
    debug_drawer.draw_text_2d(modus::format("FPS: {:04.2f}", engine.fps()), 10, 5, 1.0f,
                              glm::vec3(1.0f));
    debug_drawer.draw_text_2d(modus::format("Score: {:09}", m_score_system.total_score()), 10, 30,
                              1.0f, glm::vec3(1.0f));
}

void ModusWorld::tick_fixed(engine::Engine& engine, const IMilisec tick) {
    MODUS_PROFILE_GAME_BLOCK("ModusWorld::tick_fixed");
    {
        MODUS_PROFILE_GAME_BLOCK("Command Queue");
        // Process command queue
        process_command_queue(engine);
    }
    {
        MODUS_PROFILE_GAME_BLOCK("Entity Manager Update");
        // Update entity management
        m_entity_manager.update(engine);
    }

    {
        MODUS_PROFILE_GAME_BLOCK("System Runner");
        // Update Systems
        m_system_runner.update(engine, tick, *this, m_entity_manager);
    }
}

engine::gameplay::EntityId ModusWorld::main_camera() const {
    return m_camera_id;
}

Result<> ModusWorld::reset(engine::Engine& engine) {
    MODUS_PROFILE_GAME_BLOCK("ModusWorld::reset");
    // Cleanup left over asteroids
    engine::gameplay::EntityManagerView<> view(m_entity_manager);
    view.for_each_with_tag(kEntityTagAsteroid, [this](const engine::gameplay::EntityId id) {
        m_entity_manager.destroy(id);
    });

    // Fully delete entities
    m_entity_manager.update(engine);

    if (!m_player_system.reset(engine, *this, m_entity_manager)) {
        MODUS_LOGE("ModusWorld: Failed to reset player system");
        return Error<>();
    }

    m_player_input_context.reset();
    m_player_input_context.set_player_entity_id(m_player_system.player_id());

    if (!setup_camera(engine)) {
        MODUS_LOGE("ModusWorld: Failed to setup camera");
        return Error<>();
    }

    if (!m_fire_system.reset(m_entity_manager)) {
        return Error<>();
    }
    m_spawner_system.reset();
    m_cage_system.reset(glm::vec3(3000.f));
    m_score_system.reset(m_player_system.player_id());

    return Ok<>();
}

Result<> ModusWorld::setup_camera(engine::Engine& engine) {
    if (!m_camera_id) {
        auto r_entity = m_entity_manager.create();
        if (!r_entity) {
            return Error<>();
        }
        m_camera_id = *r_entity;
    }

    auto r_component =
        m_entity_manager.add_component<engine::gameplay::TransformComponent>(m_camera_id);
    if (!r_component) {
        return Error<>();
    }

    engine::gameplay::TransformComponent& tc = *(*r_component);
    tc.deattach(m_entity_manager);
    if (!tc.attach(m_entity_manager, player_entity_id())) {
        MODUS_LOGE("ModusWord: Failed to attach camera to ship");
        return Error<>();
    }

    tc.set_translation(glm::vec3(0.f, 6.f, -15.f));

    auto r_camera = m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
    if (!r_camera) {
        return Error<>();
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto window = graphics_module->window();
    const app::WindowState window_state = window->window_state();
    (*r_camera)->m_camera.frustum().set_aspect_ratio(f32(window_state.width) /
                                                     f32(window_state.height));

    (*r_camera)->m_forward_vec = glm::vec3(0.f, 0.f, 50.f);
    (*r_camera)->m_camera.frustum().set_near_far(.1f, 10000.f);
    return Ok<>();
}

}    // namespace modus::game
