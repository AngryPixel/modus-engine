/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define MODUS_WORLD_USE_THREADED_SYSTEM_RUNNER

#include <engine/audio/ecs/systems/audio_system.hpp>
#include <engine/gameplay/camera_controllers.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#if defined(MODUS_WORLD_USE_THREADED_SYSTEM_RUNNER)
#include <engine/gameplay/ecs/systems/threaded_system_runner.hpp>
#else
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#endif
#include <engine/gameplay/ecs/systems/transform_animation_system.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/ecs/systems/camera_system.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/physics/ecs/systems/collision_system.hpp>
#include <engine/physics/ecs/systems/physics_system.hpp>
#include <gameplay/entity_manager.hpp>
#include <gameplay/systems/ambient_track_system.hpp>
#include <gameplay/systems/asteroid_spawner_system.hpp>
#include <gameplay/systems/cage_system.hpp>
#include <gameplay/systems/flight_system.hpp>
#include <gameplay/systems/health_system.hpp>
#include <gameplay/systems/laser_gun_system.hpp>
#include <gameplay/systems/lifetime_system.hpp>
#include <gameplay/systems/player_system.hpp>
#include <gameplay/systems/score_system.hpp>
#include <input/player_input_context.hpp>
#include <math/random.hpp>
#include <physics/collision_shape.hpp>
#include <physics/world.hpp>

namespace modus::engine {
class DebugCameraInputContext;
namespace event {
class Event;
}
}    // namespace modus::engine

namespace modus::audio {
class AudioContext;
}

namespace modus::engine::gameplay {
class FSM;
}

namespace modus::game {
class ModusWorld final : public engine::gameplay::GameWorld {
   private:
    struct Debug {
        bool draw_sphere_bounds = false;
    };

   private:
    std::unique_ptr<modus::physics::PhysicsWorld> m_physics_world;

    // ECS
    engine::physics::PhysicsSystem m_physics_system;
    engine::physics::CollisionSystem m_collision_system;
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::gameplay::TransformAnimationSystem m_transform_animation_system;
    engine::graphics::LightSystem m_light_system;
    engine::graphics::CameraSystem m_camera_system;
    LifetimeSystem m_lifetime_system;
    HealthSystem m_health_system;
    CageSystem m_cage_system;
    LaserGunSystem m_fire_system;
    AsteroidSpawnerSystem m_spawner_system;
    ScoreSystem m_score_system;
    PlayerSystem m_player_system;
    engine::audio::AudioSystem m_audio_system;
    FlightSystem m_flight_system;
    AmbientTrackSystem m_ambient_track_system;
#if !defined(MODUS_WORLD_USE_THREADED_SYSTEM_RUNNER)
    engine::gameplay::DefaultSystemRunner m_system_runner;
#else
    engine::gameplay::ThreadedSystemRunner m_system_runner;
#endif

    // Gameplay
    math::random::RandomGenerator m_random_generator;

    // Input
    PlayerInputContext m_player_input_context;

    // Important Entity IDs
    engine::gameplay::EntityId m_camera_id;

    std::unique_ptr<engine::gameplay::FSM> m_state;
    // Debug Options
    Debug m_debug_opt;

   public:
    static Result<> register_components(engine::gameplay::EntityManager& em);

    ModusWorld();

    ~ModusWorld();

    Result<> initialize(engine::Engine&) override;

    Result<> shutdown(engine::Engine&) override;

    void on_enter(engine::Engine&) override;

    void on_exit(engine::Engine&) override;

    void tick(engine::Engine& engine, const IMilisec tick) override;

    void tick_fixed(engine::Engine& engine, const IMilisec tick) override;

    engine::gameplay::EntityId main_camera() const override;

    NotMyPtr<modus::physics::PhysicsWorld> physics_world() { return m_physics_world.get(); }

    NotMyPtr<const modus::physics::PhysicsWorld> physics_world() const {
        return m_physics_world.get();
    }

    auto& random_generator() { return m_random_generator; }

    engine::gameplay::EntityManager& entity_manager() { return m_entity_manager; }

    const engine::gameplay::EntityManager& entity_manager() const { return m_entity_manager; }

    LaserGunSystem& fire_system() { return m_fire_system; }

    const LaserGunSystem& fire_system() const { return m_fire_system; }

    const AsteroidSpawnerSystem& asteroid_spawner_system() const { return m_spawner_system; }

    const ScoreSystem& score_system() const { return m_score_system; }

    engine::gameplay::EntityId player_entity_id() const { return m_player_system.player_id(); }

    Result<> reset(engine::Engine& engine);

   private:
    Result<> setup_camera(engine::Engine& engine);
};

}    // namespace modus::game
