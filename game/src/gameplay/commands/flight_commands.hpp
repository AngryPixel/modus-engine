/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/ecs_types.hpp>
#include <engine/gameplay/gameworld.hpp>

namespace modus::game {

class FlightYawCommand final {
   private:
    engine::gameplay::EntityId m_entity_id;
    f32 m_value;

   public:
    FlightYawCommand(const engine::gameplay::EntityId id, const f32 value);

    void operator()(engine::Engine& engine, engine::gameplay::GameWorld& game_world);
};

class FlightRollCommand final {
   private:
    engine::gameplay::EntityId m_entity_id;
    f32 m_value;

   public:
    FlightRollCommand(const engine::gameplay::EntityId id, const f32 value);

    void operator()(engine::Engine& engine, engine::gameplay::GameWorld& game_world);
};

class FlightPitchCommand final {
   private:
    engine::gameplay::EntityId m_entity_id;
    f32 m_value;

   public:
    FlightPitchCommand(const engine::gameplay::EntityId id, const f32 value);

    void operator()(engine::Engine& engine, engine::gameplay::GameWorld& game_world);
};

class FlightThrusterCommand final {
   private:
    engine::gameplay::EntityId m_entity_id;
    f32 m_value;

   public:
    FlightThrusterCommand(const engine::gameplay::EntityId id, const f32 value);

    void operator()(engine::Engine& engine, engine::gameplay::GameWorld& game_world);
};

class FlightVerticalThrusterCommand final {
   private:
    engine::gameplay::EntityId m_entity_id;
    f32 m_value;

   public:
    FlightVerticalThrusterCommand(const engine::gameplay::EntityId id, const f32 value);

    void operator()(engine::Engine& engine, engine::gameplay::GameWorld& game_world);
};

}    // namespace modus::game
