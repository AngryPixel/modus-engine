/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <gameplay/commands/flight_commands.hpp>
#include <gameplay/modus_world.hpp>
#include <gameplay/components/flight_component.hpp>

namespace modus::game {

FlightYawCommand::FlightYawCommand(const engine::gameplay::EntityId id, const f32 value)
    : m_entity_id(id), m_value(value) {}

void FlightYawCommand::operator()(engine::Engine&, engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    auto r_fc = world.entity_manager().component<FlightComponent>(m_entity_id);
    if (r_fc) {
        (*r_fc)->m_yaw_amount = m_value;
    } /*else {
        MODUS_LOGE("FightYawCommand: Enitity {} does not have a Flight Component", m_entity_id);
    }*/
}

FlightRollCommand::FlightRollCommand(const engine::gameplay::EntityId id, const f32 value)
    : m_entity_id(id), m_value(value) {}

void FlightRollCommand::operator()(engine::Engine&, engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    auto r_fc = world.entity_manager().component<FlightComponent>(m_entity_id);
    if (r_fc) {
        (*r_fc)->m_roll_amount = m_value;
    } /*else {
        MODUS_LOGE("FightRollCommand: Enitity {} does not have a Flight Component", m_entity_id);
    }*/
}

FlightPitchCommand::FlightPitchCommand(const engine::gameplay::EntityId id, const f32 value)
    : m_entity_id(id), m_value(value) {}

void FlightPitchCommand::operator()(engine::Engine&, engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    auto r_fc = world.entity_manager().component<FlightComponent>(m_entity_id);
    if (r_fc) {
        (*r_fc)->m_pitch_amount = m_value;
    } /*else {
        MODUS_LOGE("FightPitchCommand: Enitity {} does not have a Flight Component", m_entity_id);
    }*/
}

FlightThrusterCommand::FlightThrusterCommand(const engine::gameplay::EntityId id, const f32 value)
    : m_entity_id(id), m_value(value) {}

void FlightThrusterCommand::operator()(engine::Engine& engine,
                                       engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    MODUS_UNUSED(engine);
    auto r_fc = world.entity_manager().component<FlightComponent>(m_entity_id);
    if (r_fc) {
        (*r_fc)->m_thrust_amount = m_value;
    } /*else {
        MODUS_LOGE(
            "FightThrusterCommand: Enitity {} does not have a Flight "
            "Component",
            m_entity_id);
    }*/
}

FlightVerticalThrusterCommand::FlightVerticalThrusterCommand(const engine::gameplay::EntityId id,
                                                             const f32 value)
    : m_entity_id(id), m_value(value) {}

void FlightVerticalThrusterCommand::operator()(engine::Engine& engine,
                                               engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    MODUS_UNUSED(engine);
    auto r_fc = world.entity_manager().component<FlightComponent>(m_entity_id);
    if (r_fc) {
        (*r_fc)->m_vertical_thrust_amount = m_value;
    } /*else {
        MODUS_LOGE(
            "FightVerticalThrusterCommand: Enitity {} does not have a Flight "
            "Component",
            m_entity_id);
    } */
}

}    // namespace modus::game
