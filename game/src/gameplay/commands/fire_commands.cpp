/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <gameplay/commands/fire_commands.hpp>
#include <gameplay/modus_world.hpp>
#include <gameplay/components/laser_gun_component.hpp>

namespace modus::game {

FireBeginCommand::FireBeginCommand(const engine::gameplay::EntityId id) : m_entity_id(id) {
    MODUS_UNUSED(m_entity_id);
}

void FireBeginCommand::operator()(engine::Engine&, engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    auto r_lgc = world.entity_manager().component<LaserGunComponent>(m_entity_id);
    if (r_lgc) {
        (*r_lgc)->m_is_firing = true;
    } /*else {
        MODUS_LOGE(
            "FireBeginCommand: Enitity {} does not have a Laser Gun "
            "Component",
            m_entity_id);
    }*/
}

FireEndCommand::FireEndCommand(const engine::gameplay::EntityId id) : m_entity_id(id) {
    MODUS_UNUSED(m_entity_id);
}

void FireEndCommand::operator()(engine::Engine&, engine::gameplay::GameWorld& game_world) {
    ModusWorld& world = static_cast<ModusWorld&>(game_world);
    auto r_lgc = world.entity_manager().component<LaserGunComponent>(m_entity_id);
    if (r_lgc) {
        (*r_lgc)->m_is_firing = false;
    } /*else {
        MODUS_LOGE(
            "FireBeginCommand: Enitity {} does not have a Laser Gun "
            "Component",
            m_entity_id);
    }*/
}

}    // namespace modus::game
