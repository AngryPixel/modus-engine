/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gameplay/entity_templates/entity_templates.hpp>
// clang-format on
#include <game/entity_templates/asteroid_generated.h>
#include <game/entity_templates/laser_generated.h>
#include <game/entity_templates/ship_generated.h>
#include <game/entity_templates/cage_generated.h>

#include <engine/gameplay/ecs/entity_template_helper.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/components/velocity_component.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <gameplay/components/asteroid_info_component.hpp>
#include <gameplay/components/health_component.hpp>
#include <gameplay/components/laser_gun_component.hpp>
#include <gameplay/components/lifetime_component.hpp>
#include <gameplay/components/flight_component.hpp>
#include <engine/modules/module_gameplay.hpp>

MODUS_GAMPLEY_ENTITY_TEMPLATE_FLATBUFFER_HELPER_TRAIT(modus::game::fbs::etpl, Asteroid)
MODUS_GAMPLEY_ENTITY_TEMPLATE_FLATBUFFER_HELPER_TRAIT(modus::game::fbs::etpl, Laser)
MODUS_GAMPLEY_ENTITY_TEMPLATE_FLATBUFFER_HELPER_TRAIT(modus::game::fbs::etpl, Ship)
MODUS_GAMPLEY_ENTITY_TEMPLATE_FLATBUFFER_HELPER_TRAIT(modus::game::fbs::etpl, Cage)

namespace modus::game {

using AsteroidTemplate =
    engine::gameplay::EntityTemplateFlatbufferHelper<modus::game::fbs::etpl::Asteroid,
                                                     engine::gameplay::GameplayAllocator,
                                                     HealthComponent,
                                                     AsteroidInfoComponent,
                                                     engine::gameplay::TransformComponent,
                                                     engine::physics::CollisionComponent,
                                                     engine::graphics::GraphicsComponent,
                                                     engine::physics::PhysicsComponent,
                                                     engine::gameplay::VelocityComponent>;

using LaserTemplate =
    engine::gameplay::EntityTemplateFlatbufferHelper<modus::game::fbs::etpl::Laser,
                                                     engine::gameplay::GameplayAllocator,
                                                     engine::gameplay::TransformComponent,
                                                     engine::physics::CollisionComponent,
                                                     engine::graphics::GraphicsComponent,
                                                     engine::physics::PhysicsComponent,
                                                     LifetimeComponent,
                                                     engine::graphics::LightComponent,
                                                     engine::gameplay::VelocityComponent>;

using ShipTemplate =
    engine::gameplay::EntityTemplateFlatbufferHelper<modus::game::fbs::etpl::Ship,
                                                     engine::gameplay::GameplayAllocator,
                                                     HealthComponent,
                                                     LaserGunComponent,
                                                     FlightComponent,
                                                     engine::gameplay::TransformComponent,
                                                     engine::physics::CollisionComponent,
                                                     engine::graphics::GraphicsComponent,
                                                     engine::physics::PhysicsComponent,
                                                     engine::gameplay::VelocityComponent>;

using CageTemplate =
    engine::gameplay::EntityTemplateFlatbufferHelper<modus::game::fbs::etpl::Cage,
                                                     engine::gameplay::GameplayAllocator,
                                                     engine::gameplay::TransformComponent,
                                                     engine::graphics::GraphicsComponent,
                                                     engine::physics::PhysicsComponent>;

struct EntityTemplates::Private {
    AsteroidTemplate asteroid;
    LaserTemplate laser;
    ShipTemplate ship;
    CageTemplate cage;
    Private()
        : asteroid("modus.game.fbs.etpl.Asteroid", 8),
          laser("modus.game.fbs.etpl.Laser", 8),
          ship("modus.game.fbs.etpl.Ship", 8),
          cage("modus.game.fbs.etpl.Cage", 8) {}
};

EntityTemplates::EntityTemplates() {
    m_private = modus::make_unique<Private>();
}

EntityTemplates::~EntityTemplates() {}

Result<> EntityTemplates::initialize(engine::Engine& engine) {
    auto& template_registry = engine.module<engine::ModuleGameplay>()->entity_template_registry();
    if (!template_registry.register_template(&m_private->asteroid)) {
        MODUS_LOGE("Failed to register asteroid entity template");
        return Error<>();
    }
    if (!template_registry.register_template(&m_private->laser)) {
        MODUS_LOGE("Failed to register laser entity template");
        return Error<>();
    }
    if (!template_registry.register_template(&m_private->ship)) {
        MODUS_LOGE("Failed to register ship entity template");
        return Error<>();
    }
    if (!template_registry.register_template(&m_private->cage)) {
        MODUS_LOGE("Failed to register cage entity template");
        return Error<>();
    }
    return Ok<>();
}

void EntityTemplates::shutdown(engine::Engine& engine) {
    auto& template_registry = engine.module<engine::ModuleGameplay>()->entity_template_registry();
    template_registry.unregister_template(&m_private->asteroid);
    template_registry.unregister_template(&m_private->laser);
    template_registry.unregister_template(&m_private->ship);
    template_registry.unregister_template(&m_private->cage);
}

}    // namespace modus::game
