/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <gameplay/main_ui_world.hpp>

#include "ui/main_ui_view.hpp"
#include <engine/modules/module_ui.hpp>
#include <ui/context.hpp>

namespace modus::game {

MainUIWorld::MainUIWorld() {}

MainUIWorld::~MainUIWorld() {}

Result<> MainUIWorld::initialize(engine::Engine&) {
    m_ui_created = false;
    return Ok<>();
}

Result<> MainUIWorld::shutdown(engine::Engine&) {
    m_ui_view.reset();
    return Ok<>();
}

void MainUIWorld::on_enter(engine::Engine& engine) {
    if (m_ui_created) {
        auto ui_module = engine.module<engine::ModuleUI>();
        ui_module->context().view_controller().push_view(m_ui_view);
        ui_module->show(engine, true);
    }
}

void MainUIWorld::on_exit(engine::Engine& engine) {
    auto ui_module = engine.module<engine::ModuleUI>();
    ui_module->context().view_controller().pop_view();
    ui_module->hide(engine);
}

void MainUIWorld::tick(engine::Engine& engine, const IMilisec) {
    if (!m_ui_created) {
        if (auto r_state = engine.startup_asset_state(); !r_state) {
            MODUS_LOGE("Failed to load startup assets");
            engine.quit();
            return;
        } else if (r_state->load_count != r_state->total_count) {
            return;
        }
        MODUS_LOGD("All Engine Assets loading, presenting main menu");
        auto ui_module = engine.module<engine::ModuleUI>();
        auto& ui_context = ui_module->context();
        m_ui_view = ui_context.create_object<MainUIView>(engine);
        m_ui_view->create_ui();
        ui_module->context().view_controller().push_view(m_ui_view);
        ui_module->show(engine, true);
        m_ui_created = true;
    }
}

void MainUIWorld::tick_fixed(engine::Engine&, const IMilisec) {}

engine::gameplay::EntityId MainUIWorld::main_camera() const {
    return engine::gameplay::EntityId();
}

}    // namespace modus::game
