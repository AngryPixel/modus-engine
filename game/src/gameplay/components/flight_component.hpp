/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/component_storage/hashmap_storage.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>

namespace modus::game {

namespace fbs {
struct FlightComponent;
}

struct FlightComponent final : public engine::gameplay::Component {
    f32 m_angular_magnitude_roll;
    f32 m_angular_magnitude_pitch;
    f32 m_angular_magnitude_yaw;
    f32 m_max_velocity;
    f32 m_max_velocity_up;
    glm::vec3 m_dir_forward;
    glm::vec3 m_dir_up;
    f32 m_pitch_amount;              //[-N, N]
    f32 m_roll_amount;               //[-N, N]
    f32 m_yaw_amount;                //[-N, N]
    f32 m_thrust_amount;             //[0, N]
    f32 m_vertical_thrust_amount;    //[0, N]
    engine::gameplay::EntityId m_audio_source_entity;

    void on_create(const engine::gameplay::EntityId) override {
        m_angular_magnitude_roll = 1000.f;
        m_angular_magnitude_pitch = 1000.f;
        m_angular_magnitude_yaw = 1000.f;
        m_max_velocity = 1500.f;
        m_max_velocity_up = 800.f;
        m_dir_forward = glm::vec3(0.f, 0.f, 1.0f);
        m_dir_up = glm::vec3(0.f, 1.0f, 0.f);
        m_pitch_amount = 0.f;
        m_yaw_amount = 0.f;
        m_roll_amount = 0.f;
        m_thrust_amount = 0.f;
        m_vertical_thrust_amount = 0.f;
        m_audio_source_entity = engine::gameplay::EntityId();
    }

    void reset_state() {
        m_pitch_amount = 0.f;
        m_yaw_amount = 0.f;
        m_roll_amount = 0.f;
        m_thrust_amount = 0.f;
        m_vertical_thrust_amount = 0.f;
    }
};

}    // namespace modus::game

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<modus::game::FlightComponent> {
    using StorageType = HashMapStorage<modus::game::FlightComponent>;
    using FlatbufferType = modus::game::fbs::FlightComponent;
};
template <>
struct ComponentLoaderTraits<modus::game::FlightComponent> {
    using ComponentType = modus::game::FlightComponent;
    using IntermediateType = modus::game::FlightComponent;
    using StorageType = ComponentTraits<modus::game::FlightComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.flight();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::game::FlightComponent)
