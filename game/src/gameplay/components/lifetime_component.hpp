/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/modules/module_gameplay.hpp>
namespace modus::game {

namespace fbs {
struct LifetimeComponent;
}

struct MODUS_ENGINE_EXPORT LifetimeComponent final : public engine::gameplay::Component {
    FSeconds m_max_time_sec;
    FSeconds m_elapsed_time_sec;

    void on_create(const engine::gameplay::EntityId) override {
        m_max_time_sec = FSeconds(1.0f);
        m_elapsed_time_sec = FSeconds(0.0f);
    }
};

}    // namespace modus::game

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<modus::game::LifetimeComponent> {
    using StorageType = SparseStorage<modus::game::LifetimeComponent>;
    using FlatbufferType = modus::game::fbs::LifetimeComponent;
};
template <>
struct ComponentLoaderTraits<modus::game::LifetimeComponent> {
    using ComponentType = modus::game::LifetimeComponent;
    using IntermediateType = modus::game::LifetimeComponent;
    using StorageType = ComponentTraits<modus::game::LifetimeComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.lifetime();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::game::LifetimeComponent)
