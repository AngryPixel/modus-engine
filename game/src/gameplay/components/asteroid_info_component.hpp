/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::game {

namespace fbs {
struct AsteroidLevelComponent;
}

struct MODUS_ENGINE_EXPORT AsteroidInfoComponent final : public engine::gameplay::Component {
    u32 m_level;

    void on_create(const engine::gameplay::EntityId) override {
        m_level = std::numeric_limits<u32>::max();
    }
};

}    // namespace modus::game

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<modus::game::AsteroidInfoComponent> {
    using StorageType = SparseStorage<modus::game::AsteroidInfoComponent>;
    using FlatbufferType = modus::game::fbs::AsteroidLevelComponent;
};

template <>
struct ComponentLoaderTraits<modus::game::AsteroidInfoComponent> {
    using ComponentType = modus::game::AsteroidInfoComponent;
    using IntermediateType = modus::game::AsteroidInfoComponent;
    using StorageType = ComponentTraits<modus::game::AsteroidInfoComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.asteroid_level();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::game::AsteroidInfoComponent)
