/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/fixed_vector.hpp>
#include <engine/gameplay/ecs/component_storage/hashmap_storage.hpp>
namespace modus::game {

namespace fbs {
struct LaserGunComponent;
}

struct LaserGunComponent final : public engine::gameplay::Component {
    static constexpr u32 kMaxAudioSources = 2;
    FixedVector<engine::gameplay::EntityId, kMaxAudioSources> m_audio_sources;
    u32 m_current_audio_source;
    f32 m_damage;
    FSeconds m_fire_interval_sec;
    FSeconds m_elapsed_seconds_since_last_shot;
    glm::vec3 m_spawn_offset;
    bool m_is_firing;

    void on_create(const engine::gameplay::EntityId) override {
        m_audio_sources.clear();
        m_current_audio_source = 0;
        m_damage = 25.f;
        m_fire_interval_sec = FSeconds(.25f);
        m_elapsed_seconds_since_last_shot = FSeconds(0.f);
        m_spawn_offset = glm::vec3(0.f);
        m_is_firing = false;
    }
};

}    // namespace modus::game

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<modus::game::LaserGunComponent> {
    using StorageType = HashMapStorage<modus::game::LaserGunComponent>;
    using FlatbufferType = modus::game::fbs::LaserGunComponent;
};

template <>
struct ComponentLoaderTraits<modus::game::LaserGunComponent> {
    using ComponentType = modus::game::LaserGunComponent;
    using IntermediateType = modus::game::LaserGunComponent;
    using StorageType = ComponentTraits<modus::game::LaserGunComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.laser_gun();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::game::LaserGunComponent)
