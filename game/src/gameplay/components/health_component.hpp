/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/component_storage/vector_storage.hpp>

namespace modus::game {

namespace fbs {
struct HealthComponent;
}

struct MODUS_ENGINE_EXPORT HealthComponent final : public engine::gameplay::Component {
    using OnDeathFn = void (*)(engine::Engine&, const engine::gameplay::EntityId);
    f32 m_health = 100.0f;
    OnDeathFn m_on_death_fn = nullptr;

    void on_create(const engine::gameplay::EntityId) override {
        m_health = 100.0f;
        m_on_death_fn = nullptr;
    }
};

}    // namespace modus::game

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<modus::game::HealthComponent> {
    using StorageType = VectorStorage<modus::game::HealthComponent>;
    using FlatbufferType = modus::game::fbs::HealthComponent;
};
template <>
struct ComponentLoaderTraits<modus::game::HealthComponent> {
    using ComponentType = modus::game::HealthComponent;
    using IntermediateType = modus::game::HealthComponent;
    using StorageType = ComponentTraits<modus::game::HealthComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.health();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::game::HealthComponent)
