/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/physics/ecs/systems/collision_system.hpp>
#include <gameplay/systems/laser_gun_system.hpp>
#include <test_worlds/test_world_base.hpp>

namespace modus::game {

class ModelViewerInputContext;

class ModelViewerWorld final : public TestWorldBase {
   private:
    std::unique_ptr<ModelViewerInputContext> m_input_context;
    bool m_debug_draw_physics;
    bool m_debug_draw_skeletons;
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::graphics::LightSystem m_light_system;
    engine::physics::CollisionSystem m_collision_system;
    LaserGunSystem m_fire_system;

   public:
    ModelViewerWorld();

    ~ModelViewerWorld();

    Result<> initialize(engine::Engine&) override;

    Result<> shutdown(engine::Engine&) override;

    void on_enter(engine::Engine&) override;

    void on_exit(engine::Engine&) override;

    void tick(engine::Engine& engine, const IMilisec tick) override;

    void tick_fixed(engine::Engine& engine, const IMilisec) override;

    void toggle_debug_draw_physics();

    void toggle_debug_draw_skeletons();
};
}    // namespace modus::game
