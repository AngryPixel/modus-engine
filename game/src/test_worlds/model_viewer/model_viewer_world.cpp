/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat off
#include <app/app.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/graphics/debug_drawers/animation_debug_drawer.hpp>
#include <engine/graphics/debug_drawers/physics_debug_drawer.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/input/default_input_priorities.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <modus_game.hpp>
#include <physics/debug_drawer.hpp>
#include <physics/world.hpp>
#include <test_worlds/model_viewer/model_viewer_world.hpp>

namespace modus::game {

class ModelViewerInputContext final : public engine::input::InputContext {
   private:
    ModelViewerWorld& m_world;

   public:
    ModelViewerInputContext(ModelViewerWorld& world)
        : engine::input::InputContext(engine::input::kGameInputPriorityMin), m_world(world) {}

    StringSlice name() const override { return "ModelViewerInputContext"; }

    bool handle_event(engine::Engine& engine, const app::InputEvent& event) override {
        if (event.type == app::InputEventType::Keyboard) {
            auto input_module = engine.module<engine::ModuleInput>();
            if (input_module->manager().input_state().is_key_down(app::InputKey::KeyLeftCtrl) &&
                event.keyboard.key == app::InputKey::KeyEscape && event.keyboard.is_key_down()) {
                auto app_module = engine.module<engine::ModuleApp>();
                app_module->app().lock_mouse(!app_module->app().is_mouse_locked());
                return true;
            } else if (event.keyboard.key == app::InputKey::KeyP && event.keyboard.is_key_up()) {
                m_world.toggle_debug_draw_physics();
                return true;
            } else if (event.keyboard.key == app::InputKey::KeyB && event.keyboard.is_key_up()) {
                m_world.toggle_debug_draw_skeletons();
                return true;
            }
        }
        return false;
    }
};

ModelViewerWorld::ModelViewerWorld()
    : m_input_context(modus::make_unique<ModelViewerInputContext>(*this)),
      m_debug_draw_physics(false),
      m_debug_draw_skeletons(false),
      m_fire_system() {}

ModelViewerWorld::~ModelViewerWorld() {}

Result<> ModelViewerWorld::initialize(engine::Engine& engine) {
    if (!TestWorldBase::initialize(engine)) {
        return Error<>();
    }

    const ModusGame& game = (static_cast<ModusGame&>(engine.game()));

    if (auto r = game.assets().spawn_ship(engine, m_entity_manager, *m_physics_world,
                                          glm::vec3(-100.f, 0.f, 0.f));
        !r) {
        return Error<>();
    }

    if (!game.assets().spawn_cage(engine, m_entity_manager, *m_physics_world, 3000.0f)) {
        return Error<>();
    }

    m_debug_camera_input_context.set_position(glm::vec3(-100.f, 0.f, 10.f));

    constexpr f32 kTranslationOffset = 150.f;
    glm::vec3 translation = glm::vec3(kTranslationOffset, 0.f, 0.f);

    for (u32 l = 0; l < ModusAssets::kNumAsteroidLevels; ++l) {
        translation.x = kTranslationOffset;
        translation.z = -f32(l) * kTranslationOffset;
        for (u32 i = 0; i < ModusAssets::kNumAsteroidTypes; ++i) {
            math::Transform t = math::Transform::kIdentity;
            t.set_translation(translation);
            if (!game.assets().spawn_asteroid(engine, m_entity_manager, *m_physics_world, i, l, t,
                                              glm::vec3(0.), glm::vec3(0.))) {
                return Error<>();
            }
            translation.x += kTranslationOffset;
        }
    }

    math::Transform t = math::Transform::kIdentity;
    t.set_translation(-10.0f, 0.f, 0.f);
    if (!m_fire_system.spawn_laser_bolt_at(engine, m_entity_manager, t)) {
        return Error<>();
    }

    t.set_translation(-100.0f, 0.f, 20.0f);
    if (!m_fire_system.spawn_laser_bolt_at(engine, m_entity_manager, t)) {
        return Error<>();
    }

    return Ok<>();
}

Result<> ModelViewerWorld::shutdown(engine::Engine& engine) {
    return TestWorldBase::shutdown(engine);
}

void ModelViewerWorld::on_enter(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().add_context(m_input_context.get());
}

void ModelViewerWorld::on_exit(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().remove_context(m_input_context.get());
}

void ModelViewerWorld::tick(engine::Engine& engine, const IMilisec tick) {
    // Do entity add/removals from last frame
    m_entity_manager.update(engine);

    // Update camera
    math::Transform camera_transform;
    m_debug_camera_input_context.update_camera(engine, camera_transform);
    camera().set_transform(camera_transform);

    NotMyPtr<engine::gameplay::System> systems[] = {&m_transform_system, &m_graphics_system,
                                                    &m_light_system};
    engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager, systems);

    /*
    m_entity_manager.for_each_matching<engine::graphics::LightComponent>(
        [&engine](const engine::gameplay::EntityManager&, const
    engine::gameplay::EntityId, const engine::graphics::LightComponent& lc) {
            engine.modules().graphics().debug_drawer().draw_cube(lc.light.position(),
                                                                 glm::vec3(1.0f),
                                                                 glm::vec4(lc.light.diffuse_color(),
    1.0f), false);

            engine.modules().graphics().debug_drawer().draw_sphere(lc.light.position(),
                                                                   lc.light.bounding_sphere().radius,
                                                                 glm::vec4(lc.light.diffuse_color(),
    1.0f), true);
            });*/
    if (m_debug_draw_physics) {
        engine::graphics::PhysicsDebugDrawer debug_drawer(engine);
        m_physics_world->debug_draw(debug_drawer);
    }

    if (m_debug_draw_skeletons) {
        engine::graphics::AnimationDebugDrawer debug_drawer(engine);
        engine::gameplay::EntityManagerView<const engine::gameplay::TransformComponent,
                                            const engine::graphics::GraphicsComponent>
            view(entity_manager());
        view.for_each([&debug_drawer](const engine::gameplay::EntityId,
                                      const engine::gameplay::TransformComponent& tc,
                                      const engine::graphics::GraphicsComponent& gc) {
            debug_drawer.draw_skeleton_from_animator(tc.world_transform().to_matrix(), gc.animator,
                                                     true);
        });
    }
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    graphics_module->debug_drawer().draw_text_2d(modus::format("FPS: {:04.2f}", engine.fps()), 10,
                                                 5, 1.0f, glm::vec3(1.0f));
}

void ModelViewerWorld::tick_fixed(engine::Engine&, const IMilisec) {}

void ModelViewerWorld::toggle_debug_draw_physics() {
    m_debug_draw_physics = !m_debug_draw_physics;
}

void ModelViewerWorld::toggle_debug_draw_skeletons() {
    m_debug_draw_skeletons = !m_debug_draw_skeletons;
}

}    // namespace modus::game
