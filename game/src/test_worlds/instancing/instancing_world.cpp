/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat off
#include <app/app.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/graphics/debug_drawers/physics_debug_drawer.hpp>
#include <engine/input/default_input_priorities.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <math/random.hpp>
#include <modus_game.hpp>
#include <physics/world.hpp>
#include <test_worlds/instancing/instancing_world.hpp>
namespace modus::game {

class InstancingInputContext final : public engine::input::InputContext {
   private:
    InstancingWorld& m_world;

   public:
    InstancingInputContext(InstancingWorld& world)
        : engine::input::InputContext(engine::input::kGameInputPriorityMin), m_world(world) {}

    StringSlice name() const override { return "InstancingInputContext"; }

    bool handle_event(engine::Engine& engine, const app::InputEvent& event) override {
        if (event.type == app::InputEventType::Keyboard) {
            auto input_module = engine.module<engine::ModuleInput>();
            if (input_module->manager().input_state().is_key_down(app::InputKey::KeyLeftCtrl) &&
                event.keyboard.key == app::InputKey::KeyEscape && event.keyboard.is_key_down()) {
                auto app_module = engine.module<engine::ModuleApp>();
                app_module->app().lock_mouse(!app_module->app().is_mouse_locked());
                return true;
            } else if (event.keyboard.key == app::InputKey::KeyP && event.keyboard.is_key_up()) {
                m_world.toggle_debug_draw_physics();
                return true;
            }
        }
        return false;
    }
};

InstancingWorld::InstancingWorld()
    : m_input_context(modus::make_unique<InstancingInputContext>(*this)),
      m_debug_draw_physics(false) {}

InstancingWorld::~InstancingWorld() {}

Result<> InstancingWorld::initialize(engine::Engine& engine) {
    if (!TestWorldBase::initialize(engine)) {
        return Error<>();
    }

    const ModusGame& game = (static_cast<ModusGame&>(engine.game()));

    if (!game.assets().spawn_ship(engine, m_entity_manager, *m_physics_world, glm::vec3(0.f))) {
        return Error<>();
    }

    constexpr f32 kTranslationOffset = 300.f;
    constexpr usize kNumRows = 60;
    constexpr usize kNumColums = 50;
    constexpr usize kNumSlices = 6;
    math::random::RandomGenerator random;
    random.init(12345);

    for (usize i = 0; i < kNumRows; ++i) {
        for (usize j = 0; j < kNumColums; ++j) {
            for (usize z = 0; z < kNumSlices; ++z) {
                const glm::vec3 translation =
                    glm::vec3(f32(i) * kTranslationOffset, f32(j) * kTranslationOffset,
                              f32(z) * kTranslationOffset);

                const u32 type = u32(random.generate() % ModusAssets::kNumAsteroidTypes);
                const u32 level = u32(random.generate() % ModusAssets::kNumAsteroidLevels);
                const f32 rot_angle = glm::radians(360.f * f32(random.generate_decimal()));
                const f32 rot_axis_x = f32(random.generate_decimal());
                const f32 rot_axis_y = f32(random.generate_decimal());
                const f32 rot_axis_z = f32(random.generate_decimal());
                math::Transform t;
                t.set_scale(1.0f);
                t.set_rotation(
                    glm::angleAxis(rot_angle, glm::vec3(rot_axis_x, rot_axis_y, rot_axis_z)));
                t.set_translation(translation);
                if (!game.assets().spawn_asteroid(engine, m_entity_manager, *m_physics_world, type,
                                                  level, t, glm::vec3(0.), glm::vec3(0.))) {
                    return Error<>();
                }
            }
        }
    }

    camera().set_look_at(glm::vec3((f32(kNumRows) * kTranslationOffset) / 2.0f,
                                   (f32(kNumColums) * kTranslationOffset) / 2.0f,
                                   (f32(kNumSlices) * kTranslationOffset) * 6.0f),
                         glm::vec3((f32(kNumRows) * kTranslationOffset) / 2.0f,
                                   (f32(kNumColums) * kTranslationOffset) / 2.0f, 0.f),
                         glm::vec3(0.f, 1.f, 0.f));

    return Ok<>();
}

Result<> InstancingWorld::shutdown(engine::Engine& engine) {
    return TestWorldBase::shutdown(engine);
}

void InstancingWorld::on_enter(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().add_context(m_input_context.get());
}

void InstancingWorld::on_exit(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().remove_context(m_input_context.get());
}

void InstancingWorld::tick(engine::Engine& engine, const IMilisec tick) {
    // Do entity add/removals from last frame
    m_entity_manager.update(engine);

    NotMyPtr<engine::gameplay::System> systems[] = {
        &m_transform_system,
        &m_graphics_system,
    };
    engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager, systems);

    if (m_debug_draw_physics) {
        engine::graphics::PhysicsDebugDrawer debug_drawer(engine);
        m_physics_world->debug_draw(debug_drawer);
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    graphics_module->debug_drawer().draw_fps(engine, 5, 5, 1.0f, glm::vec3(0.f, 1.f, 0.f));
}

void InstancingWorld::tick_fixed(engine::Engine&, const IMilisec) {}

void InstancingWorld::toggle_debug_draw_physics() {
    m_debug_draw_physics = !m_debug_draw_physics;
}

}    // namespace modus::game
