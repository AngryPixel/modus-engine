/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <test_worlds/test_world_base.hpp>

namespace modus::physics {
class PhysicsWorld;
}

namespace modus::game {

class FlightControlsWorld final : public TestWorldBase {
   private:
    engine::gameplay::EntityId m_ship_id;
    engine::gameplay::EntityId m_camera_entity;
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;

   public:
    Result<> initialize(engine::Engine&) override;

    Result<> shutdown(engine::Engine&) override;

    void tick(engine::Engine& engine, const IMilisec tick) override;

    void tick_fixed(engine::Engine& engine, const IMilisec tick) override;

    void on_enter(engine::Engine&) override;

    void on_exit(engine::Engine&) override;

   private:
    Result<> setup_input(engine::Engine&);
    Result<> setup_camera(engine::Engine&);

    void on_strafe_y(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_strafe_x(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_pitch(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_roll(engine::Engine&, const engine::input::AxisMap& mapping);
    void on_thrust(engine::Engine&, const engine::input::AxisMap& mapping);
};
}    // namespace modus::game
