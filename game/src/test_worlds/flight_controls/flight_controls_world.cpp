/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat off

#include <engine/assets/asset_loader.hpp>
#include <engine/gameplay/ecs/entity_template_asset.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/graphics/debug_drawers/physics_debug_drawer.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <engine/modules/module_physics.hpp>
#include <input/action_binders.hpp>
#include <math/easing_functions.hpp>
#include <modus_game.hpp>
#include <physics/debug_drawer.hpp>
#include <physics/instance.hpp>
#include <physics/world.hpp>
#include <test_worlds/flight_controls/flight_controls_world.hpp>

static constexpr const char* kKinematicAssetPath =
    "game:modus/entity_templates/asteroids/asteroid_kinematic.etpl";

namespace modus::game {

static f32 boost_lerp = 0.0f;
static void reset_lerp(engine::Engine&, const engine::input::AxisMap&) {
    boost_lerp = 0.0f;
}

class FlightCameraControls final : public engine::gameplay::System {
   private:
    engine::gameplay::EntityId m_entity_id;

    bool m_ease_thrust = false;
    bool m_ease_yaw_roll = false;
    bool m_ease_pitch = false;

   public:
    FlightCameraControls() : engine::gameplay::System("FlightControls") {
        MODUS_UNUSED(m_ease_pitch);
        MODUS_UNUSED(m_ease_yaw_roll);
        MODUS_UNUSED(&reset_lerp);
    }
    void set_entity_id(const engine::gameplay::EntityId id) { m_entity_id = id; }

    void on_begin_thrust() { m_ease_thrust = true; }

    void on_end_thrust() { m_ease_thrust = false; }

    void on_begin_pitch(const f32 axis_value) { MODUS_UNUSED(axis_value); }

    void on_end_pitch() {}

    void on_begin_yaw(const f32 axis_value) { MODUS_UNUSED(axis_value); }

    void on_end_yaw() {}

    void update(engine::Engine& engine,
                const IMilisec tick,
                engine::gameplay::GameWorld&,
                engine::gameplay::EntityManager& em) override {
        auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();
        auto tc = em.component<engine::gameplay::TransformComponent>(m_entity_id).value_or_panic();
        auto pc = em.component<engine::physics::PhysicsComponent>(m_entity_id).value_or_panic();
        if (m_ease_thrust) {
            math::Transform t = tc->world_transform();
            t.translate(t.forward() * (-30.f * FSeconds(tick).count()));
            physics_instance.rigid_body_set_transform(pc->m_handle, t).expect();
        }
    }
    void update_threaded(engine::ThreadSafeEngineAccessor&,
                         const IMilisec,
                         engine::gameplay::ThreadSafeGameWorldAccessor&,
                         engine::gameplay::ThreadSafeEntityManagerAccessor&) override {}
};

static FlightCameraControls s_flight_camera_controls;

static void on_thurst_begin(engine::Engine&, const engine::input::AxisMap&) {}
static void on_thurst_end(engine::Engine&, const engine::input::AxisMap&) {}

static void on_pitch_begin(engine::Engine&, const engine::input::AxisMap&) {}
static void on_pitch_end(engine::Engine&, const engine::input::AxisMap&) {}

static void on_yaw_begin(engine::Engine&, const engine::input::AxisMap&) {}
static void on_yaw_end(engine::Engine&, const engine::input::AxisMap&) {}

class BasicInputContext final : public engine::input::InputContext {
   public:
    BasicInputContext() : engine::input::InputContext(500000) {}
    StringSlice name() const override { return "Basic"; }
    bool handle_event(engine::Engine&, const app::InputEvent& event) override {
        if (event.type == app::InputEventType::Keyboard &&
            event.keyboard.key == app::InputKey::KeySpace) {
            if (event.keyboard.is_key_down()) {
                s_flight_camera_controls.on_begin_thrust();
            } else {
                s_flight_camera_controls.on_end_thrust();
            }
        }
        return false;
    }
};

static BasicInputContext sInputContext;

Result<> FlightControlsWorld::initialize(engine::Engine& engine) {
    if (!TestWorldBase::initialize(engine)) {
        return Error<>();
    }

    engine::assets::AsyncLoadParams load_params;
    load_params.path = kKinematicAssetPath;
    load_params.callback = [this](engine::Engine& en,
                                  const engine::assets::AsyncCallbackResult& result) {
        if (!result.asset) {
            en.quit();
        } else {
            auto etpl_asset =
                engine::assets::unsafe_assetv2_cast<const engine::gameplay::EntityTemplateAsset>(
                    result.asset);
            auto r_entity = etpl_asset->spawn(en, this->m_entity_manager);
            if (!r_entity) {
                en.quit();
                MODUS_LOGE("Failed to spawn kinematic asteroid");
                return;
            }

            m_ship_id = *r_entity;
            s_flight_camera_controls.set_entity_id(m_ship_id);

            if (!setup_camera(en)) {
                en.quit();
                MODUS_LOGE("Failed to attach camera");
            }
            auto pc = this->m_entity_manager.component<engine::physics::PhysicsComponent>(*r_entity)
                          .value_or_panic();
            this->m_physics_world->add_rigid_body(pc->m_handle).expect();
            auto& physics_instance = en.module<engine::ModulePhysics>()->instance();
            physics_instance
                .rigid_body_set_linear_velocity(pc->m_handle, glm::vec3(0.f, 0.f, 5000.f))
                .expect();
        }
    };

    // TODO: Kinematic motion state or they wont wake up by themselves
    /*
     *If you are using kinematic bodies, then getWorldTransform is called every simulation step.
This means that your kinematic body's motionstate should have a mechanism to push the current
position of the kinematic body into the motionstate:

class MyKinematicMotionState : public btMotionState {
public:
    MyKinematicMotionState(const btTransform &initialpos) { mPos1 = initialpos; }
    virtual ~ MyKinematicMotionState() { }
    void setNode(Ogre::SceneNode *node) { mVisibleobj = node; }
    virtual void getWorldTransform(btTransform &worldTrans) const { worldTrans = mPos1; }
    void setKinematicPos(btTransform &currentPos) { mPos1 = currentPos; }
    virtual void setWorldTransform(const btTransform &worldTrans) { }

protected:
    btTransform mPos1;
};

To use this, just call setKinematicPos every time that you move your body. This can be combined with
the motionstate in the ogre3d section above if you want, by just adding setKinematicPos to your ogre
motionstate.
     */
    if (!engine.load_startup_asset(load_params)) {
        return Error<>();
    }

    math::Transform transform;
    transform.set_translation(0.f, 0.f, 100.f);
    const ModusGame& game = (static_cast<ModusGame&>(engine.game()));
    if (auto r = game.assets().spawn_asteroid(engine, m_entity_manager, *m_physics_world, 0, 2,
                                              transform, glm::vec3(0.f), glm::vec3(0.f));
        !r) {
        MODUS_LOGE("Failed to spawn asteroid");
        return Error<>();
    }

    if (!setup_input(engine)) {
        return Error<>();
    }

    return Ok<>();
}

Result<> FlightControlsWorld::shutdown(engine::Engine& engine) {
    return TestWorldBase::shutdown(engine);
}

template <typename T>
T lerp(const T& a, const T& b, const f32 tick) {
    return a + (tick * (b - a));
}

void FlightControlsWorld::tick(engine::Engine& engine, const IMilisec) {
    MODUS_UNUSED(engine);

    // m_debug_camera_input_context.update_camera(engine, m_camera);

    // spawn some sphere for reference
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& debug_drawer = graphics_module->debug_drawer();
    /*
    constexpr f32 spacingf = 100.f;
    debug_drawer.draw_sphere(glm::vec3(0.0f), 30.0f, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(spacingf, spacingf, spacingf), 30.0f,
                             glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(-spacingf, spacingf, spacingf), 30.0f,
                             glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(-spacingf, spacingf, -spacingf), 30.0f,
                             glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(spacingf, spacingf, -spacingf), 30.0f,
                             glm::vec4(1.0f, 0.0f, 1.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(spacingf, -spacingf, spacingf), 30.0f,
                             glm::vec4(1.0f, 1.0f, 0.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(-spacingf, -spacingf, spacingf), 30.0f,
                             glm::vec4(0.0f, 1.0f, 1.0f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(-spacingf, -spacingf, -spacingf), 30.0f,
                             glm::vec4(0.5f, 0.5f, .5f, 1.0f), false);
    debug_drawer.draw_sphere(glm::vec3(spacingf, -spacingf, -spacingf), 30.0f,
                             glm::vec4(1.0f, .5f, 1.0f, 1.0f), false);*/

    engine::graphics::PhysicsDebugDrawer physics_debug_drawer(engine);
    m_physics_world->debug_draw(physics_debug_drawer);

    auto tc = m_entity_manager.component<engine::gameplay::TransformComponent>(m_ship_id)
                  .value_or_panic();

    debug_drawer.draw_text_2d(modus::format("P{}", tc->world_transform().translation()), 20, 20,
                              1.0, glm::vec3(1.0));
}

void FlightControlsWorld::tick_fixed(engine::Engine& engine, const IMilisec tick) {
    // Do entity add/removals from last frame
    m_entity_manager.update(engine);

    NotMyPtr<engine::gameplay::System> systems[] = {
        &m_physics_system,
        &s_flight_camera_controls,
        &m_transform_system,
        &m_graphics_system,
    };
    engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager, systems);

    // Update camera
    {
        auto r_tc_ship =
            m_entity_manager.component<engine::gameplay::TransformComponent>(m_ship_id);
        auto r_tc_camera =
            m_entity_manager.component<engine::gameplay::TransformComponent>(m_camera_entity);
        if (r_tc_ship && r_tc_camera) {
            engine::gameplay::TransformComponent& tc_ship = *(*r_tc_ship);
            engine::gameplay::TransformComponent& tc_camera = *(*r_tc_camera);
            MODUS_UNUSED(tc_camera);
            MODUS_UNUSED(tc_ship);
            camera().set_look_at(
                tc_camera.world_transform().translation(),
                tc_ship.world_transform().translation() +
                    (tc_ship.world_transform().rotation() * glm::vec3(0., 0, 50.0f)),
                tc_ship.world_transform().rotation() * glm::vec3(0.f, 1.0f, 0.f));
            // engine.modules().graphics().debug_drawer().draw_cube(
            //    tc_ship.m_world.translation() +
            //        (tc_ship.world_transform().rotation() * glm::vec3(0.,
            //        0, 50.f -tc_camera.local_transform().translation().z)),
            //        glm::vec3(1.), glm::vec4(1.f), false);
        }
    }
}

void FlightControlsWorld::on_enter(engine::Engine& engine) {
    TestWorldBase::on_enter(engine);
    engine.module<engine::ModuleInput>()->manager().add_context(&sInputContext);
}
void FlightControlsWorld::on_exit(engine::Engine& engine) {
    engine.module<engine::ModuleInput>()->manager().remove_context(&sInputContext);
    TestWorldBase::on_exit(engine);
}

Result<> FlightControlsWorld::setup_camera(engine::Engine&) {
    auto r_entity = m_entity_manager.create();
    if (!r_entity) {
        return Error<>();
    }
    m_camera_entity = *r_entity;

    auto r_component =
        m_entity_manager.add_component<engine::gameplay::TransformComponent>(*r_entity);
    if (!r_component) {
        return Error<>();
    }

    engine::gameplay::TransformComponent& tc = *(*r_component);

    if (!tc.attach(m_entity_manager, m_ship_id)) {
        return Error<>();
    }

    tc.set_translation(glm::vec3(0.f, 40.f, -40.f));

    return Ok<>();
}

Result<> FlightControlsWorld::setup_input(engine::Engine&) {
    // Placeholder
    GameplayCallbacks callbacks;

    callbacks.on_yaw =
        engine::input::AxisMap::Callback::build<FlightControlsWorld,
                                                &FlightControlsWorld::on_strafe_x>(this);
    callbacks.on_yaw_begin = engine::input::AxisMap::Callback::build<on_yaw_begin>();
    callbacks.on_yaw_end = engine::input::AxisMap::Callback::build<on_yaw_end>();

    callbacks.on_roll =
        engine::input::AxisMap::Callback::build<FlightControlsWorld, &FlightControlsWorld::on_roll>(
            this);

    callbacks.on_pitch =
        engine::input::AxisMap::Callback::build<FlightControlsWorld,
                                                &FlightControlsWorld::on_pitch>(this);
    callbacks.on_pitch_begin = engine::input::AxisMap::Callback::build<on_pitch_begin>();
    callbacks.on_pitch_end = engine::input::AxisMap::Callback::build<on_pitch_end>();

    callbacks.on_vertical =
        engine::input::AxisMap::Callback::build<FlightControlsWorld,
                                                &FlightControlsWorld::on_strafe_y>(this);

    callbacks.on_thrust =
        engine::input::AxisMap::Callback::build<FlightControlsWorld,
                                                &FlightControlsWorld::on_thrust>(this);

    callbacks.on_thrust_begin = engine::input::AxisMap::Callback::build<on_thurst_begin>();
    callbacks.on_thrust_end = engine::input::AxisMap::Callback::build<on_thurst_end>();

    return Ok<>();
}

void FlightControlsWorld::on_strafe_y(engine::Engine& engine,
                                      const engine::input::AxisMap& mapping) {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(mapping);
}
void FlightControlsWorld::on_strafe_x(engine::Engine&, const engine::input::AxisMap& mapping) {
    MODUS_UNUSED(mapping);
}
void FlightControlsWorld::on_pitch(engine::Engine&, const engine::input::AxisMap& mapping) {
    MODUS_UNUSED(mapping);
}
void FlightControlsWorld::on_roll(engine::Engine&, const engine::input::AxisMap& mapping) {
    MODUS_UNUSED(mapping);
}
void FlightControlsWorld::on_thrust(engine::Engine& engine, const engine::input::AxisMap& mapping) {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(mapping);
}

}    // namespace modus::game
