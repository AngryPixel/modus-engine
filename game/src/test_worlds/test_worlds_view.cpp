/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/modules/module_gameplay.hpp>
#include <test_worlds/audio/audio_world.hpp>
#include <test_worlds/flight_controls/flight_controls_world.hpp>
#include <test_worlds/instancing/instancing_world.hpp>
#include <test_worlds/model_viewer/model_viewer_world.hpp>
#include <test_worlds/test_world_base.hpp>
#include <test_worlds/test_worlds_view.hpp>

namespace modus::game {

Result<std::unique_ptr<engine::gameplay::GameWorld>> create_test_world(StringSlice name) {
    using OkType = Ok<std::unique_ptr<engine::gameplay::GameWorld>>;
    if (name == "model_viewer") {
        return OkType(modus::make_unique<ModelViewerWorld>());

    } else if (name == "flight_controls") {
        return OkType(modus::make_unique<FlightControlsWorld>());
    } else if (name == "instancing") {
        return OkType(modus::make_unique<InstancingWorld>());
    } else if (name == "audio") {
        return OkType(modus::make_unique<AudioWorld>());
    }
    return Error<>();
}
}    // namespace modus::game
