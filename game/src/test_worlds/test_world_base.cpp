/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_event.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <engine/modules/module_physics.hpp>
#include <gameplay/modus_world.hpp>
#include <modus_game.hpp>
#include <physics/world.hpp>
#include <test_worlds/test_world_base.hpp>

namespace modus::game {

TestWorldBase::TestWorldBase() {}

TestWorldBase::~TestWorldBase() {}

Result<> TestWorldBase::initialize(engine::Engine& engine) {
    if (!ModusWorld::register_components(m_entity_manager)) {
        return Error<>();
    }
    if (!initialize_command_queue()) {
        MODUS_LOGE("Failed to initialize command queue");
        return Error<>();
    }

    if (!m_entity_manager.initialize(engine, 1024)) {
        MODUS_LOGE("TestWorldBase: Failed to initialize entity manager");
        return Error<>();
    }

    modus::physics::WorldCreateParams world_params;
    world_params.gravity = glm::vec3(0.0f);
    // world_params.callback = on_collision;

    auto physics_module = engine.module<engine::ModulePhysics>();
    m_physics_world = physics_module->create_world();
    if (!m_physics_world || !m_physics_world->initialize(world_params)) {
        MODUS_LOGE("Failed to initialize physics world ");
        return Error<>();
    }
    set_storage<modus::physics::PhysicsWorld>(m_physics_world.get());

    auto app_module = engine.module<engine::ModuleApp>();
    auto event_module = engine.module<engine::ModuleEvent>();
    m_window_resize_listener =
        event_module->manager()
            .register_event_listener(
                app_module->window_resize_event_handle(),
                engine::event::EventListener::build<TestWorldBase,
                                                    &TestWorldBase::on_window_resize>(this))
            .value_or_panic("Failed to register event handler");

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto window = graphics_module->window();
    const app::WindowState window_state = window->window_state();

    // Create camera
    {
        m_camera_id = m_entity_manager.create("Camera").value_or_panic("failed to create camera");

        auto r_cc = m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
        if (!r_cc) {
            return Error<>();
        }
        (*r_cc)->m_camera.frustum().set_aspect_ratio(f32(window_state.width) /
                                                     f32(window_state.height));
        (*r_cc)->m_camera.frustum().set_near_far(0.1f, 10000.0f);
    }

    if (auto r_entity = m_entity_manager.create("GlobalLight"); !r_entity) {
        MODUS_LOGE("Failed to create light entity");
        return Error<>();
    } else {
        if (auto r_lc = m_entity_manager.add_component<engine::graphics::LightComponent>(*r_entity);
            r_lc) {
            auto& light = (*r_lc)->light;
            light.set_type(engine::graphics::LightType::Directional);
            light.set_position(glm::vec3(1000.f, 0, 1000.f));
            light.set_diffuse_intensity(0.5f);
            (*r_lc)->enabled = true;
            light.set_directional_light_area(4000.0f, 4000.0f);
            light.set_directional_light_near_far(0.1f, 10000.0f);
            light.set_emits_shadow(true);
        } else {
            MODUS_LOGE("Failed to create light component");
            return Error<>();
        }
    }

    if (auto r_ambient_light = m_entity_manager.create("AmbientLight"); !r_ambient_light) {
        return Error<>();
    } else {
        if (auto r_lc =
                m_entity_manager.add_component<engine::graphics::LightComponent>(*r_ambient_light);
            r_lc) {
            auto& light = (*r_lc)->light;
            light.set_type(engine::graphics::LightType::Ambient);
            light.set_diffuse_color(glm::vec3(1.f));
            light.set_diffuse_intensity(0.8f);
            (*r_lc)->enabled = true;
        }
    }
    return Ok<>();
}

Result<> TestWorldBase::shutdown(engine::Engine& engine) {
    if (!m_entity_manager.shutdown(engine)) {
        MODUS_LOGE("TestWorldBase: Failed to shutdown entity manager");
    }

    reset_storage<modus::physics::PhysicsWorld>();
    if (m_physics_world) {
        m_physics_world.reset();
    }

    auto app_module = engine.module<engine::ModuleApp>();
    auto event_module = engine.module<engine::ModuleEvent>();
    if (!event_module->manager().unregister_event_listner(app_module->window_resize_event_handle(),
                                                          m_window_resize_listener)) {
        MODUS_LOGE(
            "TestWorldBase: Failed to unregister window resize event "
            "handler");
    }
    m_window_resize_listener = engine::event::ListenerHandle();

    return Ok<>();
}

void TestWorldBase::on_enter(engine::Engine&) {}

void TestWorldBase::on_exit(engine::Engine&) {}

engine::gameplay::EntityId TestWorldBase::main_camera() const {
    return m_camera_id;
}

engine::graphics::Camera& TestWorldBase::camera() {
    return m_entity_manager.component<engine::graphics::CameraComponent>(m_camera_id)
        .value_or_panic("Failed to get camera")
        ->m_camera;
}

void TestWorldBase::on_window_resize(engine::Engine& engine, engine::event::Event& event) {
    MODUS_UNUSED(engine);
    const engine::AppWindowResizeEvent& window_resize_event =
        static_cast<const engine::AppWindowResizeEvent&>(event);
    camera().frustum().set_aspect_ratio(f32(window_resize_event.m_width) /
                                        f32(window_resize_event.m_height));
}

void TestWorldBase::push_debug_camera_context(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    auto& input_manager = input_module->manager();
    input_manager.add_context(&m_debug_camera_input_context);
}

void TestWorldBase::pop_debug_camera_context(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    auto& input_manager = input_module->manager();
    input_manager.remove_context(&m_debug_camera_input_context);
}

}    // namespace modus::game
