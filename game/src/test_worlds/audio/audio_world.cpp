/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat off
#include <app/app.hpp>
#include <audio/audio_device.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/graphics/debug_drawers/physics_debug_drawer.hpp>
#include <engine/input/default_input_priorities.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_audio.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_input.hpp>
#include <modus_game.hpp>
#include <test_worlds/audio/audio_world.hpp>

namespace modus::game {

class AudioWorldInputContext final : public engine::input::InputContext {
   private:
    AudioWorld& m_world;

   public:
    AudioWorldInputContext(AudioWorld& world)
        : engine::input::InputContext(engine::input::kGameInputPriorityMin), m_world(world) {}

    StringSlice name() const override { return "ModelViewerInputContext"; }

    bool handle_event(engine::Engine& engine, const app::InputEvent& event) override {
        MODUS_UNUSED(m_world);
        if (event.type == app::InputEventType::Keyboard) {
            auto input_module = engine.module<engine::ModuleInput>();
            if (input_module->manager().input_state().is_key_down(app::InputKey::KeyLeftCtrl) &&
                event.keyboard.key == app::InputKey::KeyEscape && event.keyboard.is_key_down()) {
                auto app_module = engine.module<engine::ModuleApp>();
                app_module->app().lock_mouse(!app_module->app().is_mouse_locked());
                return true;
            }
        }
        return false;
    }
};

AudioWorld::AudioWorld()
    : m_input_context(modus::make_unique<AudioWorldInputContext>(*this)), m_audio_system(false) {}

AudioWorld::~AudioWorld() {}

static constexpr f32 kSoundRadius = 5.f;
static constexpr f32 kGain = 0.8f;
static const glm::vec3 kPos1 = glm::vec3(20);
static const glm::vec3 kPos2 = glm::vec3(-20);
static const glm::vec3 kPos3 = glm::vec3(0, 0, -40);
Result<> AudioWorld::initialize(engine::Engine& engine) {
    if (!TestWorldBase::initialize(engine)) {
        return Error<>();
    }

    ModusGame& game = static_cast<ModusGame&>(engine.game());
    MODUS_UNUSED(game);

    if (!m_audio_system.initialize(engine, *this, m_entity_manager)) {
        return Error<>();
    }

    if (!game.assets().load_sounds(engine)) {
        return Error<>();
    }

    auto& audio_context = engine.module<engine::ModuleAudio>()->context();
    // Create Listener
    if (auto r = m_entity_manager.create("Listener"); !r) {
        MODUS_LOGE("Failed to create listener entity");
        return Error<>();
    } else {
        m_listener_id = *r;
        m_entity_manager.add_component<engine::audio::AudioListenerComponent>(m_listener_id)
            .expect("Failed to add component");
        m_entity_manager.add_component<engine::gameplay::TransformComponent>(m_listener_id)
            .expect("Failed to add component");
        audio_context.set_listener_gain(1.0f).expect("Failed to set listener gain");
    }

    const ModusAssets::AudioAssets& audio_assets = game.assets().audio_assets();

    auto create_audio_source = [this, &audio_context](
                                   const modus::audio::SoundHandle sound, const glm::vec3& position,
                                   const Optional<f32> distance = Optional<f32>()) {
        modus::audio::AudioSourceCreateParams source_params;
        source_params.gain = kGain;
        source_params.position = position;
        source_params.looping = true;
        source_params.direction = glm::vec3(0.f);
        if (distance) {
            source_params.max_distance = distance.value();
        }
        auto audio_source_handle = audio_context.create_source(source_params)
                                       .value_or_panic("Failed to create audio source");
        audio_context.set_source_sound(audio_source_handle, sound)
            .expect("Failed to set source data");
        audio_context.play(audio_source_handle).expect("Failed to play audio source");

        auto entity = m_entity_manager.create().value_or_panic("Failed to create entity");
        auto transform =
            m_entity_manager.add_component<engine::gameplay::TransformComponent>(entity)
                .value_or_panic("Failed to create transform component");
        auto audio_source =
            m_entity_manager.add_component<engine::audio::AudioSourceComponent>(entity)
                .value_or_panic("Failed to create audio source component");

        transform->set_translation(position);
        audio_source->audio_source = audio_source_handle;

        return entity;
    };

    // Create sound sources
    // create_audio_source(audio_assets.ambient.main, glm::vec3(0.f), 5.f);
    m_audio_entities.push_back(create_audio_source(audio_assets.ship.laser1, kPos1, kSoundRadius));
    m_audio_entities.push_back(create_audio_source(audio_assets.ship.laser2, kPos2, kSoundRadius));
    m_audio_entities.push_back(create_audio_source(audio_assets.ship.alert, kPos3, kSoundRadius));
    return Ok<>();
}

Result<> AudioWorld::shutdown(engine::Engine& engine) {
    for (auto& entity_id : m_audio_entities) {
        m_entity_manager.destroy(entity_id);
    }
    m_entity_manager.update(engine);
    ModusGame& game = static_cast<ModusGame&>(engine.game());
    if (!game.assets().destroy_sounds(engine)) {
        MODUS_LOGE("Failed to destroy sounds");
    }
    return TestWorldBase::shutdown(engine);
}

void AudioWorld::on_enter(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().add_context(m_input_context.get());
}

void AudioWorld::on_exit(engine::Engine& engine) {
    auto input_module = engine.module<engine::ModuleInput>();
    input_module->manager().remove_context(m_input_context.get());
}

void AudioWorld::tick(engine::Engine& engine, const IMilisec tick) {
    // Update audio stream

    // Do entity add/removals from last frame
    m_entity_manager.update(engine);

    // Update camera
    math::Transform camera_transform;
    m_debug_camera_input_context.update_camera(engine, camera_transform);
    camera().set_transform(camera_transform);

    // Update listener
    auto transform = m_entity_manager.component<engine::gameplay::TransformComponent>(m_listener_id)
                         .value_or_panic("Failed to retreive transform component");
    transform->set_transform(camera().to_world_transform());

    NotMyPtr<engine::gameplay::System> systems[] = {
        &m_transform_system,
        &m_audio_system,
        &m_graphics_system,
    };
    engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager, systems);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& debug_drawer = graphics_module->debug_drawer();
    debug_drawer.draw_sphere(kPos1, kSoundRadius, glm::vec4(1.f), true);
    debug_drawer.draw_sphere(kPos2, kSoundRadius, glm::vec4(1.f), true);
    debug_drawer.draw_sphere(kPos3, kSoundRadius, glm::vec4(1.f), true);
}

void AudioWorld::tick_fixed(engine::Engine&, const IMilisec) {}

}    // namespace modus::game
