/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/audio/ecs/systems/audio_system.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <test_worlds/test_world_base.hpp>

namespace modus::audio {
class AudioContext;
}

namespace modus::game {

class AudioWorldInputContext;

class AudioWorld final : public TestWorldBase {
   private:
    std::unique_ptr<AudioWorldInputContext> m_input_context;
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::audio::AudioSystem m_audio_system;
    engine::gameplay::EntityId m_listener_id;
    Vector<engine::gameplay::EntityId> m_audio_entities;

   public:
    AudioWorld();

    ~AudioWorld();

    Result<> initialize(engine::Engine&) override;

    Result<> shutdown(engine::Engine&) override;

    void on_enter(engine::Engine&) override;

    void on_exit(engine::Engine&) override;

    void tick(engine::Engine& engine, const IMilisec tick) override;

    void tick_fixed(engine::Engine& engine, const IMilisec tick) override;
};
}    // namespace modus::game
