/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/event/engine_events.hpp>
#include <engine/gameplay/debug/debug_camera_input_context.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/physics/ecs/components/physics_component.hpp>
#include <engine/physics/ecs/systems/physics_system.hpp>

namespace modus::physics {
class PhysicsWorld;
}

namespace modus::game {

class TestWorldBase : public engine::gameplay::GameWorld {
   protected:
    engine::gameplay::EntityId m_camera_id;
    engine::DebugCameraInputContext m_debug_camera_input_context;
    engine::event::ListenerHandle m_window_resize_listener;
    std::unique_ptr<modus::physics::PhysicsWorld> m_physics_world;
    engine::physics::PhysicsSystem m_physics_system;

   public:
    TestWorldBase();

    ~TestWorldBase();

    Result<> initialize(engine::Engine&) override;

    Result<> shutdown(engine::Engine&) override;

    void on_enter(engine::Engine&) override;

    void on_exit(engine::Engine&) override;

    engine::gameplay::EntityId main_camera() const override;

   protected:
    engine::graphics::Camera& camera();
    void on_window_resize(engine::Engine& engine, engine::event::Event& event);

    void push_debug_camera_context(engine::Engine& engine);

    void pop_debug_camera_context(engine::Engine& engine);
};
}    // namespace modus::game
