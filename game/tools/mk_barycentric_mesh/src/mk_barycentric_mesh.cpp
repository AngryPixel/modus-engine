/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <engine/engine_pch.h>
#include <os/os_pch.h>
// clang-format on
#include <core/getopt.hpp>
#include <core/io/byte_stream.hpp>
#include <core/io/memory_stream.hpp>
#include <engine/graphics/image.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/guid_gen.hpp>
#include <math/packing.hpp>

#include <engine/graphics/mesh_generated.h>

using namespace modus;
using namespace modus::engine::graphics;

static Result<fbs::MeshT, void> load_mesh(const char* path) {
    auto r_file = os::FileBuilder().read().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", path);
        return Error<>();
    }

    RamStream<> ram_stream;
    auto r_mem_read = ram_stream.populate_from_stream(*r_file);
    if (!r_mem_read || r_mem_read.value() != r_file->size()) {
        std::cerr << fmt::format("Failed to read {} into memory\n", path);
        return Error<>();
    }

    const ByteSlice bytes = ram_stream.as_bytes();
    flatbuffers::Verifier v(bytes.data(), bytes.size());
    if (!fbs::VerifyMeshBuffer(v)) {
        std::cerr << fmt::format("File {} is not a mesh!\n", path);
        return Error<>();
    }

    const fbs::Mesh* fbs_mesh = fbs::GetMesh(bytes.data());
    const auto* fbs_lods = fbs_mesh->submeshes();
    if (fbs_lods == nullptr || fbs_lods->size() == 0) {
        std::cerr << fmt::format("No Submeshes specified in mesh {}\n", path);
        return Error<>();
    }

    if (fbs_lods->size() > 1) {
        std::cerr << "Mesh already contains more than one submesh levels, this is "
                     "not supported\n";
        return Error<>();
    }

    const auto fbs_data_buffers = fbs_mesh->data_buffers();
    const auto fbs_index_buffers = fbs_mesh->index_buffers();
    if (fbs_data_buffers == nullptr || fbs_data_buffers->size() > 1 ||
        (fbs_index_buffers != nullptr && fbs_index_buffers->size() > 1)) {
        std::cerr << fmt::format(
            "Barycentric creator only works on meshes with one index and data "
            "buffer. "
            "Mesh:{} does not meet this requirement\n",
            path);
        return Error<>();
    }
    fbs::MeshT mesh;
    fbs_mesh->UnPackTo(&mesh);
    return Ok<fbs::MeshT>(std::move(mesh));
}

Result<> process_mesh(fbs::MeshT& mesh, const StringSlice output_path) {
    // const auto fbs_data_buffer = mesh.data->data_buffers()->Get(0)->data();
    const auto& fbs_submesh = mesh.submeshes[0];
    const bool has_index_buffer = fbs_submesh->index_type != fbs::IndexType::None;
    const u32 stride = fbs_submesh->data_buffers[0].stride();

    if (fbs_submesh->primitive_type != fbs::PrimitiveType::Triangles) {
        std::cerr << "This tool currently only knows how to handle Triange list\n";
        return Error<>();
    }

    if (fbs_submesh->custom4) {
        std::cerr << "Mesh attribute custom 4 must be empty.\n";
        return Error<>();
    }

    std::vector<u8> new_data;
    if (has_index_buffer) {
        const auto& vertex_data = mesh.data_buffers[0]->data;
        const auto& index_data = mesh.index_buffers[0]->data;

        if (index_data.size() % 3 != 0) {
            std::cerr << "Index buffer's size is not a multiple of 3\n";
            return Error<>();
        }

        new_data.resize(stride * fbs_submesh->draw_count);
        const u8* index_ptr = &index_data[0];
        auto get_index = [](const u8*& data, const fbs::IndexType type) -> u32 {
            u32 index = 0;
            if (type == fbs::IndexType::U8) {
                index = *data;
                data++;
            } else if (type == fbs::IndexType::U16) {
                index = *(reinterpret_cast<const u16*>(data));
                data += 2;
            } else if (type == fbs::IndexType::U32) {
                index = *(reinterpret_cast<const u32*>(data));
                data += 4;
            } else {
                modus_panic("Unhandled index type");
            }
            return index;
        };

        for (usize i = 0; i < fbs_submesh->draw_count; i++) {
            const u32 index = get_index(index_ptr, fbs_submesh->index_type);
            u8* new_data_ptr = &new_data[stride * index];
            const u8* data_ptr = &vertex_data[stride * index];
            memcpy(new_data_ptr, data_ptr, stride);
        }

        // Disable indices
        fbs_submesh->index_type = fbs::IndexType::None;
        mesh.index_buffers.clear();

        // Replace buffer data
        mesh.data_buffers[0]->data = new_data;
    }

    // Write out end result
    auto r_file = os::FileBuilder().create().write().open(output_path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", output_path);
        return Error<>();
    }

    flatbuffers::FlatBufferBuilder fbs_builder;
    fbs_builder.Finish(fbs::Mesh::Pack(fbs_builder, &mesh), "MESH");

    const ByteSlice builder_slice =
        ByteSlice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = r_file->write_exactly(builder_slice);
    if (!r_write) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", output_path);
        return Error<>();
    }

    return Ok<>();
}

static const StringSlice kIntro =
    "Process an existing mesh and turn it into a non-indexed triange list "
    "with barycentric coordinates.\n"
    "modus_mk_barycentric_mesh -o <output> <input>";
int main(const int argc, const char** argv) {
    // NOTE: This tool assumes that all the meshes are written out with one
    // data buffer and one index buffer. Meshes with submeshes should be
    // prepared so that all the submeshes share the same buffer.
    CmdOptionString opt_output("-o", "--output", "Path for the generated output", "");
    CmdOptionParser opt_parser;
    opt_parser.add(opt_output).expect("Failed to add cmd parser option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (!opt_output.parsed()) {
        std::cerr << fmt::format("No output file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_result->size() == 0) {
        std::cerr << "No input file specified\n";
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    auto r_mesh = load_mesh(opt_result->at(0));
    if (!r_mesh) {
        return EXIT_FAILURE;
    }

    auto r_process = process_mesh(*r_mesh, opt_output.value());
    if (!r_process) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
