/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <engine/engine_pch.h>
#include <os/os_pch.h>
// clang-format on
#include <core/getopt.hpp>
#include <core/io/byte_stream.hpp>
#include <core/io/memory_stream.hpp>
#include <engine/graphics/image.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/guid_gen.hpp>
#include <math/packing.hpp>
#include <math/bounding_volumes.hpp>
#include <math/transform.hpp>

#include <engine/graphics/mesh_generated.h>

using namespace modus;
using namespace modus::engine::graphics;

struct alignas(4) VertexData {
    glm::vec3 pos;
    u32 norm;
};

struct alignas(4) VertexDataWithBones {
    glm::vec3 pos;
    u32 norm;
    glm::u8vec4 bone_indices;
    glm::u8vec4 bone_weights;
};

Result<> generate_mesh(const StringSlice output, Slice<VertexData> data) {
    static constexpr const u32 kStride = 16;
    static constexpr const u32 kNormOffset = 12;
    static constexpr const u32 kBufferIndex = 0;

    // Generate guid
    auto r_guid = os::guid::generate();
    if (!r_guid) {
        std::cerr << "Failed to generate guid\n";
        return Error<>();
    }

    // generate sphere bounding volume
    math::bv::Sphere sphere;
    math::bv::AABB aabb;
    for (const auto& d : data) {
        math::bv::merge(sphere, d.pos);
        math::bv::merge(aabb, d.pos);
    }

    flatbuffers::FlatBufferBuilder fbs_builder;

    // Data Buffers
    const auto data_bytes = data.as_bytes();
    auto fbs_data = fbs_builder.CreateVector<u8>(data_bytes.data(), data_bytes.size());
    auto fbs_data_buffer = fbs::CreateBuffer(fbs_builder, fbs_data);
    auto fbs_data_vector = fbs_builder.CreateVector(&fbs_data_buffer, 1);

    fbs::DrawableDataBufferDesc data_buffer_desc;
    data_buffer_desc.mutate_offset(0);
    data_buffer_desc.mutate_stride(kStride);
    data_buffer_desc.mutate_data_buffer_index(0);
    auto fbs_data_buffer_desc_vec = fbs_builder.CreateVectorOfStructs(&data_buffer_desc, 1);

    // Drawables desc
    fbs::DrawableDesc vertex_info;
    vertex_info.mutate_offset(0);
    vertex_info.mutate_data_type(fbs::DataType::Vec3F32);
    vertex_info.mutate_data_buffer_index(kBufferIndex);

    fbs::DrawableDesc normal_info;
    normal_info.mutate_offset(kNormOffset);
    normal_info.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
    normal_info.mutate_normalize(true);
    normal_info.mutate_data_buffer_index(kBufferIndex);

    // Drawable
    fbs::DrawableBuilder drawable_builder(fbs_builder);
    drawable_builder.add_data_buffers(fbs_data_buffer_desc_vec);
    drawable_builder.add_draw_count(data.size());
    drawable_builder.add_draw_start(0);
    drawable_builder.add_index_type(fbs::IndexType::None);
    drawable_builder.add_primitive_type(fbs::PrimitiveType::Triangles);
    drawable_builder.add_vertex(&vertex_info);
    drawable_builder.add_normals(&normal_info);

    auto fbs_drawable = drawable_builder.Finish();
    auto fbs_drawable_vec = fbs_builder.CreateVector(&fbs_drawable, 1);

    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = r_guid->as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    // Create lod
    math::fbs::Vec3 sphere_center(sphere.center.x, sphere.center.y, sphere.center.z);
    math::fbs::Vec3 aabb_center(aabb.center.x, aabb.center.y, aabb.center.z);
    math::fbs::Vec3 aabb_extend(aabb.extends.x, aabb.extends.y, aabb.extends.z);
    fbs::BoundingVolumeSphere fbs_bv_sphere(sphere_center, sphere.radius);
    fbs::BoundingVolumeAABB fbs_bv_aabb(aabb_center, aabb_extend);
    auto fbs_bv = fbs::BoundingVolume(fbs_bv_sphere, fbs_bv_aabb);

    fbs::MeshBuilder mesh_builder(fbs_builder);
    mesh_builder.add_guid(&fbs_guid);
    mesh_builder.add_bounding_volume(&fbs_bv);
    mesh_builder.add_data_buffers(fbs_data_vector);
    mesh_builder.add_submeshes(fbs_drawable_vec);
    auto fbs_mesh = mesh_builder.Finish();

    fbs_builder.Finish(fbs_mesh, "MESH");

    // Write out end result
    auto r_file = os::FileBuilder().create().write().open(output);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", output);
        return Error<>();
    }

    const ByteSlice builder_slice =
        ByteSlice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = r_file->write_exactly(builder_slice);
    if (!r_write) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", output);
        return Error<>();
    }

    return Ok<>();
}

Result<> generate_mesh(const StringSlice output,
                       Slice<VertexDataWithBones> data,
                       const engine::animation::fbs::AnimationCatalogT& catalog) {
    static constexpr const u32 kStride = 24;
    static constexpr const u32 kNormOffset = 12;
    static constexpr const u32 kBoneIdxOffset = 16;
    static constexpr const u32 kBoneWOffset = 20;
    static constexpr const u32 kBufferIndex = 0;

    // Generate guid
    auto r_guid = os::guid::generate();
    if (!r_guid) {
        std::cerr << "Failed to generate guid\n";
        return Error<>();
    }

    // generate sphere bounding volume
    math::bv::Sphere sphere;
    math::bv::AABB aabb;
    for (const auto& d : data) {
        math::bv::merge(sphere, d.pos);
        math::bv::merge(aabb, d.pos);
    }

    flatbuffers::FlatBufferBuilder fbs_builder;

    // Data Buffers
    const auto data_bytes = data.as_bytes();
    auto fbs_data = fbs_builder.CreateVector<u8>(data_bytes.data(), data_bytes.size());
    auto fbs_data_buffer = fbs::CreateBuffer(fbs_builder, fbs_data);
    auto fbs_data_vector = fbs_builder.CreateVector(&fbs_data_buffer, 1);

    fbs::DrawableDataBufferDesc data_buffer_desc;
    data_buffer_desc.mutate_offset(0);
    data_buffer_desc.mutate_stride(kStride);
    data_buffer_desc.mutate_data_buffer_index(0);
    auto fbs_data_buffer_desc_vec = fbs_builder.CreateVectorOfStructs(&data_buffer_desc, 1);

    // Drawables desc
    fbs::DrawableDesc vertex_info;
    vertex_info.mutate_offset(0);
    vertex_info.mutate_data_type(fbs::DataType::Vec3F32);
    vertex_info.mutate_data_buffer_index(kBufferIndex);

    fbs::DrawableDesc normal_info;
    normal_info.mutate_offset(kNormOffset);
    normal_info.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
    normal_info.mutate_normalize(true);
    normal_info.mutate_data_buffer_index(kBufferIndex);

    fbs::DrawableDesc bone_idx_info;
    bone_idx_info.mutate_offset(kBoneIdxOffset);
    bone_idx_info.mutate_data_type(fbs::DataType::Vec4U8);
    bone_idx_info.mutate_data_buffer_index(kBufferIndex);

    fbs::DrawableDesc bone_w_info;
    bone_w_info.mutate_offset(kBoneWOffset);
    bone_w_info.mutate_data_type(fbs::DataType::Vec4U8);
    bone_w_info.mutate_normalize(true);
    bone_w_info.mutate_data_buffer_index(kBufferIndex);

    // Drawable
    fbs::DrawableBuilder drawable_builder(fbs_builder);
    drawable_builder.add_data_buffers(fbs_data_buffer_desc_vec);
    drawable_builder.add_draw_count(data.size());
    drawable_builder.add_draw_start(0);
    drawable_builder.add_index_type(fbs::IndexType::None);
    drawable_builder.add_primitive_type(fbs::PrimitiveType::Triangles);
    drawable_builder.add_vertex(&vertex_info);
    drawable_builder.add_normals(&normal_info);
    drawable_builder.add_bone_indices(&bone_idx_info);
    drawable_builder.add_bone_weights(&bone_w_info);

    auto fbs_drawable = drawable_builder.Finish();
    auto fbs_drawable_vec = fbs_builder.CreateVector(&fbs_drawable, 1);

    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = r_guid->as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    // Create lod
    math::fbs::Vec3 sphere_center(sphere.center.x, sphere.center.y, sphere.center.z);
    math::fbs::Vec3 aabb_center(aabb.center.x, aabb.center.y, aabb.center.z);
    math::fbs::Vec3 aabb_extend(aabb.extends.x, aabb.extends.y, aabb.extends.z);
    fbs::BoundingVolumeSphere fbs_bv_sphere(sphere_center, sphere.radius);
    fbs::BoundingVolumeAABB fbs_bv_aabb(aabb_center, aabb_extend);
    auto fbs_bv = fbs::BoundingVolume(fbs_bv_sphere, fbs_bv_aabb);

    auto fbs_catalog = engine::animation::fbs::AnimationCatalog::Pack(fbs_builder, &catalog);

    fbs::MeshBuilder mesh_builder(fbs_builder);
    mesh_builder.add_guid(&fbs_guid);
    mesh_builder.add_bounding_volume(&fbs_bv);
    mesh_builder.add_data_buffers(fbs_data_vector);
    mesh_builder.add_submeshes(fbs_drawable_vec);
    mesh_builder.add_animation_catalog(fbs_catalog);
    auto fbs_mesh = mesh_builder.Finish();

    fbs_builder.Finish(fbs_mesh, "MESH");

    // Write out end result
    auto r_file = os::FileBuilder().create().write().open(output);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", output);
        return Error<>();
    }

    const ByteSlice builder_slice =
        ByteSlice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = r_file->write_exactly(builder_slice);
    if (!r_write) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", output);
        return Error<>();
    }

    return Ok<>();
}

template <class T>
Result<> gen_normals(SliceMut<T> data) {
    if (data.size() % 3 != 0) {
        return Error<>();
    }
    for (usize i = 0; i < data.size(); i += 3) {
        T& v1 = data[i];
        T& v2 = data[i + 1];
        T& v3 = data[i + 2];

        const glm::vec3 normal = glm::normalize(glm::cross(v2.pos - v1.pos, v3.pos - v1.pos));
        const u32 normal_packed = math::pack_snorm_1010102_rev(glm::vec4(normal, 0.f));
        v1.norm = normal_packed;
        v2.norm = normal_packed;
        v3.norm = normal_packed;
    }
    return Ok<>();
}

std::unique_ptr<math::fbs::Transform> to_fbs_transform(const math::Transform& transform) {
    auto t = modus::make_unique<math::fbs::Transform>();

    const auto& pos = transform.translation();
    const auto& scale = transform.scale();
    const auto& rot = transform.rotation();
    t->mutable_position().mutate_x(pos.x);
    t->mutable_position().mutate_y(pos.y);
    t->mutable_position().mutate_z(pos.z);
    t->mutable_rotation().mutate_w(rot.w);
    t->mutable_rotation().mutate_x(rot.x);
    t->mutable_rotation().mutate_y(rot.y);
    t->mutable_rotation().mutate_z(rot.z);
    t->mutate_scale(scale);
    return t;
}

void gen_ship_shoot_anim(engine::animation::fbs::AnimationCatalogT& catalog,
                         const glm::vec3& engine_offset) {
    auto animation = modus::make_unique<engine::animation::fbs::SkeletonAnimationT>();
    animation->name = "Shoot";
    animation->duration_sec = 0.25f;
    animation->skeleton_index = 0;

    auto bone_keyframe = modus::make_unique<engine::animation::fbs::BoneKeyframeT>();
    bone_keyframe->bone_index = 1;

    {
        // Bone Transform 1
        math::Transform t;
        t.set_translation(engine_offset);
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.0f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    {
        // Bone Transform 2
        math::Transform t;
        t.set_translation(engine_offset * 0.75f);
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.125f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    {
        // Bone Transform 2
        math::Transform t;
        t.set_translation(engine_offset);
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.5f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    animation->bone_keyframes.push_back(std::move(bone_keyframe));
    catalog.skeleton_animations.push_back(std::move(animation));
}

void gen_ship_engine_animation(engine::animation::fbs::AnimationCatalogT& catalog,
                               const glm::vec3& engine_offset) {
    auto animation = modus::make_unique<engine::animation::fbs::SkeletonAnimationT>();
    animation->name = "EngineSpin";
    animation->duration_sec = 1.0f;
    animation->skeleton_index = 0;

    auto bone_keyframe = modus::make_unique<engine::animation::fbs::BoneKeyframeT>();
    bone_keyframe->bone_index = 5;

    {
        // Bone Transform 1
        math::Transform t;
        t.set_translation(engine_offset);
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.0f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    {
        // Bone Transform 2
        math::Transform t;
        t.set_translation(engine_offset * 1.25f);
        t.set_rotation(glm::angleAxis(glm::radians(90.f), glm::vec3(0.f, 0.f, 1.0)));
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.25f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    {
        // Bone Transform 2
        math::Transform t;
        t.set_translation(engine_offset * 1.5f);
        t.set_rotation(glm::angleAxis(glm::radians(180.f), glm::vec3(0.f, 0.f, 1.0)));
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.5f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    {
        // Bone Transform 2
        math::Transform t;
        t.set_translation(engine_offset * 1.25f);
        t.set_rotation(glm::angleAxis(glm::radians(270.f), glm::vec3(0.f, 0.f, 1.0)));
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.75f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    {
        // Bone Transform 2
        math::Transform t;
        t.set_translation(engine_offset);
        t.set_rotation(glm::angleAxis(glm::radians(359.f), glm::vec3(0.f, 0.f, 1.0)));
        auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
        keyframe->timestamp = 0.99f;
        keyframe->bone_transform = to_fbs_transform(t);
        bone_keyframe->bone_transforms.push_back(std::move(keyframe));
    }
    animation->bone_keyframes.push_back(std::move(bone_keyframe));
    catalog.skeleton_animations.push_back(std::move(animation));
}

void gen_wing_animation(engine::animation::fbs::AnimationCatalogT& catalog,
                        const glm::vec3& translation_top,
                        const glm::vec3& translation_left,
                        const glm::vec3& translation_right) {
    auto animation = modus::make_unique<engine::animation::fbs::SkeletonAnimationT>();
    animation->name = "WingsMove";
    animation->duration_sec = 2.0f;
    animation->skeleton_index = 0;

    const u32 bone_indices[] = {2, 3, 4};
    const glm::vec3 translations[] = {translation_top, translation_left, translation_right};

    for (u32 i = 0; i < 3; ++i) {
        auto bone_keyframe = modus::make_unique<engine::animation::fbs::BoneKeyframeT>();
        bone_keyframe->bone_index = bone_indices[i];

        {
            // Bone Transform 1
            math::Transform t;
            t.set_translation(translations[i]);
            auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
            keyframe->timestamp = 0.0f;
            keyframe->bone_transform = to_fbs_transform(t);
            bone_keyframe->bone_transforms.push_back(std::move(keyframe));
        }
        {
            // Bone Transform 2
            math::Transform t;
            t.set_translation(translations[i] * 0.85f);
            auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
            keyframe->timestamp = 1.f;
            keyframe->bone_transform = to_fbs_transform(t);
            bone_keyframe->bone_transforms.push_back(std::move(keyframe));
        }
        {
            // Bone Transform 3
            math::Transform t;
            t.set_translation(translations[i]);
            auto keyframe = modus::make_unique<engine::animation::fbs::BoneTransformT>();
            keyframe->timestamp = 2.f;
            keyframe->bone_transform = to_fbs_transform(t);
            bone_keyframe->bone_transforms.push_back(std::move(keyframe));
        }
        animation->bone_keyframes.push_back(std::move(bone_keyframe));
    }
    catalog.skeleton_animations.push_back(std::move(animation));
}

Result<> gen_ship(const StringSlice output) {
    constexpr f32 kWingHeight = 1.f;
    constexpr f32 kWingDistFromBody = 0.5f;
    constexpr f32 kWingHeightTotal = kWingHeight + kWingDistFromBody;
    constexpr f32 kWingBack = .5f;

    constexpr f32 kNoseOffset = 3.f;
    constexpr f32 kShipRadius = 1.f;
    constexpr f32 kShipRadiusUp = 0.58f;
    constexpr f32 kShipRadiusDown = 1.73f - kShipRadiusUp;

    constexpr f32 kEngineRadius = 0.5f;
    constexpr f32 kEngineRadiusUp = 0.29f;
    constexpr f32 kEngineRadiusDown = 0.87f - kEngineRadiusUp;
    constexpr f32 kEngineExtrusion = kEngineRadiusUp + kEngineRadiusDown;

    // clang-format off

    constexpr usize kWingVertices = 12;
    const glm::vec3 wing_template[] = {
        {-kShipRadius, kShipRadiusUp + kWingDistFromBody, 0.f},
        {kShipRadius, kShipRadiusUp + kWingDistFromBody, 0.f},
        {0.f, kShipRadiusUp + kWingHeightTotal, kWingBack},
        // wing top - bottom
        {-kShipRadius, kShipRadiusUp + kWingDistFromBody, 0.f},
        {0.f, kWingDistFromBody, -kNoseOffset},
        {kShipRadius, kShipRadiusUp + kWingDistFromBody, 0.f},
        // wing top - left
        {-kShipRadius, kShipRadiusUp + kWingDistFromBody, 0.f},
        {0.f, kShipRadiusUp + kWingHeightTotal, kWingBack},
        {0.f, kWingDistFromBody, -kNoseOffset},
        // wing top - right
        {kShipRadius, kShipRadiusUp + kWingDistFromBody, 0.f},
        {0.f, kWingDistFromBody, -kNoseOffset},
        {0.f, kShipRadiusUp + kWingHeightTotal, kWingBack},
    };
    static_assert (modus::array_size(wing_template) == kWingVertices);

    constexpr u32 kEngineVertices = 12;
    const glm::vec3 engine_template [] = {
        // engine - back
        {0.f,-kEngineRadiusDown,0.f},
        {-kEngineRadius, kEngineRadiusUp, 0.f},
        {kEngineRadius, kEngineRadiusUp,0.f},
        // engine top
        {kEngineRadius, kEngineRadiusUp, 0.f},
        {-kEngineRadius, kEngineRadiusUp, 0.f},
        {0.f, 0.f, kEngineExtrusion},
        // engine-left
        {-kEngineRadius, kEngineRadiusUp, 0.f},
        {0.f, -kEngineRadiusDown, 0.f},
        {0.f, 0.f, kEngineExtrusion},
        // engine -right
        {kEngineRadius, kEngineRadiusUp, 0.f},
        {0.f, 0.f, kEngineExtrusion},
        {0.f, -kEngineRadiusDown, 0.f},
    };
    static_assert (modus::array_size(engine_template) == kEngineVertices);


    const glm::quat rot_left = glm::angleAxis(glm::radians(120.f), glm::vec3(0.f,0.f, 1.f));
    const glm::quat rot_right = glm::angleAxis(glm::radians(-120.f), glm::vec3(0.f,0.f, 1.f));
    const glm::vec3 engine_offset = glm::vec3(0, 0, 0.5f);


    const glm::u8vec4 hull_bone_idx = glm::u8vec4(1u,0u,0u,0u);
    const glm::u8vec4 wtop_bone_idx = glm::u8vec4(2u,0u,0u,0u);
    const glm::u8vec4 wleft_bone_idx = glm::u8vec4(3u,0u,0u,0u);
    const glm::u8vec4 wright_bone_idx = glm::u8vec4(4u,0u,0u,0u);
    const glm::u8vec4 engine_bone_idx = glm::u8vec4(5u,0u,0u,0u);
    const glm::u8vec4 one_bone_w = glm::u8vec4(255u,0u,0u,0u);

    VertexDataWithBones data[] = {
        // Main hull - Back
        {{0.f,-kShipRadiusDown,0.f}, 0, hull_bone_idx, one_bone_w},
        {{kShipRadius, kShipRadiusUp,0.f}, 0, hull_bone_idx, one_bone_w},
        {{-kShipRadius, kShipRadiusUp, 0.f}, 0, hull_bone_idx, one_bone_w},
        // Main hull - Top
        {{kShipRadius, kShipRadiusUp, 0.f}, 0, hull_bone_idx, one_bone_w},
        {{0.f, 0.f, -kNoseOffset}, 0, hull_bone_idx, one_bone_w},
        {{-kShipRadius, kShipRadiusUp, 0.f}, 0, hull_bone_idx, one_bone_w},
        // Main hull - left
        {{-kShipRadius, kShipRadiusUp, 0.f}, 0, hull_bone_idx, one_bone_w},
        {{0.f, 0.f, -kNoseOffset}, 0, hull_bone_idx, one_bone_w},
        {{0.f, -kShipRadiusDown, 0.f}, 0, hull_bone_idx, one_bone_w},
        // Main hull - Right
        {{kShipRadius, kShipRadiusUp, 0.f}, 0, hull_bone_idx, one_bone_w},
        {{0.f, -kShipRadiusDown, 0.f}, 0, hull_bone_idx, one_bone_w},
        {{0.f, 0.f, -kNoseOffset}, 0, hull_bone_idx, one_bone_w},

        // ###################
        // wing top
        {wing_template[0], 0, wtop_bone_idx, one_bone_w},
        {wing_template[1], 0, wtop_bone_idx, one_bone_w},
        {wing_template[2], 0, wtop_bone_idx, one_bone_w},
        {wing_template[3], 0, wtop_bone_idx, one_bone_w},
        {wing_template[4], 0, wtop_bone_idx, one_bone_w},
        {wing_template[5], 0, wtop_bone_idx, one_bone_w},
        {wing_template[6], 0, wtop_bone_idx, one_bone_w},
        {wing_template[7], 0, wtop_bone_idx, one_bone_w},
        {wing_template[8], 0, wtop_bone_idx, one_bone_w},
        {wing_template[9], 0, wtop_bone_idx, one_bone_w},
        {wing_template[10], 0, wtop_bone_idx, one_bone_w},
        {wing_template[11], 0, wtop_bone_idx, one_bone_w},

        // ###################
        // wing left
        {rot_left * wing_template[0], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[1], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[2], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[3], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[4], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[5], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[6], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[7], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[8], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[9], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[10], 0, wleft_bone_idx, one_bone_w},
        {rot_left * wing_template[11], 0, wleft_bone_idx, one_bone_w},

        // ###################
        // wing right
        {rot_right * wing_template[0], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[1], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[2], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[3], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[4], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[5], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[6], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[7], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[8], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[9], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[10], 0, wright_bone_idx, one_bone_w},
        {rot_right * wing_template[11], 0, wright_bone_idx, one_bone_w},

        // ###################
        // engine
        { engine_offset + engine_template[0], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[1], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[2], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[3], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[4], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[5], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[6], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[7], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[8], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[9], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[10], 0, engine_bone_idx, one_bone_w},
        { engine_offset + engine_template[11], 0, engine_bone_idx, one_bone_w},

    };

    glm::quat post_rot = glm::angleAxis(glm::radians(180.f), glm::vec3(1.f,0.f,0.f));

    for(auto& vd : data) {
        vd.pos = post_rot * vd.pos;
    }

    // clang-format on

    if (!gen_normals(SliceMut<VertexDataWithBones>(data))) {
        std::cerr << "Failed to generate normals\n";
        return Error<>();
    }

    // Animation Data:
    engine::animation::fbs::AnimationCatalogT catalog;
    {
        // create skeleton
        auto skeleton = modus::make_unique<engine::animation::fbs::SkeletonT>();

        // Create bones
        // Bone order
        // [0] Root Bone
        // [1] Hull bone
        // [2] wing top
        // [3] wing left
        // [4] wing right
        // [5] engine

        {
            // Root
            auto bone = modus::make_unique<engine::animation::fbs::BoneT>();
            bone->name = "Root";
            bone->parent_index = std::numeric_limits<u32>::max();
            bone->bone_transform = to_fbs_transform(math::Transform());
            bone->inv_bind_transform = to_fbs_transform(math::Transform());
            skeleton->bones.push_back(std::move(bone));
        }
        {
            // Hull
            math::Transform t;
            t.set_translation(post_rot * glm::vec3(0.f, 0.f, -1.f));
            auto bone = modus::make_unique<engine::animation::fbs::BoneT>();
            bone->name = "Hull";
            bone->parent_index = 0;
            bone->bone_transform = to_fbs_transform(t);
            bone->inv_bind_transform = to_fbs_transform(t.inverted());
            skeleton->bones.push_back(std::move(bone));
        }
        {
            // Wing Top
            math::Transform t;
            t.set_translation(post_rot * glm::vec3(0.f, kShipRadiusUp + kWingDistFromBody, 0.f));
            auto bone = modus::make_unique<engine::animation::fbs::BoneT>();
            bone->name = "WingTop";
            bone->parent_index = 0;
            bone->bone_transform = to_fbs_transform(t);
            bone->inv_bind_transform = to_fbs_transform(t.inverted());
            skeleton->bones.push_back(std::move(bone));
        }
        {
            // Wing Left
            math::Transform t;
            t.set_translation(post_rot *
                              (rot_left * glm::vec3(0.f, kShipRadiusUp + kWingDistFromBody, 0.f)));
            auto bone = modus::make_unique<engine::animation::fbs::BoneT>();
            bone->name = "WingLeft";
            bone->parent_index = 0;
            bone->bone_transform = to_fbs_transform(t);
            bone->inv_bind_transform = to_fbs_transform(t.inverted());
            skeleton->bones.push_back(std::move(bone));
        }
        {
            // Wing Right
            math::Transform t;
            t.set_translation(post_rot *
                              (rot_right * glm::vec3(0.f, kShipRadiusUp + kWingDistFromBody, 0.f)));
            auto bone = modus::make_unique<engine::animation::fbs::BoneT>();
            bone->name = "WingRight";
            bone->parent_index = 0;
            bone->bone_transform = to_fbs_transform(t);
            bone->inv_bind_transform = to_fbs_transform(t.inverted());
            skeleton->bones.push_back(std::move(bone));
        }
        {
            // Engine
            math::Transform t;
            t.set_translation(post_rot * engine_offset);
            auto bone = modus::make_unique<engine::animation::fbs::BoneT>();
            bone->name = "Engine";
            bone->parent_index = 0;
            bone->bone_transform = to_fbs_transform(t);
            bone->inv_bind_transform = to_fbs_transform(t.inverted());
            skeleton->bones.push_back(std::move(bone));
        }
        catalog.skeletons.push_back(std::move(skeleton));
    }

    gen_ship_engine_animation(catalog, post_rot * engine_offset);
    gen_ship_shoot_anim(catalog, post_rot * glm::vec3(0.f, 0.f, -1.0f));
    {
        const glm::vec3 wing_translation = glm::vec3(0.f, kShipRadiusUp + kWingDistFromBody, 0.f);
        gen_wing_animation(catalog, post_rot * wing_translation,
                           post_rot * (rot_left * wing_translation),
                           post_rot * (rot_right * wing_translation));
    }

    return generate_mesh(output, Slice(data), catalog);
}

Result<> gen_laser(const StringSlice output) {
    constexpr f32 kRadius = 1.f;
    constexpr f32 kFrontExtend = 2.0f;
    constexpr f32 kBackExtend = 4.0f;

    // clang-format off

    VertexData data[] = {
        // Front - top left
        {{-kRadius, 0.f, 0.f}, 0},
        {{0.f, kRadius, 0.f}, 0},
        {{0.f, 0.f, -kFrontExtend}, 0},
        // Front - top right
        {{kRadius, 0.f, 0.f}, 0},
        {{0.f, 0.f, -kFrontExtend}, 0},
        {{0.f, kRadius, 0.f}, 0},
        // Front - bottom right
        {{kRadius, 0.f, 0.f}, 0},
        {{0.f, -kRadius, 0.f}, 0},
        {{0.f, 0.f, -kFrontExtend}, 0},
        // Front - bottom left
        {{-kRadius, 0.f, 0.f}, 0},
        {{0.f, 0.f, -kFrontExtend}, 0},
        {{0.f, -kRadius, 0.f}, 0},
        // Back - top left
        {{-kRadius, 0.f, 0.f}, 0},
        {{0.f, 0.f, kBackExtend}, 0},
        {{0.f, kRadius, 0.f}, 0},
        // Back - top right
        {{0.f, kRadius, 0.f}, 0},
        {{0.f, 0.f, kBackExtend}, 0},
        {{kRadius, 0.f, 0.f}, 0},
        // Back - bottom right
        {{kRadius, 0.f, 0.f}, 0},
        {{0.f, 0.f, kBackExtend}, 0},
        {{0.f, -kRadius, 0.f}, 0},
        // Back - bottom left
        {{-kRadius, 0.f, 0.f}, 0},
        {{0.f, -kRadius, 0.f}, 0},
        {{0.f, 0.f, kBackExtend}, 0},
    };

    // clang-format on
    glm::quat rot = glm::angleAxis(glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));

    for (auto& vd : data) {
        vd.pos = rot * vd.pos;
    }

    if (!gen_normals(SliceMut<VertexData>(data))) {
        std::cerr << "Failed to generate normals\n";
        return Error<>();
    }

    return generate_mesh(output, Slice(data));
}

Result<> gen_track(const StringSlice output) {
    static constexpr f32 kPlaneHeight = 1.0f;
    static constexpr f32 kPlaneWidth = 8.0f;
    static constexpr f32 kPlaneDepth = 20.0f;

    // clang-format off
    const glm::vec3 template_plane[] = {
        {-kPlaneWidth, 0.f, -kPlaneDepth},
        {-kPlaneWidth, 0.f, kPlaneDepth},
        {kPlaneWidth, 0.f, kPlaneDepth},
        {kPlaneWidth, 0.f, kPlaneDepth},
        {kPlaneWidth, 0.f, -kPlaneDepth},
        {-kPlaneWidth, 0.f, -kPlaneDepth}
    };

    const glm::vec3 template_plane_lifted[] = {
        {-kPlaneWidth, kPlaneHeight, -kPlaneDepth},
        {-kPlaneWidth, kPlaneHeight, kPlaneDepth},
        {kPlaneWidth, kPlaneHeight, kPlaneDepth},
        {kPlaneWidth, kPlaneHeight, kPlaneDepth},
        {kPlaneWidth, kPlaneHeight, -kPlaneDepth},
        {-kPlaneWidth, kPlaneHeight, -kPlaneDepth}
    };

    const glm::vec3 template_plane_inc_left[] = {
        {-1.f, kPlaneHeight, -kPlaneDepth},
        {-1.f, kPlaneHeight, kPlaneDepth},
        {1.0f, 0.f, kPlaneDepth},
        {1.0f, 0.f, kPlaneDepth},
        {1.0f, 0.f, -kPlaneDepth},
        {-1.f, kPlaneHeight, -kPlaneDepth}
    };

    const glm::vec3 template_plane_inc_right[] = {
        {-1.f, 0.f, -kPlaneDepth},
        {-1.f, 0.f, kPlaneDepth},
        {1.0f, kPlaneHeight, kPlaneDepth},
        {1.0f, kPlaneHeight, kPlaneDepth},
        {1.0f, kPlaneHeight, -kPlaneDepth},
        {-1.f, 0.f, -kPlaneDepth}
    };

#define INSERT_TEMPLATE(templ)\
        {{templ[0]}, 0}, \
        {{templ[1]}, 0}, \
        {{templ[2]}, 0}, \
        {{templ[3]}, 0}, \
        {{templ[4]}, 0}, \
        {{templ[5]}, 0}, \

#define INSERT_TEMPLATE_T(templ, t)\
        {{t.transform(templ[0])}, 0}, \
        {{t.transform(templ[1])}, 0}, \
        {{t.transform(templ[2])}, 0}, \
        {{t.transform(templ[3])}, 0}, \
        {{t.transform(templ[4])}, 0}, \
        {{t.transform(templ[5])}, 0}, \

    math::Transform t_left_x1;
    t_left_x1.translate(glm::vec3(-kPlaneWidth - 1.f, 0.f, 0.f));
    math::Transform t_left_x2;
    t_left_x2.translate(glm::vec3(-kPlaneWidth * 2.f - 2.f, 0.f, 0.f));
    math::Transform t_right_x1;
    t_right_x1.translate(glm::vec3(kPlaneWidth + 1.0f, 0.f, 0.f));
    math::Transform t_right_x2;
    t_right_x2.translate(glm::vec3(kPlaneWidth * 2.0f + 2.0f, 0.f, 0.f));

    VertexData data[] = {
        INSERT_TEMPLATE_T(template_plane_lifted, t_left_x2)
        INSERT_TEMPLATE_T(template_plane_inc_left, t_left_x1)
        INSERT_TEMPLATE(template_plane)
        INSERT_TEMPLATE_T(template_plane_inc_right, t_right_x1)
        INSERT_TEMPLATE_T(template_plane_lifted, t_right_x2)
    };

#undef INSERT_TEMPLATE
#undef INSERT_TEMPLATE_T
    // clang-format on

    if (!gen_normals(SliceMut<VertexData>(data))) {
        std::cerr << "Failed to generate normals\n";
        return Error<>();
    }

    return generate_mesh(output, Slice(data));
}

static const StringSlice kIntro =
    "Generate Modus game models\n"
    "modus_mk_game_model -o <output>";
int main(const int argc, const char** argv) {
    CmdOptionString opt_output("-o", "--output", "Path for the generated output", "");

    CmdOptionEmpty opt_ship("", "--ship", "Generate ship model");
    CmdOptionEmpty opt_laser("", "--laser", "Generate laser model");
    CmdOptionEmpty opt_track("", "--track", "Generate track model");

    CmdOptionParser opt_parser;
    opt_parser.add(opt_output).expect("Failed to add cmd parser option");
    opt_parser.add(opt_ship).expect("Failed to add cmd parser option");
    opt_parser.add(opt_laser).expect("Failed to add cmd parser option");
    opt_parser.add(opt_track).expect("Failed to add cmd parser option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (!opt_output.parsed()) {
        std::cerr << fmt::format("No output file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_ship.parsed()) {
        if (!gen_ship(opt_output.value())) {
            return EXIT_FAILURE;
        }
    } else if (opt_laser.parsed()) {
        if (!gen_laser(opt_output.value())) {
            return EXIT_FAILURE;
        }
    } else if (opt_track.parsed()) {
        if (!gen_track(opt_output.value())) {
            return EXIT_FAILURE;
        }
    } else {
        std::cerr << "No specific model requested\n";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
