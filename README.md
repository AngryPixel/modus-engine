# Modus Engine

Toy modular game engine I've been working on since August 2019 to explore certain patterns and approaches at my own 
leisure and with emphasis on robustness. Additionally, develop a game that was designed to run on the Raspberry PI4 as 
the primary platform.

**NOTE:** The game in this repository is not consider a finished product and is still very rough around the edges.

# About
## Design Decision

- Clear defined boundaries between Game and Engine
- Modular extensibility
- Testable code blocks
- Avoid globals/singletons, exceptions for:
  - Logging
  - Memory allocators
  - TypeID generators
- Thread safety (e.g: ThreadSafe Engine and Module APIs)
- Avoid sharing pointers outside of Module scope
- Multithreading
- Multi-platform
  - Linux
  - Windows
  - Raspberry PI 4
- Engine as an OS

## Features

- OpenGL ES 3.1 renderer (Limited by Raspberry PI 4)
- Async Asset Loading
- Job System based parallelization
- Entity Component System based gameplay
- Custom UI
- Custom resource manager to prepare assets

## Future work
- Vulkan Support
- Editor
- Reflection API
- Engine SDK
- In-game Console

# Build Instructions

## Requirements

 - CMake 3.18
 - git lfs
 - C++ 17 compatible C++ compiler:
   - Linux/RPI4: GCC >= 8.0.3 (Clang can also be used, but it is not tested as often)
   - Windows: VS 2019
 - 3rdparty libraries:
   - libogg 1.3.4
   - libvorbis 1.3.7
   - libbullet 3.08
   - libassimp 5.0
   - liblz4 1.9.2
   - libfmt 7.0.3
   - libcatch 2.12.1
   - libspdlog 1.7.0
   - SDL 2.0.14

**NOTE:** It may be possible to build the project with earlier version of the above libraries. For the best experience,
it is recommended to either use a rolling release distro or a package manager 
[such as VCPKG](https://github.com/Microsoft/vcpkg). VCKPG will automatically be used if the _VCPKG_ROOT_ evironment 
variable is present.

## General Build Instructions
Be sure to either clone this repo **recursively** or **initialize the submodules** after you have cloned the repo.

Using CMake generate the project using your preferred generator and proceed to build afterwards. For more details or 
help see [the official CMake documentation](https://cmake.org/cmake/help/v3.18/guide/user-interaction/index.html).

The resulting binaries will be located in the build directory, with :
- **bin:** Main binaries and tools
- **bin_tests:** Binaries for test applications
- **lib:** Library outputs

### Raspberry PI4
The CMake project will try to configure the project automatically on the Raspberry PI. Right now it only supports the 
official Raspbian distribution.

It is recommended to install all the dependencies above via VCPKG or another package manager to get the more up to 
date versions. However, it is possible to use the distribution provided bullet package.

### Windows
It's recommended to use packages provided by VCPKG to build this project.

The windows build at the moment does not support being built as shared libraries.

# Known Issues

## Build Errors
If you run into a flatbuffer generator error such as *error: datatype already exists*, just build again and the error
will go away.

## Raspberry PI Test Crashes
At the time of writing, 2021/01/09, some tests are currently failing on the RPI due to a crash in the GPU driver. I 
have not yet got around to investigating why they started happening.

# Launching the Binaries
The resulting binaries can be launched directly from your build environment without further change.  
Be sure you have the assets submodule checked out as it contains all the binary files.

# Rebuilding the binary Assets
There are two custom CMake targets, *make_bin_assets* and *make_bin_assets_clean*, which you can run that will rebuild 
all the binary assets if they are out of date.

For more information consult modus_package.
