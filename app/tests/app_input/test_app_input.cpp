/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <app/app_pch.h>
#include <app/app.hpp>
#include <app/event.hpp>
#include <iostream>
#include <log/log.hpp>

using namespace modus;

static bool on_event(const app::Event& event) {
    switch (event.type) {
        case app::EventType::Input: {
            MODUS_LOGI("Event: {}", event.input);
            break;
        }
        case app::EventType::GameControllerAdded: {
            MODUS_LOGI("GameControllerAdded: {}", event.controller_added.id);
            break;
        }
        case app::EventType::GameControllerRemoved: {
            MODUS_LOGI("GameControllerRemoved: {}", event.controller_removed.id);
            break;
        }
        default:
            MODUS_LOGE("Unkown Event!");
            break;
    }
    return false;
}
int main() {
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::set(log::make_basic_stdout_logger());
#endif
    log::LogService::set_level(log::Level::Debug);
    std::unique_ptr<app::App> app;
    app = modus::app::create_app();

    app::AppInitParams init_params;
    init_params.name = "AppInputTest";
    init_params.name_preferences = "modus.appinputtest";
    init_params.event_handler.bind<on_event>();
    if (!app->initialize(init_params)) {
        return EXIT_FAILURE;
    }

    app::WindowCreateParams window_params;
    window_params.title = "Modus App Input Test";
    app::AppResult<NotMyPtr<app::Window>> result = app->create_window(window_params);
    if (!result) {
        MODUS_LOGE("Failed to create window: {}", result.error());
        return EXIT_FAILURE;
    }

    const app::WindowState state = result.value()->window_state();
    MODUS_LOGD("Window Created ::  x:{:03} y:{:03} w:{:03} h:{:03}", state.x, state.y, state.width,
               state.height);
    app->run();

    if (!app->shutdown()) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
