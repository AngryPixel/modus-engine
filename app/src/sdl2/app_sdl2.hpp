/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <app/app.hpp>
#include <app/input.hpp>
#include <app_private.hpp>
#include <sdl2/window_sdl2.hpp>

namespace modus::app {
class IEventHandler;
}

namespace modus::app::sdl2 {

class AppSDL2 final : public App {
   private:
    Vector<DeviceId> m_gamepads;

   public:
    AppSDL2();

    AppResult<void> initialize(const AppInitParams& params) override;

    AppResult<void> shutdown() override;

    TickResult tick() override;

    AppResult<NotMyPtr<Window>> create_window(const WindowCreateParams& params) override;

    Optional<NotMyPtr<Window>> main_window() override;

    Result<String, void> impl_path_resources() override;

    void lock_mouse(const bool value) override;

    bool is_mouse_locked() const override;

    Slice<DeviceId> game_controllers() const override;

   private:
    Result<DeviceId, void> add_game_controller(const i32 index);

    Result<> remove_game_controller(const DeviceId id);

   private:
    EventHandler m_event_handler;
    std::unique_ptr<WindowSDL2> m_window;
};

}    // namespace modus::app::sdl2
