/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <sdl2/app_sdl2.hpp>
#include <sdl2/window_sdl2.hpp>
#include <threed/instance.hpp>

namespace modus::app::sdl2 {

static Result<u32, String> pre_window_create_setup(const threed::ApiVersion& api,
                                                   const WindowCreateParams& params) {
    u32 flags = 0;

    if (api.api == threed::Api::OpenGLES3) {
        if (api.major == 4) {
            // Windows doesn't have OpenGLES support but OpenGL 4.x
            // has the required extensions
            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE) !=
                0) {
                return Error(
                    String("Failed to set SDL2 GL context profile attribute on "
                           "window"));
            }

            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4) != 0) {
                return Error(
                    String("Failed to set SDL2 GL context version major attribute "
                           "on window"));
            }

            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3) != 0) {
                return Error(
                    String("Failed to set SDL2 GL context version minor attribute "
                           "on window"));
            }
        } else {
            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES) != 0) {
                return Error(
                    String("Failed to set SDL2 GL context profile attribute on "
                           "window"));
            }

            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) != 0) {
                return Error(
                    String("Failed to set SDL2 GL context version major attribute "
                           "on window"));
            }

            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) != 0) {
                return Error(
                    String("Failed to set SDL2 GL context version minor attribute "
                           "on window"));
            }
        }

        if (SDL_GL_SetAttribute(SDL_GL_RED_SIZE, params.framebuffer.red_bits) != 0) {
            return Error(String("Failed to set SDL2 GL context Red size"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, params.framebuffer.blue_bits) != 0) {
            return Error(String("Failed to set SDL2 GL context Blue size"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, params.framebuffer.green_bits) != 0) {
            return Error(String("Failed to set SDL2 GL context Green size"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 0) != 0) {
            return Error(String("Failed to set SDL2 GL context Alpha size"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, params.framebuffer.depth_bits) != 0) {
            return Error(String("Failed to set SDL2 GL context Depth size"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, params.framebuffer.stencil_bits) != 0) {
            return Error(String("Failed to set SDL2 GL context Stencil size"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1) != 0) {
            return Error(String("Failed to set SDL2 GL double buffer"));
        }

        if (SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1) != 0) {
            return Error(String("Failed to set SDL2 GL accelerated visual"));
        }

        if (params.framebuffer.srgb &&
            SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1) != 0) {
            return Error(String("Failed to create SRGB capacble context\n"));
        }

        /*
        if (debug) {
            if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,
                                    SDL_GL_CONTEXT_DEBUG_FLAG) != 0) {
                return Error(String(
                    "Failed to set SDL2 GL context version minor attribute "
                    "on window"));
            }
        }*/
        flags |= SDL_WINDOW_OPENGL;
    }

    return Ok(flags);
}
inline SDL2Ptr<SDL_Window> make_sdl2_window_ptr() {
    return SDL2Ptr<SDL_Window>(nullptr, [](SDL_Window* window) {
        if (window != nullptr) {
            SDL_DestroyWindow(window);
        }
    });
}

Result<std::unique_ptr<WindowSDL2>, String> WindowSDL2::create(AppSDL2& app,
                                                               const WindowCreateParams& params) {
    char cstr[128];
    params.title.to_cstr(cstr, sizeof(cstr));

    u32 flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_MOUSE_FOCUS |
                SDL_WINDOW_INPUT_FOCUS;
    // Set API flags
    {
        const auto threed_api = app.threed_instance().api();
        if (threed_api.api != threed::Api::Null) {
            auto result = pre_window_create_setup(app.threed_instance().api(), params);
            if (!result) {
                return Error(String(result.error()));
            }

            flags |= result.value();
        }
    }

    switch (params.fullscreen) {
        case WindowFullscreenMode::Exclusive:
            flags |= SDL_WINDOW_FULLSCREEN;
            break;
        case WindowFullscreenMode::Windowed:
            flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
            break;
        case WindowFullscreenMode::Borderless:
            flags |= SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_BORDERLESS;
            break;
        default:
            break;
    }

    SDL_Window* sdl_window = SDL_CreateWindow(cstr, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                              params.width ? params.width.value() : 1280,
                                              params.height ? params.height.value() : 720, flags);

    if (sdl_window == nullptr) {
        return Error(String(SDL_GetError()));
    }
    SDL_SetWindowTitle(sdl_window, cstr);

    SDL2Ptr<SDL_Window> window = make_sdl2_window_ptr();
    window.reset(sdl_window);

    // Initialize compositor
    auto impl = modus::make_unique<WindowSDL2>(app, std::move(window));

    if (auto r = impl->create_and_bind_context(); !r) {
        return Error(r.release_error());
    }
    if (params.fullscreen == WindowFullscreenMode::Exclusive) {
        // TODO: Improve
        SDL_DisplayMode display_mode;
        if (SDL_GetDisplayMode(0, 0, &display_mode) != 0) {
            MODUS_LOGE("WindowSDL2: Failed to retrieve display mode: {}", SDL_GetError());
            return Error<String>(SDL_GetError());
        }

        MODUS_LOGD("WindowSDL2: Switching to {}x{} display mode\n", display_mode.w, display_mode.h);

        if (SDL_SetWindowDisplayMode(impl->sdl_window(), &display_mode) != 0) {
            MODUS_LOGE("WindowSDL2: Failed set window display mode: {}", SDL_GetError());
            return Error<String>(SDL_GetError());
        }
    }
    if (params.hidden) {
        SDL_HideWindow(sdl_window);
    } else {
        SDL_ShowWindow(sdl_window);
    }

    return Ok(std::move(impl));
}

Result<NotMyPtr<threed::Compositor>, void> WindowSDL2::create_compositor(threed::Device& device,
                                                                         const bool vsync) {
    if (m_app.threed_instance().api().api == threed::Api::Null) {
        m_compositor = modus::make_unique<threed::NullCompositor>();
    } else {
        auto sdl2_compositor = modus::make_unique<CompositorSDL2>();
        auto compositor_result =
            sdl2_compositor->create(*m_window, m_app.threed_instance(), device, vsync);
        if (!compositor_result) {
            return Error<>();
        }
        m_compositor = std::move(sdl2_compositor);
    }
    return Ok<NotMyPtr<threed::Compositor>>(m_compositor.get());
}

NotMyPtr<threed::IContext> WindowSDL2::window_context() {
    return m_window_context.get();
}

Optional<NotMyPtr<threed::Compositor>> WindowSDL2::compositor() {
    return m_compositor != nullptr ? NotMyPtr<threed::Compositor>(m_compositor.get())
                                   : Optional<NotMyPtr<threed::Compositor>>();
}

WindowSDL2::WindowSDL2(AppSDL2& app, SDL2Ptr<SDL_Window>&& window)
    : m_app(app), m_window(std::move(window)) {}

WindowSDL2::~WindowSDL2() {}

WindowState WindowSDL2::window_state() const {
    WindowState state;
    SDL_Window* window = m_window.get();

    SDL_GetWindowPosition(window, &state.x, &state.y);
    SDL_GetWindowSize(window, &state.width, &state.height);
    const u32 window_flags = SDL_GetWindowFlags(window);

    if ((window_flags & SDL_WINDOW_FULLSCREEN) == SDL_WINDOW_FULLSCREEN) {
        state.fullscreen = WindowFullscreenMode::Exclusive;
    } else if ((window_flags & (SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_BORDERLESS)) ==
               (SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_BORDERLESS)) {
        state.fullscreen = WindowFullscreenMode::Borderless;
    } else if ((window_flags & SDL_WINDOW_FULLSCREEN_DESKTOP) == SDL_WINDOW_FULLSCREEN_DESKTOP) {
        state.fullscreen = WindowFullscreenMode::Windowed;
    }
    return state;
}

void WindowSDL2::set_window_state(const WindowState& state) {
    modus_assert(m_window);
    SDL_SetWindowSize(m_window.get(), state.width, state.height);

    if (state.fullscreen == WindowFullscreenMode::None) {
        SDL_SetWindowBordered(m_window.get(), SDL_TRUE);
        SDL_SetWindowFullscreen(m_window.get(), 0);
    } else if (state.fullscreen == WindowFullscreenMode::Exclusive) {
        SDL_SetWindowFullscreen(m_window.get(), SDL_WINDOW_FULLSCREEN);
    } else if (state.fullscreen == WindowFullscreenMode::Borderless) {
        SDL_SetWindowBordered(m_window.get(), SDL_FALSE);
        SDL_SetWindowFullscreen(m_window.get(), SDL_WINDOW_FULLSCREEN_DESKTOP);
    } else if (state.fullscreen == WindowFullscreenMode::Windowed) {
        SDL_SetWindowBordered(m_window.get(), SDL_TRUE);
        SDL_SetWindowFullscreen(m_window.get(), SDL_WINDOW_FULLSCREEN_DESKTOP);
    }
}

class ContextGLESSDL2 final : public threed::IContext {
    SDL_Window& m_window;
    SDL_GLContext m_context;

   public:
    ContextGLESSDL2(SDL_Window& window, SDL_GLContext context)
        : m_window(window), m_context(context) {
        modus_assert(context != nullptr);
    }

    ~ContextGLESSDL2() override { SDL_GL_DeleteContext(m_context); }

    Result<void, String> bind_to_thread() override {
        if (SDL_GL_MakeCurrent(&m_window, m_context) != 0) {
            return Error<String>(SDL_GetError());
        }
        return Ok<>();
    }
};

Result<std::unique_ptr<threed::IContext>, String> WindowSDL2::create_context(
    NotMyPtr<threed::IContext> shared) {
    modus_assert(m_window);
    const auto threed_api = m_app.threed_instance().api();
    if (threed_api.api != threed::Api::OpenGLES3 && threed_api.api != threed::Api::Null) {
        return Error(String("Contexts for API's other than OpenGLES not yet inplemented"));
    }

    if (shared) {
        return Error(String("Shared contexts not yet inplemented"));
    }

    std::unique_ptr<threed::IContext> ptr;
    if (threed_api.api == threed::Api::OpenGLES3) {
        SDL_GLContext context = SDL_GL_CreateContext(m_window.get());
        if (context == nullptr) {
            return Error<String>(SDL_GetError());
        }
        ptr = modus::make_unique<ContextGLESSDL2>(*m_window, context);
    } else if (threed_api.api == threed::Api::Null) {
        ptr = modus::make_unique<threed::NullContext>();
    } else {
        return Error(String("Current API context not implemented"));
    }
    return Ok(std::move(ptr));
}

void WindowSDL2::set_title(const StringSlice title) {
    const String str_title = title.to_str();
    SDL_SetWindowTitle(m_window.get(), str_title.c_str());
}

Result<void, String> WindowSDL2::create_and_bind_context() {
    auto r_context = create_context(NotMyPtr<threed::IContext>());
    if (!r_context) {
        return Error(r_context.release_error());
    }

    m_window_context = std::move(r_context.release_value());

    if (auto r = m_window_context->bind_to_thread(); !r) {
        return Error(r.release_error());
    }
    return Ok<>();
}

}    // namespace modus::app::sdl2
