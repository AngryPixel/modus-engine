/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <sdl2/input_sdl2.hpp>

namespace modus::app::sdl2 {

static InputKey k_sdl_scancode_to_input_key[SDL_NUM_SCANCODES];

void init_input_key_map() {
    std::fill(std::begin(k_sdl_scancode_to_input_key), std::end(k_sdl_scancode_to_input_key),
              InputKey::Total);
#define MODUS_SDL_KEY(sdl, engine) k_sdl_scancode_to_input_key[sdl] = InputKey::engine
    MODUS_SDL_KEY(SDL_SCANCODE_0, Key0);
    MODUS_SDL_KEY(SDL_SCANCODE_1, Key1);
    MODUS_SDL_KEY(SDL_SCANCODE_2, Key2);
    MODUS_SDL_KEY(SDL_SCANCODE_3, Key3);
    MODUS_SDL_KEY(SDL_SCANCODE_4, Key4);
    MODUS_SDL_KEY(SDL_SCANCODE_5, Key5);
    MODUS_SDL_KEY(SDL_SCANCODE_6, Key6);
    MODUS_SDL_KEY(SDL_SCANCODE_7, Key7);
    MODUS_SDL_KEY(SDL_SCANCODE_8, Key8);
    MODUS_SDL_KEY(SDL_SCANCODE_9, Key9);
    MODUS_SDL_KEY(SDL_SCANCODE_A, KeyA);
    MODUS_SDL_KEY(SDL_SCANCODE_B, KeyB);
    MODUS_SDL_KEY(SDL_SCANCODE_C, KeyC);
    MODUS_SDL_KEY(SDL_SCANCODE_D, KeyD);
    MODUS_SDL_KEY(SDL_SCANCODE_E, KeyE);
    MODUS_SDL_KEY(SDL_SCANCODE_F, KeyF);
    MODUS_SDL_KEY(SDL_SCANCODE_G, KeyG);
    MODUS_SDL_KEY(SDL_SCANCODE_H, KeyH);
    MODUS_SDL_KEY(SDL_SCANCODE_I, KeyI);
    MODUS_SDL_KEY(SDL_SCANCODE_J, KeyJ);
    MODUS_SDL_KEY(SDL_SCANCODE_K, KeyK);
    MODUS_SDL_KEY(SDL_SCANCODE_L, KeyL);
    MODUS_SDL_KEY(SDL_SCANCODE_M, KeyM);
    MODUS_SDL_KEY(SDL_SCANCODE_N, KeyN);
    MODUS_SDL_KEY(SDL_SCANCODE_O, KeyO);
    MODUS_SDL_KEY(SDL_SCANCODE_P, KeyP);
    MODUS_SDL_KEY(SDL_SCANCODE_Q, KeyQ);
    MODUS_SDL_KEY(SDL_SCANCODE_R, KeyR);
    MODUS_SDL_KEY(SDL_SCANCODE_S, KeyS);
    MODUS_SDL_KEY(SDL_SCANCODE_T, KeyT);
    MODUS_SDL_KEY(SDL_SCANCODE_U, KeyU);
    MODUS_SDL_KEY(SDL_SCANCODE_V, KeyV);
    MODUS_SDL_KEY(SDL_SCANCODE_W, KeyW);
    MODUS_SDL_KEY(SDL_SCANCODE_X, KeyX);
    MODUS_SDL_KEY(SDL_SCANCODE_Y, KeyY);
    MODUS_SDL_KEY(SDL_SCANCODE_Z, KeyZ);
    MODUS_SDL_KEY(SDL_SCANCODE_RETURN, KeyEnter);
    MODUS_SDL_KEY(SDL_SCANCODE_ESCAPE, KeyEscape);
    MODUS_SDL_KEY(SDL_SCANCODE_BACKSPACE, KeyBackSpace);
    MODUS_SDL_KEY(SDL_SCANCODE_TAB, KeyTab);
    MODUS_SDL_KEY(SDL_SCANCODE_SPACE, KeySpace);
    MODUS_SDL_KEY(SDL_SCANCODE_MINUS, KeyMinus);
    MODUS_SDL_KEY(SDL_SCANCODE_EQUALS, KeyEquals);
    MODUS_SDL_KEY(SDL_SCANCODE_LEFTBRACKET, KeyLeftBracket);
    MODUS_SDL_KEY(SDL_SCANCODE_RIGHTBRACKET, KeyRightBracket);
    MODUS_SDL_KEY(SDL_SCANCODE_BACKSLASH, KeyBackSlash);
    MODUS_SDL_KEY(SDL_SCANCODE_SEMICOLON, KeySemiColon);
    MODUS_SDL_KEY(SDL_SCANCODE_APOSTROPHE, KeyApostrophe);
    MODUS_SDL_KEY(SDL_SCANCODE_GRAVE, KeyTilde);
    MODUS_SDL_KEY(SDL_SCANCODE_COMMA, KeyComma);
    MODUS_SDL_KEY(SDL_SCANCODE_PERIOD, KeyPeriod);
    MODUS_SDL_KEY(SDL_SCANCODE_SLASH, KeySlash);
    MODUS_SDL_KEY(SDL_SCANCODE_CAPSLOCK, KeyCapsLock);
    MODUS_SDL_KEY(SDL_SCANCODE_F1, KeyF1);
    MODUS_SDL_KEY(SDL_SCANCODE_F2, KeyF2);
    MODUS_SDL_KEY(SDL_SCANCODE_F3, KeyF3);
    MODUS_SDL_KEY(SDL_SCANCODE_F4, KeyF4);
    MODUS_SDL_KEY(SDL_SCANCODE_F5, KeyF5);
    MODUS_SDL_KEY(SDL_SCANCODE_F6, KeyF6);
    MODUS_SDL_KEY(SDL_SCANCODE_F7, KeyF7);
    MODUS_SDL_KEY(SDL_SCANCODE_F8, KeyF8);
    MODUS_SDL_KEY(SDL_SCANCODE_F9, KeyF9);
    MODUS_SDL_KEY(SDL_SCANCODE_F10, KeyF10);
    MODUS_SDL_KEY(SDL_SCANCODE_F11, KeyF11);
    MODUS_SDL_KEY(SDL_SCANCODE_F12, KeyF12);
    MODUS_SDL_KEY(SDL_SCANCODE_PRINTSCREEN, KeyPrintScreen);
    MODUS_SDL_KEY(SDL_SCANCODE_SCROLLLOCK, KeyScrollLock);
    MODUS_SDL_KEY(SDL_SCANCODE_PAUSE, KeyPause);
    MODUS_SDL_KEY(SDL_SCANCODE_INSERT, KeyInsert);
    MODUS_SDL_KEY(SDL_SCANCODE_HOME, KeyHome);
    MODUS_SDL_KEY(SDL_SCANCODE_PAGEUP, KeyPgUp);
    MODUS_SDL_KEY(SDL_SCANCODE_DELETE, KeyDelete);
    MODUS_SDL_KEY(SDL_SCANCODE_END, KeyEnd);
    MODUS_SDL_KEY(SDL_SCANCODE_PAGEDOWN, KeyPgDown);
    MODUS_SDL_KEY(SDL_SCANCODE_RIGHT, KeyRight);
    MODUS_SDL_KEY(SDL_SCANCODE_LEFT, KeyLeft);
    MODUS_SDL_KEY(SDL_SCANCODE_DOWN, KeyDown);
    MODUS_SDL_KEY(SDL_SCANCODE_UP, KeyUp);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_DIVIDE, KeyKPDivide);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_MULTIPLY, KeyKPMulitply);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_MINUS, KeyKPMinus);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_PLUS, KeyKPPlus);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_ENTER, KeyKPEnter);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_1, KeyKP1);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_2, KeyKP2);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_3, KeyKP3);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_4, KeyKP4);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_5, KeyKP5);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_6, KeyKP6);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_7, KeyKP7);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_8, KeyKP8);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_9, KeyKP9);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_0, KeyKP0);
    MODUS_SDL_KEY(SDL_SCANCODE_KP_PERIOD, KeyKPPeriod);
    MODUS_SDL_KEY(SDL_SCANCODE_LSHIFT, KeyLeftShift);
    MODUS_SDL_KEY(SDL_SCANCODE_RSHIFT, KeyRightShift);
    MODUS_SDL_KEY(SDL_SCANCODE_LCTRL, KeyLeftCtrl);
    MODUS_SDL_KEY(SDL_SCANCODE_RCTRL, KeyRightCtrl);
    MODUS_SDL_KEY(SDL_SCANCODE_LALT, KeyLeftAlt);
    MODUS_SDL_KEY(SDL_SCANCODE_RALT, KeyRightAlt);
    MODUS_SDL_KEY(SDL_SCANCODE_LGUI, KeyLeftOS);
    MODUS_SDL_KEY(SDL_SCANCODE_RGUI, KeyRightOS);
    MODUS_SDL_KEY(SDL_SCANCODE_NONUSBACKSLASH, KeyBackSlash);
#undef SDK_KEY
}

static inline InputKey sdl_scancode_to_input_Key(const u32 sdl_scancode) {
    modus_assert(sdl_scancode < SDL_NUM_SCANCODES) return (sdl_scancode < SDL_NUM_SCANCODES)
        ? k_sdl_scancode_to_input_key[sdl_scancode]
        : InputKey::Total;
}

static inline GameControllerButton sdl_controller_button_to_input_controller_button(
    const u8 button) {
    switch (button) {
        case SDL_CONTROLLER_BUTTON_A:
            return GameControllerButton::A;
        case SDL_CONTROLLER_BUTTON_B:
            return GameControllerButton::B;
        case SDL_CONTROLLER_BUTTON_X:
            return GameControllerButton::X;
        case SDL_CONTROLLER_BUTTON_Y:
            return GameControllerButton::Y;
        case SDL_CONTROLLER_BUTTON_BACK:
            return GameControllerButton::Back;
        case SDL_CONTROLLER_BUTTON_GUIDE:
            return GameControllerButton::Guide;
        case SDL_CONTROLLER_BUTTON_START:
            return GameControllerButton::Start;
        case SDL_CONTROLLER_BUTTON_LEFTSTICK:
            return GameControllerButton::LeftStick;
        case SDL_CONTROLLER_BUTTON_RIGHTSTICK:
            return GameControllerButton::RightStick;
        case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
            return GameControllerButton::LeftShoulder;
        case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
            return GameControllerButton::RightShoulder;
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
            return GameControllerButton::DPadUp;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
            return GameControllerButton::DPadDown;
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
            return GameControllerButton::DPadLeft;
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
            return GameControllerButton::DPadRight;
        default:
            modus_assert(false);
            return GameControllerButton::Total;
    }
}

static inline GameControllerAxis sdl_controller_axis_to_input_controller_axis(const u8 axis,
                                                                              i32& in_out_value) {
    switch (axis) {
        case SDL_CONTROLLER_AXIS_LEFTX:
            return GameControllerAxis::LeftX;
        case SDL_CONTROLLER_AXIS_LEFTY:
            in_out_value = -in_out_value;
            return GameControllerAxis::LeftY;
        case SDL_CONTROLLER_AXIS_RIGHTX:
            return GameControllerAxis::RightX;
        case SDL_CONTROLLER_AXIS_RIGHTY:
            in_out_value = -in_out_value;
            return GameControllerAxis::RightY;
        case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
            return GameControllerAxis::LeftTrigger;
        case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
            return GameControllerAxis::RightTrigger;
        default:
            modus_assert(false);
            return GameControllerAxis::Total;
    }
}

bool convert_event(InputEvent& event, const SDL_Event& sdl_event) {
    const u32 event_type = sdl_event.type;
    switch (event_type) {
        case SDL_KEYDOWN:
        case SDL_KEYUP: {
            // translate key;
            const InputKey input_key = sdl_scancode_to_input_Key(sdl_event.key.keysym.scancode);
            if (input_key >= InputKey::Total) {
                modus_assert_message(false, "Unkown key detected");
                return false;
            }
            // Create keyboard event
            event.type = InputEventType::Keyboard;
            event.keyboard.key = input_key;
            event.keyboard.state =
                (event_type == SDL_KEYDOWN) ? ButtonState::Down : ButtonState::Up;
            return true;
        }
        case SDL_MOUSEMOTION: {
            event.type = InputEventType::MouseMove;
            event.mouse_move.x = sdl_event.motion.x;
            event.mouse_move.y = sdl_event.motion.y;
            event.mouse_move.deltax = sdl_event.motion.xrel;
            event.mouse_move.deltay = sdl_event.motion.yrel;
            return true;
        }
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP: {
            event.type = InputEventType::MouseButton;
            event.mouse_button.button = static_cast<MouseButton>(sdl_event.button.button - 1);
            event.mouse_button.state =
                (event_type == SDL_MOUSEBUTTONDOWN) ? (ButtonState::Down) : (ButtonState::Up);
            event.mouse_button.x = sdl_event.button.x;
            event.mouse_button.y = sdl_event.button.y;
            return true;
        }

        case SDL_MOUSEWHEEL: {
            event.type = InputEventType::MouseScroll;
            event.mouse_scroll.xvalue = sdl_event.wheel.x;
            event.mouse_scroll.yvalue = -sdl_event.wheel.y;
            return true;
        }

        case SDL_CONTROLLERBUTTONUP:
        case SDL_CONTROLLERBUTTONDOWN: {
            event.type = InputEventType::GameControllerButton;
            event.controller_button.state =
                (event_type == SDL_CONTROLLERBUTTONDOWN) ? (ButtonState::Down) : (ButtonState::Up);
            event.controller_button.button =
                sdl_controller_button_to_input_controller_button(sdl_event.cbutton.button);
            event.controller_button.device_id = DeviceId(sdl_event.cbutton.which);
            return true;
        }
        case SDL_CONTROLLERAXISMOTION: {
            i32 value = sdl_event.caxis.value;
            event.type = InputEventType::GameControllerAxis;
            event.controller_axis.axis =
                sdl_controller_axis_to_input_controller_axis(sdl_event.caxis.axis, value);
            event.controller_axis.value = value;
            event.controller_axis.device_id = DeviceId(sdl_event.caxis.which);
            modus_assert(sdl_event.caxis.value >= GameControllerAxisEvent::kValueMin &&
                         sdl_event.caxis.value <= GameControllerAxisEvent::kValueMax);
            return true;
        }

        default:
            return false;
    }
}
}    // namespace modus::app::sdl2
