/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/event.hpp>
#include <sdl2/app_sdl2.hpp>
#include <sdl2/input_sdl2.hpp>
#include <sdl2/window_sdl2.hpp>

namespace modus::app::sdl2 {

AppSDL2::AppSDL2() : m_event_handler({nullptr, nullptr}) {}

AppResult<void> AppSDL2::initialize(const AppInitParams& params) {
    if (params.threed_api != threed::Api::OpenGLES3 && params.threed_api != threed::Api::Null) {
        return Error(AppErrorType(AppError::Init, "Unsupported Threed API\n"));
    }

    if (params.threed_api == threed::Api::OpenGLES3) {
        m_threed_instance = threed::create_gl_instance();
    } else {
        m_threed_instance = threed::create_null_instance();
    }

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER) != 0) {
        return Error(AppErrorType(AppError::Init, SDL_GetError()));
    }

    if (auto r = init_paths(params); !r) {
        return Error(r.error());
    }

    init_input_key_map();
    if (params.event_handler) {
        m_event_handler = params.event_handler;
    }

    m_gamepads.reserve(8);

    for (i32 i = 0; i < SDL_NumJoysticks(); ++i) {
        if (SDL_IsGameController(i)) {
            (void)add_game_controller(i);
        }
    }

    if (log::LogService::level() == log::Level::Debug) {
        SDL_DisplayMode display_mode;
        MODUS_LOGD("SDL2 DisplayModes:");
        for (int i = 0; i != SDL_GetNumVideoDisplays(); ++i) {
            int n_display_modes = SDL_GetNumDisplayModes(i);
            for (int j = 0; j < n_display_modes; ++j) {
                if (SDL_GetDisplayMode(i, j, &display_mode) != 0) {
                    MODUS_LOGE("    > {:02} - {:02}: Failed to get display mode: %s", i, j,
                               SDL_GetError());
                } else {
                    MODUS_LOGD("    > {:02} - {:02}: {} {}x{} {:02}Hz", i, j,
                               SDL_GetPixelFormatName(display_mode.format), display_mode.w,
                               display_mode.h, display_mode.refresh_rate);
                }
            }
            // const int result = SDL_GetCurrentDisplayMode(i, &display_mode);
        }
    }
    return Ok<>();
}

AppResult<void> AppSDL2::shutdown() {
    // destroy window
    m_window.reset();

    if (m_threed_instance) {
        m_threed_instance->shutdown();
        m_threed_instance.reset();
    }
    for (auto& id : m_gamepads) {
        (void)remove_game_controller(id);
    }
    m_gamepads.clear();

    // Shutdown SDL2
    SDL_Quit();
    return Ok<>();
}

TickResult AppSDL2::tick() {
    SDL_Event sdl_event;
    Event app_event;
    while (SDL_PollEvent(&sdl_event)) {
        const u32 event_type = sdl_event.type;

        switch (event_type) {
            case SDL_QUIT:
                return TickResult::Quit;

            case SDL_WINDOWEVENT: {
                switch (sdl_event.window.event) {
                    case SDL_WINDOWEVENT_RESIZED: {
                        app_event.type = EventType::Window;
                        app_event.window.type = WindowEventType::Resize;
                        app_event.window.window = m_window.get();
                        app_event.window.resize.width = sdl_event.window.data1;
                        app_event.window.resize.height = sdl_event.window.data2;
                        auto optional = m_window->compositor();
                        CompositorSDL2* compositor =
                            static_cast<CompositorSDL2*>(optional.value().get());
                        if (compositor != nullptr) {
                            compositor->on_window_resize(sdl_event.window.windowID,
                                                         sdl_event.window.data1,
                                                         sdl_event.window.data2);
                        }
                        if (m_event_handler) {
                            m_event_handler(app_event);
                        }
                        break;
                    }
                    case SDL_WINDOWEVENT_LEAVE: {
                        app_event.type = EventType::Window;
                        app_event.window.type = WindowEventType::Leave;
                        app_event.window.window = m_window.get();
                        if (m_event_handler) {
                            m_event_handler(app_event);
                        }
                        break;
                    }
                    case SDL_WINDOWEVENT_ENTER: {
                        app_event.type = EventType::Window;
                        app_event.window.type = WindowEventType::Enter;
                        app_event.window.window = m_window.get();
                        if (m_event_handler) {
                            m_event_handler(app_event);
                        }
                        break;
                    }
                    case SDL_WINDOWEVENT_HIDDEN:
                        // TODO: Paused
                        break;
                    case SDL_WINDOWEVENT_SHOWN:
                    case SDL_WINDOWEVENT_EXPOSED:
                        // TODO: Unpause

                    default:
                        break;
                }
                break;
            }

            case SDL_KEYDOWN:
            case SDL_KEYUP:
            case SDL_MOUSEMOTION:
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            case SDL_CONTROLLERAXISMOTION:
            case SDL_CONTROLLERBUTTONUP:
            case SDL_CONTROLLERBUTTONDOWN:
            case SDL_MOUSEWHEEL: {
                app_event.type = EventType::Input;
                const bool did_process = convert_event(app_event.input, sdl_event);
                MODUS_UNUSED(did_process);
                modus_assert(did_process);
                if (m_event_handler) {
                    m_event_handler(app_event);
                }
                break;
            }
            case SDL_CONTROLLERDEVICEADDED: {
                if (SDL_IsGameController(sdl_event.cdevice.which)) {
                    if (auto r = add_game_controller(sdl_event.cdevice.which); r) {
                        app_event.type = EventType::GameControllerAdded;
                        app_event.controller_added.id = *r;
                        if (m_event_handler) {
                            m_event_handler(app_event);
                        }
                    }
                }
                break;
            }
            case SDL_CONTROLLERDEVICEREMOVED: {
                if (sdl_event.cdevice.which >= 0) {
                    const DeviceId id(sdl_event.cdevice.which);
                    if (remove_game_controller(id)) {
                        app_event.type = EventType::GameControllerRemoved;
                        app_event.controller_removed.id = id;
                        if (m_event_handler) {
                            m_event_handler(app_event);
                        }
                    }
                }
                break;
            }

            default:
                continue;
        }
    }
    return TickResult::Continue;
}

AppResult<NotMyPtr<Window>> AppSDL2::create_window(const WindowCreateParams& params) {
    if (m_window) {
        return Error(AppErrorType(AppError::WindowExists, "A Window has already been created"));
    }

    Result<std::unique_ptr<WindowSDL2>, String> window_create_result =
        sdl2::WindowSDL2::create(*this, params);
    if (!window_create_result) {
        return Error(AppErrorType(AppError::WindowCreate, window_create_result.error()));
    }

    m_window = window_create_result.release_value();

    if (!m_threed_instance->initialize()) {
        return Error(
            AppErrorType(AppError::WindowCreate, "Failed to initialize threed instance\n"));
        m_window.reset();
    }

    return Ok(NotMyPtr<Window>(static_cast<Window*>(m_window.get())));
}

Optional<NotMyPtr<Window>> AppSDL2::main_window() {
    if (!m_window) {
        return Optional<NotMyPtr<Window>>();
    }
    return NotMyPtr<Window>(static_cast<Window*>(m_window.get()));
}

Result<String, void> AppSDL2::impl_path_resources() {
    const char* base_path = SDL_GetBasePath();
    if (base_path == nullptr) {
        return Error<>();
    }
    return Ok(String(base_path));
}

void AppSDL2::lock_mouse(const bool value) {
    if (value) {
        SDL_ShowCursor(SDL_FALSE);
        SDL_SetRelativeMouseMode(SDL_TRUE);
    } else {
        SDL_ShowCursor(SDL_TRUE);
        SDL_SetRelativeMouseMode(SDL_FALSE);
    }
}

bool AppSDL2::is_mouse_locked() const {
    return SDL_GetRelativeMouseMode() == SDL_TRUE;
}

Slice<DeviceId> AppSDL2::game_controllers() const {
    return make_slice(m_gamepads);
}

Result<DeviceId, void> AppSDL2::add_game_controller(const i32 index) {
    modus_assert(SDL_IsGameController(index));
    SDL_GameController* controller = SDL_GameControllerOpen(index);
    if (controller != nullptr) {
        SDL_Joystick* joystick = SDL_GameControllerGetJoystick(controller);
        const i32 instance_id = SDL_JoystickInstanceID(joystick);
        if (instance_id >= 0) {
            m_gamepads.push_back(DeviceId(instance_id));
            return Ok(DeviceId(instance_id));
        }
    }
    return Error<>();
}

Result<> AppSDL2::remove_game_controller(const DeviceId id) {
    auto it = std::find(m_gamepads.begin(), m_gamepads.end(), id);
    if (it != m_gamepads.end()) {
        SDL_GameController* controller = SDL_GameControllerFromInstanceID(id);
        if (controller != nullptr) {
            SDL_GameControllerClose(controller);
            return Ok<>();
        }
    }
    return Error<>();
}

}    // namespace modus::app::sdl2
