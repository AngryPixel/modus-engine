/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <app/window.hpp>
#include <sdl2/compositor_sdl2.hpp>

extern "C" {
struct SDL_Window;
}

namespace modus::threed {
class Instance;
}

namespace modus::app::sdl2 {
class ContextGLESSDL2;
class AppSDL2;
template <typename T>
using SDL2Ptr = std::unique_ptr<T, void (*)(T*)>;

// TODO: Cleanup
class WindowSDL2 final : Window {
    friend class AppSDL2;

   private:
    AppSDL2& m_app;
    SDL2Ptr<SDL_Window> m_window;
    std::unique_ptr<threed::Compositor> m_compositor;
    std::unique_ptr<threed::IContext> m_window_context;

   public:
    WindowSDL2(AppSDL2& app, SDL2Ptr<SDL_Window>&& window);

    virtual ~WindowSDL2();

    static Result<std::unique_ptr<WindowSDL2>, String> create(AppSDL2& app,
                                                              const WindowCreateParams& params);

    WindowState window_state() const override;

    void set_window_state(const WindowState& state) override;

    Result<NotMyPtr<threed::Compositor>, void> create_compositor(threed::Device& device,
                                                                 const bool vsync) override;

    NotMyPtr<threed::IContext> window_context() override;

    Optional<NotMyPtr<threed::Compositor>> compositor() override;

    Result<std::unique_ptr<threed::IContext>, String> create_context(
        NotMyPtr<threed::IContext> shared) override;

    void set_title(const StringSlice title) override;

    SDL_Window* sdl_window() { return m_window.get(); }

   private:
    Result<void, String> create_and_bind_context();
};

}    // namespace modus::app::sdl2
