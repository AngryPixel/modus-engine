/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <threed/compositor.hpp>

extern "C" {
struct SDL_Window;
}

namespace modus::threed {
class Instance;
class Device;

}    // namespace modus::threed

namespace modus::app::sdl2 {

class CompositorSDL2 final : public modus::threed::Compositor {
   private:
    SDL_Window* m_window;

   public:
    CompositorSDL2();

    ~CompositorSDL2() override;

    Result<> create(SDL_Window& window,
                    threed::Instance& instance,
                    threed::Device& device,
                    const bool vsync);

    void destroy();

    Result<void, String> update(threed::Device& device) override;

    void swap_buffers() override;

    void on_window_resize(const u32 window_id, const i32 width, const i32 height);
};

}    // namespace modus::app::sdl2
