﻿/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <sdl2/compositor_sdl2.hpp>
#include <threed/device.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/instance.hpp>

namespace modus::app::sdl2 {

CompositorSDL2::CompositorSDL2() : m_window(nullptr) {}

CompositorSDL2::~CompositorSDL2() {
    destroy();
    modus_assert(m_window == nullptr);
}

Result<> CompositorSDL2::create(SDL_Window& window,
                                threed::Instance& instance,
                                threed::Device& device,
                                const bool vsync) {
    modus_assert(m_window == nullptr);
    m_window = &window;
    MODUS_UNUSED(device);
    Optional<threed::FramebufferHandle> default_framebuffer = instance.default_framebuffer();
    if (!default_framebuffer) {
        modus_assert_message(false, "Not yet handled!");
        return Error<>();
    }
    m_frame_buffer = default_framebuffer.value();
    SDL_GetWindowSize(&window, &m_current_width, &m_current_height);

    m_vsync = vsync;
    if (SDL_GL_SetSwapInterval(m_vsync ? 1 : 0) != 0) {
        return Error<>();
    }

    return Ok<>();
}

void CompositorSDL2::destroy() {
    m_window = nullptr;
}

Result<void, String> CompositorSDL2::update(threed::Device&) {
    // Not much to do here, just indicate that we have updated our size
    // as requested.
    m_current_width = m_new_width;
    m_current_height = m_new_height;
    m_requires_update = false;

    if (m_new_vsync != m_vsync) {
        MODUS_LOGW("Chaning VSync when rendering causes issues. Investigate");
        /* if (SDL_GL_SetSwapInterval(m_new_vsync ? 1 : 0) != 0) {
             String str =
                 modus::format("Failed to set vsync: {}", SDL_GetError());
             return Error(std::move(str));
         }
         m_vsync = m_new_vsync;*/
    }

    return Ok<>();
}

void CompositorSDL2::swap_buffers() {
    modus_assert(m_window != nullptr);
    SDL_GL_SwapWindow(m_window);
}

void CompositorSDL2::on_window_resize(const u32 window_id, const i32 width, const i32 height) {
    modus_assert(m_window);
    modus_assert(window_id == SDL_GetWindowID(m_window))(void) window_id;
    m_new_width = width;
    m_new_height = height;
    m_requires_update = true;
}

}    // namespace modus::app::sdl2
