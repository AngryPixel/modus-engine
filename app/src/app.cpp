/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <app_private.hpp>
#include <os/path.hpp>

#if defined(MODUS_APP_USE_SDL2)
#include <sdl2/app_sdl2.hpp>
using AppType = modus::app::sdl2::AppSDL2;
#else
#error No known application impl
#endif

#include <threed/instance.hpp>

namespace modus::app {

App::App() {}

App::~App() {}

int App::run() {
    while (TickResult::Quit != tick()) {
    }
    return 0;
}

AppResult<void> App::init_paths(const AppInitParams& params) {
    m_name = params.name.to_str();
    m_name_pref = params.name_preferences.to_str();
    {
        auto path_result = os::Path::os_temp_path();
        if (!path_result) {
            return Error(AppErrorType(AppError::Init, "Failed to retrievve temp path"));
        }
        m_path_tmp = path_result.release_value();
    }
    {
        auto path_result = os::Path::os_user_cache_path();
        if (!path_result) {
            return Error(AppErrorType(AppError::Init, "Failed to retrieve cache path"));
        }
        m_path_cache = path_result.release_value();
    }
    {
        auto path_result = os::Path::os_user_pref_config_path();
        if (!path_result) {
            return Error(
                AppErrorType(AppError::Init, "Failed to retrieve preferences config path"));
        }
        m_path_pref_config = path_result.release_value();
    }
    {
        auto path_result = os::Path::os_user_pref_data_path();
        if (!path_result) {
            return Error(AppErrorType(AppError::Init, "Failed to retrieve preferences data path"));
        }
        m_path_pref_data = path_result.release_value();
    }
    {
        auto path_result = impl_path_resources();
        if (!path_result) {
            return Error(AppErrorType(AppError::Init, "Failed to retrieve resource path"));
        }
        m_path_resources = path_result.release_value();
    }

    os::Path::join_inplace(m_path_tmp, m_name_pref);
    os::Path::join_inplace(m_path_cache, m_name_pref);
    os::Path::join_inplace(m_path_pref_config, m_name_pref);
    os::Path::join_inplace(m_path_pref_data, m_name_pref);

    if (!os::Path::make_directory(m_path_tmp)) {
        std::string error_msg = fmt::format("Failed to create directory : {}", m_path_tmp);
        return Error(AppErrorType(AppError::Init, StringSlice(error_msg)));
    }

    if (!os::Path::make_directory(m_path_cache)) {
        std::string error_msg = fmt::format("Failed to create directory : {}", m_path_cache);
        return Error(AppErrorType(AppError::Init, StringSlice(error_msg)));
    }

    if (!os::Path::make_directory(m_path_pref_data)) {
        std::string error_msg = fmt::format("Failed to create directory : {}", m_path_pref_data);
        return Error(AppErrorType(AppError::Init, StringSlice(error_msg)));
    }

    if (!os::Path::make_directory(m_path_pref_config)) {
        std::string error_msg = fmt::format("Failed to create directory : {}", m_path_pref_config);
        return Error(AppErrorType(AppError::Init, StringSlice(error_msg)));
    }

    // TODO: Get resources path!
    return Ok<>();
}

void App::reset_paths() {
    m_name.clear();
    m_path_tmp.clear();
    m_path_cache.clear();
    m_path_pref_data.clear();
    m_path_pref_config.clear();
    m_path_resources.clear();
}

std::unique_ptr<App> create_app() {
    return modus::make_unique<AppType>();
}

}    // namespace modus::app
