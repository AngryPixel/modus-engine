/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/input.hpp>

namespace modus::app {
char app_input_event_to_ascii(const KeyboardEvent& event) {
    char result = '?';
    {
        switch (event.key) {
            case InputKey::KeyA:
                result = 'a';
                break;
            case InputKey::KeyB:
                result = 'b';
                break;
            case InputKey::KeyC:
                result = 'c';
                break;
            case InputKey::KeyD:
                result = 'd';
                break;
            case InputKey::KeyE:
                result = 'e';
                break;
            case InputKey::KeyF:
                result = 'f';
                break;
            case InputKey::KeyG:
                result = 'g';
                break;
            case InputKey::KeyH:
                result = 'h';
                break;
            case InputKey::KeyI:
                result = 'i';
                break;
            case InputKey::KeyJ:
                result = 'j';
                break;
            case InputKey::KeyK:
                result = 'k';
                break;
            case InputKey::KeyL:
                result = 'l';
                break;
            case InputKey::KeyM:
                result = 'm';
                break;
            case InputKey::KeyN:
                result = 'n';
                break;
            case InputKey::KeyO:
                result = 'o';
                break;
            case InputKey::KeyP:
                result = 'p';
                break;
            case InputKey::KeyQ:
                result = 'q';
                break;
            case InputKey::KeyR:
                result = 'r';
                break;
            case InputKey::KeyS:
                result = 's';
                break;
            case InputKey::KeyT:
                result = 't';
                break;
            case InputKey::KeyU:
                result = 'u';
                break;
            case InputKey::KeyV:
                result = 'v';
                break;
            case InputKey::KeyW:
                result = 'w';
                break;
            case InputKey::KeyX:
                result = 'x';
                break;
            case InputKey::KeyY:
                result = 'y';
                break;
            case InputKey::KeyZ:
                result = 'z';
                break;
            case InputKey::KeyKP0:
            case InputKey::Key0:
                result = '0';
                break;
            case InputKey::KeyKP1:
            case InputKey::Key1:
                result = '1';
                break;
            case InputKey::KeyKP2:
            case InputKey::Key2:
                result = '2';
                break;
            case InputKey::KeyKP3:
            case InputKey::Key3:
                result = '3';
                break;
            case InputKey::KeyKP4:
            case InputKey::Key4:
                result = '4';
                break;
            case InputKey::KeyKP5:
            case InputKey::Key5:
                result = '5';
                break;
            case InputKey::KeyKP6:
            case InputKey::Key6:
                result = '6';
                break;
            case InputKey::KeyKP7:
            case InputKey::Key7:
                result = '7';
                break;
            case InputKey::KeyKP8:
            case InputKey::Key8:
                result = '8';
                break;
            case InputKey::KeyKP9:
            case InputKey::Key9:
                result = '9';
                break;
            case InputKey::KeyTilde:
                result = '~';
                break;
            case InputKey::KeyKPPlus:
            case InputKey::KeyPlus:
                result = '+';
                break;
            case InputKey::KeyKPMinus:
            case InputKey::KeyMinus:
                result = '-';
                break;
            case InputKey::KeyBackSlash:
                result = '\\';
                break;
            case InputKey::KeyKPPeriod:
            case InputKey::KeyPeriod:
                result = '.';
                break;
            case InputKey::KeyComma:
                result = ',';
                break;
            case InputKey::KeySlash:
                result = '/';
                break;
            case InputKey::KeyColon:
                result = ':';
                break;
            case InputKey::KeyApostrophe:
                result = '\'';
                break;
            case InputKey::KeyEquals:
                result = '=';
                break;
            case InputKey::KeyLeftBracket:
                result = '[';
                break;
            case InputKey::KeyRightBracket:
                result = ']';
                break;
            case InputKey::KeySemiColon:
                result = ';';
                break;
            case InputKey::KeyKPDivide:
                result = '/';
                break;
            case InputKey::KeyKPMulitply:
                result = '*';
                break;
            default:
                break;
        }
    }
    return result;
}

#define INPUT_KEY_TO_STR(KEY) #KEY
static const StringSlice k_input_key_str[static_cast<u32>(InputKey::Total)] = {
    INPUT_KEY_TO_STR(KeyA),           INPUT_KEY_TO_STR(KeyB),
    INPUT_KEY_TO_STR(KeyC),           INPUT_KEY_TO_STR(KeyD),
    INPUT_KEY_TO_STR(KeyE),           INPUT_KEY_TO_STR(KeyF),
    INPUT_KEY_TO_STR(KeyG),           INPUT_KEY_TO_STR(KeyH),
    INPUT_KEY_TO_STR(KeyI),           INPUT_KEY_TO_STR(KeyJ),
    INPUT_KEY_TO_STR(KeyK),           INPUT_KEY_TO_STR(KeyL),
    INPUT_KEY_TO_STR(KeyM),           INPUT_KEY_TO_STR(KeyN),
    INPUT_KEY_TO_STR(KeyO),           INPUT_KEY_TO_STR(KeyP),
    INPUT_KEY_TO_STR(KeyQ),           INPUT_KEY_TO_STR(KeyR),
    INPUT_KEY_TO_STR(KeyS),           INPUT_KEY_TO_STR(KeyT),
    INPUT_KEY_TO_STR(KeyU),           INPUT_KEY_TO_STR(KeyV),
    INPUT_KEY_TO_STR(KeyW),           INPUT_KEY_TO_STR(KeyX),
    INPUT_KEY_TO_STR(KeyY),           INPUT_KEY_TO_STR(KeyZ),
    INPUT_KEY_TO_STR(Key0),           INPUT_KEY_TO_STR(Key1),
    INPUT_KEY_TO_STR(Key2),           INPUT_KEY_TO_STR(Key3),
    INPUT_KEY_TO_STR(Key4),           INPUT_KEY_TO_STR(Key5),
    INPUT_KEY_TO_STR(Key6),           INPUT_KEY_TO_STR(Key7),
    INPUT_KEY_TO_STR(Key8),           INPUT_KEY_TO_STR(Key9),
    INPUT_KEY_TO_STR(KeyUp),          INPUT_KEY_TO_STR(KeyDown),
    INPUT_KEY_TO_STR(KeyRight),       INPUT_KEY_TO_STR(KeyLeft),
    INPUT_KEY_TO_STR(KeySpace),       INPUT_KEY_TO_STR(KeyLeftShift),
    INPUT_KEY_TO_STR(KeyRightShift),  INPUT_KEY_TO_STR(KeyLeftCtrl),
    INPUT_KEY_TO_STR(KeyRightCtrl),   INPUT_KEY_TO_STR(KeyLeftAlt),
    INPUT_KEY_TO_STR(KeyRightAlt),    INPUT_KEY_TO_STR(KeyTab),
    INPUT_KEY_TO_STR(KeyTilde),       INPUT_KEY_TO_STR(KeyPlus),
    INPUT_KEY_TO_STR(KeyMinus),       INPUT_KEY_TO_STR(KeyBackSlash),
    INPUT_KEY_TO_STR(KeyPeriod),      INPUT_KEY_TO_STR(KeyComma),
    INPUT_KEY_TO_STR(KeySlash),       INPUT_KEY_TO_STR(KeyColon),
    INPUT_KEY_TO_STR(KeyApostrophe),  INPUT_KEY_TO_STR(KeyBackSpace),
    INPUT_KEY_TO_STR(KeyEnter),       INPUT_KEY_TO_STR(KeyCapsLock),
    INPUT_KEY_TO_STR(KeyEscape),      INPUT_KEY_TO_STR(KeyEquals),
    INPUT_KEY_TO_STR(KeyLeftBracket), INPUT_KEY_TO_STR(KeyRightBracket),
    INPUT_KEY_TO_STR(KeySemiColon),   INPUT_KEY_TO_STR(KeyF1),
    INPUT_KEY_TO_STR(KeyF2),          INPUT_KEY_TO_STR(KeyF3),
    INPUT_KEY_TO_STR(KeyF4),          INPUT_KEY_TO_STR(KeyF5),
    INPUT_KEY_TO_STR(KeyF6),          INPUT_KEY_TO_STR(KeyF7),
    INPUT_KEY_TO_STR(KeyF8),          INPUT_KEY_TO_STR(KeyF9),
    INPUT_KEY_TO_STR(KeyF10),         INPUT_KEY_TO_STR(KeyF11),
    INPUT_KEY_TO_STR(KeyF12),         INPUT_KEY_TO_STR(KeyPrintScreen),
    INPUT_KEY_TO_STR(KeyScrollLock),  INPUT_KEY_TO_STR(KeyPause),
    INPUT_KEY_TO_STR(KeyHome),        INPUT_KEY_TO_STR(KeyDelete),
    INPUT_KEY_TO_STR(KeyInsert),      INPUT_KEY_TO_STR(KeyEnd),
    INPUT_KEY_TO_STR(KeyPgUp),        INPUT_KEY_TO_STR(KeyPgDown),
    INPUT_KEY_TO_STR(KeyKPDivide),    INPUT_KEY_TO_STR(KeyKPMulitply),
    INPUT_KEY_TO_STR(KeyKPPlus),      INPUT_KEY_TO_STR(KeyKPMinus),
    INPUT_KEY_TO_STR(KeyKPEnter),     INPUT_KEY_TO_STR(KeyKPPeriod),
    INPUT_KEY_TO_STR(KeyKP0),         INPUT_KEY_TO_STR(KeyKP1),
    INPUT_KEY_TO_STR(KeyKP2),         INPUT_KEY_TO_STR(KeyKP3),
    INPUT_KEY_TO_STR(KeyKP4),         INPUT_KEY_TO_STR(KeyKP5),
    INPUT_KEY_TO_STR(KeyKP6),         INPUT_KEY_TO_STR(KeyKP7),
    INPUT_KEY_TO_STR(KeyKP8),         INPUT_KEY_TO_STR(KeyKP9),
    INPUT_KEY_TO_STR(KeyLeftOS),      INPUT_KEY_TO_STR(KeyRightOS)};

const StringSlice app_input_key_to_string(const InputKey key) {
    return (key < InputKey::Total) ? k_input_key_str[static_cast<u32>(key)] : "Unknown";
}

StringSlice game_controller_button_to_string(const GameControllerButton button) {
    switch (button) {
        case GameControllerButton::A:
            return "A";
        case GameControllerButton::B:
            return "B";
        case GameControllerButton::X:
            return "X";
        case GameControllerButton::Y:
            return "Y";
        case GameControllerButton::Start:
            return "Start";
        case GameControllerButton::Guide:
            return "Guide";
        case GameControllerButton::Back:
            return "Back";
        case GameControllerButton::LeftShoulder:
            return "LeftShoulder";
        case GameControllerButton::RightShoulder:
            return "RightShoulder";
        case GameControllerButton::LeftStick:
            return "LeftStick";
        case GameControllerButton::RightStick:
            return "RightStick";
        case GameControllerButton::DPadUp:
            return "DPadUp";
        case GameControllerButton::DPadDown:
            return "DPadDown";
        case GameControllerButton::DPadLeft:
            return "DPadLeft";
        case GameControllerButton::DPadRight:
            return "DPadRight";
        default:
            modus_assert_message(false, "Should not be reached!");
            return "Uknown";
    }
}

StringSlice game_controller_axis_to_string(const GameControllerAxis axis) {
    switch (axis) {
        case GameControllerAxis::LeftX:
            return "LeftX";
        case GameControllerAxis::LeftY:
            return "LeftY";
        case GameControllerAxis::RightX:
            return "RightX";
        case GameControllerAxis::RightY:
            return "RightY";
        case GameControllerAxis::LeftTrigger:
            return "LeftTrigger";
        case GameControllerAxis::RightTrigger:
            return "RighteTrigger";
        default:
            modus_assert_message(false, "Should not be reached!");
            return "Uknown";
    }
}

}    // namespace modus::app
