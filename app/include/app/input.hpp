/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <app/module_config.hpp>
#include <core/core.hpp>
#include <core/string_slice.hpp>

namespace modus::app {

using DeviceId = u32;

enum class InputEventType {
    Keyboard,
    MouseButton,
    MouseMove,
    MouseScroll,
    Text,
    GameControllerAxis,
    GameControllerButton
};

enum class MouseButton : u8 {
    Left = 0,
    Middle,
    Right,
    Extra0,
    Extra1,
    Total,
};

enum class InputKey : u8 {
    KeyA = 0,
    KeyB,
    KeyC,
    KeyD,
    KeyE,
    KeyF,
    KeyG,
    KeyH,
    KeyI,
    KeyJ,
    KeyK,
    KeyL,
    KeyM,
    KeyN,
    KeyO,
    KeyP,
    KeyQ,
    KeyR,
    KeyS,
    KeyT,
    KeyU,
    KeyV,
    KeyW,
    KeyX,
    KeyY,
    KeyZ,
    Key0,
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    KeyUp,
    KeyDown,
    KeyRight,
    KeyLeft,
    KeySpace,
    KeyLeftShift,
    KeyRightShift,
    KeyLeftCtrl,
    KeyRightCtrl,
    KeyLeftAlt,
    KeyRightAlt,
    KeyTab,
    KeyTilde,
    KeyPlus,
    KeyMinus,
    KeyBackSlash,
    KeyPeriod,
    KeyComma,
    KeySlash,
    KeyColon,
    KeyApostrophe,
    KeyBackSpace,
    KeyEnter,
    KeyCapsLock,
    KeyEscape,
    KeyEquals,
    KeyLeftBracket,
    KeyRightBracket,
    KeySemiColon,
    KeyF1,
    KeyF2,
    KeyF3,
    KeyF4,
    KeyF5,
    KeyF6,
    KeyF7,
    KeyF8,
    KeyF9,
    KeyF10,
    KeyF11,
    KeyF12,
    KeyPrintScreen,
    KeyScrollLock,
    KeyPause,
    KeyHome,
    KeyDelete,
    KeyInsert,
    KeyEnd,
    KeyPgUp,
    KeyPgDown,
    KeyKPDivide,
    KeyKPMulitply,
    KeyKPPlus,
    KeyKPMinus,
    KeyKPEnter,
    KeyKPPeriod,
    KeyKP0,
    KeyKP1,
    KeyKP2,
    KeyKP3,
    KeyKP4,
    KeyKP5,
    KeyKP6,
    KeyKP7,
    KeyKP8,
    KeyKP9,
    KeyLeftOS,
    KeyRightOS,
    // place holder do not use
    Total,
};

enum class GameControllerAxis : u8 {
    LeftX = 0,
    LeftY,
    RightX,
    RightY,
    LeftTrigger,
    RightTrigger,
    Total,
};

enum class GameControllerButton : u8 {
    A = 0,
    B,
    X,
    Y,
    Back,
    Guide,
    Start,
    LeftStick,
    RightStick,
    RightShoulder,
    LeftShoulder,
    DPadUp,
    DPadDown,
    DPadLeft,
    DPadRight,
    Total
};

enum class ButtonState : u8 { Down = 1, Up = 2 };

struct MODUS_APP_EXPORT KeyboardEvent {
    ButtonState state;
    InputKey key;

    inline bool is_key_down() const { return (this->state == ButtonState::Down); }

    inline bool is_key_up() const { return (this->state == ButtonState::Up); }
};

struct MODUS_APP_EXPORT MouseButtonEvent {
    ButtonState state;
    MouseButton button;
    i32 x;
    i32 y;

    inline bool is_mouse_down() const { return this->state == ButtonState::Down; }

    inline bool is_mouse_up() const { return this->state == ButtonState::Up; }
};

struct MODUS_APP_EXPORT MouseScrollEvent {
    i32 xvalue;
    i32 yvalue;
};

struct MODUS_APP_EXPORT MouseMoveEvent {
    i32 x;
    i32 y;
    i32 deltax;
    i32 deltay;
};

struct MODUS_APP_EXPORT GameControllerAxisEvent {
    static constexpr i32 kValueMax = 32767;
    static constexpr i32 kValueMin = -32768;
    DeviceId device_id;
    GameControllerAxis axis;
    i32 value;

    inline f32 normalized() const { return f32(value) / kValueMax; }

    inline f32 normalized_positive_only() const {
        if (value < 0) {
            return 0.0f;
        }
        return normalized();
    }
};

struct MODUS_APP_EXPORT GameControllerButtonEvent {
    DeviceId device_id;
    GameControllerButton button;
    ButtonState state;

    inline bool is_button_down() const { return (this->state == ButtonState::Down); }

    inline bool is_button_up() const { return (this->state == ButtonState::Up); }
};

struct MODUS_APP_EXPORT InputEvent {
    InputEventType type;
    union {
        KeyboardEvent keyboard;
        MouseButtonEvent mouse_button;
        MouseMoveEvent mouse_move;
        MouseScrollEvent mouse_scroll;
        GameControllerButtonEvent controller_button;
        GameControllerAxisEvent controller_axis;
    };
};

MODUS_APP_EXPORT char app_input_event_to_ascii(const KeyboardEvent& event);

MODUS_APP_EXPORT const StringSlice app_input_key_to_string(const InputKey key);

MODUS_APP_EXPORT StringSlice game_controller_button_to_string(const GameControllerButton button);

MODUS_APP_EXPORT StringSlice game_controller_axis_to_string(const GameControllerAxis axis);

}    // namespace modus::app

template <>
struct fmt::formatter<modus::app::KeyboardEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::KeyboardEvent& event, FormatContext& ctx) {
        const modus::app::InputKey key = event.key;
        return format_to(ctx.out(), "Keyboard(Key:{}, ASCII: {} State:{})",
                         app_input_key_to_string(key), app_input_event_to_ascii(event),
                         event.is_key_down() ? "DOWN" : "UP");
    }
};

template <>
struct fmt::formatter<modus::app::MouseButtonEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::MouseButtonEvent& event, FormatContext& ctx) {
        return format_to(ctx.out(), "MouseButton(Button:{}, x:{}, y:{}, State:{})", event.button,
                         event.x, event.y, event.is_mouse_down() ? "DOWN" : "UP");
    }
};

template <>
struct fmt::formatter<modus::app::MouseMoveEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::MouseMoveEvent& event, FormatContext& ctx) {
        return format_to(ctx.out(), "MouseMove(x:{}, y:{}, dx:{}, dy:{})", event.x, event.y,
                         event.deltax, event.deltay);
    }
};

template <>
struct fmt::formatter<modus::app::MouseScrollEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::MouseScrollEvent& event, FormatContext& ctx) {
        return format_to(ctx.out(), "MouseScroll(x:{}, y:{})", event.xvalue, event.yvalue);
    }
};

template <>
struct fmt::formatter<modus::app::GameControllerButtonEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::GameControllerButtonEvent& event, FormatContext& ctx) {
        return format_to(ctx.out(), "GameControllerButton(Device:{} Button:{}, State:{})",
                         event.device_id, game_controller_button_to_string(event.button),
                         (event.is_button_down()) ? "Down" : "Up");
    }
};

template <>
struct fmt::formatter<modus::app::GameControllerAxisEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::GameControllerAxisEvent& event, FormatContext& ctx) {
        return format_to(ctx.out(), "GameControllerAxis(Device: {} Axis:{}, Value:{})",
                         event.device_id, game_controller_axis_to_string(event.axis), event.value);
    }
};

template <>
struct fmt::formatter<modus::app::InputEvent> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::app::InputEvent& event, FormatContext& ctx) {
        auto result = format_to(ctx.out(), "InputEvent {{ ");
        switch (event.type) {
            case modus::app::InputEventType::Keyboard:
                result = format_to(result, "{} }}", event.keyboard);
                break;
            case modus::app::InputEventType::MouseButton:
                result = format_to(result, "{} }}", event.mouse_button);
                break;
            case modus::app::InputEventType::MouseMove:
                result = format_to(result, "{} }}", event.mouse_move);
                break;
            case modus::app::InputEventType::MouseScroll:
                result = format_to(result, "{} }}", event.mouse_scroll);
                break;
            case modus::app::InputEventType::GameControllerButton:
                result = format_to(result, "{} }}", event.controller_button);
                break;
            case modus::app::InputEventType::GameControllerAxis:
                result = format_to(result, "{} }}", event.controller_axis);
                break;
            default:
                result = format_to(result, "{} }}", "Unknown");
                break;
        }
        return result;
    }
};
