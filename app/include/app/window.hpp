/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <app/module_config.hpp>
#include <core/core.hpp>
#include <threed/instance.hpp>

namespace modus::threed {
class Compositor;
class Instance;
class Device;
}    // namespace modus::threed

namespace modus::app {

class App;

enum class WindowFullscreenMode { None, Exclusive, Windowed, Borderless };

struct MODUS_APP_EXPORT WindowCreateParams {
    StringSlice title;
    Optional<i32> width;
    Optional<i32> height;
    WindowFullscreenMode fullscreen = WindowFullscreenMode::None;
    bool debug = false;
    bool hidden = false;
    struct {
        u8 red_bits = 8;
        u8 green_bits = 8;
        u8 blue_bits = 8;
        u8 depth_bits = 24;
        u8 stencil_bits = 0;
        bool srgb = false;
        bool double_bufferde = true;
    } framebuffer;
};

struct MODUS_APP_EXPORT WindowState {
    i32 x = 0;
    i32 y = 0;
    i32 width = 0;
    i32 height = 0;
    WindowFullscreenMode fullscreen = WindowFullscreenMode::None;
};

class MODUS_APP_EXPORT Window {
   public:
    virtual ~Window() = default;

    virtual Result<NotMyPtr<threed::Compositor>, void> create_compositor(
        threed::Device& device,
        const bool vsync = true) = 0;

    virtual Optional<NotMyPtr<threed::Compositor>> compositor() = 0;

    virtual WindowState window_state() const = 0;

    virtual void set_window_state(const WindowState& state) = 0;

    virtual NotMyPtr<threed::IContext> window_context() = 0;

    virtual void set_title(const StringSlice title) = 0;

    virtual Result<std::unique_ptr<threed::IContext>, String> create_context(
        NotMyPtr<threed::IContext> shared) = 0;
};

}    // namespace modus::app
