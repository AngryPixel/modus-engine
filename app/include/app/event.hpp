/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <app/input.hpp>

namespace modus::app {
class Window;
enum class EventType { Input, Window, GameControllerAdded, GameControllerRemoved };

enum class WindowEventType { Resize, Enter, Leave };

struct WindowResizeEvent {
    u32 width;
    u32 height;
};

struct WindowEvent {
    WindowEventType type;
    Window* window;
    union {
        WindowResizeEvent resize;
    };
};

struct GameControllerAddedEvent {
    DeviceId id;
};

struct GameControllerRemovedEvent {
    DeviceId id;
};

struct Event {
    EventType type;
    union {
        InputEvent input;
        WindowEvent window;
        GameControllerAddedEvent controller_added;
        GameControllerRemovedEvent controller_removed;
    };
};

}    // namespace modus::app
