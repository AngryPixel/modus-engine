/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// clang-format off
#include <core/core.hpp>
#include <app/module_config.hpp>
// clang-format on

#include <core/pointers.hpp>
#include <core/enum_string.hpp>
#include <core/string_slice.hpp>
#include <core/delegate.hpp>

#include <app/input.hpp>
#include <app/window.hpp>
#include <threed/threed.hpp>

namespace modus::threed {
class Compositor;
class Instance;
}    // namespace modus::threed

namespace modus::app {

enum class AppError {
    Init,
    InvalidParams,
    NoWindow,
    WindowExists,
    WindowCreate,
    CompositorCreate,
    Unknown
};

using AppErrorType = EnumString<AppError>;

template <typename T>
using AppResult = Result<T, AppErrorType>;

struct Event;

using EventHandler = Delegate<bool(const Event&)>;

enum class TickResult { Continue, Quit, Paused };

struct MODUS_APP_EXPORT AppInitParams {
    StringSlice name;
    StringSlice name_preferences;
    EventHandler event_handler;
    threed::Api threed_api = threed::Api::OpenGLES3;
};

/// Interface which manages the native app
class MODUS_APP_EXPORT App {
   protected:
    std::unique_ptr<threed::Instance> m_threed_instance;
    String m_name;
    String m_name_pref;
    String m_path_tmp;
    String m_path_pref_config;
    String m_path_pref_data;
    String m_path_cache;
    String m_path_resources;

   public:
    App();

    virtual ~App();

    virtual AppResult<void> initialize(const AppInitParams& params) = 0;

    virtual AppResult<void> shutdown() = 0;

    virtual TickResult tick() = 0;

    int run();

    virtual AppResult<NotMyPtr<Window>> create_window(const WindowCreateParams& params) = 0;

    virtual Optional<NotMyPtr<Window>> main_window() = 0;

    virtual void lock_mouse(const bool value) = 0;

    virtual bool is_mouse_locked() const = 0;

    virtual Slice<DeviceId> game_controllers() const = 0;

    StringSlice path_tmp() const { return m_path_tmp; }

    StringSlice path_pref_config() const { return m_path_pref_config; }

    StringSlice path_pref_data() const { return m_path_pref_data; }

    StringSlice path_cache() const { return m_path_cache; }

    StringSlice path_resources() const { return m_path_resources; }

    threed::Instance& threed_instance() {
        modus_assert(m_threed_instance);
        return *m_threed_instance;
    }

    const threed::Instance& threed_instance() const {
        modus_assert(m_threed_instance);
        return *m_threed_instance;
    }

   protected:
    AppResult<void> init_paths(const AppInitParams& params);

    void reset_paths();

    virtual Result<String, void> impl_path_resources() = 0;
};

MODUS_APP_EXPORT std::unique_ptr<App> create_app();
}    // namespace modus::app
