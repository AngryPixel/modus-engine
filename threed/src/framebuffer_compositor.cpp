/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <threed/device.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/framebuffer_compositor.hpp>
#include <threed/texture.hpp>

namespace modus::threed {

Result<> FramebufferCompositor::initialize(const FramebufferCompositorCreateParams& params,
                                           Device& device,
                                           const u32 buffer_count) {
    if (params.width == 0 || params.height == 0 || buffer_count > kMaxBufferCount) {
        return Error<>();
    }

    if (params.colour_format == TextureFormat::Total) {
        return Error<>();
    }

    m_buffer_count = buffer_count;

    const TextureFormatDesc& colour_desc = texture_format_desc(params.colour_format);

    if (colour_desc.n_bits_depth != 0 || colour_desc.n_bits_stencil != 0) {
        return Error<>();
    }

    if (params.depth_format != TextureFormat::Total) {
        const TextureFormatDesc& depth_desc = texture_format_desc(params.depth_format);

        if (depth_desc.n_bits_depth == 0 || depth_desc.n_bits_red != 0 ||
            depth_desc.n_bits_green != 0 || depth_desc.n_bits_blue != 0 ||
            depth_desc.n_bits_alpha != 0) {
            return Error<>();
        }
    }

    m_params = params;

    m_buffer_index = 0;
    for (u32 i = 0; i < m_buffer_count; ++i) {
        auto r_framebuffer = create_framebuffer(device);
        if (!r_framebuffer) {
            return Error<>();
        }
        m_resources[i] = r_framebuffer.value();
    }
    m_frame_buffer = m_resources[m_buffer_index].framebuffer;
    m_current_width = m_params.width;
    m_current_height = m_params.height;
    return Ok<>();
}

void FramebufferCompositor::shutdown(Device& device) {
    destroy_resources(device);
}

void FramebufferCompositor::set_dimensions(const u32 width, const u32 height) {
    if (m_params.width != width || m_params.height != height) {
        m_params.height = height;
        m_params.width = width;
        m_current_width = width;
        m_current_height = height;
        m_requires_update = true;
    }
}

Result<void, String> FramebufferCompositor::update(Device& device) {
    if (m_requires_update) {
        destroy_resources(device);

        // Create new framebuffers
        for (u32 i = 0; i < m_buffer_count; ++i) {
            auto r_framebuffer = create_framebuffer(device);
            if (!r_framebuffer) {
                return Error(r_framebuffer.release_error());
            }
            m_resources[i] = r_framebuffer.value();
        }
        m_frame_buffer = m_resources[m_buffer_index].framebuffer;
    }
    return Ok<>();
}

void FramebufferCompositor::swap_buffers() {
    m_buffer_index = (m_buffer_index + 1) % m_buffer_count;
    m_frame_buffer = m_resources[m_buffer_index].framebuffer;
}

Result<FramebufferCompositor::Resource, String> FramebufferCompositor::create_framebuffer(
    Device& device) {
    TextureHandle tex_colour, tex_depth;

    const threed::TextureType tex_type =
        m_params.sample_count <= 2 ? threed::TextureType::T2D : threed::TextureType::T2DMultiSample;

    threed::TextureCreateParams colour_params;
    colour_params.format = m_params.colour_format;
    colour_params.type = tex_type;
    colour_params.width = m_params.width;
    colour_params.height = m_params.height;
    colour_params.mip_map_levels = 1;
    colour_params.depth = 1;
    colour_params.sample_count = m_params.sample_count;

    auto r_colour_texture = device.create_texture(colour_params);
    if (!r_colour_texture) {
        return Error<String>("Failed to create colour texture for framebuffer");
    }

    tex_colour = r_colour_texture.value();

    if (m_params.depth_format != TextureFormat::Total) {
        threed::TextureCreateParams depth_params;
        depth_params.type = tex_type;
        depth_params.format = m_params.depth_format;
        depth_params.width = m_params.width;
        depth_params.height = m_params.height;
        depth_params.mip_map_levels = 1;
        depth_params.depth = 1;
        depth_params.sample_count = m_params.sample_count;

        auto r_depth_texture = device.create_texture(depth_params);
        if (!r_depth_texture) {
            device.destroy_texture(tex_colour);
            return Error<String>("Failed to create depth texutre for framebuffer");
        }
        tex_depth = r_depth_texture.value();
    }

    threed::FramebufferCreateParams fb_params;
    fb_params.depth.handle = tex_depth;
    fb_params.depth.sample_count = m_params.sample_count;
    fb_params.colour[0].handle = tex_colour;
    fb_params.colour[0].mip_map_level = 0;
    fb_params.colour[0].sample_count = m_params.sample_count;

    auto r_framebuffer = device.create_framebuffer(fb_params);
    if (!r_framebuffer) {
        device.destroy_texture(tex_colour);
        if (tex_depth) {
            device.destroy_texture(tex_depth);
        }
        return Error<String>("Failed to create framebuffer");
    }
    Resource r;
    r.framebuffer = r_framebuffer.value();
    r.tex_colour = tex_colour;
    r.tex_depth = tex_depth;
    return Ok(r);
}

void FramebufferCompositor::destroy_resources(Device& device) {
    // Clean up sources
    m_frame_buffer = FramebufferHandle();
    m_buffer_index = 0;
    for (u32 i = 0; i < m_buffer_count; ++i) {
        Resource& resource = m_resources[i];
        if (resource.framebuffer.is_valid()) {
            device.destroy_framebuffer(resource.framebuffer);
            resource.framebuffer = FramebufferHandle();
        }
        if (resource.tex_colour.is_valid()) {
            device.destroy_texture(resource.tex_colour);
            resource.tex_colour = TextureHandle();
        }
        if (resource.tex_depth.is_valid()) {
            device.destroy_texture(resource.tex_depth);
            resource.tex_depth = TextureHandle();
        }
    }
}

}    // namespace modus::threed
