/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <threed/texture.hpp>

namespace modus::threed {

static const char* kTextureTypeStr[] = {"T2D", "T2DArray", "TCubeMap", "T3D", "T2DMultiSample"};

static_assert(modus::array_size(kTextureTypeStr) == usize(TextureType::Total));

static const char* kTextureFormatStr[] = {
    "R8G8B8A8",
    "R8G8B8",
    "R5G6B5",
    "R4G4B4A4",
    "5G5B5A1",
    "Depth24",
    "Depth32F",
    "Depth24Stencil8",
    "RGBA_ASTC4x4",
    "RGBA_ASTC5x4",
    "RGBA_ASTC5x5",
    "RGBA_ASTC6x5",
    "RGBA_ASTC6x6",
    "RGBA_ASTC8x5",
    "RGBA_ASTC8x6",
    "RGBA_ASTC8x8",
    "RGBA_ASTC10x5",
    "RGBA_ASTC10x6",
    "RGBA_ASTC10x8",
    "RGBA_ASTC10x10",
    "RGBA_ASTC12x10",
    "RGBA_ASTC12x10",
    "R8",
    "RGBA_ASTC4x4_srgb",
    "RGBA_ASTC5x4_srgb",
    "RGBA_ASTC5x5_srgb",
    "RGBA_ASTC6x5_srgb",
    "RGBA_ASTC6x6_srgb",
    "RGBA_ASTC8x5_srgb",
    "RGBA_ASTC8x6_srgb",
    "RGBA_ASTC8x8_srgb",
    "RGBA_ASTC10x5_srgb",
    "RGBA_ASTC10x6_srgb",
    "RGBA_ASTC10x8_srgb",
    "RGBA_ASTC10x10_srgb",
    "RGBA_ASTC12x10_srgb",
    "RGBA_ASTC12x12_srgb",
    "R8G8B8_srgb",
    "R8G8B8A8_srgb",
    "R_EAC11",
    "R_EAC11_SIGNED",
    "RG_EAC11",
    "RG_EAC11_SIGNED",
    "RGB_ETC2",
    "RGB_ETC2_srgb",
    "RGBA_ETC2_A1",
    "RGBA_ETC2_A1_srgb",
    "RGBA_ETC2",
    "RGBA_ETC2_srgb",
    "R10G10B10A2_I32",
    "R10G10B10A2_U32",
    "R16G16B16_F16",
    "R16G16B16A_F16",
    "R16G16_F16",
    "R16_U16",
    "R32G32_U32",
    "R16G16_U16",
    "Depth16F",
};

// clang-format off

static const TextureFormatDesc kTextureFormatDescs[] = {
    {TextureFormat::R8G8B8A8, 4, false, false, false, 4, 8, 8, 8, 8, 0, 0, 0, 0,
     0, 0},
    {TextureFormat::R8G8B8, 3, false, false, false, 3, 8, 8, 8, 0, 0, 0, 0, 0,
     0, 0},
    {TextureFormat::R5G6B5, 2, false, false, false, 3, 5, 6, 5, 0, 0, 0, 0, 0,
     0, 0},
    {TextureFormat::R4G4B4A4, 2, false, false, false, 4, 4, 4, 4, 4, 0, 0, 0, 0,
     0, 0},
    {TextureFormat::R5G5B5A1, 2, false, false, false, 4, 5, 5, 5, 1, 0, 0, 0, 0,
     0, 0},
    {TextureFormat::Depth24, 3, false, false, false, 0, 0, 0, 0, 0, 0, 24, 0,
     0, 0, 0},
    {TextureFormat::Depth32F, 4, false, false, false, 0, 0, 0, 0, 0, 0, 32, 0,
     0, 0, 0},
    {TextureFormat::Depth24Stencil8, 4, false, false, false, 255, 0, 0, 0, 0, 8,
     24, 0, 0, 0, 0},
    {TextureFormat::RGBA_ASTC4x4, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 4, 4, 1},
    {TextureFormat::RGBA_ASTC5x4, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 5, 4, 1},
    {TextureFormat::RGBA_ASTC5x5, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 5, 5, 1},
    {TextureFormat::RGBA_ASTC6x5, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 6, 5, 1},
    {TextureFormat::RGBA_ASTC6x6, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 6, 6, 1},
    {TextureFormat::RGBA_ASTC8x5, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 8, 5, 1},
    {TextureFormat::RGBA_ASTC8x6, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 8, 6, 1},
    {TextureFormat::RGBA_ASTC8x8, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 8, 8, 1},
    {TextureFormat::RGBA_ASTC10x5, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 10, 5, 1},
    {TextureFormat::RGBA_ASTC10x6, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 10, 6, 1},
    {TextureFormat::RGBA_ASTC10x8, 4, false, true, false, 4, 255, 255, 255, 255,
     255, 255, 16, 10, 8, 1},
    {TextureFormat::RGBA_ASTC10x10, 4, false, true, false, 4, 255, 255, 255,
     255, 255, 255, 16, 10, 10, 1},
    {TextureFormat::RGBA_ASTC12x10, 4, false, true, false, 4, 255, 255, 255,
     255, 255, 255, 16, 12, 10, 1},
    {TextureFormat::RGBA_ASTC12x12, 4, false, true, false, 4, 255, 255, 255,
     255, 255, 255, 16, 12, 12, 1},
    {TextureFormat::R8, 1, false, false, false, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0,
     0},
    {TextureFormat::RGBA_ASTC4x4_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 4, 4, 1},
    {TextureFormat::RGBA_ASTC5x4_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 5, 4, 1},
    {TextureFormat::RGBA_ASTC5x5_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 5, 5, 1},
    {TextureFormat::RGBA_ASTC6x5_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 6, 5, 1},
    {TextureFormat::RGBA_ASTC6x6_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 6, 6, 1},
    {TextureFormat::RGBA_ASTC8x5_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 8, 5, 1},
    {TextureFormat::RGBA_ASTC8x6_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 8, 6, 1},
    {TextureFormat::RGBA_ASTC8x8_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 8, 8, 1},
    {TextureFormat::RGBA_ASTC10x5_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 10, 5, 1},
    {TextureFormat::RGBA_ASTC10x6_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 10, 6, 1},
    {TextureFormat::RGBA_ASTC10x8_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 10, 8, 1},
    {TextureFormat::RGBA_ASTC10x10_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 10, 10, 1},
    {TextureFormat::RGBA_ASTC12x10_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 12, 10, 1},
    {TextureFormat::RGBA_ASTC12x12_srgb, 4, false, true, true, 4, 255, 255, 255,
     255, 255, 255, 16, 12, 12, 1},
    {TextureFormat::R8G8B8_srgb, 3, false, false, true, 3, 8, 8, 8, 0, 0, 0, 0,
     0, 0, 0},
    {TextureFormat::R8G8B8A8_srgb, 4, false, false, true, 4, 8, 8, 8, 8, 0, 0,
     0, 0, 0, 0},
    {TextureFormat::R_EAC11, 0, false, true, false, 1, 11, 0, 0, 0, 0, 0, 8, 4,
     4, 1},
    {TextureFormat::R_EAC11_SIGNED, 0, false, true, false, 1, 11, 0, 0, 0, 0, 0,
     8, 4, 4, 1},
    {TextureFormat::RG_EAC11, 0, false, true, false, 2, 11, 11, 0, 0, 0, 0, 16,
     4, 4, 1},
    {TextureFormat::RG_EAC11_SIGNED, 0, false, true, false, 2, 11, 11, 0, 0, 0,
     0, 16, 4, 4, 1},
    {TextureFormat::RGB_ETC2, 0, false, true, false, 3, 0, 0, 0, 0, 0, 0, 8, 4,
     4, 1},
    {TextureFormat::RGB_ETC2_srgb, 0, false, true, true, 3, 0, 0, 0, 0, 0, 0, 8,
     4, 4, 1},
    {TextureFormat::RGBA_ETC2_A1, 0, false, true, false, 4, 0, 0, 0, 0, 0, 0, 8,
     4, 4, 1},
    {TextureFormat::RGBA_ETC2_A1_srgb, 0, false, true, true, 4, 0, 0, 0, 0, 0,
     0, 8, 4, 4, 1},
    {TextureFormat::RGBA_ETC2, 0, false, true, false, 4, 0, 0, 0, 0, 0, 0, 16,
     4, 4, 1},
    {TextureFormat::RGBA_ETC2_srgb, 0, false, true, true, 4, 0, 0, 0, 0, 0, 0,
     16, 4, 4, 1},
    {TextureFormat::R10G10B10A2_I32, 4, false, false, false, 4, 10, 10, 10, 2,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R10G10B10A2_U32, 4, false, false, false, 4, 10, 10, 10, 2,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R16G16B16_F16, 6, false, false, false, 3, 16, 16, 16, 0,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R16G16B16A_F16, 8, false, false, false, 4, 16, 16, 16, 16,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R16G16_F16, 4, false, false, false, 2, 16, 16, 0, 0,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R16_U16, 2, false, false, false, 1, 16, 0, 0, 0,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R32G32_U32, 8, false, false, false, 2, 32, 32, 0, 0,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::R16G16_U16, 4, false, false, false, 2, 16, 16, 0, 0,
     0, 0, 0, 0, 0, 0},
    {TextureFormat::Depth16F, 2, false, false, false, 0, 0, 0, 0, 0, 0, 16, 0,
     0, 0, 0},
};

// clang-format on

static_assert(modus::array_size(kTextureFormatDescs) == usize(TextureFormat::Total));

static_assert(modus::array_size(kTextureFormatStr) == usize(TextureFormat::Total));

StringSlice texture_format_to_str(const TextureFormat format) {
    modus_assert(format < TextureFormat::Total);
    return kTextureFormatStr[u8(format)];
}

StringSlice texture_type_to_str(const TextureType type) {
    modus_assert(type < TextureType::Total);
    return kTextureTypeStr[u8(type)];
}

const TextureFormatDesc& texture_format_desc(const TextureFormat format) {
    modus_assert(format < TextureFormat::Total);
    return kTextureFormatDescs[u8(format)];
}

}    // namespace modus::threed
