/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <threed/compositor.hpp>
#include <threed/device.hpp>

#if defined(MODUS_THREED_USE_OPENGL)
#include <gles3/api_gles3.hpp>
#else
#error "Threed API is not defined!"
#endif

namespace modus::threed {
Device::Device(Instance& instance) : m_instance(instance) {}

/*MappedBufferScope::MappedBufferScope()
    : m_device(nullptr), m_buffer(BufferHandle()), m_slice() {}

MappedBufferScope::MappedBufferScope(Device& device,
                                     const BufferHandle buffer,
                                     ByteSliceMut slice)
    : m_device(&device), m_buffer(buffer), m_slice(slice) {}

MappedBufferScope::MappedBufferScope(MappedBufferScope&& rhs) noexcept
    : m_device(rhs.m_device), m_buffer(rhs.m_buffer), m_slice(rhs.m_slice) {
    rhs.m_device = nullptr;
    rhs.m_buffer = BufferHandle();
    rhs.m_slice = ByteSliceMut();
}

MappedBufferScope& MappedBufferScope::operator=(
    MappedBufferScope&& rhs) noexcept {
    if (this != &rhs) {
        if (m_device != nullptr && m_buffer.is_valid()) {
            m_device->unmap_buffer(m_buffer);
        }
        m_device = rhs.m_device;
        m_buffer = rhs.m_buffer;
        m_slice = rhs.m_slice;
        rhs.m_device = nullptr;
        rhs.m_buffer = BufferHandle();
        rhs.m_slice = ByteSliceMut();
    }
    return *this;
}

MappedBufferScope::~MappedBufferScope() {
    if (m_device != nullptr) {
        m_device->unmap_buffer(m_buffer);
    }
}*/

}    // namespace modus::threed
