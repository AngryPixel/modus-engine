/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <threed/program_gen/shader_snippet_db.hpp>
// clang-format on
#include <threed/program_gen/program_gen.hpp>

namespace modus::threed::dynamic {

ShaderSnippet::ShaderSnippet(StringSlice id) : m_id(id.to_str()) {}

Result<> ShaderSnippetDB::register_snippet(NotMyPtr<const ShaderSnippet> snippet) {
    const String& key = snippet->id();
    auto& snippet_entry = m_snippets[key];
    if (snippet_entry) {
        return Error<>();
    }
    snippet_entry = snippet;
    return Ok<>();
}

void ShaderSnippetDB::unregister_snippet(NotMyPtr<const ShaderSnippet> snippet) {
    const String& key = snippet->id();
    m_snippets.erase(key);
}

Result<> ShaderSnippetDB::apply(ProgramGenParams& params,
                                const ShaderStage stage,
                                const String& snippet_id) const {
    auto it = m_snippets.find(snippet_id);
    if (it == m_snippets.end()) {
        return Error<>();
    }
    if (stage == ShaderStage::Vertex) {
        if (!it->second->apply(params.vertex_stage)) {
            return Error<>();
        }
    } else if (stage == ShaderStage::Fragment) {
        if (!it->second->apply(params.fragment_stage)) {
            return Error<>();
        }
    } else {
        modus_assert_message(false, "Unknown shader stage");
        return Error<>();
    }
    return Ok<>();
}

Result<> ShaderSnippetDB::apply(ProgramGenParams& params,
                                const ShaderStage stage,
                                Slice<String> snippet_ids) const {
    for (const auto& snippet_id : snippet_ids) {
        if (!apply(params, stage, snippet_id)) {
            return Error<>();
        }
    }
    return Ok<>();
}

}    // namespace modus::threed::dynamic
