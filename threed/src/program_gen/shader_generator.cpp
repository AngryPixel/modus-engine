/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <threed/program_gen/shader_generator.hpp>
#include <threed/program_gen/program_gen.hpp>
// clang-format on

namespace modus::threed::dynamic {

Result<void, String> NullShaderGenerator::validate(const ProgramGenParams&) const {
    return Ok<>();
}
PrecisionQualifier NullShaderGenerator::default_precision_qualifier() const {
    return PrecisionQualifier::High;
}
ShaderGenerator::WriteResult NullShaderGenerator::write_header(IByteWriter&,
                                                               const ShaderStage) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write_input_header(IByteWriter&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write_snippet_header(IByteWriter&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write_types_header(IByteWriter&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write_vertex_input_header(IByteWriter&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write_fragment_output_header(IByteWriter&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write_main(IByteWriter&,
                                                             const StringSlice) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&,
                                                        const Slice<StringSlice>) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&,
                                                        const u8,
                                                        const ConstantInputDesc&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&,
                                                        const u8,
                                                        const ConstantBufferInputDesc&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&,
                                                        const u8,
                                                        const ShaderBufferInputDesc&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&,
                                                        const u8,
                                                        const SamplerInputDesc&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&, const FragmentOutput&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&, const VertexInput&) const {
    return Ok(usize(0));
}
ShaderGenerator::WriteResult NullShaderGenerator::write(IByteWriter&,
                                                        const Slice<VaryingDesc>,
                                                        const ShaderStage) const {
    return Ok(usize(0));
}
}    // namespace modus::threed::dynamic
