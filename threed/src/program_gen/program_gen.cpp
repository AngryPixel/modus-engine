/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <threed/program_gen/program_gen.hpp>
// clang-format on

#include <threed/program_gen/shader_generator.hpp>
#include <core/io/memory_stream.hpp>
#include <threed/pipeline.hpp>

namespace modus::threed::dynamic {

FragmentOutput FragmentOutput::create_default() {
    FragmentOutput result;
    result.name = "OUT_COLOR";
    result.slot = 0;
    result.data_type = DataType::Vec4F32;
    return result;
}

void RuntimeBinders::bind(threed::Command& command, threed::Device& device) const {
    const u8 constant_count = u8(constants.size());
    const u8 sampler_count = u8(samplers.size());
    const u8 cbuffer_count = u8(cbuffers.size());
    const u8 sbuffer_count = u8(sbuffers.size());
    for (u8 i = 0; i < constant_count; i++) {
        const auto& binder = constants[i];
        if (binder) {
            binder->bind(command, device, i);
        }
    }
    for (u8 i = 0; i < cbuffer_count; i++) {
        const auto& binder = cbuffers[i];
        if (binder) {
            binder->bind(command, device, i);
        }
    }
    for (u8 i = 0; i < sbuffer_count; i++) {
        const auto& binder = sbuffers[i];
        if (binder) {
            binder->bind(command, device, i);
        }
    }
    for (u8 i = 0; i < sampler_count; i++) {
        const auto& binder = samplers[i];
        if (binder) {
            binder->bind(command, device, i);
        }
    }
}

struct ProgramGenerator::Variant {
    ProgramGeneratorResult result;
    RamStream<> vertex_stream;
    RamStream<> fragment_stream;
    u8 constant_counter = 0;
    u8 cbuffer_counter = 0;
    u8 sbuffer_counter = 0;
    u8 sampler_counter = 0;
};

ProgramGenerator::ProgramGenerator() {}

ProgramGenerator::~ProgramGenerator() {}

void ProgramGenerator::reset() {
    m_variants.clear();
}

Result<usize, String> ProgramGenerator::generate(
    const ProgramGenParams& params,
    const ShaderGenerator& generator,
    const Slice<ProgramGeneratorCondition> conditions) {
    MODUS_UNUSED(params);
    MODUS_UNUSED(generator);

    if (auto r = generator.validate(params); !r) {
        return Error(r.release_error());
    }

    // Generate Non-instanced
    Options opt;
    opt.conditions = conditions;
    {
        opt.instanced = false;
        Variant variant;
        opt.stage = ShaderStage::Vertex;
        if (auto r = generate(variant, opt, params, generator); !r) {
            return Error(r.release_error());
        }
        opt.stage = ShaderStage::Fragment;
        if (auto r = generate(variant, opt, params, generator); !r) {
            return Error(r.release_error());
        }
        m_variants.push_back(std::move(variant));
    }
    // Generate Instanced
    if (!params.skip_instanced_variant) {
        opt.instanced = true;
        Variant variant;
        opt.stage = ShaderStage::Vertex;
        if (auto r = generate(variant, opt, params, generator); !r) {
            return Error(r.release_error());
        }
        opt.stage = ShaderStage::Fragment;
        if (auto r = generate(variant, opt, params, generator); !r) {
            return Error(r.release_error());
        }
        m_variants.push_back(std::move(variant));
    }
    return Ok<usize>(m_variants.size());
}

Result<NotMyPtr<const ProgramGeneratorResult>> ProgramGenerator::result(const usize index) const {
    if (index < m_variants.size()) {
        return Ok(NotMyPtr<const ProgramGeneratorResult>(&m_variants[index].result));
    }
    return Error<>();
}

Result<void, String> ProgramGenerator::generate(Variant& variant,
                                                const Options& options,
                                                const ProgramGenParams& params,
                                                const ShaderGenerator& generator) {
    RamStream<>& stream =
        options.stage == ShaderStage::Vertex ? variant.vertex_stream : variant.fragment_stream;

    GenerateParams gen_params;
    gen_params.writer = &stream;
    gen_params.default_precision_qualifier = generator.default_precision_qualifier();
    gen_params.instanced = options.instanced;
    gen_params.stage = options.stage;
    gen_params.generator = &generator;
    // write header
    if (auto r = generator.write_header(stream, options.stage); !r) {
        return Error(String("Failed to write shader header"));
    }

    // write conditions
    Vector<StringSlice> conditions;
    for (const auto& c : options.conditions) {
        if (!c.enabled) {
            continue;
        }
        if (c.stage == options.stage) {
            if (options.instanced && !c.use_when_instancing) {
                continue;
            }
            conditions.push_back(c.name.as_string_slice());
        }
    }

    if (auto r = generator.write(stream, make_slice(conditions)); !r) {
        return Error(String("Failed to write conditions"));
    }

    auto write_types_fn = [&](const ShaderStageCommon& common_params) -> Result<void, String> {
        // Write Snippets header
        if (auto r = generator.write_types_header(stream); !r) {
            return Error(String("Failed to write types header"));
        }
        // Write Constant Snippets
        for (const auto& input : common_params.constants) {
            if (auto r = input->generate_type_snippet(gen_params); !r) {
                return Error(modus::format("Failed to generate constant types snippet for {}",
                                           input->m_desc.name));
            }
        }
        // Write Constant Buffer Snippets
        for (const auto& input : common_params.constant_buffers) {
            if (auto r = input->generate_type_snippet(gen_params); !r) {
                return Error(modus::format(
                    "Failed to generate constant buffer types snippet for {}", input->m_desc.name));
            }
        }
        // Write Shader Buffer Snippets
        for (const auto& input : common_params.shader_buffers) {
            if (auto r = input->generate_type_snippet(gen_params); !r) {
                return Error(modus::format("Failed to generate shader buffer types snippet for {}",
                                           input->m_desc.name));
            }
        }

        // Write Sampler Snippets
        for (const auto& input : common_params.constants) {
            if (auto r = input->generate_code_snippet(gen_params); !r) {
                return Error(modus::format("Failed to generate sampler code snippet for {}",
                                           input->m_desc.name));
            }
        }
        return Ok<>();
    };

    auto write_inputs_fn = [&](const ShaderStageCommon& common_params) -> Result<void, String> {
        // Write input header
        if (auto r = generator.write_input_header(stream); !r) {
            return Error(String("Failed to write input header"));
        }

        // Write Constant Inputs
        for (const auto& input : common_params.constants) {
            if (auto r = input->generate_declaration(variant.constant_counter, gen_params); !r) {
                return Error(modus::format("Failed to generate constant input declaration for {}",
                                           input->m_desc.name));
            } else if (*r == BindableInputResult::Generated) {
                ProgramConstantInputParam param;
                param.name = input->m_desc.name;
                param.data_type = input->m_desc.data_type;
                if (!variant.result.create_params.constants.push_back(param)) {
                    return Error(modus::format(
                        "Failed to add constant input to program params, limit exceeded"));
                }
                variant.result.binders.constants.push_back(input->runtime_binder())
                    .expect("Failed to push binder");
                variant.constant_counter++;
            }
        }

        // Write Constant Buffer Inputs
        for (const auto& input : common_params.constant_buffers) {
            if (auto r = input->generate_declaration(variant.cbuffer_counter, gen_params); !r) {
                return Error(
                    modus::format("Failed to generate constant buffer input declaration for {}",
                                  input->m_desc.name));
            } else if (*r == BindableInputResult::Generated) {
                ProgramCBufferInputParam param;
                param.name = input->m_desc.name;
                if (!variant.result.create_params.cbuffers.push_back(param)) {
                    return Error(modus::format(
                        "Failed to add constant buffer input to program params, limit exceeded"));
                }
                variant.result.binders.cbuffers.push_back(input->runtime_binder())
                    .expect("Failed to push binder");
                variant.cbuffer_counter++;
            }
        }

        // Write Shader Buffer Inputs
        for (const auto& input : common_params.shader_buffers) {
            if (auto r = input->generate_declaration(variant.sbuffer_counter, gen_params); !r) {
                return Error(
                    modus::format("Failed to generate shader buffer input declaration for {}",
                                  input->m_desc.name));
            } else if (*r == BindableInputResult::Generated) {
                variant.result.binders.sbuffers.push_back(input->runtime_binder())
                    .expect("Failed to push binder");
                variant.sbuffer_counter++;
            }
        }

        // Write Sampler Inputs
        for (const auto& input : common_params.samplers) {
            if (auto r = input->generate_declaration(variant.sampler_counter, gen_params); !r) {
                return Error(modus::format("Failed to generate sampler input declaration for {}",
                                           input->m_desc.name));
            } else if (*r == BindableInputResult::Generated) {
                ProgramSamplerInputParam param;
                param.name = input->m_desc.name;
                if (!variant.result.create_params.samplers.push_back(param)) {
                    return Error(modus::format(
                        "Failed to add sampler input to program params, limit exceeded"));
                }
                variant.result.binders.samplers.push_back(input->runtime_binder())
                    .expect("Failed to push binder");
                variant.sampler_counter++;
            }
        }
        return Ok<>();
    };

    auto write_snippets_fn = [&](const ShaderStageCommon& common_params) -> Result<void, String> {
        // Write Snippets header
        if (auto r = generator.write_snippet_header(stream); !r) {
            return Error(String("Failed to write snippit header"));
        }
        // Write Constant Snippets
        for (const auto& input : common_params.constants) {
            if (auto r = input->generate_code_snippet(gen_params); !r) {
                return Error(modus::format("Failed to generate constant input code snippet for {}",
                                           input->m_desc.name));
            }
        }
        // Write Constant Buffer Snippets
        for (const auto& input : common_params.constant_buffers) {
            if (auto r = input->generate_code_snippet(gen_params); !r) {
                return Error(
                    modus::format("Failed to generate constant buffer input code snippet for {}",
                                  input->m_desc.name));
            }
        }
        // Write Shader Buffer Snippets
        for (const auto& input : common_params.shader_buffers) {
            if (auto r = input->generate_code_snippet(gen_params); !r) {
                return Error(
                    modus::format("Failed to generate shader buffer input code snippet for {}",
                                  input->m_desc.name));
            }
        }

        // Write Sampler Snippets
        for (const auto& input : common_params.constants) {
            if (auto r = input->generate_code_snippet(gen_params); !r) {
                return Error(modus::format("Failed to generate sampler code snippet for {}",
                                           input->m_desc.name));
            }
        }
        return Ok<>();
    };

    // Vertex
    if (options.stage == ShaderStage::Vertex) {
        // Write types
        if (auto r = write_types_fn(params.vertex_stage); !r) {
            return r;
        }
        // Write Inputs
        if (auto r = write_inputs_fn(params.vertex_stage); !r) {
            return r;
        }
        // Write vertex inputs
        if (auto r = generator.write_vertex_input_header(stream); !r) {
            return Error(modus::format("Failed to write vertex input header"));
        }
        for (const auto& input : params.vertex_stage.inputs) {
            if (auto r = generator.write(stream, input); !r) {
                return Error(modus::format("Failed to write vertex input {}", input.name));
            }
        }
        // Write snippets
        if (auto r = write_snippets_fn(params.vertex_stage); !r) {
            return r;
        }
        // write varying
        if (auto r = generator.write(stream, make_slice(params.varying), options.stage); !r) {
            return Error(String("Failed to write varying vertex section"));
        }
        // Write main
        if (auto r = generator.write_main(stream, params.vertex_stage.main); !r) {
            return Error(modus::format("Failed to write vertex stage main"));
        }
        variant.result.create_params.vertex = stream.as_bytes();
    }

    // Fragment
    if (options.stage == ShaderStage::Fragment) {
        // Write types
        if (auto r = write_types_fn(params.vertex_stage); !r) {
            return r;
        }
        // Write Inputs
        if (auto r = write_inputs_fn(params.fragment_stage); !r) {
            return r;
        }
        // Write fragment outputs
        if (auto r = generator.write_fragment_output_header(stream); !r) {
            return Error(modus::format("Failed to write fragment output header"));
        }
        for (const auto& output : params.fragment_stage.fragment_outputs) {
            if (auto r = generator.write(stream, output); !r) {
                return Error(modus::format("Failed to write fragment output {}", output.name));
            }
        }
        // Write snippets
        if (auto r = write_snippets_fn(params.fragment_stage); !r) {
            return r;
        }
        // write varying
        if (auto r = generator.write(stream, make_slice(params.varying), options.stage); !r) {
            return Error(String("Failed to write varying fragment section"));
        }
        // Write main
        if (auto r = generator.write_main(stream, params.fragment_stage.main); !r) {
            return Error(modus::format("Failed to write fragment stage main"));
        }
        variant.result.create_params.fragment = stream.as_bytes();
    }
    return Ok<>();
}

}    // namespace modus::threed::dynamic
