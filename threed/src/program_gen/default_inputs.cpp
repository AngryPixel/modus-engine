/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <threed/program_gen/default_inputs.hpp>
// clang-format on
#include <threed/program_gen/shader_generator.hpp>

namespace modus::threed::dynamic {

Result<BindableInputResult> DefaultConstantInput::generate_declaration(
    const u8 slot,
    GenerateParams& params) const {
    if (!params.generator->write(*params.writer, slot, m_desc)) {
        return Error<>();
    }
    return Ok(BindableInputResult::Generated);
}

Result<BindableInputResult> DefaultConstantInput::generate_type_snippet(GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultConstantInput::generate_code_snippet(GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultConstantBufferInput::generate_declaration(
    const u8 slot,
    GenerateParams& params) const {
    if (!params.generator->write(*params.writer, slot, m_desc)) {
        return Error<>();
    }
    return Ok(BindableInputResult::Generated);
}

Result<BindableInputResult> DefaultConstantBufferInput::generate_type_snippet(
    GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultConstantBufferInput::generate_code_snippet(
    GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultShaderBufferInput::generate_declaration(
    const u8 slot,
    GenerateParams& params) const {
    if (!params.generator->write(*params.writer, slot, m_desc)) {
        return Error<>();
    }
    return Ok(BindableInputResult::Generated);
}

Result<BindableInputResult> DefaultShaderBufferInput::generate_type_snippet(GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultShaderBufferInput::generate_code_snippet(GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultSamplerInput::generate_declaration(
    const u8 slot,
    GenerateParams& params) const {
    if (!params.generator->write(*params.writer, slot, m_desc)) {
        return Error<>();
    }
    return Ok(BindableInputResult::Generated);
}

Result<BindableInputResult> DefaultSamplerInput::generate_type_snippet(GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

Result<BindableInputResult> DefaultSamplerInput::generate_code_snippet(GenerateParams&) const {
    return Ok(BindableInputResult::Ignored);
}

}    // namespace modus::threed::dynamic
