/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/buffer_gles3.hpp>
#include <gles3/state_cache_gles3.hpp>

namespace modus::threed::gles3 {

static const GLenum kBufferTypeGL[] = {GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER};

static_assert(modus::array_size(kBufferTypeGL) == usize(BufferType::Total));

static const GLenum kBufferUsageGL[] = {
    GL_STATIC_DRAW,
    GL_DYNAMIC_DRAW,
    GL_STREAM_DRAW,
};

static_assert(modus::array_size(kBufferUsageGL) == usize(BufferUsage::Total));

static Result<BufferGLES3, void> create_buffer(const BufferCreateParams& params,
                                               StateCacheGLES3& state_cache) {
    if (params.usage >= BufferUsage::Total || params.type >= BufferType::Total) {
        return Error<>();
    }

    GLuint gl_id;
    glGenBuffers(1, &gl_id);

    BufferGLES3 result;
    result.params = params;
    const GLenum buffer_type = kBufferTypeGL[static_cast<u8>(params.type)];
    result.gl_type = buffer_type;
    result.gl_usage = kBufferUsageGL[static_cast<u8>(params.usage)];
    state_cache.bind_buffer(params.type, GLBufferId(gl_id), result.gl_type);
    glBufferData(buffer_type, static_cast<GLsizeiptr>(params.size), params.data, result.gl_usage);
    result.gl_id = GLBufferId(gl_id);
    return Ok(result);
}

static void destroy_buffer(const GLBufferId id) {
    const GLuint gl_id = id;
    glDeleteBuffers(1, &gl_id);
}

void BufferDBGLES3::destroy_resource(const BufferGLES3& buffer) {
    destroy_buffer(buffer.gl_id);
}

Result<BufferHandle, void> BufferDBGLES3::create(const BufferCreateParams& params,
                                                 StateCacheGLES3& state_cache) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error<>();
    }

    modus_assert(params.size != 0);
    auto buffer_result = create_buffer(params, state_cache);
    if (!buffer_result) {
        return Error<>();
    }

    auto add_result = m_pool.create(*buffer_result);
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        destroy_resource(*buffer_result);
        return Error<>();
    }

    return Ok<BufferHandle>(add_result.value().first);
}

/*
Result<ByteSliceMut, void> BufferDBGLES3::map(const BufferHandle handle,
                                              const BufferMapParams& params,
                                              StateCacheGLES3& state_cache) {
    auto buffer_result = m_pool.get(handle);
    if (buffer_result) {
        NotMyPtr<BufferGLES3> buffer = buffer_result.value();
        if (buffer->mapped) {
            return Error<>();
        }
        if (params.start + params.size > buffer->params.size) {
            return Error<>();
        }

        GLbitfield flags = 0;
        if (params.read) {
            flags |= GL_MAP_READ_BIT;
        }
        if (params.write) {
            flags |= GL_MAP_WRITE_BIT;
        }

        if (params.invalidate && params.start == 0 &&
            params.size == buffer->params.size) {
            flags |= GL_MAP_INVALIDATE_BUFFER_BIT;
        } else if (params.invalidate) {
            flags |= GL_MAP_INVALIDATE_RANGE_BIT;
        }

        state_cache.bind_buffer(buffer->params.type, buffer->gl_id,
                                buffer->gl_type);
        state_cache.map_buffer(buffer->params.type, buffer->gl_id, true);
        void* ptr =
            glMapBufferRange(buffer->gl_type, params.start, params.size, flags);
        if (ptr == nullptr) {
            return Error<>();
        }
        buffer->mapped = true;
        return Ok(ByteSliceMut(reinterpret_cast<u8*>(ptr), params.size));
    }
    return Error<>();
}

void BufferDBGLES3::unmap(const BufferHandle handle,
                          StateCacheGLES3& state_cache) {
    auto buffer_result = m_pool.get(handle);
    if (buffer_result) {
        NotMyPtr<BufferGLES3> buffer = buffer_result.value();
        modus_assert(buffer->mapped);
        state_cache.map_buffer(buffer->params.type, buffer->gl_id, false);
        buffer->mapped = false;
        glUnmapBuffer(buffer->gl_type);
    }
}*/

Result<> BufferDBGLES3::update(const BufferHandle handle,
                               const ByteSlice slice,
                               const u32 offset,
                               const bool invalidate_buffer,
                               StateCacheGLES3& state_cache) {
    MODUS_PROFILE_THREED_BLOCK("BufferGLES3::update");
    auto buffer_result = m_pool.get(handle);
    if (!buffer_result) {
        return Error<>();
    }

    const BufferGLES3& buffer = *(buffer_result.value());
    if (offset >= buffer.params.size || offset + slice.size() > buffer.params.size) {
        MODUS_LOGE(
            "BufferGLES3: Updating buffer with invalid range offset:{} size:{} "
            "for buffer size:{}",
            offset, slice.size(), buffer.params.size);
        return Error<>();
    }

    if (buffer.params.usage == BufferUsage::Static) {
        MODUS_LOGE("BufferGLES3: Can't update buffer with static usage");
        return Error<>();
    }
    state_cache.bind_buffer(buffer.params.type, buffer.gl_id, buffer.gl_type);
    if (buffer.params.size == slice.size() || invalidate_buffer) {
        glBufferData(buffer.gl_type, buffer.params.size, nullptr, buffer.gl_usage);
    }
    glBufferSubData(buffer.gl_type, offset, slice.size(), slice.data());
    return Ok<>();
}

Result<ConstantBufferHandle, void> ConstantBufferDBGLES3::create(
    const ConstantBufferCreateParams& params,
    StateCacheGLES3& state_cache) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error<>();
    }

    if ((params.size % 16) != 0) {
        MODUS_LOGE(
            "ConstantBufferGLES3: Constant buffer size must be a multiple of "
            "16");
        return Error<>();
    }

    modus_assert(params.size != 0);
    if (params.size > m_max_uniform_buffer_size) {
        MODUS_LOGE("ConstantBuffer size with buffering exceeds device capacity");
        return Error<>();
    }

    GLuint gl_buffer;
    GLenum gl_usage = kBufferUsageGL[u32(params.usage)];
    glGenBuffers(1, &gl_buffer);
    state_cache.bind_buffer_uniform(GLBufferId(gl_buffer));
    glBufferData(GL_UNIFORM_BUFFER, params.size, params.data, gl_usage);

    ConstantBufferGLES3 buffer;
    buffer.gl_id = GLBufferId(gl_buffer);
    buffer.size = params.size;
    buffer.gl_usage = gl_usage;

    auto add_result = m_pool.create(buffer);
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        glDeleteBuffers(1, &gl_buffer);
        return Error<>();
    }

    return Ok<ConstantBufferHandle>(add_result.value().first);
}

void ConstantBufferDBGLES3::initialize() {
    GLint alignment;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &alignment);
    m_uniform_offset_alignment = alignment;
    GLint max_uniform_block_size;
    glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &max_uniform_block_size);
    m_max_uniform_buffer_size = max_uniform_block_size;
}

Result<const ConstantBufferGLES3*, void> ConstantBufferDBGLES3::get(
    const ConstantBufferHandle handle) const {
    auto result = m_pool.get(handle);
    if (!result) {
        return Error<>();
    }
    return Ok(result.value().get());
}

Result<> ConstantBufferDBGLES3::update(const ConstantBufferHandle handle,
                                       const void* ptr,
                                       const usize size,
                                       StateCacheGLES3& state_cache) {
    MODUS_PROFILE_THREED_BLOCK("ConstantBufferGLES3::update");
    auto result = m_pool.get(handle);
    if (!result) {
        return Error<>();
    }

    const ConstantBufferGLES3& buffer = *(result.value());

    if (buffer.gl_usage == GL_STATIC_DRAW) {
        MODUS_LOGE(
            "ConstantBufferGLES3: Attempting to update static constant "
            "buffer");
        return Error<>();
    }

    if (size > buffer.size) {
        MODUS_LOGE(
            "ConstantBufferGLES3: Updating constant buffer with the wrong "
            "size!");
        return Error<>();
    }

    state_cache.bind_buffer_uniform(buffer.gl_id);
    const u32 offset = 0;
    glBufferData(GL_UNIFORM_BUFFER, buffer.size, nullptr, buffer.gl_usage);
    glBufferSubData(GL_UNIFORM_BUFFER, offset, size, ptr);
    return Ok<>();
}

void ConstantBufferDBGLES3::swap_buffers() {}

void ConstantBufferDBGLES3::destroy_resource(const ConstantBufferGLES3& resource) {
    destroy_buffer(resource.gl_id);
}

Result<ShaderBufferHandle, void> ShaderBufferDBGLES3::create(const ShaderBufferCreateParams& params,
                                                             StateCacheGLES3& state_cache) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error<>();
    }

    if ((params.size % 4) != 0) {
        MODUS_LOGE(
            "ConstantBufferGLES3: Constant buffer size must be a multiple of "
            "4");
        return Error<>();
    }

    GLuint gl_buffer;
    GLenum gl_usage = kBufferUsageGL[u32(params.usage)];
    glGenBuffers(1, &gl_buffer);
    state_cache.bind_buffer_storage(GLBufferId(gl_buffer));
    glBufferData(GL_SHADER_STORAGE_BUFFER, params.size, params.data, gl_usage);

    ShaderBufferGLES3 buffer;
    buffer.gl_id = GLBufferId(gl_buffer);
    buffer.size = params.size;
    buffer.gl_usage = gl_usage;

    auto add_result = m_pool.create(buffer);
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        glDeleteBuffers(1, &gl_buffer);
        return Error<>();
    }

    return Ok<ShaderBufferHandle>(add_result.value().first);
}

Result<const ShaderBufferGLES3*, void> ShaderBufferDBGLES3::get(
    const ShaderBufferHandle handle) const {
    auto result = m_pool.get(handle);
    if (!result) {
        return Error<>();
    }
    return Ok(result.value().get());
}

Result<> ShaderBufferDBGLES3::update(const ShaderBufferHandle handle,
                                     const void* ptr,
                                     const usize size,
                                     StateCacheGLES3& state_cache) {
    MODUS_PROFILE_THREED_BLOCK("ConstantBufferGLES3::update");
    auto result = m_pool.get(handle);
    if (!result) {
        return Error<>();
    }

    const ShaderBufferGLES3& buffer = *(result.value());

    if (buffer.gl_usage == GL_STATIC_DRAW) {
        MODUS_LOGE(
            "ConstantBufferGLES3: Attempting to update static constant "
            "buffer");
        return Error<>();
    }

    if (size > buffer.size) {
        MODUS_LOGE(
            "ConstantBufferGLES3: Updating constant buffer with the wrong "
            "size!");
        return Error<>();
    }

    state_cache.bind_buffer_storage(buffer.gl_id);
    const u32 offset = 0;
    glBufferData(GL_SHADER_STORAGE_BUFFER, buffer.size, nullptr, buffer.gl_usage);
    glBufferSubData(GL_SHADER_STORAGE_BUFFER, offset, size, ptr);
    return Ok<>();
}

void ShaderBufferDBGLES3::destroy_resource(const ShaderBufferGLES3& resource) {
    destroy_buffer(resource.gl_id);
}

}    // namespace modus::threed::gles3
