/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/fixed_handle_pool.hpp>
#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/program.hpp>

namespace modus::threed {
struct ProgramCreateParams;
}

namespace modus::threed::gles3 {

class ProgramGLES3 {
   public:
    GLProgramId program = GLProgramId();
    struct {
        GLShaderId vert = GLShaderId();
        GLShaderId frag = GLShaderId();
    } shaders;
    FixedVector<ProgramConstantInputParam, kMaxProgramConstantInputs> constants;
    FixedVector<ProgramSamplerInputParam, kMaxProgramSamplerInputs> textures;
#if !defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
    GLint loc_textures[kMaxProgramSamplerInputs];
    GLint loc_constants[kMaxProgramConstantInputs];
    GLint loc_cbuffers[kMaxProgramConstantInputs];
#endif
};

static constexpr u32 kMaxProgramCount = 64;
class ProgramDBGLES3
    : public ResourceDB<ProgramDBGLES3, ProgramGLES3, kMaxProgramCount, ProgramHandle> {
   public:
    ProgramDBGLES3() = default;

    Result<ProgramHandle, String> create(const ProgramCreateParams& params);

    void destroy_resource(const ProgramGLES3& resource);

    Result<> reflect(const ProgramHandle handle, ProgramReflection& reflection) const;
};

}    // namespace modus::threed::gles3
