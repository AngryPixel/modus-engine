/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

//clang-format off
#include <pch.h>
#include <gles3/instance_gles3.hpp>
//clang-format on
#include <gles3/api_gles3.hpp>
#include <gles3/device_gles3.hpp>
#include <gles3/program_gen/shader_generator_gles3.hpp>
namespace modus::threed::gles3 {

InstanceGLES3::InstanceGLES3() {}

InstanceGLES3::~InstanceGLES3() {}

InstanceResult InstanceGLES3::initialize() {
    auto api_init_result = gles3::initialize();
    if (!api_init_result) {
        return api_init_result;
    }

    return Ok<>();
}

void InstanceGLES3::shutdown() {}

Optional<FramebufferHandle> InstanceGLES3::default_framebuffer() {
    return Optional<FramebufferHandle>(gles3::default_framebuffer());
}

[[nodiscard]] std::unique_ptr<Device> InstanceGLES3::create_device() {
    return modus::make_unique<gles3::DeviceGLES3>(*this);
}

ApiVersion InstanceGLES3::api() const {
    ApiVersion version;
    version.api = Api::OpenGLES3;
#if defined(MODUS_THREED_USE_OPENGL_43)
    version.major = 4;
    version.minor = 2;
#else
    version.major = 3;
    version.minor = 0;
#endif
    version.patch = 0;
    return version;
}

}    // namespace modus::threed::gles3
