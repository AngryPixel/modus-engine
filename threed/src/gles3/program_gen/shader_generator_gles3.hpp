/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <threed/program_gen/shader_generator.hpp>

namespace modus::threed::gles3 {

class ShaderGeneratorGLES3 final : public dynamic::ShaderGenerator {
   public:
    Result<void, String> validate(const dynamic::ProgramGenParams&) const override;

    dynamic::PrecisionQualifier default_precision_qualifier() const override;

    WriteResult write_header(IByteWriter& writer, const dynamic::ShaderStage) const override;
    WriteResult write_input_header(IByteWriter& writer) const override;
    WriteResult write_snippet_header(IByteWriter& writer) const override;
    WriteResult write_types_header(IByteWriter& writer) const override;
    WriteResult write_vertex_input_header(IByteWriter&) const override;
    WriteResult write_fragment_output_header(IByteWriter&) const override;
    WriteResult write_main(IByteWriter& writer, const StringSlice main) const override;
    WriteResult write(IByteWriter& writer, const Slice<StringSlice> conditions) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const dynamic::ConstantInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const dynamic::ConstantBufferInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const dynamic::ShaderBufferInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const dynamic::SamplerInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer, const dynamic::FragmentOutput& desc) const override;
    WriteResult write(IByteWriter& writer, const dynamic::VertexInput& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const Slice<dynamic::VaryingDesc> descs,
                      const dynamic::ShaderStage stage) const override;
};

}    // namespace modus::threed::gles3
