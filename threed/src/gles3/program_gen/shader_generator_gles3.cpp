/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gles3/program_gen/shader_generator_gles3.hpp>
// clang-format on

#include <threed/program_gen/program_gen.hpp>
#include <gles3/api_gles3.hpp>
#include <core/io/byte_stream.hpp>

namespace modus::threed::gles3 {

Result<void, String> ShaderGeneratorGLES3::validate(const dynamic::ProgramGenParams&) const {
    return Ok<void>();
}

dynamic::PrecisionQualifier ShaderGeneratorGLES3::default_precision_qualifier() const {
    return dynamic::PrecisionQualifier::High;
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_header(
    IByteWriter& writer,
    const dynamic::ShaderStage stage) const {
    StringSlice stage_desc = "Unknown";
    if (stage == dynamic::ShaderStage::Vertex) {
        stage_desc = "Vertex";
    } else if (stage == dynamic::ShaderStage::Fragment) {
        stage_desc = "Fragment";
    }

    const StringSlice header = "#version 310 es\nprecision highp float;\nprecision highp int;\n";
    if (auto r = writer.write_exactly(header.as_bytes()); !r) {
        return r;
    }

    return writer.write_exactly(
        StringSlice(modus::format("// Auto generated {} shader, do not modify\n", stage_desc))
            .as_bytes());
}
ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_input_header(
    IByteWriter& writer) const {
    return writer.write_exactly(
        StringSlice("\n// ----- Shader Inputs -------------------- \n\n").as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_snippet_header(
    IByteWriter& writer) const {
    return writer.write_exactly(
        StringSlice("\n// ----- Code Snippets -------------------- \n\n").as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_types_header(
    IByteWriter& writer) const {
    return writer.write_exactly(
        StringSlice("\n// ----- Type Snippets -------------------- \n\n").as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_vertex_input_header(
    IByteWriter& writer) const {
    return writer.write_exactly(
        StringSlice("\n// ----- Vertex Inputs -------------------- \n\n").as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_fragment_output_header(
    IByteWriter& writer) const {
    return writer.write_exactly(
        StringSlice("\n// ----- Fragment Output -------------------- \n\n").as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write_main(IByteWriter& writer,
                                                                   const StringSlice main) const {
    if (auto r = writer.write_exactly(
            StringSlice("\n// ----- Shader Main   -------------------- \n\n").as_bytes());
        !r) {
        return r;
    }
    return writer.write_exactly(main.as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const Slice<StringSlice> conditions) const {
    if (auto r = writer.write_exactly(
            StringSlice("\n// ----- Conditions    -------------------- \n\n").as_bytes());
        !r) {
        return r;
    }
    for (const auto& condition : conditions) {
        const String text = modus::format("#define {}\n", condition);
        if (auto r = writer.write_exactly(StringSlice(text).as_bytes()); !r) {
            return r;
        }
    }
    return Ok(usize(0));
}
static constexpr const char* kDataTypeStr[] = {
    "int",   "uint",  "int",   "uint",  "int",   "uint",  "float", "ivec2", "uvec2",
    "ivec2", "uvec2", "ivec2", "uvec2", "vec2",  "ivec3", "uvec3", "ivec3", "uvec3",
    "ivec3", "uvec3", "vec3",  "ivec4", "uvec4", "ivec4", "uvec4", "ivec4", "uvec4",
    "vec4",  "mat2",  "mat3",  "mat4",  "vec4",  "vec4",
};
static_assert(modus::array_size(kDataTypeStr) == u32(DataType::Total));

static StringSlice qualifier_to_string(const dynamic::PrecisionQualifier qualifier) {
    if (qualifier == dynamic::PrecisionQualifier::Low) {
        return "lowp";
    } else if (qualifier == dynamic::PrecisionQualifier::Medium) {
        return "mediump";
    } else {
        return "highp";
    };
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const u8 slot,
    const dynamic::ConstantInputDesc& desc) const {
    modus_assert(desc.data_type < DataType::Total);
    const StringSlice data_type_str = kDataTypeStr[u32(desc.data_type)];
    StringSlice precision = qualifier_to_string(desc.pqualifier);
    const String constant_text = modus::format("layout(location={}) uniform {} {} {};\n", slot,
                                               precision, data_type_str, desc.name);

    return writer.write_exactly(StringSlice(constant_text).as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const u8 slot,
    const dynamic::ConstantBufferInputDesc& desc) const {
    const String buffer_intro =
        modus::format("layout(std140, binding={}) uniform {} {{\n", slot, desc.name);
    if (auto r = writer.write_exactly(StringSlice(buffer_intro).as_bytes()); !r) {
        return r;
    }
    for (const auto& member : desc.buffer_members) {
        StringSlice precision = qualifier_to_string(member.pqualifier);
        String text;
        if (member.type == dynamic::BufferMemberDesc::Type::Data) {
            modus_assert(member.data_type < DataType::Total);
            const StringSlice data_type_str = kDataTypeStr[u32(member.data_type)];
            text = modus::format("    {} {} {};\n", precision, data_type_str, member.name);
        } else {
            text = modus::format("    {} {} {};\n", precision, member.custom_type_str, member.name);
        }
        if (auto r = writer.write_exactly(StringSlice(text).as_bytes()); !r) {
            return r;
        }
    }
    return writer.write_exactly(StringSlice("};\n\n").as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const u8 slot,
    const dynamic::ShaderBufferInputDesc& desc) const {
    StringSlice access_str = "";
    if (desc.access == threed::dynamic::ShaderBufferInputDesc::Access::Read) {
        access_str = "readonly ";
    } else if (desc.access == threed::dynamic::ShaderBufferInputDesc::Access::Write) {
        access_str = "writeonly ";
    }

    const String buffer_intro =
        modus::format("layout(std430, binding={}) {}buffer {} {{\n", slot, access_str, desc.name);
    if (auto r = writer.write_exactly(StringSlice(buffer_intro).as_bytes()); !r) {
        return r;
    }
    for (const auto& member : desc.buffer_members) {
        StringSlice precision = qualifier_to_string(member.pqualifier);
        String text;
        if (member.type == dynamic::BufferMemberDesc::Type::Data) {
            modus_assert(member.data_type < DataType::Total);
            const StringSlice data_type_str = kDataTypeStr[u32(member.data_type)];
            text = modus::format("    {} {} {};\n", precision, data_type_str, member.name);
        } else {
            text = modus::format("    {} {} {};\n", precision, member.custom_type_str, member.name);
        }
        if (auto r = writer.write_exactly(StringSlice(text).as_bytes()); !r) {
            return r;
        }
    }
    return writer.write_exactly(StringSlice("};\n\n").as_bytes());
}

static StringSlice sampler_type_to_string(const dynamic::SamplerType st) {
    switch (st) {
        case dynamic::SamplerType::ST2D:
            return "sampler2D";
        case dynamic::SamplerType::ST2DArray:
            return "sampler2DArray";
        case dynamic::SamplerType::STCubeMap:
            return "samplerCube";
        case dynamic::SamplerType::STShadow:
            return "samplerShadow";
        default:
            modus_assert(false);
            return "Uknown";
    }
}

static char sampler_data_type_prefix(const dynamic::SamplerDataType dt) {
    switch (dt) {
        case dynamic::SamplerDataType::UInt:
            return 'u';
        case dynamic::SamplerDataType::Int:
            return 'i';
        case dynamic::SamplerDataType::Float:
            [[fallthrough]];
        default:
            return ' ';
    }
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const u8 slot,
    const dynamic::SamplerInputDesc& desc) const {
    const StringSlice sampler_str = sampler_type_to_string(desc.stype);
    const char sampler_prefix = sampler_data_type_prefix(desc.sdtype);
    StringSlice precision = qualifier_to_string(desc.pqualifier);

    const String text = modus::format("layout(binding={}) uniform {} {}{} {};\n", slot, precision,
                                      sampler_prefix, sampler_str, desc.name);

    return writer.write_exactly(StringSlice(text).as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const dynamic::FragmentOutput& desc) const {
    const StringSlice data_type_str = kDataTypeStr[u32(desc.data_type)];
    const String text =
        modus::format("layout(location={}) out {} {};\n", desc.slot, data_type_str, desc.name);
    return writer.write_exactly(StringSlice(text).as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const dynamic::VertexInput& desc) const {
    const StringSlice data_type_str = kDataTypeStr[u32(desc.data_type)];
    const String text =
        modus::format("layout(location={}) in {} {};\n", desc.slot, data_type_str, desc.name);
    return writer.write_exactly(StringSlice(text).as_bytes());
}

ShaderGeneratorGLES3::WriteResult ShaderGeneratorGLES3::write(
    IByteWriter& writer,
    const Slice<dynamic::VaryingDesc> descs,
    const dynamic::ShaderStage stage) const {
    String prefix = "";
    if (stage == dynamic::ShaderStage::Vertex) {
        prefix = "out";
    } else if (stage == dynamic::ShaderStage::Fragment) {
        prefix = "in";
    } else {
        modus_assert_message(false, "Unhandled shader stage");
    }

    for (const auto& desc : descs) {
        const StringSlice type_str =
            desc.type == dynamic::VaryingDesc::Type::Smooth ? "smooth" : "flat";
        const StringSlice data_type_str = kDataTypeStr[u32(desc.data_type)];
        StringSlice precision = qualifier_to_string(desc.pqualifier);
        const String text = modus::format("{} {} {} {} {};\n", type_str, prefix, precision,
                                          data_type_str, desc.name);
        if (auto r = writer.write_exactly(StringSlice(text).as_bytes()); !r) {
            return r;
        }
    }
    return Ok(usize(0));
}

}    // namespace modus::threed::gles3
