/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/state_cache_gles3.hpp>
#include <gles3/texture_gles3.hpp>
#include <threed/texture.hpp>

namespace modus::threed::gles3 {

static const GLenum kTextureTypeGL[] = {GL_TEXTURE_2D, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_CUBE_MAP,
                                        GL_TEXTURE_3D, GL_TEXTURE_2D_MULTISAMPLE};

static_assert(modus::array_size(kTextureTypeGL) == usize(TextureType::Total));

// clang-format off
static const GLTextureDesc kTextureFormatsGL[] = {
    {GL_RGBA, GL_RGBA8, GL_UNSIGNED_BYTE, false, false, false},
    {GL_RGB, GL_RGB8, GL_UNSIGNED_BYTE, false, false, false},
    {GL_RGB, GL_RGB565, GL_UNSIGNED_SHORT_5_6_5, false, false, false},
    {GL_RGBA, GL_RGBA4, GL_UNSIGNED_SHORT_4_4_4_4, false, false, false},
    {GL_RGBA, GL_RGB5_A1, GL_UNSIGNED_SHORT_5_5_5_1, false, false, false},
    {GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT24, GL_UNSIGNED_INT, false, true, false},
    {GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT32F, GL_FLOAT, false, true, false},
    {GL_DEPTH_STENCIL, GL_DEPTH24_STENCIL8, GL_UNSIGNED_INT_24_8, false, true, true},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_4x4_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_5x4_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_5x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_6x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_6x6_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_8x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_8x6_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_8x8_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_10x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_10x6_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_10x8_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_10x10_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_12x10_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA_ASTC_12x12_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RED, GL_R8, GL_UNSIGNED_BYTE, false, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR, GL_INVALID_ENUM, true, false, false},
    {GL_RGB, GL_SRGB8, GL_UNSIGNED_BYTE, false, false, false},
    {GL_RGBA, GL_SRGB8_ALPHA8, GL_UNSIGNED_BYTE, false, false, false},
    {GL_RED, GL_COMPRESSED_R11_EAC, GL_INVALID_ENUM, true, false, false},
    {GL_RED, GL_COMPRESSED_SIGNED_R11_EAC, GL_INVALID_ENUM, true, false, false},
    {GL_RG, GL_COMPRESSED_RG11_EAC, GL_INVALID_ENUM, true, false, false},
    {GL_RG, GL_COMPRESSED_SIGNED_RG11_EAC, GL_INVALID_ENUM, true, false, false},
    {GL_RGB, GL_COMPRESSED_RGB8_ETC2, GL_INVALID_ENUM, true, false, false},
    {GL_RGB, GL_COMPRESSED_SRGB8_ETC2, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_RGBA8_ETC2_EAC, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC, GL_INVALID_ENUM, true, false, false},
    {GL_RGBA, GL_RGB10_A2, GL_UNSIGNED_INT_2_10_10_10_REV, false, false, false},
    {GL_RGBA_INTEGER, GL_RGB10_A2UI, GL_UNSIGNED_INT_2_10_10_10_REV, false, false, false},
    {GL_RGB, GL_RGB16F, GL_HALF_FLOAT, false, false, false},
    {GL_RGBA, GL_RGBA16F, GL_HALF_FLOAT, false, false, false},
    {GL_RG, GL_RG16F, GL_HALF_FLOAT, false, false, false},
    {GL_RED_INTEGER, GL_R16UI, GL_UNSIGNED_SHORT, false, false, false},
    {GL_RG_INTEGER, GL_RG32UI, GL_UNSIGNED_INT, false, false, false},
    {GL_RG_INTEGER, GL_RG16UI, GL_UNSIGNED_SHORT, false, false, false},
    {GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT16, GL_HALF_FLOAT, false, true, false},
};

// clang-format on

static_assert(modus::array_size(kTextureFormatsGL) == usize(TextureFormat::Total));

const GLTextureDesc& gl_texture_desc(const TextureFormat format) {
    modus_assert(format < TextureFormat::Total);
    return kTextureFormatsGL[u8(format)];
}

static Result<TextureGLES3, void> create_texture(const TextureCreateParams& params,
                                                 StateCacheGLES3& state_cache) {
    if (params.type >= TextureType::Total || params.format >= TextureFormat::Total) {
        return Error<>();
    }

    if (params.sample_count < 2 && params.type == TextureType::T2DMultiSample) {
        // Need to have more than one sample for multisample texture
        return Error<>();
    }

    TextureGLES3 texture;
    const GLenum gl_type = kTextureTypeGL[u8(params.type)];
    GLuint gl_id;
    glGenTextures(1, &gl_id);
    texture.gl_id = GLTextureId(gl_id);
    texture.gl_type = kTextureTypeGL[u8(params.type)];
    texture.params = params;

    const GLTextureDesc& gl_texture_desc = kTextureFormatsGL[u8(params.format)];
    state_cache.bind_texture(0, texture.gl_id, gl_type);
    switch (params.type) {
        case TextureType::T2D:
        case TextureType::TCubeMap:
            glTexStorage2D(gl_type, params.mip_map_levels, gl_texture_desc.internal,
                           i32(params.width), i32(params.height));
            break;
        case TextureType::T2DMultiSample:
            glTexStorage2DMultisample(gl_type, params.sample_count, gl_texture_desc.internal,
                                      i32(params.width), i32(params.height), GL_TRUE);
            break;
        case TextureType::T2DArray:
        case TextureType::T3D:
            glTexStorage3D(gl_type, params.mip_map_levels, gl_texture_desc.internal,
                           i32(params.width), i32(params.height), i32(params.depth));
            break;
        default:
            modus_assert_message(false, "Should not happend!");
            break;
    }
#if !defined(MODUS_THREED_USE_OPENGL_ES3)
    if (params.border_color) {
        glTexParameterfv(gl_type, GL_TEXTURE_BORDER_COLOR, &params.border_color.value()[0]);
    }
#endif
    return Ok(texture);
}

static void destroy_texture(const GLTextureId id) {
    const GLuint gl_id = id;
    glDeleteTextures(1, &gl_id);
}

void TextureDBGLES3::destroy_resource(const TextureGLES3& resource) {
    destroy_texture(resource.gl_id);
}

Result<TextureHandle, void> TextureDBGLES3::create(const TextureCreateParams& params,
                                                   StateCacheGLES3& state_cache) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error<>();
    }

    modus_assert(params.width != 0);
    modus_assert(params.height != 0);
    modus_assert(params.depth != 0);
    auto create_result = create_texture(params, state_cache);
    if (!create_result) {
        return Error<>();
    }

    auto add_result = m_pool.create(create_result.value());
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        destroy_texture(create_result.value().gl_id);
        return Error<>();
    }

    return Ok<TextureHandle>(add_result.value().first);
}

}    // namespace modus::threed::gles3
