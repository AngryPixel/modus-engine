/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <gles3/device_gles3.hpp>
#include <gles3/instance_gles3.hpp>
#include <gles3/states_gles3.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/pipeline.hpp>

#if defined(MODUS_DEBUG)
#define MODUS_THREED_GLES3_DEBUG_EFFECT_DRAWABLE_INPUTS
#endif

namespace modus::threed::gles3 {

struct CommandGLES3 : public Command {
    CommandGLES3* next = nullptr;
};

struct PassGLES3 : public Pass {
    PassGLES3* pass_next = nullptr;
    CommandGLES3* command_first = nullptr;
    CommandGLES3* command_last = nullptr;
};

struct PipelineGLES3 : public Pipeline {
    PassGLES3* pass_first = nullptr;
    PassGLES3* pass_last = nullptr;
};

struct ConstantHandlePrivate {
    unsigned offset : 24;
    unsigned data_size : 8;
};

union ConstantHandleConversion {
    u32 integer;
    ConstantHandlePrivate constant;
};

u32 constant_handle_private_to_u32(const ConstantHandlePrivate handle) {
    ConstantHandleConversion c;
    c.constant = handle;
    return c.integer;
}

ConstantHandlePrivate u32_to_constant_handle_private(const u32 integer) {
    ConstantHandleConversion c;
    c.integer = integer;
    return c.constant;
}

static_assert(sizeof(ConstantHandlePrivate) == sizeof(u32));

DeviceGLES3::DeviceGLES3(InstanceGLES3& instance) : Device(instance) {}

Result<> DeviceGLES3::initialize() {
    MODUS_UNUSED(m_thread_watcher);
    modus_assert(m_thread_watcher.validate());

    if (!m_frame_allocator.initialize(4 * 1024 * 1024)) {
        return Error<>();
    }

    if (!m_drawable_db.initialize()) {
        return Error<>();
    }

    // Get max constant buffer size
    GLint max_uniform_block_size;
    glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &max_uniform_block_size);
    m_device_limits.max_constant_buffer_size = max_uniform_block_size;
    m_device_limits.min_constant_buffer_size = 16384;

    // Check texture support
#if defined(MODUS_THREED_USE_OPENGL_43)
    m_device_limits.compressed_texture_support.etc2 = FLEXT_ARB_ES3_compatibility == 1;
#else
    m_device_limits.compressed_texture_support.etc2 = true;
#endif

    // Do not report ASTC as supported on the Desktop as mesa emulates support
    // for certain hardware, where as ETC2 is guaranteed to be supported.
#if defined(MODUS_THREED_USE_OPENGL_43)
    m_device_limits.compressed_texture_support.astc = 0;
#else
    m_device_limits.compressed_texture_support.astc = FLEXT_KHR_texture_compression_astc_ldr == 1;
    m_device_limits.compressed_texture_support.astc = 0;
#endif
    m_device_limits.compressed_texture_support.dxt1 = FLEXT_EXT_texture_compression_dxt1 == 1;

#if defined(MODUS_THREED_USE_OPENGL_43)
    m_device_limits.compressed_texture_support.etc1 = false;
#else
    m_device_limits.compressed_texture_support.etc1 = FLEXT_OES_compressed_ETC1_RGB8_texture == 1;
#endif
    m_device_limits.compressed_texture_support.s3tc = FLEXT_EXT_texture_compression_s3tc;
    m_device_limits.compressed_texture_support.s3tc_srgb = FLEXT_EXT_texture_compression_s3tc_srgb;

    GLint max_texture_size = 0;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);
    m_device_limits.max_texture_size_px = max_texture_size;

    GLint shader_storage_buffer_offset_alignment = 0;
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT,
                  &shader_storage_buffer_offset_alignment);
    m_device_limits.shader_buffer_offset_alignment = shader_storage_buffer_offset_alignment;

    GLint uniform_buffer_offset_alignment = 0;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &uniform_buffer_offset_alignment);
    m_device_limits.constant_buffer_offset_alignment = uniform_buffer_offset_alignment;

    m_constant_buffer_db.initialize();
    // Enable FRAMEBUFFER_SRGB
    // glEnable(0x8DB9);
#if defined(MODUS_THREED_USE_OPENGL_43)
    glEnable(GL_MULTISAMPLE);
#endif
    return Ok<>();
}

void DeviceGLES3::shutdown() {
    modus_assert(m_thread_watcher.validate());
    m_frame_allocator.shutdown();
    m_drawable_db.shutdown();
    m_program_db.shutdown();
    m_effect_db.shutdown();
    m_buffer_db.shutdown();
    m_constant_buffer_db.shutdown();
    m_shader_buffer_db.shutdown();
    m_framebuffer_db.shutdown();
    m_texture_db.shutdown();
    m_sampler_db.shutdown();
}

const dynamic::ShaderGenerator& DeviceGLES3::shader_generator() const {
    return m_shader_generator;
}

void DeviceGLES3::begin_frame() {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    m_pipeline_created = false;
    m_pipeline_submitted = false;
#endif
    modus_assert(m_thread_watcher.validate());
    // Device cache needs to be reset due to VAO changing the state
    // of bound buffers or the swap buffers clearing out the bound buffers?
    m_state_cache.reset_buffer_cache();
    m_frame_allocator.clear();

    // Destroy resources from previous frame
    m_drawable_db.flush_destroy_queue();
    m_program_db.flush_destroy_queue();
    m_effect_db.flush_destroy_queue();
    m_buffer_db.flush_destroy_queue();
    m_constant_buffer_db.flush_destroy_queue();
    m_shader_buffer_db.flush_destroy_queue();
    m_framebuffer_db.flush_destroy_queue();
    m_texture_db.flush_destroy_queue();
    m_sampler_db.flush_destroy_queue();
}

void DeviceGLES3::end_frame() {
    modus_assert(m_thread_watcher.validate());
    m_constant_buffer_db.swap_buffers();
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    m_pipeline_created = false;
    m_pipeline_submitted = false;
#endif
    /*
    for (u32 i = 0; i < kMaxProgramConstantInputs; ++i) {
        glBindBufferBase(GL_UNIFORM_BUFFER, i, 0);
    }*/
}

const DeviceLimits& DeviceGLES3::device_limits() const {
    return m_device_limits;
}

Result<FramebufferHandle, FramebufferError> DeviceGLES3::create_framebuffer(
    const FramebufferCreateParams& params) {
    modus_assert(m_thread_watcher.validate());
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new resources once pipeline is submitted");
    }
#endif
    return m_framebuffer_db.create(params, m_texture_db, m_state_cache);
}

void DeviceGLES3::destroy_framebuffer(const FramebufferHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_framebuffer_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES3: Failed to destroy framebuffer");
    }
}

void DeviceGLES3::framebuffer_copy(const FramebufferCopyParams& params) {
    modus_assert(m_thread_watcher.validate());
    auto r_from = m_framebuffer_db.get(params.from.handle);
    auto r_to = m_framebuffer_db.get(params.to.handle);

    if (!r_from || !r_to) {
        modus_assert(false);
        return;
    }

    GLuint flags = 0;
    if (params.copy_color) {
        flags |= GL_COLOR_BUFFER_BIT;
    }
    if (params.copy_depth) {
        flags |= GL_DEPTH_BUFFER_BIT;
    }
    if (params.copy_stencil) {
        flags |= GL_STENCIL_BUFFER_BIT;
    }

    m_state_cache.bind_framebuffer_read(r_from.value()->gl_id);
    m_state_cache.bind_framebuffer_draw(r_to.value()->gl_id);
    glBlitFramebuffer(params.from.x, params.from.y, params.from.x + params.from.width,
                      params.from.y + params.from.height, params.to.x, params.to.y,
                      params.to.x + params.to.width, params.to.y + params.to.height, flags,
                      params.fast ? GL_NEAREST : GL_LINEAR);
    modus_assert(m_thread_watcher.validate());
}

static const GLenum kGlPrimitive[] = {GL_POINTS,    GL_LINES,          GL_LINE_STRIP,  GL_LINE_LOOP,
                                      GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN};

static_assert(modus::array_size(kGlPrimitive) == usize(Primitive::Total));

static const GLenum kGlIndexType[] = {GL_INVALID_ENUM, GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT,
                                      GL_UNSIGNED_INT};

static_assert(modus::array_size(kGlIndexType) == usize(IndicesType::Total));

void DeviceGLES3::execute_pipeline(const Pipeline& pipeline) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    m_pipeline_submitted = true;
#endif
    modus_assert(m_thread_watcher.validate());
    const PipelineGLES3& pipeline_gl = static_cast<const PipelineGLES3&>(pipeline);
    for (PassGLES3* pass_ptr = pipeline_gl.pass_first; pass_ptr != nullptr;
         pass_ptr = pass_ptr->pass_next) {
        MODUS_PROFILE_THREED_BLOCK("GLES3 Pass");
        modus_assert(pass_ptr != nullptr);

        const PassGLES3& pass = *pass_ptr;

        auto r_framebuffer = m_framebuffer_db.get(pass.frame_buffer);
        if (!r_framebuffer) {
            modus_assert(false);
            continue;
        }

        // Pre-pass framebuffer copy
        if (pass.framebuffer_copy.pre_pass) {
            const PassFramebufferCopy& copy_params = *pass.framebuffer_copy.pre_pass;
            FramebufferCopyParams params;
            params.from.x = copy_params.other.x;
            params.from.y = copy_params.other.y;
            params.from.width = copy_params.other.width;
            params.from.height = copy_params.other.height;
            params.from.handle = copy_params.other.handle;
            params.to.handle = pass.frame_buffer;
            params.to.x = copy_params.pass.x;
            params.to.y = copy_params.pass.x;
            params.to.width = copy_params.pass.width;
            params.to.height = copy_params.pass.height;
            params.copy_color = copy_params.copy_color;
            params.copy_depth = copy_params.copy_depth;
            params.copy_stencil = copy_params.copy_stencil;
            framebuffer_copy(params);
        }

        // Bind ViewPort
        m_state_cache.bind_framebuffer_draw(r_framebuffer.value()->gl_id);

        // Only apply draw buffer if this is not the default framebuffer
        if (r_framebuffer.value()->gl_id.value() != 0) {
            // Select Draw buffers
            FixedVector<GLenum, kMaxFrambufferColorTargets> draw_buffers;
            for (u8 i = 0; i < kMaxFrambufferColorTargets; ++i) {
                if (pass.color_targets & (1 << i)) {
                    draw_buffers.push_back(GL_COLOR_ATTACHMENT0 + i)
                        .expect("Failed to push to draw_buffers");
                }
            }
            if (draw_buffers.size() != 0) {
                glDrawBuffers(draw_buffers.size(), draw_buffers.data());
            } else {
                const GLenum draw_buffer = GL_NONE;
                glDrawBuffers(1, &draw_buffer);
            }
        }

        // Set View Port
        m_state_cache.set_view_port(pass.viewport.x, pass.viewport.y, pass.viewport.width,
                                    pass.viewport.height);

        if (pass.state.scissor.enabled) {
            const ScissorState& state = pass.state.scissor;
            m_state_cache.set_scissor_test_enabled(true);
            m_state_cache.set_scissor(state.x, state.y, state.width, state.height);
        } else {
            m_state_cache.set_scissor_test_enabled(false);
        }

        for (u32 i = 0; i < kMaxRenderTargetCount; ++i) {
            if (pass.state.clear.colour[i].clear) {
                clear_colour(i, pass.state.clear.colour[i].colour);
            }
        }

        if (pass.state.clear.clear_depth) {
            clear_depth(pass.state.clear.depth);
        }

        if (pass.state.clear.clear_stencil) {
            clear_stencil(pass.state.clear.stencil);
        }

        {
            MODUS_PROFILE_THREED_BLOCK("GLES3 Pass Commands");
            for (const CommandGLES3* command_ptr = pass.command_first; command_ptr != nullptr;
                 command_ptr = command_ptr->next) {
                const Command& command = *command_ptr;
                NotMyPtr<const DrawableGLES3> drawable;
                if (command.drawable.is_valid()) {
                    auto r_drawable = m_drawable_db.get(command.drawable);
                    if (!r_drawable) {
                        modus_assert_message(false, "Failed to locate drawable");
                        break;
                    }
                    drawable = r_drawable.value();
                }

                if (command.scissor.enabled) {
                    const ScissorState& state = command.scissor;
                    m_state_cache.set_scissor_test_enabled(true);
                    m_state_cache.set_scissor(state.x, state.y, state.width, state.height);
                } else {
                    m_state_cache.set_scissor_test_enabled(false);
                }

                auto r_program = m_program_db.get(command.program);
                modus_assert_message(r_program, "Couldn't locate gpu program");
                if (!r_program) {
                    break;
                }
                auto effect_lookup_result = m_effect_db.get(command.effect);
                if (!effect_lookup_result) {
                    modus_assert_message(false, "Failed to locate effect");
                    break;
                }
                NotMyPtr<const EffectGLES3> effect = effect_lookup_result.value();

                // Setup the effect
                apply_effect(*r_program, *effect, command);

                MODUS_PROFILE_THREED_BLOCK("GLES3 Draw");
                // Draw
                // const DrawState& draw_state = command.draw_state;
                GLenum gl_primitive = GL_INVALID_ENUM;
                u32 draw_start = 0;
                u32 draw_count = 0;
                u32 index_buffer_offset = 0;
                if (drawable) {
                    draw_start = drawable->params.start;
                    draw_count = drawable->params.count;
                    index_buffer_offset = drawable->params.index_buffer.offset;
                }
#if defined MODUS_THREED_GLES3_DEBUG_EFFECT_DRAWABLE_INPUTS
                else {
                    modus_assert(command.draw.type == DrawType::Custom);
                }
#endif
                if (command.draw.type == DrawType::CustomPrimitive ||
                    command.draw.type == DrawType::Custom) {
                    modus_assert(command.draw.primitive < Primitive::Total);
                    gl_primitive = kGlPrimitive[static_cast<u8>(command.draw.primitive)];
                } else if (drawable) {
                    modus_assert(drawable->params.primitive < Primitive::Total);
                    gl_primitive = kGlPrimitive[static_cast<u8>(drawable->params.primitive)];
                }

                if (command.draw.type == DrawType::CustomRange ||
                    command.draw.type == DrawType::Custom) {
                    draw_start = command.draw.range_start;
                    draw_count = command.draw.range_count;
                }

                if (drawable) {
                    m_state_cache.bind_vao(drawable->vao);

                    if (command.drawable_update) {
                        const DrawableUpdateParams* update_params =
                            reinterpret_cast<const DrawableUpdateParams*>(
                                (const void*)(m_frame_allocator.data() +
                                              command.drawable_update.value()));
                        [[maybe_unused]] auto r = m_drawable_db.update(
                            command.drawable, *update_params, m_buffer_db, m_state_cache);
                        modus_assert_message(r, "Failed to update drawable");
                    }

                    modus_assert(drawable->params.index_type < IndicesType::Total);
                    if (drawable->params.index_type != IndicesType::None) {
                        const GLenum gl_index_type = kGlIndexType[u8(drawable->params.index_type)];
                        modus_assert(drawable->params.index_buffer.buffer.is_valid());
                        if (command.draw.instance_count == 0) {
                            glDrawElements(gl_primitive, draw_count, gl_index_type,
                                           (void*)usize(index_buffer_offset + draw_start));
                            /*
                            glDrawRangeElements(gl_primitive, draw_start,
                                                draw_start + draw_count,
                            draw_count, gl_index_type, nullptr);*/
                        } else {
                            glDrawElementsInstanced(
                                gl_primitive, draw_count, gl_index_type,
                                (void*)(usize(index_buffer_offset + draw_start)),
                                command.draw.instance_count);
                        }
                    } else {
                        if (command.draw.instance_count == 0) {
                            glDrawArrays(gl_primitive, draw_start, draw_count);
                        } else {
                            glDrawArraysInstanced(gl_primitive, draw_start, draw_count,
                                                  command.draw.instance_count);
                        }
                    }
                } else {
                    m_state_cache.bind_vao(m_drawable_db.default_vao());
                    glDrawArrays(gl_primitive, draw_start, draw_count);
                }
            }
        }

        // Post-pass framebuffer copy
        if (pass.framebuffer_copy.post_pass) {
            const PassFramebufferCopy& copy_params = *pass.framebuffer_copy.post_pass;
            FramebufferCopyParams params;
            params.to.x = copy_params.other.x;
            params.to.y = copy_params.other.y;
            params.to.width = copy_params.other.width;
            params.to.height = copy_params.other.height;
            params.to.handle = copy_params.other.handle;
            params.from.handle = pass.frame_buffer;
            params.from.x = copy_params.pass.x;
            params.from.y = copy_params.pass.x;
            params.from.width = copy_params.pass.width;
            params.from.height = copy_params.pass.height;
            params.copy_color = copy_params.copy_color;
            params.copy_depth = copy_params.copy_depth;
            params.copy_stencil = copy_params.copy_stencil;
            framebuffer_copy(params);
        }
    }

    // Make sure to reset vao so that buffer updates don't affect last bound VAO
    m_state_cache.bind_vao(GLVAOId(0));
}

Result<ProgramHandle, String> DeviceGLES3::create_program(const ProgramCreateParams& params) {
    modus_assert(m_thread_watcher.validate());
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new resources once pipeline is submitted");
    }
#endif
    return m_program_db.create(params);
}

void DeviceGLES3::destroy_program(const ProgramHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_program_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES: Failed to destory program");
    }
}

Result<> DeviceGLES3::reflect_program(const ProgramHandle handle,
                                      ProgramReflection& reflection) const {
    modus_assert(m_thread_watcher.validate());
    return m_program_db.reflect(handle, reflection);
}

Result<BufferHandle, void> DeviceGLES3::create_buffer(const BufferCreateParams& params) {
    modus_assert(m_thread_watcher.validate());
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new resources once pipeline is submitted");
    }
#endif
    return m_buffer_db.create(params, m_state_cache);
}

void DeviceGLES3::destroy_buffer(const BufferHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_buffer_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES3: Failed to destroy buffer");
    }
}
Result<ConstantBufferHandle, void> DeviceGLES3::create_constant_buffer(
    const ConstantBufferCreateParams& params) {
    modus_assert(m_thread_watcher.validate());
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    if (params.size > m_device_limits.max_constant_buffer_size) {
        MODUS_LOGE("DeviceGLES3: Max constant buffer size exceed: {} > {}", params.size,
                   m_device_limits.max_constant_buffer_size);
        return Error<>();
    }

    return m_constant_buffer_db.create(params, m_state_cache);
}

void DeviceGLES3::destroy_constant_buffer(const ConstantBufferHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_constant_buffer_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES3: Failed to destroy constant buffer");
    }
}

Result<> DeviceGLES3::update_constant_buffer(const ConstantBufferHandle handle,
                                             const void* ptr,
                                             const usize size) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_constant_buffer_db.update(handle, ptr, size, m_state_cache);
}

Result<ShaderBufferHandle, void> DeviceGLES3::create_shader_buffer(
    const ShaderBufferCreateParams& params) {
    modus_assert(m_thread_watcher.validate());
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return m_shader_buffer_db.create(params, m_state_cache);
}

void DeviceGLES3::destroy_shader_buffer(const ShaderBufferHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_shader_buffer_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES3: Failed to destroy shader buffer");
    }
}

Result<> DeviceGLES3::update_shader_buffer(const ShaderBufferHandle handle,
                                           const void* ptr,
                                           const usize size) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_shader_buffer_db.update(handle, ptr, size, m_state_cache);
}

Result<> DeviceGLES3::update_buffer(const BufferHandle handle,
                                    const ByteSlice data,
                                    const u32 start_offset,
                                    const bool invalidate_buffer) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_buffer_db.update(handle, data, start_offset, invalidate_buffer, m_state_cache);
}

Result<EffectHandle, String> DeviceGLES3::create_effect(const EffectCreateParams& params) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_effect_db.create(params);
}

void DeviceGLES3::destroy_effect(const EffectHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_effect_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES3: Failed to destroy effect");
    }
}

ConstantHandle DeviceGLES3::allocate_constant(const void* ptr, const usize size) {
    modus_assert(m_thread_watcher.validate());
    u8* constant_ptr = reinterpret_cast<u8*>(m_frame_allocator.allocate(size));
    modus_assert(size <= sizeof(glm::mat4));
    modus_assert(constant_ptr != nullptr);
    if (constant_ptr == nullptr) {
        return ConstantHandle();
    }
    // copy data
    memcpy(constant_ptr, ptr, size);
    ConstantHandlePrivate cp;
    cp.data_size = static_cast<u8>(size);
    // calculate offset
    const u8* allocator_start = m_frame_allocator.data();
    const usize diff =
        reinterpret_cast<usize>(constant_ptr) - reinterpret_cast<usize>(allocator_start);
    modus_assert(diff < (1 << 24));
    cp.offset = static_cast<u32>(diff);
    return ConstantHandle(constant_handle_private_to_u32(cp));
}

Result<TextureHandle, void> DeviceGLES3::create_texture(const TextureCreateParams& params) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());

    if (params.width >= m_device_limits.max_texture_size_px ||
        params.height >= m_device_limits.max_texture_size_px) {
        MODUS_LOGE(
            "Allocation a texture with w:{}px and h:{}px, but maximum device "
            "limit is {}",
            params.width, params.height, m_device_limits.max_texture_size_px);
        return Error<>();
    }
    return m_texture_db.create(params, m_state_cache);
}

static void gl_update_texture(StateCacheGLES3& state_cache,
                              const TextureGLES3& gl_texture,
                              Slice<TextureData> data) {
    GLint previous_unpack_alignment = 0;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &previous_unpack_alignment);
    const GLTextureDesc& texture_desc = gl_texture_desc(gl_texture.params.format);

    GLint cur_unpack_alignment = previous_unpack_alignment;
    state_cache.bind_texture(0, gl_texture.gl_id, gl_texture.gl_type);
    if (gl_texture.params.type == TextureType::T2D ||
        gl_texture.params.type == TextureType::TCubeMap) {
        u32 index = 0;
        for (auto& tex_data : data) {
            if (cur_unpack_alignment != tex_data.unpack_alignment) {
                glPixelStorei(GL_UNPACK_ALIGNMENT, tex_data.unpack_alignment);
                cur_unpack_alignment = tex_data.unpack_alignment;
            }
            const GLenum gl_target = gl_texture.params.type == TextureType::T2D
                                         ? GL_TEXTURE_2D
                                         : (GL_TEXTURE_CUBE_MAP_POSITIVE_X + index);
            if (!texture_desc.compressed) {
                glTexSubImage2D(gl_target, tex_data.mip_map_level, 0, 0, GLsizei(tex_data.width),
                                GLsizei(tex_data.height), texture_desc.format,
                                texture_desc.data_type, tex_data.data.data());
            } else {
                glCompressedTexSubImage2D(gl_target, tex_data.mip_map_level, 0, 0,
                                          GLsizei(tex_data.width), GLsizei(tex_data.height),
                                          texture_desc.internal, tex_data.data.size(),
                                          tex_data.data.data());
            }
            ++index;
        }
    } else if (gl_texture.params.type == TextureType::T2DArray) {
        for (auto& tex_data : data) {
            if (cur_unpack_alignment != tex_data.unpack_alignment) {
                glPixelStorei(GL_UNPACK_ALIGNMENT, tex_data.unpack_alignment);
                cur_unpack_alignment = tex_data.unpack_alignment;
            }
            const GLenum gl_target = GL_TEXTURE_2D_ARRAY;
            if (!texture_desc.compressed) {
                glTexSubImage3D(gl_target, tex_data.mip_map_level, 0, 0, GLsizei(tex_data.depth),
                                GLsizei(tex_data.width), GLsizei(tex_data.height), 1,
                                texture_desc.format, texture_desc.data_type, tex_data.data.data());
            } else {
                glCompressedTexSubImage3D(gl_target, tex_data.mip_map_level, 0, 0,
                                          GLsizei(tex_data.depth), GLsizei(tex_data.width),
                                          GLsizei(tex_data.height), 1, texture_desc.internal,
                                          tex_data.data.size(), tex_data.data.data());
            }
        }
    }
    glPixelStorei(GL_UNPACK_ALIGNMENT, previous_unpack_alignment);
}

Result<TextureHandle, void> DeviceGLES3::create_texture(const TextureCreateParams& params,
                                                        Slice<TextureData> data) {
    modus_assert(m_thread_watcher.validate());
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif

    if (params.width >= m_device_limits.max_texture_size_px ||
        params.height >= m_device_limits.max_texture_size_px) {
        MODUS_LOGE(
            "Allocation a texture with w:{}px and h:{}px, but maximum device "
            "limit is {}",
            params.width, params.height, m_device_limits.max_texture_size_px);
        return Error<>();
    }

    if (!(params.type == TextureType::T2D || params.type == TextureType::TCubeMap ||
          params.type == TextureType::T2DArray)) {
        modus_assert_message(false, "Not yet supported!");
        return Error<>();
    }

    auto r_texture_handle = m_texture_db.create(params, m_state_cache);
    if (!r_texture_handle) {
        return r_texture_handle;
    }

    auto r_texture = m_texture_db.get(r_texture_handle.value());
    modus_assert(r_texture);
    if (!r_texture) {
        return Error<>();
    }

    const TextureGLES3& gl_texture = *(r_texture.value());
    gl_update_texture(m_state_cache, gl_texture, data);
    return r_texture_handle;
}

Result<TextureHandle, void> DeviceGLES3::create_texture(const TextureCreateParams& params,
                                                        Slice<TextureDataCubeMap> data) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    MODUS_UNUSED(params);
    MODUS_UNUSED(data);
    return Error<>();
}

Result<> DeviceGLES3::update_texture(const TextureHandle h, Slice<TextureData> data) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    auto r_texture = m_texture_db.get(h);
    modus_assert(r_texture);
    if (!r_texture) {
        return Error<>();
    }
    const TextureGLES3& gl_texture = *(r_texture.value());
    gl_update_texture(m_state_cache, gl_texture, data);
    return Ok<>();
}

void DeviceGLES3::destroy_texture(const TextureHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_texture_db.destroy(handle)) {
        MODUS_LOGW("DeviceGLES3: Failed to destroy texture");
    }
}

Result<DrawableHandle, String> DeviceGLES3::create_drawable(const DrawableCreateParams& params) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_drawable_db.create(params, m_buffer_db, m_state_cache);
}

void DeviceGLES3::destroy_drawable(const DrawableHandle handle) {
    modus_assert(m_thread_watcher.validate());
    (void)m_drawable_db.destroy(handle);
}

Result<void, String> DeviceGLES3::update_drawable(const DrawableHandle handle,
                                                  const DrawableUpdateParams& params) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_drawable_db.update(handle, params, m_buffer_db, m_state_cache);
}

Result<DrawableUpdateHandle> DeviceGLES3::allocate_drawable_update(
    const DrawableUpdateParams& params) {
    for (auto& buffer : params.data_buffers) {
        if (buffer.buffer && !m_buffer_db.get(buffer.buffer)) {
            return Error<>();
        }
    }
    if (params.index_buffer.buffer && !m_buffer_db.get(params.index_buffer.buffer)) {
        return Error<>();
    }

    DrawableUpdateParams* params_ptr = reinterpret_cast<DrawableUpdateParams*>(
        m_frame_allocator.allocate(sizeof(DrawableUpdateParams)));
    if (params_ptr == nullptr) {
        return Error<>();
    }
    *params_ptr = params;

    const u8* allocator_start = m_frame_allocator.data();
    const usize diff =
        reinterpret_cast<usize>(params_ptr) - reinterpret_cast<usize>(allocator_start);
    return Ok(DrawableUpdateHandle(u32(diff)));
}

Result<SamplerHandle, String> DeviceGLES3::create_sampler(const SamplerCreateParams& params) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    modus_assert(m_thread_watcher.validate());
    return m_sampler_db.create(params);
}

void DeviceGLES3::destroy_sampler(const SamplerHandle handle) {
    modus_assert(m_thread_watcher.validate());
    (void)m_sampler_db.destroy(handle);
}

Command* DeviceGLES3::allocate_command(Pass& pass) {
    void* ptr = this->m_frame_allocator.allocate(sizeof(CommandGLES3));
    if (ptr == nullptr) {
        modus_panic("DeviceGLES3: Failed to allocate command");
    }
    CommandGLES3* command = new (ptr) CommandGLES3();
    command->next = nullptr;
    PassGLES3& pass_gl = static_cast<PassGLES3&>(pass);
    if (pass_gl.command_first != nullptr) {
        pass_gl.command_last->next = command;
        pass_gl.command_last = command;
    } else {
        pass_gl.command_first = command;
        pass_gl.command_last = command;
    }
    return command;
}

Pass* DeviceGLES3::allocate_pass(Pipeline& pipeline, const StringSlice name) {
    void* ptr = this->m_frame_allocator.allocate(sizeof(PassGLES3));
    if (ptr == nullptr) {
        modus_panic("DeviceGLES3: Failed to allocate pass");
    }
    PipelineGLES3& pipeline_gl = static_cast<PipelineGLES3&>(pipeline);
    PassGLES3* pass = new (ptr) PassGLES3();
    pass->name = name;
    pass->pass_next = nullptr;
    pass->command_last = nullptr;
    pass->command_first = nullptr;
    if (pipeline_gl.pass_first != nullptr) {
        pipeline_gl.pass_last->pass_next = pass;
        pipeline_gl.pass_last = pass;
    } else {
        pipeline_gl.pass_first = pass;
        pipeline_gl.pass_last = pass;
    }
    return pass;
}

Pipeline* DeviceGLES3::allocate_pipeline(const StringSlice name) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_created) {
        modus_panic("There can be only one pipeline");
    }
    m_pipeline_created = true;
#endif
    void* ptr = this->m_frame_allocator.allocate(sizeof(PipelineGLES3));
    if (ptr == nullptr) {
        modus_panic("DeviceGLES3: Failed to allocate pipeline");
    }
    PipelineGLES3* pipeline = new (ptr) PipelineGLES3();
    pipeline->name = name;
    pipeline->pass_last = nullptr;
    pipeline->pass_first = nullptr;
    return pipeline;
}

void DeviceGLES3::clear_colour(const u32 index, const glm::vec4& colour) {
    glClearBufferfv(GL_COLOR, index, &colour.x);
    // glClear(GL_COLOR_BUFFER_BIT);
}

void DeviceGLES3::clear_depth(const f32 depth) {
    // Clear requires writable access to depth mask which may have been disabled
    // by an effect
    m_state_cache.set_depth_mask_enabled(true);
    glClearDepthf(depth);
    glClear(GL_DEPTH_BUFFER_BIT);
}

void DeviceGLES3::clear_stencil(const u8 stencil) {
    glClearStencil(stencil);
    glClear(GL_STENCIL_BUFFER_BIT);
}

void DeviceGLES3::apply_depth_stencil_state(const DepthStencilState& ds) {
    m_state_cache.set_depth_test_enabled(ds.depth.enabled);
    if (ds.depth.enabled) {
        m_state_cache.set_depth_mask_enabled(ds.depth.mask);
        m_state_cache.set_depth_func(gl_compare_function(ds.depth.function));
    }

    m_state_cache.set_stencil_test_enabled(ds.stencil.enabled);
    if (ds.stencil.enabled) {
        const GLenum gl_front_compare = gl_compare_function(ds.stencil.front.compare);
        const GLenum gl_back_compare = gl_compare_function(ds.stencil.back.compare);

        const GLenum gl_front_op_pass = gl_stencil_op(ds.stencil.front.pass);
        const GLenum gl_front_op_fail_stencil = gl_stencil_op(ds.stencil.front.fail);
        const GLenum gl_front_op_fail_depth = gl_stencil_op(ds.stencil.front.fail_depth);

        const GLenum gl_back_op_pass = gl_stencil_op(ds.stencil.back.pass);
        const GLenum gl_back_op_fail_stencil = gl_stencil_op(ds.stencil.back.fail);
        const GLenum gl_back_op_fail_depth = gl_stencil_op(ds.stencil.back.fail_depth);

        glStencilFuncSeparate(GL_FRONT, gl_front_compare, ds.stencil.ref, ds.stencil.mask);
        glStencilFuncSeparate(GL_BACK, gl_back_compare, ds.stencil.ref, ds.stencil.mask);

        glStencilOpSeparate(GL_FRONT, gl_front_op_fail_stencil, gl_front_op_fail_depth,
                            gl_front_op_pass);
        glStencilOpSeparate(GL_BACK, gl_back_op_fail_stencil, gl_back_op_fail_depth,
                            gl_back_op_pass);
    }
}

static const GLenum kGLBlendEquation[] = {
    GL_ZERO,
    GL_ONE,
    GL_SRC_COLOR,
    GL_ONE_MINUS_SRC_COLOR,
    GL_SRC_ALPHA,
    GL_ONE_MINUS_SRC_ALPHA,
    GL_DST_COLOR,
    GL_ONE_MINUS_DST_COLOR,
    GL_DST_ALPHA,
    GL_ONE_MINUS_DST_ALPHA,
    GL_CONSTANT_COLOR,
    GL_ONE_MINUS_CONSTANT_COLOR,
    GL_CONSTANT_ALPHA,
    GL_ONE_MINUS_CONSTANT_ALPHA,
    GL_SRC_ALPHA_SATURATE,
};

static_assert(modus::array_size(kGLBlendEquation) == usize(BlendEquation::Total));

static const GLenum kGLBlendOperation[] = {GL_FUNC_ADD, GL_FUNC_SUBTRACT, GL_FUNC_REVERSE_SUBTRACT,
                                           GL_MIN, GL_MAX};

static_assert(modus::array_size(kGLBlendOperation) == usize(BlendOperation::Total));

void DeviceGLES3::apply_blend_state(const BlendState& bs) {
    MODUS_UNUSED(bs);

    modus_assert_message(!bs.aplha_to_coverage, "Not yet implemented!");
#if defined(MODUS_ENABLE_ASSERTS)
    for (u32 i = 1; i < kMaxRenderTargetCount; ++i) {
        modus_assert_message(bs.targets[1].enabled == false, "Not supported");
    }
#endif
    m_state_cache.set_blend_enabled(bs.targets[0].enabled);
    if (bs.targets[0].enabled) {
        m_state_cache.set_blend_params(bs.independent_blending,
                                       kGLBlendEquation[u32(bs.targets[0].eq_source)],
                                       kGLBlendEquation[u32(bs.targets[0].eq_destination)],
                                       kGLBlendEquation[u32(bs.targets[0].eq_source_alpha)],
                                       kGLBlendEquation[u32(bs.targets[0].eq_destination_alpha)],
                                       kGLBlendOperation[u32(bs.targets[0].op_colour)],
                                       kGLBlendOperation[u32(bs.targets[0].op_alpha)]);
    }
}

void DeviceGLES3::apply_raster_state(const RasterState& rs) {
    m_state_cache.set_cullface_enabled(rs.cull_enabled);
    if (rs.cull_enabled) {
        m_state_cache.set_front_face(rs.face_counter_clockwise ? GL_CCW : GL_CW);
        GLenum gl_cull_mode = GL_INVALID_ENUM;
        switch (rs.cull_mode) {
            case CullMode::Front:
                gl_cull_mode = GL_FRONT;
                break;
            case CullMode::Back:
                gl_cull_mode = GL_BACK;
                break;
            case CullMode::FrontAndBack:
                gl_cull_mode = GL_FRONT_AND_BACK;
                break;
            default:
                modus_assert_message(false, "Should not be reached");
                break;
        }
        m_state_cache.set_cullface(gl_cull_mode);
    }
}

static void upload_uniform(const GLint loc, const void* ptr, const u32 size, const DataType type) {
    MODUS_UNUSED(size);
    modus_assert(loc != -1);
    switch (type) {
        case DataType::F32:
            modus_assert(size == sizeof(f32));
            glUniform1fv(loc, 1, reinterpret_cast<const f32*>(ptr));
            break;
        case DataType::I32:
            modus_assert(size == sizeof(i32));
            glUniform1iv(loc, 1, reinterpret_cast<const i32*>(ptr));
            break;
        case DataType::U32:
            modus_assert(size == sizeof(u32));
            glUniform1uiv(loc, 1, reinterpret_cast<const u32*>(ptr));
            break;
        case DataType::Vec2F32:
            modus_assert(size == sizeof(glm::vec2));
            glUniform2fv(loc, 1, reinterpret_cast<const f32*>(ptr));
            break;
        case DataType::Vec2I32:
            modus_assert(size == (sizeof(i32) * 2));
            glUniform2iv(loc, 1, reinterpret_cast<const i32*>(ptr));
            break;
        case DataType::Vec2U32:
            modus_assert(size == (sizeof(u32) * 2));
            glUniform2uiv(loc, 1, reinterpret_cast<const u32*>(ptr));
            break;
        case DataType::Vec3F32:
            modus_assert(size == sizeof(glm::vec3));
            glUniform3fv(loc, 1, reinterpret_cast<const f32*>(ptr));
            break;
        case DataType::Vec3I32:
            modus_assert(size == (sizeof(i32) * 3));
            glUniform3iv(loc, 1, reinterpret_cast<const i32*>(ptr));
            break;
        case DataType::Vec3U32:
            modus_assert(size == (sizeof(u32) * 3));
            glUniform3uiv(loc, 1, reinterpret_cast<const u32*>(ptr));
            break;
        case DataType::Vec4F32:
            modus_assert(size == sizeof(glm::vec4));
            glUniform4fv(loc, 1, reinterpret_cast<const f32*>(ptr));
            break;
        case DataType::Vec4I32:
            modus_assert(size == (sizeof(i32) * 4));
            glUniform4iv(loc, 1, reinterpret_cast<const i32*>(ptr));
            break;
        case DataType::Vec4U32:
            modus_assert(size == (sizeof(u32) * 4));
            glUniform4uiv(loc, 1, reinterpret_cast<const u32*>(ptr));
            break;

        case DataType::Mat2F32:
            modus_assert(size == sizeof(glm::mat2));
            glUniformMatrix2fv(loc, 1, GL_FALSE, reinterpret_cast<const f32*>(ptr));
            break;
        case DataType::Mat3F32:
            modus_assert(size == sizeof(glm::mat3));
            glUniformMatrix3fv(loc, 1, GL_FALSE, reinterpret_cast<const f32*>(ptr));
            break;
        case DataType::Mat4F32:
            modus_assert(size == sizeof(glm::mat4));
            glUniformMatrix4fv(loc, 1, GL_FALSE, reinterpret_cast<const f32*>(ptr));
            break;
        default:
            modus_assert_message(false, "Should not be reached, unknown or invalid data type");
            break;
    }
}

void DeviceGLES3::apply_effect(const NotMyPtr<const ProgramGLES3>& program,
                               const EffectGLES3& effect,
                               const Command& command) {
    MODUS_PROFILE_THREED_BLOCK("GLES3 Apply Effect");

    if (!m_active_program || m_active_program != program) {
        m_active_program = program;
        // bind program
        glUseProgram(m_active_program->program);
    }

    apply_depth_stencil_state(effect.params.state.depth_stencil);
    apply_raster_state(effect.params.state.raster);
    apply_blend_state(effect.params.state.blend);

    {
        MODUS_PROFILE_THREED_BLOCK("Set samplers / textures");
        // Bind textures and samplers
        for (u8 i = 0; i < kMaxProgramSamplerInputs; ++i) {
#if defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
            if (command.inputs.samplers[i].texture) {
#else
            if (m_active_program->loc_textures[i] != -1 &&
                command.inputs.samplers[i].textures.is_valid()) {
#endif
                // Set texture slot
                auto r_texture = m_texture_db.get(command.inputs.samplers[i].texture);
                modus_assert(r_texture);
                const TextureGLES3& gl_texture = *(r_texture.value());
#if !defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
                glUniform1i(m_active_program->loc_textures[i], i);
#endif
                m_state_cache.bind_texture(i, gl_texture.gl_id, gl_texture.gl_type);
                if (auto r = m_sampler_db.get(command.inputs.samplers[i].sampler); r) {
                    m_state_cache.bind_sampler(i, (*r)->gl_id);
                }
            }
        }
    }

    // Update uniforms
    {
        MODUS_PROFILE_THREED_BLOCK("Set Uniforms");
        for (u8 i = 0; i < kMaxProgramConstantInputs; ++i) {
#if defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
            if (command.inputs.constants[i]) {
#else
            if (m_active_program->loc_constants[i] != -1 && command.inputs.constants[i]) {
#endif
                const ProgramConstantInputParam& constant_input = m_active_program->constants[i];
#if defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
                const GLint loc = i;
#else
                const GLint loc = m_active_program->loc_constants[i];
#endif
                const ConstantHandle handle = command.inputs.constants[i];
                modus_assert(handle.is_valid());
                const ConstantHandlePrivate cp = u32_to_constant_handle_private(handle.value());
                modus_assert(cp.offset < m_frame_allocator.size());
                const u8* ptr = m_frame_allocator.data() + cp.offset;
                upload_uniform(loc, ptr, cp.data_size, constant_input.data_type);
            }
        }
    }
    {
        MODUS_PROFILE_THREED_BLOCK("Set CBuffers");
        for (u8 i = 0; i < kMaxProgramConstantInputs; ++i) {
            const auto& input = command.inputs.cbuffers[i];
#if defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
            if (input.handle) {
                const GLint loc = i;
#else
            if (m_active_program->loc_cbuffers[i] != -1 && command.inputs.cbuffers[i]) {
                const GLint loc = m_active_program->loc_cbuffers[i];
#endif
                modus_assert_message(
                    input.offset % m_device_limits.constant_buffer_offset_alignment == 0,
                    "Constant buffer offset does not meet alignment requirements");
                // Bind buffer
                auto r_buffer = m_constant_buffer_db.get(input.handle);
                if (!r_buffer) {
                    modus_assert_message(false, "Failed to locate/bind constant buffer");
                    continue;
                }
                // Map buffer to uniform slot
                m_state_cache.bind_buffer_base_constant((*r_buffer)->gl_id, loc, input.offset,
                                                        input.size);
                // glUniformBlockBinding(m_active_program->program, loc, loc);
            }
        }
    }
    {
        MODUS_PROFILE_THREED_BLOCK("Set SBuffers");
        for (u8 i = 0; i < kMaxProgramConstantInputs; ++i) {
            const auto& input = command.inputs.sbuffers[i];
            if (input.handle) {
                const GLint loc = i;
                modus_assert_message(
                    input.offset % m_device_limits.shader_buffer_offset_alignment == 0,
                    "Shader buffer offset does not meet alignment requirements");
                // Bind buffer
                auto r_buffer = m_shader_buffer_db.get(input.handle);
                if (!r_buffer) {
                    modus_assert_message(false, "Failed to locate/bind constant buffer");
                    continue;
                }
                // Map buffer to uniform slot
                m_state_cache.bind_buffer_base_storage((*r_buffer)->gl_id, loc, input.offset,
                                                       input.size);
                // glUniformBlockBinding(m_active_program->program, loc, loc);
            }
        }
    }
}
}    // namespace modus::threed::gles3
