/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/buffer_gles3.hpp>
#include <gles3/drawable_gles3.hpp>
#include <gles3/state_cache_gles3.hpp>

namespace modus::threed::gles3 {

struct GLES3VertAttribDataType {
    GLenum type;
    u8 size;
    bool is_float;
    u8 slots = 1;
};

// clang-format off
static const GLES3VertAttribDataType kVertexAttribInfo[] = {
    {GL_BYTE, 1, false},  // I8
    {GL_UNSIGNED_BYTE, 1, false}, // U8
    {GL_SHORT, 1, false}, // I16
    {GL_UNSIGNED_SHORT, 1, false}, // U16
    {GL_INT, 0, false}, // I32
    {GL_UNSIGNED_INT, 0, false}, // U32
    {GL_FLOAT, 1, true}, // F32

    {GL_BYTE, 2, false}, // Vec2I8
    {GL_UNSIGNED_BYTE, 2, false}, // Vec2U8
    {GL_SHORT, 2, false}, // Vec2I16
    {GL_UNSIGNED_SHORT, 2, false}, // Vec2U16
    {GL_INT, 0, false}, // Vec2I32
    {GL_UNSIGNED_INT, 0, false}, // Vec2U32
    {GL_FLOAT, 2, true}, // Vec2F32

    {GL_BYTE, 3, false }, // Vec3I8
    {GL_UNSIGNED_BYTE, 3, false }, // Vec3U8
    {GL_SHORT, 3, false}, // Vec3I16
    {GL_UNSIGNED_SHORT, 3, false}, // Vec3U16
    {GL_INT, 0, false}, // Vec3I32
    {GL_UNSIGNED_INT, 0, false}, // VecU32
    {GL_FLOAT, 3, true}, // Vec3F32

    {GL_BYTE, 4, false}, // Vec4I8
    {GL_UNSIGNED_BYTE, 4, false}, // Vec4U8
    {GL_SHORT, 4, false}, // Vec4I16
    {GL_UNSIGNED_SHORT, 4, false }, // Vec4U16
    {GL_INT, 4, false }, // Vec4I32
    {GL_UNSIGNED_INT, 4, false}, // Vec4U32
    {GL_FLOAT, 4, true}, // Vec4F32

    {GL_FLOAT, 2, true, 2}, // Mat2F32
    {GL_FLOAT, 3, true, 3}, // Mat3F32
    {GL_FLOAT, 4, true, 4}, // Mat4F32
    {GL_UNSIGNED_INT_2_10_10_10_REV, 4, true}, // U32_2_x10_REV
    {GL_INT_2_10_10_10_REV, 4, true}
};
// clang-format on

static_assert(modus::array_size(kVertexAttribInfo) == usize(DataType::Total));

static u32 enable_vertex_attrib(const DataType data_type,
                                const u32 slot,
                                const u32 offset,
                                const bool normalized,
                                const u32 buffer_slot) {
    // Fill Vertex Attrib Pointer
    const GLES3VertAttribDataType attrib_info = kVertexAttribInfo[static_cast<u8>(data_type)];
    modus_assert(attrib_info.type != GL_INVALID_ENUM);

    for (u8 i = 0; i < attrib_info.slots; ++i) {
        glEnableVertexAttribArray(slot + i);
        if (attrib_info.is_float || normalized) {
            glVertexAttribFormat(slot + i, attrib_info.size, attrib_info.type,
                                 normalized ? GL_TRUE : GL_FALSE,
                                 offset + ((sizeof(f32) * attrib_info.slots) * i));
        } else {
            glVertexAttribIFormat(slot + i, attrib_info.size, attrib_info.type,
                                  offset + ((sizeof(f32) * attrib_info.slots) * i));
        }
        glVertexAttribBinding(slot + i, buffer_slot);
    }

    return attrib_info.slots;
}

static void disable_vertex_attrib(const u32 slot) {
    glDisableVertexAttribArray(slot);
}

static Result<void, String> create_drawable(DrawableGLES3& drawable,
                                            const DrawableCreateParams& params,
                                            const BufferDBGLES3& buffer_db,
                                            StateCacheGLES3& state_cache) {
    NotMyPtr<const BufferGLES3> gl_buffers[kMaxDrawableBufferInputs];
    NotMyPtr<const BufferGLES3> gl_buffer_index;
    // validate input
    for (u8 i = 0; i < kMaxDrawableBufferInputs; ++i) {
        if (params.data_buffers[i].buffer) {
            auto r_buffer = buffer_db.get(params.data_buffers[i].buffer);
            if (!r_buffer) {
                auto string = modus::format("Failed to locate data buffer {}", i);
                return Error<String>(std::move(string));
            }

            if (r_buffer.value()->gl_type != GL_ARRAY_BUFFER) {
                auto string = modus::format("Data buffer {} is not data buffer.", i);
                return Error<String>(std::move(string));
            }
            gl_buffers[i] = r_buffer.value();
        }
    }

    // validate indices
    if (params.index_type != IndicesType::None) {
        auto r_buffer = buffer_db.get(params.index_buffer.buffer);
        if (!r_buffer) {
            auto string = "Failed to locate index buffer";
            return Error<String>(string);
        }

        if (r_buffer.value()->gl_type != GL_ELEMENT_ARRAY_BUFFER) {
            auto string = "Buffer for indices is not an index buffer.";
            return Error<String>(string);
        }
        gl_buffer_index = r_buffer.value();
    }

    GLuint gl_vao = 0;
    glGenVertexArrays(1, &gl_vao);
    drawable.vao = GLVAOId(gl_vao);
    drawable.params = params;

    state_cache.bind_vao(drawable.vao);

    for (u8 i = 0; i < kMaxDrawableInputCount;) {
        // Fill Vertex Attrib Pointer
        const auto& data = params.data[i];
        // For vao we need to bind the buf
        if (data.data_type != DataType::Total) {
            const u32 slot_count = enable_vertex_attrib(data.data_type, i, data.offset,
                                                        data.normalized, data.buffer_index);
            i += slot_count;
        } else {
            disable_vertex_attrib(i);
            ++i;
        }
    }

    for (u8 i = 0; i < kMaxDrawableBufferInputs; ++i) {
        const DrawableDataBufferDesc& desc = params.data_buffers[i];
        if (desc.buffer) {
            glBindVertexBuffer(i, gl_buffers[i]->gl_id, desc.offset, desc.stride);
            glVertexBindingDivisor(i, desc.instance_divisor);
        } else {
            glBindVertexBuffer(i, 0, 0, 0);
            glVertexBindingDivisor(i, 0);
        }
    }

    if (gl_buffer_index != nullptr) {
        // TODO: For some reason index buffer doesn't work properly until it is
        // forcibly set. Maybe something to do with VAO binding?
        state_cache.force_bind_buffer_element_array(gl_buffer_index->gl_id);
    } else {
        state_cache.bind_buffer(BufferType::Indices, GLBufferId(0), GL_ARRAY_BUFFER);
    }

    state_cache.bind_vao(GLVAOId(0));
    return Ok<>();
}

static void destroy_drawable(const DrawableGLES3& drawable) {
    if (drawable.vao.is_valid()) {
        glDeleteVertexArrays(1, &drawable.vao.value());
    }
}

Result<> DrawableDBGLES3::initialize() {
    DrawableGLES3 drawable;
    drawable.params.count = 0;
    drawable.params.start = 0;
    GLuint id;
    glGenVertexArrays(1, &id);
    drawable.vao = m_default_vao = GLVAOId(id);
    if (!m_pool.create(drawable)) {
        MODUS_LOGD("DrawableDBGLES3: Failed to register default vao");
    }
    return Ok<>();
}

Result<DrawableHandle, String> DrawableDBGLES3::create(const DrawableCreateParams& params,
                                                       const BufferDBGLES3& buffer_db,
                                                       StateCacheGLES3& state_cache) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error(String("Max pool capacity reached"));
    }

    DrawableGLES3 drawable;
    auto drawable_result = create_drawable(drawable, params, buffer_db, state_cache);
    if (!drawable_result) {
        return Error(drawable_result.release_error());
    }

    auto add_result = m_pool.create(drawable);
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        return Error(String("Unknown error"));
    }

    return Ok<DrawableHandle>(add_result.value().first);
}

Result<void, String> DrawableDBGLES3::update(const DrawableHandle handle,
                                             const DrawableUpdateParams& params,
                                             const BufferDBGLES3& buffer_db,
                                             StateCacheGLES3& state_cache) {
    auto r_drawable = m_pool.get(handle);
    if (!r_drawable) {
        return Error(String("Failed to locate drawable"));
    }
    NotMyPtr<DrawableGLES3> drawable = *r_drawable;

    // Locate buffers
    GLBufferId gl_buffers[kMaxDrawableBufferInputs];
    GLBufferId gl_index_buffer;
    for (u8 i = 0; i < kMaxDrawableBufferInputs; ++i) {
        if (params.data_buffers[i].buffer) {
            auto r_buffer = buffer_db.get(params.data_buffers[i].buffer);
            if (!r_buffer) {
                return Error(modus::format("Failed to locate buffer {}", i));
            }
            if ((*r_buffer)->params.type != BufferType::Data) {
                return Error(modus::format("Data buffer handle {} is not a data buffer", i));
            }
            gl_buffers[i] = (*r_buffer)->gl_id;
        }
    }

    if (params.index_buffer.buffer) {
        auto r_buffer = buffer_db.get(params.index_buffer.buffer);
        if (!r_buffer) {
            return Error(String("Failed to locate index buffer"));
        }
        if ((*r_buffer)->params.type != BufferType::Indices) {
            return Error(String("Index buffer handle is not an index buffer"));
        }
        gl_index_buffer = (*r_buffer)->gl_id;
    }

    state_cache.bind_vao(drawable->vao);
    for (u8 i = 0; i < kMaxDrawableBufferInputs; ++i) {
        const DrawableDataBufferDesc& desc = params.data_buffers[i];
        DrawableDataBufferDesc& draw_desc = drawable->params.data_buffers[i];
        if (desc.buffer != draw_desc.buffer ||
            desc.instance_divisor != draw_desc.instance_divisor ||
            desc.offset != draw_desc.offset || desc.stride != draw_desc.stride) {
            draw_desc = desc;
            if (desc.buffer) {
                glBindVertexBuffer(i, gl_buffers[i], desc.offset, desc.stride);
                glVertexBindingDivisor(i, desc.instance_divisor);
            } else {
                glBindVertexBuffer(i, 0, 0, 0);
                glVertexBindingDivisor(i, 0);
            }
        }
    }

    if (drawable->params.index_buffer.buffer != params.index_buffer.buffer) {
        drawable->params.index_buffer = params.index_buffer;
        if (gl_index_buffer) {
            // TODO: For some reason index buffer doesn't work properly until it is
            // forcibly set. Maybe something to do with VAO binding?
            state_cache.force_bind_buffer_element_array(gl_index_buffer);
        } else {
            state_cache.force_bind_buffer_element_array(GLBufferId(0));
        }
    }
    return Ok<>();
}

void DrawableDBGLES3::destroy_resource(const DrawableGLES3& resource) {
    destroy_drawable(resource);
}

}    // namespace modus::threed::gles3
