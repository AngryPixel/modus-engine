/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <gles3/sampler_gles3.hpp>
// clang-format on

namespace modus::threed::gles3 {

static const GLenum kGLSamplerFilter[u8(SamplerFilter::Total)] = {GL_NEAREST,
                                                                  GL_LINEAR,
                                                                  GL_NEAREST_MIPMAP_NEAREST,
                                                                  GL_NEAREST_MIPMAP_LINEAR,
                                                                  GL_LINEAR_MIPMAP_LINEAR,
                                                                  GL_LINEAR_MIPMAP_NEAREST};

static const GLenum kGLSamplerWrapMode[u8(SamplerWrapMode::Total)] = {
    GL_CLAMP_TO_EDGE,
    GL_MIRRORED_REPEAT,
    GL_REPEAT,
};

static Result<void, String> create_sampler(SamplerGLES3& sampler,
                                           const SamplerCreateParams& params) {
    // Check samplers
    if (params.wrap_r >= SamplerWrapMode::Total) {
        return Error(String("Sampler has an invalid wrap_r value"));
    }
    if (params.wrap_s >= SamplerWrapMode::Total) {
        return Error(String("Sampler has an invalid wrap_s value"));
    }
    if (params.wrap_t >= SamplerWrapMode::Total) {
        return Error(String("Sampler has an invalid wrap_t value"));
    }
    if (params.filter_min >= SamplerFilter::Total) {
        return Error(String("Sampler has an invalid filter_min value"));
    }
    if (params.filter_mag != SamplerFilter::Linear && params.filter_mag != SamplerFilter::Nearest) {
        return Error(String("Sampler has an invalid filter_mag value"));
    }

    GLuint gl_sampler;
    glGenSamplers(1, &gl_sampler);
    const GLenum gl_wrap_r = kGLSamplerWrapMode[u8(params.wrap_r)];
    const GLenum gl_wrap_s = kGLSamplerWrapMode[u8(params.wrap_s)];
    const GLenum gl_wrap_t = kGLSamplerWrapMode[u8(params.wrap_t)];
    const GLenum gl_filter_min = kGLSamplerFilter[u8(params.filter_min)];
    const GLenum gl_filter_mag = kGLSamplerFilter[u8(params.filter_mag)];
    glSamplerParameteri(gl_sampler, GL_TEXTURE_WRAP_R, gl_wrap_r);
    glSamplerParameteri(gl_sampler, GL_TEXTURE_WRAP_S, gl_wrap_s);
    glSamplerParameteri(gl_sampler, GL_TEXTURE_WRAP_T, gl_wrap_t);
    glSamplerParameteri(gl_sampler, GL_TEXTURE_MIN_FILTER, gl_filter_min);
    glSamplerParameteri(gl_sampler, GL_TEXTURE_MAG_FILTER, gl_filter_mag);
    sampler.gl_id = GLSamplerId(gl_sampler);
    return Ok<>();
}

static void destroy_sampler(const SamplerGLES3& sampler) {
    if (sampler.gl_id.is_valid()) {
        glDeleteSamplers(1, &sampler.gl_id.value());
    }
}

Result<SamplerHandle, String> SamplerDBGLES3::create(const SamplerCreateParams& params) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error(String("Max pool capacity reached"));
    }

    SamplerGLES3 sampler;
    auto sampler_result = create_sampler(sampler, params);
    if (!sampler_result) {
        return Error(sampler_result.release_error());
    }

    auto add_result = m_pool.create(sampler);
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        return Error(String("Unknown error"));
    }

    return Ok<SamplerHandle>(add_result.value().first);
}

void SamplerDBGLES3::destroy_resource(const SamplerGLES3& resource) {
    destroy_sampler(resource);
}

}    // namespace modus::threed::gles3
