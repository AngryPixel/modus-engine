/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/api_gles3.hpp>
#include <gles3/state_cache_gles3.hpp>

//#define DISABLE_CACHE

namespace modus::threed::gles3 {

StateCacheGLES3::StateCacheGLES3() {
    MODUS_UNUSED(m_stencil);
}

void StateCacheGLES3::reset_buffer_cache() {
    m_buffers.data = GLBufferId();
    m_buffers.index = GLBufferId();
    m_buffers.uniform = GLBufferId();
    m_buffers.storage = GLBufferId();

    for (auto& b : m_program_inputs.cbuffers) {
        b = BufferProgramInputBinding();
    }
    for (auto& b : m_program_inputs.sbuffers) {
        b = BufferProgramInputBinding();
    }
}

void StateCacheGLES3::bind_buffer(const BufferType type,
                                  const GLBufferId id,
                                  [[maybe_unused]] const GLenum gl_type) {
    modus_assert(type < BufferType::Total);
#if defined(DISABLE_CACHE)
    glBindBuffer(gl_type, id);
#else
    switch (type) {
        case threed::BufferType::Data:
            bind_buffer_data(id);
            break;
        case threed::BufferType::Indices:
            bind_buffer_element_array(id);
            break;
        default:
            modus_assert_message(false, "Unhandled buffer type");
            break;
    }
#endif
}

void StateCacheGLES3::bind_texture(const u32 index, const GLTextureId id, const GLenum gl_type) {
#if defined(DISABLE_CACHE)
    glActiveTexture(GL_TEXTURE0 + index);
    glBindTexture(gl_type, id);
#else
    if (m_textures.active_index != index) {
        m_textures.active_index = index;
        glActiveTexture(GL_TEXTURE0 + index);
    }
    if (m_textures.textures[index] != id) {
        m_textures.textures[index] = id;
        glBindTexture(gl_type, id);
    }
#endif
}

void StateCacheGLES3::bind_buffer_base_constant(GLBufferId id,
                                                const u32 loc,
                                                const u32 offset,
                                                const u32 size) {
#if !defined(DISABLE_CACHE)
    const BufferProgramInputBinding binding = {id, offset, size};
    if (m_program_inputs.cbuffers[loc] != binding) {
        m_program_inputs.cbuffers[loc] = binding;
#endif
        if (offset == 0 && size == 0) {
            glBindBufferBase(GL_UNIFORM_BUFFER, loc, id);
        } else {
            glBindBufferRange(GL_UNIFORM_BUFFER, loc, id, offset, size);
        }
#if !defined(DISABLE_CACHE)
    }
#endif
}

void StateCacheGLES3::bind_buffer_base_storage(GLBufferId id,
                                               const u32 loc,
                                               const u32 offset,
                                               const u32 size) {
#if !defined(DISABLE_CACHE)
    const BufferProgramInputBinding binding = {id, offset, size};
    if (m_program_inputs.sbuffers[loc] != binding) {
        m_program_inputs.sbuffers[loc] = binding;
#endif
        if (offset == 0 && size == 0) {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, loc, id);
        } else {
            glBindBufferRange(GL_SHADER_STORAGE_BUFFER, loc, id, offset, size);
        }
#if !defined(DISABLE_CACHE)
    }
#endif
}

void StateCacheGLES3::bind_sampler(const u32 index, const GLSamplerId id) {
#if defined(DISABLE_CACHE)
    glBindSampler(index, id);
#else
    if (m_textures.samplers[index] != id) {
        m_textures.samplers[index] = id;
        glBindSampler(index, id);
    }
#endif
}

void StateCacheGLES3::bind_framebuffer_read(GLFramebufferId id) {
#if defined(DISABLE_CACHE)
    glBindFramebuffer(GL_READ_FRAMEBUFFER, id.value());
#else
    if (m_frambuffer_read != id) {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, id.value());
        m_frambuffer_read = id;
    }
#endif
}

void StateCacheGLES3::bind_framebuffer_draw(GLFramebufferId id) {
#if defined(DISABLE_CACHE)
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id.value());
#else
    if (m_frambuffer_draw != id) {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, id.value());
        m_frambuffer_draw = id;
    }
#endif
}

void StateCacheGLES3::set_view_port(const u32 x, const u32 y, const u32 w, const u32 h) {
#if defined(DISABLE_CACHE)
    glViewport(x, y, w, h);
#else
    if (m_viewport.x != x || m_viewport.y != y || m_viewport.w != w || m_viewport.h != h) {
        m_viewport.x = x;
        m_viewport.y = y;
        m_viewport.w = w;
        m_viewport.h = h;
        glViewport(x, y, w, h);
    }
#endif
}

void StateCacheGLES3::set_scissor(const u32 x, const u32 y, const u32 w, const u32 h) {
#if defined(DISABLE_CACHE)
    glScissor(x, y, w, h);
#else
    if (m_scissor.x != x || m_scissor.y != y || m_scissor.w != w || m_scissor.h != h) {
        m_scissor.x = x;
        m_scissor.y = y;
        m_scissor.w = w;
        m_scissor.h = h;
        glScissor(x, y, w, h);
    }
#endif
}

static inline void do_enable(const GLenum gl_enum, const bool value) {
    if (value) {
        glEnable(gl_enum);
    } else {
        glDisable(gl_enum);
    }
}

void StateCacheGLES3::set_scissor_test_enabled(const bool v) {
#if defined(DISABLE_CACHE)
    do_enable(GL_SCISSOR_TEST, v);
#else
    if (m_enabled.test(u32(GLEnable::ScissorTest)) != v) {
        m_enabled.set(u32(GLEnable::ScissorTest), v);
        do_enable(GL_SCISSOR_TEST, v);
    }
#endif
}

void StateCacheGLES3::bind_buffer_element_array(const GLBufferId id) {
#if defined(DISABLE_CACHE)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
#else
    if (m_buffers.index != id) {
        m_buffers.index = id;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
    }
#endif
}

void StateCacheGLES3::force_bind_buffer_element_array(const GLBufferId id) {
#if defined(DISABLE_CACHE)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
#else
    m_buffers.index = id;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
#endif
}

void StateCacheGLES3::bind_buffer_data(const GLBufferId id) {
#if defined(DISABLE_CACHE)
    glBindBuffer(GL_ARRAY_BUFFER, id);
#else
    if (m_buffers.data != id) {
        m_buffers.data = id;
        glBindBuffer(GL_ARRAY_BUFFER, id);
    }
#endif
}

void StateCacheGLES3::bind_buffer_uniform(const GLBufferId id) {
#if defined(DISABLE_CACHE)
    glBindBuffer(GL_UNIFORM_BUFFER, id);
#else
    if (m_buffers.uniform != id) {
        m_buffers.uniform = id;
        glBindBuffer(GL_UNIFORM_BUFFER, id);
    }
#endif
}

void StateCacheGLES3::bind_buffer_storage(const GLBufferId id) {
#if defined(DISABLE_CACHE)
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
#else
    if (m_buffers.storage != id) {
        m_buffers.storage = id;
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
    }
#endif
}

void StateCacheGLES3::set_cullface_enabled(const bool enabled) {
#if defined(DISABLE_CACHE)
    do_enable(GL_CULL_FACE, enabled);
#else
    if (m_enabled.test(u32(GLEnable::CullFace)) != enabled) {
        m_enabled.set(u32(GLEnable::CullFace), enabled);
        do_enable(GL_CULL_FACE, enabled);
    }
#endif
}

void StateCacheGLES3::set_cullface(const GLenum cull_face) {
#if defined(DISABLE_CACHE)
    glCullFace(cull_face);
#else
    if (m_cull.face != cull_face) {
        glCullFace(cull_face);
        m_cull.face = cull_face;
    }
#endif
}

void StateCacheGLES3::set_front_face(const GLenum front_face) {
#if defined(DISABLE_CACHE)
    glFrontFace(front_face);
#else
    if (m_cull.front_face != front_face) {
        m_cull.front_face = front_face;
        glFrontFace(front_face);
    }
#endif
}

void StateCacheGLES3::set_depth_test_enabled(const bool enabled) {
#if defined(DISABLE_CACHE)
    do_enable(GL_DEPTH_TEST, enabled);
#else
    if (m_enabled.test(u32(GLEnable::DepthTest)) != enabled) {
        m_enabled.set(u32(GLEnable::DepthTest), enabled);
        do_enable(GL_DEPTH_TEST, enabled);
    }
#endif
}

void StateCacheGLES3::set_depth_mask_enabled(const bool enabled) {
#if defined(DISABLE_CACHE)
    glDepthMask(enabled ? GL_TRUE : GL_FALSE);
#else
    if (m_enabled.test(u32(GLEnable::DepthMask)) != enabled) {
        m_enabled.set(u32(GLEnable::DepthMask), enabled);
        glDepthMask(enabled ? GL_TRUE : GL_FALSE);
    }
#endif
}

void StateCacheGLES3::set_depth_func(const GLenum func) {
#if defined(DISABLE_CACHE)
    glDepthFunc(func);
#else
    if (m_depth.func != func) {
        m_depth.func = func;
        glDepthFunc(func);
    }
#endif
}

void StateCacheGLES3::set_stencil_test_enabled(const bool enabled) {
#if defined(DISABLE_CACHE)
    do_enable(GL_STENCIL_TEST, enabled);
#else
    if (m_enabled.test(u32(GLEnable::StencilTest)) != enabled) {
        m_enabled.set(u32(GLEnable::StencilTest), enabled);
        do_enable(GL_STENCIL_TEST, enabled);
    }
#endif
}

void StateCacheGLES3::bind_vao(const GLVAOId id) {
#if defined(DISABLE_CACHE)
    glBindVertexArray(id);
#else
    if (m_vao != id) {
        m_vao = id;
        glBindVertexArray(id);
    }
#endif
}

void StateCacheGLES3::set_blend_enabled(const bool enabled) {
#if defined(DISABLE_CACHE)
    do_enable(GL_BLEND, enabled);
#else
    if (m_enabled.test(u32(GLEnable::Blend)) != enabled) {
        m_enabled.set(u32(GLEnable::Blend), enabled);
        do_enable(GL_BLEND, enabled);
    }
#endif
}

void StateCacheGLES3::set_blend_params(const bool separate,
                                       const GLenum equation_source,
                                       const GLenum equation_destination,
                                       const GLenum equation_source_aplha,
                                       const GLenum equation_destination_alpha,
                                       const GLenum operation_colour,
                                       const GLenum operation_alpha) {
    bool force_update = false;
#if defined(DISABLE_CACHE)
    force_update = true;
#endif
    if (m_enabled.test(u32(GLEnable::BlendIndependent)) != separate) {
        m_enabled.set(u32(GLEnable::BlendIndependent), separate);
        force_update = true;
    }

    if (separate) {
        if (force_update || m_blend.equation_source != equation_source ||
            m_blend.equation_destination != equation_destination ||
            m_blend.equation_source_alpha != equation_source_aplha ||
            m_blend.equation_destination_alpha != equation_destination_alpha) {
            m_blend.equation_source = equation_source;
            m_blend.equation_destination = equation_destination;
            m_blend.equation_source_alpha = equation_source_aplha;
            m_blend.equation_destination_alpha = equation_destination_alpha;
            glBlendFuncSeparate(equation_source, equation_destination, equation_source_aplha,
                                equation_destination_alpha);
        }

        if (force_update || m_blend.operation_colour != operation_colour ||
            m_blend.operation_alpha != operation_alpha) {
            m_blend.operation_colour = operation_colour;
            m_blend.operation_alpha = operation_alpha;
            glBlendEquationSeparate(operation_colour, operation_alpha);
        }
    } else {
        if (force_update || m_blend.equation_source != equation_source ||
            m_blend.equation_destination != equation_destination) {
            m_blend.equation_source = equation_source;
            m_blend.equation_destination = equation_destination;
            glBlendFunc(equation_source, equation_destination);
        }

        if (force_update || m_blend.operation_colour != operation_colour) {
            m_blend.operation_colour = operation_colour;
            glBlendEquation(operation_colour);
        }
    }
}

}    // namespace modus::threed::gles3
