/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/program_gles3.hpp>
#include <threed/program.hpp>

namespace modus::threed::gles3 {

#if !defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
static bool valid_uniform_type(const DataType type) {
    switch (type) {
        case DataType::I8:
        case DataType::U8:
        case DataType::I16:
        case DataType::U16:
        case DataType::Vec2I8:
        case DataType::Vec3I8:
        case DataType::Vec4I8:
        case DataType::Vec2U8:
        case DataType::Vec3U8:
        case DataType::Vec4U8:
        case DataType::Vec2I16:
        case DataType::Vec3I16:
        case DataType::Vec4I16:
        case DataType::Vec2U16:
        case DataType::Vec3U16:
        case DataType::Vec4U16:
            return false;
        default:
            return true;
    }
}
#endif
static Result<GLShaderId, String> compile_shader(const GLenum shader_type, const ByteSlice& data) {
    if (data.size() >= usize(std::numeric_limits<int>::max())) {
        return Error(String("Shader source too long"));
    }

    StringSlice shader_src(reinterpret_cast<const char*>(data.data()), data.size());
    GLShaderId gl_id(glCreateShader(shader_type));
    int shader_src_len = int(shader_src.size());

    const char* shader_src_chr = shader_src.data();
    glShaderSource(gl_id, 1, &shader_src_chr, &shader_src_len);
    glCompileShader(gl_id);

    GLint compile_status = 0;
    glGetShaderiv(gl_id, GL_COMPILE_STATUS, &compile_status);
    if (compile_status != GL_TRUE) {
        GLsizei length = 0, bytes_written = 0;
        glGetShaderiv(gl_id, GL_INFO_LOG_LENGTH, &length);
        String error("Compile Error ");
        if (shader_type == GL_VERTEX_SHADER) {
            error += "Vertex Shader: ";
        } else if (shader_type == GL_FRAGMENT_SHADER) {
            error += "Fragment Shader: ";
        } else {
            modus_assert_message(false, "Uknown shader type");
            error += "(?) Shader: ";
        }

        if (length != 0) {
            const usize error_len = error.size();
            error.resize(error_len + length, '\0');
            glGetShaderInfoLog(gl_id, length, &bytes_written, error.data() + error_len);
            glDeleteShader(gl_id);
        } else {
            error = "Failed to compile shader, no log info available.";
        }
#if !defined(MODUS_RTM)
        error += modus::format("\n{}", StringSlice(data));
#endif
        return Error(std::move(error));
    }
    return Ok(gl_id);
}

static Result<GLProgramId, String> create_program(const u32 id_vert, const u32 id_frag) {
    GLProgramId gl_id(glCreateProgram());
    glAttachShader(gl_id, id_vert);
    glAttachShader(gl_id, id_frag);
    glLinkProgram(gl_id);
    GLint link_result = 0;
    glGetProgramiv(gl_id, GL_LINK_STATUS, &link_result);
    if (link_result != GL_TRUE) {
        GLsizei length, bytes_written;
        glGetProgramiv(gl_id, GL_INFO_LOG_LENGTH, &length);
        String error;
        if (length > 0) {
            error.resize(length, '\0');
            glGetProgramInfoLog(gl_id, length, &bytes_written, error.data());
            glDeleteProgram(gl_id);
        } else {
            error = "Failed to link program, no log info available.";
        }
        return Error(std::move(error));
    }
    return Ok(gl_id);
}

static Result<GLProgramId, String> create_program(ProgramGLES3& prog,
                                                  const ProgramCreateParams& params) {
    auto result_vert = compile_shader(GL_VERTEX_SHADER, params.vertex);
    if (!result_vert) {
        return Error(result_vert.release_error());
    }

    auto result_frag = compile_shader(GL_FRAGMENT_SHADER, params.fragment);
    if (!result_frag) {
        glDeleteShader(result_vert.value());
        return Error(result_frag.release_error());
    }

    auto result_program = create_program(result_vert.value(), result_frag.value());
    if (!result_program) {
        glDeleteShader(result_vert.value());
        glDeleteShader(result_frag.value());
#if !defined(MODUS_RTM)
        String error =
            modus::format("{}\nVertexShader:\n{}\nFragmentShader:\n{}\n", result_program.error(),
                          StringSlice(params.vertex), StringSlice(params.fragment));
        return Error(std::move(error));
#else
        return Error(result_program.release_error());
#endif
    }

    prog.program = result_program.value();
    prog.shaders.vert = result_vert.value();
    prog.shaders.frag = result_frag.value();
    prog.constants = params.constants;
    prog.textures = params.samplers;
#if !defined(MODUS_THREED_GLPROGRAM_SKIP_INPUT_LOOKUP)
    // Default initialize
    for (auto& i : prog.loc_textures) {
        i = -1;
    }
    for (auto& i : prog.loc_constants) {
        i = -1;
    }

    // locate all texture inputs
    for (u8 i = 0; i < params.samplers.size(); ++i) {
        // locate all texture inputs
        modus_assert(i < kMaxProgramSamplerInputs);
        const ProgramSamplerInputParam& param = params.samplers[i];
        const GLint loc = glGetUniformLocation(*result_program, param.name.c_str());
        if (loc == -1) {
            glDeleteProgram(*result_program);
            return Error(modus::format("Could not locate texture input '{}' in program",
                                       param.name.c_str()));
        }
        prog.loc_textures[i] = loc;
    }

    // locate all contsant inputs
    for (u8 i = 0; i < params.constants.size(); ++i) {
        modus_assert(i < kMaxProgramConstantInputs);
        const ProgramConstantInputParam& param = params.constants[i];
        GLint loc = -1;
        if (!valid_uniform_type(param.data_type)) {
            glDeleteProgram(*result_program);
            return Error(
                modus::format("Constant input '{}' has an invalid data type", param.name.c_str()));
        }
        loc = glGetUniformLocation(*result_program, param.name.c_str());
        // If we can't locate it, it has been optimized out by the shader compiler
        /*if (loc == -1) {
            return Error(modus::format("Could not locate constant input '{}' in program",
                                       param.name.c_str()));
        }*/
        prog.loc_constants[i] = loc;
    }

    // locate all constant buffers
    for (u8 i = 0; i < params.cbuffers.size(); ++i) {
        modus_assert(i < kMaxProgramConstantInputs);
        const ProgramCBufferInputParam& param = params.cbuffers[i];
        GLint loc = -1;
        loc = glGetUniformBlockIndex(*result_program, param.name.c_str());
        glUniformBlockBinding(*result_program, loc, loc);
        // If we can't locate it, it has been optimized out by the shader compiler
        /*if (loc == -1) {
            return Error(modus::format("Could not locate constant input '{}' in program",
                                       param.name.c_str()));
        }*/
        prog.loc_cbuffers[i] = loc;
    }
#endif
    return Ok(result_program.value());
}

Result<ProgramHandle, String> ProgramDBGLES3::create(const ProgramCreateParams& params) {
    auto handle_result = m_pool.create(ProgramGLES3());
    if (!handle_result) {
        return Error(
            String("ProgramDBGLES3: Failed to acquire more handles, please "
                   "increas program handle limit."));
    }

    NotMyPtr<ProgramGLES3> program = (handle_result)->second;
    auto create_result = create_program(*program, params);
    if (!create_result) {
        auto result = m_pool.erase(handle_result.value().first);
        MODUS_UNUSED(result);
        modus_assert_message(result, "ProgramDBGLES3: Failed to erase program handle");
        return Error(create_result.release_error());
    }
    return Ok(ProgramHandle(handle_result.value().first.value()));
}

void ProgramDBGLES3::destroy_resource(const ProgramGLES3& resource) {
    glDeleteProgram(resource.program);
    glDeleteShader(resource.shaders.vert);
    glDeleteShader(resource.shaders.frag);
}

Result<> ProgramDBGLES3::reflect(const ProgramHandle handle, ProgramReflection& reflection) const {
    auto r_prog = m_pool.get(handle);
    if (!r_prog) {
        return Error<>();
    }

    static constexpr GLsizei kNameBufferLen = 256;
    const GLuint gl_id = (*r_prog)->program.value();
    GLint n_active_uniforms = 0;
    GLint n_active_blocks = 0;
    glGetProgramiv(gl_id, GL_ACTIVE_UNIFORMS, &n_active_uniforms);
    glGetProgramiv(gl_id, GL_ACTIVE_UNIFORM_BLOCKS, &n_active_blocks);

    for (GLint i = 0; i < n_active_blocks; ++i) {
        GLchar name[kNameBufferLen];
        GLsizei actual_name_size = 0;
        glGetActiveUniformBlockName(gl_id, i, kNameBufferLen, &actual_name_size, name);
        ProgramReflection::Buffer b;
        b.name = StringSlice(name, actual_name_size);
        if (!reflection.buffers.push_back(b)) {
            modus_assert_message(false, "Too many buffers");
            return Error<>();
        }
    }

    modus_assert(n_active_uniforms >= 0);
    for (GLuint i = 0; i < GLuint(n_active_uniforms); ++i) {
        GLchar name[kNameBufferLen];
        GLsizei actual_name_size = 0;
        GLint uniform_size = 0;
        GLenum uniform_type;
        glGetActiveUniform(gl_id, i, kNameBufferLen, &actual_name_size, &uniform_size,
                           &uniform_type, name);

        GLint block_index = 0;
        glGetActiveUniformsiv(gl_id, 1, &i, GL_UNIFORM_BLOCK_INDEX, &block_index);
        if (block_index != -1) {
            // skip, uniform belongs to a block
            continue;
        }
        DataType data_type;
        TextureType tex_type;
        bool is_sampler = false;
        switch (uniform_type) {
            case GL_FLOAT:
                data_type = DataType::F32;
                break;
            case GL_FLOAT_VEC2:
                data_type = DataType::Vec2F32;
                break;
            case GL_FLOAT_VEC3:
                data_type = DataType::Vec3F32;
                break;
            case GL_FLOAT_VEC4:
                data_type = DataType::Vec4F32;
                break;
            case GL_INT:
                data_type = DataType::I32;
                break;
            case GL_INT_VEC2:
                data_type = DataType::Vec2I32;
                break;
            case GL_INT_VEC3:
                data_type = DataType::Vec3I32;
                break;
            case GL_INT_VEC4:
                data_type = DataType::Vec4I32;
                break;
            case GL_UNSIGNED_INT:
                data_type = DataType::U32;
                break;
            case GL_UNSIGNED_INT_VEC2:
                data_type = DataType::Vec2U32;
                break;
            case GL_UNSIGNED_INT_VEC3:
                data_type = DataType::Vec3U32;
                break;
            case GL_UNSIGNED_INT_VEC4:
                data_type = DataType::Vec4U32;
                break;
            case GL_FLOAT_MAT2:
                data_type = DataType::Mat2F32;
                break;
            case GL_FLOAT_MAT3:
                data_type = DataType::Mat3F32;
                break;
            case GL_FLOAT_MAT4:
                data_type = DataType::Mat4F32;
                break;
            case GL_SAMPLER_2D:
            case GL_SAMPLER_2D_SHADOW:
            case GL_INT_SAMPLER_2D:
            case GL_UNSIGNED_INT_SAMPLER_2D:
                tex_type = TextureType::T2D;
                is_sampler = true;
                break;
            case GL_SAMPLER_3D:
            case GL_INT_SAMPLER_3D:
            case GL_UNSIGNED_INT_SAMPLER_3D:
                tex_type = TextureType::T3D;
                is_sampler = true;
                break;
            case GL_SAMPLER_CUBE:
            case GL_SAMPLER_CUBE_SHADOW:
            case GL_INT_SAMPLER_CUBE:
            case GL_UNSIGNED_INT_SAMPLER_CUBE:
                tex_type = TextureType::TCubeMap;
                is_sampler = true;
                break;
            case GL_SAMPLER_2D_ARRAY_SHADOW:
            case GL_INT_SAMPLER_2D_ARRAY:
            case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
            case GL_SAMPLER_2D_ARRAY:
                tex_type = TextureType::T2DArray;
                is_sampler = true;
                break;

            default:
                modus_assert_message(false, "Unhandled uniform type\n");
                return Error<>();
        }

        if (!is_sampler) {
            ProgramReflection::Constant c;
            c.name = StringSlice(name, actual_name_size);
            c.location_hint = i;
            c.data_type = data_type;
            if (!reflection.constants.push_back(c)) {
                modus_assert_message(false, "Too many uniforms");
                return Error<>();
            }
        } else {
            ProgramReflection::Sampler s;
            s.name = StringSlice(name, actual_name_size);
            s.location_hint = i;
            s.type = tex_type;
            if (!reflection.samplers.push_back(s)) {
                modus_assert_message(false, "Too many samplers");
                return Error<>();
            }
        }
    }

    return Ok<>();
}

}    // namespace modus::threed::gles3
