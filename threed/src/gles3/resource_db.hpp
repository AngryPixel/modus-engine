/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/fixed_handle_pool.hpp>
#include <threed/threed.hpp>

namespace modus::threed::gles3 {

template <typename Derived, typename ResourceType, u32 MaxResourceCount, typename HandleType>
class ResourceDB {
   protected:
    using PoolType = FixedHandlePool<ResourceType, MaxResourceCount, HandleType>;
    PoolType m_pool;
    threed::Vector<HandleType> m_queued_destroy;

    static constexpr usize kDestroyQueueInitialSize = 16;

   public:
    using this_type = ResourceDB<Derived, ResourceType, MaxResourceCount, HandleType>;
    ResourceDB() { m_queued_destroy.reserve(16); }

    virtual ~ResourceDB() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(ResourceDB, this_type);

    inline Result<> destroy(const HandleType handle) {
        auto r = m_pool.get(handle);
        if (r) {
            m_queued_destroy.push_back(handle);
            return Ok<>();
        }
        return Error<>();
    }

    void flush_destroy_queue() {
        Derived* derived = static_cast<Derived*>(this);
        for (auto& handle : m_queued_destroy) {
            auto r_lookup = m_pool.get(handle);
            if (r_lookup) {
                derived->destroy_resource(*r_lookup.value());
                (void)m_pool.erase(handle);
            }
        }
        m_queued_destroy.clear();
    }

    inline Result<NotMyPtr<const ResourceType>, void> get(const HandleType handle) const {
        return m_pool.get(handle);
    }

    inline void shutdown() {
        Derived* derived = static_cast<Derived*>(this);
        auto call = [derived](const ResourceType& resource) {
            derived->destroy_resource(resource);
        };
        m_pool.for_each(call);
        m_pool.clear();
        m_queued_destroy.clear();
    }
};

}    // namespace modus::threed::gles3
