/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gles3/types_gles3.hpp>
#include <threed/buffer.hpp>
#include <threed/program.hpp>

namespace modus::threed::gles3 {

enum class GLEnable : u32 {
    ScissorTest = 0,
    CullFace,
    DepthTest,
    DepthMask,
    StencilTest,
    Blend,
    BlendIndependent,
    Total
};

class StateCacheGLES3 {
   private:
    std::bitset<u32(GLEnable::Total)> m_enabled;
    GLFramebufferId m_frambuffer_read;
    GLFramebufferId m_frambuffer_draw;
    struct {
        u32 x = 0;
        u32 y = 0;
        u32 w = 0;
        u32 h = 0;
    } m_viewport;
    struct {
        u32 x = 0;
        u32 y = 0;
        u32 w = 0;
        u32 h = 0;
    } m_scissor;

    struct {
        GLBufferId data;
        GLBufferId index;
        GLBufferId uniform;
        GLBufferId storage;
    } m_buffers;

    struct {
        GLenum face = GL_INVALID_ENUM;
        GLenum front_face = GL_INVALID_ENUM;
    } m_cull;

    struct {
        GLenum func = GL_INVALID_ENUM;
    } m_depth;

    struct {
        // TODO:
    } m_stencil;

    GLVAOId m_vao;

    struct {
        GLenum equation_source = GL_INVALID_ENUM;
        GLenum equation_destination = GL_INVALID_ENUM;
        GLenum equation_source_alpha = GL_INVALID_ENUM;
        GLenum equation_destination_alpha = GL_INVALID_ENUM;
        GLenum operation_colour = GL_INVALID_ENUM;
        GLenum operation_alpha = GL_INVALID_ENUM;
    } m_blend;

    struct {
        u32 active_index = 0;
        GLTextureId textures[kMaxProgramSamplerInputs];
        GLSamplerId samplers[kMaxProgramSamplerInputs];
    } m_textures;

    struct BufferProgramInputBinding {
        GLBufferId id;
        u32 offset = 0;
        u32 size = 0;

        bool operator==(const BufferProgramInputBinding& rhs) const {
            return id == rhs.id && offset == rhs.offset && size == rhs.offset;
        }
        bool operator!=(const BufferProgramInputBinding& rhs) const {
            return id != rhs.id || offset != rhs.offset || size != rhs.offset;
        }
    };
    struct {
        BufferProgramInputBinding cbuffers[kMaxProgramConstantInputs];
        BufferProgramInputBinding sbuffers[kMaxProgramConstantInputs];
    } m_program_inputs;

   public:
    StateCacheGLES3();

    MODUS_CLASS_DISABLE_COPY_MOVE(StateCacheGLES3);

    void reset_buffer_cache();

    void bind_buffer(const BufferType type, const GLBufferId id, const GLenum gl_type);

    void bind_texture(const u32 index, const GLTextureId id, const GLenum gl_type);

    void bind_buffer_base_constant(GLBufferId id, const u32 loc, const u32 offset, const u32 size);

    void bind_buffer_base_storage(GLBufferId id, const u32 loc, const u32 offset, const u32 size);

    void bind_sampler(const u32 index, const GLSamplerId id);

    void bind_framebuffer_read(GLFramebufferId id);

    void bind_framebuffer_draw(GLFramebufferId id);

    void set_view_port(const u32 x, const u32 y, const u32 w, const u32 h);

    void set_scissor(const u32 x, const u32 y, const u32 w, const u32 h);

    void set_scissor_test_enabled(const bool v);

    void bind_buffer_element_array(const GLBufferId id);

    void force_bind_buffer_element_array(const GLBufferId id);

    void bind_buffer_data(const GLBufferId id);

    void bind_buffer_uniform(const GLBufferId id);

    void bind_buffer_storage(const GLBufferId id);

    void set_cullface_enabled(const bool enabled);

    void set_cullface(const GLenum cull_face);

    void set_front_face(const GLenum front_face);

    void set_depth_test_enabled(const bool enabled);

    void set_depth_mask_enabled(const bool enabled);

    void set_depth_func(const GLenum func);

    void set_stencil_test_enabled(const bool enabled);

    void bind_vao(const GLVAOId id);

    void set_blend_enabled(const bool enabled);

    void set_blend_params(const bool separate,
                          const GLenum equation_source,
                          const GLenum equation_destination,
                          const GLenum equation_source_aplha,
                          const GLenum equation_destination_alpha,
                          const GLenum operation_colour,
                          const GLenum operation_alpha);
};

}    // namespace modus::threed::gles3
