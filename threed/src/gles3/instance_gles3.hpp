/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/instance.hpp>

namespace modus::threed::gles3 {

class ContextGLES;

class InstanceGLES3 final : public Instance {
   public:
    InstanceGLES3();

    ~InstanceGLES3();

    InstanceResult initialize() override;

    void shutdown() override;

    Optional<FramebufferHandle> default_framebuffer() override;

    [[nodiscard]] std::unique_ptr<Device> create_device() override;

    ApiVersion api() const override;
};
}    // namespace modus::threed::gles3
