/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/frame_buffer.hpp>

namespace modus::threed::gles3 {

class TextureDBGLES3;
class StateCacheGLES3;

struct FramebufferGLES3 {
    GLFramebufferId gl_id;
    FramebufferCreateParams params;
};

static constexpr u32 kMaxFramebufferCount = 32;
class FramebufferDBGLES3 : public ResourceDB<FramebufferDBGLES3,
                                             FramebufferGLES3,
                                             kMaxFramebufferCount,
                                             FramebufferHandle> {
   public:
    FramebufferDBGLES3();

    Result<FramebufferHandle, FramebufferError> create(const FramebufferCreateParams& params,
                                                       const TextureDBGLES3& texture_db,
                                                       StateCacheGLES3& state_cache);

    void destroy_resource(const FramebufferGLES3& resource);
};
}    // namespace modus::threed::gles3
