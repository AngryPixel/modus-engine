/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/effect_gles3.hpp>
#include <gles3/program_gles3.hpp>

namespace modus::threed::gles3 {

Result<EffectHandle, String> EffectDBGLES3::create(const EffectCreateParams& params) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error(String("Max pool capacity reached"));
    }

    EffectGLES3 effect;
    effect.params = params;

    auto add_result = m_pool.create(effect);
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        return Error(String("Unknown error"));
    }
    return Ok<EffectHandle>(add_result.value().first);
}

void EffectDBGLES3::destroy_resource(const EffectGLES3&) {}

}    // namespace modus::threed::gles3
