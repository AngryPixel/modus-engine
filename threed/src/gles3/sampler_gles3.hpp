/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/sampler.hpp>

namespace modus::threed::gles3 {

class ProgramDBGLES3;

struct SamplerGLES3 {
    SamplerCreateParams params;
    GLSamplerId gl_id;
};

static constexpr u32 kMaxSamplerCount = 64;
class SamplerDBGLES3
    : public ResourceDB<SamplerDBGLES3, SamplerGLES3, kMaxSamplerCount, SamplerHandle> {
   public:
    SamplerDBGLES3() = default;

    Result<SamplerHandle, String> create(const SamplerCreateParams& params);

    void destroy_resource(const SamplerGLES3& resource);
};
}    // namespace modus::threed::gles3
