/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/effect.hpp>

namespace modus::threed::gles3 {

class ProgramDBGLES3;

struct EffectGLES3 {
    EffectCreateParams params;
};

static constexpr u32 kMaxEffectCount = 64;
class EffectDBGLES3 : public ResourceDB<EffectDBGLES3, EffectGLES3, kMaxEffectCount, EffectHandle> {
   public:
    EffectDBGLES3() = default;

    Result<EffectHandle, String> create(const EffectCreateParams& params);

    void destroy_resource(const EffectGLES3& resource);
};
}    // namespace modus::threed::gles3
