/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/drawable.hpp>

namespace modus::threed::gles3 {

class BufferDBGLES3;
class StateCacheGLES3;

struct DrawableGLES3 {
    DrawableCreateParams params;
    GLVAOId vao;
};

static constexpr u32 kMaxDrawableCount = 32;
class DrawableDBGLES3
    : public ResourceDB<DrawableDBGLES3, DrawableGLES3, kMaxDrawableCount, DrawableHandle> {
   private:
    GLVAOId m_default_vao;

   public:
    DrawableDBGLES3() = default;

    Result<> initialize();

    GLVAOId default_vao() const { return m_default_vao; }

    Result<DrawableHandle, modus::String> create(const DrawableCreateParams& params,
                                                 const BufferDBGLES3& buffer_db,
                                                 StateCacheGLES3& state_cache);

    Result<void, String> update(const DrawableHandle handle,
                                const DrawableUpdateParams& params,
                                const BufferDBGLES3& buffer_db,
                                StateCacheGLES3& state_cache);

    void destroy_resource(const DrawableGLES3& resource);
};
}    // namespace modus::threed::gles3
