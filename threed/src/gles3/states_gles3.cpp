/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/states_gles3.hpp>

namespace modus::threed::gles3 {

static const GLenum kGlCompareFunction[] = {
    GL_NEVER, GL_ALWAYS, GL_EQUAL, GL_NOTEQUAL, GL_LESS, GL_LEQUAL, GL_GREATER, GL_GEQUAL,
};

static_assert(modus::array_size(kGlCompareFunction) == usize(CompareFunction::Total));

GLenum gl_compare_function(const CompareFunction fn) {
    modus_assert(fn < CompareFunction::Total);
    return kGlCompareFunction[static_cast<u8>(fn)];
}

static const GLenum kGLStencilOp[]{GL_KEEP,      GL_ZERO, GL_REPLACE,   GL_INCR,
                                   GL_INCR_WRAP, GL_DECR, GL_DECR_WRAP, GL_INVERT};

static_assert(modus::array_size(kGLStencilOp) == usize(StencilOp::Total));
GLenum gl_stencil_op(const StencilOp op) {
    modus_assert(op < StencilOp::Total);
    return kGLStencilOp[u8(op)];
}

}    // namespace modus::threed::gles3
