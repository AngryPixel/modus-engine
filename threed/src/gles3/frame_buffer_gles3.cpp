/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <gles3/frame_buffer_gles3.hpp>
#include <gles3/state_cache_gles3.hpp>
#include <gles3/texture_gles3.hpp>
#include <threed/texture.hpp>

namespace modus::threed::gles3 {

static Result<FramebufferGLES3, FramebufferError> create_framebuffer(
    const FramebufferCreateParams& params,
    const TextureDBGLES3& texture_db,
    StateCacheGLES3& state_cache) {
    struct GLFramebufferAttachemnt {
        GLuint id = 0;
        GLenum type = GL_INVALID_ENUM;
        u32 index = 0;
        u32 mip_map_level = 0;
    };

    GLFramebufferAttachemnt depth;
    GLFramebufferAttachemnt stencil;
    GLFramebufferAttachemnt colours[FramebufferCreateParams::kMaxColourComponentCount];
    // Check depth attachment
    if (params.depth.handle.is_valid()) {
        auto r_depth_texture = texture_db.get(params.depth.handle);
        if (!r_depth_texture) {
            return Error(FramebufferError::InvalidDepthComponent);
        }

        const TextureGLES3& depth_texture = *(r_depth_texture.value());

        if (depth_texture.params.type != TextureType::T2D &&
            depth_texture.params.type != TextureType::T2DMultiSample) {
            return Error(FramebufferError::InvalidDepthComponent);
        }

        const GLTextureDesc& depth_desc = gl_texture_desc(depth_texture.params.format);
        if (!depth_desc.depth_component) {
            return Error(FramebufferError::InvalidDepthComponent);
        }

        depth.id = depth_texture.gl_id;
        depth.type = depth_texture.gl_type;
        depth.mip_map_level = params.depth.mip_map_level;
        if (depth_desc.stencil_component) {
            modus_assert(depth_texture.params.format == TextureFormat::Depth24Stencil8);
            stencil.id = depth.id;
        }
    }

    if (params.stencil.handle.is_valid()) {
        // Stencil component already set from depth_stencil attachment
        if (stencil.id != 0) {
            return Error(FramebufferError::DuplicateStencilComponent);
        }

        auto r_stencil_texutre = texture_db.get(params.stencil.handle);
        if (!r_stencil_texutre) {
            return Error(FramebufferError::InvalidStencilComponent);
        }

        const TextureGLES3& stencil_texture = *(r_stencil_texutre.value());

        if (stencil_texture.params.type != TextureType::T2D &&
            stencil_texture.params.type != TextureType::T2DMultiSample) {
            return Error(FramebufferError::InvalidStencilComponent);
        }

        const GLTextureDesc& stencil_desc = gl_texture_desc(stencil_texture.params.format);
        if (!stencil_desc.stencil_component) {
            return Error(FramebufferError::InvalidStencilComponent);
        }
        stencil.id = stencil_texture.gl_id;
        stencil.type = stencil_texture.gl_type;
        stencil.mip_map_level = params.stencil.mip_map_level;
    }

    for (u32 i = 0; i < FramebufferCreateParams::kMaxColourComponentCount; ++i) {
        const FramebufferTexture& param_colour = params.colour[i];
        if (param_colour.handle.is_valid()) {
            auto r_colour_texture = texture_db.get(param_colour.handle);
            if (!r_colour_texture) {
                return Error(FramebufferError::InvalidColourComponent);
            }

            const TextureGLES3& colour_texture = *(r_colour_texture.value());

            if (colour_texture.params.type == TextureType::TCubeMap &&
                param_colour.index > u32(TextureCubeMapFace::Back)) {
                return Error(FramebufferError::InvalidColourComponent);
            }

            if ((colour_texture.params.type == TextureType::T2DArray ||
                 colour_texture.params.type == TextureType::T3D) &&
                param_colour.index >= colour_texture.params.depth) {
                return Error(FramebufferError::InvalidColourComponent);
            }

            const GLTextureDesc& colour_desc = gl_texture_desc(colour_texture.params.format);
            if (colour_desc.stencil_component || colour_desc.depth_component ||
                colour_desc.compressed) {
                return Error(FramebufferError::InvalidColourComponent);
            }

            colours[i].id = colour_texture.gl_id;
            if (colour_texture.params.type == TextureType::TCubeMap) {
                colours[i].type = GL_TEXTURE_CUBE_MAP_POSITIVE_X + param_colour.index;
                colours[i].index = 0;
            } else {
                colours[i].type = colour_texture.gl_type;
                colours[i].index = param_colour.index;
            }
            colours[i].mip_map_level = param_colour.mip_map_level;
        }
    }

    FramebufferGLES3 fbo;
    GLuint gl_id;
    glGenFramebuffers(1, &gl_id);
    fbo.gl_id = GLFramebufferId(gl_id);
    fbo.params = params;
    state_cache.bind_framebuffer_draw(fbo.gl_id);
    // Attach depht
    if (depth.id != 0) {
        modus_assert_message(depth.index == 0, "3D/Array depth not yet implemented");
        if (stencil.id == depth.id) {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, depth.type,
                                   depth.id, depth.mip_map_level);
        } else {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth.type, depth.id,
                                   depth.mip_map_level);
        }
    }
    if (stencil.id != 0 && stencil.id != depth.id) {
        modus_assert_message(stencil.index == 0, "3D/Array stencil not yet implemented");
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, stencil.type, stencil.id,
                               stencil.mip_map_level);
    }

    for (u32 i = 0; i < FramebufferCreateParams::kMaxColourComponentCount; ++i) {
        if (colours[i].id != 0) {
            if (colours[i].index == 0) {
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, colours[i].type,
                                       colours[i].id, colours[i].mip_map_level);
            } else {
                glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, colours[i].id,
                                          colours[i].mip_map_level, colours[i].index);
            }
        }
    }

    // Validate Framebuffer

    bool valid_fbo = false;
    FramebufferError error = FramebufferError::Incomplete;
    const GLenum fbo_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch (fbo_status) {
        case GL_FRAMEBUFFER_COMPLETE:
            valid_fbo = true;
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            MODUS_LOGE(
                "FramebufferGLES3: attachment points are framebuffer "
                "incomplete");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            MODUS_LOGE("FramebufferGLES3: must have at least one color attachment");
            error = FramebufferError::MissingColourComponent;
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED:
            MODUS_LOGE(
                "FramebufferGLES3: The combination of internal formats of the "
                "attached images violates an implementation-dependent set of "
                "restrictions");
            error = FramebufferError::Unsupported;
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            MODUS_LOGE(
                "FramebufferGLES3: Value of GL_RENDERBUFFER_SAMPLES is not the "
                "same for all attached renderbuffers; if the value of "
                "GL_TEXTURE_SAMPLES is the not same for all attached textures; "
                "or, if the attached images are a mix of renderbuffers and "
                "textures, the value of GL_RENDERBUFFER_SAMPLES does not match "
                "the value of GL_TEXTURE_SAMPLES");
            error = FramebufferError::IncompleteMultisample;
            break;
        default:
            modus_assert_message(false, "Unhandled fbo status");
            error = FramebufferError::Unknown;
            break;
    }

    if (!valid_fbo) {
        glDeleteFramebuffers(1, &gl_id);
        return Error(error);
    }
    return Ok(fbo);
}

static void destroy_framebuffer(const GLFramebufferId id) {
    const GLuint gl_id = id;
    glDeleteFramebuffers(1, &gl_id);
}

FramebufferDBGLES3::FramebufferDBGLES3() {
    FramebufferGLES3 global_framebuffer;
    global_framebuffer.gl_id = GLFramebufferId(0);
    auto r_handle = m_pool.create(global_framebuffer);
    r_handle.expect("Failed to craete global framebuffer");
    // 0 is being used as the default framebuffer handle
    modus_assert(r_handle.value().first.value() == 0);
}

Result<FramebufferHandle, FramebufferError> FramebufferDBGLES3::create(
    const FramebufferCreateParams& params,
    const TextureDBGLES3& texture_db,
    StateCacheGLES3& state_cache) {
    if (m_pool.is_full()) {
        modus_assert_message(false, "Max capacity reached!");
        return Error(FramebufferError::MaxCount);
    }

    auto create_result = create_framebuffer(params, texture_db, state_cache);
    if (!create_result) {
        return Error(create_result.error());
    }

    auto add_result = m_pool.create(create_result.value());
    if (!add_result) {
        modus_assert_message(false, "Failed to add handle");
        destroy_framebuffer(create_result.value().gl_id);
        return Error(FramebufferError::Unknown);
    }

    return Ok<FramebufferHandle>(add_result.value().first);
}

void FramebufferDBGLES3::destroy_resource(const FramebufferGLES3& resource) {
    if (resource.gl_id.value() != 0) {
        destroy_framebuffer(resource.gl_id);
    }
}

}    // namespace modus::threed::gles3
