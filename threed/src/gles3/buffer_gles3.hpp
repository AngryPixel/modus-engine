/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/buffer.hpp>

#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>

namespace modus::threed::gles3 {

class StateCacheGLES3;

struct BufferGLES3 {
    GLBufferId gl_id;
    GLenum gl_type;
    GLenum gl_usage;
    BufferCreateParams params;
};

static constexpr u32 kMaxBufferCount = 64;
class BufferDBGLES3 : public ResourceDB<BufferDBGLES3, BufferGLES3, kMaxBufferCount, BufferHandle> {
   public:
    BufferDBGLES3() = default;

    Result<BufferHandle, void> create(const BufferCreateParams& params,
                                      StateCacheGLES3& state_cache);

    void destroy_resource(const BufferGLES3& resource);

    Result<> update(const BufferHandle handle,
                    const ByteSlice slice,
                    const u32 offset,
                    const bool invalidate_buffer,
                    StateCacheGLES3& state_cache);
};

//#define MODUS_THREED_GLES3_USE_RING_UNIFORM_BUFFER

struct ConstantBufferGLES3 {
    GLBufferId gl_id;
    u32 size;
    GLenum gl_usage;
};

static constexpr u32 kMaxConstantBufferCount = 32;
class ConstantBufferDBGLES3 : public ResourceDB<ConstantBufferDBGLES3,
                                                ConstantBufferGLES3,
                                                kMaxBufferCount,
                                                ConstantBufferHandle> {
    u32 m_uniform_offset_alignment;
    u32 m_max_uniform_buffer_size;

   public:
    ConstantBufferDBGLES3() = default;

    Result<ConstantBufferHandle, void> create(const ConstantBufferCreateParams& params,
                                              StateCacheGLES3& state_cache);

    void initialize();

    Result<const ConstantBufferGLES3*, void> get(const ConstantBufferHandle handle) const;

    Result<> update(const ConstantBufferHandle handle,
                    const void* ptr,
                    const usize size,
                    StateCacheGLES3& state_cache);

    void destroy_resource(const ConstantBufferGLES3& resource);

    void swap_buffers();
};

struct ShaderBufferGLES3 {
    GLBufferId gl_id;
    u32 size;
    GLenum gl_usage;
};

static constexpr u32 kMaxShaderBufferCount = 32;
class ShaderBufferDBGLES3 : public ResourceDB<ShaderBufferDBGLES3,
                                              ShaderBufferGLES3,
                                              kMaxBufferCount,
                                              ShaderBufferHandle> {
   public:
    ShaderBufferDBGLES3() = default;

    Result<ShaderBufferHandle, void> create(const ShaderBufferCreateParams& params,
                                            StateCacheGLES3& state_cache);

    Result<const ShaderBufferGLES3*, void> get(const ShaderBufferHandle handle) const;

    Result<> update(const ShaderBufferHandle handle,
                    const void* ptr,
                    const usize size,
                    StateCacheGLES3& state_cache);

    void destroy_resource(const ShaderBufferGLES3& resource);
};
}    // namespace modus::threed::gles3
