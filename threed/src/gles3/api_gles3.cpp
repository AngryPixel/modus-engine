/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#if defined(MODUS_THREED_USE_OPENGL_43)
#include <gles3/api_gl4/flextGL.h>
#elif defined(MODUS_THREED_USE_OPENGL_ES3)
#include <gles3/api_gles31/flextGL.h>
#else
#error "Unknown OpenGL API"
#endif

// clang-format on
#include <gles3/api_gles3.hpp>
#include <gles3/device_gles3.hpp>
#include <log/log.hpp>

namespace modus::threed::gles3 {

#if defined(MODUS_DEBUG) || (defined(_MSC_VER) && defined(MODUS_RELEASE))
#define USE_GL_DEBUG
#endif

#if defined(USE_GL_DEBUG)
static void APIENTRY gl_debug_callback(GLenum source,
                                       GLenum type,
                                       GLuint id,
                                       GLenum severity,
                                       GLsizei length,
                                       const GLchar* message,
                                       const void* userParam) {
    (void)source;
    (void)id;
    (void)length;
    (void)userParam;
    const char* chr_severity;
    switch (severity) {
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_SEVERITY_HIGH_KHR:
#else
        case GL_DEBUG_SEVERITY_HIGH:
#endif
            chr_severity = "high";
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_SEVERITY_MEDIUM_KHR:
#else
        case GL_DEBUG_SEVERITY_MEDIUM:
#endif
            chr_severity = "medium";
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_SEVERITY_LOW_KHR:
#else
        case GL_DEBUG_SEVERITY_LOW:
#endif
            chr_severity = "low";
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_SEVERITY_NOTIFICATION_KHR:
#else
        case GL_DEBUG_SEVERITY_NOTIFICATION:
#endif
            chr_severity = "notification";
            break;
        default:
            chr_severity = "unknown";
    }

    switch (type) {
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_TYPE_ERROR_KHR:
#else
        case GL_DEBUG_TYPE_ERROR:
#endif
            MODUS_LOGE("[GL_DEBUG]({}): {} (Severity: {})", "Error", message, chr_severity);
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR:
#else
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
#endif
            MODUS_LOGD("[GL_DEBUG]({}): {} (Severity: {})", "Deprecated Behaviour", message,
                       chr_severity);
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR:
#else
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
#endif
            MODUS_LOGE("[GL_DEBUG]({}): {} (Severity: {})", "Undefined Behaviour", message,
                       chr_severity);
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_TYPE_PORTABILITY_KHR:
#else
        case GL_DEBUG_TYPE_PORTABILITY:
#endif
            MODUS_LOGD("[GL_DEBUG]({}): {} (Severity: {})", "portability", message, chr_severity);
            break;
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        case GL_DEBUG_TYPE_PERFORMANCE_KHR:
#else
        case GL_DEBUG_TYPE_PERFORMANCE:
#endif
            MODUS_LOGD("[GL_DEBUG]({}): {} (Severity: {})", "performace", message, chr_severity);
            break;
        default:
#if !defined(MODUS_THREED_LOG_UNKNOWN_GL_REPORTS)
            return;
#endif
            MODUS_LOGD("[GL_DEBUG]({}): {} (Severity: {})", "Unknown", message, chr_severity);
            break;
    }
}
#endif

static void log_gl_info() {
    MODUS_LOGV("OpenGL Vendor                        : {}", glGetString(GL_VENDOR));
    MODUS_LOGV("OpenGL Renderer                      : {}", glGetString(GL_RENDERER));
    MODUS_LOGV("OpenGL Version                       : {}", glGetString(GL_VERSION));
    MODUS_LOGV("OpenGL GLSL Version                  : {}",
               glGetString(GL_SHADING_LANGUAGE_VERSION));

    // get extensions
    MODUS_LOGD("OpenGL Extensions:");
    GLint num_extensions;
    glGetIntegerv(GL_NUM_EXTENSIONS, &num_extensions);

    for (GLint i = 0; i < num_extensions; i++) {
        MODUS_LOGD("    > {}", glGetStringi(GL_EXTENSIONS, i));
    }
}

InstanceResult initialize() {
    if (flextInit() != 1) {
        return Error(InstanceError::ApiMissingFeatures);
    }

    log_gl_info();

    if (FLEXT_KHR_debug) {
#if defined(USE_GL_DEBUG)
#if defined(MODUS_THREED_USE_OPENGL_ES3)
        glEnable(GL_DEBUG_OUTPUT_KHR);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR);
        glDebugMessageCallbackKHR(gl_debug_callback, nullptr);
#else
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(gl_debug_callback, nullptr);
#endif
#endif
    }

    return Ok<>();
}

FramebufferHandle default_framebuffer() {
    return FramebufferHandle(0);
}

}    // namespace modus::threed::gles3
