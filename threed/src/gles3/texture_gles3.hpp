/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gles3/resource_db.hpp>
#include <gles3/types_gles3.hpp>
#include <threed/texture.hpp>

namespace modus::threed::gles3 {

class StateCacheGLES3;

struct TextureGLES3 {
    GLTextureId gl_id;
    GLenum gl_type;
    TextureCreateParams params;
};

struct GLTextureDesc {
    GLenum format;
    GLenum internal;
    GLenum data_type;
    bool compressed;
    bool depth_component;
    bool stencil_component;
};

const GLTextureDesc& gl_texture_desc(const TextureFormat format);

static constexpr u32 kMaxTextureCount = 64;
class TextureDBGLES3 final
    : public ResourceDB<TextureDBGLES3, TextureGLES3, kMaxTextureCount, TextureHandle> {
   public:
    TextureDBGLES3() = default;

    Result<TextureHandle, void> create(const TextureCreateParams& params,
                                       StateCacheGLES3& state_cache);

    void destroy_resource(const TextureGLES3& resource);
};
}    // namespace modus::threed::gles3
