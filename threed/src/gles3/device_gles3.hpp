/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/allocator/linear_allocator.hpp>
#include <gles3/buffer_gles3.hpp>
#include <gles3/drawable_gles3.hpp>
#include <gles3/effect_gles3.hpp>
#include <gles3/frame_buffer_gles3.hpp>
#include <gles3/program_gen/shader_generator_gles3.hpp>
#include <gles3/program_gles3.hpp>
#include <gles3/sampler_gles3.hpp>
#include <gles3/state_cache_gles3.hpp>
#include <gles3/texture_gles3.hpp>
#include <os/thread.hpp>
#include <threed/device.hpp>

namespace modus::threed {
class Device;
class Framebuffer;
class Instance;
}    // namespace modus::threed

#if !defined(MODUS_PROFILING) && !defined(MODUS_RTM)
#define MODUS_THREED_CHECK_PIPELINE_BEHAVIOR
#endif

namespace modus::threed::gles3 {

class InstanceGLES3;

class DeviceGLES3 final : public Device {
   private:
    os::ThreadWatcher m_thread_watcher;
    AlignedLinearAllocator<16, ThreedAllocator> m_frame_allocator;
    ProgramDBGLES3 m_program_db;
    BufferDBGLES3 m_buffer_db;
    ConstantBufferDBGLES3 m_constant_buffer_db;
    ShaderBufferDBGLES3 m_shader_buffer_db;
    EffectDBGLES3 m_effect_db;
    TextureDBGLES3 m_texture_db;
    FramebufferDBGLES3 m_framebuffer_db;
    DrawableDBGLES3 m_drawable_db;
    SamplerDBGLES3 m_sampler_db;
    StateCacheGLES3 m_state_cache;
    NotMyPtr<const ProgramGLES3> m_active_program;
    DeviceLimits m_device_limits;
    ShaderGeneratorGLES3 m_shader_generator;

#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    bool m_pipeline_created = false;
    bool m_pipeline_submitted = false;
#endif

   public:
    DeviceGLES3(InstanceGLES3& instance);

    Result<> initialize() override;

    void shutdown() override;

    const dynamic::ShaderGenerator& shader_generator() const override;

    void begin_frame() override;

    void end_frame() override;

    const DeviceLimits& device_limits() const override;

    Result<FramebufferHandle, FramebufferError> create_framebuffer(
        const FramebufferCreateParams&) override;

    void destroy_framebuffer(const FramebufferHandle handle) override;

    void framebuffer_copy(const FramebufferCopyParams& params) override;

    void execute_pipeline(const Pipeline& pipeline) override;

    Result<ProgramHandle, String> create_program(const ProgramCreateParams& programs) override;

    void destroy_program(const ProgramHandle handle) override;

    Result<> reflect_program(const ProgramHandle handle,
                             ProgramReflection& reflection) const override;

    Result<BufferHandle, void> create_buffer(const BufferCreateParams& params) override;

    void destroy_buffer(const BufferHandle handle) override;

    Result<ConstantBufferHandle, void> create_constant_buffer(
        const ConstantBufferCreateParams& params) override;

    void destroy_constant_buffer(const ConstantBufferHandle handle) override;

    Result<> update_constant_buffer(const ConstantBufferHandle handle,
                                    const void* ptr,
                                    const usize size) override;

    Result<ShaderBufferHandle, void> create_shader_buffer(
        const ShaderBufferCreateParams& params) override;

    void destroy_shader_buffer(const ShaderBufferHandle handle) override;

    Result<> update_shader_buffer(const ShaderBufferHandle handle,
                                  const void* ptr,
                                  const usize size) override;

    Result<> update_buffer(const BufferHandle handle,
                           const ByteSlice data,
                           const u32 start_offset,
                           const bool invalidate_buffer) override;

    Result<EffectHandle, String> create_effect(const EffectCreateParams& params) override;

    void destroy_effect(const EffectHandle handle) override;

    ConstantHandle allocate_constant(const void* ptr, const usize size) override;

    Result<TextureHandle, void> create_texture(const TextureCreateParams& params) override;

    Result<TextureHandle, void> create_texture(const TextureCreateParams& params,
                                               Slice<TextureData> data) override;

    Result<TextureHandle, void> create_texture(const TextureCreateParams& params,
                                               Slice<TextureDataCubeMap> data) override;

    Result<> update_texture(const TextureHandle h, Slice<TextureData> data) override;

    void destroy_texture(const TextureHandle handle) override;

    Result<DrawableHandle, String> create_drawable(const DrawableCreateParams& params) override;

    void destroy_drawable(const DrawableHandle handle) override;

    Result<void, String> update_drawable(const DrawableHandle handle,
                                         const DrawableUpdateParams& params) override;

    Result<DrawableUpdateHandle> allocate_drawable_update(
        const DrawableUpdateParams& params) override;

    Result<SamplerHandle, String> create_sampler(const SamplerCreateParams& params) override;

    void destroy_sampler(const SamplerHandle handle) override;

    Command* allocate_command(Pass& pass) override;

    Pass* allocate_pass(Pipeline& pipeline, const StringSlice name) override;

    Pipeline* allocate_pipeline(const StringSlice name) override;

   private:
    void clear_colour(const u32 index, const glm::vec4& colour);

    void clear_depth(const f32 depth);

    void clear_stencil(const u8 stencil);

    void apply_depth_stencil_state(const DepthStencilState& ds);

    void apply_blend_state(const BlendState& bs);

    void apply_raster_state(const RasterState& bs);

    void apply_effect(const NotMyPtr<const ProgramGLES3>& program,
                      const EffectGLES3& effect,
                      const Command& command);
};
}    // namespace modus::threed::gles3
