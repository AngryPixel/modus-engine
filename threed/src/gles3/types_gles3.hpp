/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if defined(MODUS_THREED_USE_OPENGL_43)
#include <gles3/api_gl4/flextGL.h>
#elif defined(MODUS_THREED_USE_OPENGL_ES3)
#include <gles3/api_gles31/flextGL.h>
#else
#error "Unknown OpenGL API"
#endif

#include <core/strong_type.hpp>

#define MODUS_DECLARE_THREED_GLES3_HANDLE(Name, Invalid)        \
    MODUS_STRONG_TYPE_CLASS(Name, GLuint, Invalid)              \
    inline bool is_valid() const { return value() != Invalid; } \
    inline operator bool() const { return is_valid(); }         \
    inline operator GLuint() const { return value(); }          \
    }

namespace modus::threed::gles3 {

MODUS_DECLARE_THREED_GLES3_HANDLE(GLProgramId, 0);
MODUS_DECLARE_THREED_GLES3_HANDLE(GLShaderId, 0);
MODUS_DECLARE_THREED_GLES3_HANDLE(GLTextureId, 0);
MODUS_DECLARE_THREED_GLES3_HANDLE(GLBufferId, 0);
MODUS_DECLARE_THREED_GLES3_HANDLE(GLSamplerId, 0);
MODUS_DECLARE_THREED_GLES3_HANDLE(GLVAOId, 0);
MODUS_DECLARE_THREED_GLES3_HANDLE(GLFramebufferId, std::numeric_limits<u32>::max());

}    // namespace modus::threed::gles3

#undef MODUS_DECLARE_THREED_GLES3_HANDLE
