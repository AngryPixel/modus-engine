/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/allocator/linear_allocator.hpp>
#include <threed/device.hpp>
#include <threed/program_gen/shader_generator.hpp>

namespace modus::threed {
class Device;
class Framebuffer;
class Instance;
}    // namespace modus::threed

#if !defined(MODUS_PROFILING) && !defined(MODUS_RTM)
#define MODUS_THREED_CHECK_PIPELINE_BEHAVIOR
#endif

namespace modus::threed::null {

class InstanceNull;

class DeviceNull final : public Device {
   private:
    AlignedLinearAllocator<16, ThreedAllocator> m_frame_allocator;
    DeviceLimits m_device_limits;
    u32 m_fbo_counter;
    u32 m_program_counter;
    u32 m_buffer_counter;
    u32 m_constant_buffer_counter;
    u32 m_texture_counter;
    u32 m_effect_counter;
    u32 m_drawable_counter;
    dynamic::NullShaderGenerator m_shader_generator;

#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    bool m_pipeline_created;
    bool m_pipeline_submitted;
#endif

   public:
    DeviceNull(InstanceNull& instance);

    Result<> initialize() override;

    void shutdown() override;

    const dynamic::ShaderGenerator& shader_generator() const override;

    void begin_frame() override;

    void end_frame() override;

    const DeviceLimits& device_limits() const override;

    Result<FramebufferHandle, FramebufferError> create_framebuffer(
        const FramebufferCreateParams&) override;

    void destroy_framebuffer(const FramebufferHandle handle) override;

    void framebuffer_copy(const FramebufferCopyParams& params) override;

    void execute_pipeline(const Pipeline& pipeline) override;

    Result<ProgramHandle, String> create_program(const ProgramCreateParams& programs) override;

    void destroy_program(const ProgramHandle handle) override;

    Result<> reflect_program(const ProgramHandle handle,
                             ProgramReflection& reflection) const override;

    Result<BufferHandle, void> create_buffer(const BufferCreateParams& params) override;

    void destroy_buffer(const BufferHandle handle) override;

    Result<ConstantBufferHandle, void> create_constant_buffer(
        const ConstantBufferCreateParams& params) override;

    void destroy_constant_buffer(const ConstantBufferHandle handle) override;

    Result<> update_constant_buffer(const ConstantBufferHandle handle,
                                    const void* ptr,
                                    const usize size) override;

    Result<ShaderBufferHandle, void> create_shader_buffer(
        const ShaderBufferCreateParams& params) override;

    void destroy_shader_buffer(const ShaderBufferHandle handle) override;

    Result<> update_shader_buffer(const ShaderBufferHandle handle,
                                  const void* ptr,
                                  const usize size) override;

    Result<> update_buffer(const BufferHandle handle,
                           const ByteSlice data,
                           const u32 start_offset,
                           const bool invalidate_buffer) override;

    Result<EffectHandle, String> create_effect(const EffectCreateParams& params) override;

    void destroy_effect(const EffectHandle handle) override;

    ConstantHandle allocate_constant(const void* ptr, const usize size) override;

    Result<TextureHandle, void> create_texture(const TextureCreateParams& params) override;

    Result<TextureHandle, void> create_texture(const TextureCreateParams& params,
                                               Slice<TextureData> data) override;

    Result<TextureHandle, void> create_texture(const TextureCreateParams& params,
                                               Slice<TextureDataCubeMap> data) override;

    Result<> update_texture(const TextureHandle h, Slice<TextureData> data) override;

    void destroy_texture(const TextureHandle handle) override;

    Result<DrawableHandle, String> create_drawable(const DrawableCreateParams& params) override;

    void destroy_drawable(const DrawableHandle handle) override;

    Result<void, String> update_drawable(const DrawableHandle,
                                         const DrawableUpdateParams&) override;

    Result<DrawableUpdateHandle> allocate_drawable_update(
        const DrawableUpdateParams& params) override;

    Result<SamplerHandle, String> create_sampler(const SamplerCreateParams& params) override;

    void destroy_sampler(const SamplerHandle handle) override;

    Command* allocate_command(Pass& pass) override;

    Pass* allocate_pass(Pipeline& pipeline, const StringSlice name) override;

    Pipeline* allocate_pipeline(const StringSlice name) override;
};

}    // namespace modus::threed::null
