/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

//clang-format off
#include <pch.h>
#include <null/instance_null.hpp>
//clang-format on
#include <null/device_null.hpp>
#include <threed/compositor.hpp>
#include <threed/program_gen/shader_generator.hpp>

namespace modus::threed {

NullCompositor::NullCompositor() {
    m_frame_buffer = FramebufferHandle(2);
}

Result<void, String> NullCompositor::update(Device&) {
    m_requires_update = false;
    return Ok<>();
}

void NullCompositor::swap_buffers() {}

}    // namespace modus::threed

namespace modus::threed::null {

InstanceNull::InstanceNull() {}

InstanceNull::~InstanceNull() {}

InstanceResult InstanceNull::initialize() {
    return Ok<>();
}

void InstanceNull::shutdown() {}

Optional<FramebufferHandle> InstanceNull::default_framebuffer() {
    return Optional<FramebufferHandle>(FramebufferHandle(2));
}

[[nodiscard]] std::unique_ptr<Device> InstanceNull::create_device() {
    return modus::make_unique<null::DeviceNull>(*this);
}

ApiVersion InstanceNull::api() const {
    ApiVersion version;
    version.api = Api::Null;
    version.major = 0;
    version.minor = 0;
    version.patch = 0;
    return version;
}

}    // namespace modus::threed::null
