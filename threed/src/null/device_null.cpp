/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <null/device_null.hpp>
#include <null/instance_null.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/buffer.hpp>
#include <threed/pipeline.hpp>

namespace modus::threed::null {

struct ConstantHandlePrivate {
    unsigned offset : 24;
    unsigned data_size : 8;
};

union ConstantHandleConversion {
    u32 integer;
    ConstantHandlePrivate constant;
};

u32 constant_handle_private_to_u32(const ConstantHandlePrivate handle) {
    ConstantHandleConversion c;
    c.constant = handle;
    return c.integer;
}

ConstantHandlePrivate u32_to_constant_handle_private(const u32 integer) {
    ConstantHandleConversion c;
    c.integer = integer;
    return c.constant;
}

static_assert(sizeof(ConstantHandlePrivate) == sizeof(u32));

DeviceNull::DeviceNull(InstanceNull& instance) : Device(instance) {}

Result<> DeviceNull::initialize() {
    if (!m_frame_allocator.initialize(4 * 1024 * 1024)) {
        return Error<>();
    }

    m_device_limits.compressed_texture_support.astc = false;
    m_device_limits.compressed_texture_support.etc2 = true;
    m_device_limits.max_texture_size_px = 40000;
    m_device_limits.max_constant_buffer_size = 1241241414;
    m_device_limits.min_constant_buffer_size = 1241241241;
    m_fbo_counter = 4;
    m_program_counter = 0;
    m_buffer_counter = 0;
    m_constant_buffer_counter = 0;
    m_texture_counter = 0;
    m_effect_counter = 0;
    m_drawable_counter = 0;
    return Ok<>();
}

void DeviceNull::shutdown() {
    m_frame_allocator.shutdown();
}

const dynamic::ShaderGenerator& DeviceNull::shader_generator() const {
    return m_shader_generator;
}

void DeviceNull::begin_frame() {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    m_pipeline_created = false;
    m_pipeline_submitted = false;
#endif
}

void DeviceNull::end_frame() {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    m_pipeline_created = false;
    m_pipeline_submitted = false;
#endif
    m_frame_allocator.clear();
}

const DeviceLimits& DeviceNull::device_limits() const {
    return m_device_limits;
}

Result<FramebufferHandle, FramebufferError> DeviceNull::create_framebuffer(
    const FramebufferCreateParams&) {
    return Ok(FramebufferHandle(++m_fbo_counter));
}

void DeviceNull::destroy_framebuffer(const FramebufferHandle) {}

void DeviceNull::framebuffer_copy(const FramebufferCopyParams&) {}

void DeviceNull::execute_pipeline(const Pipeline&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    m_pipeline_submitted = true;
#endif
}

Result<ProgramHandle, String> DeviceNull::create_program(const ProgramCreateParams&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new resources once pipeline is submitted");
    }
#endif
    return Ok(ProgramHandle(++m_program_counter));
}

void DeviceNull::destroy_program(const ProgramHandle) {}

Result<> DeviceNull::reflect_program(const ProgramHandle, ProgramReflection&) const {
    return Ok<>();
}

Result<BufferHandle, void> DeviceNull::create_buffer(const BufferCreateParams&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new resources once pipeline is submitted");
    }
#endif
    return Ok(BufferHandle(++m_buffer_counter));
}

void DeviceNull::destroy_buffer(const BufferHandle) {}

Result<ConstantBufferHandle, void> DeviceNull::create_constant_buffer(
    const ConstantBufferCreateParams& params) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    if (params.size > m_device_limits.max_constant_buffer_size) {
        MODUS_LOGE("DeviceNull: Max constant buffer size exceed: {} > {}", params.size,
                   m_device_limits.max_constant_buffer_size);
        return Error<>();
    }

    return Ok(ConstantBufferHandle(++m_constant_buffer_counter));
}

void DeviceNull::destroy_constant_buffer(const ConstantBufferHandle) {}

Result<> DeviceNull::update_constant_buffer(const ConstantBufferHandle, const void*, const usize) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok<>();
}

Result<ShaderBufferHandle, void> DeviceNull::create_shader_buffer(const ShaderBufferCreateParams&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok(ShaderBufferHandle(++m_constant_buffer_counter));
}

void DeviceNull::destroy_shader_buffer(const ShaderBufferHandle) {}

Result<> DeviceNull::update_shader_buffer(const ShaderBufferHandle, const void*, const usize) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok<>();
}

Result<> DeviceNull::update_buffer(const BufferHandle, const ByteSlice, const u32, const bool) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok<>();
}

Result<EffectHandle, String> DeviceNull::create_effect(const EffectCreateParams&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok(EffectHandle(++m_effect_counter));
}

void DeviceNull::destroy_effect(const EffectHandle) {}

ConstantHandle DeviceNull::allocate_constant(const void*, const usize) {
    ConstantHandlePrivate cp;
    cp.data_size = 128;
    cp.offset = static_cast<u32>(1024);
    return ConstantHandle(constant_handle_private_to_u32(cp));
}

Result<TextureHandle, void> DeviceNull::create_texture(const TextureCreateParams&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok(TextureHandle(++m_texture_counter));
}

Result<TextureHandle, void> DeviceNull::create_texture(const TextureCreateParams&,
                                                       Slice<TextureData>) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok(TextureHandle(++m_texture_counter));
}

Result<TextureHandle, void> DeviceNull::create_texture(const TextureCreateParams&,
                                                       Slice<TextureDataCubeMap>) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok(TextureHandle(++m_texture_counter));
}

Result<> DeviceNull::update_texture(const TextureHandle, Slice<TextureData>) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok<>();
}

void DeviceNull::destroy_texture(const TextureHandle) {}

Result<DrawableHandle, String> DeviceNull::create_drawable(const DrawableCreateParams&) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_submitted) {
        modus_panic("Can't create new/update resources once pipeline is submitted");
    }
#endif
    return Ok(DrawableHandle(++m_drawable_counter));
}

void DeviceNull::destroy_drawable(const DrawableHandle) {}

Result<void, String> DeviceNull::update_drawable(const DrawableHandle,
                                                 const DrawableUpdateParams&) {
    return Ok<>();
}

Result<DrawableUpdateHandle> DeviceNull::allocate_drawable_update(const DrawableUpdateParams&) {
    return Ok(DrawableUpdateHandle(1234));
}

Result<SamplerHandle, String> DeviceNull::create_sampler(const SamplerCreateParams&) {
    return Ok(SamplerHandle(30));
}

void DeviceNull::destroy_sampler(const SamplerHandle) {}

Command* DeviceNull::allocate_command(Pass&) {
    void* ptr = this->m_frame_allocator.allocate(sizeof(Command));
    if (ptr == nullptr) {
        modus_panic("DeviceNull: Failed to allocate command");
    }
    Command* command = new (ptr) Command();
    return command;
}

Pass* DeviceNull::allocate_pass(Pipeline&, const StringSlice) {
    void* ptr = this->m_frame_allocator.allocate(sizeof(Pass));
    if (ptr == nullptr) {
        modus_panic("DeviceNull: Failed to allocate pass");
    }
    Pass* pass = new (ptr) Pass();
    return pass;
}

Pipeline* DeviceNull::allocate_pipeline(const StringSlice) {
#if defined(MODUS_THREED_CHECK_PIPELINE_BEHAVIOR)
    if (m_pipeline_created) {
        modus_panic("There can be only one pipeline");
    }

#endif
    void* ptr = this->m_frame_allocator.allocate(sizeof(Pipeline));
    if (ptr == nullptr) {
        modus_panic("DeviceNull: Failed to allocate pipeline");
    }
    Pipeline* pipeline = new (ptr) Pipeline;
    return pipeline;
}
}    // namespace modus::threed::null
