/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <threed/instance.hpp>
// clang-format on

#include <gles3/instance_gles3.hpp>
#include <null/instance_null.hpp>

namespace modus::threed {

Result<void, String> NullContext::bind_to_thread() {
    return Ok<>();
}

std::unique_ptr<Instance> create_gl_instance() {
    return modus::make_unique<gles3::InstanceGLES3>();
}

std::unique_ptr<Instance> create_null_instance() {
    return modus::make_unique<null::InstanceNull>();
}

}    // namespace modus::threed
