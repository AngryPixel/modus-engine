/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::threed {

static constexpr u8 kMaxDrawableInputCount = 16;
static constexpr u8 kMaxDrawableBufferInputs = 8;

struct MODUS_THREED_EXPORT DrawableDataParams {
    u32 offset = 0;
    DataType data_type = DataType::Total;
    bool normalized = false;
    u8 buffer_index = 0;
};

struct MODUS_THREED_EXPORT DrawableDataBufferDesc {
    BufferHandle buffer;
    u32 offset = 0;
    u32 stride = 0;
    u8 instance_divisor = 0;
};

struct MODUS_THREED_EXPORT DrawableIndexBufferDesc {
    BufferHandle buffer;
    u32 offset = 0;
};

struct MODUS_THREED_EXPORT DrawableCreateParams {
    DrawableDataParams data[kMaxDrawableInputCount];
    DrawableDataBufferDesc data_buffers[kMaxDrawableBufferInputs];
    DrawableIndexBufferDesc index_buffer;
    u32 start = 0;
    u32 count = 0;
    IndicesType index_type = IndicesType::None;
    Primitive primitive = Primitive::Triangles;
};

struct MODUS_THREED_EXPORT DrawableUpdateParams {
    DrawableDataBufferDesc data_buffers[kMaxDrawableBufferInputs];
    DrawableIndexBufferDesc index_buffer;
};

}    // namespace modus::threed
