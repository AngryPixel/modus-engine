/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <threed/threed.hpp>

namespace modus::threed::dynamic {

struct ProgramGenParams;
enum class ShaderStage : u8;
struct ShaderVertexStage;
struct ShaderFragmentStage;

class MODUS_THREED_EXPORT ShaderSnippet {
   private:
    const String m_id;

   protected:
    ShaderSnippet(StringSlice id);

   public:
    virtual ~ShaderSnippet() = default;

    const String& id() const { return m_id; }

    virtual Result<> apply(ShaderVertexStage& vertex_stage) const = 0;

    virtual Result<> apply(ShaderFragmentStage& fragment_stage) const = 0;
};

class MODUS_THREED_EXPORT ShaderSnippetDB {
   private:
    HashMapWithAllocator<String, NotMyPtr<const ShaderSnippet>, ThreedAllocator> m_snippets;

   public:
    ShaderSnippetDB() = default;

    MODUS_CLASS_DISABLE_COPY_MOVE(ShaderSnippetDB);

    Result<> register_snippet(NotMyPtr<const ShaderSnippet> snippet);

    void unregister_snippet(NotMyPtr<const ShaderSnippet> snippet);

    Result<> apply(ProgramGenParams& params, const ShaderStage, const String& snippet_id) const;

    Result<> apply(ProgramGenParams& params, const ShaderStage, Slice<String> snippet_ids) const;
};

}    // namespace modus::threed::dynamic
