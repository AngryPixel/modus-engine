/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

#include <threed/device.hpp>
#include <threed/pipeline.hpp>
#include <threed/program_gen/program_gen.hpp>

namespace modus::threed::dynamic {

template <typename T>
struct DataTypeTrait {
    static constexpr DataType data_type = DataType::Total;
};

#define MODUS_THREED_IMPL_DATA_TRAIT(CLASS, DT)             \
    template <>                                             \
    struct DataTypeTrait<CLASS> {                           \
        static constexpr DataType data_type = DataType::DT; \
    }
MODUS_THREED_IMPL_DATA_TRAIT(i32, I32);
MODUS_THREED_IMPL_DATA_TRAIT(u32, U32);
MODUS_THREED_IMPL_DATA_TRAIT(f32, F32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::vec4, Vec4F32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::vec3, Vec3F32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::vec2, Vec2F32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::ivec2, Vec2I32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::ivec3, Vec3I32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::ivec4, Vec4I32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::uvec2, Vec2U32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::uvec3, Vec3U32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::uvec4, Vec4U32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::mat2, Mat2F32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::mat3, Mat3F32);
MODUS_THREED_IMPL_DATA_TRAIT(glm::mat4, Mat4F32);
#undef MODUS_THREED_IMPL_DATA_TRAIT

class MODUS_THREED_EXPORT DefaultConstantInput : public ConstantInput {
   private:
    NotMyPtr<const InputBinder> m_binder;

   public:
    DefaultConstantInput(const StringSlice name, NotMyPtr<const InputBinder> binder)
        : m_binder(binder) {
        this->m_desc.name = name;
    }
    Result<BindableInputResult> generate_declaration(const u8 slot,
                                                     GenerateParams& params) const override final;
    Result<BindableInputResult> generate_type_snippet(GenerateParams&) const override;
    Result<BindableInputResult> generate_code_snippet(GenerateParams&) const override;
    NotMyPtr<const InputBinder> runtime_binder() const override final { return m_binder; }
};

template <typename T>
class TypedDefaultConstantInputBinder;

template <typename T>
class MODUS_THREED_EXPORT TypedDefaultConstantInput final : public DefaultConstantInput {
   public:
    TypedDefaultConstantInput(const StringSlice name,
                              NotMyPtr<const TypedDefaultConstantInputBinder<T>> binder)
        : DefaultConstantInput(name, binder) {
        this->m_desc.data_type = DataTypeTrait<T>::data_type;
    }

   private:
};

class DefaultConstantBufferInputBinder;
class MODUS_THREED_EXPORT DefaultConstantBufferInput : public ConstantBufferInput {
   private:
    NotMyPtr<const InputBinder> m_binder;

   public:
    DefaultConstantBufferInput(const StringSlice name,
                               NotMyPtr<const DefaultConstantBufferInputBinder> binder)
        : m_binder(binder) {
        this->m_desc.name = name;
    }

    Result<BindableInputResult> generate_declaration(const u8 slot,
                                                     GenerateParams& params) const override final;
    Result<BindableInputResult> generate_type_snippet(GenerateParams&) const override;
    Result<BindableInputResult> generate_code_snippet(GenerateParams&) const override;
    NotMyPtr<const InputBinder> runtime_binder() const override final { return m_binder; }
};

class DefaultShaderBufferInputBinder;
class MODUS_THREED_EXPORT DefaultShaderBufferInput : public ShaderBufferInput {
   private:
    NotMyPtr<const InputBinder> m_binder;

   public:
    DefaultShaderBufferInput(const StringSlice name,
                             NotMyPtr<const DefaultShaderBufferInputBinder> binder)
        : m_binder(binder) {
        this->m_desc.name = name;
    }

    Result<BindableInputResult> generate_declaration(const u8 slot,
                                                     GenerateParams& params) const override final;
    Result<BindableInputResult> generate_type_snippet(GenerateParams&) const override;
    Result<BindableInputResult> generate_code_snippet(GenerateParams&) const override;
    NotMyPtr<const InputBinder> runtime_binder() const override final { return m_binder; }
};

class DefaultSamplerInputBinder;
class MODUS_THREED_EXPORT DefaultSamplerInput : public SamplerInput {
   private:
    NotMyPtr<const InputBinder> m_binder;

   public:
    DefaultSamplerInput(const StringSlice name, NotMyPtr<const DefaultSamplerInputBinder> binder)
        : m_binder(binder) {
        this->m_desc.name = name;
    }
    Result<BindableInputResult> generate_declaration(const u8 slot,
                                                     GenerateParams& params) const override final;
    Result<BindableInputResult> generate_type_snippet(GenerateParams&) const override;
    Result<BindableInputResult> generate_code_snippet(GenerateParams&) const override;
    NotMyPtr<const InputBinder> runtime_binder() const override final { return m_binder; }
};

template <typename T>
class TypedDefaultConstantInputBinder final : public InputBinder {
   public:
    T m_value;

    TypedDefaultConstantInputBinder() = default;

    TypedDefaultConstantInputBinder(const T& value) : m_value(value) {}

    TypedDefaultConstantInputBinder<T>& operator=(const T& value) {
        m_value = value;
        return *this;
    }

    void bind(Command& command, Device& device, const u32 index) const override {
        auto constant = device.upload_constant(m_value);
        command.inputs.constants[index] = constant;
    }
};

class DefaultConstantBufferInputBinder final : public InputBinder {
   public:
    threed::ConstantBufferHandle m_buffer;
    u32 m_offset = 0;
    u32 m_size = 0;

    DefaultConstantBufferInputBinder() = default;

    DefaultConstantBufferInputBinder(const threed::ConstantBufferHandle buffer)
        : m_buffer(buffer) {}

    DefaultConstantBufferInputBinder& operator=(const threed::ConstantBufferHandle& value) {
        m_buffer = value;
        return *this;
    }

    void bind(Command& command, Device&, const u32 index) const override {
        command.inputs.cbuffers[index].handle = m_buffer;
        command.inputs.cbuffers[index].offset = m_offset;
        command.inputs.cbuffers[index].size = m_size;
    }
};

class DefaultShaderBufferInputBinder final : public InputBinder {
   public:
    threed::ShaderBufferHandle m_buffer;
    u32 m_offset = 0;
    u32 m_size = 0;

    DefaultShaderBufferInputBinder() = default;

    DefaultShaderBufferInputBinder(const threed::ShaderBufferHandle buffer) : m_buffer(buffer) {}

    DefaultShaderBufferInputBinder& operator=(const threed::ShaderBufferHandle& value) {
        m_buffer = value;
        return *this;
    }

    void bind(Command& command, Device&, const u32 index) const override {
        command.inputs.sbuffers[index].handle = m_buffer;
        command.inputs.sbuffers[index].offset = m_offset;
        command.inputs.sbuffers[index].size = m_size;
    }
};

class DefaultSamplerInputBinder final : public InputBinder {
   public:
    threed::TextureHandle m_texture;
    threed::SamplerHandle m_sampler;

    DefaultSamplerInputBinder() = default;

    DefaultSamplerInputBinder(const threed::TextureHandle texture,
                              const threed::SamplerHandle sampler)
        : m_texture(texture), m_sampler(sampler) {}

    DefaultSamplerInputBinder& operator=(const threed::TextureHandle& value) {
        m_texture = value;
        return *this;
    }

    DefaultSamplerInputBinder& operator=(const threed::SamplerHandle& value) {
        m_sampler = value;
        return *this;
    }

    void bind(Command& command, Device&, const u32 index) const override {
        command.inputs.samplers[index] = {m_texture, m_sampler};
    }
};
}    // namespace modus::threed::dynamic
