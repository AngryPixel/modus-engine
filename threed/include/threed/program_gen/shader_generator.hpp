/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

#include <core/io/byte_stream.hpp>

namespace modus {
class IByteWriter;
}

namespace modus::threed::dynamic {

struct ConstantInputDesc;
struct ConstantBufferInputDesc;
struct ShaderBufferInputDesc;
struct SamplerInputDesc;
struct VaryingDesc;
struct VertexInput;
struct FragmentOutput;
struct GenerateParams;
struct ProgramGenParams;
struct ProgramGeneratorCondition;
enum class ShaderStage : u8;
enum class PrecisionQualifier : u8;

class MODUS_THREED_EXPORT ShaderGenerator {
   public:
    using WriteResult = IOResult<usize>;
    virtual ~ShaderGenerator() = default;

    /// Validate the program generation params
    virtual Result<void, String> validate(const ProgramGenParams&) const = 0;

    virtual PrecisionQualifier default_precision_qualifier() const = 0;

    // Writing methods
    virtual WriteResult write_header(IByteWriter& writer, const ShaderStage) const = 0;
    virtual WriteResult write_input_header(IByteWriter& writer) const = 0;
    virtual WriteResult write_snippet_header(IByteWriter& writer) const = 0;
    virtual WriteResult write_types_header(IByteWriter& writer) const = 0;
    virtual WriteResult write_vertex_input_header(IByteWriter&) const = 0;
    virtual WriteResult write_fragment_output_header(IByteWriter&) const = 0;
    virtual WriteResult write_main(IByteWriter& writer, const StringSlice main) const = 0;
    virtual WriteResult write(IByteWriter& writer, const Slice<StringSlice> conditions) const = 0;
    virtual WriteResult write(IByteWriter& writer,
                              const u8 slot,
                              const ConstantInputDesc& desc) const = 0;
    virtual WriteResult write(IByteWriter& writer,
                              const u8 slot,
                              const ConstantBufferInputDesc& desc) const = 0;
    virtual WriteResult write(IByteWriter& writer,
                              const u8 slot,
                              const ShaderBufferInputDesc& desc) const = 0;
    virtual WriteResult write(IByteWriter& writer,
                              const u8 slot,
                              const SamplerInputDesc& desc) const = 0;
    virtual WriteResult write(IByteWriter& writer, const FragmentOutput& desc) const = 0;
    virtual WriteResult write(IByteWriter& writer, const VertexInput& desc) const = 0;
    virtual WriteResult write(IByteWriter& writer,
                              const Slice<VaryingDesc> desc,
                              const ShaderStage stage) const = 0;
};

class MODUS_THREED_EXPORT NullShaderGenerator final : public ShaderGenerator {
   public:
    /// Validate the program generation params
    Result<void, String> validate(const ProgramGenParams&) const override;

    PrecisionQualifier default_precision_qualifier() const override;

    // Writing methods
    WriteResult write_header(IByteWriter& writer, const ShaderStage) const override;
    WriteResult write_input_header(IByteWriter& writer) const override;
    WriteResult write_snippet_header(IByteWriter& writer) const override;
    WriteResult write_types_header(IByteWriter& writer) const override;
    WriteResult write_vertex_input_header(IByteWriter&) const override;
    WriteResult write_fragment_output_header(IByteWriter&) const override;
    WriteResult write_main(IByteWriter& writer, const StringSlice main) const override;
    WriteResult write(IByteWriter& writer, const Slice<StringSlice> conditions) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const ConstantInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const ConstantBufferInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const ShaderBufferInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const u8 slot,
                      const SamplerInputDesc& desc) const override;
    WriteResult write(IByteWriter& writer, const FragmentOutput& desc) const override;
    WriteResult write(IByteWriter& writer, const VertexInput& desc) const override;
    WriteResult write(IByteWriter& writer,
                      const Slice<VaryingDesc> desc,
                      const ShaderStage stage) const override;
};

}    // namespace modus::threed::dynamic
