/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/fixed_stack_string.hpp>
#include <core/fixed_vector.hpp>
#include <threed/program.hpp>
#include <threed/states.hpp>
#include <threed/types.hpp>

namespace modus {
class IByteWriter;
}

namespace modus::threed {
struct Command;
class Device;
}    // namespace modus::threed
namespace modus::threed::dynamic {
constexpr usize kMaxNameLen = 64;
using StringType = FixedStackString<kMaxNameLen>;

enum class PrecisionQualifier : u8 {
    High,
    Medium,
    Low,
    Default,
};

enum class SamplerType : u8 {
    ST2D,
    ST2DArray,
    STCubeMap,
    STShadow,
};

enum class SamplerDataType : u8 {
    Int,
    UInt,
    Float,
};

enum class ShaderStage : u8 {
    Vertex = 1 << 0,
    Fragment = 1 << 1,
};

struct MODUS_THREED_EXPORT ConstantInputDesc {
    StringType name;
    DataType data_type;
    PrecisionQualifier pqualifier = PrecisionQualifier::Default;
};

struct MODUS_THREED_EXPORT BufferMemberDesc {
    enum class Type : u8 {
        Data,
        Custom,
    };
    StringType name;
    DataType data_type;
    PrecisionQualifier pqualifier = PrecisionQualifier::Default;
    Type type = Type::Data;
    StringType custom_type_str = StringType();
};

struct MODUS_THREED_EXPORT ConstantBufferInputDesc {
    StringType name;
    Vector<BufferMemberDesc> buffer_members;
};

struct MODUS_THREED_EXPORT ShaderBufferInputDesc {
    enum class Access : u8 { Read, Write, ReadWrite };

    StringType name;
    Vector<BufferMemberDesc> buffer_members;
    Access access = Access::Read;
};

struct MODUS_THREED_EXPORT SamplerInputDesc {
    StringType name;
    SamplerType stype = SamplerType::ST2D;
    SamplerDataType sdtype = SamplerDataType::Float;
    PrecisionQualifier pqualifier = PrecisionQualifier::Default;
};

struct MODUS_THREED_EXPORT VertexInput {
    StringType name;
    DataType data_type;
    u8 slot;
};

struct MODUS_THREED_EXPORT FragmentOutput {
    StringType name;
    DataType data_type;
    u8 slot;
    static FragmentOutput create_default();
};

struct MODUS_THREED_EXPORT VaryingDesc {
    enum class Type : u8 { Smooth, Flat };
    StringType name;
    DataType data_type;
    Type type = Type::Smooth;
    PrecisionQualifier pqualifier = PrecisionQualifier::Default;
};

class ShaderGenerator;
struct MODUS_THREED_EXPORT GenerateParams {
    NotMyPtr<IByteWriter> writer;
    NotMyPtr<const ShaderGenerator> generator;
    ShaderStage stage;
    PrecisionQualifier default_precision_qualifier;
    bool instanced;
};

/// Bind an input resource to a given index in the command
class MODUS_THREED_EXPORT InputBinder {
   public:
    virtual ~InputBinder() = default;
    virtual void bind(Command& command, Device& device, const u32 index) const = 0;
};

enum class BindableInputResult { Generated, Ignored };

class MODUS_THREED_EXPORT BindableInput {
   public:
    virtual ~BindableInput() = default;

    virtual Result<BindableInputResult> generate_declaration(const u8 slot,
                                                             GenerateParams& params) const = 0;

    virtual Result<BindableInputResult> generate_type_snippet(GenerateParams& params) const = 0;

    virtual Result<BindableInputResult> generate_code_snippet(GenerateParams& params) const = 0;

    virtual NotMyPtr<const InputBinder> runtime_binder() const = 0;
};

class MODUS_THREED_EXPORT ConstantInput : public BindableInput {
   public:
    ConstantInputDesc m_desc;
};

class MODUS_THREED_EXPORT ConstantBufferInput : public BindableInput {
   public:
    ConstantBufferInputDesc m_desc;
};

class MODUS_THREED_EXPORT ShaderBufferInput : public BindableInput {
   public:
    ShaderBufferInputDesc m_desc;
};

class MODUS_THREED_EXPORT SamplerInput : public BindableInput {
   public:
    SamplerInputDesc m_desc;
};

static constexpr u32 kMaxConstantInputs = 8;
static constexpr u32 kMaxShaderBufferInputs = 8;
static constexpr u32 kMaxConstantBufferInputs = 8;
static constexpr u32 kMaxSamplerInputs = 8;
static constexpr u32 kMaxVertexInputs = 16;
static constexpr u32 kMaxFragmentOutputs = 8;
static constexpr u32 kMaxVaryingDesc = 8;

struct MODUS_THREED_EXPORT ShaderStageCommon {
    FixedVector<NotMyPtr<const ConstantInput>, kMaxConstantInputs> constants;
    FixedVector<NotMyPtr<const ConstantBufferInput>, kMaxConstantBufferInputs> constant_buffers;
    FixedVector<NotMyPtr<const ShaderBufferInput>, kMaxShaderBufferInputs> shader_buffers;
    FixedVector<NotMyPtr<const SamplerInput>, kMaxSamplerInputs> samplers;
};

struct MODUS_THREED_EXPORT ShaderVertexStage : public ShaderStageCommon {
    FixedVector<VertexInput, kMaxVertexInputs> inputs;
    StringSlice main;
};

struct MODUS_THREED_EXPORT ShaderFragmentStage : public ShaderStageCommon {
    FixedVector<FragmentOutput, kMaxFragmentOutputs> fragment_outputs;
    StringSlice main;
};

struct MODUS_THREED_EXPORT ProgramGenParams {
    FixedVector<VaryingDesc, kMaxVaryingDesc> varying;
    ShaderVertexStage vertex_stage;
    ShaderFragmentStage fragment_stage;
    bool skip_instanced_variant = true;
};

struct MODUS_THREED_EXPORT RuntimeBinders {
    static constexpr u8 kMaxSamplerCount = kMaxProgramSamplerInputs;
    static constexpr u8 kMaxConstantCount = kMaxProgramConstantInputs;
    FixedVector<NotMyPtr<const InputBinder>, kMaxConstantCount> constants;
    FixedVector<NotMyPtr<const InputBinder>, kMaxConstantCount> cbuffers;
    FixedVector<NotMyPtr<const InputBinder>, kMaxConstantCount> sbuffers;
    FixedVector<NotMyPtr<const InputBinder>, kMaxSamplerCount> samplers;
    void bind(threed::Command& command, threed::Device& device) const;
};

struct MODUS_THREED_EXPORT ProgramGeneratorResult {
    ProgramCreateParams create_params;
    RuntimeBinders binders;
};

struct MODUS_THREED_EXPORT ProgramGeneratorCondition {
    StringType name;
    ShaderStage stage;
    bool use_when_instancing = true;
    bool enabled = false;
};

class MODUS_THREED_EXPORT ProgramGenerator {
   private:
    struct Variant;
    Vector<Variant> m_variants;

   public:
    ProgramGenerator();
    ~ProgramGenerator();

    MODUS_CLASS_DISABLE_COPY_MOVE(ProgramGenerator);

    void reset();

    Result<usize, String> generate(
        const ProgramGenParams& params,
        const ShaderGenerator& generator,
        const Slice<ProgramGeneratorCondition> conditions = Slice<ProgramGeneratorCondition>());

    Result<NotMyPtr<const ProgramGeneratorResult>> result(const usize index) const;

   private:
    struct Options {
        ShaderStage stage;
        bool instanced;
        Slice<ProgramGeneratorCondition> conditions;
    };

    Result<void, String> generate(Variant& variant,
                                  const Options& options,
                                  const ProgramGenParams& params,
                                  const ShaderGenerator& generator);
};

}    // namespace modus::threed::dynamic
