/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/program.hpp>
#include <threed/states.hpp>
#include <threed/threed.hpp>

namespace modus::threed {

enum class DrawType : u8 {
    Default,
    CustomRange,
    CustomPrimitive,
    Custom,
};

static constexpr u32 kMaxInstanceAttributes = 4;
struct MODUS_THREED_EXPORT InstanceAttribute {
    BufferHandle buffer;
    u32 stride = 0;
    u32 offset = 0;
    DataType data_type;
    u32 slot = 0;
    bool normalized = false;
};

struct MODUS_THREED_EXPORT Command {
    ScissorState scissor;
    ProgramHandle program;
    EffectHandle effect;
    struct {
        struct {
            TextureHandle texture;
            SamplerHandle sampler;
        } samplers[kMaxProgramSamplerInputs];
        ConstantHandle constants[kMaxProgramConstantInputs];
        struct {
            ConstantBufferHandle handle;
            u32 offset = 0;
            u32 size = 0;
        } cbuffers[kMaxProgramConstantInputs];
        struct {
            ShaderBufferHandle handle;
            u32 offset = 0;
            u32 size = 0;
        } sbuffers[kMaxProgramConstantInputs];
    } inputs;
    DrawableHandle drawable;
    DrawableUpdateHandle drawable_update;
    struct {
        u32 range_start = 0;
        u32 range_count = 0;
        u32 instance_count = 0;
        DrawType type = DrawType::Default;
        Primitive primitive;
    } draw;
};

struct MODUS_THREED_EXPORT PassFramebufferCopy {
    struct {
        FramebufferHandle handle;
        u32 x = 0;
        u32 y = 0;
        u32 width = 0;
        u32 height = 0;
    } other;
    struct {
        u32 x = 0;
        u32 y = 0;
        u32 width = 0;
        u32 height = 0;
    } pass;
    bool fast = true;
    bool copy_depth = false;
    bool copy_stencil = false;
    bool copy_color = true;
};

enum PassFramebufferColorTarget {
    kFramebufferColorTarget0 = 1 << 0,
    kFramebufferColorTarget1 = 1 << 1,
    kFramebufferColorTarget2 = 1 << 2,
    kFramebufferColorTarget3 = 1 << 3,
    kFramebufferColorTarget4 = 1 << 4,
    kFramebufferColorTarget5 = 1 << 5,
    kFramebufferColorTarget6 = 1 << 6,
    kFramebufferColorTarget7 = 1 << 7,
};

static constexpr u8 kMaxFrambufferColorTargets = 8;

struct MODUS_THREED_EXPORT Pass {
    FixedStackString<32> name;
    FramebufferHandle frame_buffer;
    struct {
        ClearState clear;
        ScissorState scissor;
        RasterState state;
    } state;
    struct {
        u32 x = 0;
        u32 y = 0;
        u32 width = 0;
        u32 height = 0;
    } viewport;
    struct {
        Optional<PassFramebufferCopy> pre_pass;
        Optional<PassFramebufferCopy> post_pass;
    } framebuffer_copy;
    u8 color_targets = kFramebufferColorTarget0;
};

struct MODUS_THREED_EXPORT Pipeline {
    FixedStackString<32> name;
};

using PipelinePtr = NotMyPtr<Pipeline>;

}    // namespace modus::threed
