/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/handle_pool_shared.hpp>
#include <core/strong_type.hpp>

#define MODUS_DECLARE_THREED_HANDLE(Name) \
    MODUS_DECLARE_HANDLE_TYPE(Name, std::numeric_limits<u32>::max())

namespace modus::threed {

MODUS_DECLARE_THREED_HANDLE(RenderTargetHandle);
MODUS_DECLARE_THREED_HANDLE(ShaderHandle);
MODUS_DECLARE_THREED_HANDLE(BufferHandle);
MODUS_DECLARE_THREED_HANDLE(ConstantBufferHandle);
MODUS_DECLARE_THREED_HANDLE(ShaderBufferHandle);
MODUS_DECLARE_THREED_HANDLE(ConstantHandle);
MODUS_DECLARE_THREED_HANDLE(TextureHandle);
MODUS_DECLARE_THREED_HANDLE(ProgramHandle);
MODUS_DECLARE_THREED_HANDLE(EffectHandle);
MODUS_DECLARE_THREED_HANDLE(FramebufferHandle);
MODUS_DECLARE_THREED_HANDLE(DrawableHandle);
MODUS_DECLARE_THREED_HANDLE(DrawableUpdateHandle);
MODUS_DECLARE_THREED_HANDLE(SamplerHandle);

static constexpr usize kConstantBufferDataAlignment = 16;

enum class DataType : u8 {
    I8 = 0,
    U8,
    I16,
    U16,
    I32,
    U32,
    F32,
    Vec2I8,
    Vec2U8,
    Vec2I16,
    Vec2U16,
    Vec2I32,
    Vec2U32,
    Vec2F32,
    Vec3I8,
    Vec3U8,
    Vec3I16,
    Vec3U16,
    Vec3I32,
    Vec3U32,
    Vec3F32,
    Vec4I8,
    Vec4U8,
    Vec4I16,
    Vec4U16,
    Vec4I32,
    Vec4U32,
    Vec4F32,
    Mat2F32,
    Mat3F32,
    Mat4F32,
    U32_2_3x10_REV,
    I32_2_3x10_REV,
    Total
};

enum class TextureType : u8 { T2D, T2DArray, TCubeMap, T3D, T2DMultiSample, Total };

enum class TextureFormat : u8 {
    R8G8B8A8 = 0,
    R8G8B8,
    R5G6B5,
    R4G4B4A4,
    R5G5B5A1,
    Depth24,
    Depth32F,
    Depth24Stencil8,
    RGBA_ASTC4x4,
    RGBA_ASTC5x4,
    RGBA_ASTC5x5,
    RGBA_ASTC6x5,
    RGBA_ASTC6x6,
    RGBA_ASTC8x5,
    RGBA_ASTC8x6,
    RGBA_ASTC8x8,
    RGBA_ASTC10x5,
    RGBA_ASTC10x6,
    RGBA_ASTC10x8,
    RGBA_ASTC10x10,
    RGBA_ASTC12x10,
    RGBA_ASTC12x12,
    R8,
    RGBA_ASTC4x4_srgb,
    RGBA_ASTC5x4_srgb,
    RGBA_ASTC5x5_srgb,
    RGBA_ASTC6x5_srgb,
    RGBA_ASTC6x6_srgb,
    RGBA_ASTC8x5_srgb,
    RGBA_ASTC8x6_srgb,
    RGBA_ASTC8x8_srgb,
    RGBA_ASTC10x5_srgb,
    RGBA_ASTC10x6_srgb,
    RGBA_ASTC10x8_srgb,
    RGBA_ASTC10x10_srgb,
    RGBA_ASTC12x10_srgb,
    RGBA_ASTC12x12_srgb,
    R8G8B8_srgb,
    R8G8B8A8_srgb,
    R_EAC11,
    R_EAC11_SIGNED,
    RG_EAC11,
    RG_EAC11_SIGNED,
    RGB_ETC2,
    RGB_ETC2_srgb,
    RGBA_ETC2_A1,
    RGBA_ETC2_A1_srgb,
    RGBA_ETC2,
    RGBA_ETC2_srgb,
    R10G10B10A2_I32,
    R10G10B10A2_U32,
    R16G16B16_F16,
    R16G16B16A_F16,
    R16G16_F16,
    R16_U16,
    R32G32_U32,
    R16G16_U16,
    Depth16F,
    Total
};

enum class TextureCubeMapFace : u8 { Left = 0, Right, Top, Bottom, Front, Back };

enum class Primitive : u8 {
    Points = 0,
    Lines,
    LineStrip,
    LineLoop,
    Triangles,
    TriangleStrip,
    TriangleFan,
    Total
};

enum class IndicesType : u8 { None, U8, U16, U32, Total };

}    // namespace modus::threed

#undef MODUS_DECLARE_THREED_HANDLE
