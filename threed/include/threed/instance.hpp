/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::threed {

class Device;

namespace dynamic {
class ShaderGenerator;
}

enum class InstanceError { ApiSetup, ApiNotSupported, ApiMissingFeatures, Context, Unknown };

struct ApiVersion {
    Api api;
    u32 major;
    u32 minor;
    u32 patch;
};

class MODUS_THREED_EXPORT IContext {
   public:
    virtual ~IContext() = default;

    virtual Result<void, String> bind_to_thread() = 0;
};

class MODUS_THREED_EXPORT NullContext final : public IContext {
   public:
    Result<void, String> bind_to_thread() override;
};

using InstanceResult = Result<void, InstanceError>;

class InstanceGLES3;
class MODUS_THREED_EXPORT Instance {
   public:
    virtual ~Instance() = default;

    virtual InstanceResult initialize() = 0;

    virtual Optional<FramebufferHandle> default_framebuffer() = 0;

    virtual void shutdown() = 0;

    [[nodiscard]] virtual std::unique_ptr<Device> create_device() = 0;

    virtual ApiVersion api() const = 0;
};

MODUS_THREED_EXPORT [[nodiscard]] std::unique_ptr<Instance> create_gl_instance();

MODUS_THREED_EXPORT [[nodiscard]] std::unique_ptr<Instance> create_null_instance();

}    // namespace modus::threed
