/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/compositor.hpp>
#include <threed/threed.hpp>

namespace modus::threed {

class Device;

struct FramebufferCompositorCreateParams {
    u32 width = 0;
    u32 height = 0;
    TextureFormat colour_format = TextureFormat::Total;
    TextureFormat depth_format = TextureFormat::Total;
    u8 sample_count = 1;
};

class MODUS_THREED_EXPORT FramebufferCompositor final : public Compositor {
   private:
    static constexpr u32 kMaxBufferCount = 2;

    struct Resource {
        TextureHandle tex_colour;
        TextureHandle tex_depth;
        FramebufferHandle framebuffer;
    };
    Resource m_resources[kMaxBufferCount];
    u32 m_buffer_index = 0;
    u32 m_buffer_count = 0;
    FramebufferCompositorCreateParams m_params;

   public:
    Result<> initialize(const FramebufferCompositorCreateParams& params,
                        Device& device,
                        const u32 buffer_count);

    void shutdown(Device& device);

    Result<void, String> update(Device& device) override;

    void swap_buffers() override;

    void set_dimensions(const u32 width, const u32 height);

    TextureHandle color_texture() const { return m_resources[m_buffer_index].tex_colour; }

   private:
    Result<Resource, String> create_framebuffer(Device& device);

    void destroy_resources(Device& device);
};

}    // namespace modus::threed
