/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

#include <core/fixed_stack_string.hpp>
#include <core/fixed_vector.hpp>

namespace modus::threed {

static constexpr u8 kMaxProgramDataInputs = 8;
static constexpr u8 kMaxProgramConstantInputs = 8;
static constexpr u8 kMaxProgramSamplerInputs = 8;

struct MODUS_THREED_EXPORT ProgramSamplerInputParam {
    static constexpr usize kMaxNameLength = 64;
    FixedStackString<kMaxNameLength> name;
};

enum class ProgramContanstInputType : u8 { Buffer, Constant };

struct MODUS_THREED_EXPORT ProgramConstantInputParam {
    static constexpr usize kMaxNameLength = 64;
    FixedStackString<kMaxNameLength> name;
    DataType data_type = DataType::F32;
};

struct MODUS_THREED_EXPORT ProgramCBufferInputParam {
    static constexpr usize kMaxNameLength = 64;
    FixedStackString<kMaxNameLength> name;
};

struct MODUS_THREED_EXPORT ProgramCreateParams {
    ByteSlice vertex;
    ByteSlice fragment;
    FixedVector<ProgramConstantInputParam, kMaxProgramConstantInputs> constants;
    FixedVector<ProgramCBufferInputParam, kMaxProgramConstantInputs> cbuffers;
    FixedVector<ProgramSamplerInputParam, kMaxProgramSamplerInputs> samplers;
};

struct MODUS_THREED_EXPORT ProgramReflection {
    static constexpr usize kMaxUniforms = 8;
    static constexpr usize kMaxNameLength = 64;

    struct Constant {
        FixedStackString<kMaxNameLength> name;
        DataType data_type;
        u32 location_hint;
    };

    struct Sampler {
        FixedStackString<kMaxNameLength> name;
        TextureType type;
        u32 location_hint;
    };

    struct Buffer {
        FixedStackString<kMaxNameLength> name;
    };

    FixedVector<Constant, kMaxUniforms> constants;
    FixedVector<Buffer, kMaxUniforms> buffers;
    FixedVector<Sampler, kMaxUniforms> samplers;
};

}    // namespace modus::threed
