/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::threed {

// #### General      #####################################################

static constexpr usize kMaxRenderTargetCount = 8;
static constexpr usize kMaxBoundTextures = 8;

enum class CompareFunction : u8 {
    Never = 0,
    Always,
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    Total
};

// #### Depth Stencil #####################################################

enum class StencilOp : u8 {
    Keep = 0,
    Zero,
    Replace,
    Increment,
    IncrementWrap,
    Decrement,
    DecrementWrap,
    Invert,
    Total
};

struct MODUS_THREED_EXPORT StencilOperation {
    StencilOp pass = StencilOp::Keep;
    StencilOp fail = StencilOp::Keep;
    StencilOp fail_depth = StencilOp::Keep;
    CompareFunction compare = CompareFunction::Always;
};

struct MODUS_THREED_EXPORT DepthStencilState {
    struct {
        bool enabled = true;
        bool mask = true;
        CompareFunction function = CompareFunction::Less;
    } depth;

    struct {
        bool enabled = false;
        u8 ref = 255;
        u8 mask = 255;
        StencilOperation front;
        StencilOperation back;
    } stencil;
};

// #### Blend State #####################################################

enum class BlendEquation : u8 {
    Zero = 0,
    One,
    SourceColor,
    OneMinusSourceColor,
    SourceAlpha,
    OneMinusSourceAlpha,
    DestinationColor,
    OneMinusDestinationColor,
    DestinationAlpha,
    OneMinusDestinationAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SourceAlphaSaturate,
    Total
};

enum class BlendOperation : u8 { Add = 0, Subtract, SubtractReverse, Min, Max, Total };

struct MODUS_THREED_EXPORT BlendState {
    bool aplha_to_coverage = false;
    bool independent_blending = false;
    struct {
        bool enabled = false;
        BlendEquation eq_source = BlendEquation::One;
        BlendEquation eq_destination = BlendEquation::Zero;
        BlendOperation op_colour = BlendOperation::Add;
        BlendEquation eq_source_alpha = BlendEquation::One;
        BlendEquation eq_destination_alpha = BlendEquation::Zero;
        BlendOperation op_alpha = BlendOperation::Add;
        struct {
            u8 r : 1;
            u8 g : 1;
            u8 b : 1;
            u8 a : 1;
        } mask;
    } targets[kMaxRenderTargetCount];
};

// #### Sampler State #####################################################

enum class TextureFilter : u8 {
    Nearest = 0,
    Linear,
    NearestMipMapNearest,
    NearestMipMapLinear,
    LinearMipMapLinear,
    LinearMipMapNearest,
    Total
};

enum class TextureWrapMode : u8 { ClampToEdge = 0, MirroredRepeat, Repeat, Total };

// #### Raster State #####################################################

enum class CullMode : u8 { Front = 0, Back, FrontAndBack, Total };

struct MODUS_THREED_EXPORT RasterState {
    bool cull_enabled = true;
    bool face_counter_clockwise = true;
    CullMode cull_mode = CullMode::Back;
};

// ### Clear State ########################################################

struct MODUS_THREED_EXPORT ClearState {
    struct {
        glm::vec4 colour = glm::vec4(1.f);
        bool clear = false;
    } colour[kMaxRenderTargetCount];
    f32 depth = 1.0f;
    u8 stencil = 0;
    bool clear_depth = false;
    bool clear_stencil = false;
};

// ### Draw State #########################################################

struct MODUS_THREED_EXPORT ScissorState {
    u32 x = 0;
    u32 y = 0;
    u32 width = 0;
    u32 height = 0;
    bool enabled = false;
};

}    // namespace modus::threed
