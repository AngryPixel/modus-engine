/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::threed {

enum class BufferType : u8 { Data, Indices, Total };

enum class BufferUsage : u8 { Static, Dynamic, Stream, Total };

struct MODUS_THREED_EXPORT BufferCreateParams {
    usize size = 0;
    BufferType type = BufferType::Data;
    BufferUsage usage = BufferUsage::Static;
    const void* data = nullptr;
};

struct MODUS_THREED_EXPORT ConstantBufferCreateParams {
    usize size = 0;
    BufferUsage usage = BufferUsage::Static;
    const void* data = nullptr;
};

struct MODUS_THREED_EXPORT ShaderBufferCreateParams {
    usize size = 0;
    BufferUsage usage = BufferUsage::Static;
    const void* data = nullptr;
};

struct MODUS_THREED_EXPORT BufferMapParams {
    u32 start = 0;
    u32 size = 0;
    bool read = false;
    bool write = false;
    bool invalidate = false;
};

}    // namespace modus::threed
