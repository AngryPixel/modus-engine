/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/string_slice.hpp>
#include <threed/threed.hpp>

namespace modus::threed {

struct MODUS_THREED_EXPORT TextureCreateParams {
    u32 width = 0;
    u32 height = 0;
    u32 depth = 1;
    u8 mip_map_levels = 1;
    u8 sample_count = 1;
    TextureType type = TextureType::T2D;
    TextureFormat format = TextureFormat::R8G8B8A8;
    Optional<glm::vec4> border_color;
};

struct MODUS_THREED_EXPORT TextureData {
    ByteSlice data = ByteSlice();
    u32 width = 0;
    u32 height = 0;
    u32 depth = 1;
    u8 mip_map_level = 0;
    u8 unpack_alignment = 4;
};

struct MODUS_THREED_EXPORT TextureDataCubeMap {
    TextureData right;
    TextureData left;
    TextureData top;
    TextureData bottom;
    TextureData front;
    TextureData back;
};

struct TextureFormatDesc {
    TextureFormat format;
    u8 bytes_per_pixel;
    bool normalized;
    bool compressed;
    bool srgb;
    u8 n_channels;
    u8 n_bits_red;
    u8 n_bits_green;
    u8 n_bits_blue;
    u8 n_bits_alpha;
    u8 n_bits_stencil;
    u8 n_bits_depth;
    u8 block_size;
    u8 block_width;
    u8 block_height;
    u8 block_depth;
};

MODUS_THREED_EXPORT StringSlice texture_format_to_str(const TextureFormat format);

MODUS_THREED_EXPORT StringSlice texture_type_to_str(const TextureType type);

MODUS_THREED_EXPORT const TextureFormatDesc& texture_format_desc(const TextureFormat format);

}    // namespace modus::threed
