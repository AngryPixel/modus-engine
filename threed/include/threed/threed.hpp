/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <core/core.hpp>

// When not using pch, include the pch header here to ensure code completion engines based on
// clangd can find all the required headers.
#if !defined(MODUS_USE_PCH)
#include <threed/threed_pch.h>
#endif

#include <threed/module_config.hpp>
#include <threed/types.hpp>

namespace modus::threed {

using ThreedAllocator = DefaultAllocator;
template <typename T>
using Vector = modus::Vector<T, ThreedAllocator>;

enum class Api { OpenGLES3, Vulkan, Null };

MODUS_THREED_EXPORT Api default_api();

}    // namespace modus::threed
