/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus::threed {

enum class FramebufferError : u8 {
    InvalidDepthComponent,
    InvalidStencilComponent,
    DuplicateStencilComponent,
    InvalidColourComponent,
    MissingColourComponent,
    Incomplete,
    IncompleteMultisample,
    Unsupported,
    MaxCount,
    Unknown
};

/// Framenbuffer texture attachment
/// If texture is of type:
///     CubeMap : index == TextureCubeMapFace
///     2D Array: index == array index
///     3D      : index == depth
struct MODUS_THREED_EXPORT FramebufferTexture {
    TextureHandle handle;
    u32 index = 0;
    u8 mip_map_level = 0;
    u8 sample_count = 1;
};

struct MODUS_THREED_EXPORT FramebufferCreateParams {
    static constexpr u32 kMaxColourComponentCount = 8;
    FramebufferTexture depth;
    FramebufferTexture stencil;
    FramebufferTexture colour[kMaxColourComponentCount];
};

struct MODUS_THREED_EXPORT FramebufferCopyParams {
    struct {
        FramebufferHandle handle;
        u32 x = 0;
        u32 y = 0;
        u32 width = 0;
        u32 height = 0;
    } from;
    struct {
        FramebufferHandle handle;
        u32 x = 0;
        u32 y = 0;
        u32 width = 0;
        u32 height = 0;
    } to;
    bool fast = true;
    bool copy_depth = false;
    bool copy_stencil = false;
    bool copy_color = true;
};

}    // namespace modus::threed
