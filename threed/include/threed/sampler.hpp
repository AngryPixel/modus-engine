/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::threed {

enum class SamplerFilter : u8 {
    Nearest = 0,
    Linear,
    NearestMipMapNearest,
    NearestMipMapLinear,
    LinearMipMapLinear,
    LinearMipMapNearest,
    Total
};

enum class SamplerWrapMode : u8 { ClampToEdge = 0, MirroredRepeat, Repeat, Total };

struct MODUS_THREED_EXPORT SamplerCreateParams {
    SamplerFilter filter_min = SamplerFilter::NearestMipMapLinear;
    SamplerFilter filter_mag = SamplerFilter::Linear;
    SamplerWrapMode wrap_s = SamplerWrapMode::ClampToEdge;
    SamplerWrapMode wrap_r = SamplerWrapMode::ClampToEdge;
    SamplerWrapMode wrap_t = SamplerWrapMode::ClampToEdge;
};

}    // namespace modus::threed
