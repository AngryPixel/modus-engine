/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

namespace modus::threed {

class Device;

/// Interface for compositors (Swapchain, System/App fbo)
class MODUS_THREED_EXPORT Compositor {
   protected:
    i32 m_current_width = 0;
    i32 m_current_height = 0;
    i32 m_new_width = 0;
    i32 m_new_height = 0;
    bool m_vsync = true;
    bool m_new_vsync = true;
    bool m_requires_update = false;
    FramebufferHandle m_frame_buffer;

   protected:
    Compositor() = default;

   public:
    virtual ~Compositor() {}

    inline i32 width() const { return m_current_width; }

    inline i32 height() const { return m_current_height; }

    inline bool requires_update() const { return m_requires_update; }

    inline bool vsync_enabled() const { return m_vsync; }

    FramebufferHandle frame_buffer() const { return m_frame_buffer; }

    inline void set_vsync(const bool value) {
        m_new_vsync = value;
        m_requires_update = true;
    }

    // Apply any pending updates to this compositor
    virtual Result<void, String> update(Device& device) = 0;

    // Swap buffers, commit current frame
    virtual void swap_buffers() = 0;
};

class MODUS_THREED_EXPORT NullCompositor final : public Compositor {
   public:
    NullCompositor();

    Result<void, String> update(Device& device) override;

    void swap_buffers() override;
};

}    // namespace modus::threed
