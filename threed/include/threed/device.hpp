/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <threed/threed.hpp>

#include <threed/pipeline.hpp>

namespace modus::threed {

namespace dynamic {
class ShaderGenerator;
}
class Instance;
class Compositor;
class Framebuffer;

struct ProgramCreateParams;
struct FramebufferCreateParams;
struct BufferCreateParams;
struct ConstantBufferCreateParams;
struct ShaderBufferCreateParams;
struct EffectCreateParams;
struct TextureCreateParams;
struct TextureData;
struct TextureDataCubeMap;
struct FramebufferCopyParams;
struct DrawableCreateParams;
struct DrawableUpdateParams;
struct SamplerCreateParams;

enum class FramebufferError : u8;

struct Command;
struct Pipeline;
struct Pass;

struct MODUS_THREED_EXPORT DeviceLimits {
    u32 min_constant_buffer_size = 0;
    u32 max_constant_buffer_size = 0;
    struct {
        bool astc = false;
        bool dxt1 = false;
        bool etc2 = false;
        bool etc1 = false;
        bool s3tc = false;
        bool s3tc_srgb = false;
    } compressed_texture_support;
    u32 max_texture_size_px = 0;
    u32 shader_buffer_offset_alignment = 1;
    u32 constant_buffer_offset_alignment = 256;
};

class MODUS_THREED_EXPORT Device {
    friend class MappedBufferScope;

   protected:
    Instance& m_instance;

   public:
    Device(Instance& instance);

    virtual ~Device() = default;

    virtual Result<> initialize() = 0;

    virtual void shutdown() = 0;

    virtual const dynamic::ShaderGenerator& shader_generator() const = 0;

    virtual const DeviceLimits& device_limits() const = 0;

    virtual void begin_frame() = 0;

    virtual void end_frame() = 0;

    virtual Result<FramebufferHandle, FramebufferError> create_framebuffer(
        const FramebufferCreateParams& params) = 0;

    virtual void destroy_framebuffer(const FramebufferHandle handle) = 0;

    virtual void framebuffer_copy(const FramebufferCopyParams& params) = 0;

    virtual void execute_pipeline(const Pipeline& pipeline) = 0;

    virtual Result<ProgramHandle, String> create_program(const ProgramCreateParams& params) = 0;

    virtual Result<> reflect_program(const ProgramHandle handle,
                                     ProgramReflection& reflection) const = 0;

    virtual void destroy_program(const ProgramHandle handle) = 0;

    virtual Result<BufferHandle, void> create_buffer(const BufferCreateParams& params) = 0;

    virtual void destroy_buffer(const BufferHandle handle) = 0;

    virtual Result<ConstantBufferHandle, void> create_constant_buffer(
        const ConstantBufferCreateParams& params) = 0;

    virtual void destroy_constant_buffer(const ConstantBufferHandle handle) = 0;

    virtual Result<> update_buffer(const BufferHandle handle,
                                   const ByteSlice data,
                                   const u32 start_offset,
                                   const bool invalidate_buffer) = 0;

    template <typename T>
    Result<> update_constant_buffer(const ConstantBufferHandle handle, const T& value) {
        return update_constant_buffer(handle, &value, sizeof(value));
    }

    template <typename T, usize Size>
    Result<> update_constant_buffer(const ConstantBufferHandle handle, const T (&value)[Size]) {
        return update_constant_buffer(handle, value, sizeof(T) * Size);
    }

    virtual Result<> update_constant_buffer(const ConstantBufferHandle handle,
                                            const void* ptr,
                                            const usize size) = 0;

    virtual Result<ShaderBufferHandle, void> create_shader_buffer(
        const ShaderBufferCreateParams& params) = 0;

    virtual void destroy_shader_buffer(const ShaderBufferHandle handle) = 0;

    template <typename T>
    Result<> update_shader_buffer(const ShaderBufferHandle handle, const T& value) {
        return update_shader_buffer(handle, &value, sizeof(value));
    }

    template <typename T, usize Size>
    Result<> update_shader_buffer(const ShaderBufferHandle handle, const T (&value)[Size]) {
        return update_shader_buffer(handle, value, sizeof(T) * Size);
    }
    virtual Result<> update_shader_buffer(const ShaderBufferHandle handle,
                                          const void* ptr,
                                          const usize size) = 0;

    virtual Result<EffectHandle, String> create_effect(const EffectCreateParams& params) = 0;

    virtual void destroy_effect(const EffectHandle handle) = 0;

    template <typename T>
    ConstantHandle upload_constant(const T& constant) {
        return allocate_constant(&constant, sizeof(T));
    }

    // Create Texture
    virtual Result<TextureHandle, void> create_texture(const TextureCreateParams& params) = 0;

    virtual Result<TextureHandle, void> create_texture(const TextureCreateParams& params,
                                                       Slice<TextureData> data) = 0;

    virtual Result<TextureHandle, void> create_texture(const TextureCreateParams& params,
                                                       Slice<TextureDataCubeMap> data) = 0;

    virtual Result<> update_texture(const TextureHandle h, Slice<TextureData> data) = 0;

    virtual void destroy_texture(const TextureHandle handle) = 0;

    // Create Drawable
    virtual Result<DrawableHandle, String> create_drawable(const DrawableCreateParams& params) = 0;

    virtual void destroy_drawable(const DrawableHandle handle) = 0;

    virtual Result<void, String> update_drawable(const DrawableHandle handle,
                                                 const DrawableUpdateParams& params) = 0;

    // TODO: Probably better to replace this with explicit buffer params in Command
    /// For render time updates of buffer data in a drawable
    virtual Result<DrawableUpdateHandle> allocate_drawable_update(
        const DrawableUpdateParams& params) = 0;

    virtual Result<SamplerHandle, String> create_sampler(const SamplerCreateParams& params) = 0;

    virtual void destroy_sampler(const SamplerHandle handle) = 0;

    virtual Command* allocate_command(Pass& pass) = 0;

    virtual Pass* allocate_pass(Pipeline& pipeline, const StringSlice name) = 0;

    virtual Pipeline* allocate_pipeline(const StringSlice name) = 0;

   private:
    virtual ConstantHandle allocate_constant(const void* ptr, const usize size) = 0;
};

}    // namespace modus::threed
