#
# Parse include statements in glsl files
#

modus_add_tool(modus_glsl_include)

target_sources(modus_glsl_include PRIVATE
    src/glsl_include.cpp
)

target_link_libraries(modus_glsl_include PRIVATE core os modus_stb_include)
