/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <os/os_pch.h>

#include <core/core.hpp>
#include <core/getopt.hpp>
#include <core/io/memory_stream.hpp>
#include <iostream>
#include <os/file.hpp>

#define STB_INCLUDE_IMPLEMENTATION
#define STB_INCLUDE_LINE_GLSL
#include <stb_include.h>

using namespace modus;

static const StringSlice kIntro =
    "modus_glsl_include: Parse include statements in glsl files\n"
    "Usage: modus_glsl_include [options] file";

static const StringSlice kTypeImage = "image";

static Result<String, void> read_file_to_memory(const StringSlice path) {
    auto r_file = os::FileBuilder().read().binary().open(path);
    if (!r_file) {
        return Error<>();
    }

    String str;
    str.resize(r_file.value().size(), '\0');

    SliceMut<char> str_slice(str.data(), str.size());

    auto r_read = r_file.value().read(str_slice.as_bytes());
    if (!r_read || r_read.value() != str_slice.size()) {
        return Error<>();
    }

    return Ok(std::move(str));
}

// Escape paths according to the rules set out in
// https://github.com/llvm/llvm-project/blob/release/9.x/clang/lib/Frontend/DependencyFile.cpp#L233
// "$" replaced by "$$"
// "#" replaced by "\#"
// " " replaced by "\ "
// "\#" replaced by "\\#"
// "\ " replaced by "\\\ "
//

static String escape_dep_file_chars(const StringSlice input) {
    String result;
    result.reserve(input.size());
    for (isize i = 0; i < isize(input.size()); ++i) {
        if (input[i] == '$') {
            result += '$';
        } else if (input[i] == '#') {
            result += '\\';
        } else if (input[i] == ' ') {
            result += '\\';
            isize backwards_it = i - 1;
            while (backwards_it > 0 && input[backwards_it] == '\\') {
                result += '\\';
                --backwards_it;
            }
        }
        result += input[i];
    }
    return result;
}

static Result<> generate_dep_file(const StringSlice path,
                                  const StringSlice input_file,
                                  const modus_dep_list* dep_list) {
    String output_contents = escape_dep_file_chars(modus::format("{}:", input_file));
    output_contents += " \\\n";
    while (dep_list != nullptr) {
        output_contents += "  ";
        output_contents += escape_dep_file_chars(modus::format("{}", dep_list->full_path));
        dep_list = dep_list->next;
        if (dep_list != nullptr) {
            output_contents += " \\\n";
        }
    }

    auto r_file = os::FileBuilder().write().create().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", path);
        return Error<>();
    }

    if (!output_contents.empty()) {
        if (!r_file->write_exactly(StringSlice(output_contents).as_bytes())) {
            std::cerr << fmt::format("Failed to write contents to {}\n", path);
            return Error<>();
        }
    }

    return Ok<>();
}

int main(const int argc, const char** argv) {
    CmdOptionString opt_output(
        "-o", "--output",
        "Output for the generated file. If no output file is specified it will "
        "be generated in the same location as the input file",
        "");

    CmdOptionStringList opt_include_paths("-i", "--include-dirs",
                                          "Include directory(ies) for include resolution.");

    CmdOptionString opt_glsl_version(
        "-g", "--glsl-version", "GLSL Version number string to be injected. Default = '300 es' ",
        "300 es");

    CmdOptionString opt_dep_output_file("", "--dep-output-file",
                                        "Generate ninja compatible depfile", "");
    CmdOptionString opt_dep_file(
        "", "--dep-file", "File for which the rule in dep-output-file should be created", "");

    CmdOptionParser opt_parser;

    opt_parser.add(opt_include_paths).expect("Failed to add option");
    opt_parser.add(opt_output).expect("Failed to add option");
    opt_parser.add(opt_glsl_version).expect("Failed to add option");
    opt_parser.add(opt_dep_output_file).expect("Failed to add option");
    opt_parser.add(opt_dep_file).expect("Failed to add option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (opt_result.value().size() == 0) {
        std::cerr << fmt::format("No input file specified\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    String output_file;
    if (opt_output.parsed()) {
        output_file = opt_output.value_str();
    } else {
        output_file = opt_result->at(0);
        output_file += ".parsed";
    }

    const char* input_file = opt_result->at(0);
    auto r_file = read_file_to_memory(input_file);
    if (!r_file) {
        std::cerr << fmt::format("Failed to load: {}\n", input_file);
        return EXIT_FAILURE;
    }

    String version_str("#version ");
    version_str += opt_glsl_version.value_str();
    version_str += '\n';
    char* strings[] = {const_cast<char*>(version_str.c_str()),
                       const_cast<char*>(r_file.value().c_str())};

    modus_dep_list* dep_list = nullptr;
    const auto include_paths = opt_include_paths.value();

    std::vector<char*> cinclude_paths;
    cinclude_paths.reserve(include_paths.size());
    for (const auto& path : include_paths) {
        cinclude_paths.push_back(const_cast<char*>(path.c_str()));
    }
    char* parsed_string = nullptr;
    char error[256];
    // NOLINTNEXTLINE
    parsed_string =
        stb_include_strings(strings, modus::array_size(strings), nullptr, cinclude_paths.data(),
                            cinclude_paths.size(), const_cast<char*>(input_file), error, &dep_list);
    if (parsed_string == nullptr) {
        std::cerr << fmt::format("Failed to process file: {}\n", error);
        return EXIT_FAILURE;
    }

    if (strlen(parsed_string) == 0) {
        std::cerr << fmt::format("Generated output file is empty: {}\n", output_file);
        free(parsed_string);
        modus_dep_list_free(dep_list);
        return EXIT_FAILURE;
    }

    if (opt_dep_file.parsed()) {
        if (!generate_dep_file(opt_dep_output_file.value(), opt_dep_file.value(), dep_list)) {
            free(parsed_string);
            modus_dep_list_free(dep_list);
            return EXIT_FAILURE;
        }
    }

    auto r_output = os::FileBuilder().write().create().open(output_file);
    if (!r_output) {
        std::cerr << fmt::format("Failed to open output file: {}\n", output_file);
        free(parsed_string);
        modus_dep_list_free(dep_list);
        return EXIT_FAILURE;
    }

    const StringSlice parsed_string_slice(parsed_string);
    auto r_write = r_output.value().write(parsed_string_slice.as_bytes());
    if (!r_write || r_write.value() != parsed_string_slice.size()) {
        std::cerr << fmt::format("Failed to write contents to output file: {}\n", output_file);
        free(parsed_string);
        modus_dep_list_free(dep_list);
        return EXIT_FAILURE;
    }

    free(parsed_string);
    modus_dep_list_free(dep_list);
    return EXIT_SUCCESS;
}
