/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/assets/asset_waiter.hpp>
// clang-format on
#include <engine/assets/asset_loader.hpp>
namespace modus::engine::assets {

void AssetWaiter::reset(String&& asset_path) {
    m_handle = AssetHandleV2();
    m_asset_path = std::move(asset_path);
}

void AssetWaiter::reset(const StringSlice asset_path) {
    m_handle = AssetHandleV2();
    m_asset_path = asset_path.to_str();
}

void AssetWaiter::reset(const AssetHandleV2 handle) {
    m_handle = handle;
    m_asset_path.clear();
}

static AssetWaiterState to_asset_waiter_state(const AssetStateV2 state) {
    switch (state) {
        case AssetStateV2::Error:
            return AssetWaiterState::Error;
        case AssetStateV2::Created:
            return AssetWaiterState::Done;
        default:
            return AssetWaiterState::Waiting;
    }
}

Result<AssetWaiterState> AssetWaiter::update(Engine& engine, AssetLoader& loader) {
    if (!m_handle) {
        modus_assert(!m_asset_path.empty());
        AsyncLoadParams params;
        params.path = m_asset_path;
        auto r = loader.load_async(engine, params);
        if (!r) {
            return Error<>();
        }
        auto [handle, state] = *r;
        m_handle = handle;
        return Ok(to_asset_waiter_state(state));
    } else {
        auto r_state = loader.asset_state(m_handle);
        if (r_state) {
            return Ok(to_asset_waiter_state(*r_state));
        }
    }
    return Error<>();
}

void AssetGroupWaiter::reserve(const size_t size) {
    m_waiters.reserve(size);
}

void AssetGroupWaiter::reset() {
    m_waiters.clear();
}

void AssetGroupWaiter::add(String&& asset_path) {
    m_waiters.emplace_back();
    m_waiters.back().reset(std::forward<String>(asset_path));
}

void AssetGroupWaiter::add(const StringSlice asset_path) {
    m_waiters.emplace_back();
    m_waiters.back().reset(asset_path);
}

void AssetGroupWaiter::add(const AssetHandleV2 handle) {
    m_waiters.emplace_back();
    m_waiters.back().reset(handle);
}

AssetGroupWaiterState AssetGroupWaiter::update(Engine& engine, AssetLoader& loader) {
    AssetGroupWaiterState state;
    state.error_count = 0;
    state.finished_count = 0;
    state.waiting_count = 0;
    for (auto& waiter : m_waiters) {
        auto r = waiter.update(engine, loader);
        if (!r || *r == AssetWaiterState::Error) {
            state.error_count++;
        } else if (*r == AssetWaiterState::Waiting) {
            state.waiting_count++;
        } else {
            state.finished_count++;
        }
    }
    return state;
}
}    // namespace modus::engine::assets
