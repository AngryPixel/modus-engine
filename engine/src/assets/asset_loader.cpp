/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/assets/asset_loader.hpp>
// clang-format on

#include <core/io/memory_stream.hpp>
#include <engine/engine.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_parallel.hpp>
#include <os/path.hpp>
#include <vfs/vfs.hpp>

namespace modus::engine::assets {

StringSlice asset_state_to_string(const AssetStateV2 state) {
    switch (state) {
        case AssetStateV2::Unloaded:
            return "Unloaded";
        case AssetStateV2::Loading:
            return "Loading";
        case AssetStateV2::Waiting:
            return "Wainting on Dependencies";
        case AssetStateV2::Loaded:
            return "Loaded";
        case AssetStateV2::Created:
            return "Created";
        case AssetStateV2::Destroy:
            return "Destroy";
        case AssetStateV2::Error:
            return "Error";
        default:
            return "Unknown";
    }
}
StringSlice asset_error_to_string(const AssetErrorV2 error) {
    switch (error) {
        case AssetErrorV2::None:
            return "None";
        case AssetErrorV2::Load:
            return "An error occured while loading the asset from a byte stream";
        case AssetErrorV2::IO:
            return "An error occured while trying to open a bytes stream";
        case AssetErrorV2::Create:
            return "An error occured while trying to create the asset";
        case AssetErrorV2::Dependency:
            return "An error occured while loading one of the asset's dependencies";
        default:
            return "Unknown";
    }
}

struct AssetLoader::AssetLoaderJob final {
    NotMyPtr<modus::vfs::VirtualFileSystem> m_fs;
    NotMyPtr<AssetLoader::AssetInfo> asset;
    AssetLoaderJob() = default;
    MODUS_CLASS_DEFAULT_MOVE(AssetLoaderJob);
    MODUS_CLASS_DISABLE_COPY(AssetLoaderJob);

    void operator()() {
        MODUS_PROFILE_ASSETS_BLOCK("AssetLoaderJob");
        auto r_path = vfs::VirtualPath::from(asset->file_path);
        if (!r_path) {
            asset->state = AssetStateV2::Error;
            asset->error = AssetErrorV2::IO;
            return;
        }
        auto r_file = m_fs->open_readable(*r_path);
        if (!r_file) {
            asset->state = AssetStateV2::Error;
            asset->error = AssetErrorV2::IO;
            return;
        }

        auto r = asset->data->load(*(*r_file));
        if (!r) {
            asset->state = AssetStateV2::Error;
            asset->error = AssetErrorV2::Load;
            return;
        }

        if (*r == AssetLoadResult::Done) {
            asset->state = AssetStateV2::Loaded;
        } else {
            asset->state = AssetStateV2::Waiting;
        }
        asset->error = AssetErrorV2::None;
    }
};

AssetLoader::AssetLoader() : m_assets(64), m_active_queue(0) {
    for (auto& queue : m_queued_assets) {
        queue.reserve(16);
    }
    m_factories.reserve(8);
    m_path_map.reserve(32);
}

AssetLoader::~AssetLoader() {
    modus_assert(m_thread_watcher.validate());
}

Result<> AssetLoader::initialize(Engine& engine) {
    m_thread_watcher.set_to_current_thread();
    auto fs_module = engine.module<ModuleFileSystem>();
    if (!fs_module) {
        MODUS_LOGE("Could not locate vfs module");
        return Error<>();
    }
    return Ok<>();
}

void AssetLoader::shutdown() {
    MODUS_LOGI("AssetLoader: Shutting down");
    modus_assert(m_thread_watcher.validate());
}

void AssetLoader::update(Engine& engine) {
    MODUS_PROFILE_ASSETS_BLOCK("AssetLoader::Update");
    MODUS_UNUSED(engine);
    modus_assert(m_thread_watcher.validate());

    auto& queue = m_queued_assets[m_active_queue];
    m_active_queue = (m_active_queue + 1) % kQueueCount;
    auto& other_queue = m_queued_assets[m_active_queue];
    auto it = queue.begin();
    while (it != queue.end()) {
        AssetInfo& info = **it;
        MODUS_ASSET_LOADER_LOG("Asset:{} :: {}\n", info.file_path,
                               asset_state_to_string(info.state));
        switch (info.state) {
            case AssetStateV2::Unloaded: {
                modus_panic("Unloaded assets should not be in the queue");
            }
            case AssetStateV2::Destroy: {
                if (info.factory) {
                    destroy_asset(engine, info);
                }
                break;
            }
            case AssetStateV2::Loading: {
                other_queue.push_back(*it);
                it++;
                continue;
            }
            case AssetStateV2::Waiting: {
                if (auto r = info.data->eval_dependencies(engine, *this); r) {
                    if (*r) {
                        info.state = AssetStateV2::Loaded;
                    } else {
                        other_queue.push_back(*it);
                        it++;
                    }
                } else {
                    info.state = AssetStateV2::Error;
                    info.error = AssetErrorV2::Dependency;
                }
                continue;
            }
            case AssetStateV2::Error: {
                MODUS_LOGE("AssetLoader: Failed to load/create asset {}", info.file_path);
                info.data.reset();
                trigger_asset_callbacks(engine, info);
                it++;
                break;
            }
            case AssetStateV2::Loaded: {
                // Factory was unloaded during processing, remove asset
                if (!info.factory) {
                    info.state = AssetStateV2::Unloaded;
                    info.data.reset();
                    remove_asset_info(info);
                    break;
                }

                // Create asset
                auto r = info.factory->create(engine, *info.data);
                if (!r) {
                    info.state = AssetStateV2::Error;
                    info.error = AssetErrorV2::Create;
                    continue;
                }
                info.data.reset();
                info.state = AssetStateV2::Created;
                info.error = AssetErrorV2::None;
                info.asset = *r;
                (*r)->m_asset_handle = info.handle;
                trigger_asset_callbacks(engine, info);
                ++it;
                break;
            }
            default:
                modus_assert_message(false, "unknown asset state");
                it++;
                continue;
        }
    }
    queue.clear();
}

Result<AsyncLoadResult> AssetLoader::load_async(Engine& engine, AsyncLoadParams& params) {
    MODUS_PROFILE_ASSETS_BLOCK("AssetLoader::load_async");
    MODUS_UNUSED(params);
    modus_assert(m_thread_watcher.validate());

    const StringSlice extension = os::Path::get_extension(params.path);
    auto r_factory = find_factory_with_ext(extension);
    if (!r_factory) {
        MODUS_LOGE("Couldn't locate any factory for extension: '{}'", extension);
        return Error<>();
    }
    String path_str = params.path.to_str();
    auto r_find = find_asset(path_str);
    if (r_find) {
        const AssetStateV2 state = (*r_find)->state;
        (*r_find)->m_ref_count++;
        if (params.callback) {
            if (state == AssetStateV2::Created) {
                params.callback(
                    engine, {params.path, (*r_find)->handle, (*r_find)->error, (*r_find)->asset});
            } else {
                (*r_find)->callbacks.push_back(std::move(params.callback));
            }
        }
        return Ok(AsyncLoadResult{(*r_find)->handle, state});
    }

    auto r_asset = create_asset_info(*r_factory, std::move(path_str));
    if (!r_asset) {
        return Error<>();
    }
    NotMyPtr<AssetInfo> asset = *r_asset;
    asset->state = AssetStateV2::Loading;
    if (params.callback) {
        asset->callbacks.push_back(std::move(params.callback));
    }
    m_queued_assets[m_active_queue].push_back(asset);
    AssetLoaderJob asset_loader_job;
    asset_loader_job.m_fs = &engine.module<ModuleFileSystem>()->vfs();
    asset_loader_job.asset = asset;
    auto& job_system = engine.module<ModuleParallel>()->job_system();
    auto job = job_system.create(std::move(asset_loader_job));
    job_system.run(job);
    return Ok(AsyncLoadResult{asset->handle, asset->state});
}

void AssetLoader::destroy_async(const AssetHandleV2 handle) {
    modus_assert(m_thread_watcher.validate());
    MODUS_UNUSED(handle);
    auto r_asset = m_assets.get(handle);
    if (r_asset) {
        NotMyPtr<AssetInfo> asset = *r_asset;
        modus_assert(asset->m_ref_count >= 1);
        asset->m_ref_count--;
        if (asset->m_ref_count == 0) {
            (*r_asset)->state = AssetStateV2::Destroy;
            m_queued_assets[m_active_queue].push_back(asset);
        }
    }
}

Result<AssetStateV2> AssetLoader::asset_state(const AssetHandleV2 handle) const {
    auto r = m_assets.get(handle);
    if (!r) {
        return Error<>();
    }
    const AssetStateV2 state = (*r)->state;
    return Ok<AssetStateV2>(state);
}

Result<StringSlice> AssetLoader::asset_path(const AssetHandleV2 handle) const {
    auto r = m_assets.get(handle);
    if (!r) {
        return Error<>();
    }
    return Ok(StringSlice((*r)->file_path));
}

Result<NotMyPtr<const AssetBase>, AssetStateV2> AssetLoader::resolve(
    const AssetHandleV2 handle) const {
    auto r = m_assets.get(handle);
    if (!r) {
        return Error(AssetStateV2::Unloaded);
    }
    if ((*r)->state != AssetStateV2::Created) {
        return Error(AssetStateV2((*r)->state));
    }
    return Ok<NotMyPtr<const AssetBase>>((*r)->asset);
}

Result<NotMyPtr<const AssetBase>, AssetStateV2> AssetLoader::resolve_by_path(
    const StringSlice path) const {
    const String path_str = path.to_str();
    auto it = m_path_map.find(path_str);
    if (it == m_path_map.end()) {
        return Error(AssetStateV2::Unloaded);
    }
    return resolve(it->second);
}

Result<> AssetLoader::register_factory(NotMyPtr<AssetFactory> factory) {
    modus_assert(m_thread_watcher.validate());
    auto r = find_factory_with_ext(factory->file_extension());
    if (r) {
        return Error<>();
    }
    m_factories.push_back(factory);
    return Ok<>();
}

void AssetLoader::unregister_factory(Engine& engine, NotMyPtr<AssetFactory> factory) {
    modus_assert(m_thread_watcher.validate());
    auto r_factory = find_factory_with_ext(factory->file_extension());
    if (!r_factory) {
        return;
    }
    m_assets.for_each([&engine, factory, this](AssetInfo& info) {
        if (info.factory == factory) {
            if (info.state == AssetStateV2::Created) {
                // Note: this will also destroy the path map entry and the handle
                // As long as we are using handle pool, we can delete while iterating
                destroy_asset(engine, info);
            } else {
                info.factory.reset();
            }
        }
    });
}

Result<NotMyPtr<AssetFactory>, void> AssetLoader::find_factory_with_ext(const StringSlice ext) {
    modus_assert(m_thread_watcher.validate());
    auto it = std::find_if(m_factories.begin(), m_factories.end(),
                           [ext](const auto& factory) { return factory->file_extension() == ext; });
    if (it == m_factories.end()) {
        return Error<>();
    }
    return Ok<NotMyPtr<AssetFactory>>(*it);
}

Result<NotMyPtr<AssetLoader::AssetInfo>, void> AssetLoader::find_asset(const String& path) {
    modus_assert(m_thread_watcher.validate());
    auto it = m_path_map.find(path);
    if (it == m_path_map.end()) {
        return Error<>();
    }
    return m_assets.get(it->second);
}

void AssetLoader::remove_asset_info(AssetInfo& info) {
    modus_assert(m_thread_watcher.validate());
    m_path_map.erase(info.file_path);
    (void)m_assets.erase(info.handle);
}

Result<NotMyPtr<AssetLoader::AssetInfo>> AssetLoader::create_asset_info(
    NotMyPtr<AssetFactory> factory,
    String&& path_str) {
    modus_assert(m_thread_watcher.validate());
    AssetInfo info;
    info.factory = factory;
    info.file_path = std::move(path_str);
    info.m_ref_count = 1;
    info.callbacks.reserve(4);
    info.data = factory->create_asset_data();
    String key_str = info.file_path;
    auto r = m_assets.create(std::move(info));
    if (!r) {
        return Error<>();
    }
    (*r).second->handle = (*r).first;
    m_path_map.insert({std::move(key_str), r->first});
    return Ok(r->second);
}

void AssetLoader::destroy_asset(Engine& engine, AssetInfo& info) {
    if (info.m_ref_count > 1) {
        MODUS_LOGW("Destroying asset with has active references {:02}: {}", info.m_ref_count,
                   info.file_path);
    }
    if (!info.factory->destroy(engine, info.asset)) {
        MODUS_LOGW("Failed to destroy asset:{}", info.file_path);
    }
    remove_asset_info(info);
}

void AssetLoader::trigger_asset_callbacks(Engine& engine, AssetInfo& info) {
    for (auto& cb : info.callbacks) {
        cb(engine, {info.file_path, info.handle, info.error, info.asset});
    }
    info.callbacks.clear();
}

}    // namespace modus::engine::assets
