/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/engine.hpp>
// clang-format on

#include <app/app.hpp>
#include <engine/event/engine_events.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_event.hpp>
#include <engine/modules/module_parallel.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_config.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_input.hpp>
#include <engine/assets/asset_waiter.hpp>
#include <engine/assets/asset_loader.hpp>

#if defined(MODUS_LOG_USE_SPDLOG)
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/rotating_file_sink.h>
#endif
#include <os/path.hpp>

namespace modus::engine {

struct Engine::EnginePrivate {
    ModuleApp m_mod_app;
    ModuleEvent m_mod_event;
    ModuleConfig m_mod_config;
    ModuleFileSystem m_mod_filesystem;
    ModuleAssets m_mod_assets;
    ModuleInput m_mod_input;
    ModuleParallel m_mod_parallel;
    assets::AssetGroupWaiter m_init_assets;
};

Engine::Engine()
    : m_game(nullptr),
      m_timer(),
      m_previous_tick_ms(0),
      m_tick_ms(0),
      m_fps_tick_ms(0),
      m_fps(0),
      m_frame_count(0),
      m_tick_sec(0),
      m_quit_requested(false) {
    m_private = modus::make_unique<EnginePrivate>();
#define register_module(module)                                                \
    m_modules.register_module<decltype(m_private->module)>(&m_private->module) \
        .expect("failed to register module")
    register_module(m_mod_parallel);
    register_module(m_mod_app);
    register_module(m_mod_event);
    register_module(m_mod_config);
    register_module(m_mod_filesystem);
    register_module(m_mod_assets);
    register_module(m_mod_input);
#undef register_module
}

Engine::~Engine() {}

Result<bool, void> Engine::parse_command_options(const int argc, const char** argv, IGame& game) {
    CmdOptionParser cmd_parser;

    if (!add_to_option_parser(cmd_parser, m_options)) {
        fprintf(stderr, "Failed to add cmd line options from engine\n");
    }

    if (!game.add_cmd_options(cmd_parser)) {
        fprintf(stderr, "Failed to add cmd line options from game\n");
    }

    if (!cmd_parser.parse(argc, argv) || cmd_parser.help_requested()) {
        FILE* out = cmd_parser.help_requested() ? stdout : stderr;
        auto help_string = cmd_parser.help_string(modus::format("{} Options", game.name()));
        fprintf(out, "%s", help_string.c_str());
        if (out == stderr) {
            return Error<>();
        }
        return Ok(true);
    }
    return Ok(false);
}

EngineInitResult Engine::initialize(IGame& game) {
    MODUS_PROFILE_MARK_MAIN_THREAD;
    if (m_options.profiling.enabled_remote.parsed()) {
        MODUS_PROFILE_START_LISTENING(static_cast<u16>(m_options.profiling.server_port.value()));
    }
    if (m_options.profiling.enabled_local.parsed() || m_options.profiling.enabled_remote.parsed()) {
        MODUS_PROFILE_ENABLE;
    }
    // MODUS_PROFILE_START_LISTENING();
    MODUS_PROFILE_ENGINE_BLOCK("Engine::initialize");
    // Init log
#if defined(MODUS_LOG_USE_SPDLOG)
    {
        auto r_exec_dir = os::Path::executable_directory();
        if (!r_exec_dir) {
            return Error(EngineInitError::Os);
        }

        // Console/Stdout logger
        spdlog::sink_ptr console = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        console->set_level(spdlog::level::debug);
        console->set_pattern("%^[%H:%M:%S.%f][%L] %v%$");

        // File Logger
        String engine_log_file = os::Path::join(*r_exec_dir, game.name());
        engine_log_file += ".log";
        auto max_size = 104876 * 5;
        auto max_files = 3;
        spdlog::sink_ptr file_logger = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(
            spdlog::filename_t(engine_log_file), max_size, max_files, true);
        file_logger->set_level(spdlog::level::trace);
        file_logger->set_pattern("[%C%m%d_%H%M%S.%f][%L] %v");

        spdlog::sink_ptr log_sinks[] = {console, file_logger};
        auto logger =
            std::make_shared<spdlog::logger>("", std::begin(log_sinks), std::end(log_sinks));
        log::LogService::set(logger);
    }
#if defined(MODUS_DEBUG)
    log::LogService::set_level(log::Level::Debug);
#endif
#else
    log::LogService::set(log::make_basic_stdout_logger());
#if defined(MODUS_DEBUG)
    log::LogService::set_level(log::Level::Debug);
#endif
#endif

    if (m_options.logging.level.parsed()) {
        const StringSlice log_level = m_options.logging.level.value();
        if (auto r = log::log_level_from_str(log_level); r) {
            log::LogService::set_level(*r);
        } else {
            MODUS_LOGE("Invalid log level '{}', using configuration default", log_level);
        }
    }

    MODUS_LOGI("Modus Engine {:02}.{:02}.{:02}", MODUS_ENGINE_VERSION_MAJOR,
               MODUS_ENGINE_VERSION_MINOR, MODUS_ENGINE_VERSION_PATCH);
#if defined(MODUS_PROFILING)
    MODUS_LOGI("Profiling enabled");
#endif
    MODUS_LOGI("Initializing...");

    m_game = &game;
    ModuleRegistrator module_registrator(m_modules);

    if (!game.register_modules(module_registrator)) {
        MODUS_LOGE("Failed to register game modules");
        return Error(EngineInitError::Game);
    }

    // Init Engine systems
    MODUS_LOGI("Initializing Engine Modules");
    if (!m_modules.initialize(*this)) {
        return Error(EngineInitError::Module);
    }

    if (auto r_module_event = this->module<ModuleEvent>(); !r_module_event) {
        MODUS_LOGE("Expected Event module {} is not in module list", module_guid<ModuleEvent>());
        return Error(EngineInitError::Module);
    }

    MODUS_LOGI("Engine Initialization complete");
    // Init game
    MODUS_LOGI("Initializing Game");
    Result<> game_init_result = Error<>();
    {
        MODUS_PROFILE_ENGINE_BLOCK("Game Init");
        game_init_result = m_game->initialize(*this);
        if (!game_init_result) {
            MODUS_LOGE("Failed to initialize Game. Check log for details.");
            return Error(EngineInitError::Game);
        }
    }

    // Request Engine View from Game
    MODUS_LOGI("Initialization complete!");
    return Ok<>();
}

EngineInitResult Engine::shutdown() {
    {
        MODUS_PROFILE_ENGINE_BLOCK("Engine::shutdown");

        MODUS_LOGI("Engine shutdown requested");

        // Shutdown game
        MODUS_LOGI("Game shutdown");
        Result<> game_shutdown_result = Ok<>();
        if (m_game != nullptr) {
            MODUS_PROFILE_ENGINE_BLOCK("Game Shutdown");
            game_shutdown_result = m_game->shutdown(*this);
            if (!game_shutdown_result) {
                MODUS_LOGE("Failed to shutdown game. Check log for details");
            }
        }

        // Shutdown Engine Systems
        if (auto modules_shutdown_result = m_modules.shutdown(*this); !modules_shutdown_result) {
            MODUS_LOGE("Failed to shutdown one or more modules");
        }

        MODUS_LOGI("Engine Shutdown complete, goodbye!");
        // Shutdown complete;
    }

    if (m_options.profiling.enabled_local.parsed()) {
#if defined(MODUS_PROFILER_USE_EASY_PROFILER)
        auto file_path = m_options.profiling.output_file_path.value_cstr();
        const u32 n_blocks = MODUS_PROFILE_DUMP_TO_FILE(file_path);
        MODUS_LOGI("Profiling finished: {} blocks recorded", n_blocks);
        MODUS_UNUSED(file_path);
#endif
    }

    // Shutdown log
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::unset();
#endif

    return Ok<>();
}

void Engine::run() {
    // Set previous tick to now to avoid high delta time on first
    // run due to long loading times.
    m_previous_tick_ms = m_timer.elapsed_miliseconds();
    while (!m_quit_requested) {
        MODUS_PROFILE_ENGINE_BLOCK("tick");
        // update fps counter
        const IMilisec current_ms = m_timer.elapsed_miliseconds();
        m_tick_ms = (current_ms - m_previous_tick_ms);
        m_tick_sec = FSeconds(m_tick_ms);
        m_fps_tick_ms += IMilisec(m_tick_ms);
        m_previous_tick_ms = current_ms;
        if (m_fps_tick_ms > IMilisec(500)) {
            m_fps = 2.0 * (f64(m_frame_count) / (f64(m_fps_tick_ms.count()) / 500.0));
            m_frame_count = 0;
            m_fps_tick_ms = IMilisec(0);
        }
        m_frame_count++;

        auto module_event = this->module<ModuleEvent>();
        {
            BeginFrameEvent event;
            module_event->manager().trigger(*this, module_event->engine_events().begin_frame,
                                            event);
        }

        // tick game
        {
            MODUS_PROFILE_ENGINE_BLOCK("Tick Game");
            m_game->tick(*this);
        }

        // tick engine
        {
            MODUS_PROFILE_ENGINE_BLOCK("Modules");
            m_modules.tick(*this);
        }
        // run engine update

        {
            EndFrameEvent event;
            module_event->manager().trigger(*this, module_event->engine_events().end_frame, event);
        }

        MODUS_PROFILE_MARK_FRAME;
    }
}

void Engine::quit() {
    m_quit_requested = true;
}

Result<assets::AssetHandleV2> Engine::load_startup_asset(assets::AsyncLoadParams& params) {
    auto asset_module = this->module<ModuleAssets>();

    auto r = asset_module->loader().load_async(*this, params);
    if (!r) {
        return Error<>();
    }
    if (r->state == assets::AssetStateV2::Error) {
        return Error<>();
    }
    m_private->m_init_assets.add(r->handle);
    return Result<assets::AssetHandleV2>(r->handle);
}

Result<EngineStartupAssetState> Engine::startup_asset_state() {
    auto asset_module = this->module<ModuleAssets>();
    const assets::AssetGroupWaiterState state =
        m_private->m_init_assets.update(*this, asset_module->loader());
    if (state.error_count != 0) {
        return Error<>();
    }
    return Ok(EngineStartupAssetState{state.finished_count, u32(m_private->m_init_assets.count())});
}

IMilisec Engine::elapsed_time_ms() const {
    return m_timer.elapsed_miliseconds();
}

DSeconds Engine::elapsed_time_sec() const {
    return m_timer.elapsed_seconds();
}

}    // namespace modus::engine
