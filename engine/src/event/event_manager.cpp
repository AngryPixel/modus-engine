/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/event/event_manager.hpp>
#include <os/time.hpp>

namespace modus::engine::event {

Event::Event(const StringSlice debug_text) {
    MODUS_UNUSED(debug_text);
#if defined(MODUS_ENGINE_EVENT_DEBUG)
    m_debug_name = debug_text;
#endif
}

EventManager::EventManager()
    : m_event_listeners(),
      m_event_queues{EventQueue(), EventQueue()},
      m_queued_registrations(),
      m_queued_unregistrations(),
      m_active_queue_index(0),
      m_processing_events(false) {
    static constexpr usize kVectorReserveAmount = 8;
    for (auto& queue : m_event_queues) {
        queue.reserve(kVectorReserveAmount);
    }
    m_queued_registrations.reserve(kVectorReserveAmount);
    m_queued_unregistrations.reserve(kVectorReserveAmount);
    MODUS_UNUSED(m_thread_watcher);
}

EventManager::~EventManager() {
    modus_assert(m_event_listeners.count() == 0);
}

void EventManager::update(Engine& engine, const IMilisec max_duration_ms) {
    modus_assert(m_thread_watcher.validate());
    {
        MODUS_PROFILE_EVENT_BLOCK("Queued Listener Registrations");
        // Register queued listeners

        for (auto& queued_register : m_queued_registrations) {
            auto r_listeners = m_event_listeners.get(queued_register.type);
            if (r_listeners) {
                auto r_listener = (*r_listeners)->get(queued_register.handle);
                if (r_listener) {
                    *(*r_listener) = queued_register.listener;
                }
            }
        }
        m_queued_registrations.clear();
    }
    {
        MODUS_PROFILE_EVENT_BLOCK("Queued Listener Unregistrations");
        // Unregister queued listeners
        for (auto& queued_unregister : m_queued_unregistrations) {
            auto r_listener = m_event_listeners.get(queued_unregister.type);
            if (r_listener) {
                (void)(*r_listener)->erase(queued_unregister.handle);
            }
        }
        m_queued_unregistrations.clear();
    }

    // Begin Event Processing
    MODUS_PROFILE_EVENT_BLOCK("Queued Events");
    os::Timer timer;
    const usize current_queue_index = m_active_queue_index;
    m_active_queue_index = (m_active_queue_index + 1) % kMaxEventQueues;

    auto it = m_event_queues[current_queue_index].begin();
    auto it_end = m_event_queues[current_queue_index].end();
    m_processing_events = true;

    for (; it != it_end; ++it) {
        trigger(engine, (*it)->event_handle(), *(*it));
        AllocatorTraits<EventAllocator>::destroy_and_free((*it));
        if (timer.elapsed_microseconds() >= max_duration_ms) {
            MODUS_LOGW(
                "EventManager: Exceeded event processing time limit. Queuing "
                "remaining events in next queue");
            break;
        }
    }
    // put any levft over events in the other queue
    if (!m_event_queues[current_queue_index].empty()) {
        m_event_queues[m_active_queue_index].insert(m_event_queues[current_queue_index].end(), it,
                                                    it_end);
    }
    m_event_queues[current_queue_index].clear();
    m_processing_events = false;
}

Result<EventHandle, void> EventManager::create_event_type() {
    modus_assert(m_thread_watcher.validate());
    modus_assert_message(!m_event_listeners.is_full(),
                         "Maximum number of event types reached, increase limit");
    if (m_event_listeners.is_full()) {
        MODUS_LOGE("EventManger: Maximum number of event types reached.");
        return Error<>();
    }

    auto r_event = m_event_listeners.create(EventListenerList());
    if (!r_event) {
        return Error<>();
    }
    return Ok(r_event->first);
}

Result<> EventManager::destroy_event_type(const EventHandle handle) {
    modus_assert(m_thread_watcher.validate());
    return m_event_listeners.erase(handle);
}

Result<ListenerHandle, void> EventManager::register_event_listener(
    const EventHandle type,
    modus::engine::event::EventListener listener) {
    modus_assert(m_thread_watcher.validate());
    auto r_event_type = m_event_listeners.get(type);
    if (!r_event_type) {
        return Error<>();
    }

    NotMyPtr<EventListenerList> event_listeners_list = *r_event_type;
    modus_assert_message(!event_listeners_list->is_full(),
                         "Maximum number of event listeners reached, increase limit");

    ListenerHandle handle;
    if (!m_processing_events) {
        auto r_create = event_listeners_list->create(listener);
        if (!r_create) {
            return Error<>();
        }
        handle = r_create->first;
    } else {
        // When queueing, create an emtpy event listener to reserve a handle and
        // on the next update replace with the real value
        auto r_create = event_listeners_list->create(EventListener{nullptr, nullptr});
        if (!r_create) {
            return Error<>();
        }
        handle = r_create->first;
        m_queued_registrations.emplace_back(QueuedRegistration{type, handle, listener});
    }
    return Ok(handle);
}

Result<> EventManager::unregister_event_listner(const EventHandle type,
                                                const ListenerHandle handle) {
    modus_assert(m_thread_watcher.validate());
    if (!m_processing_events) {
        auto r_event_type = m_event_listeners.get(type);
        if (!r_event_type) {
            return Error<>();
        }
        return (*r_event_type)->erase(handle);
    } else {
        m_queued_unregistrations.emplace_back(QueuedUnregistration{type, handle});
        return Ok<>();
    }
}

void EventManager::trigger(Engine& engine,
                           const EventHandle handle,
                           modus::engine::event::Event& event) {
    MODUS_PROFILE_EVENT_BLOCK("EventManger::Trigger");
    modus_assert(m_thread_watcher.validate());
    bool revert_flag = false;
    if (!m_processing_events) {
        m_processing_events = true;
        revert_flag = true;
    }
    event.m_event_handle = handle;

    auto r_listener = m_event_listeners.get(handle);
    if (r_listener) {
        (*r_listener)->for_each([&engine, &event](EventListener& listener) {
            if (listener) {
                listener(engine, event);
            }
        });
    }
    if (revert_flag) {
        m_processing_events = false;
    }
}

void EventManager::clear() {
    modus_assert(m_thread_watcher.validate());
    m_event_listeners.clear();
    for (auto& queue : m_event_queues) {
        for (auto& event : queue) {
            AllocatorTraits<EventAllocator>::destroy_and_free(event);
        }
        queue.clear();
    }
    m_active_queue_index = 0;
    m_processing_events = false;
}

void EventManager::queue(const EventHandle handle, Event* event) {
    modus_assert(m_thread_watcher.validate());
    event->m_event_handle = handle;
    m_event_queues[m_active_queue_index].push_back(event);
}

}    // namespace modus::engine::event
