/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <engine/engine.hpp>
#include <engine/input/input_mapper.hpp>
#include <engine/modules/module_app.hpp>

namespace modus::engine::input {

namespace v2 {

AxisMap::AxisMap() {
    reset();
}

void AxisMap::bind_positive_button(const app::InputKey k) {
    m_positive_button.type = ButtonBindingType::KeyBoard;
    m_positive_button.key = k;
}

void AxisMap::bind_positive_button(const app::MouseButton b) {
    m_positive_button.type = ButtonBindingType::Mouse;
    m_positive_button.mouse_button = b;
}

void AxisMap::bind_positive_button(const app::GameControllerButton b) {
    m_positive_button.type = ButtonBindingType::GameController;
    m_positive_button.controller_button = b;
}

void AxisMap::bind_negative_button(const app::InputKey k) {
    m_negative_button.type = ButtonBindingType::KeyBoard;
    m_negative_button.key = k;
}

void AxisMap::bind_negative_button(const app::MouseButton b) {
    m_negative_button.type = ButtonBindingType::Mouse;
    m_negative_button.mouse_button = b;
}

void AxisMap::bind_negative_button(const app::GameControllerButton b) {
    m_negative_button.type = ButtonBindingType::GameController;
    m_negative_button.controller_button = b;
}

void AxisMap::bind_alt_positive_button(const app::InputKey k) {
    m_alt_positive_button.type = ButtonBindingType::KeyBoard;
    m_alt_positive_button.key = k;
}

void AxisMap::bind_alt_positive_button(const app::MouseButton b) {
    m_alt_positive_button.type = ButtonBindingType::Mouse;
    m_alt_positive_button.mouse_button = b;
}

void AxisMap::bind_alt_positive_button(const app::GameControllerButton b) {
    m_alt_positive_button.type = ButtonBindingType::GameController;
    m_alt_positive_button.controller_button = b;
}

void AxisMap::bind_alt_negative_button(const app::InputKey k) {
    m_alt_negative_button.type = ButtonBindingType::KeyBoard;
    m_alt_negative_button.key = k;
}

void AxisMap::bind_alt_negative_button(const app::MouseButton b) {
    m_alt_negative_button.type = ButtonBindingType::Mouse;
    m_alt_negative_button.mouse_button = b;
}

void AxisMap::bind_alt_negative_button(const app::GameControllerButton b) {
    m_alt_negative_button.type = ButtonBindingType::GameController;
    m_alt_negative_button.controller_button = b;
}

void AxisMap::bind_axis(const AxisBindingType type) {
    m_axis_type = type;
}

void AxisMap::unbind_positive_button() {
    m_positive_button.type = ButtonBindingType::None;
    m_positive_button.m_is_down = false;
}
void AxisMap::unbind_negative_button() {
    m_negative_button.type = ButtonBindingType::None;
    m_negative_button.m_is_down = false;
}
void AxisMap::unbind_alt_positive_button() {
    m_alt_positive_button.type = ButtonBindingType::None;
    m_alt_positive_button.m_is_down = false;
}
void AxisMap::unbind_alt_negative_button() {
    m_alt_negative_button.type = ButtonBindingType::None;
    m_alt_negative_button.m_is_down = false;
}
void AxisMap::unbind_axis() {
    m_axis_type = AxisBindingType::None;
}

void AxisMap::bind_on_button_down(Callback delegate) {
    m_on_down = delegate;
}

void AxisMap::bind_on_button_up(Callback delegate) {
    m_on_up = delegate;
}

void AxisMap::bind_on_update(Callback delegate) {
    m_on_update = delegate;
}

void AxisMap::on_frame_begin(Engine& engine) {
    MODUS_UNUSED(engine);
    // If axis type is mouse move or scroll we need to reset the value
    // every frame as these event just produce values when they are used
    // and do not reset themselves.
    if (m_axis_type == AxisBindingType::MouseX || m_axis_type == AxisBindingType::MouseY ||
        m_axis_type == AxisBindingType::MouseScrollX ||
        m_axis_type == AxisBindingType::MouseScrollY) {
        m_value_axis = 0;
        m_value_axis_normalized = 0.0f;
        m_axis_active = false;
    }
}

bool AxisMap::on_event(Engine& engine, const app::InputEvent& event) {
    auto fn_handle_positive_button = [this, &engine](ButtonBinding& binding,
                                                     const bool is_down) -> void {
        if (binding.m_is_down != is_down) {
            binding.m_is_down = is_down;
            if (binding.m_is_down) {
                m_value += 1;
                m_value_normalized += 1.0f;
                on_button_down(engine);
            } else {
                m_value -= 1;
                m_value_normalized -= 1.0f;
                on_button_up(engine);
            }
        }
    };

    auto fn_handle_negative_button = [this, &engine](ButtonBinding& binding,
                                                     const bool is_down) -> void {
        if (binding.m_is_down != is_down) {
            binding.m_is_down = is_down;
            if (binding.m_is_down) {
                m_value -= 1;
                m_value_normalized -= 1.0f;
                on_button_down(engine);
            } else {
                m_value += 1;
                m_value_normalized += 1.0f;
                on_button_up(engine);
            }
        }
    };

    if (event.type == app::InputEventType::Keyboard) {
        if (m_positive_button.type == ButtonBindingType::KeyBoard &&
            m_positive_button.key == event.keyboard.key) {
            fn_handle_positive_button(m_positive_button, event.keyboard.is_key_down());
            return true;
        }
        if (m_alt_positive_button.type == ButtonBindingType::KeyBoard &&
            m_alt_positive_button.key == event.keyboard.key) {
            fn_handle_positive_button(m_alt_positive_button, event.keyboard.is_key_down());
            return true;
        }
        if (m_negative_button.type == ButtonBindingType::KeyBoard &&
            m_negative_button.key == event.keyboard.key) {
            fn_handle_negative_button(m_negative_button, event.keyboard.is_key_down());
            return true;
        }
        if (m_alt_negative_button.type == ButtonBindingType::KeyBoard &&
            m_alt_negative_button.key == event.keyboard.key) {
            fn_handle_negative_button(m_alt_negative_button, event.keyboard.is_key_down());
            return true;
        }
    } else if (event.type == app::InputEventType::MouseButton) {
        const bool is_down = event.mouse_button.is_mouse_down();
        const app::MouseButton button = event.mouse_button.button;
        if (m_positive_button.type == ButtonBindingType::Mouse &&
            m_positive_button.mouse_button == button) {
            fn_handle_positive_button(m_positive_button, is_down);
            return true;
        }
        if (m_alt_positive_button.type == ButtonBindingType::Mouse &&
            m_alt_positive_button.mouse_button == button) {
            fn_handle_positive_button(m_alt_positive_button, is_down);
            return true;
        }
        if (m_negative_button.type == ButtonBindingType::Mouse &&
            m_negative_button.mouse_button == button) {
            fn_handle_negative_button(m_negative_button, is_down);
            return true;
        }
        if (m_alt_negative_button.type == ButtonBindingType::Mouse &&
            m_alt_negative_button.mouse_button == button) {
            fn_handle_negative_button(m_alt_negative_button, is_down);
            return true;
        }
    } else if (event.type == app::InputEventType::GameControllerButton) {
        const bool is_down = event.controller_button.is_button_down();
        const app::GameControllerButton button = event.controller_button.button;
        if (m_positive_button.type == ButtonBindingType::GameController &&
            m_positive_button.controller_button == button) {
            fn_handle_positive_button(m_positive_button, is_down);
            return true;
        }
        if (m_alt_positive_button.type == ButtonBindingType::GameController &&
            m_alt_positive_button.controller_button == button) {
            fn_handle_positive_button(m_alt_positive_button, is_down);
            return true;
        }
        if (m_negative_button.type == ButtonBindingType::GameController &&
            m_negative_button.controller_button == button) {
            fn_handle_negative_button(m_negative_button, is_down);
            return true;
        }
        if (m_alt_negative_button.type == ButtonBindingType::GameController &&
            m_alt_negative_button.controller_button == button) {
            fn_handle_negative_button(m_alt_negative_button, is_down);
            return true;
        }
    } else if (event.type == app::InputEventType::MouseMove) {
        auto app_module = engine.module<ModuleApp>();
        auto opt_window = app_module->app().main_window();
        if (m_axis_type == AxisBindingType::MouseX && event.mouse_move.deltax != 0) {
            m_value_axis = event.mouse_move.deltax;
            if (opt_window) {
                m_value_axis_normalized =
                    f32(m_value_axis) / f32((*opt_window)->window_state().width);
            } else {
                m_value_axis_normalized = 0.f;
            }
            return true;
        } else if (m_axis_type == AxisBindingType::MouseY && event.mouse_move.deltay != 0) {
            m_value_axis = event.mouse_move.deltay;
            if (opt_window) {
                m_value_axis_normalized =
                    f32(m_value_axis) / f32((*opt_window)->window_state().height);
            } else {
                m_value_axis_normalized = 0.f;
            }
            return true;
        }
    } else if (event.type == app::InputEventType::MouseScroll) {
        if (m_axis_type == AxisBindingType::MouseScrollX && event.mouse_scroll.xvalue != 0) {
            m_value_axis = event.mouse_scroll.xvalue;
            m_value_axis_normalized = m_value > 0 ? 1.0f : -1.0f;
            return true;
        } else if (m_axis_type == AxisBindingType::MouseScrollY && event.mouse_scroll.yvalue != 0) {
            m_value_axis = event.mouse_scroll.yvalue;
            m_value_axis_normalized = m_value > 0 ? 1.0f : -1.0f;
            return true;
        }
    } else if (event.type == app::InputEventType::GameControllerAxis) {
        auto fn_set_value = [this, &engine](const app::GameControllerAxisEvent& axis_event) {
            const f32 normalized = axis_event.normalized();
            if (std::abs(normalized) > m_dead_zone) {
                m_value_axis = axis_event.value;
                m_value_axis_normalized = normalized;
                if (m_axis_active != true) {
                    m_axis_active = true;
                    on_button_down(engine);
                }
            } else {
                m_value_axis = 0;
                m_value_axis_normalized = 0.0f;
                if (m_axis_active != false) {
                    m_axis_active = false;
                    on_button_up(engine);
                }
            }
        };
        const app::GameControllerAxis axis = event.controller_axis.axis;
        switch (m_axis_type) {
            case AxisBindingType::GameControllerX: {
                if (axis == app::GameControllerAxis::LeftX ||
                    axis == app::GameControllerAxis::RightX) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerY: {
                if (axis == app::GameControllerAxis::LeftY ||
                    axis == app::GameControllerAxis::RightY) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerLeftX: {
                if (axis == app::GameControllerAxis::LeftX) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerLeftY: {
                if (axis == app::GameControllerAxis::LeftY) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerRightX: {
                if (axis == app::GameControllerAxis::RightX) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerRightY: {
                if (axis == app::GameControllerAxis::RightY) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerRightTrigger: {
                if (axis == app::GameControllerAxis::RightTrigger) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            case AxisBindingType::GameControllerLeftTrigger: {
                if (axis == app::GameControllerAxis::LeftTrigger) {
                    fn_set_value(event.controller_axis);
                    return true;
                }
            } break;
            default:
                break;
        }
    }
    return false;
}

void AxisMap::on_update(Engine& engine) {
    if (is_active() && m_on_update) {
        m_on_update(engine, *this);
    }
}

void AxisMap::set_dead_zone(const f32 value) {
    m_dead_zone = value;
}
void AxisMap::set_inverted(const bool value) {
    m_invert = value;
}
void AxisMap::set_sensitivity(const f32 sensitivity) {
    m_sensitivity = sensitivity;
}
void AxisMap::set_enabled(const bool value) {
    m_enabled = value;
}
void AxisMap::set_name(StringSlice name) {
    m_name = name;
}

i32 AxisMap::value() const {
    if (!m_enabled) {
        return 0;
    }

    const i32 total_value = m_value + m_value_axis;
    return i32(f32(m_invert ? -total_value : total_value) * m_sensitivity);
}

f32 AxisMap::value_normalized() const {
    if (!m_enabled) {
        return 0.0f;
    }
    const f32 total_value = m_value_normalized + m_value_axis_normalized;
    return (m_invert ? -total_value : total_value) * m_sensitivity;
}

bool AxisMap::is_active() const {
    return (m_value + m_value_axis) != 0;
}

void AxisMap::reset() {
    unbind_negative_button();
    unbind_alt_negative_button();
    unbind_positive_button();
    unbind_alt_positive_button();
    unbind_axis();
    m_name = "Unnamed";
    m_value = 0;
    m_value_normalized = 0.0f;
    m_value_axis = 0;
    m_value_axis_normalized = 0.0f;
    m_sensitivity = 1.0f;
    m_enabled = false;
    m_invert = false;
    m_dead_zone = 0.03f;
    m_button_down_count = 0;
    m_on_up = Callback();
    m_on_down = Callback();
    m_on_update = Callback();
    m_axis_active = false;
}

void AxisMap::reset_input_state() {
    m_value = 0;
    m_value_normalized = 0.0f;
    m_value_axis = 0;
    m_value_axis_normalized = 0.0f;
    m_button_down_count = 0;
    m_positive_button.m_is_down = false;
    m_alt_positive_button.m_is_down = false;
    m_negative_button.m_is_down = false;
    m_alt_negative_button.m_is_down = false;
    m_axis_active = false;
}

void AxisMap::on_button_down(Engine& engine) {
    if (m_button_down_count == 0 && m_enabled && m_on_down) {
        m_on_down(engine, *this);
    }
    m_button_down_count++;
}

void AxisMap::on_button_up(Engine& engine) {
    modus_assert(m_button_down_count > 0);
    if (m_button_down_count == 1 && m_enabled && m_on_up) {
        m_on_up(engine, *this);
    }
    m_button_down_count--;
}

InputMapper::InputMapper() {}

Result<NotMyPtr<AxisMap>, void> InputMapper::action(const u32 action_index) {
    if (action_index >= m_actions.size()) {
        return Error<>();
    }
    return Ok(NotMyPtr<AxisMap>(&m_actions[action_index]));
}

Result<NotMyPtr<const AxisMap>, void> InputMapper::action(const u32 action_index) const {
    if (action_index >= m_actions.size()) {
        return Error<>();
    }
    return Ok(NotMyPtr<const AxisMap>(&m_actions[action_index]));
}

Result<> InputMapper::set_enabled(const u32 action_index, const bool value) {
    if (action_index >= m_actions.size()) {
        return Error<>();
    }
    m_actions[action_index].set_enabled(value);
    return Ok<>();
}

void InputMapper::on_frame_begin(Engine& engine) {
    for (auto& action : m_actions) {
        action.on_frame_begin(engine);
    }
}

bool InputMapper::on_event(Engine& engine, const app::InputEvent& event) {
    for (auto& action : m_actions) {
        if (action.on_event(engine, event)) {
            return true;
        }
    }
    return false;
}

void InputMapper::update(Engine& engine) {
    for (auto& action : m_actions) {
        action.on_update(engine);
    }
}

void InputMapper::reset_input_state() {
    for (auto& action : m_actions) {
        action.reset_input_state();
    }
}

void MappedInputContext::handle_frame_begin(Engine& engine) {
    m_input_mapper.on_frame_begin(engine);
}

bool MappedInputContext::handle_event(Engine& engine, const app::InputEvent& event) {
    return m_input_mapper.on_event(engine, event);
}

void MappedInputContext::update(Engine& engine) {
    m_input_mapper.update(engine);
}

}    // namespace v2

}    // namespace modus::engine::input
