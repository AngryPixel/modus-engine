/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/input.hpp>
#include <engine/engine.hpp>
#include <engine/input/basic_input_contexts.hpp>
#include <engine/input/default_input_priorities.hpp>
#include <engine/modules/module_input.hpp>

namespace modus::engine::input {

CtrlAltQuitInputContext::CtrlAltQuitInputContext() : InputContext(kGlobalInputPriorityMax) {}

StringSlice CtrlAltQuitInputContext::name() const {
    return "CtrlAltQuit";
}

bool CtrlAltQuitInputContext::handle_event(Engine& engine, const app::InputEvent& event) {
    if (event.type == app::InputEventType::Keyboard) {
        auto input_module = engine.module<ModuleInput>();
        auto& input_state = input_module->manager().input_state();
        if (event.keyboard.key == app::InputKey::KeyQ &&
            input_state.is_key_down(app::InputKey::KeyLeftCtrl) &&
            input_state.is_key_down(app::InputKey::KeyLeftAlt)) {
            engine.quit();
            return true;
        }
    }
    return false;
}

}    // namespace modus::engine::input
