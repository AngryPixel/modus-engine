/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/input/input_state.hpp>

namespace modus::engine::input {

void InputState::update_begin() {
    m_mouse_delta_x = 0;
    m_mouse_delta_y = 0;
    m_previous_mouse_x = m_mouse_x;
    m_previous_mouse_y = m_mouse_y;
}

void InputState::update(const app::InputEvent& event) {
    switch (event.type) {
        case app::InputEventType::Keyboard:
            m_keys_down.set(u32(event.keyboard.key), event.keyboard.is_key_down());
            break;
        case app::InputEventType::MouseButton:
            m_mouse_buttons_down.set(u32(event.mouse_button.button),
                                     event.mouse_button.is_mouse_down());
            break;
        case app::InputEventType::MouseMove:
            m_mouse_x = event.mouse_move.x;
            m_mouse_y = event.mouse_move.y;
            if (m_first_mouse_move) {
                m_previous_mouse_x = m_mouse_x;
                m_previous_mouse_y = m_mouse_y;
                m_first_mouse_move = false;
            }

            // Track mouse delta since during mouse locking/grabbing the mouse
            // will not leave the window boundaries
            m_mouse_delta_x = event.mouse_move.deltax;
            m_mouse_delta_y = event.mouse_move.deltay;
            break;
        default:
            break;
    }
}

void InputState::update_end() {
    // m_mouse_delta_x = m_mouse_x - m_previous_mouse_x;
    // m_mouse_delta_y = -(m_mouse_y - m_previous_mouse_y);
}

bool InputState::is_key_down(const app::InputKey key) const {
    modus_assert(key < app::InputKey::Total);
    return m_keys_down.test(u32(key));
}

bool InputState::is_mouse_botton_down(const app::MouseButton button) const {
    modus_assert(button < app::MouseButton::Total);
    return m_mouse_buttons_down.test(u32(button));
}

}    // namespace modus::engine::input
