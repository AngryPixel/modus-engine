/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/input/input_manager.hpp>

namespace modus::engine::input {

StringSlice NullInputContext::name() const {
    return "Null";
}

bool NullInputContext::handle_event(Engine&, const app::InputEvent&) {
    return false;
}

InputManager::InputManager() {
    constexpr usize kVectorReserveAmount = 16;
    m_contexts.reserve(kVectorReserveAmount);
}

void InputManager::add_context(NotMyPtr<InputContext> context) {
    modus_assert(context);
    auto it = std::find(m_contexts.begin(), m_contexts.end(), context);
    if (it == m_contexts.end()) {
        m_contexts.push_back(context);
        std::sort(m_contexts.begin(), m_contexts.end(), [](const PtrType& v1, const PtrType& v2) {
            return v1->priority() < v2->priority();
        });
    }
}

void InputManager::remove_context([[maybe_unused]] NotMyPtr<InputContext> context) {
    modus_assert(context);
    auto it = std::find(m_contexts.begin(), m_contexts.end(), context);
    if (it != m_contexts.end()) {
        m_contexts.erase(it);
    }
}

bool InputManager::on_event(Engine& engine, const app::InputEvent& event) {
    MODUS_PROFILE_INPUT_BLOCK("InputManager::on_event");
    m_state.update(event);

    for (auto& context : m_contexts) {
        if (context->enabled() && context->handle_event(engine, event)) {
            return true;
        }
    }
    return false;
}

void InputManager::update_begin(Engine& engine) {
    MODUS_PROFILE_INPUT_BLOCK("InputManager::update_begin");
    m_state.update_begin();
    for (auto& context : m_contexts) {
        context->handle_frame_begin(engine);
    }
}

void InputManager::update(Engine& engine) {
    MODUS_PROFILE_INPUT_BLOCK("InputManager::update");
    for (auto& context : m_contexts) {
        context->update(engine);
    }
}

void InputManager::update_end(Engine& engine) {
    MODUS_UNUSED(engine);
    MODUS_PROFILE_INPUT_BLOCK("InputManager:update_end");
    m_state.update_end();
}

}    // namespace modus::engine::input
