/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format on
#include <pch.h>
// clang-format off

#include <engine/modules/module_list.hpp>
#include <core/topo_sort.hpp>

namespace modus::engine {
namespace module_detail {
u32 generate_module_index() {
    static u32 kModuleCounter = 0;
    const u32 new_module_index = kModuleCounter++;
    modus_assert_message(new_module_index<ModuleList::kMaxModuleCount,
    "Maximum number of modules reached. Please increase limit.");
    return new_module_index;
}
}

using TopoSorter = TopologicalSorter<Module, GID>;

ModuleList::ModuleList() {
}

ModuleList::~ModuleList() {

}

Result<> ModuleList::register_module(NotMyPtr<Module> module, const u32 module_index)
{
    if (module_index >= kMaxModuleCount) {
        MODUS_LOGE("Module index {} is greater than current limit {}", module_index, kMaxModuleCount);
        return Error<>();
    }

    if (m_module_list[module_index]) {
        MODUS_LOGE("Module with index ({}) already exists and has name: {}", module_index, m_module_list[module_index]->name());
        return Error<>();
    }

    if (auto r = this->module_with_guid(module->guid()); r) {
        MODUS_LOGE("Module with GUID ({}) already exists and has name: {}", (*r)->guid(), (*r)->name());
        return Error<>();
    }
    m_module_list[module_index] = module;
    return Ok<>();
}


Result<> ModuleList::initialize(Engine& engine) {
    MODUS_PROFILE_ENGINE_BLOCK("ModueList::initialize")
    m_init_count = 0;
    m_init_order.clear();
    m_update_order.clear();

    TopoSorter sorter;
    for (auto& mod : m_module_list) {
        if (!mod) { continue;}
        if (sorter.add(mod, mod->guid(), mod->module_dependencies()) != TopologicalSortError::None) {
            MODUS_LOGE("Failed to add module to sorter: {} :: {}", mod->guid(), mod->name());
            return Error<>();
        }
    }

    Vector<NotMyPtr<Module>> final_list;
    const TopologicalSortError error = sorter.evaluate(final_list);
    if (error != TopologicalSortError::None) {
        if (error == TopologicalSortError::DependencyNotFound) {
            MODUS_LOGE("A module dependency has not been found");
        } else if (error == TopologicalSortError::DependencyCycle) {
            MODUS_LOGE("There's a cyclic dependency in the current module list.");
            MODUS_LOGE("Detected while processing {} with dependency {}.",
                       sorter.node_being_processed_when_cycle_was_detected().value_or(GID()),
                       sorter.node_where_cycle_was_detected().value_or(GID()));
        } else {
            MODUS_LOGE("Uknown error during dependency order evaluation");
        }
        return Error<>();
    }

    if (final_list.size() >= m_init_order.capacity()) {
        MODUS_LOGE("{} modules detected, but we can only track {} at the moment. Update the capacity",
                   final_list.size(), m_init_order.capacity());
        return Error<>();
    }

    MODUS_LOGD("Module Init Order:");
    {
    u32 idx = 0;
    for (auto& ptr : final_list) {
        MODUS_LOGD("    [{:02}] {} :: {}", idx, ptr->guid(), ptr->name());
        m_init_order.push_back(ptr).expect("Failed to instet module in init list");
        ++idx;
    }
    }

    for (auto& module : m_init_order) {
        if (module->update_category() != ModuleUpdateCategory::Total) {
            m_update_order.push_back(module).expect("Failed to insert module in update list");
        }
    }
    std::sort(m_update_order.begin(), m_update_order.end(), [](const NotMyPtr<Module>& v1, const NotMyPtr<Module>& v2) {
       return v1->update_category() < v2 ->update_category();
    });

    if (log::LogService::level() == log::Level::Debug) {
        MODUS_LOGD("Module Update Order:");
        u32 idx = 0;
        for (auto& ptr : m_update_order) {
            MODUS_LOGD("    [{:02}] {} :: {}", idx, ptr->guid(), ptr->name());
            ++idx;
        }
    }

    for (auto& module : m_init_order) {
        if (!module->initialize(engine)) {
            MODUS_LOGE("Failed to initialize module: {} :: {}", module->guid(), module->name());
            (void)module->shutdown(engine);
            return Error<>();
        }
        m_init_count++;
    }
    return Ok<>();
}

Result<> ModuleList::shutdown(Engine& engine) {
    bool succeeded = true;
    for (u32 i = 0; i < m_init_count && m_init_order.size(); i++) {
        const usize index = m_init_count - 1 - i;
        if (m_init_order[index] != nullptr) {
            if (!m_init_order[index]->shutdown(engine)) {
                MODUS_LOGE("Failed to shudown engine module '{}'"
                        , m_init_order[index]->name());
                succeeded = false;
            }
        }
    }
    m_init_order.clear();
    return succeeded
        ? Result<>(Ok<>())
        : Result<>(Error<>());
}

void ModuleList::tick(Engine& engine) {
    for(auto& module : m_update_order) {
        module->tick(engine);
    }
}

Result<NotMyPtr<Module>,void> ModuleList::module_with_guid(const GID& guid) {
    for (auto& module : m_module_list) {
        if (module && module->guid() == guid) {
            return Ok(module);
        }
    }
    return Error<>();
}

Result<NotMyPtr<const Module>,void> ModuleList::module_with_guid(const GID& guid) const {
    for (auto& module : m_module_list) {
        if (module && module->guid() == guid) {
            return Ok(NotMyPtr<const Module>(module));
        }
    }
    return Error<>();
}
} // namespace modus::engine
