/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/event/engine_events.hpp>
#include <engine/event/event_manager.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_event.hpp>
#include <engine/modules/module_input.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleInput, "bc44de95-6883-420f-8be6-8f033a08a7db");

ModuleInput::ModuleInput()
    : Module(module_guid<ModuleInput>(), "Input", ModuleUpdateCategory::Input) {}

Result<> ModuleInput::initialize(Engine& engine) {
    MODUS_PROFILE_INPUT_BLOCK("ModuleInput::initialize")
    auto app_module = engine.module<ModuleApp>();
    if (!app_module) {
        MODUS_LOGE("Expected App module {} is not in module list {}", module_guid<ModuleApp>());
        return Error<>();
    }

    auto listener_on_frame_begin =
        event::EventListener::build<ModuleInput, &ModuleInput::on_begin_frame>(this);

    auto listener_on_input_event =
        event::EventListener::build<ModuleInput, &ModuleInput::on_app_input>(this);

    auto module_event = engine.module<ModuleEvent>();
    if (!module_event) {
        MODUS_LOGE("ModuleInput: Event module is not present");
        return Error<>();
    }

    if (auto r = (module_event)
                     ->manager()
                     .register_event_listener((module_event)->engine_events().begin_frame,
                                              listener_on_frame_begin);
        !r) {
        MODUS_LOGE("ModuleInput: Failed to register begin frame event listener");
        return Error<>();
    } else {
        m_frame_begin_listener_handle = *r;
    }

    if (auto r = module_event->manager().register_event_listener((app_module)->input_event_handle(),
                                                                 listener_on_input_event);
        !r) {
        MODUS_LOGE("ModuleInput: Failed to register app input event listener");
        return Error<>();
    } else {
        m_app_input_listener_handle = *r;
    }
    return Ok<>();
}

Result<> ModuleInput::shutdown(Engine& engine) {
    MODUS_PROFILE_INPUT_BLOCK("ModuleInput::shutdown")
    if (m_frame_begin_listener_handle) {
        auto module_event = engine.module<ModuleEvent>();
        if (!module_event->manager().unregister_event_listner(
                module_event->engine_events().begin_frame, m_frame_begin_listener_handle)) {
            MODUS_LOGE(
                "ModuleInput: Failed to unregister begin frame event "
                "listener");
        }
    }
    if (m_app_input_listener_handle) {
        auto module_event = engine.module<ModuleEvent>();
        auto module_app = engine.module<ModuleApp>();
        if (!module_event->manager().unregister_event_listner(module_app->input_event_handle(),
                                                              m_app_input_listener_handle)) {
            MODUS_LOGE(
                "ModuleInput: Failed to unregister app input event "
                "listener");
        }
    }
    return Ok<>();
}

void ModuleInput::tick(Engine& engine) {
    MODUS_PROFILE_INPUT_BLOCK("ModuleInput::tick")
    m_input_manager.update(engine);
}

Slice<GID> ModuleInput::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleEvent>(), module_guid<ModuleApp>()};
    return Slice(deps);
}

void ModuleInput::on_begin_frame(Engine& engine, event::Event&) {
    update_begin(engine);
}

void ModuleInput::on_app_input(Engine& engine, event::Event& event) {
    AppInputEvent& app_input_event = static_cast<AppInputEvent&>(event);
    manager().on_event(engine, app_input_event.input_event());
}

}    // namespace modus::engine
