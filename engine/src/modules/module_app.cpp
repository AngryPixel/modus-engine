/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/modules/module_app.hpp>
#include <engine/engine.hpp>
#include <engine/event/engine_events.hpp>
#include <app/app.hpp>
#include <app/window.hpp>
#include <engine/igame.hpp>

#include <engine/modules/module_event.hpp>

namespace modus::engine {

AppWindowResizeEvent::AppWindowResizeEvent(NotMyPtr<app::Window> window,
                                           const i32 width,
                                           const i32 height)
    : event::Event("AppWindowResize"), m_window(window), m_width(width), m_height(height) {}

AppInputEvent::AppInputEvent(const app::InputEvent& event)
    : event::Event("AppInputEvent"), m_event(event) {}

MODUS_ENGINE_IMPL_MODULE(ModuleApp, "d66b51bd-6df5-4bfd-bad9-6d32cf07b858");

ModuleApp::ModuleApp() : Module(module_guid<ModuleApp>(), "App", ModuleUpdateCategory::App) {}

Result<> ModuleApp::initialize(Engine& engine) {
    MODUS_PROFILE_APP_BLOCK("ModuleApp::initialize");
    MODUS_LOGI("Initializing Application");
    const StringSlice threed_api = engine.cmd_options().rendering.threed_api.value();

    threed::Api api = threed::Api::Null;
    if (threed_api == "gles") {
        api = threed::Api::OpenGLES3;
    } else if (threed_api == "null") {
        // do nothing;
    } else {
        MODUS_LOGE("ModuleApp: Invalid threed-api:{}", threed_api);
        return Error<>();
    }

    m_app = modus::app::create_app();
    if (!m_app) {
        MODUS_LOGE("Failed to create App instance");
        return Error<>();
    }

    app::AppInitParams app_init_params;
    app_init_params.name = engine.game().name();
    app_init_params.name_preferences = engine.game().name_preferences();
    app_init_params.event_handler.bind<ModuleApp, &ModuleApp::on_app_event>(this);
    app_init_params.threed_api = api;
    auto app_init_result = m_app->initialize(app_init_params);
    if (!app_init_result) {
        MODUS_LOGE("Failed to initialize App: {}", app_init_result.error());
        return Error<>();
    }

    auto event_module = engine.module<ModuleEvent>();
    if (!event_module) {
        MODUS_LOGE("Event module {} not in module list", module_guid<ModuleEvent>());
        return Error<>();
    }

    if (auto r_event = event_module->manager().create_event_type(); !r_event) {
        MODUS_LOGE("Failed to create app window event type");
        return Error<>();
    } else {
        m_window_resize_event_handle = *r_event;
    }

    if (auto r_event = event_module->manager().create_event_type(); !r_event) {
        MODUS_LOGE("Failed to create app input event type");
        return Error<>();
    } else {
        m_input_event_handle = *r_event;
    }

    m_engine = &engine;
    return Ok<>();
}

ModuleApp::~ModuleApp() {}

Result<> ModuleApp::shutdown(Engine& engine) {
    MODUS_PROFILE_APP_BLOCK("ModuleApp::shutdown");
    auto event_module = engine.module<ModuleEvent>();
    if (event_module) {
        if (m_window_resize_event_handle) {
            (void)(event_module)->manager().destroy_event_type(m_window_resize_event_handle);
        }

        if (m_input_event_handle) {
            (void)(event_module)->manager().destroy_event_type(m_input_event_handle);
        }
    }

    // Shutdown app
    MODUS_LOGI("Application shutdown");
    if (auto app_shutdown_result = m_app->shutdown(); !app_shutdown_result) {
        MODUS_LOGE("Failed to shutdown App: {}", app_shutdown_result.error());
    }
    m_engine = NotMyPtr<Engine>();
    m_app.reset();
    return Ok<>();
}

void ModuleApp::tick(Engine& engine) {
    MODUS_PROFILE_APP_BLOCK("ModuleApp::tick");
    // tick app
    if (m_app->tick() == app::TickResult::Quit) {
        engine.quit();
    }
}

bool ModuleApp::on_app_event(const app::Event& event) {
    MODUS_PROFILE_APP_BLOCK("ModuleApp::on_app_event");
    auto event_module = m_engine->module<ModuleEvent>();

    switch (event.type) {
            // TODO: Sever this connection
        case app::EventType::Input: {
            AppInputEvent input_event(event.input);
            event_module->manager().trigger(*m_engine, m_input_event_handle, input_event);
            return true;
        }
        case app::EventType::Window: {
            switch (event.window.type) {
                case app::WindowEventType::Resize: {
                    modus_assert(event.window.window != nullptr);
                    AppWindowResizeEvent resize_event(
                        event.window.window, event.window.resize.width, event.window.resize.height);
                    event_module->manager().trigger(*m_engine, m_window_resize_event_handle,
                                                    resize_event);
                    return true;
                }
                default:
                    return false;
            }
        }
        default:
            return false;
    }
}

Slice<GID> ModuleApp::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleEvent>()};
    return Slice(deps);
}

}    // namespace modus::engine
