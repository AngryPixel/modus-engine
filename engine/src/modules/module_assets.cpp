/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_parallel.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleAssets, "7589ea1b-c355-4a1b-87e9-218294102676");

ModuleAssets::ModuleAssets()
    : Module(module_guid<ModuleAssets>(), "Assets", ModuleUpdateCategory::Assets) {}

Result<> ModuleAssets::initialize(Engine& engine) {
    MODUS_PROFILE_ASSETS_BLOCK("ModuleAssets::initialize");
    if (auto r_fs = engine.module<ModuleFileSystem>(); !r_fs) {
        MODUS_LOGE("Expected File System module {} is not in module list {}",
                   module_guid<ModuleFileSystem>());
        return Error<>();
    }

    if (auto r = engine.module<ModuleParallel>(); !r) {
        MODUS_LOGE("Expected Parallel module {} is not in module list {}",
                   module_guid<ModuleParallel>());
        return Error<>();
    }

    return m_loader.initialize(engine);
}

Result<> ModuleAssets::shutdown(Engine&) {
    MODUS_PROFILE_ASSETS_BLOCK("ModuleAssets::shutdown");
    m_loader.shutdown();
    return Ok<>();
}

void ModuleAssets::tick(Engine& engine) {
    MODUS_PROFILE_ASSETS_BLOCK("ModuleAssets::tick");
    m_loader.update(engine);
}

Slice<GID> ModuleAssets::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleFileSystem>(), module_guid<ModuleParallel>()};
    return Slice(deps);
}

}    // namespace modus::engine
