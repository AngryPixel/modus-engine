/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/modules/module_parallel.hpp>
// clang-format on

#include <engine/engine.hpp>

namespace modus::engine {

constexpr usize kCommandQueueSize = 1024 * 1024;

MODUS_ENGINE_IMPL_MODULE(ModuleParallel, "d543bdcd-f95c-47b2-9aa0-869ae6fe600a");

ModuleParallel::ModuleParallel()
    : Module(module_guid<ModuleParallel>(), "Parallel", ModuleUpdateCategory::App) {}

ModuleParallel::~ModuleParallel() {}

Result<> ModuleParallel::initialize(Engine&) {
    MODUS_PROFILE_PARALLEL_BLOCK("ModuleParallel::initialize");
    m_active_queue = 0;
    for (u32 i = 0; i < kQueueCount; ++i) {
        if (!m_queues[i].initialize(kCommandQueueSize)) {
            MODUS_LOGE("Failed to initialize command queue {}", i);
            return Error<>();
        }
    }

    if (!m_job_system.initialize(Optional<u32>())) {
        MODUS_LOGE("Failed to initialize job system");
        return Error<>();
    }
    return Ok<>();
}

Result<> ModuleParallel::shutdown(Engine&) {
    MODUS_PROFILE_PARALLEL_BLOCK("ModuleParallel::shutdown");
    m_job_system.shutdown();

    for (u32 i = 0; i < kQueueCount; ++i) {
        m_queues[i].shutdown();
    }

    return Ok<>();
}

void ModuleParallel::tick(Engine& engine) {
    MODUS_PROFILE_PARALLEL_BLOCK("ModuleParallel::tick");
    const u32 command_queue_index = m_active_queue.fetch_add(1) % kQueueCount;
    {
        MODUS_PROFILE_PARALLEL_BLOCK("Queued Commands");
        m_queues[command_queue_index].execute(engine);
    }
}

Slice<GID> ModuleParallel::module_dependencies() const {
    return Module::module_dependencies();
}
}    // namespace modus::engine
