/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/modules/module_event.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleEvent, "caacce8e-9f69-497e-ae8d-bbc10879d2ee");

ModuleEvent::ModuleEvent()
    : Module(module_guid<ModuleEvent>(), "Event", ModuleUpdateCategory::Event) {}

Result<> ModuleEvent::initialize(Engine& engine) {
    MODUS_PROFILE_EVENT_BLOCK("ModuleEvent::initialize");
    MODUS_UNUSED(engine);
    m_engine_events.begin_frame =
        m_manager.create_event_type().value_or_panic("Failed to register event type");
    m_engine_events.end_frame =
        m_manager.create_event_type().value_or_panic("Failed to register event type");
    return Ok<>();
}

Result<> ModuleEvent::shutdown(Engine&) {
    MODUS_PROFILE_EVENT_BLOCK("ModuleEvent::shutdown");
    m_manager.clear();
    return Ok<>();
}

void ModuleEvent::tick(Engine& engine) {
    MODUS_PROFILE_EVENT_BLOCK("ModuleEvent::update");
    m_manager.update(engine);
}

}    // namespace modus::engine
