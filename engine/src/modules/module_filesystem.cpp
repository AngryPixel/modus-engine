/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <engine/engine.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <os/time.hpp>

namespace modus::engine {

static constexpr const char* k_tag_tmp = "tmp";
static constexpr const char* k_tag_resources = "data";
static constexpr const char* k_tag_pref_config = "pref";
static constexpr const char* k_tag_pref_data = "prefdata";
static constexpr const char* k_tag_cache = "cache";

MODUS_ENGINE_IMPL_MODULE(ModuleFileSystem, "a1791d82-e02d-4c43-a68c-30741e7073af");

ModuleFileSystem::ModuleFileSystem()
    : Module(module_guid<ModuleFileSystem>(), "FileSystem", ModuleUpdateCategory::Total) {}

Result<> ModuleFileSystem::initialize(Engine& engine) {
    MODUS_PROFILE_VFS_BLOCK("ModuleFileSystem::initialize");
    MODUS_LOGV("Initializing File System");

    auto app_module = engine.module<ModuleApp>();
    if (!app_module) {
        MODUS_LOGE("Expected App module {} is not in module list {}", module_guid<ModuleApp>());
        return Error<>();
    }

    const app::App& app = app_module->app();

    auto mount_call = [this](const StringSlice& tag, const StringSlice& path,
                             const bool read_only) -> Result<> {
        vfs::VFSMountParams params;
        params.read_only = read_only;
        params.tag = tag;
        params.path = path;
        auto mount_result = m_file_system.mount_path(params);
        if (!mount_result) {
            MODUS_LOGE("Failed to mount {} path:{}", tag, path);
            return Error<>();
        }
        MODUS_LOGD("Mounted '{:8}' ({:9}) with path: {}", tag, read_only ? "read only" : "writable",
                   path);
        return Ok<>();
    };

    if (!mount_call(k_tag_tmp, app.path_tmp(), false)) {
        return Error<>();
    }

    if (!mount_call(k_tag_resources, app.path_resources(), true)) {
        return Error<>();
    }

    if (!mount_call(k_tag_cache, app.path_cache(), false)) {
        return Error<>();
    }

    if (!mount_call(k_tag_pref_config, app.path_pref_config(), false)) {
        return Error<>();
    }

    if (!mount_call(k_tag_pref_data, app.path_pref_data(), false)) {
        return Error<>();
    }

    MODUS_LOGV("File System Initialized");
    return Ok<>();
}

Result<> ModuleFileSystem::shutdown(Engine&) {
    MODUS_PROFILE_VFS_BLOCK("ModuleFileSystem::shutdown");
    MODUS_LOGV("Shutting down File System");

    u32 attempt_count = 0;
    do {
        // Attempt to unmount file system, if there are still resources
        // being used, sleep and try again up to 5 times.
        auto result = m_file_system.unmount_all();
        if (!result) {
            ++attempt_count;
            os::sleep_sec(ISeconds(5));
        } else {
            break;
        }
    } while (attempt_count < 5);

    if (attempt_count >= 5) {
        MODUS_LOGE(
            "Could not properly shutdown file system. There may still be "
            "active resoures somewhere.");
    }

    MODUS_LOGV("File System Sutdown complete");
    return Ok<>();
}

Slice<GID> ModuleFileSystem::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleApp>()};
    return Slice(deps);
}
}    // namespace modus::engine
