/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <engine/engine.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_config.hpp>
#include <os/file.hpp>
#include <os/path.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleConfig, "f1990955-2545-4090-a3e5-ce1fba27a244");

ModuleConfig::ModuleConfig()
    : Module(module_guid<ModuleConfig>(), "Config", ModuleUpdateCategory::Total) {}

Result<> ModuleConfig::initialize(Engine& engine) {
    auto app_module = engine.module<ModuleApp>();
    if (!app_module) {
        MODUS_LOGE("Expected App module {} is not in module list {}", module_guid<ModuleApp>());
        return Error<>();
    }

    os::Path::join_inplace(m_config_path, (app_module)->app().path_pref_config(), "engine.cfg");
    m_config_path_tmp = m_config_path;
    m_config_path_tmp += ".tmp";

    os::FileBuilder builder;
    auto open_result = builder.read().open(m_config_path);
    if (!open_result) {
        MODUS_LOGW("Failed to locate config file '{}', settings will be empty.", m_config_path);
    } else {
        MODUS_LOGV("Loading config file '{}'", m_config_path);
        os::File file = open_result.release_value();
        if (!m_cfg.load_from(file)) {
            MODUS_LOGE("Failed to parse configuration file. File will be erased.");
            if (!os::Path::remove(m_config_path)) {
                MODUS_LOGE("Failed to remove corrupt configuration file. Now What?");
            }
        }
    }
    return Ok<>();
}

Result<> ModuleConfig::write_to_disk() {
    os::FileBuilder builder;
    auto open_result = builder.write().create().open(m_config_path_tmp);
    if (!open_result) {
        MODUS_LOGE("Failed to create tmp config file: {}", m_config_path_tmp);
        return Error<>();
    }

    {
        os::File file = open_result.release_value();
        auto save_result = m_cfg.save_to(file);
        if (!save_result) {
            MODUS_LOGE("Failed to write config file");
            return Error<>();
        }
        file.flush();
    }

    if (!os::Path::rename(m_config_path_tmp, m_config_path)) {
        MODUS_LOGE("Failed to move written config file into place");
        return Error<>();
    }
    return Ok<>();
}

Result<> ModuleConfig::shutdown(Engine&) {
    if (m_cfg.has_changed()) {
        MODUS_LOGV("Configuration changes detected, writing to disk.");
        return write_to_disk();
    }
    return Ok<>();
}

Slice<GID> ModuleConfig::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleApp>()};
    return Slice(deps);
}
}    // namespace modus::engine
