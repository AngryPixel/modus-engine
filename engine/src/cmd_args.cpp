/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/cmd_args.hpp>

namespace modus::engine {

CmdOptsProfiling::CmdOptsProfiling()
    : enabled_local("", "--profile", "Enabe local profiling capture."),
      output_file_path("",
                       "--profile-output",
                       "Path to file where profile data will be stored",
                       "profile.prof"),
      enabled_remote("", "--profile-server", "Enable remote profile server."),
      server_port("",
                  "--profile-server-port",
                  "Port on which to create the server for remote profiling.",
                  28077) {}

CmdOptsLogging::CmdOptsLogging()
    : level("", "--log-level", "Log verbosity: [debug|error|info|warn|verbose].", "") {}

CmdOptsRendering::CmdOptsRendering()
    : threed_api("",
                 "--threed-api",
                 "Select threed rendering api [null|gles]. Default = gles",
                 "gles") {}

CmdOptions::CmdOptions() {}

Result<> add_to_option_parser(CmdOptionParser& parser, CmdOptions& options) {
    if (!parser.add(options.logging.level)) {
        return Error<>();
    }

    if (!parser.add(options.rendering.threed_api)) {
        return Error<>();
    }

#if defined(MODUS_PROFILER_USE_EASY_PROFILER)
    if (!parser.add(options.profiling.enabled_local)) {
        return Error<>();
    }

    if (!parser.add(options.profiling.output_file_path)) {
        return Error<>();
    }

    if (!parser.add(options.profiling.enabled_remote)) {
        return Error<>();
    }

    if (!parser.add(options.profiling.server_port)) {
        return Error<>();
    }
#endif

    return Ok<>();
}

}    // namespace modus::engine
