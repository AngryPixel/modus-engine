/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <os/os_pch.h>
#include <flatc_package_command.hpp>
// clang-format on
#include <os/path.hpp>
#include <package/status_reporter.hpp>

namespace modus {

Result<> FlatcCommandBase::execute(package::StatusReporter& reporter,
                                   const package::CommandContext& context) const {
    String flatc_ouput = context.output.to_str();
    flatc_ouput += ".bin";
    const String output_directory = os::Path::get_path(context.output).to_str();
    // Execute flatc
    auto r_exec = package::detail::execute_process(m_args, output_directory.c_str(), reporter);
    if (!r_exec) {
        return r_exec;
    }

    // Check if flatc output actually exists
    if (!os::Path::is_file(flatc_ouput)) {
        package::ReportError(reporter, "Flatc Command({}) expected flatc output does not exist: {}",
                             type(), flatc_ouput);
        return Error<>();
    }

    // Rename .bin to final output
    if (auto r = os::Path::rename(flatc_ouput, context.output); !r) {
        package::ReportError(reporter, "Flatc Command({}) Failed to move {} to {} : {}", type(),
                             flatc_ouput, context.output, os::path_error_to_str(r.error()));
        return Error<>();
    }
    return Ok<>();
}
u64 FlatcCommandBase::command_hash(const package::CommandContext&) const {
    Hasher64 hasher;
    for (const auto& arg : m_args) {
        hasher.update(arg.data(), arg.size());
    }
    return hasher.digest();
}

Vector<String> FlatcCommandBase::command_dependencies() const {
    return {String(m_args[4]), String(m_args.back())};
}

FlatcCommandCreatorBase::FlatcCommandCreatorBase(const StringSlice fbs_idl_path)
    : m_flatc_bin(), m_fbs_idl_relative_path(fbs_idl_path.to_str()), m_fbs_include_path() {}

Result<> FlatcCommandCreatorBase::initialize(package::StatusReporter& reporter) {
    auto r_exec_dir = os::Path::executable_directory();
    if (!r_exec_dir) {
        package::ReportError(reporter, "Failed to get executable directory");
        return Error<>();
    }

#if defined(MODUS_OS_WIN32)
    constexpr const char* kExecName = "flatc.exe";
#else
    constexpr const char* kExecName = "flatc";
#endif

    String flatc_bin = os::Path::join(*r_exec_dir, kExecName);
    if (!os::Path::is_executable(flatc_bin)) {
        package::ReportError(reporter, "Could not locate flatc executable: {}", flatc_bin);
        return Error<>();
    }

    String fbs_include_path = os::Path::join(*r_exec_dir, "fbs");
    if (!os::Path::is_directory(fbs_include_path)) {
        package::ReportError(reporter, "Could not locate fbs include directory: {}",
                             fbs_include_path);
        return Error<>();
    }

    String fbs_file_path = os::Path::join(fbs_include_path, m_fbs_idl_relative_path);
    if (!os::Path::is_file(fbs_file_path)) {
        package::ReportError(reporter, "Could not locate fbs idl file: {}", fbs_file_path);
        return Error<>();
    }

    m_flatc_bin = std::move(flatc_bin);
    m_fbs_include_path = std::move(fbs_include_path);
    m_fbs_idl_full_path = std::move(fbs_file_path);
    return Ok<>();
}

Result<package::ICommand*> FlatcCommandCreatorBase::create(
    package::StatusReporter& reporter,
    const package::CommandCreateParams& params) {
    package::ReportDebug(reporter, "Creating Flatc Command {}", type());
    static constexpr const char* kInputElem = "input";

    auto r_input = params.parser.read_string(kInputElem);
    if (!r_input) {
        package::ReportError(reporter, "FlatcCommandCreator({}): {}", type(), r_input.error());
        return Error<>();
    }

    std::vector<std::string> args;
    args.reserve(5);
    args.push_back(std::string(m_flatc_bin));
    args.push_back("-I");
    args.push_back(std::string(m_fbs_include_path));
    args.push_back("-b");
    args.push_back(std::string(m_fbs_idl_full_path));
    args.push_back(std::string(os::Path::join(params.current_directory, *r_input)));

    auto command = new_command();
    modus_assert(command != nullptr);
    command->m_args = std::move(args);
    return Ok<package::ICommand*>(command);
}

}    // namespace modus
