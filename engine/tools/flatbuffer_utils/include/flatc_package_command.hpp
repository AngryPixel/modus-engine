/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <package/command.hpp>

namespace modus::package {
class StatusReporter;
}

namespace modus {

class FlatcCommandBase : public package::ICommand {
    friend class FlatcCommandCreatorBase;

   private:
    std::vector<std::string> m_args;

   public:
    Result<> execute(package::StatusReporter& reporter,
                     const package::CommandContext& context) const override final;

    u64 command_hash(const package::CommandContext& context) const final;

    Vector<String> command_dependencies() const override final;
};

class FlatcCommandCreatorBase : public package::ICommandCreator {
   protected:
    String m_flatc_bin;
    const String m_fbs_idl_relative_path;
    String m_fbs_idl_full_path;
    String m_fbs_include_path;

   public:
    FlatcCommandCreatorBase() = delete;
    FlatcCommandCreatorBase(const StringSlice fbs_idl_path);

    Result<> initialize(package::StatusReporter& reporter) override final;

    Result<package::ICommand*> create(package::StatusReporter& reporter,
                                      const package::CommandCreateParams& params) override final;

   protected:
    virtual FlatcCommandBase* new_command() = 0;
};

}    // namespace modus
