/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// clang-format off
#include <app/app_pch.h>
#include <config/config_pch.h>
#include <core/core_pch.h>
#include <log/log_pch.h>
#include <os/os_pch.h>
#include <vfs/vfs_pch.h>
#include <jobsys/jobsys_pch.h>
#include <tuple>
// clang-format on

#include <log/log.hpp>
#include <profiler/profiler.hpp>

#define MODUS_PROFILE_ENGINE_BLOCK(name) \
    MODUS_PROFILE_BLOCK(name, modus::profiler::color::LightBlue)
#define MODUS_PROFILE_ENGINE_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::LightBlue)

#define MODUS_PROFILE_ASSETS_BLOCK(name) MODUS_PROFILE_BLOCK(name, modus::profiler::color::Amber)
#define MODUS_PROFILE_ASSETS_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::Amber)

#define MODUS_PROFILE_INPUT_BLOCK(name) MODUS_PROFILE_BLOCK(name, modus::profiler::color::BlueGrey)
#define MODUS_PROFILE_INPUT_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::BlueGrey)

#define MODUS_PROFILE_VFS_BLOCK(name) MODUS_PROFILE_BLOCK(name, modus::profiler::color::Olive)
#define MODUS_PROFILE_VFS_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::Olive)

#define MODUS_PROFILE_EVENT_BLOCK(name) MODUS_PROFILE_BLOCK(name, modus::profiler::color::Green)
#define MODUS_PROFILE_EVENT_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::Green)

#define MODUS_PROFILE_APP_BLOCK(name) MODUS_PROFILE_BLOCK(name, modus::profiler::color::Navy)
#define MODUS_PROFILE_APP_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::Navy)

#define MODUS_PROFILE_PARALLEL_BLOCK(name) \
    MODUS_PROFILE_BLOCK(name, modus::profiler::color::BlueGrey800)
#define MODUS_PROFILE_PARALLEL_SCOPE() MODUS_PROFILE_SCOPE(modus::profiler::color::BlueGrey800)
