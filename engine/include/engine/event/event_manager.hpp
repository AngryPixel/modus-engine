/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/containers.hpp>
#include <core/delegate.hpp>
#include <core/fixed_handle_pool.hpp>
#include <engine/event/event_types.hpp>
#include <os/thread.hpp>

#if defined(MODUS_DEBUG)
#define MODUS_ENGINE_EVENT_DEBUG
#endif

#if defined(MODUS_ENGINE_EVENT_DEBUG)
#include <core/fixed_stack_string.hpp>
#endif

namespace modus::engine {
class Engine;
}

namespace modus::engine::event {

class MODUS_ENGINE_EXPORT Event {
    friend class EventManager;

   private:
#if defined(MODUS_ENGINE_EVENT_DEBUG)
    static constexpr usize kMaxDebugStringLen = 64;
    FixedStackString<64> m_debug_name;
#endif
    mutable EventHandle m_event_handle;

   public:
    Event(const StringSlice debug_text);

    virtual ~Event() = default;

    EventHandle event_handle() const { return m_event_handle; }
};

using EventListener = Delegate<void(Engine&, Event&)>;

class MODUS_ENGINE_EXPORT EventManager final {
   private:
    static constexpr usize kMaxEventTypes = 32;
    static constexpr usize kMaxListeners = 16;
    static constexpr usize kMaxEventQueues = 2;

    struct QueuedRegistration {
        EventHandle type;
        ListenerHandle handle;
        EventListener listener;
    };

    struct QueuedUnregistration {
        EventHandle type;
        ListenerHandle handle;
    };

    using EventListenerList = FixedHandlePool<EventListener, kMaxListeners, ListenerHandle>;
    using EventPool = FixedHandlePool<EventListenerList, kMaxEventTypes, EventHandle>;
    using EventQueue = event::Vector<Event*>;
    using QueuedRegistrations = event::Vector<QueuedRegistration>;
    using QueuedUnregistrations = event::Vector<QueuedUnregistration>;

    EventPool m_event_listeners;
    EventQueue m_event_queues[kMaxEventQueues];
    QueuedRegistrations m_queued_registrations;
    QueuedUnregistrations m_queued_unregistrations;
    os::ThreadWatcher m_thread_watcher;
    usize m_active_queue_index;
    bool m_processing_events;

   public:
    EventManager();

    ~EventManager();

    template <typename T>
    void queue_event(const EventHandle handle, T&& event) {
        Event* event_ptr =
            AllocatorTraits<EventAllocator>::allocate_and_construct<T>(std::forward<T>(event));
        modus_assert(event_ptr != nullptr);
        if (event_ptr != nullptr) {
            queue(handle, event_ptr);
        }
    }

    void update(Engine& engine,
                const IMilisec max_duration_ms = IMilisec(std::numeric_limits<i64>::max()));

    Result<EventHandle, void> create_event_type();

    Result<> destroy_event_type(const EventHandle handle);

    Result<ListenerHandle, void> register_event_listener(const EventHandle type,
                                                         EventListener listener);

    Result<> unregister_event_listner(const EventHandle type, const ListenerHandle hanlde);

    void trigger(Engine& engine, const EventHandle handle, Event& event);

    void clear();

   private:
    void queue(const EventHandle handle, Event* event);
};

}    // namespace modus::engine::event
