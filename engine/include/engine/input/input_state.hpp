/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <app/input.hpp>

namespace modus::engine::input {

/// Tracks the global input state so far
class MODUS_ENGINE_EXPORT InputState {
   private:
    std::bitset<u32(app::InputKey::Total)> m_keys_down;
    std::bitset<u32(app::MouseButton::Total)> m_mouse_buttons_down;
    i32 m_previous_mouse_x = 0;
    i32 m_previous_mouse_y = 0;
    i32 m_mouse_x = 0;
    i32 m_mouse_y = 0;
    i32 m_mouse_delta_x = 0;
    i32 m_mouse_delta_y = 0;
    bool m_first_mouse_move = true;

   public:
    InputState() = default;

    void update_begin();

    void update(const app::InputEvent& event);

    void update_end();

    bool is_key_down(const app::InputKey key) const;

    bool is_mouse_botton_down(const app::MouseButton button) const;

    inline i32 mouse_x() const { return m_mouse_x; }

    inline i32 mouse_y() const { return m_mouse_y; }

    i32 mouse_delta_x() const { return m_mouse_delta_x; }

    i32 mouse_delta_y() const { return m_mouse_delta_y; }
};

}    // namespace modus::engine::input
