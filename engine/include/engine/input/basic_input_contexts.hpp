/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/input/input_manager.hpp>

namespace modus::engine::input {

class MODUS_ENGINE_EXPORT CtrlAltQuitInputContext final : public InputContext {
   public:
    CtrlAltQuitInputContext();

    StringSlice name() const override;

    bool handle_event(Engine& engine, const app::InputEvent& event) override;
};

}    // namespace modus::engine::input
