/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <app/input.hpp>
#include <core/fixed_stack.hpp>
#include <engine/input/input_state.hpp>

namespace modus::app {
struct InputEvent;
}

namespace modus::engine {
class Engine;
}

namespace modus::engine::input {

constexpr u32 kInputContextPriorityMax = 0;
constexpr u32 kInputContextPriorityMin = std::numeric_limits<u32>::max();

class MODUS_ENGINE_EXPORT InputContext {
   private:
    u32 m_priority;
    bool m_enabled = true;

   protected:
    InputContext(const u32 priority) : m_priority(priority) {}

   public:
    virtual ~InputContext() = default;

    virtual StringSlice name() const = 0;

    bool enabled() const { return m_enabled; }

    void set_enabled(const bool v) { m_enabled = v; }

    u32 priority() const { return m_priority; }

    void set_priority(const u32 priority) { m_priority = priority; }

    virtual void handle_frame_begin(Engine&) {}

    virtual bool handle_event(Engine& engine, const app::InputEvent& event) = 0;

    virtual void update(Engine&) {}
};

class NullInputContext final : public InputContext {
   public:
    StringSlice name() const override;

    bool handle_event(Engine& engine, const app::InputEvent& event) override;
};

/// Maintains an input context list sorted by priority.
/// Input contexts are identified by they pointer address for addition and
/// removal so please ensure those do not change during the input manager's
/// lifetime.
class MODUS_ENGINE_EXPORT InputManager final {
   private:
    using PtrType = NotMyPtr<InputContext>;
    Vector<PtrType> m_contexts;
    InputState m_state;

   public:
    InputManager();

    MODUS_CLASS_DISABLE_COPY_MOVE(InputManager);

    void add_context(NotMyPtr<InputContext> context);

    void remove_context(NotMyPtr<InputContext> context);

    inline const InputState& input_state() const { return m_state; }

    bool on_event(Engine& engine, const app::InputEvent& event);

    void update_begin(Engine& engine);

    void update(Engine& engine);

    void update_end(Engine& engine);
};

}    // namespace modus::engine::input
