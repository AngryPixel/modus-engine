/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <app/input.hpp>
#include <core/delegate.hpp>
#include <core/fixed_stack_string.hpp>
#include <engine/input/input_manager.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::input {

static constexpr u32 kMaxActionCount = 16;
static constexpr u32 kMaxActionNameLength = 32;

namespace v2 {
enum class ButtonBindingType : u8 { None, KeyBoard, Mouse, GameController };

struct MODUS_ENGINE_EXPORT ButtonBinding {
    ButtonBindingType type = ButtonBindingType::None;
    bool m_is_down;
    union {
        app::InputKey key;
        app::MouseButton mouse_button;
        app::GameControllerButton controller_button;
    };
};

enum class AxisBindingType : u8 {
    None,
    MouseX,
    MouseY,
    MouseScrollX,
    MouseScrollY,
    GameControllerX,
    GameControllerY,
    GameControllerLeftX,
    GameControllerLeftY,
    GameControllerRightX,
    GameControllerRightY,
    GameControllerLeftTrigger,
    GameControllerRightTrigger,
};

class MODUS_ENGINE_EXPORT AxisMap {
   public:
    using Callback = Delegate<void(Engine&, const AxisMap&)>;

   private:
    FixedStackString<kMaxActionNameLength> m_name;
    ButtonBinding m_positive_button;
    ButtonBinding m_negative_button;
    ButtonBinding m_alt_positive_button;
    ButtonBinding m_alt_negative_button;
    f32 m_dead_zone;
    f32 m_sensitivity;
    i32 m_value;
    f32 m_value_normalized;
    i32 m_value_axis;
    f32 m_value_axis_normalized;
    AxisBindingType m_axis_type;
    u8 m_button_down_count;
    bool m_axis_active;
    bool m_invert;
    bool m_enabled;

    Callback m_on_down;
    Callback m_on_up;
    Callback m_on_update;

   public:
    AxisMap();

    void bind_positive_button(const app::InputKey key);
    void bind_positive_button(const app::MouseButton button);
    void bind_positive_button(const app::GameControllerButton button);
    void bind_negative_button(const app::InputKey key);
    void bind_negative_button(const app::MouseButton button);
    void bind_negative_button(const app::GameControllerButton button);
    void bind_alt_positive_button(const app::InputKey key);
    void bind_alt_positive_button(const app::MouseButton button);
    void bind_alt_positive_button(const app::GameControllerButton button);
    void bind_alt_negative_button(const app::InputKey key);
    void bind_alt_negative_button(const app::MouseButton button);
    void bind_alt_negative_button(const app::GameControllerButton button);
    void bind_axis(const AxisBindingType axis);

    void unbind_positive_button();
    void unbind_negative_button();
    void unbind_alt_positive_button();
    void unbind_alt_negative_button();
    void unbind_axis();

    void bind_on_button_down(Callback delegate);
    void bind_on_button_up(Callback delegate);
    void bind_on_update(Callback delegate);

    void set_dead_zone(const f32 value);
    void set_inverted(const bool value);
    void set_sensitivity(const f32 sensitivity);
    void set_enabled(const bool value);
    void set_name(StringSlice name);

    StringSlice name() const { return m_name.as_string_slice(); }
    bool enabled() const { return m_enabled; }
    bool inverted() const { return m_invert; }
    f32 dead_zone() const { return m_dead_zone; }
    f32 sensitivity() const { return m_sensitivity; }

    void on_frame_begin(Engine& engine);
    bool on_event(Engine& engine, const app::InputEvent& event);
    void on_update(Engine& engine);

    i32 value() const;
    f32 value_normalized() const;
    bool is_active() const;

    /// Resets AxisMap input state and binding settings
    void reset();

    /// Only resets input state, but leaves binding and sensitivty options
    /// intact
    void reset_input_state();

   private:
    void on_button_down(Engine& engine);

    void on_button_up(Engine& engine);
};

class MODUS_ENGINE_EXPORT InputMapper {
   private:
    Array<AxisMap, kMaxActionCount> m_actions;

   public:
    InputMapper();

    Result<NotMyPtr<AxisMap>, void> action(const u32 action_index);

    Result<NotMyPtr<const AxisMap>, void> action(const u32 action_index) const;

    Result<> set_enabled(const u32 action_index, const bool value);

    void on_frame_begin(Engine& engine);

    bool on_event(Engine& engine, const app::InputEvent& event);

    void update(Engine& engine);

    u32 action_count() const { return m_actions.size(); }

    void reset_input_state();
};

class MODUS_ENGINE_EXPORT MappedInputContext : public InputContext {
   protected:
    InputMapper m_input_mapper;

   public:
    MappedInputContext(const u32 priority) : InputContext(priority) {}

    ~MappedInputContext() = default;

    virtual void handle_frame_begin(Engine& engine) override;

    virtual bool handle_event(Engine& engine, const app::InputEvent& event) override;

    InputMapper& input_mapper() { return m_input_mapper; }

    const InputMapper& input_mapper() const { return m_input_mapper; }

    void update(Engine& engine) override;
};

}    // namespace v2

using namespace v2;

}    // namespace modus::engine::input
