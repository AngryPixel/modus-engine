/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// Collection of suggested input priority ranges
namespace modus::engine::input {

constexpr u32 kGlobalInputPriorityMax = 100;
constexpr u32 kGlobalInputPriorityMin = 150;

constexpr u32 kDebugInputPriorityMax = 300;
constexpr u32 kDebugInputPriorityMin = 400;

constexpr u32 kUIInputPriorityMax = 500;
constexpr u32 kUIInputPriorityMin = 600;

constexpr u32 kGameInputPriorityMax = 100000;
constexpr u32 kGameInputPriorityMin = 200000;
}    // namespace modus::engine::input