/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <engine/assets/asset_types.hpp>

#include <core/handle_pool.hpp>
#include <engine/assets/asset_types.hpp>
#include <os/thread.hpp>

namespace modus {
class ISeekableReader;
}

namespace modus::engine {
class Engine;
}

//#define MODUS_ENABLE_ASSETS_DEBUG
#if defined(MODUS_ENABLE_ASSETS_DEBUG)
#define MODUS_ASSET_LOADER_LOG(...) MODUS_LOGD(__VA_ARGS__)
#else
#define MODUS_ASSET_LOADER_LOG(...)
#endif

namespace modus::engine::assets {

MODUS_ENGINE_EXPORT StringSlice asset_state_to_string(const AssetStateV2 state);
MODUS_ENGINE_EXPORT StringSlice asset_error_to_string(const AssetErrorV2 state);

struct AsyncCallbackResult {
    StringSlice path;
    AssetHandleV2 handle;
    AssetErrorV2 error;
    NotMyPtr<const AssetBase> asset;
};

/// Asset load completion callback for an asset.
using AssetLoaderCallback = FixedFunction<16, void(Engine&, const AsyncCallbackResult&)>;

struct MODUS_ENGINE_EXPORT AsyncLoadParams {
    StringSlice path;
    AssetLoaderCallback callback;
};

struct AsyncLoadResult {
    AssetHandleV2 handle;
    AssetStateV2 state;
};

class MODUS_ENGINE_EXPORT AssetLoader {
   private:
    static constexpr u32 kQueueCount = 2;
    struct AssetLoaderJob;
    struct AssetInfo {
        AssetHandleV2 handle;
        volatile AssetStateV2 state = AssetStateV2::Unloaded;
        volatile AssetErrorV2 error = AssetErrorV2::None;
        u16 m_ref_count = 0;
        String file_path;
        Vector<AssetLoaderCallback> callbacks;
        std::unique_ptr<AssetData> data;
        NotMyPtr<AssetBase> asset;
        NotMyPtr<AssetFactory> factory;
    };
    using PoolType = HandlePool<AssetInfo, AssetsAllocator, AssetHandleV2>;
    PoolType m_assets;
    HashMapWithAllocator<assets::String, AssetHandleV2, AssetsAllocator> m_path_map;
    Vector<NotMyPtr<AssetFactory>> m_factories;
    Vector<NotMyPtr<AssetInfo>> m_queued_assets[kQueueCount];
    os::ThreadWatcher m_thread_watcher;
    u32 m_active_queue;

   public:
    AssetLoader();

    ~AssetLoader();

    Result<> initialize(Engine& engine);

    void shutdown();

    void update(Engine& engine);

    MODUS_CLASS_DISABLE_COPY_MOVE(AssetLoader);

    Result<AsyncLoadResult> load_async(Engine&, AsyncLoadParams& params);

    void destroy_async(const AssetHandleV2 handle);

    Result<AssetStateV2> asset_state(const AssetHandleV2 handle) const;

    Result<StringSlice> asset_path(const AssetHandleV2 handle) const;

    Result<NotMyPtr<const AssetBase>, AssetStateV2> resolve(const AssetHandleV2 handle) const;

    Result<NotMyPtr<const AssetBase>, AssetStateV2> resolve_by_path(const StringSlice path) const;

    template <typename T>
    Result<NotMyPtr<const T>> resolve_typed(const AssetHandleV2 handle) const {
        auto r = resolve(handle);
        if (!r) {
            return Error<>();
        }
        return assetv2_cast<const T>(*r);
    }

    template <typename T>
    Result<NotMyPtr<const T>> resolve_by_path_typed(const StringSlice path) const {
        auto r = resolve_by_path(path);
        if (!r) {
            return Error<>();
        }
        return assetv2_cast<const T>(*r);
    }

    Result<> register_factory(NotMyPtr<AssetFactory> factory);

    void unregister_factory(Engine& engine, NotMyPtr<AssetFactory> factory);

    size_t pending_asset_count() const { return m_queued_assets[m_active_queue].size(); }

   private:
    Result<NotMyPtr<AssetFactory>, void> find_factory_with_ext(const StringSlice ext);

    Result<NotMyPtr<AssetInfo>, void> find_asset(const modus::engine::assets::String& path);

    void remove_asset_info(AssetInfo& info);

    Result<NotMyPtr<AssetInfo>> create_asset_info(NotMyPtr<AssetFactory> factory,
                                                  String&& path_str);

    void destroy_asset(Engine& engine, AssetInfo& info);

    void trigger_asset_callbacks(Engine& engine, AssetInfo& info);
};

}    // namespace modus::engine::assets
