/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/allocator/pool_allocator.hpp>
#include <core/fixed_function.hpp>
#include <core/guid.hpp>
#include <core/handle_pool_shared.hpp>
#include <core/io/memory_stream.hpp>
#include <core/strong_type.hpp>
#include <os/guid_gen.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::assets {
using AssetsAllocator = DefaultAllocator;
template <typename T>
using Vector = modus::Vector<T, AssetsAllocator>;
using String = modus::StringWithAllocator<AssetsAllocator>;

MODUS_DECLARE_STRONG_TYPE(AssetType, u32, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(AssetHandleV2, std::numeric_limits<u32>::max());
using AssetHandle = AssetHandleV2;

class AssetLoader;
using AssetTypeId = GID;

class MODUS_ENGINE_EXPORT AssetBase {
    friend class AssetLoader;

   private:
    AssetTypeId m_type_id;
    AssetHandleV2 m_asset_handle;

   protected:
    AssetBase(const AssetTypeId& type_id) : m_type_id(type_id) {}

    MODUS_CLASS_DISABLE_COPY_MOVE(AssetBase);

   public:
    virtual ~AssetBase() = default;
    const AssetTypeId& type_id() const { return m_type_id; }
    AssetHandleV2 asset_handle() const { return m_asset_handle; }
};

template <typename T>
struct AssetTraits;

#define MODUS_ENGINE_ASSET_TYPE_DECLARE(C)                                          \
    namespace modus::engine::assets {                                               \
    template <>                                                                     \
    struct MODUS_ENGINE_EXPORT AssetTraits<C> {                                     \
        static_assert(std::is_base_of_v<AssetBase, C>, #C " is not an asset type"); \
        MODUS_ENGINE_EXPORT static const AssetTypeId& type_id();                    \
    };                                                                              \
    }

#define MODUS_ENGINE_ASSET_TYPE_IMPL(C)                                              \
    namespace modus::engine::assets {                                                \
    const AssetTypeId& AssetTraits<C>::type_id() {                                   \
        static const AssetTypeId kId =                                               \
            os::guid::generate().value_or_panic("Failed to generate asset type id"); \
        return kId;                                                                  \
    }                                                                                \
    }

template <typename T>
Result<NotMyPtr<T>, void> assetv2_cast(
    NotMyPtr<std::conditional_t<std::is_const_v<T>, const AssetBase, AssetBase>> asset) {
    static_assert(std::is_base_of_v<AssetBase, T>);
    modus_assert(asset.get() != nullptr);
    if (AssetTraits<std::remove_cv_t<T>>::type_id() != asset->type_id()) {
        return Error<>();
    }
    return Ok<NotMyPtr<T>>(static_cast<T*>(asset.get()));
}

template <typename T>
NotMyPtr<T> unsafe_assetv2_cast(
    NotMyPtr<std::conditional_t<std::is_const_v<T>, const AssetBase, AssetBase>> asset) {
    static_assert(std::is_base_of_v<AssetBase, T>);
    modus_assert(asset.get() != nullptr);
    if (AssetTraits<std::remove_cv_t<T>>::type_id() != asset->type_id()) {
        modus_panic("Attempting to cast between invalid asset types");
    }
    return NotMyPtr<T>(static_cast<T*>(asset.get()));
}

enum class AssetLoadResult {
    Done,
    HasDependencies,
};

/// AssetData that the background loading process should fill out and prepare. This will be passed
/// to the asset creation on the main thread. AssetData should have no ties to factory or any other
/// resource, as it is possible for the AssetFactory to be destroyed during the loading process.
class MODUS_ENGINE_EXPORT AssetData {
   public:
    virtual ~AssetData() = default;

    virtual Result<AssetLoadResult> load(ISeekableReader& reader) = 0;

    /// Return true if all dependencies have been loaded
    virtual Result<bool> eval_dependencies(Engine&, AssetLoader&) { return Ok(true); }
};

class MODUS_ENGINE_EXPORT AssetFactory {
   protected:
    AssetTypeId m_type_id;

   protected:
    AssetFactory(const AssetTypeId& type_id) : m_type_id(type_id) {}

   public:
    virtual ~AssetFactory() = default;

    const AssetTypeId& type_id() const { return m_type_id; }

    virtual std::unique_ptr<AssetData> create_asset_data() = 0;

    virtual StringSlice file_extension() const = 0;

    virtual Result<NotMyPtr<AssetBase>> create(Engine&, AssetData& data) = 0;

    virtual Result<> destroy(Engine&, NotMyPtr<AssetBase> asset) = 0;
};

enum class AssetStateV2 : u8 { Unloaded, Destroy, Loading, Waiting, Loaded, Created, Error };
enum class AssetErrorV2 : u8 { None, IO, Load, Create, Dependency };

template <typename T, typename Allocator>
class MODUS_ENGINE_EXPORT PooledAssetFactory : public AssetFactory {
   protected:
    using factory_type = PooledAssetFactory<T, Allocator>;
    PoolAllocatorTyped<T, Allocator> m_asset_pool;

    PooledAssetFactory(const size_t num_items_per_block)
        : AssetFactory(AssetTraits<T>::type_id()), m_asset_pool(num_items_per_block) {}
};

class RamAssetData : public assets::AssetData {
   private:
    RamStream<assets::AssetsAllocator> m_ram_stream;

   public:
    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        MODUS_PROFILE_ASSETS_BLOCK("RamAssetDataLoad");
        auto r = m_ram_stream.populate_from_stream(reader);
        if (!r) {
            return Error<>();
        }
        m_ram_stream.seek(0).expect("Failed to seek beginning of stream");
        return Ok(assets::AssetLoadResult::Done);
    }

    SliceStream stream() const { return SliceStream(m_ram_stream.as_bytes()); }

    ByteSlice bytes() const { return m_ram_stream.as_bytes(); }
};

}    // namespace modus::engine::assets
