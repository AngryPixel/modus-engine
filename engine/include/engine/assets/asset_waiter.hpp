/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/assets/asset_types.hpp>

namespace modus::engine::assets {

class AssetLoader;

enum class AssetWaiterState { Done, Waiting, Error };

/// Helper to wait on single asset to finish loading
class MODUS_ENGINE_EXPORT AssetWaiter final {
   private:
    String m_asset_path;
    AssetHandleV2 m_handle;

   public:
    void reset(String&& asset_path);

    void reset(const StringSlice asset_path);

    void reset(const AssetHandleV2 handle);

    AssetHandleV2 handle() const { return m_handle; }

    StringSlice path() const { return m_asset_path; }

    Result<AssetWaiterState> update(Engine& engine, AssetLoader& loader);
};

struct MODUS_ENGINE_EXPORT AssetGroupWaiterState {
    u32 finished_count = 0;
    u32 waiting_count = 0;
    u32 error_count = 0;
};

/// Helper to track the progress of an asset group/list
class MODUS_ENGINE_EXPORT AssetGroupWaiter final {
   private:
    Vector<AssetWaiter> m_waiters;

   public:
    size_t count() const { return m_waiters.size(); }

    void reset();

    void reserve(const size_t size);

    void add(String&& asset_path);

    void add(const StringSlice asset_path);

    void add(const AssetHandleV2 handle);

    Slice<AssetWaiter> waiters() const { return make_slice(m_waiters); }

    AssetGroupWaiterState update(Engine& engine, AssetLoader& loader);
};

}    // namespace modus::engine::assets
