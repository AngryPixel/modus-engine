/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/action_queue.hpp>
#include <engine/modules/module.hpp>
#include <jobsys/jobsys.hpp>

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleParallel final : public Module {
   private:
    static constexpr u32 kQueueCount = 2;
    jobsys::JobSystem m_job_system;
    using QueueType = ThreadSafeActionQueue<void(Engine&), DefaultAllocator>;
    QueueType m_queues[kQueueCount];
    std::atomic<u32> m_active_queue;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleParallel();

    ~ModuleParallel();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    void tick(Engine&) override;

    Slice<GID> module_dependencies() const override;

    jobsys::JobSystem& job_system() { return m_job_system; }

    template <typename T>
    Result<> queue_on_main_thread(T&& fn) {
        return get_active_queue().queue(std::forward<T>(fn));
    }

    template <void (*FN)(Engine&)>
    Result<> queue_on_main_thread() {
        return get_active_queue().queue<FN>();
    }

    class ThreadSafeModuleAccessor final {
       private:
        NotMyPtr<ModuleParallel> m_module;

       public:
        ThreadSafeModuleAccessor(NotMyPtr<ModuleParallel> module) : m_module(module) {}
        template <typename T>
        Result<> queue_on_main_thread(T&& fn) {
            return m_module->queue_on_main_thread(std::forward<T>(fn));
        }
        template <void (*FN)(Engine&)>
        Result<> queue_on_main_thread() {
            return m_module->queue_on_main_thread<FN>();
        }
    };

   private:
    inline QueueType& get_active_queue() {
        const u32 queue_index = m_active_queue % kQueueCount;
        return m_queues[queue_index];
    }
};

}    // namespace modus::engine
