/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/engine_includes.hpp>

#include <config/config.hpp>
#include <engine/modules/module.hpp>

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleConfig final : public Module {
   private:
    String m_config_path;
    String m_config_path_tmp;
    config::Config m_cfg;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleConfig();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    Slice<GID> module_dependencies() const override;

    Result<> write_to_disk();

    config::Config& cfg() { return m_cfg; }

    const config::Config& cfg() const { return m_cfg; }
};

}    // namespace modus::engine
