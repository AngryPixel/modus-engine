/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/engine_includes.hpp>

#include <engine/event/event_types.hpp>
#include <engine/input/input_manager.hpp>
#include <engine/modules/module.hpp>

namespace modus::engine {

namespace event {
class Event;
}

class MODUS_ENGINE_EXPORT ModuleInput final : public Module {
   private:
    input::InputManager m_input_manager;
    event::ListenerHandle m_frame_begin_listener_handle;
    event::ListenerHandle m_app_input_listener_handle;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleInput();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    void tick(Engine&) override;

    input::InputManager& manager() { return m_input_manager; }

    inline void update_begin(Engine& engine) { m_input_manager.update_begin(engine); }

    inline void update_end(Engine& engine) { m_input_manager.update_end(engine); }

    Slice<GID> module_dependencies() const override;

   private:
    void on_begin_frame(Engine&, event::Event&);

    void on_app_input(Engine&, event::Event&);
};

}    // namespace modus::engine
