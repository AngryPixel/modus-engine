/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/engine_includes.hpp>
#include <engine/modules/module.hpp>

#include <vfs/vfs.hpp>

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleFileSystem final : public Module {
   private:
    vfs::VirtualFileSystem m_file_system;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleFileSystem();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    vfs::VirtualFileSystem& vfs() { return m_file_system; }

    const vfs::VirtualFileSystem& vfs() const { return m_file_system; }

    Slice<GID> module_dependencies() const override;
};

}    // namespace modus::engine
