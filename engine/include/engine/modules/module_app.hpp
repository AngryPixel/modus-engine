/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <app/event.hpp>
#include <engine/event/event_manager.hpp>
#include <engine/modules/module.hpp>

namespace modus::app {
class App;
struct Event;
}    // namespace modus::app

namespace modus::engine {

class MODUS_ENGINE_EXPORT AppWindowResizeEvent : public event::Event {
   public:
    NotMyPtr<app::Window> m_window;
    i32 m_width;
    i32 m_height;
    AppWindowResizeEvent(NotMyPtr<app::Window> window, const i32 width, const i32 height);
};

class MODUS_ENGINE_EXPORT AppInputEvent final : public event::Event {
   private:
    app::InputEvent m_event;

   public:
    AppInputEvent(const app::InputEvent& event);

    const app::InputEvent& input_event() const { return m_event; }
};

class MODUS_ENGINE_EXPORT ModuleApp final : public Module {
   private:
    std::unique_ptr<app::App> m_app;
    NotMyPtr<Engine> m_engine;
    event::EventHandle m_window_resize_event_handle;
    event::EventHandle m_input_event_handle;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleApp();

    ~ModuleApp();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    void tick(Engine& engine) override;

    Slice<GID> module_dependencies() const override;

    app::App& app() {
        modus_assert(m_app);
        return *m_app;
    }

    event::EventHandle window_resize_event_handle() const { return m_window_resize_event_handle; }

    event::EventHandle input_event_handle() const { return m_input_event_handle; }

   private:
    bool on_app_event(const app::Event& event);
};

}    // namespace modus::engine
