/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/fixed_vector.hpp>
#include <engine/modules/module.hpp>

namespace modus::engine {
class Engine;

class MODUS_ENGINE_EXPORT ModuleList {
   public:
    static constexpr u32 kMaxModuleCount = 16;

   private:
    Array<NotMyPtr<Module>, kMaxModuleCount> m_module_list;
    FixedVector<NotMyPtr<Module>, kMaxModuleCount> m_init_order;
    FixedVector<NotMyPtr<Module>, kMaxModuleCount> m_update_order;
    u32 m_init_count;

   public:
    ModuleList();
    ~ModuleList();
    ModuleList(const ModuleList&) = delete;
    ModuleList(ModuleList&&) = delete;
    ModuleList& operator=(const ModuleList&) = delete;
    ModuleList& operator=(ModuleList&&) = delete;

    Result<> initialize(Engine&);

    Result<> shutdown(Engine&);

    void tick(Engine&);

    Result<NotMyPtr<Module>, void> module_with_guid(const GID& guid);

    Result<NotMyPtr<const Module>, void> module_with_guid(const GID& guid) const;

    template <typename T>
    NotMyPtr<std::remove_cv_t<T>> module() {
        static_assert(std::is_base_of_v<Module, T>, "T is not a module subclass");
        modus_assert(T::module_index() < m_module_list.size());
#if defined(MODUS_DEBUG)
        if (m_module_list[T::module_index()]->guid() != module_guid<T>()) {
            modus_panic("Attempting to cast between incompatible module instances");
        }
#endif
        return m_module_list[T::module_index()];
    }

    template <typename T>
    NotMyPtr<const std::remove_cv_t<T>> module() const {
        static_assert(std::is_base_of_v<Module, T>, "T is not a module subclass");
        modus_assert(T::module_index() < m_module_list.size());
#if defined(MODUS_DEBUG)
        if (m_module_list[T::module_index()]->guid() != module_guid<T>()) {
            modus_panic("Attempting to cast between incompatible module instances");
        }
#endif
        return m_module_list[T::module_list()];
    }

    template <typename T>
    Result<> register_module(NotMyPtr<std::remove_cv_t<T>> module) {
        static_assert(std::is_base_of_v<Module, T>, "T is not a module subclass");
        return register_module(module, T::module_index());
    }

   private:
    Result<> register_module(NotMyPtr<Module> module, const u32 module_index);
};

class MODUS_ENGINE_EXPORT ModuleRegistrator final {
   private:
    ModuleList& m_module_list;

   public:
    ModuleRegistrator(ModuleList& list) : m_module_list(list) {}

    MODUS_CLASS_DISABLE_COPY_MOVE(ModuleRegistrator);

    template <typename T>
    inline Result<> register_module(NotMyPtr<std::remove_cv_t<T>> module) {
        return m_module_list.register_module<T>(module);
    }
};

}    // namespace modus::engine
