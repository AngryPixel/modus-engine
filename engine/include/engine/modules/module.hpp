/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/guid.hpp>

namespace modus::engine {
class Engine;

// TODO: rewrite this so it is dynamic
enum class ModuleUpdateCategory : u32 {
    Event,
    Assets,
    App,
    Input,
    Gameplay,
    Audio,
    Animation,
    UI,
    Graphics,
    Total
};

// An engine module
class MODUS_ENGINE_EXPORT Module {
   private:
    const GID m_guid;
    const StringSlice m_module_name;
    const ModuleUpdateCategory m_update_category;

   protected:
    Module(const GID& guid, const StringSlice name, const ModuleUpdateCategory category)
        : m_guid(guid), m_module_name(name), m_update_category(category) {}

   public:
    ModuleUpdateCategory update_category() const { return m_update_category; }

    const GID& guid() const { return m_guid; }

    StringSlice name() const { return m_module_name; }

    virtual Result<> initialize(Engine&) { return Ok<>(); };

    virtual Result<> shutdown(Engine&) { return Ok<>(); };

    virtual void tick(Engine&){};

    virtual Slice<GID> module_dependencies() const { return Slice<GID>(); }
};

template <typename T>
class MODUS_ENGINE_EXPORT NoOpThreadSafeModuleAccessor {
   public:
    NoOpThreadSafeModuleAccessor(NotMyPtr<T>) {}
};

namespace module_detail {
MODUS_ENGINE_EXPORT u32 generate_module_index();
}

}    // namespace modus::engine

#define MODUS_ENGINE_DECLARE_MODULE() \
   public:                            \
    static const GID kModuleGUID;     \
    static u32 module_index()

#define MODUS_ENGINE_IMPL_MODULE(Module, Guid)                                                 \
    u32 Module::module_index() {                                                               \
        static const u32 kModuleIndex = modus::engine::module_detail::generate_module_index(); \
        return kModuleIndex;                                                                   \
    }                                                                                          \
    const GID Module::kModuleGUID =                                                            \
        modus::GID::from_string(Guid).value_or_panic("Invalid module guid")

namespace modus::engine {
template <typename T>
inline const GID& module_guid() {
    static_assert(std::is_base_of_v<Module, T>);
    return T::kModuleGUID;
}
}    // namespace modus::engine
