/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <engine/assets/asset_loader.hpp>
#include <engine/modules/module.hpp>

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleAssets final : public Module {
   private:
    assets::AssetLoader m_loader;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleAssets();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    void tick(Engine&) override;

    assets::AssetLoader& loader() { return m_loader; }

    const assets::AssetLoader& loader() const { return m_loader; }

    Slice<GID> module_dependencies() const override;
};

}    // namespace modus::engine
