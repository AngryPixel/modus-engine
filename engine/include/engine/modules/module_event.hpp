/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/engine_includes.hpp>

#include <engine/event/event_manager.hpp>
#include <engine/modules/module.hpp>
namespace modus::engine {

struct EngineEvents {
    event::EventHandle begin_frame;
    event::EventHandle end_frame;
};

class MODUS_ENGINE_EXPORT ModuleEvent final : public Module {
   private:
    event::EventManager m_manager;
    EngineEvents m_engine_events;

    MODUS_ENGINE_DECLARE_MODULE();

   public:
    ModuleEvent();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    void tick(Engine& engine) override;

    event::EventManager& manager() { return m_manager; }

    const EngineEvents& engine_events() const { return m_engine_events; }
};

}    // namespace modus::engine
