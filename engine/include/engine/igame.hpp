/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/string_slice.hpp>

namespace modus {
class CmdOptionParser;
}

namespace modus::engine {

class Engine;
class View;
class ModuleRegistrator;

/// Interface for a game implementation
class MODUS_ENGINE_EXPORT IGame {
   public:
    virtual ~IGame() {}

    /// Add any command line options that can be parsed by this game
    virtual Result<> add_cmd_options(CmdOptionParser& parser) = 0;

    /// List of Modules the game should load
    virtual Result<> register_modules(ModuleRegistrator&) { return Ok<>(); }

    /// Name of the game
    virtual StringSlice name() const = 0;

    /// Name of the preferences folder for this game
    virtual StringSlice name_preferences() const = 0;

    /// On success, return the view that should be used to transition
    /// into the game
    virtual Result<> initialize(Engine& engine) = 0;

    virtual Result<> shutdown(Engine& engine) = 0;

    virtual void tick(Engine& engine) = 0;
};
}    // namespace modus::engine
