/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <app/event.hpp>
#include <engine/assets/asset_types.hpp>
#include <engine/cmd_args.hpp>
#include <engine/modules/module_list.hpp>
#include <os/time.hpp>

namespace modus::app {
class App;
}
namespace modus::engine {

namespace assets {
struct AsyncLoadParams;
}

class IGame;

enum class EngineInitError { App, Module, Os, Game, Uknown };

using EngineInitResult = Result<void, EngineInitError>;

struct EngineStartupAssetState {
    u32 load_count;
    u32 total_count;
};

class MODUS_ENGINE_EXPORT Engine {
   private:
    struct EnginePrivate;
    std::unique_ptr<EnginePrivate> m_private;
    CmdOptions m_options;
    IGame* m_game;
    // Fps Counter
    os::Timer m_timer;
    IMilisec m_previous_tick_ms;
    IMilisec m_tick_ms;
    IMilisec m_fps_tick_ms;
    f64 m_fps;
    u32 m_frame_count;
    FSeconds m_tick_sec;
    // quit request
    bool m_quit_requested;
    ModuleList m_modules;

   public:
    Engine();

    ~Engine();

    /// Return success and whether the help option was request, error otherwise.
    Result<bool, void> parse_command_options(const int argc, const char** argv, IGame& game);

    EngineInitResult initialize(IGame& game);

    EngineInitResult shutdown();

    void run();

    void quit();

    /// Load an Asset that is required for the engine to work properly.
    Result<assets::AssetHandleV2> load_startup_asset(assets::AsyncLoadParams& params);

    /// Check the current state of all the asset required by the startup phase.
    /// If there is no error during asset loading, an EngineStartupAssetState will be
    /// returned with the number of loaded assets and total assets.
    /// If there was an error, Error<> will be returned.
    Result<EngineStartupAssetState> startup_asset_state();

    inline const CmdOptions& cmd_options() const { return m_options; }

    inline IGame& game() { return *m_game; }

    inline const ModuleList& modules() const { return m_modules; }

    template <typename T>
    NotMyPtr<std::remove_cv_t<T>> module() {
        return m_modules.module<T>();
    }

    template <typename T>
    NotMyPtr<const std::remove_cv_t<T>> module() const {
        return m_modules.module<T>();
    }

    IMilisec elapsed_time_ms() const;

    DSeconds elapsed_time_sec() const;

    inline IMilisec tick_ms() const { return IMilisec(m_tick_ms.count()); }

    inline FSeconds tick_sec() const { return m_tick_sec; }

    inline f64 fps() const { return m_fps; }
};

class MODUS_ENGINE_EXPORT ThreadSafeEngineAccessor final {
   private:
    NotMyPtr<Engine> m_engine;

   public:
    ThreadSafeEngineAccessor(Engine& engine) : m_engine(&engine) {}

    inline IMilisec tick_ms() const { return m_engine->tick_ms(); }

    inline FSeconds tick_sec() const { return m_engine->tick_sec(); }

    template <typename T>
    typename std::remove_cv_t<T>::ThreadSafeModuleAccessor module() {
        return typename std::remove_cv_t<T>::ThreadSafeModuleAccessor(m_engine->module<T>());
    }
};

}    // namespace modus::engine
