/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_includes.hpp>

#include <core/getopt.hpp>

namespace modus::engine {

struct MODUS_ENGINE_EXPORT CmdOptsProfiling final {
    CmdOptsProfiling();
    CmdOptionEmpty enabled_local;
    CmdOptionString output_file_path;
    CmdOptionEmpty enabled_remote;
    CmdOptionU32 server_port;
};

struct MODUS_ENGINE_EXPORT CmdOptsLogging final {
    CmdOptsLogging();
    CmdOptionString level;
};

struct MODUS_ENGINE_EXPORT CmdOptsRendering final {
    CmdOptsRendering();
    CmdOptionString threed_api;
};

struct MODUS_ENGINE_EXPORT CmdOptions final {
    CmdOptions();

    MODUS_CLASS_DISABLE_COPY_MOVE(CmdOptions);

    CmdOptsProfiling profiling;
    CmdOptsLogging logging;
    CmdOptsRendering rendering;
};

MODUS_ENGINE_EXPORT Result<> add_to_option_parser(CmdOptionParser& parser, CmdOptions& options);
}    // namespace modus::engine
