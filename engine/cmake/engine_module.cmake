#
# Configuration for Engine Modules
#

set(MODUS_ENGINE_CMAKE_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR} CACHE STRING "")

function(modus_generate_engine_module_config_header target
        version_major version_minor version_patch)
    string(TOUPPER "ENGINE_MODULE_${target}" MODULE_NAME)
    set(MODULE_VERSION_MAJOR ${version_major})
    set(MODULE_VERSION_MINOR ${version_minor})
    set(MODULE_VERSION_PATCH ${version_patch})
    set(MODULE_VERSION_STRING "${version_major}.${version_minor}.${version_patch}")
    get_target_property(binary_dir ${target} BINARY_DIR)
    set(generated_dir ${binary_dir}/gen_config/)
    set(generated_file ${generated_dir}/${target}/engine_module_config.hpp)
    configure_file(${MODUS_ENGINE_CMAKE_CMAKE_DIR}/engine_module_config.hpp.in
        ${generated_file}
    )
    target_include_directories(${target} PUBLIC ${generated_dir})
    target_sources(${target} PRIVATE ${generated_file})
    set_target_properties(${target} PROPERTIES VERSION ${MODULE_VERSION_STRING})
    if (MODUS_BUILD_SHARED)
        target_compile_definitions(${target}
            PUBLIC MODUS_SHARED_LIBRARY_BUILD=1
        )
    endif()
    target_compile_definitions(${target}
        PRIVATE MODUS_${MODULE_NAME}_BUILDING=1
    )
endfunction()

function(modus_add_engine_module target)

    cmake_parse_arguments(arg
        "NO_ERROR_ON_WARNING;STATIC"
        "VERSION_MAJOR;VERSION_MINOR;VERSION_PATCH"
        ""
        ${ARGN}
    )

    if (arg_STATIC OR NOT MODUS_BUILD_SHARED)
        add_library(${target} STATIC)
    else()
        add_library(${target} SHARED)
    endif()

    modus_apply_output_dir(${target})

    modus_generate_engine_module_config_header(${target}
        ${arg_VERSION_MAJOR}
        ${arg_VERSION_MINOR}
        ${arg_VERSION_PATCH}
    )

    modus_apply_common_target_flags(${target})

    modus_apply_pch(${target})

    target_link_libraries(${target} PUBLIC engine)

    # Enable tests
    if (MODUS_BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests)
        add_subdirectory(tests)
    endif()

    # Enable tools
    if (MODUS_BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools)
        add_subdirectory(tools)
    endif()
endfunction()
