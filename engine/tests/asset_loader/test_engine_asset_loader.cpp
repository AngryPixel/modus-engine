/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>

#include <core/io/memory_stream.hpp>
#include <engine/assets/asset_loader.hpp>
#include <engine/assets/asset_waiter.hpp>
#include <engine/engine.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <os/guid_gen.hpp>
#include <os/path.hpp>

using namespace modus;

class TestAsset final : public engine::assets::AssetBase {
   public:
    TestAsset(const engine::assets::AssetTypeId& type_id) : engine::assets::AssetBase(type_id) {}
};

MODUS_ENGINE_ASSET_TYPE_DECLARE(TestAsset)
MODUS_ENGINE_ASSET_TYPE_IMPL(TestAsset)

class TestAssetData final : public engine::assets::AssetData {
   public:
    engine::assets::AssetWaiter m_waiter;

    Result<engine::assets::AssetLoadResult> load(ISeekableReader& stream) override {
        RamStream<> ram_stream;
        auto r_load = ram_stream.populate_from_stream(stream);
        if (!r_load) {
            return Error<>();
        }
        if (*r_load != 0) {
            const StringSlice dependency = ram_stream.as_string_slice();
            if (dependency.size() != 0) {
                String dependency_str = dependency.to_str();
                auto pos = dependency_str.find('\n');
                if (pos != String::npos) {
                    dependency_str.erase(dependency_str.begin() + pos);
                }
                pos = dependency_str.find('\r');
                if (pos != String::npos) {
                    dependency_str.erase(dependency_str.begin() + pos);
                }
                m_waiter.reset(std::move(dependency_str));
                return Ok(engine::assets::AssetLoadResult::HasDependencies);
            }
        }
        return Ok(engine::assets::AssetLoadResult::Done);
    }

    /// Return true if all dependencies have been loaded
    Result<bool> eval_dependencies(engine::Engine& engine,
                                   engine::assets::AssetLoader& loader) override {
        auto r = m_waiter.update(engine, loader);
        if (!r || *r == engine::assets::AssetWaiterState::Error) {
            return Error<>();
        }
        if (*r == engine::assets::AssetWaiterState::Done) {
            return Ok(true);
        }
        return Ok(false);
    }
};

class TestAssetFactory final : public engine::assets::AssetFactory {
   public:
    TestAssetFactory()
        : engine::assets::AssetFactory(engine::assets::AssetTraits<TestAsset>::type_id()) {}

    std::unique_ptr<engine::assets::AssetData> create_asset_data() override {
        return modus::make_unique<TestAssetData>();
    }

    StringSlice file_extension() const override { return "test"; }

    Result<NotMyPtr<engine::assets::AssetBase>> create(engine::Engine&,
                                                       engine::assets::AssetData& data) override {
        MODUS_UNUSED(data);
        return Ok<NotMyPtr<engine::assets::AssetBase>>(new TestAsset(type_id()));
    }

    Result<> destroy(engine::Engine&, NotMyPtr<engine::assets::AssetBase> asset) override {
        delete asset.get();
        return Ok<void>();
    }
};

class TestAssetLoader final : public engine::IGame {
   private:
    TestAssetFactory m_factory;
    engine::assets::AssetHandleV2 m_asset;
    bool m_quit = false;

   public:
    StringSlice name() const override { return "AssetLoaderTest"; }
    StringSlice name_preferences() const override { return "modus.test.asset_loader"; }

    Result<> add_cmd_options(modus::CmdOptionParser&) override { return Ok<>(); }

    Result<> initialize(engine::Engine& engine) override {
        auto fs_module = engine.module<engine::ModuleFileSystem>();
        auto asset_module = engine.module<engine::ModuleAssets>();

        {
            String path = os::Path::join(MODUS_TEST_SOURCE_DIR, "data");
            vfs::VFSMountParams mount_params;
            mount_params.tag = "data";
            mount_params.path = path;
            mount_params.read_only = true;
            if (!fs_module->vfs().mount_path(mount_params)) {
                MODUS_LOGE("Failed to mount data path");
                return Error<>();
            }
        }

        {
            if (!asset_module->loader().register_factory(&m_factory)) {
                MODUS_LOGE("Failed to register asset factory");
                return Error<>();
            }
        }

        engine::assets::AsyncLoadParams params;
        params.path = "data:foo.test";
        params.callback = [this](engine::Engine&,
                                 const engine::assets::AsyncCallbackResult& result) {
            if (result.error == engine::assets::AssetErrorV2::None) {
                MODUS_LOGI("Asset Loaded");
            } else {
                MODUS_LOGE("Asset '{}' failed to load: {}", result.path,
                           engine::assets::asset_error_to_string(result.error));
            }
            m_quit = true;
        };

        auto r_asset = asset_module->loader().load_async(engine, params);
        if (!r_asset) {
            MODUS_LOGE("Failed to queue asset load");
            return Error<void>();
        }
        m_asset = r_asset->handle;
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto asset_module = engine.module<engine::ModuleAssets>();
        asset_module->loader().unregister_factory(engine, &m_factory);
        return Ok<>();
    }

    void tick(engine::Engine& engine) override {
        if (m_quit) {
            auto asset_module = engine.module<engine::ModuleAssets>();
            auto r = asset_module->loader().resolve_typed<TestAsset>(m_asset);
            if (!r) {
                MODUS_LOGE("Failed to resolve asset");
            } else {
                MODUS_LOGI("Resolved asset");
            }
            engine.quit();
        }
        /*
        auto asset_module =
            engine.module<engine::ModuleAssets>(engine.modules().assets());
        if (auto r = asset_module->loader().asset_state(m_asset); r) {
            //MODUS_LOGI("Asset State: {}\n", engine::assets::asset_state_to_string(*r));
        }*/
    }
};

int main(const int argc, const char** argv) {
    TestAssetLoader game;
    engine::Engine engine;

    auto r_parse = engine.parse_command_options(argc, argv, game);
    if (!r_parse) {
        return EXIT_FAILURE;
    } else if (r_parse.value()) {
        return EXIT_SUCCESS;
    }
    if (!engine.initialize(game)) {
        fprintf(stderr, "Failed to initialize engine");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr, "Failed to shutdown engine");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
