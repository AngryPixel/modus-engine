#
# Run some tests on the asset loader system
#

modus_add_test(test_engine_asset_loader NAME EngineAssetLoader SKIP_CATCH)

target_sources(test_engine_asset_loader PRIVATE test_engine_asset_loader.cpp)

target_link_libraries(test_engine_asset_loader PRIVATE engine)
