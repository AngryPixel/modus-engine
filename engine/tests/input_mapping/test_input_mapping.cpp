/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>

#include <engine/engine.hpp>
#include <engine/igame.hpp>
#include <engine/input/input_mapper.hpp>
#include <engine/modules/module_input.hpp>

using namespace modus;

enum class Action : u32 { Quit, Zoom, MouseLookY, AxisX, AxisY, Fire };

void on_button_down(engine::Engine&, const engine::input::v2::AxisMap& map) {
    MODUS_LOGI("on_btn_down   : [{:16}]", map.name());
}

void on_button_up(engine::Engine&, const engine::input::v2::AxisMap& map) {
    MODUS_LOGI("on_btn_up     : [{:16}]", map.name());
}

void on_update(engine::Engine&, const engine::input::v2::AxisMap& map) {
    MODUS_LOGI("on_btn_update : [{:16}] v:{} vn:{}", map.name(), map.value(),
               map.value_normalized());
}

class TestInputContext final : public engine::input::v2::MappedInputContext {
   public:
    TestInputContext() : engine::input::v2::MappedInputContext(50000) {}

    StringSlice name() const override { return "TestInputContext"; }

    void setup() {
        {
            auto r_action = m_input_mapper.action(u32(Action::Quit));
            if (!r_action) {
                modus_panic("Failed to retrieve action");
            }
            auto& action = *r_action;
            action->set_name("Quit");
            action->bind_positive_button(app::InputKey::KeyEscape);
            action->set_enabled(true);
        }
        {
            auto r_action = m_input_mapper.action(u32(Action::Zoom));
            if (!r_action) {
                modus_panic("Failed to retrieve action");
            }
            auto& action = *r_action;
            action->set_name("Zoom");
            action->bind_positive_button(app::InputKey::KeyI);
            action->bind_negative_button(app::InputKey::KeyO);
            action->bind_alt_positive_button(app::GameControllerButton::A);
            action->bind_alt_negative_button(app::GameControllerButton::B);
            action->bind_axis(engine::input::v2::AxisBindingType::MouseScrollY);
            action->set_enabled(true);
            action->bind_on_button_down(
                engine::input::v2::AxisMap::Callback::build<on_button_down>());
            action->bind_on_button_up(engine::input::v2::AxisMap::Callback::build<on_button_up>());
            action->bind_on_update(engine::input::v2::AxisMap::Callback::build<on_update>());
        }
        {
            auto r_action = m_input_mapper.action(u32(Action::MouseLookY));
            if (!r_action) {
                modus_panic("Failed to retrieve action");
            }
            auto& action = *r_action;
            action->set_name("MouseLookY");
            action->bind_axis(engine::input::v2::AxisBindingType::MouseY);
            action->set_enabled(true);
        }
        {
            auto r_action = m_input_mapper.action(u32(Action::AxisX));
            if (!r_action) {
                modus_panic("Failed to retrieve action");
            }
            auto& action = *r_action;
            action->set_name("AxisX");
            action->bind_positive_button(app::InputKey::KeyD);
            action->bind_negative_button(app::InputKey::KeyA);
            action->bind_axis(engine::input::v2::AxisBindingType::GameControllerLeftX);
            action->set_enabled(true);
        }
        {
            auto r_action = m_input_mapper.action(u32(Action::AxisY));
            if (!r_action) {
                modus_panic("Failed to retrieve action");
            }
            auto& action = *r_action;
            action->set_name("AxisY");
            action->bind_positive_button(app::InputKey::KeyW);
            action->bind_negative_button(app::InputKey::KeyS);
            action->bind_axis(engine::input::v2::AxisBindingType::GameControllerLeftY);
            action->set_enabled(true);
        }
        {
            auto r_action = m_input_mapper.action(u32(Action::Fire));
            if (!r_action) {
                modus_panic("Failed to retrieve action");
            }
            auto& action = *r_action;
            action->set_name("Fire");
            action->bind_positive_button(app::InputKey::KeySpace);
            action->bind_negative_button(app::MouseButton::Left);
            action->bind_axis(engine::input::v2::AxisBindingType::GameControllerRightTrigger);
            action->set_enabled(true);
        }
    }

    void print_status(engine::Engine& engine) {
        const auto quit_action = m_input_mapper.action(u32(Action::Quit));
        if (quit_action && (*quit_action)->is_active()) {
            engine.quit();
        }

        m_input_mapper.update(engine);

        /* for (u32 i = 0; i < m_input_mapper.action_count(); ++i) {
             auto& action = m_input_mapper.action(i).value_or_panic();
             if (action->enabled() && action->is_active()) {
                 MODUS_LOGI("[{:16}]: v:{} vn{} inverted:{}", action->name(),
                            action->value(), action->value_normalized(),
                            action->inverted());
             }
         }*/
    }
};

class TestAssetLoader final : public engine::IGame {
   private:
    TestInputContext m_input_context;

   public:
    StringSlice name() const override { return "Engine Input Mapping Test"; }
    StringSlice name_preferences() const override { return "modus.test.engine_input_mapping"; }

    Result<> add_cmd_options(modus::CmdOptionParser&) override { return Ok<>(); }

    Result<> initialize(engine::Engine& engine) override {
        m_input_context.setup();
        auto input_module = engine.module<engine::ModuleInput>();
        input_module->manager().add_context(&m_input_context);
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto input_module = engine.module<engine::ModuleInput>();
        input_module->manager().remove_context(&m_input_context);
        return Ok<>();
    }

    void tick(engine::Engine& engine) override {
        MODUS_UNUSED(engine);
        m_input_context.print_status(engine);
    }
};

int main() {
    TestAssetLoader game;
    engine::Engine engine;
    if (!engine.initialize(game)) {
        fprintf(stderr, "Failed to initialize engine\n");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr, "Failed to shutdown engine\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
