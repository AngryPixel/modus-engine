/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>

#include <engine/engine.hpp>
#include <engine/igame.hpp>
#include <engine/view.hpp>

using namespace modus;

class SubView final : public engine::View {
    void preload(engine::Engine&) override { MODUS_LOGD("On SubView Preload\n"); }

    void on_enter(engine::Engine&) override {
        MODUS_LOGD("On SubView Enter\n");
        auto controller = view_controller();
        if (controller) {
            (void)(*controller)->pop_view();
        }
    }

    void on_exit(engine::Engine&) override { MODUS_LOGD("On SubView Exit\n"); }

    void unload(engine::Engine&) override { MODUS_LOGD("On SubView Unload\n"); }
};

class StartupView final : public engine::View {
   public:
    SubView m_sub_view;

    bool m_pushed = false;
    void preload(engine::Engine&) override { MODUS_LOGD("On View Preload\n"); }

    void on_enter(engine::Engine&) override {
        MODUS_LOGD("On View Enter\n");
        if (!m_pushed) {
            auto controller = view_controller();
            if (controller) {
                m_pushed = true;
                if (!(*controller)->push_view(&m_sub_view)) {
                    MODUS_LOGE("Failed to push subview");
                }
            }
        }
    }

    void on_exit(engine::Engine&) override { MODUS_LOGD("On View Exit\n"); }

    void unload(engine::Engine&) override { MODUS_LOGD("On View Unload\n"); }
};

class ModelViewer final : public engine::IGame {
   private:
    f64 m_elapsed_ms = 0.0;
    StartupView m_view;

   public:
    StringSlice name() const override { return "Engine Startup Test"; }
    StringSlice name_preferences() const override { return "modus.test.engine_startup"; }

    Result<void, void> add_cmd_options(modus::CmdOptionParser&) override { return Ok<void>(); }

    Result<engine::View*, void> initialize(engine::Engine&) override {
        return Ok<engine::View*>(&m_view);
    }

    Result<void, void> shutdown(engine::Engine&) override { return Ok<void>(); }

    void tick(engine::Engine& engine) override {
        m_elapsed_ms += engine.tick_ms();
        if (m_elapsed_ms > 1000.0) {
            engine.quit();
        }
    }
};

int main() {
    ModelViewer game;
    engine::Engine engine;
    if (!engine.initialize(game)) {
        fprintf(stderr, "Failed to initialize engine\n");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr, "Failed to shutdown engine\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
