/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>

#include <engine/engine.hpp>
#include <engine/igame.hpp>

using namespace modus;

class TestAssetLoader final : public engine::IGame {
   private:
    DMilisec m_elapsed_ms = DMilisec(0.0);

   public:
    StringSlice name() const override { return "Engine Startup Test"; }
    StringSlice name_preferences() const override { return "modus.test.engine_startup"; }

    Result<> add_cmd_options(modus::CmdOptionParser&) override { return Ok<>(); }

    Result<> initialize(engine::Engine&) override { return Ok<>(); }

    Result<> shutdown(engine::Engine&) override { return Ok<>(); }

    void tick(engine::Engine& engine) override {
        m_elapsed_ms += engine.tick_ms();
        if (m_elapsed_ms > DMilisec(1000.0)) {
            engine.quit();
        }
    }
};

int main() {
    TestAssetLoader game;
    engine::Engine engine;
    if (!engine.initialize(game)) {
        fprintf(stderr, "Failed to initialize engine\n");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr, "Failed to shutdown engine\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
