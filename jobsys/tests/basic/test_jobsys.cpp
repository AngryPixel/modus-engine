/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <jobsys/jobsys_pch.h>
#include <jobsys/jobsys.hpp>

#include <catch2/catch.hpp>
#include <iostream>

#include <os/thread.hpp>
#include <os/time.hpp>

using namespace modus;

TEST_CASE("Basic Startup and Shutdown", "[JobSys]") {
    jobsys::JobSystem js;
    auto r_init = js.initialize(4);
    CHECK(r_init);
    std::cout << fmt::format("Main Thread: t={}\n", os::Thread::current_thread_id());
    if (r_init) {
        auto job = js.create([]() {
            os::sleep_ms(IMilisec(500));
            std::cout << fmt::format("Hello world: t={}\n", os::Thread::current_thread_id());
        });
        js.run(job);
        js.wait(job);
    }
    js.shutdown();
}

TEST_CASE("Child Jobs", "[JobSys]") {
    constexpr u32 kChildCount = 200;
    u32 array[kChildCount];
    memset(array, 0, sizeof(array));
    jobsys::JobSystem js;
    auto r_init = js.initialize(4);
    CHECK(r_init);
    std::cout << fmt::format("Main Thread: t={}\n", os::Thread::current_thread_id());
    if (r_init) {
        auto job = js.create([]() {});
        for (u32 i = 0; i < kChildCount; ++i) {
            auto child_job = js.create_as_child(job, [child = i, ptr = &array[0]]() {
                ptr[child] = child;
                std::cout << fmt::format("Child={:02} t={}\n", child,
                                         os::Thread::current_thread_id());
            });
            js.run(child_job);
        }
        js.run(job);
        js.wait(job);
        for (u32 i = 0; i < kChildCount; ++i) {
            CHECK(array[i] == i);
        }
    }
    js.shutdown();
}

TEST_CASE("Chained Jobs", "[JobSys]") {
    constexpr u32 kChildCount = 200;
    u32 array[kChildCount];
    memset(array, 0, sizeof(array));
    jobsys::JobSystem js;
    auto r_init = js.initialize(4);
    CHECK(r_init);
    std::cout << fmt::format("Main Thread: t={}\n", os::Thread::current_thread_id());
    std::atomic<int> counter(0);
    if (r_init) {
        auto job = js.create([]() {});
        auto last_job_handle = job;
        for (u32 i = 0; i < kChildCount; ++i) {
            last_job_handle =
                js.create_chained(last_job_handle, [child = i, ptr = &array[0], &counter]() {
                    ptr[child] = counter.fetch_add(1);
                    std::cout << fmt::format("Chained Child={:02} t={}\n", child,
                                             os::Thread::current_thread_id());
                });
        }
        js.run(job);
        js.wait(last_job_handle);
        for (u32 i = 0; i < kChildCount; ++i) {
            CHECK(array[i] == i);
        }
    }
    js.shutdown();
}

TEST_CASE("ParallelFor (range < thread count)", "[JobSys]") {
    jobsys::JobSystem js;
    auto r_init = js.initialize(4);
    CHECK(r_init);
    if (r_init) {
        int data[2];
        auto job =
            js.parallel_for(array_size(data), [&data](const usize offset, const usize range) {
                std::cout << fmt::format("offset={:03} range={:03} t={}\n", offset, range,
                                         os::Thread::current_thread_id());
                for (auto& v : SliceMut(data).sub_slice(offset, range)) {
                    v++;
                }
            });
        js.run(job);
        js.wait(job);
    }
    js.shutdown();
}
TEST_CASE("ParallelFor (range == thread count)", "[JobSys]") {
    jobsys::JobSystem js;
    auto r_init = js.initialize(4);
    CHECK(r_init);
    if (r_init) {
        int data[4];
        auto job = js.parallel_for(
            SliceMut(data), [job_data = SliceMut(data)](const usize offset, const usize range) {
                std::cout << fmt::format("offset={:03} range={:03} t={}\n", offset, range,
                                         os::Thread::current_thread_id());
                for (auto& v : job_data.sub_slice(offset, range)) {
                    v++;
                }
            });
        js.run(job);
        js.wait(job);
    }
    js.shutdown();
}

TEST_CASE("ParallelFor (range >= thread count)", "[JobSys]") {
    jobsys::JobSystem js;
    auto r_init = js.initialize(4);
    CHECK(r_init);
    if (r_init) {
        int data[535];
        auto job =
            js.parallel_for(array_size(data), [&data](const usize offset, const usize range) {
                std::cout << fmt::format("offset={:03} range={:03} t={}\n", offset, range,
                                         os::Thread::current_thread_id());
                for (auto& v : SliceMut(data).sub_slice(offset, range)) {
                    v++;
                }
            });
        js.run(job);
        js.wait(job);
    }
    js.shutdown();
}
