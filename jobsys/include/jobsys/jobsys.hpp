/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <jobsys/module_config.hpp>

#include <core/containers.hpp>
#include <core/slice.hpp>
#include <jobsys/jobsys_types.hpp>

namespace modus::jobsys {

class MODUS_JOBSYS_EXPORT JobSystem final {
   private:
    struct Private;
    std::unique_ptr<Private> m_impl;

   public:
    JobSystem();
    ~JobSystem();
    MODUS_CLASS_DISABLE_COPY_MOVE(JobSystem);

    Result<> initialize(const Optional<u32> thread_count);
    void shutdown();
    u32 thread_count() const;

    template <typename T>
    JobHandle create(T&& fn) {
        return create(JobFunctionType(std::forward<T>(fn)));
    }

    /// This creates a job that will be queued for execution once the parent's work finishes.
    /// NOTE: The parent job will not wait for this job's completion before reporting itself as
    /// finished.
    template <typename T>
    JobHandle create_chained(const JobHandle parent, T&& fn) {
        return create_chained(parent, JobFunctionType(std::forward<T>(fn)));
    }

    /// This creates a job that will cause the parent job to wait for its completion before
    /// reporting itself as finished.
    /// NOTE: The job will not be queued for execution as soon as the parent's work finishes. See
    /// create_as_chained_child() for that purpose.
    template <typename T>
    JobHandle create_as_child(const JobHandle parent, T&& fn) {
        return create_as_child(parent, JobFunctionType(std::forward<T>(fn)));
    }

    /// This creates a job that will be queued for execution once the parent's work finishes and
    /// will make the parent wait for it's completion before reporting itself as finished.
    template <typename T>
    JobHandle create_as_chained_child(const JobHandle parent, T&& fn) {
        return create_as_chained_child(parent, JobFunctionType(std::forward<T>(fn)));
    }

    /// Queue a job for execution
    void run(const JobHandle handle);

    /// Wait for the job to finish. While waiting, the calling thread may execute work.
    void wait(const JobHandle handle);

    static constexpr usize kMinimumParallelForEachGroupSize = 128;
    template <typename T>
    JobHandle parallel_for(const usize size,
                           T&& fn,
                           const usize group_size = kMinimumParallelForEachGroupSize) {
        static_assert(std::is_invocable_v<T, const usize, const usize>,
                      "Invalid function/callable signature");
        if (size < kMinimumParallelForEachGroupSize) {
            auto job = create([fn = std::move(fn), size]() { fn(0, size); });
            return job;
        } else {
            auto parent_job = create([] {});
            const usize divisor = thread_count();
            const usize actual_group_size =
                group_size != 0 ? group_size : std::max(divisor, size / divisor);
            usize remaining = size;
            usize offset = 0;
            while (remaining != 0) {
                const usize range = std::min(actual_group_size, remaining);
                auto child_job = create_as_child(
                    parent_job, [fn = std::move(fn), offset, range]() { fn(offset, range); });
                run(child_job);
                remaining -= range;
                offset += range;
            }
            return parent_job;
        }
    }

    template <typename D, typename T>
    JobHandle parallel_for(SliceMut<D> slice,
                           T&& fn,
                           const usize group_size = kMinimumParallelForEachGroupSize) {
        return parallel_for(slice.size(), std::forward<T>(fn), group_size);
    }

    template <typename D, typename T>
    JobHandle parallel_for(Slice<D> slice,
                           T&& fn,
                           const usize group_size = kMinimumParallelForEachGroupSize) {
        return parallel_for(slice.size(), std::forward<T>(fn), group_size);
    }

   private:
    JobHandle create(JobFunctionType&& fn);

    JobHandle create_chained(const JobHandle parent, JobFunctionType&& fn);

    JobHandle create_as_child(const JobHandle parent, JobFunctionType&& fn);

    JobHandle create_as_chained_child(const JobHandle parent, JobFunctionType&& fn);
};

}    // namespace modus::jobsys