/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <jobsys/jobsys.hpp>
// clang-format on

#include <job_thread.hpp>

namespace modus::jobsys {

struct JobSystem::Private {
    std::unique_ptr<ThreadData[]> m_thread_data;
    std::unique_ptr<JobThread[]> m_job_threads;
    u32 m_thread_count;
};

JobSystem::JobSystem() {}

JobSystem::~JobSystem() {
    modus_assert(!m_impl);
}

Result<> JobSystem::initialize(const Optional<u32> thread_count) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    m_impl = modus::make_unique<Private>();
    if (!m_impl) {
        return Error<>();
    }

    m_impl->m_thread_count = std::max(2u, thread_count.value_or(os::os_cpu_count()));
    {
        // setup thread data
        m_impl->m_thread_data.reset(new ThreadData[m_impl->m_thread_count]);
        if (!m_impl->m_thread_data) {
            return Error<>();
        }
        for (u32 i = 0; i < m_impl->m_thread_count; ++i) {
            m_impl->m_thread_data[i].random_generator.init(i);
        }
    }

    {
        // setup threads
        m_impl->m_job_threads.reset(new JobThread[m_impl->m_thread_count - 1]);
        if (!m_impl->m_job_threads) {
            return Error<>();
        }
        for (u32 i = 0; i < m_impl->m_thread_count - 1; ++i) {
            m_impl->m_job_threads[i].set_data(
                SliceMut(m_impl->m_thread_data.get(), m_impl->m_thread_count), i + 1);
            if (!m_impl->m_job_threads[i].spawn()) {
                return Error<>();
            }
        }
    }

    tls_init_pointers(m_impl->m_thread_data[kMainThreadIndex]);
    return Ok<>();
}

void JobSystem::shutdown() {
    MODUS_PROFILE_JOBSYS_SCOPE();
    if (m_impl) {
        if (m_impl->m_job_threads) {
            for (u32 i = 0; i < m_impl->m_thread_count - 1; ++i) {
                m_impl->m_thread_data[i + 1].exit = true;
                m_impl->m_job_threads[i].join();
            }
        }
        m_impl.reset();
        tls_reset_pointers();
    }
}

u32 JobSystem::thread_count() const {
    modus_assert(m_impl);
    return m_impl->m_thread_count;
}

void JobSystem::run(const JobHandle handle) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    modus_assert(m_impl);
    NotMyPtr<JobPool> job_pool = get_tls_job_pool();
    NotMyPtr<QueueType> job_queue = get_tls_job_queue();
    modus_assert(job_pool && job_queue);
    NotMyPtr<Job> job = job_pool->job_from_handle(handle);
    modus_assert(job);
    job_queue->push(job);
}

void JobSystem::wait(const JobHandle handle) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    modus_assert(m_impl);
    NotMyPtr<JobPool> job_pool = get_tls_job_pool();
    modus_assert(job_pool);
    NotMyPtr<Job> job = job_pool->job_from_handle(handle);
    modus_assert(job);
    while (!job->has_completed()) {
        NotMyPtr<Job> other_job = get_job(
            SliceMut(m_impl->m_thread_data.get(), m_impl->m_thread_count), kMainThreadIndex);
        if (other_job) {
            {
                MODUS_PROFILE_JOBSYS_BLOCK("Execute Main Thread");
                other_job->execute();
            }
            other_job->finish();
        }
    }
}

JobHandle JobSystem::create(JobFunctionType&& fn) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    modus_assert(m_impl);
    NotMyPtr<JobPool> job_pool = get_tls_job_pool();
    modus_assert(job_pool);

    auto r = job_pool->allocate();
    r.job->function = std::move(fn);
    modus_assert(r.job->function);
    r.job->parent.reset();
    r.job->unfinised_jobs = 1;
    return JobHandle(r.index);
}

JobHandle JobSystem::create_as_child(const JobHandle parent, JobFunctionType&& fn) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    modus_assert(m_impl);
    NotMyPtr<JobPool> job_pool = get_tls_job_pool();
    modus_assert(job_pool);
    NotMyPtr<Job> parent_job = job_pool->job_from_handle(parent);
    modus_assert(parent_job->function);
    modus_assert(!parent_job->has_completed());
    modus_assert(parent_job->function);
    parent_job->unfinised_jobs++;
    auto r = job_pool->allocate();
    r.job->function = std::move(fn);
    modus_assert(r.job->function);
    r.job->parent = parent_job;
    r.job->unfinised_jobs = 1;
    return JobHandle(r.index);
}
JobHandle JobSystem::create_as_chained_child(const JobHandle parent, JobFunctionType&& fn) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    modus_assert(m_impl);
    NotMyPtr<JobPool> job_pool = get_tls_job_pool();
    modus_assert(job_pool);
    NotMyPtr<Job> parent_job = job_pool->job_from_handle(parent);
    modus_assert(parent_job->function);
    modus_assert(!parent_job->has_completed());
    modus_assert(parent_job->function);
    const u32 chain_index = parent_job->chained_job_count.fetch_add(1);
    if (chain_index >= kMaxChainedJobs) {
        modus_panic("We ran out of chained job space in the parent job");
    }
    parent_job->unfinised_jobs++;
    auto r = job_pool->allocate();
    r.job->function = std::move(fn);
    modus_assert(r.job->function);
    r.job->parent = parent_job;
    r.job->unfinised_jobs = 1;
    parent_job->chained_jobs[chain_index] = r.job;
    return JobHandle(r.index);
}
JobHandle JobSystem::create_chained(const JobHandle parent, JobFunctionType&& fn) {
    MODUS_PROFILE_JOBSYS_SCOPE();
    modus_assert(m_impl);
    NotMyPtr<JobPool> job_pool = get_tls_job_pool();
    modus_assert(job_pool);
    NotMyPtr<Job> parent_job = job_pool->job_from_handle(parent);
    modus_assert(parent_job->function);
    modus_assert(!parent_job->has_completed());
    modus_assert(parent_job->function);
    const u32 chain_index = parent_job->chained_job_count.fetch_add(1);
    if (chain_index >= kMaxChainedJobs) {
        modus_panic("We ran out of chained job space in the parent job");
    }
    auto r = job_pool->allocate();
    r.job->function = std::move(fn);
    modus_assert(r.job->function);
    r.job->unfinised_jobs = 1;
    parent_job->chained_jobs[chain_index] = r.job;
    return JobHandle(r.index);
}

}    // namespace modus::jobsys
