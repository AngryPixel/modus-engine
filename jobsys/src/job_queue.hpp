/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <jobsys_private.hpp>

#include <os/locks.hpp>

namespace modus::jobsys {

class WorkStealingJobQueue final {
   private:
    constexpr static u32 kNumJobs = 512;
    constexpr static u32 kMask = kNumJobs - 1u;
    struct Indices {
        u32 bottom = 0;
        u32 top = 0;
    };
    os::Mutex<Indices> m_indices;
    NotMyPtr<Job> m_jobs[kNumJobs];

   public:
    WorkStealingJobQueue() = default;
    MODUS_CLASS_DISABLE_COPY_MOVE(WorkStealingJobQueue);

    void push(NotMyPtr<Job> job);

    /// May return invalid ptr if no job is available
    NotMyPtr<Job> pop();

    /// May return invalid ptr if no job is available
    NotMyPtr<Job> steal();
};

/// Lockless work stealing dequeue. push() and pop() are expected to be called only by the owning
/// thread and steal() by other thread
class LocklessWorkStealingJobQueue final {
   private:
    constexpr static u32 kNumJobs = 512;
    constexpr static u32 kMask = kNumJobs - 1u;
    std::atomic<u32> m_bottom = 0;    // Tracks the push/pop operations LIFO
    std::atomic<u32> m_top = 0;       // Tracks the work stealing indices FIFO
    NotMyPtr<Job> m_jobs[kNumJobs];

   public:
    LocklessWorkStealingJobQueue() = default;
    MODUS_CLASS_DISABLE_COPY_MOVE(LocklessWorkStealingJobQueue);

    void push(NotMyPtr<Job> job);

    /// May return invalid ptr if no job is available
    NotMyPtr<Job> pop();

    /// May return invalid ptr if no job is available
    NotMyPtr<Job> steal();
};

}    // namespace modus::jobsys
