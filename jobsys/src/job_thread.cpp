/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <job_thread.hpp>
// clang-format on

namespace modus::jobsys {

namespace {
thread_local JobPool* tls_job_pool = nullptr;
thread_local QueueType* tls_job_queue = nullptr;
}    // namespace

void tls_init_pointers(ThreadData& data) {
    tls_job_pool = &data.job_pool;
    tls_job_queue = &data.job_queue;
}

void tls_reset_pointers() {
    tls_job_pool = nullptr;
    tls_job_queue = nullptr;
}

void Job::finish() {
    const i32 unfinished_jobs = --this->unfinised_jobs;
    if (unfinished_jobs == 0) {
        this->function.reset();
        if (this->parent) {
            this->parent->finish();
        }
    }

    const u32 chained_count = std::min(this->chained_job_count.exchange(0), kMaxChainedJobs);
    NotMyPtr<QueueType> queue = get_tls_job_queue();
    for (u32 i = 0; i < chained_count; ++i) {
        queue->push(this->chained_jobs[i]);
    }
}

NotMyPtr<JobPool> get_tls_job_pool() {
    return tls_job_pool;
}
NotMyPtr<QueueType> get_tls_job_queue() {
    return tls_job_queue;
}

JobThread::JobThread(SliceMut<ThreadData> thread_data, const u32 thread_index)
    : m_thread_data(thread_data), m_thread_index(thread_index) {
    modus_assert(thread_index < thread_data.size());
}

NotMyPtr<Job> get_job(SliceMut<ThreadData> thread_data, const u32 this_thread_index) {
    ThreadData& this_thread_data = thread_data[this_thread_index];
    NotMyPtr<Job> job = this_thread_data.job_queue.pop();
    if (!job) {
        // No pending jobs, try to steall one from another thread.
        const u32 random_index =
            u32(this_thread_data.random_generator.generate() % thread_data.size());
        if (random_index == this_thread_index) {
            // don't steal from self
            os::Thread::yield();
            return NotMyPtr<Job>();
        }
        ThreadData& other_thread_data = thread_data[random_index];
        job = other_thread_data.job_queue.steal();
        if (!job) {
            // failed to steal, yield
            os::Thread::yield();
            return NotMyPtr<Job>();
        }
    }
    return job;
}

void JobThread::set_data(SliceMut<ThreadData> thread_data, const u32 thread_index) {
    modus_assert(thread_index < thread_data.size());
    m_thread_index = thread_index;
    m_thread_data = thread_data;
}

void JobThread::run() {
    MODUS_PROFILE_MARK_THREAD("JobSystemThread");
    (void)os::Thread::set_current_thread_name(
        modus::format("JobSystemThread{:02}", m_thread_index));
    //(void)os::Thread::set_current_thread_priority(os::Thread::Priority::Low);
    ThreadData& my_thread_data = m_thread_data[m_thread_index];
    tls_init_pointers(my_thread_data);
    while (!my_thread_data.exit) {
        NotMyPtr<Job> job = get_job(m_thread_data, m_thread_index);
        if (job) {
            MODUS_PROFILE_MARK_FRAME {
                MODUS_PROFILE_JOBSYS_BLOCK("JobThread Execute");
                job->execute();
                job->finish();
            }
        }
    }
    tls_reset_pointers();
}

}    // namespace modus::jobsys
