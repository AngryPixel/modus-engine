/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <jobsys/jobsys.hpp>

namespace modus::jobsys {

constexpr u32 kMaxChainedJobs = 8;

struct Job {
    JobFunctionType function;
    NotMyPtr<Job> parent;
    std::atomic<i32> unfinised_jobs = 0;
    std::atomic<u32> chained_job_count = 0;
    NotMyPtr<Job> chained_jobs[kMaxChainedJobs];

    void finish();

    void execute() { this->function(); }

    bool has_completed() const { return this->unfinised_jobs == 0; }
};

constexpr u32 kNumJobsPerQueue = 512;
constexpr u32 kJobsQueueMask = kNumJobsPerQueue - 1u;

struct JobAllocResult {
    NotMyPtr<Job> job;
    u32 index;
};

class JobPool final {
   private:
    Array<Job, kNumJobsPerQueue> m_jobs;
    u32 m_allocated_jobs;

   public:
    JobPool() : m_allocated_jobs(0) {}

    MODUS_CLASS_DISABLE_COPY_MOVE(JobPool);

    JobAllocResult allocate() {
        const u32 index = m_allocated_jobs++;
        JobAllocResult result;
        result.job = &m_jobs[index & (kJobsQueueMask)];
        result.index = index;
        if (result.job->unfinised_jobs > 0) {
            modus_panic("Allocating a job with unfinished work, we may have run out of free jobs");
        }
        result.job->unfinised_jobs = 0;
        result.job->chained_job_count = 0;
        return result;
    }

    NotMyPtr<Job> job_from_handle(const JobHandle handle) {
        const u32 index = handle.value() & kJobsQueueMask;
        return &m_jobs[index];
    }
};

}    // namespace modus::jobsys