/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <job_queue.hpp>
// clang-format on

namespace modus::jobsys {

void WorkStealingJobQueue::push(NotMyPtr<Job> job) {
    auto accessor = m_indices.lock();
    auto& job_entry = m_jobs[accessor->bottom & kMask];
    if (job_entry) {
        modus_panic("we may have run out of free space in the job queue");
    }
    job_entry = job;
    ++accessor->bottom;
}

NotMyPtr<Job> WorkStealingJobQueue::pop() {
    auto accessor = m_indices.lock();
    if (accessor->top >= accessor->bottom) {
        return NotMyPtr<Job>();
    }
    accessor->bottom--;
    auto& job_ref = m_jobs[accessor->bottom & kMask];
    auto job = job_ref;
    job_ref.reset();
    return job;
}

NotMyPtr<Job> WorkStealingJobQueue::steal() {
    auto accessor = m_indices.lock();
    if (accessor->top >= accessor->bottom) {
        return NotMyPtr<Job>();
    }
    auto& job_ref = m_jobs[accessor->top & kMask];
    accessor->top++;
    auto job = job_ref;
    job_ref.reset();
    return job;
}

// Inspired by:
// https://blog.molecular-matters.com/2015/09/25/job-system-2-0-lock-free-work-stealing-part-3-going-lock-free/

void LocklessWorkStealingJobQueue::push(NotMyPtr<Job> job) {
    u32 bottom = m_bottom.load(std::memory_order_acquire);
    auto& job_entry = m_jobs[m_bottom & kMask];
    if (job_entry) {
        modus_panic("we may have run out of free space in the job queue");
    }
    job_entry = job;
    m_bottom.store(bottom + 1, std::memory_order_release);
}

NotMyPtr<Job> LocklessWorkStealingJobQueue::pop() {
    // Read/modify bottom before top to ensure concurrent steal() calls do not remove any jobs
    // without us noticing.
    u32 bottom = m_bottom.load(std::memory_order_acquire);
    if (bottom != 0) {
        bottom--;
    }
    m_bottom.exchange(bottom, std::memory_order_release);

    u32 top = m_top.load(std::memory_order_acquire);
    if (top <= bottom) {
        // non empty queue
        auto& job_ref = m_jobs[bottom & kMask];
        if (top != bottom) {
            // More than one job left in the queue
            auto job = job_ref;
            job_ref.reset();
            return job;
        }

        // Exactly one job left. We need to update top as well to ensure that only one of pop() or
        // steal() can access the last element in the queue.
        auto job = job_ref;
        if (!m_top.compare_exchange_strong(top, top + 1, std::memory_order_acq_rel)) {
            // We lost the compare exchange, which means steal got the job
            job.reset();
        } else {
            // We won the compare exchange, so we have the job
            job_ref.reset();
        }
        // Regardless of the outcome above we still need to update bottom to be same as top + 1
        // to match the empty queue setting
        m_bottom.store(top + 1, std::memory_order_release);
        return job;

    } else {
        // Queue is empty, we set the value of bottom to top
        m_bottom.store(top, std::memory_order_release);
        return NotMyPtr<Job>();
    }
}

NotMyPtr<Job> LocklessWorkStealingJobQueue::steal() {
    u32 top = m_top.load(std::memory_order_acquire);
    u32 bottom = m_bottom.load(std::memory_order_acquire);

    if (top < bottom) {
        auto& job_ref = m_jobs[top & kMask];
        if (!m_top.compare_exchange_strong(top, top + 1, std::memory_order_acq_rel)) {
            // Concurrent operation happened, we have to retry
            return NotMyPtr<Job>();
        }
        auto job = job_ref;
        job_ref.reset();
        return job;

    } else {
        return NotMyPtr<Job>();
    }
}

}    // namespace modus::jobsys
