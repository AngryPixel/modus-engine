/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <jobsys_private.hpp>
#include <os/thread.hpp>

#include <core/random.hpp>
#include <job_queue.hpp>
namespace modus::jobsys {

#define MODUS_JOBSYS_USE_LOCKLESS_WORK_QUEUE
#if defined(MODUS_JOBSYS_USE_LOCKLESS_WORK_QUEUE)
using QueueType = LocklessWorkStealingJobQueue;
#else
using QueueType = WorkStealingJobQueue;
#endif
struct ThreadData {
    JobPool job_pool;
    QueueType job_queue;
    random::RandomGenerator random_generator;
    volatile bool exit = false;

    ThreadData() = default;
    MODUS_CLASS_DISABLE_COPY_MOVE(ThreadData);
};

// Thread local setup
constexpr u32 kMainThreadIndex = 0;
void tls_init_pointers(ThreadData& data);
void tls_reset_pointers();
NotMyPtr<JobPool> get_tls_job_pool();
NotMyPtr<QueueType> get_tls_job_queue();

NotMyPtr<Job> get_job(SliceMut<ThreadData> thread_data, const u32 this_thread_index);

class JobThread final : public os::Thread {
   private:
    SliceMut<ThreadData> m_thread_data;
    u32 m_thread_index;

   public:
    JobThread() = default;
    JobThread(SliceMut<ThreadData> thread_data, const u32 thread_index);

    void set_data(SliceMut<ThreadData> thread_data, const u32 thread_index);

    void run() override;
};

}    // namespace modus::jobsys
