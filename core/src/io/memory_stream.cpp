/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <core/io/memory_stream.hpp>

namespace modus {

SliceStream::SliceStream(const SliceMut<u8> slice)
    : m_writable(slice), m_offset(0), m_mode(Mode::Write) {}

SliceStream::SliceStream(const Slice<u8> slice)
    : m_readable(slice), m_offset(0), m_mode(Mode::Read) {}

SliceStream::SliceStream(const StringSlice slice) : SliceStream(slice.as_bytes()) {}

IOResult<usize> SliceStream::read(SliceMut<u8> slice) {
    usize bytes_to_read = slice.size();
    if (m_offset + bytes_to_read > m_readable.size()) {
        bytes_to_read = m_readable.size() - m_offset;
    }

    if (bytes_to_read > 0) {
        u8* dst = slice.data();
        const u8* src = m_readable.data() + m_offset;
        memcpy(dst, src, bytes_to_read);
        m_offset += bytes_to_read;
    }

    return Ok(bytes_to_read);
}

IOResult<usize> SliceStream::write(const Slice<u8> slice) {
    if (m_mode != Mode::Write) {
        return Error(IOError::ReadOnly);
    }

    usize bytes_to_write = slice.size();
    if (m_offset + bytes_to_write > m_writable.size()) {
        bytes_to_write = m_writable.size() - m_offset;
    }

    if (bytes_to_write > 0) {
        memcpy(m_writable.data() + m_offset, slice.data(), bytes_to_write);
        m_offset += bytes_to_write;
    }

    return Ok(bytes_to_write);
}

IOResult<usize> SliceStream::seek(const usize position) {
    if (position >= m_readable.size()) {
        return Error(IOError::SeekFailed);
    }
    m_offset = position;
    return Ok(m_offset);
}

IOResult<usize> SliceStream::seek_end() {
    m_offset = m_readable.size();
    return Ok(m_offset);
}

IOResult<usize> SliceStream::position() const {
    return Ok(m_offset);
}

usize SliceStream::size() const {
    return m_readable.size();
}

template <>
class RamStream<DefaultAllocator>;
}    // namespace modus
