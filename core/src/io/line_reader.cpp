/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <core/io/line_reader.hpp>
// clang-format on

namespace modus {

LineReader::LineReader(IByteReader& reader) : m_reader(reader), m_offset(0) {}

Result<Optional<StringSlice>, IOError> LineReader::read_line(const StringSlice delimiter) {
    m_line.clear();
    while (true) {
        // Reach end of file
        if (m_offset == m_bytes_read) {
            return Ok(Optional<StringSlice>());
        }

        // check if we need to refill buffer
        if (m_offset == 0 || m_offset >= kBufferSize) {
            m_offset = 0;
            m_bytes_read = 0;
            auto r_read = m_reader.read(SliceMut(m_buffer).as_bytes());
            if (!r_read) {
                return Error(r_read.error());
            }

            if (*r_read == 0) {
                return Ok(Optional<StringSlice>(StringSlice(m_line)));
            }
            m_bytes_read = *r_read;
        }

        // find delimiter
        auto r_find = StringSlice(m_buffer + m_offset, m_bytes_read - m_offset).find(delimiter);
        if (!r_find) {
            m_line.append(m_buffer + m_offset, m_bytes_read);
            m_offset = 0;
            continue;
        } else if (*r_find == 0) {
            m_offset += 1;
            continue;
        }
        m_line.append(m_buffer + m_offset, *r_find);
        m_offset += *r_find + 1;
        return Ok(Optional<StringSlice>(StringSlice(m_line)));
    }
}
}    // namespace modus
