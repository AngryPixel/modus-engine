/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <core/debug/assert.hpp>
// clang-format on

namespace modus {

#if defined(__GNUC__) || defined(__clang__)
#define debug_break() __builtin_trap()
#elif defined(_MSC_VER)
#define debug_break() __debugbreak()
#else
#erorr "Debug break not defined for current compiler/platform"
#endif

void assert_impl(const char* expression, const char* filename, const u32 line) {
    fprintf(stderr, "%s:%u :: Expression '%s' failed.\n", filename, line, expression);
    fflush(stderr);
    fflush(stdout);
    debug_break();
}

void assert_impl(const char* expression,
                 const char* message,
                 const char* filename,
                 const u32 line) {
    fprintf(stderr, "%s:%u :: Expression '%s' failed. %s\n", filename, line, expression, message);
    fflush(stderr);
    fflush(stdout);
    debug_break();
}

void panic_impl(const char* message, const char* filename, const u32 line) {
    fprintf(stderr, "[PANIC] '%s'\n[PANIC] at  %s:%u\n", message, filename, line);
    fflush(stderr);
    fflush(stdout);
#if defined(MODUS_DEBUG)
    debug_break();
#endif
    ::exit(-1);
}

}    // namespace modus
