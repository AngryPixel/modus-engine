/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <core/allocator/allocator_libc.hpp>

namespace modus {

void* AllocatorLibC::allocate(const usize size) {
    void* ptr = ::malloc(size);
    MODUS_PROFILE_ALLOC(ptr, size);
    return ptr;
}

void* AllocatorLibC::realloc(void* ptr, const usize size) {
    MODUS_PROFILE_FREE(ptr);
    void* new_ptr = ::realloc(ptr, size);
    MODUS_PROFILE_ALLOC(new_ptr, size);
    return new_ptr;
}

void AllocatorLibC::deallocate(void* ptr) {
    MODUS_PROFILE_FREE(ptr);
    ::free(ptr);
}
}    // namespace modus
