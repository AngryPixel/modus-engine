/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/result.hpp>

// String operation utilities that can be shared.
// They all operate on non null terminate strings.
namespace modus::string_ops {

using FindResult = Result<usize, void>;

FindResult find(const char ch, const char* cstr, const usize size, const usize offset);

inline FindResult find(const char ch, const char* cstr, const usize size) {
    return find(ch, cstr, size, 0);
}

FindResult rfind(const char ch, const char* cstr, const usize size, const usize offset);

inline FindResult rfind(const char ch, const char* cstr, const usize size) {
    return rfind(ch, cstr, size, 0);
}

FindResult find(const char* pattern,
                const usize pattern_len,
                const char* cstr,
                const usize cstr_len,
                const usize offset);

inline FindResult find(const char* pattern,
                       const usize pattern_len,
                       const char* cstr,
                       const usize cstr_len) {
    return find(pattern, pattern_len, cstr, cstr_len, 0);
}

FindResult rfind(const char* pattern,
                 const usize pattern_len,
                 const char* cstr,
                 const usize cstr_len,
                 const usize offset);

inline FindResult rfind(const char* pattern,
                        const usize pattern_len,
                        const char* cstr,
                        const usize cstr_len) {
    return find(pattern, pattern_len, cstr, cstr_len, 0);
}

bool cstr_starts_with(const char* pattern,
                      const usize pattern_len,
                      const char* cstr,
                      const usize cstr_len);

bool cstr_ends_with(const char* pattern,
                    const usize pattern_len,
                    const char* cstr,
                    const usize cstr_len);

}    // namespace modus::string_ops
