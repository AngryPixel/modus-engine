/*
 * Copyright 2019-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <core/guid.hpp>

namespace modus {

static const u8 kCharToByte[256] = {
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

static const char kHexCharMap[] = {"0123456789ABCDEF"};

bool hex_is_valid_char(const char c) {
    return kCharToByte[u8(c)] != 0xFF;
}

MODUS_CORE_EXPORT std::pair<char, char> byte_to_hex(const u8 byte) {
    const char c1 = kHexCharMap[((byte & 0xf0) >> 4)];
    const char c2 = kHexCharMap[(byte & 0xf)];
    return {c1, c2};
}

MODUS_CORE_EXPORT Result<u8, void> hex_to_byte(const char c1, const char c2) {
    const u8 b1 = kCharToByte[u8(c1)];
    const u8 b2 = kCharToByte[u8(c2)];
    if (b1 == 0xFF || b2 == 0xFF) {
        return Error<>();
    }
    return Ok<u8>(((b1 & 0xf) << 4) | (b2 & 0xf));
}

MODUS_CORE_EXPORT Result<GID, void> GID::from_string(const StringSlice& slice) {
    char ch_one = '\0';
    char ch_two = '\0';
    bool scan_first_char = true;
    unsigned next_byte = 0;
    Array<u8, 16> bytes;
    for (const char& ch : slice) {
        if (ch == '-')
            continue;

        if (next_byte >= 16 || !hex_is_valid_char(ch)) {
            return Error<>();
        }

        if (scan_first_char) {
            ch_one = ch;
            scan_first_char = false;
        } else {
            ch_two = ch;
            auto byte_result = hex_to_byte(ch_one, ch_two);
            if (!byte_result) {
                return Error<>();
            }
            bytes[next_byte++] = byte_result.value();
            scan_first_char = true;
        }
    }

    // if there were fewer than 16 bytes in the string then guid is bad
    if (next_byte < 16) {
        return Error<>();
    }

    // Check for 0 GUID
    u32 sum = 0;
    for (auto& b : slice) {
        sum += b;
    }
    if (sum == 0) {
        return Error<>();
    }

    return Ok(GID(bytes));
}

Result<GID, void> GID::from_slice(const ByteSlice& slice) {
    if (slice.size() != 16) {
        return Error<>();
    }

    // Check for 0 GUID
    u32 sum = 0;
    for (auto& b : slice) {
        sum += b;
    }
    if (sum == 0) {
        return Error<>();
    }
    return Ok(GID(slice));
}

GID::GID(const ByteStorageType& bytes) {
    memcpy(m_bytes.data(), bytes.data(), bytes.size());
}

GID::GID(const ByteSlice& bytes) {
    modus_assert(bytes.size() == m_bytes.size());
    memcpy(m_bytes.data(), bytes.data(), bytes.size());
}

GID::GID() {
    memset(m_bytes.data(), 0, sizeof(m_bytes));
}

bool GID::operator==(const GID& rhs) const {
    return memcmp(m_bytes.data(), rhs.m_bytes.data(), sizeof(m_bytes)) == 0;
}

bool GID::operator!=(const GID& rhs) const {
    return memcmp(m_bytes.data(), rhs.m_bytes.data(), sizeof(m_bytes)) != 0;
}

String GID::to_str() const {
    return modus::format(
        "{:02X}{:02X}{:02X}{:02X}-{:02X}{:02X}-{:02X}{:02X}-{:02X}{:02X}-{:02X}"
        "{:02X}{:02X}{"
        ":02X}{:02X}{:02X}",
        m_bytes[0], m_bytes[1], m_bytes[2], m_bytes[3], m_bytes[4], m_bytes[5], m_bytes[6],
        m_bytes[7], m_bytes[8], m_bytes[9], m_bytes[10], m_bytes[11], m_bytes[12], m_bytes[13],
        m_bytes[14], m_bytes[15]);
}

}    // namespace modus
