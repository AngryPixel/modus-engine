/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <string_ops.hpp>

namespace modus::string_ops {

static FindResult boyer_moore_search(const char* pattern,
                                     const usize pattern_len,
                                     const char* cstr,
                                     const usize cstr_len,
                                     const usize offset) {
    if (offset + pattern_len > cstr_len) {
        return Error<>();
    }

    // Build look bad character rul table
    constexpr usize k_shift_table_size = 256;
    usize shift_table[k_shift_table_size];
    for (auto& value : shift_table) {
        value = pattern_len;
    }

    for (usize i = 0; i < pattern_len; ++i) {
        shift_table[usize(pattern[i])] = pattern_len - i - 1;
    }

    // perform search
    usize cstr_offset = offset + pattern_len - 1;
    while (cstr_offset < cstr_len) {
        usize needle = 0;
        while (needle < pattern_len && (cstr[cstr_offset] == pattern[pattern_len - 1 - needle])) {
            --cstr_offset;
            needle++;
        }
        if (needle == pattern_len) {
            return Ok(cstr_offset + 1);
        }
        cstr_offset += shift_table[usize(cstr[cstr_offset])];
    }

    return Error<>();
}

static FindResult boyer_moore_search_reverse(const char* pattern,
                                             const usize pattern_len,
                                             const char* cstr,
                                             const usize cstr_len,
                                             const usize offset) {
    if (offset + pattern_len > cstr_len) {
        return Error<>();
    }

    // Build look bad character rul table
    constexpr usize k_shift_table_size = 256;
    usize shift_table[k_shift_table_size];
    for (auto& value : shift_table) {
        value = pattern_len;
    }

    for (usize i = 0; i < pattern_len; ++i) {
        shift_table[usize(pattern[i])] = i;
    }

    // perform search
    usize cstr_offset = offset;
    while (cstr_offset < cstr_len) {
        usize needle = 0;
        usize reverse_offset = cstr_len - (cstr_offset + pattern_len);
        while (needle < pattern_len && (cstr[reverse_offset] == pattern[needle])) {
            ++reverse_offset;
            ++needle;
        }
        if (needle == pattern_len) {
            return Ok(cstr_len - cstr_offset - pattern_len);
        }
        cstr_offset += shift_table[usize(cstr[reverse_offset])];
    }

    return Error<>();
}

FindResult find(const char ch, const char* cstr, const usize size, const usize offset) {
    modus_assert(cstr != nullptr && size > 0);
    if (offset >= size) {
        return Error<>();
    }

    const void* result = ::memchr(cstr + offset, ch, size - offset);
    if (result == nullptr) {
        return Error<>();
    }

    const usize location = reinterpret_cast<usize>(result) - reinterpret_cast<usize>(cstr);
    return Ok(location);
}

FindResult rfind(const char ch, const char* cstr, const usize size, const usize offset) {
    modus_assert(cstr != nullptr && size > 0);
    if (offset >= size) {
        return Error<>();
    }

    const usize reverse_offset = size - offset;
#if defined(_MSC_VER)
    const void* result = nullptr;
    for (usize i = 0; i < reverse_offset; ++i) {
        const char c = cstr[reverse_offset - i - 1];
        if (c == ch) {
            result = &cstr[reverse_offset - i - 1];
            break;
        }
    }
#else
    const void* result = memrchr(cstr, ch, reverse_offset);
#endif
    if (result == nullptr) {
        return Error<>();
    }

    const usize location = reinterpret_cast<usize>(result) - reinterpret_cast<usize>(cstr);
    return Ok(location);
}

FindResult find(const char* pattern,
                const usize pattern_len,
                const char* cstr,
                const usize cstr_len,
                const usize offset) {
    modus_assert(pattern != nullptr);
    modus_assert(pattern_len > 0);
    modus_assert(cstr != nullptr);
    modus_assert(cstr_len > 0);
    return boyer_moore_search(pattern, pattern_len, cstr, cstr_len, offset);
}

FindResult rfind(const char* pattern,
                 const usize pattern_len,
                 const char* cstr,
                 const usize cstr_len,
                 const usize offset) {
    modus_assert(pattern != nullptr);
    modus_assert(pattern_len > 0);
    modus_assert(cstr != nullptr);
    modus_assert(cstr_len > 0);
    return boyer_moore_search_reverse(pattern, pattern_len, cstr, cstr_len, offset);
}

bool cstr_starts_with(const char* pattern,
                      const usize pattern_len,
                      const char* cstr,
                      const usize cstr_len) {
    modus_assert(pattern != nullptr);
    modus_assert(pattern_len > 0);
    modus_assert(cstr != nullptr);
    modus_assert(cstr_len > 0);

    if (pattern_len > cstr_len) {
        return false;
    }

    return memcmp(pattern, cstr, pattern_len) == 0;
}

bool cstr_ends_with(const char* pattern,
                    const usize pattern_len,
                    const char* cstr,
                    const usize cstr_len) {
    modus_assert(pattern != nullptr);
    modus_assert(pattern_len > 0);
    modus_assert(cstr != nullptr);
    modus_assert(cstr_len > 0);

    if (pattern_len > cstr_len) {
        return false;
    }

    return memcmp(pattern, cstr + (cstr_len - pattern_len), pattern_len) == 0;
}
}    // namespace modus::string_ops
