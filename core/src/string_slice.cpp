/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <core/string_slice.hpp>
#include <string_ops.hpp>

namespace modus {

StringSlice::StringSlice(const char* cstr)
    : SliceType(cstr, ::strlen(cstr)){modus_assert(cstr != nullptr)}

      StringSlice::StringSlice(const char* cstr, const usize size)
    : SliceType(cstr, size) {
    modus_assert(cstr != nullptr) modus_assert(size > 0);
}

StringSlice::StringSlice(const ByteSlice bytes)
    : SliceType(reinterpret_cast<const char*>(bytes.data()), bytes.size()) {}

StringSlice::FindResult StringSlice::find(const char ch, const usize offset) const {
    return string_ops::find(ch, data(), size(), offset);
}

StringSlice::FindResult StringSlice::rfind(const char ch, const usize offset) const {
    return string_ops::rfind(ch, data(), size(), offset);
}

StringSlice::FindResult StringSlice::find(const StringSlice& str, const usize offset) const {
    return string_ops::find(str.data(), str.size(), data(), size(), offset);
}

StringSlice::FindResult StringSlice::rfind(const StringSlice& str, const usize offset) const {
    return string_ops::rfind(str.data(), str.size(), data(), size(), offset);
}

bool StringSlice::starts_with(const StringSlice& str) const {
    return string_ops::cstr_starts_with(str.data(), str.size(), data(), size());
}

bool StringSlice::ends_with(const StringSlice& str) const {
    return string_ops::cstr_ends_with(str.data(), str.size(), data(), size());
}

String StringSlice::replace(const StringSlice pattern, const StringSlice with) const {
    // TODO: Boyer-Moore string search!
    if (pattern.size() > this->size() || pattern.data() == nullptr || with.data() == nullptr) {
        return to_str();
    }

    String result;
    result.reserve(this->size());

    size_t offset = 0;
    size_t lastFoundOffset = 0;

    while (offset + pattern.size() <= this->size()) {
        if (StringSlice(this->data() + offset, pattern.size()) == pattern) {
            result.append(this->data() + lastFoundOffset, offset - lastFoundOffset);
            result.append(with.data(), with.size());
            offset += pattern.size();
            lastFoundOffset = offset;
        } else {
            offset += 1;
        }
    }

    if (offset - lastFoundOffset != 0) {
        result.append(this->data() + lastFoundOffset, this->size() - lastFoundOffset);
    }
    return result;
}

bool StringSlice::operator==(const StringSlice& str) const {
    if (str.size() != size()) {
        return false;
    }
    if (str.size() == 0 && str.size() == 0) {
        return true;
    }

    return ::memcmp(data(), str.data(), size()) == 0;
}

bool StringSlice::operator!=(const StringSlice& str) const {
    if (str.size() != size()) {
        return true;
    }
    if (str.size() == 0 && str.size() == 0) {
        return false;
    }

    return ::memcmp(data(), str.data(), size()) != 0;
}

void StringSlice::to_cstr(char* buffer, const usize size) const {
    modus_assert(buffer != nullptr && size > 0);
    usize copy_size = std::min(this->size(), size - 1);
    if (copy_size != 0) {
        memcpy(buffer, data(), copy_size);
    }
    buffer[copy_size] = '\0';
}

}    // namespace modus
