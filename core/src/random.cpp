/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <core/random.hpp>
// clang-format on

#include <cfloat>

#include <core/hash.hpp>

namespace modus::random {

void XoroShiro128SS64::init(const u64 seed) {
    m_state[0] = hash<u64>{}(seed);
    m_state[1] = hash<u64>{}(m_state[0]);
}

static inline u64 rotl_64(const u64 x, int k) {
    return (x << k) | (x >> (64 - k));
}

u64 XoroShiro128SS64::generate() {
    const uint64_t s0 = m_state[0];
    uint64_t s1 = m_state[1];
    const uint64_t result = rotl_64(s0 * 5, 7) * 9;
    s1 ^= s0;
    m_state[0] = rotl_64(s0, 24) ^ s1 ^ (s1 << 16);    // a, b
    m_state[1] = rotl_64(s1, 37);                      // c
    return result;
}

f64 XoroShiro128SS64::generate_decimal() {
    return f64(generate() >> 11) * 0x1.0p-53;
}

void XoroShiro128SS32::init(const u32 seed) {
    m_state[0] = hash<u32>{}(seed);
    m_state[1] = hash<u32>{}(m_state[0]);
    m_state[2] = hash<u32>{}(m_state[1]);
    m_state[3] = hash<u32>{}(m_state[2]);
}

static inline uint32_t rotl_32(const uint32_t x, int k) {
    return (x << k) | (x >> (32 - k));
}

u32 XoroShiro128SS32::generate() {
    const uint32_t result = rotl_32(m_state[1] * 5, 7) * 9;
    const uint32_t t = m_state[1] << 9;
    m_state[2] ^= m_state[0];
    m_state[3] ^= m_state[1];
    m_state[1] ^= m_state[2];
    m_state[0] ^= m_state[3];
    m_state[2] ^= t;
    m_state[3] = rotl_32(m_state[3], 11);
    return result;
}

f32 XoroShiro128SS32::generate_decimal() {
    return ldexpf(f32(generate() & ((1 << FLT_MANT_DIG) - 1)), -FLT_MANT_DIG);
}

}    // namespace modus::random
