/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <core/hash.hpp>

namespace modus {

u32 hash_data32(const void* data, const size_t size) {
    return XXH32(data, size, 0);
}

u64 hash_data64(const void* data, const size_t size) {
    return XXH64(data, size, 0);
}

Hasher64::Hasher64(const u64 seed) : m_state(XXH64_createState()) {
    reset(seed);
}

Hasher64::~Hasher64() {
    XXH64_freeState(reinterpret_cast<XXH64_state_t*>(m_state));
}

void Hasher64::reset(const u64 seed) {
    XXH64_state_t* state = reinterpret_cast<XXH64_state_t*>(m_state);
    XXH64_reset(state, seed);
}

void Hasher64::update(const void* data, const usize size) {
    XXH64_state_t* state = reinterpret_cast<XXH64_state_t*>(m_state);
    [[maybe_unused]] const XXH_errorcode result = XXH64_update(state, data, size);
    modus_assert(result != XXH_ERROR);
}

u64 Hasher64::digest() {
    XXH64_state_t* state = reinterpret_cast<XXH64_state_t*>(m_state);
    return XXH64_digest(state);
}

Hasher32::Hasher32(const u32 seed) : m_state(XXH32_createState()) {
    reset(seed);
}

Hasher32::~Hasher32() {
    XXH32_freeState(reinterpret_cast<XXH32_state_t*>(m_state));
}

void Hasher32::reset(const u32 seed) {
    XXH32_state_t* state = reinterpret_cast<XXH32_state_t*>(m_state);
    XXH32_reset(state, seed);
}

void Hasher32::update(const void* data, const usize size) {
    XXH32_state_t* state = reinterpret_cast<XXH32_state_t*>(m_state);
    [[maybe_unused]] const XXH_errorcode result = XXH32_update(state, data, size);
    modus_assert(result != XXH_ERROR);
}

u32 Hasher32::digest() {
    XXH32_state_t* state = reinterpret_cast<XXH32_state_t*>(m_state);
    return XXH32_digest(state);
}
}    // namespace modus
