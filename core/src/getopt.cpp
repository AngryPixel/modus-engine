/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <core/getopt.hpp>
// clang-format on

namespace modus {

static bool is_option(const StringSlice& opt) {
    if (opt.size() < 2) {
        return false;
    }

    if (opt.size() == 2 && opt[0] == '-') {
        return true;
    }

    if (opt.size() >= 3 && opt[0] == '-' && opt[1] == '-') {
        return true;
    }

    return false;
}

CmdOption::CmdOption(const StringSlice id_short, const StringSlice id_long, const StringSlice help)
    : m_id_short(id_short), m_id_long(id_long), m_help(help), m_found(false) {
    modus_assert(id_short.size() == 2 || id_short.size() == 0);
#if defined(MODUS_ENABLE_ASSERTS)
    if (id_short.size() == 2) {
        modus_assert(id_short[0] == '-');
    }
#endif
    modus_assert(id_long.size() >= 3);
    modus_assert(id_long[0] == '-' && id_long[1] == '-');
}

CmdOptionParser::CmdOptionParser() : m_opt_help("-h", "--help", "Print this message") {}

Result<> CmdOptionParser::add(CmdOption& option) {
    auto it = std::find_if(m_options.begin(), m_options.end(),
                           [&option](const NotMyPtr<CmdOption>& value) {
                               if (option.has_short_id() && value->has_short_id() &&
                                   option.m_id_short == value->m_id_short) {
                                   return true;
                               }
                               return option.m_id_long == value->m_id_long;
                           });

    if (it != m_options.end()) {
        return Error<>();
    }
    m_max_option_len = std::max(m_max_option_len, option.m_id_long.size());
    m_options.push_back(NotMyPtr<CmdOption>(&option));
    return Ok<>();
}

String CmdOptionParser::help_string(const StringSlice& intro) const {
    String help_string = modus::format("\n{}\n\n", intro.to_str());

    for (auto& option : m_options) {
        help_string += modus::format("  {:<2},{:<{}} : {}\n", option->m_id_short, option->m_id_long,
                                     m_max_option_len, option->m_help);
    }
    help_string += '\n';
    return help_string;
}

Result<Slice<const char*>, String> CmdOptionParser::parse(const int argc, const char** argv) {
    auto find_option = [this](const StringSlice& slice) {
        return std::find_if(m_options.begin(), m_options.end(),
                            [&slice](const NotMyPtr<CmdOption>& value) {
                                if (value->has_short_id() && slice == value->m_id_short) {
                                    return true;
                                }
                                return slice == value->m_id_long;
                            });
    };
    (void)add(m_opt_help);

    int i = 1;
    while (i < argc) {
        const StringSlice arg(argv[i]);
        auto it = find_option(arg);
        if (it == m_options.end()) {
            if (is_option(arg)) {
                return Error(modus::format("Unknown option '{}'", arg));
            }
            return Ok(Slice(argv + i, argc - i));
        }
        i++;
        const int args_remaining = argc - i;
        modus_assert(args_remaining >= 0);

        auto result = (*it)->parse(args_remaining, argv + i);
        if (!result) {
            String error;
            switch (result.error()) {
                case CmdOptionError::InvalidParameter:
                    error = modus::format("Invalid argument supplied to option {}/{}: '{}'",
                                          (*it)->m_id_short, (*it)->m_id_long, argv[i]);
                    break;
                case CmdOptionError::MissingParameterValue:
                    error = modus::format("No argument was supplied to option {}/{}",
                                          (*it)->m_id_short, (*it)->m_id_long);
                    break;
                default:
                    modus_assert(false);
                    error = "Unknown Error";
                    break;
            }
            return Error(std::move(error));
        }
        i += result.value();
        (*it)->m_found = true;
    }
    if (i >= argc) {
        return Ok(Slice<const char*>());
    }
    return Ok(Slice(argv + i, argc - i));
}

CmdOptionEmpty::CmdOptionEmpty(const StringSlice id_short,
                               const StringSlice id_long,
                               const StringSlice help)
    : CmdOption(id_short, id_long, help) {}

CmdOptionResult<int> CmdOptionEmpty::parse(const int, const char**) {
    return Ok(0);
}

CmdOptionBool::CmdOptionBool(const StringSlice id_short,
                             const StringSlice id_long,
                             const StringSlice help,
                             const bool default_value)
    : CmdOption(id_short, id_long, help), m_value(default_value) {}

CmdOptionResult<int> CmdOptionBool::parse(const int argc, const char** argv) {
    if (argc < 1 || is_option(argv[0])) {
        return Error(CmdOptionError::MissingParameterValue);
    }
    const StringSlice arg_value = argv[0];
    if (StringSlice("true") == arg_value) {
        m_value = true;
        return Ok(1);
    }
    if (StringSlice("false") == arg_value) {
        m_value = false;
        return Ok(1);
    }
    return Error(CmdOptionError::InvalidParameter);
}

CmdOptionString::CmdOptionString(const StringSlice id_short,
                                 const StringSlice id_long,
                                 const StringSlice help,
                                 const StringSlice& default_value)
    : CmdOption(id_short, id_long, help), m_value(default_value.to_str()) {}

CmdOptionResult<int> CmdOptionString::parse(const int argc, const char** argv) {
    if (argc < 1) {
        return Error(CmdOptionError::MissingParameterValue);
    }
    m_value = argv[0];
    return Ok(1);
}

CmdOptionStringList::CmdOptionStringList(const StringSlice id_short,
                                         const StringSlice id_long,
                                         const StringSlice help)
    : CmdOption(id_short, id_long, help) {}

CmdOptionResult<int> modus::CmdOptionStringList::parse(const int argc, const char** argv) {
    if (argc < 1) {
        return Error(CmdOptionError::MissingParameterValue);
    }
    int i = 0;
    for (; i < argc; ++i) {
        if (is_option(argv[i])) {
            return Ok(i);
        }

        m_value.emplace_back(argv[i]);
    }
    return Ok(i);
}

CmdOptionStringMap::CmdOptionStringMap(const StringSlice id_short,
                                       const StringSlice id_long,
                                       const StringSlice help)
    : CmdOption(id_short, id_long, help) {}

CmdOptionResult<int> CmdOptionStringMap::parse(const int argc, const char** argv) {
    if (argc < 1) {
        return Error(CmdOptionError::MissingParameterValue);
    }
    int i = 0;
    for (; i < argc; ++i) {
        if (is_option(argv[i])) {
            return Ok(i);
        }
        constexpr char kDelimiter = ':';
        StringSlice arg(argv[i]);

        if (auto r = arg.find(kDelimiter); r) {
            if (*r == arg.size()) {
                return Error(CmdOptionError::InvalidParameter);
            }
            String key = StringSlice(arg.sub_slice(0, *r)).to_str();
            String value = StringSlice(arg.sub_slice(*r + 1, arg.size())).to_str();
            m_value.insert({std::move(key), std::move(value)});
        } else {
            if (m_value.size() == 0) {
                return Error(CmdOptionError::InvalidParameter);
            } else {
                return Ok(i);
            }
        }
    }
    return Ok(i);
}

CmdOptionF32::CmdOptionF32(const StringSlice id_short,
                           const StringSlice id_long,
                           const StringSlice help,
                           const f32 default_value)
    : CmdOption(id_short, id_long, help), m_value(default_value) {}

CmdOptionResult<int> CmdOptionF32::parse(const int argc, const char** argv) {
    if (argc < 1 || is_option(argv[0])) {
        return Error(CmdOptionError::MissingParameterValue);
    }

    char* end_ptr;
    f32 value = strtof(argv[0], &end_ptr);
    if (end_ptr == argv[0]) {
        return Error(CmdOptionError::InvalidParameter);
    }
    m_value = value;
    return Ok(1);
}

CmdOptionI32::CmdOptionI32(const StringSlice id_short,
                           const StringSlice id_long,
                           const StringSlice help,
                           const i32 default_value)
    : CmdOption(id_short, id_long, help), m_value(default_value) {}

CmdOptionResult<int> CmdOptionI32::parse(const int argc, const char** argv) {
    if (argc < 1 || is_option(argv[0])) {
        return Error(CmdOptionError::MissingParameterValue);
    }

    char* end_ptr;
    i32 value = strtol(argv[0], &end_ptr, 10);
    if (end_ptr == argv[0]) {
        return Error(CmdOptionError::InvalidParameter);
    }
    m_value = value;
    return Ok(1);
}

CmdOptionU32::CmdOptionU32(const StringSlice id_short,
                           const StringSlice id_long,
                           const StringSlice help,
                           const u32 default_value)
    : CmdOption(id_short, id_long, help), m_value(default_value) {}

CmdOptionResult<int> CmdOptionU32::parse(const int argc, const char** argv) {
    if (argc < 1 || is_option(argv[0])) {
        return Error(CmdOptionError::MissingParameterValue);
    }

    char* end_ptr;
    u32 value = strtoul(argv[0], &end_ptr, 10);
    if (end_ptr == argv[0]) {
        return Error(CmdOptionError::InvalidParameter);
    }
    m_value = value;
    return Ok(1);
}
}    // namespace modus
