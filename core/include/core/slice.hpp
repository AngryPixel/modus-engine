/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <core/core.hpp>

#include <core/pointer_iterator.hpp>
#include <core/result.hpp>

#if defined(MODUS_BOUNDS_CHECKING)
#define MODUS_SLICE_BOUNDS_CHECKING
#endif

#if defined(MODUS_SLICE_BOUNDS_CHECKING)
#define modus_slice_bounds_check(x) modus_assert_message(x, "Slice<T> bounds checking failed")
#else
#define modus_slice_bounds_check(x)
#endif

namespace modus {

template <typename T>
class Slice;

/// Type trait to for implicit construction of any type into a Slice.
template <typename T>
struct MODUS_CORE_EXPORT AsSlice {
    using value_type = const typename T::value_type;
    [[noreturn]] static Slice<value_type> to_slice(const T&) {
        modus_panic("Missing implementation");
    }
};

/// Slice, a non-owning view into a memory address with a fixed size.
template <typename T>
class MODUS_CORE_EXPORT Slice {
    static_assert(!std::is_const_v<T>, "Use non-const type for Slice<>");

   private:
    const T* m_ptr = nullptr;
    usize m_size = 0;

   public:
    using value_type = const T;
    using size_type = usize;
    using FindResult = Result<usize, void>;

    Slice() = default;

    ~Slice() = default;

    template <usize SIZE>
    inline Slice(const T (&array)[SIZE]) noexcept : Slice(&array[0], SIZE) {}

    inline Slice(value_type* ptr, const usize size) noexcept : m_ptr(ptr), m_size(size) {}

    inline Slice(const std::nullptr_t) noexcept : Slice() {}

    inline Slice(const Slice<T>& rhs) noexcept : Slice(rhs.m_ptr, rhs.m_size) {}

    inline Slice(Slice<T>&& rhs) noexcept : Slice(rhs.m_ptr, rhs.m_size) {
        rhs.m_ptr = nullptr;
        rhs.m_size = 0;
    }

    inline Slice<T>& operator=(const Slice<T>& rhs) noexcept {
        if (this != &rhs) {
            m_ptr = rhs.m_ptr;
            m_size = rhs.m_size;
        }
        return *this;
    }

    inline Slice<T>& operator=(Slice<T>&& rhs) noexcept {
        if (this != &rhs) {
            m_ptr = rhs.m_ptr;
            m_size = rhs.m_size;
            rhs.m_ptr = nullptr;
            rhs.m_size = 0;
        }
        return *this;
    }

    inline bool is_empty() const { return m_size == 0; }

    inline usize size() const { return m_size; }

    inline const value_type* data() const { return m_ptr; }

    inline const value_type* data_end() const { return m_ptr + m_size; }

    inline value_type& at(const usize index) const { return (*this)[index]; }

    inline value_type& operator[](const usize index) const {
        modus_slice_bounds_check(index < m_size);
        return m_ptr[index];
    }

    inline Slice<T> sub_slice(const usize length) const { return sub_slice(0, length); }

    inline Slice<T> sub_slice(const usize start, const usize length) const {
        if (start >= m_size) {
            return Slice<T>();
        }
        const usize sub_slice_size = start + length > m_size ? m_size - start : length;
        return Slice<T>(m_ptr + start, sub_slice_size);
    }

    // Iterator
    using const_iterator = PointerIterator<const value_type>;

    inline const_iterator begin() const { return const_iterator(data(), data(), data_end()); }

    inline const_iterator end() const { return const_iterator(data_end(), data(), data_end()); }

    inline FindResult find(const T& value) const { return find(value, 0); }

    inline FindResult find(const T& value, const usize offset) const {
        for (usize i = offset; i < m_size; ++i) {
            if (m_ptr[i] == value) {
                return Ok(i);
            }
        }
        return Error<>();
    }

    FindResult rfind(const T& value) const { return rfind(value, 0); }

    FindResult rfind(const T& value, const usize offset) const {
        for (usize i = offset; i < m_size; ++i) {
            const usize index = m_size - 1 - i;
            if (m_ptr[index] == value) {
                return Ok(index);
            }
        }
        return Error<>();
    }

    inline Slice<u8> as_bytes() const {
        static_assert(sizeof(T) >= sizeof(u8));
        if constexpr (sizeof(T) > sizeof(u8)) {
            return Slice<u8>(reinterpret_cast<const u8*>(data()), sizeof(T) * m_size);
        } else {
            return Slice<u8>(reinterpret_cast<const u8*>(data()), m_size);
        }
    }

    template <typename U>
    inline Slice<U> rebind() const {
        static_assert(sizeof(U) == sizeof(T));
        return Slice<U>(reinterpret_cast<const U*>(data()), this->size());
    }

    inline bool operator==(const Slice<T>& rhs) const {
        if (m_size != rhs.m_size) {
            return false;
        }
        for (usize i = 0; i < m_size; ++i) {
            if (m_ptr[i] != rhs.m_ptr[i]) {
                return false;
            }
        }
        return true;
    }

    inline bool operator!=(const Slice<T>& rhs) const {
        if (m_size == rhs.m_size) {
            return false;
        }
        for (usize i = 0; i < m_size; ++i) {
            if (m_ptr[i] == rhs.m_ptr[i]) {
                return false;
            }
        }
        return true;
    }
};

template <typename T, usize SIZE>
inline Slice<T> make_slice(const T (&array)[SIZE]) {
    return Slice<T>(array);
}

template <typename T>
class SliceMut;

template <typename T>
struct MODUS_CORE_EXPORT AsSliceMut {
    using value_type = typename T::value_type;
    [[noreturn]] static SliceMut<value_type> to_slice(T&) { modus_panic("Missing implementation"); }
};

template <typename T>
class SliceMut : public Slice<T> {
    static_assert(!std::is_const_v<T>, "Use Slice<T> for const templates");

   public:
    using value_type = T;
    using size_type = typename Slice<T>::size_type;
    using FindResult = typename Slice<T>::FindResult;

    SliceMut() = default;

    ~SliceMut() = default;

    template <usize SIZE>
    inline SliceMut(T (&array)[SIZE]) noexcept : Slice<T>(&array[0], SIZE) {}

    inline SliceMut(value_type* ptr, const usize size) noexcept : Slice<T>(ptr, size) {}

    inline SliceMut(const std::nullptr_t) noexcept : Slice<T>() {}

    inline SliceMut(const SliceMut<T>& rhs) noexcept
        : Slice<T>(static_cast<const Slice<T>&>(rhs)) {}

    inline SliceMut(SliceMut<T>&& rhs) noexcept : Slice<T>(static_cast<Slice<T>&&>(rhs)) {}

    SliceMut<T>& operator=(const SliceMut<T>& rhs) noexcept = default;

    SliceMut<T>& operator=(SliceMut<T>&& rhs) noexcept = default;

    inline value_type* data() { return const_cast<value_type*>(Slice<T>::data()); }

    inline value_type* data_end() { return const_cast<value_type*>(Slice<T>::data_end()); }

    inline value_type& at(const usize index) { return (*this)[index]; }

    inline value_type& operator[](const usize index) {
        return const_cast<value_type&>(Slice<T>::at(index));
    }

    inline SliceMut<T> sub_slice(const usize length) const { return sub_slice(0, length); }

    inline SliceMut<T> sub_slice(const usize start, const usize length) const {
        Slice<T> slice = Slice<T>::sub_slice(start, length);
        return SliceMut<T>(const_cast<value_type*>(slice.data()), slice.size());
    }

    // Iterator
    using iterator = PointerIterator<value_type>;

    inline iterator begin() { return iterator(data(), data(), data_end()); }

    inline iterator end() { return iterator(data_end(), data(), data_end()); }

    SliceMut<u8> as_bytes() const {
        return SliceMut<u8>(reinterpret_cast<u8*>(const_cast<T*>(Slice<T>::data())),
                            this->size() * sizeof(T));
    }

    template <typename U>
    inline SliceMut<U> rebind() const {
        static_assert(sizeof(U) == sizeof(T));
        return Slice<U>(reinterpret_cast<U*>(const_cast<T*>(Slice<T>::data())), this->size());
    }
};

using ByteSlice = Slice<u8>;
using ByteSliceMut = SliceMut<u8>;

template <typename T>
inline ByteSlice make_byte_slice(const T& value) {
    return ByteSlice(reinterpret_cast<const u8*>(&value), sizeof(T));
}

template <typename T>
inline ByteSliceMut make_byte_slice_mut(T& value) {
    return ByteSliceMut(reinterpret_cast<u8*>(&value), sizeof(T));
}

template <typename T>
inline Slice<typename T::value_type> make_slice(const T& v) {
    return Slice<typename T::value_type>(v.data(), v.size());
}

template <typename T>
inline SliceMut<typename T::value_type> make_slice_mut(T& v) {
    return SliceMut<typename T::value_type>(v.data(), v.size());
}

}    // namespace modus

#undef MODUS_SLICE_BOUNDS_CHECKING
#undef modus_slice_bounds_check
