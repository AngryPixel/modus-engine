/*
 * Copyright 2019-2021 by leander beernaert
 *
 * this file is part of modus.
 *
 * modus is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 *
 * modus is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 *
 * you should have received a copy of the gnu general public license
 * along with modus. if not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

namespace modus {

// Implementation in guid.cpp

MODUS_CORE_EXPORT std::pair<char, char> byte_to_hex(const u8 byte);

MODUS_CORE_EXPORT Result<u8, void> hex_to_byte(const char c1, const char c2);

MODUS_CORE_EXPORT bool hex_is_valid_char(const char c);

}    // namespace modus
