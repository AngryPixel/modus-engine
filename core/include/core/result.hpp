/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/debug/assert.hpp>
#include <core/module_config.hpp>

namespace modus {

template <typename T = void>
class MODUS_CORE_EXPORT Ok {
    static_assert(!std::is_rvalue_reference<T>::value && !std::is_lvalue_reference<T>::value,
                  "T must be an rvalue so it can be owned by the result");
    static_assert(std::is_nothrow_move_constructible<T>::value ||
                  std::is_move_constructible<T>::value);
    static_assert(std::is_nothrow_move_assignable<T>::value || std::is_move_assignable<T>::value);

   public:
    Ok() = delete;
    Ok(const T& data) noexcept : m_data(data) {}
    Ok(T&& data) noexcept : m_data(std::move(data)) {}
    Ok(const Ok<T>&) = delete;
    Ok(Ok<T>&&) = default;
    Ok& operator=(const Ok<T>&) = delete;
    Ok& operator=(Ok<T>&&) = default;

   public:
    T m_data;
};

template <>
class Ok<> {};

template <typename T = void>
class MODUS_CORE_EXPORT Error {
    static_assert(!std::is_rvalue_reference<T>::value && !std::is_lvalue_reference<T>::value,
                  "T must be an rvalue so it can be owned by the result");
    static_assert(std::is_move_constructible<T>::value && std::is_move_assignable<T>::value);

   public:
    Error() = delete;
    Error(const T& data) : m_data(data) {}
    Error(T&& data) noexcept : m_data(std::move(data)) {}
    Error(const Error<T>&) = delete;
    Error(Error<T>&&) = default;
    Error& operator=(const Error<T>&) = delete;
    Error& operator=(Error<T>&&) = default;

   public:
    T m_data;
};

template <>
class Error<> {};

/// Result type, either holds an error or a value.
template <typename T = void, typename E = void>
class MODUS_CORE_EXPORT [[nodiscard]] Result {
    enum class State { value, error, unitialized };

   public:
    using ok_type = Ok<T>;
    using error_type = Error<E>;

    Result() = delete;

    Result(const Result<T, E>&) = delete;

    Result& operator=(const Result<T, E>&) = delete;

    Result(ok_type&& ok) : m_type(std::forward<ok_type>(ok)), m_state(State::value) {}

    Result(error_type&& error) : m_error(std::forward<error_type>(error)), m_state(State::error) {}

    Result(Result<T, E>&& rhs) noexcept : m_state(rhs.m_state) {
        if (m_state == State::value) {
            MODUS_INPLACE_NEW(&m_type) ok_type(std::move(rhs.m_type));
        } else if (m_state == State::error) {
            MODUS_INPLACE_NEW(&m_error) Error(std::move(rhs.m_error));
        }
        rhs.m_state = State::unitialized;
    }

    Result& operator=(Result<T, E>&& rhs) noexcept {
        if (this != &rhs) {
            // clean up existing state
            if (m_state == State::value) {
                m_type.~ok_type();
            } else if (m_state == State::error) {
                m_error.~error_type();
            }

            m_state = rhs.m_state;
            // move data
            if (m_state == State::value) {
                MODUS_INPLACE_NEW(&m_type) ok_type(std::move(rhs.m_type));
            } else if (m_state == State::error) {
                MODUS_INPLACE_NEW(&m_error) error_type(std::move(rhs.m_error));
            }

            rhs.m_state = State::unitialized;
        }
        return *this;
    }

    ~Result() {
        if (m_state == State::value) {
            m_type.~ok_type();
        } else if (m_state == State::error) {
            m_error.~error_type();
        }
    }

    inline bool is_error() const { return m_state == State::error; }

    inline bool is_value() const { return m_state == State::value; }

    inline explicit operator bool() const { return is_value(); }

    inline void expect(const char* str = "Result does not contain a value!") const {
        if (!is_value()) {
            modus_panic(str);
        }
    }

    template <typename V = E>
    typename std::enable_if<!std::is_same<V, void>::value, const E>::type& error() const {
        modus_assert(is_error());
        return m_error.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, T>::type& value() {
        modus_assert(m_state == State::value);
        return m_type.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, const T>::type& value() const {
        modus_assert(m_state == State::value);
        return m_type.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, const T>::type& value_or_panic(
        const char* str = "Result does not have a value") const {
        if (m_state != State::value) {
            modus_panic(str);
        }
        return m_type.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, T>::type&& value_or_panic(
        const char* str = "Result does not have a value") {
        if (m_state != State::value) {
            modus_panic(str);
        }
        return std::move(m_type.m_data);
    }

    template <typename V = T>
    [[nodiscard]] typename std::enable_if<!std::is_same<V, void>::value, T>::type&&
    release_value() {
        modus_assert(m_state == State::value);
        return std::move(m_type.m_data);
    }

    template <typename V = E>
    [[nodiscard]] typename std::enable_if<!std::is_same<V, void>::value, E>::type&&
    release_error() {
        modus_assert(m_state == State::error);
        return std::move(m_error.m_data);
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, T>::type& operator*() {
        modus_assert(m_state == State::value);
        return m_type.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, const T>::type& operator*() const {
        modus_assert(m_state == State::value);
        return m_type.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, T>::type* operator->() {
        modus_assert(m_state == State::value);
        return &m_type.m_data;
    }

    template <typename V = T>
    typename std::enable_if<!std::is_same<V, void>::value, const T>::type* operator->() const {
        modus_assert(m_state == State::value);
        return &m_type.m_data;
    }

   private:
    union {
        ok_type m_type;
        error_type m_error;
    };
    State m_state;
};

}    // namespace modus

namespace fmt {
template <typename Ok, typename Error>
struct formatter<modus::Result<Ok, Error>> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::Result<Ok, Error>& result, FormatContext& ctx) {
        if (result.is_value()) {
            if constexpr (std::is_same_v<Ok, void>) {
                return format_to(ctx.out(), "Ok()");
            } else {
                return format_to(ctx.out(), "Ok({})", result.value());
            }
        } else {
            if constexpr (std::is_same_v<Error, void>) {
                return format_to(ctx.out(), "Error()");
            } else {
                return format_to(ctx.out(), "Error({})", result.error());
            }
        }
    }
};
}    // namespace fmt
