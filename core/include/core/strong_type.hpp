/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/hash.hpp>

namespace modus {

template <typename T>
class StrongType {
    static_assert(std::is_pod_v<T>, "StrongType is only to be used with pod types");
    static_assert(std::is_move_constructible_v<T> && std::is_move_assignable_v<T>);

   private:
    T m_value;

   public:
    using this_type = StrongType<T>;

   protected:
    StrongType() = delete;

    inline explicit StrongType(const T& value) : m_value(value) {}

   public:
    using value_type = T;
    StrongType(const this_type&) = default;
    StrongType(this_type&&) = default;
    StrongType& operator=(const this_type&) = default;
    StrongType& operator=(this_type&&) = default;

    inline T& value() { return m_value; }

    inline const T& value() const { return m_value; }

    inline bool operator==(const this_type& other) const { return m_value == other.m_value; }

    inline bool operator!=(const this_type& other) const { return m_value != other.m_value; }

    inline bool operator<(const this_type& other) const { return m_value < other.m_value; }

    inline bool operator<=(const this_type& other) const { return m_value <= other.m_value; }

    inline bool operator>(const this_type& other) const { return m_value > other.m_value; }

    inline bool operator>=(const this_type& other) const { return m_value >= other.m_value; }
};
}    // namespace modus

#define MODUS_STRONG_TYPE_CLASS(Name, Type, DefaultValue)                                   \
    class Name : public modus::StrongType<Type> {                                           \
       public:                                                                              \
        using base_type = modus::StrongType<Type>;                                          \
        inline Name() : base_type(DefaultValue) {}                                          \
        inline explicit Name(const typename base_type::value_type& val) : base_type(val) {} \
        Name(const Name&) = default;                                                        \
        Name(Name&&) = default;                                                             \
        Name& operator=(const Name&) = default;                                             \
        Name& operator=(Name&&) = default;

#define MODUS_DECLARE_STRONG_TYPE(Name, Type, DefaultValue) \
    MODUS_STRONG_TYPE_CLASS(Name, Type, DefaultValue)       \
    }

namespace modus {
template <typename T>
struct hash<StrongType<T>> {
    std::size_t operator()(const StrongType<T>& type) const {
        return modus::hash<T>{}(type.value());
    }
};
}    // namespace modus

namespace fmt {
template <typename T>
struct formatter<modus::StrongType<T>> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::StrongType<T>& strong_type, FormatContext& ctx) {
        return format_to(ctx.out(), "StrongType{{{}}}", strong_type.value());
    }
};
}    // namespace fmt
