/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

namespace modus {

template <typename FunctionSignature>
class Delegate;

template <typename ReturnType, typename... ARGS>
class Delegate<ReturnType(ARGS...)> {
   public:
    using FuncType = ReturnType (*)(void*, ARGS...);

   private:
    void* m_object;
    FuncType m_func;

   public:
    using this_type = Delegate<ReturnType(ARGS...)>;
    Delegate() : m_object(nullptr), m_func(nullptr) {}

    Delegate(void* object, FuncType f) : m_object(object), m_func(f) {}

    ReturnType operator()(ARGS... args) {
        modus_assert(m_func != nullptr);
        if constexpr (std::is_same<ReturnType, void>::value) {
            (*m_func)(m_object, std::forward<ARGS>(args)...);
        } else {
            return (*m_func)(m_object, std::forward<ARGS>(args)...);
        }
    }

    explicit operator bool() const { return m_func != nullptr; }

    bool operator==(const this_type& rhs) const {
        return m_object == rhs.m_object && m_func == rhs.m_func;
    }

    bool operator!=(const this_type& rhs) const {
        return !(m_object == rhs.m_object && m_func == rhs.m_func);
    }

    template <typename U, ReturnType (U::*Func)(ARGS...) const>
    void bind(U* o) {
        struct _ {
            static ReturnType wrapper(void* o, ARGS... a) {
                return (static_cast<U*>(o)->*Func)(std::forward<ARGS>(a)...);
            }
        };
        m_object = o;
        m_func = _::wrapper;
    }

    template <typename U, ReturnType (U::*Func)(ARGS...)>
    void bind(U* o) {
        struct _ {
            static ReturnType wrapper(void* o, ARGS... a) {
                return (static_cast<U*>(o)->*Func)(std::forward<ARGS>(a)...);
            }
        };
        m_object = o;
        m_func = _::wrapper;
    }

    template <ReturnType (*Func)(ARGS...)>
    void bind() {
        struct _ {
            static ReturnType wrapper(void*, ARGS... a) {
                return (*Func)(std::forward<ARGS>(a)...);
            }
        };
        m_object = nullptr;
        m_func = _::wrapper;
    }

    template <typename U, ReturnType (U::*Func)(ARGS...) const>
    static this_type build(U* o) {
        struct _ {
            static ReturnType wrapper(void* o, ARGS... a) {
                return (static_cast<U*>(o)->*Func)(std::forward<ARGS>(a)...);
            }
        };
        return this_type(o, _::wrapper);
    }

    template <typename U, ReturnType (U::*Func)(ARGS...)>
    static this_type build(U* o) {
        struct _ {
            static ReturnType wrapper(void* o, ARGS... a) {
                return (static_cast<U*>(o)->*Func)(std::forward<ARGS>(a)...);
            }
        };
        return this_type(o, _::wrapper);
    }

    template <ReturnType (*Func)(ARGS...)>
    static this_type build() {
        struct _ {
            static ReturnType wrapper(void*, ARGS... a) {
                return (*Func)(std::forward<ARGS>(a)...);
            }
        };
        return this_type(nullptr, _::wrapper);
    }

    void reset() {
        m_object = nullptr;
        m_func = nullptr;
    }
};

}    // namespace modus
