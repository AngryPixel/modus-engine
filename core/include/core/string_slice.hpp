/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <core/slice.hpp>

namespace modus {

class StringSlice;

/// Type trait to for implicit construction of any type into a StringSlice.
template <typename T>
struct MODUS_CORE_EXPORT AsStringSlice {
    [[noreturn]] static StringSlice to_string_slice(const T&);
};

// Slice which inlcudes extra methods which operate on strings
class MODUS_CORE_EXPORT StringSlice : public Slice<char> {
    using SliceType = Slice<char>;

   public:
    using iterator = SliceType::const_iterator;
    using const_iterator = SliceType::const_iterator;
    using FindResult = SliceType::FindResult;

    StringSlice() = default;

    ~StringSlice() = default;

    StringSlice(const StringSlice&) = default;

    StringSlice(StringSlice&&) = default;

    inline StringSlice(const std::string_view& view) : SliceType(view.data(), view.size()) {}

    inline StringSlice(const SliceType& rhs) : SliceType(rhs) {}

    inline StringSlice(const String& str) : StringSlice(str.c_str(), str.size()) {}

    inline StringSlice(std::nullptr_t) : StringSlice() {}

    StringSlice(const char* cstr);

    StringSlice(const char* cstr, const size_t size);

    StringSlice(const ByteSlice bytes);

    StringSlice& operator=(const StringSlice&) = default;

    StringSlice& operator=(StringSlice&&) = default;

    StringSlice& operator=(const char* cstr) {
        *this = StringSlice(cstr);
        return *this;
    }

    StringSlice& operator=(const ByteSlice& bytes) {
        *this = StringSlice(bytes);
        return *this;
    }

    bool starts_with(const StringSlice& str) const;

    bool ends_with(const StringSlice& str) const;

    inline FindResult find(const char ch) const { return find(ch, 0); }

    FindResult find(const char ch, const usize offset) const;

    inline FindResult rfind(const char ch) const { return rfind(ch, 0); }

    FindResult rfind(const char ch, const usize offset) const;

    inline FindResult find(const StringSlice& str) const { return find(str, 0); }

    FindResult find(const StringSlice& str, const usize offset) const;

    FindResult rfind(const StringSlice& str) const { return rfind(str, 0); }

    FindResult rfind(const StringSlice& str, const usize offset) const;

    String replace(const StringSlice pattern, const StringSlice with) const;

    bool operator==(const StringSlice& str) const;
    bool operator!=(const StringSlice& str) const;

    void to_cstr(char* buffer, const usize size) const;

    template <typename Allocator = DefaultAllocatorString>
    StringWithAllocator<Allocator> to_str() const {
        return size() != 0 ? StringWithAllocator<Allocator>(data(), size())
                           : StringWithAllocator<Allocator>();
    }
};

}    // namespace modus

namespace fmt {
template <>
struct formatter<modus::StringSlice> : formatter<fmt::string_view> {
    template <typename FormatContext>
    auto format(const modus::StringSlice& p, FormatContext& ctx) {
        const fmt::string_view view(p.data(), p.size());
        return formatter<fmt::string_view>::format(view, ctx);
    }
};
}    // namespace fmt

namespace modus {
template <>
struct hash<StringSlice> {
    std::size_t operator()(const StringSlice& slice) const {
        return modus::hash_data(slice.data(), slice.size());
    }
};

template <typename Allocator>
struct hash<modus::StringWithAllocator<Allocator>> {
    std::size_t operator()(const modus::StringWithAllocator<Allocator>& str) const {
        return modus::hash<StringSlice>{}(StringSlice(str.c_str(), str.size()));
    }
};
}    // namespace modus
