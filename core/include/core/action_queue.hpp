/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <core/core.hpp>

#include <core/allocator/linear_allocator.hpp>

namespace modus {

namespace detail {

template <typename ReturnType, typename... Args>
struct MODUS_CORE_EXPORT ActionHeader {
    size_t size = 0;
    void (*dtor)(void*) = nullptr;
    ReturnType (*fn)(void*, Args...) = nullptr;

    void destroy() {
        if (dtor != nullptr) {
            dtor(ptr());
        }
    }

    void* ptr() { return (this + 1); }

    template <typename T>
    void capture(T&) {
        size = sizeof(T) + sizeof(*this);
        dtor = [](void* p) {
            T* t = reinterpret_cast<T*>(p);
            { t->~T(); }
        };
        fn = [](void* p, Args... args) {
            T* t = reinterpret_cast<T*>(p);
            return (t->operator())(std::forward<Args>(args)...);
        };
    }

    template <ReturnType (*FN)(Args...)>
    void capture() {
        dtor = nullptr;
        size = sizeof(*this);
        fn = [](void*, Args... args) { return FN(std::forward<Args>(args)...); };
    }

    ReturnType operator()(Args... args) {
        void* p = ptr();
        if constexpr (std::is_same_v<ReturnType, void>) {
            fn(p, std::forward<Args>(args)...);
        } else {
            return fn(p, std::forward<Args>(args)...);
        }
    }
};

}    // namespace detail

template <bool ThreadSafe, typename FunctionSignature, typename Allocator>
class ActionQueueBase;

/// Push lambdas or pure functions to be executed at a later stage. All required memory is allocated
/// using a fixed linear allocator. This also allows you push lambdas with captured data.
template <bool ThreadSafe, typename Allocator, typename ReturnType, typename... Args>
class ActionQueueBase<ThreadSafe, ReturnType(Args...), Allocator> {
   private:
    using HeaderType = detail::ActionHeader<ReturnType, Args...>;
    std::conditional_t<ThreadSafe, ThreadSafeLinearAllocator<Allocator>, LinearAllocator<Allocator>>
        m_allocator;
    std::conditional_t<ThreadSafe, std::atomic<u32>, u32> m_queued_actions;
#if defined(MODUS_DEBUG)
    std::conditional_t<ThreadSafe, std::atomic<bool>, bool> m_executing = false;
#endif
   protected:
    struct Iterator {
        HeaderType* ptr = nullptr;
        u32 action_count = 0;
        u32 action_index = 0;

        [[nodiscard]] HeaderType* next() {
            HeaderType* result = nullptr;
            if (action_index < action_count) {
                result = ptr;
                action_index++;
                ptr =
                    reinterpret_cast<HeaderType*>(reinterpret_cast<uintptr_t>(ptr) + result->size);
            }
            return result;
        }
    };

    Iterator iterator() {
        return Iterator{
            reinterpret_cast<HeaderType*>(reinterpret_cast<uintptr_t>(m_allocator.data())),
            m_queued_actions, 0};
    }

   public:
    using this_type = ActionQueueBase<ThreadSafe, ReturnType(Args...), Allocator>;
    ActionQueueBase() : m_allocator(), m_queued_actions(0) {}

    ~ActionQueueBase() { shutdown(); }

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(ActionQueueBase, this_type);

    Result<> initialize(const usize allocator_size) {
        m_queued_actions = 0;
#if defined(MODUS_ENABLE_ASSERTS)
        m_executing = false;
#endif
        return m_allocator.initialize(allocator_size);
    }

    void shutdown() {
#if defined(MODUS_ENABLE_ASSERTS)
        m_executing = true;
#endif
        Iterator it = iterator();
        HeaderType* header = nullptr;
        while ((header = it.next()) != nullptr) {
            header->destroy();
        }
        m_allocator.shutdown();
        m_queued_actions = 0;
    }

    void execute(Args... args) {
#if defined(MODUS_ENABLE_ASSERTS)
        m_executing = true;
#endif
        Iterator it = iterator();
        HeaderType* header = nullptr;
        while ((header = it.next()) != nullptr) {
            (*header)(std::forward<Args>(args)...);
            header->destroy();
        }
        m_queued_actions = 0;
        m_allocator.clear();
#if defined(MODUS_ENABLE_ASSERTS)
        m_executing = false;
#endif
    }

    template <typename T>
    Result<> queue(T&& action) {
#if defined(MODUS_ENABLE_ASSERTS)
        modus_assert_message(!m_executing, "Can't queue items to queue while executing commands");
#endif
        HeaderType header;
        header.capture(action);

        void* data = m_allocator.allocate(header.size);
        if (data == nullptr) {
            return Error<>();
        }

        HeaderType* header_ptr = reinterpret_cast<HeaderType*>(data);
        *header_ptr = header;
        new (header_ptr + 1) T(std::move(action));
        m_queued_actions++;
        return Ok<>();
    }

    template <ReturnType (*FN)(Args... args)>
    Result<> queue() {
#if defined(MODUS_ENABLE_ASSERTS)
        modus_assert_message(!m_executing, "Can't queue items to queue while executing commands");
#endif
        HeaderType header;
        header.template capture<FN>();

        void* data = m_allocator.allocate(header.size);
        if (data == nullptr) {
            return Error<>();
        }

        HeaderType* header_ptr = reinterpret_cast<HeaderType*>(data);
        *header_ptr = header;
        m_queued_actions++;
        return Ok<>();
    }

    u32 queued_action_count() const { return m_queued_actions; }
};

template <typename FunctionSignature, typename Allocator>
using ActionQueue = ActionQueueBase<false, FunctionSignature, Allocator>;

template <typename FunctionSignature, typename Allocator>
using ThreadSafeActionQueue = ActionQueueBase<true, FunctionSignature, Allocator>;
}    // namespace modus
