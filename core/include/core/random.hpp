/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <core/core.hpp>

namespace modus::random {

// xoshiro/xoroshiro random number generator http://prng.di.unimi.it

/// XoroShiro128** pseudo random number generator
/// Default for 64bit build
class MODUS_CORE_EXPORT XoroShiro128SS64 {
   private:
    u64 m_state[2];

   public:
    using value_type = u64;
    using decimal_type = double;

    void init(const u64 seed);

    u64 generate();

    /// Generate random f64 between 0-1
    f64 generate_decimal();
};

/// XoroShiro128** pseudo random number generator
/// Default for 32bit build
class MODUS_CORE_EXPORT XoroShiro128SS32 {
   private:
    u32 m_state[4];

   public:
    using value_type = u32;
    using decimal_type = f32;

    void init(const u32 seed);

    u32 generate();

    /// Genreate random f32 between 0-1
    f32 generate_decimal();
};

#if defined(MODUS_32BIT)
using RandomGenerator = XoroShiro128SS32;
#else
using RandomGenerator = XoroShiro128SS64;
#endif

}    // namespace modus::random
