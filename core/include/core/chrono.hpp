/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

using DSeconds = std::chrono::duration<f64>;
using FSeconds = std::chrono::duration<f32>;
using ISeconds = std::chrono::duration<i64>;

using DMilisec = std::chrono::duration<f64, std::milli>;
using FMilisec = std::chrono::duration<f32, std::milli>;
using IMilisec = std::chrono::duration<i64, std::milli>;

using DMicrosec = std::chrono::duration<f64, std::micro>;
using IMicrosec = std::chrono::duration<i64, std::micro>;

using DNanosec = std::chrono::duration<f64, std::nano>;
using INanosec = std::chrono::duration<i64, std::nano>;

template <typename A, typename B, typename C>
constexpr inline A chrono_cast(const std::chrono::duration<B, C>& v) {
    return std::chrono::duration_cast<A, B, C>(v);
}

}    // namespace modus
