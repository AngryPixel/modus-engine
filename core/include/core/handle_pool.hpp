/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

#include <core/allocator/pool_allocator.hpp>
#include <core/containers.hpp>
#include <core/handle_pool_shared.hpp>

namespace modus {

/// Generational Handle Pool with no upper limit on the number of handles
/// which can be allocated.
/// Note if no reserve call is made, the first allocation will allocate 8
/// handles.
template <typename T, typename Allocator, typename HandleType = Handle>
class MODUS_CORE_EXPORT HandlePool {
    static_assert(sizeof(HandleType) == sizeof(Handle));
    static_assert(std::is_move_constructible_v<T>);

   private:
    struct Private : public handle_pool_shared::HandlePrivate {
        T* pointer = nullptr;
    };
    Vector<Private, Allocator> m_handles;
    PoolAllocatorTyped<T, Allocator> m_pool;
    u32 m_count;
    u32 m_next_free;

   public:
    using this_type = HandlePool<T, Allocator, HandleType>;

    HandlePool(const u32 entries_per_block)
        : m_handles(), m_pool(entries_per_block), m_count(0), m_next_free(0) {}

    ~HandlePool() {
        using namespace handle_pool_shared;
        for (auto& priv : m_handles) {
            if (priv.active) {
                m_pool.destroy(priv.pointer);
            }
        }
    }

    Result<> reserve(const u32 size) {
        using namespace handle_pool_shared;
        if (size >= k_max_size) {
            return Error<>();
        }
        if (!expand(size)) {
            return Error<>();
        }
        return Ok<>();
    }

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(HandlePool, this_type);

    Result<Pair<HandleType, NotMyPtr<T>>, void> create(T&& value) {
        using namespace handle_pool_shared;
        auto result = create_internal();
        if (result) {
            HandlePublic public_hdl = handle_to_handle_public(result.value());
            const u32 index = public_hdl.index;
            T* ptr = m_pool.construct(std::forward<T>(value));
            if (ptr == nullptr) {
                erase_internal(index);
                return Error<>();
            }
            m_handles[index].pointer = ptr;
            return Ok(Pair<HandleType, NotMyPtr<T>>(result.value(), ptr));
        }
        return Error<>();
    }

    Result<Pair<HandleType, NotMyPtr<T>>, void> create(const T& value) {
        using namespace handle_pool_shared;
        auto result = create_internal();
        if (result) {
            HandlePublic public_hdl = handle_to_handle_public(result.value());
            const u32 index = public_hdl.index;
            T* ptr = m_pool.construct(value);
            if (ptr == nullptr) {
                erase_internal(index);
                return Error<>();
            }
            m_handles[index].pointer = ptr;
            return Ok(Pair<HandleType, NotMyPtr<T>>(result.value(), ptr));
        }
        return Error<>();
    }

    Result<> erase(const HandleType hdl) {
        using namespace handle_pool_shared;
        Result<u32, void> lookup_result = contains_internal(hdl);
        if (!lookup_result) {
            return Error<>();
        }
        const u32 index = lookup_result.value();
        modus_assert(index < m_handles.size());
        erase_internal(index);
        return Ok<>();
    }

    Result<NotMyPtr<T>, void> get(const HandleType hdl) {
        Result<u32, void> lookup_result = contains_internal(hdl);
        if (!lookup_result) {
            return Error<>();
        }
        return Ok(NotMyPtr<T>(m_handles[*lookup_result].pointer));
    }

    Result<NotMyPtr<const T>, void> get(const HandleType hdl) const {
        Result<u32, void> lookup_result = contains_internal(hdl);
        if (!lookup_result) {
            return Error<>();
        }
        return Ok(NotMyPtr<const T>(m_handles[*lookup_result].pointer));
    }

    bool contains(const HandleType hdl) const { return contains_internal(hdl).is_value(); }

    inline u32 count() const { return m_count; }

    void clear() {
        for (u32 i = 0; i < m_handles.size(); ++i) {
            Private& priv = m_handles[i];
            if (priv.pointer != nullptr) {
                m_pool.destroy(priv.pointer);
                priv.pointer = nullptr;
            }
            priv.next_free = i + 1;
            priv.counter = 0;
            priv.active = 0;
        }
        m_next_free = m_handles.size();
        m_count = 0;
    }

    template <typename Callable>
    inline void for_each(Callable&& fn) {
        using namespace handle_pool_shared;
        for (auto& priv_hdl : m_handles) {
            if (priv_hdl.active) {
                modus_assert(priv_hdl.pointer != nullptr);
                fn(*priv_hdl.pointer);
            }
        }
    }

    template <typename Callable>
    inline void for_each(Callable&& fn) const {
        using namespace handle_pool_shared;
        for (const auto& priv_hdl : m_handles) {
            if (priv_hdl.active) {
                modus_assert(priv_hdl.pointer != nullptr);
                fn(*priv_hdl.pointer);
            }
        }
    }

   private:
    void erase_internal(const u32 index) {
        modus_assert(index < m_handles.size());
        Private& priv_hdl = m_handles[index];
        if (priv_hdl.pointer != nullptr) {
            m_pool.destroy(priv_hdl.pointer);
            priv_hdl.pointer = nullptr;
        }
        priv_hdl.active = 0;
        priv_hdl.counter++;
        priv_hdl.next_free = m_next_free;
        m_next_free = index;
        m_count--;
    }

    Result<u32, void> contains_internal(const HandleType hdl) const {
        using namespace handle_pool_shared;
        const HandlePublic hdl_pub = handle_to_handle_public(hdl);
        const u32 index = hdl_pub.index;
        const u32 counter = hdl_pub.counter;
        if (index >= m_handles.size()) {
            return Error<>();
        }
        const HandlePrivate& priv_hdl = m_handles[index];
        return (priv_hdl.active && priv_hdl.counter == counter) ? Result<u32, void>(Ok(index))
                                                                : Result<u32, void>(Error<>());
    }

    Result<HandleType, void> create_internal() {
        using namespace handle_pool_shared;
        if (m_next_free >= m_handles.size()) {
            if (!expand(m_handles.size() * 2)) {
                return Error<>();
            }
        }
        const u32 hdl_index = m_next_free;
        HandlePrivate& hdl = m_handles[hdl_index];
        // Update nextfree
        m_next_free = hdl.next_free;
        // Update internal hdl
        hdl.active = 1;
        m_count++;

        // Prepare public handle
        HandlePublic public_hdl;
        public_hdl.__pad__ = 0;
        public_hdl.index = hdl_index;
        public_hdl.counter = hdl.counter;
        return Ok(handle_public_to_handle<HandleType>(public_hdl));
    }

    [[nodiscard]] bool expand(u32 next_size) {
        using namespace handle_pool_shared;
        if (next_size == 0) {
            next_size = 8;
        } else if (next_size >= k_max_size) {
            if (m_handles.size() == k_max_size) {
                // reached max capacity addressable by handles
                return false;
            }
            next_size = k_max_size;
        }
        modus_assert(next_size > m_handles.size()) modus_assert(m_next_free = k_max_size);
        const u32 cur_size = m_handles.size();
        m_handles.resize(next_size);
        for (u32 i = cur_size; i < m_handles.size(); ++i) {
            Private& priv = m_handles[i];
            priv.next_free = i + 1;
            priv.pointer = nullptr;
            priv.counter = 0;
            priv.active = 0;
        }
        m_handles[m_handles.size() - 1].next_free = m_handles.size();
        m_next_free = cur_size;
        return true;
    }
};

}    // namespace modus
