/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// When not using pch, include the pch header here to ensure code completion engines based on
// clangd can find all the required headers.
#if !defined(MODUS_USE_PCH)
#include <core/core_pch.h>
#endif

#if defined(__GNUC__) || defined(__clang__)
#define MODUS_FORCE_INLINE __attribute__((always_inline))
#elif defined(_MSC_VER)
#define MODUS_FORCE_INLINE __forceinline
#endif

// clang-format off
#include <core/debug/assert.hpp>
//clang-format on

// force bounds checking in debug
#if defined(MODUS_DEBUG)
#define MODUS_BOUNDS_CHECKING
#endif

#define MODUS_UNUSED(x)(void)x

#define MODUS_CLASS_DEFAULT_COPY(Class) \
    Class(const Class&) = default;\
    Class& operator=(const Class&) = default

#define MODUS_CLASS_DEFAULT_MOVE(Class) \
    Class(Class&&) noexcept = default;\
    Class& operator=(Class&&) noexcept = default

#define MODUS_CLASS_DEFAULT_COPY_MOVE(Class) \
    MODUS_CLASS_DEFAULT_COPY(Class); \
    MODUS_CLASS_DEFAULT_MOVE(Class)

#define MODUS_CLASS_DISABLE_COPY(Class) \
    Class(const Class&) = delete;\
    Class& operator=(const Class&) = delete

#define MODUS_CLASS_DISABLE_MOVE(Class) \
    Class(Class&&) = delete;\
    Class& operator=(Class&&) = delete

#define MODUS_CLASS_DISABLE_COPY_MOVE(Class) \
    MODUS_CLASS_DISABLE_COPY(Class); \
    MODUS_CLASS_DISABLE_MOVE(Class)

#define MODUS_CLASS_TEMPLATE_DEFAULT_COPY(Class, ThisType) \
    Class(const ThisType&) = default;\
    ThisType& operator=(const ThisType&) = default

#define MODUS_CLASS_TEMPLATE_DISABLE_COPY(Class, ThisType) \
    Class(const ThisType&) = delete;\
    ThisType& operator=(const ThisType&) = delete

#define MODUS_CLASS_TEMPLATE_DISABLE_MOVE(Class, ThisType) \
    Class(ThisType&&) = delete;\
    ThisType& operator=(ThisType&&) = delete

#define MODUS_CLASS_TEMPLATE_DEFAULT_MOVE(Class, ThisType) \
    Class(ThisType&&) noexcept = default;\
    ThisType& operator=(ThisType&&) noexcept= default

#define MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(Class, ThisType) \
    MODUS_CLASS_TEMPLATE_DISABLE_COPY(Class, ThisType); \
    MODUS_CLASS_TEMPLATE_DISABLE_MOVE(Class, ThisType)

namespace modus {
template<typename T, usize N>
static constexpr usize array_size(const T(&)[N]) {
    return N;
}

inline constexpr usize size_aligned(const usize size, const usize alignment) {
    modus_assert(alignment > 0);
    return size + alignment - 1;
}
template<typename T>
inline constexpr T* align_ptr(T* ptr , const usize alignment) {
    modus_assert(alignment > 0);
    uintptr_t addr = reinterpret_cast<uintptr_t>(ptr);
    addr += (alignment - 1);
    addr &= ~(alignment-1);
    modus_assert(addr % alignment == 0);
    return reinterpret_cast<T*>(addr);
}

template<typename T>
inline constexpr const T* align_ptr(const T* ptr, const usize alignment) {
    modus_assert(alignment > 0);
    uintptr_t addr = reinterpret_cast<uintptr_t>(ptr);
    addr += (alignment - 1);
    addr &= ~(alignment-1);
    modus_assert(addr % alignment == 0);
    return reinterpret_cast<T*>(addr);
}

template <typename T>
constexpr usize aligned_type_size() {
    struct alignas(T) AlignedType {
        u8 data[sizeof(T)];
    };
    return sizeof(AlignedType);
}

}

#define MODUS_NEW new (std::nothrow)
#define MODUS_INPLACE_NEW(x) new (x)
#define MODUS_DELETE delete

#define MODUS_CHECKED_NEW(VAR, EXP) \
    VAR = MODUS_NEW EXP;            \
    if (VAR == nullptr)             \
    modus_panic("Failed to allocate memory")

#include <core/result.hpp>
#include <core/chrono.hpp>
