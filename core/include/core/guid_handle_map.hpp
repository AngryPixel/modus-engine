/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/fixed_handle_pool.hpp>
#include <core/guid.hpp>

namespace modus {

template <typename T,
          usize Size,
          typename HandleType = Handle,
          typename Allocator = DefaultAllocator>
class MODUS_CORE_EXPORT GUIDHandleMap {
   private:
    using PoolType = FixedHandlePool<T, Size, HandleType>;
    using HashMapType = HashMapWithAllocator<GID, HandleType, Allocator>;

    PoolType m_types;
    HashMapType m_types_by_id;

   public:
    using value_type = T;
    using handle_type = HandleType;
    using this_type = GUIDHandleMap<T, Size, Handle, Allocator>;

    GUIDHandleMap() = default;

    virtual ~GUIDHandleMap() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(GUIDHandleMap, this_type);

    inline Result<handle_type, void> add(const GID& id, value_type&& type) {
        auto it = m_types_by_id.find(id);
        if (it != m_types_by_id.end()) {
            return Error<>();
        }

        auto add_result = m_types.create(std::forward<value_type>(type));
        if (!add_result) {
            return Error<>();
        }

        m_types_by_id.insert(std::make_pair(id, add_result.value().first));
        return Ok(add_result.value().first);
    }

    inline Result<handle_type, void> add(const GID& id, const value_type& type) {
        auto it = m_types_by_id.find(id);
        if (it != m_types_by_id.end()) {
            return Error<>();
        }

        auto add_result = m_types.create(type);
        if (!add_result) {
            return Error<>();
        }

        m_types_by_id.insert(std::make_pair(id, add_result.value().first));
        return Ok(add_result.value().first);
    }

    inline Result<handle_type, void> get_handle(const GID& id) const {
        auto it = m_types_by_id.find(id);
        if (it == m_types_by_id.end()) {
            return Error<>();
        }

        return Ok(it->second);
    }

    inline Result<NotMyPtr<value_type>, void> get(const GID& id) {
        auto it = m_types_by_id.find(id);
        if (it == m_types_by_id.end()) {
            return Error<>();
        }
        return m_types.get(it->second);
    }

    inline Result<NotMyPtr<const value_type>, void> get(const GID& id) const {
        auto it = m_types_by_id.find(id);
        if (it == m_types_by_id.end()) {
            return Error<>();
        }
        return m_types.get(it->second);
    }

    inline Result<NotMyPtr<const value_type>, void> get(const handle_type handle) const {
        return m_types.get(handle);
    }

    inline Result<NotMyPtr<value_type>, void> get(const handle_type handle) {
        return m_types.get(handle);
    }

    inline usize size() const { return m_types.count(); }

    void erase(const handle_type handle) {
        auto r_object = m_types.get(handle);
        if (r_object) {
            m_types_by_id.erase(r_object.value()->guid());
            (void)m_types.erase(handle);
        }
    }

    void erase(const GID& id) {
        auto it = m_types_by_id.find(id);
        if (it != m_types_by_id.end()) {
            (void)m_types.erase(it->second);
            m_types_by_id.erase(it);
        }
    }

    inline void clear() {
        m_types.clear();
        m_types_by_id.clear();
    }

    inline constexpr usize capacity() const { return Size; }

    template <typename Callable>
    inline void for_each(Callable&& fn) {
        m_types.for_each(std::forward<Callable>(fn));
    }

    template <typename Callable>
    inline void for_each(Callable&& fn) const {
        m_types.for_each(std::forward<Callable>(fn));
    }
};
}    // namespace modus
