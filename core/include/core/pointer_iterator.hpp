/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

#if defined(MODUS_BOUNDS_CHECKING)
#define MODUS_POINTER_ITERATOR_BOUNDS_CHECKING
#endif

#if defined(MODUS_POINTER_ITERATOR_BOUNDS_CHECKING)
#define modus_pointer_iterator_bounds_check(x) \
    modus_assert_message(x, "PointerIterator<T> bounds checking failed")
#else
#define modus_pointer_iterator_bounds_check(x)
#endif

#if !defined(MODUS_POINTER_ITERATOR_STL_COMPATABILITY)
#define MODUS_POINTER_ITERATOR_STL_COMPATABILITY
#endif

#if defined(MODUS_POINTER_ITERATOR_STL_COMPATABILITY)
#include <iterator>
#endif

namespace modus {

/// Iterator of a contiguous memory block
template <typename T>
class MODUS_CORE_EXPORT PointerIterator {
   public:
    using value_type = T;
    using difference_type = isize;
    using iterator_category = std::random_access_iterator_tag;

   private:
    value_type* m_ptr = nullptr;
#if defined(MODUS_POINTER_ITERATOR_BOUNDS_CHECKING)
    const value_type* m_begin = nullptr;
    const value_type* m_end = nullptr;
#endif
   public:
    PointerIterator() = default;

    ~PointerIterator() = default;

    PointerIterator(value_type* ptr, const value_type* begin, const value_type* end)
        : m_ptr(ptr)
#if defined(MODUS_POINTER_ITERATOR_BOUNDS_CHECKING)
          ,
          m_begin(begin),
          m_end(end)
#endif
    {
        (void)begin;
        (void)end;
    }

    inline PointerIterator(const PointerIterator& rhs)
        : m_ptr(rhs.m_ptr)
#if defined(MODUS_POINTER_ITERATOR_BOUNDS_CHECKING)
          ,
          m_begin(rhs.m_begin),
          m_end(rhs.m_end)
#endif
    {
    }

    inline PointerIterator& operator=(const PointerIterator& rhs) {
        if (this != &rhs) {
            m_ptr = rhs.m_ptr;
#if defined(MODUS_POINTER_ITERATOR_BOUNDS_CHECKING)
            m_begin = rhs.m_begin;
            m_end = rhs.m_end;
#endif
        }
        return *this;
    }

    inline PointerIterator& operator++() {
        modus_pointer_iterator_bounds_check(m_ptr + 1 <= m_end);
        ++m_ptr;
        return *this;
    }

    inline PointerIterator operator++(int) {
        modus_pointer_iterator_bounds_check(m_ptr + 1 <= m_end);
        auto copy = *this;
        ++m_ptr;
        return copy;
    }

    inline PointerIterator& operator--() {
        modus_pointer_iterator_bounds_check(m_ptr <= m_end);
        modus_pointer_iterator_bounds_check(m_ptr - 1 >= m_begin);
        --m_ptr;
        return *this;
    }

    inline PointerIterator operator--(int) {
        modus_pointer_iterator_bounds_check(m_ptr - 1 >= m_begin);
        modus_pointer_iterator_bounds_check(m_ptr != m_end);
        auto copy = *this;
        --m_ptr;
        return copy;
    }

    inline PointerIterator operator+(const usize diff) const {
        modus_pointer_iterator_bounds_check(m_ptr + diff <= m_end);
        auto copy = *this;
        copy += diff;
        return copy;
    }

    inline PointerIterator& operator+=(const usize diff) {
        modus_pointer_iterator_bounds_check(m_ptr + diff <= m_end);
        m_ptr += diff;
        return *this;
    }

    inline PointerIterator operator-(const usize diff) const {
        modus_pointer_iterator_bounds_check(m_ptr - diff >= m_begin);
        modus_pointer_iterator_bounds_check(m_ptr <= m_end);
        auto copy = *this;
        copy -= diff;
        return copy;
    }

    inline PointerIterator& operator-=(const usize diff) {
        modus_pointer_iterator_bounds_check(m_ptr - diff >= m_begin);
        modus_pointer_iterator_bounds_check(m_ptr <= m_end);
        m_ptr -= diff;
        return *this;
    }

    inline isize operator-(const PointerIterator& rhs) const {
        modus_assert(m_ptr != nullptr && rhs.m_ptr != nullptr);
        modus_pointer_iterator_bounds_check(m_ptr >= rhs.m_ptr);
        return (reinterpret_cast<isize>(m_ptr) - reinterpret_cast<isize>(rhs.m_ptr)) / sizeof(T);
    }

    inline bool operator==(const PointerIterator& rhs) const { return m_ptr == rhs.m_ptr; }

    inline bool operator!=(const PointerIterator& rhs) const { return m_ptr != rhs.m_ptr; }

    inline bool operator<(const PointerIterator& rhs) const { return m_ptr < rhs.m_ptr; }

    inline bool operator<=(const PointerIterator& rhs) const { return m_ptr <= rhs.m_ptr; }

    inline bool operator>(const PointerIterator& rhs) const { return m_ptr > rhs.m_ptr; }

    inline bool operator>=(const PointerIterator& rhs) const { return m_ptr >= rhs.m_ptr; }

    inline value_type& operator*() const {
        modus_assert(m_ptr != nullptr);
        modus_pointer_iterator_bounds_check(m_ptr != m_end);
        return *m_ptr;
    }

    inline value_type* operator->() const {
        modus_assert(m_ptr != nullptr);
        modus_pointer_iterator_bounds_check(m_ptr != m_end);
        return m_ptr;
    }

    inline value_type& operator[](const difference_type offset) {
        modus_assert(m_ptr != nullptr);
        modus_pointer_iterator_bounds_check(m_ptr + offset <= m_end);
        modus_pointer_iterator_bounds_check(m_ptr - offset >= m_begin);
        return *(m_ptr + offset);
    }

    inline const value_type& operator[](const difference_type offset) const {
        modus_assert(m_ptr != nullptr);
        modus_pointer_iterator_bounds_check(m_ptr + offset <= m_end);
        modus_pointer_iterator_bounds_check(m_ptr - offset >= m_begin);
        return *(m_ptr + offset);
    }
};

template <typename T, usize SIZE>
inline PointerIterator<T> pointer_iterator_begin(T (&array)[SIZE]) {
    static_assert(SIZE >= usize(0));
    return PointerIterator<T>(&array[0], &array[0], &array[SIZE]);
}

template <typename T, usize SIZE>
inline PointerIterator<T> pointer_iterator_end(T (&array)[SIZE]) {
    static_assert(SIZE >= usize(0));
    return PointerIterator<T>(&array[SIZE], &array[0], &array[SIZE]);
}
}    // namespace modus

#if defined(MODUS_POINTER_ITERATOR_STL_COMPATABILITY)
namespace std {
template <typename T>
struct iterator_traits<modus::PointerIterator<T>> {
    using value_type = typename modus::PointerIterator<T>::value_type;
    using difference_type = typename modus::PointerIterator<T>::difference_type;
    using iterator_category = std::random_access_iterator_tag;
};
}    // namespace std
#endif

#undef MODUS_POINTER_ITERATOR_BOUNDS_CHECKING
#undef modus_pointer_iterator_bounds_check
