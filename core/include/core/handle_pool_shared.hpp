/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <core/core.hpp>

#include <core/strong_type.hpp>

namespace modus {

#define MODUS_DECLARE_HANDLE_TYPE(Name, InvalidValue)                \
    MODUS_STRONG_TYPE_CLASS(Name, u32, InvalidValue)                 \
    inline bool is_valid() const { return value() != InvalidValue; } \
    inline explicit operator bool() const { return is_valid(); }     \
    }

MODUS_DECLARE_HANDLE_TYPE(Handle, std::numeric_limits<u32>::max());

namespace handle_pool_shared {

static constexpr u32 k_max_size = (1 << 16) - 1;
struct HandlePrivate {
    unsigned active : 1;
    unsigned counter : 15;
    unsigned next_free : 16;
};

struct HandlePublic {
    unsigned __pad__ : 1;
    unsigned counter : 15;
    unsigned index : 16;
};

static_assert(sizeof(HandlePrivate) == sizeof(Handle::value_type));
static_assert(sizeof(HandlePublic) == sizeof(Handle::value_type));

template <typename T>
struct Blob {
    struct Data {
        alignas(T) u8 data[sizeof(T)];
    } data;
    inline T* to_ptr() { return reinterpret_cast<T*>(&data); }
    inline const T* to_ptr() const { return reinterpret_cast<const T*>(&data); }
    inline void construct(T&& value) { MODUS_INPLACE_NEW(to_ptr()) T(std::forward<T>(value)); }
    inline void construct(const T& value) { new (to_ptr()) T(value); }
    inline void destroy() { to_ptr()->~T(); }
};

union HandlePublicUnion {
    HandlePublic pub;
    Handle::value_type hdl;
};

template <typename HandleType>
inline static HandlePublic handle_to_handle_public(const HandleType hdl) {
    static_assert(sizeof(HandleType) == sizeof(Handle));
    HandlePublicUnion conversion;
    conversion.hdl = hdl.value();
    return conversion.pub;
}

template <typename HandleType>
inline static HandleType handle_public_to_handle(const HandlePublic& pub) {
    static_assert(sizeof(HandleType) == sizeof(Handle));
    HandlePublicUnion conversion;
    conversion.pub = pub;
    return HandleType(conversion.hdl);
}
}    // namespace handle_pool_shared
}    // namespace modus
