/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

#include <core/allocator/allocator_libc.hpp>
#include <core/allocator/allocator_traits.hpp>
#include <core/hash.hpp>
#include <core/non_owning_ptr.hpp>

#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wuseless-cast"
#pragma GCC diagnostic ignored "-Woverflow"
#pragma GCC diagnostic ignored "-Wnull-dereference"
#endif

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdouble-promotion"
#endif

#if defined(__clang__)
#pragma clang diagnostic pop
#endif

#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic pop
#endif

namespace modus {

using DefaultAllocator = modus::AllocatorLibC;
using DefaultAllocatorString = modus::AllocatorLibC;

template <typename T, typename Allocator = DefaultAllocator>
using Vector = std::vector<T, StdAllocatorWrapperStateless<T, Allocator>>;

using String = std::basic_string<char,
                                 std::char_traits<char>,
                                 StdAllocatorWrapperStateless<char, DefaultAllocatorString>>;

template <typename Allocator>
using StringWithAllocator =
    std::basic_string<char, std::char_traits<char>, StdAllocatorWrapperStateless<char, Allocator>>;

template <typename Allocator>
using StringWithAllocator =
    std::basic_string<char, std::char_traits<char>, StdAllocatorWrapperStateless<char, Allocator>>;

template <typename KEY,
          typename VALUE,
          typename HASH = modus::hash<KEY>,
          typename KEY_EQUAL = std::equal_to<KEY>,
          typename Allocator =
              StdAllocatorWrapperStateless<std::pair<const KEY, VALUE>, DefaultAllocator>>
using HashMap = std::unordered_map<KEY, VALUE, HASH, KEY_EQUAL, Allocator>;

template <typename KEY,
          typename VALUE,
          typename Allocator,
          typename HASH = modus::hash<KEY>,
          typename KEY_EQUAL = std::equal_to<KEY>>
using HashMapWithAllocator =
    std::unordered_map<KEY,
                       VALUE,
                       HASH,
                       KEY_EQUAL,
                       StdAllocatorWrapperStateless<std::pair<const KEY, VALUE>, Allocator>>;

template <typename KEY,
          typename HASH = modus::hash<KEY>,
          typename KEY_EQUAL = std::equal_to<KEY>,
          typename Allocator = StdAllocatorWrapperStateless<KEY, DefaultAllocator>>
using HashSet = std::unordered_set<KEY, HASH, KEY_EQUAL, Allocator>;

template <typename KEY,
          typename Allocator,
          typename HASH = modus::hash<KEY>,
          typename KEY_EQUAL = std::equal_to<KEY>>
using HashSetWithAllocator =
    std::unordered_set<KEY, HASH, KEY_EQUAL, StdAllocatorWrapperStateless<KEY, Allocator>>;

template <typename T>
using Optional = std::optional<T>;

template <typename T, usize SIZE>
using Array = std::array<T, SIZE>;

template <typename T>
using NotMyPtr = NonOwningPtr<T>;

template <usize Size>
using BitSet = std::bitset<Size>;

template <typename T, typename U>
using Pair = std::pair<T, U>;

template <typename T, typename... Args>
inline std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(MODUS_NEW T(std::forward<Args>(args)...));
}

}    // namespace modus

namespace modus {
namespace format_impl {
using Allocator = StdAllocatorWrapperStateless<char, DefaultAllocatorString>;

inline String vformat(fmt::string_view format_str, fmt::format_args args) {
    Allocator alloc;
    using custom_memory_buffer = fmt::basic_memory_buffer<char, fmt::inline_buffer_size, Allocator>;
    custom_memory_buffer buf(alloc);
    fmt::vformat_to(buf, format_str, args);
    return String(buf.data(), buf.size(), alloc);
}

}    // namespace format_impl

template <typename... Args>
inline String format(fmt::string_view format_str, const Args&... args) {
    return format_impl::vformat(format_str, fmt::make_format_args(args...));
}
}    // namespace modus
