/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

#include <core/containers.hpp>

namespace modus {

// Get the index of Type T in the tuple
template <class T, class Tuple>
struct TupleTypeIndex;

template <class T, class... Types>
struct TupleTypeIndex<T, std::tuple<T, Types...>> {
    static constexpr const std::size_t value = 0;
};

template <class T, class U, class... Types>
struct TupleTypeIndex<T, std::tuple<U, Types...>> {
    static constexpr const std::size_t value = 1 + TupleTypeIndex<T, std::tuple<Types...>>::value;
};

// Get the number of variadic arguments
template <typename T>
struct VariadicArgCountValue {
    static constexpr usize v = 1;
};
template <typename... R>
struct VariadicArgCount {
    constexpr static usize count = (0 + ... + VariadicArgCountValue<R>::v);
};

/// A BitMask which will set/unset a bit based on the location of type T in
/// a tuple of types.
template <typename... types>
class MODUS_CORE_EXPORT TupleTypeMask {
   private:
    using TupleType = std::tuple<types...>;
    BitSet<VariadicArgCount<types...>::count> m_bitset;

   public:
    using this_type = TupleTypeMask<types...>;
    TupleTypeMask() = default;

    template <typename T>
    inline void mark() {
        constexpr usize index = TupleTypeIndex<T, TupleType>::value;
        m_bitset.set(index, true);
    }

    template <typename T>
    inline void unmark() {
        constexpr usize index = TupleTypeIndex<T, TupleType>::value;
        m_bitset.set(index, false);
    }

    template <typename T>
    inline bool is_set() const {
        constexpr usize index = TupleTypeIndex<T, TupleType>::value;
        return m_bitset.test(index);
    }

    inline bool is_set(const usize index) const {
        modus_assert(index < m_bitset.size());
        return m_bitset.test(index);
    }

    inline void clear() { m_bitset.reset(); }

    inline bool operator==(const this_type& rhs) const { return m_bitset == rhs.m_bitset; }

    inline bool operator!=(const this_type& rhs) const { return m_bitset != rhs.m_bitset; }

    inline bool matches(const this_type& rsh) const {
        return (m_bitset & rsh.m_bitset) == rsh.m_bitset;
    }

    // Create a new mask for a list of types
    template <typename... Args>
    static this_type make_mask() {
        this_type mask;
        (mask.mark<Args>(), ...);
        return mask;
    }
};

}    // namespace modus
