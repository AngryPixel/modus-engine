/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

namespace modus {

MODUS_CORE_EXPORT u64 hash_data64(const void*, const usize size);
MODUS_CORE_EXPORT u32 hash_data32(const void*, const usize size);

class MODUS_CORE_EXPORT Hasher64 {
   private:
    void* m_state;

   public:
    Hasher64(const u64 seed = 0);
    ~Hasher64();
    MODUS_CLASS_DISABLE_COPY_MOVE(Hasher64);

    void reset(const u64 seed = 0);

    void update(const void* data, const usize size);

    u64 digest();
};

class MODUS_CORE_EXPORT Hasher32 {
   private:
    void* m_state;

   public:
    Hasher32(const u32 seed = 0);
    ~Hasher32();
    MODUS_CLASS_DISABLE_COPY_MOVE(Hasher32);

    void reset(const u32 seed = 0);

    void update(const void* data, const usize size);

    u32 digest();
};

#if defined(MODUS_64BIT)
using hash_type = u64;
#define hash_data hash_data64
#else
using hash_type = u32;
#define hash_data hash_data32
#endif

template <typename T>
struct hash {
    std::size_t operator()(const T& type) const;
};

#define MODUS_HASH_IMPL_BUILTIN(x)                                                          \
    template <>                                                                             \
    struct hash<x> {                                                                        \
        std::size_t operator()(const x& type) const { return hash_data(&type, sizeof(x)); } \
    }

MODUS_HASH_IMPL_BUILTIN(i8);
MODUS_HASH_IMPL_BUILTIN(u8);
MODUS_HASH_IMPL_BUILTIN(i16);
MODUS_HASH_IMPL_BUILTIN(u16);
MODUS_HASH_IMPL_BUILTIN(i32);
MODUS_HASH_IMPL_BUILTIN(u32);
MODUS_HASH_IMPL_BUILTIN(u64);
MODUS_HASH_IMPL_BUILTIN(f32);
MODUS_HASH_IMPL_BUILTIN(f64);
#undef MODUS_HASH_IMPL_BUILTIN

}    // namespace modus
