/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core.hpp>

#include <core/containers.hpp>
#include <core/slice.hpp>

namespace modus {

template <typename T>
struct DefaultDisjointSetCompare {
    constexpr bool operator()(const T& k1, const T& k2) const { return k1 == k2; }
};

enum class [[nodiscard]] DisjointSetError{Ok, Duplicate, DependencyNotFound};

/**
 * Disjoint Set with union find and path compression
 */
template <typename T, typename Key, typename KeyCompare = DefaultDisjointSetCompare<Key>>
class DisjointSet {
   public:
    using ptr_type = NotMyPtr<T>;
    using this_type = DisjointSet<T, Key, KeyCompare>;

   private:
    struct Node {
        ptr_type m_type;
        Key m_key;
        Vector<u32> m_edges;
    };

    struct SubSet {
        u32 parent;
        u32 rank;
        u32 list_index;
    };

    Vector<Node> m_nodes;
    Vector<SubSet> m_subsets;
    Vector<Vector<ptr_type>> m_sets;
    u32 m_subset_count;

   public:
    DisjointSetError add(ptr_type value, const Key& key, Slice<Key> dependencies) {
        if (find_key(key)) {
            return DisjointSetError::Duplicate;
        }

        Node node;
        node.m_key = key;
        node.m_type = value;
        node.m_edges.reserve(dependencies.size());
        for (auto& dep : dependencies) {
            auto r_find = find_key(dep);
            if (!r_find) {
                return DisjointSetError::DependencyNotFound;
            }
            node.m_edges.push_back(*r_find);
        }
        m_nodes.push_back(std::move(node));
        return DisjointSetError::Ok;
    }

    void reserve(const size_t size) { m_nodes.reserve(size); }

    void evaluate() {
        m_subsets.clear();
        m_subsets.resize(m_nodes.size());
        m_subset_count = m_nodes.size();
        m_sets.clear();

        for (u32 i = 0; i < m_subsets.size(); i++) {
            m_subsets[i].parent = i;
            m_subsets[i].rank = 0;
            m_subsets[i].list_index = m_nodes.size();
        }

        // do evaluation
        for (u32 i = 0; i < m_nodes.size(); ++i) {
            const Node& node = m_nodes[i];
            for (u32 j = 0; j < node.m_edges.size(); j++) {
                const u32 x = find(i);
                const u32 y = find(node.m_edges[j]);
                do_union(x, y);
            }
        }

        // evaluate list index
        u32 list_counter = 0;
        for (u32 i = 0; i < m_subsets.size(); i++) {
            SubSet& subset = m_subsets[i];
            if (subset.parent == i && subset.list_index == m_nodes.size()) {
                subset.list_index = list_counter++;
                modus_assert(list_counter <= m_subset_count);
            }
        }
    }

    void build_sets() {
        if (!m_sets.empty()) {
            return;
        }

        // build sets
        const u32 reserve_size = m_nodes.size() / m_subset_count;
        m_sets.resize(m_subset_count);
        for (auto& subset : m_sets) {
            subset.reserve(reserve_size);
        }

        for (u32 i = 0; i < m_subsets.size(); i++) {
            const SubSet& subset = m_subsets[i];
            const SubSet& parent = m_subsets[subset.parent];
            modus_assert(parent.list_index < m_sets.size());
            m_sets[parent.list_index].push_back(m_nodes[i].m_type);
        }
    }

    bool are_in_set(const Key& k1, const Key& k2) const {
        auto r_find1 = find_key(k1);
        auto r_find2 = find_key(k2);
        if (!r_find1 || !r_find2) {
            return false;
        }
        return m_subsets[*r_find1].parent == m_subsets[*r_find2].parent;
    }

    u32 subset_count() const { return m_subset_count; }

    Slice<Vector<ptr_type>> subsets() const { return make_slice(m_sets); }

   private:
    Result<u32, void> find_key(const Key& key) const {
        for (u32 i = 0; i < m_nodes.size(); i++) {
            const Node& node = m_nodes[i];
            if (KeyCompare()(node.m_key, key)) {
                return Ok(i);
            }
        }
        return Error<>();
    }

    u32 find(const u32 i) {
        if (m_subsets[i].parent != i) {
            m_subsets[i].parent = find(m_subsets[i].parent);
        }
        return m_subsets[i].parent;
    }

    void do_union(const u32 x, const u32 y) {
        const u32 xroot = find(x);
        const u32 yroot = find(y);

        if (xroot != yroot) {
            modus_assert(m_subset_count > 0);
            m_subset_count--;
        }

        // Attach smaller rank tree under root of high rank tree
        // (Union by Rank)
        if (m_subsets[xroot].rank < m_subsets[yroot].rank) {
            m_subsets[xroot].parent = yroot;
        } else if (m_subsets[xroot].rank > m_subsets[yroot].rank) {
            m_subsets[yroot].parent = xroot;
        }
        // If ranks are same, then make one as root and increment
        // its rank by one
        else {
            m_subsets[yroot].parent = xroot;
            m_subsets[xroot].rank++;
        }
    }
};

}    // namespace modus
