/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/string_slice.hpp>

namespace modus {

template <typename T>
class MODUS_CORE_EXPORT EnumString {
   private:
    T m_enum;
    String m_str;
    static_assert(std::is_enum_v<T>, "T must be an enum");

   public:
    using this_type = EnumString<T>;
    EnumString() = delete;
    ~EnumString() = default;
    inline EnumString(const T e) : m_enum(e), m_str() {}
    inline EnumString(const T e, const StringSlice slice)
        : m_enum(e), m_str(slice.data(), slice.size()) {}
    EnumString(const this_type&) = default;
    EnumString(this_type&&) = default;
    EnumString& operator=(const this_type&) = default;
    EnumString& operator=(this_type&&) = default;

    inline constexpr T value() const { return m_enum; }

    inline StringSlice str() const { return StringSlice(m_str.data(), m_str.size()); }

    inline bool has_str() const { return !m_str.empty(); }
};
}    // namespace modus

namespace fmt {
template <typename T>
struct formatter<modus::EnumString<T>> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::EnumString<T>& enum_str, FormatContext& ctx) {
        if (enum_str.has_str()) {
            return format_to(ctx.out(), "{}: {}", enum_str.value(), enum_str.str());
        } else {
            return format_to(ctx.out(), "{}", enum_str.value());
        }
    }
};
}    // namespace fmt
