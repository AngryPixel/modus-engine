/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/pointer_iterator.hpp>

namespace modus {

/// A basic stack allocated non-growable Vector with a fixed size
template <typename T, usize Size>
class FixedVector {
   private:
    struct AlignedStorage {
        alignas(T) u8 data[sizeof(T)];
    };
    AlignedStorage m_data[Size];
    usize m_size = 0;

   public:
    using value_type = T;
    using this_type = FixedVector<T, Size>;

    FixedVector() = default;

    ~FixedVector() { clear(); }

    inline FixedVector(const this_type& rhs) : m_size(rhs.m_size) {
        for (usize i = 0; i < m_size; ++i) {
            new (&(*this)[i]) T(rhs[i]);
        }
    }

    inline this_type& operator=(const this_type& rhs) {
        clear();
        new (this) this_type(rhs);
        return *this;
    }

    inline FixedVector(this_type&& rhs) noexcept : m_size(rhs.m_size) {
        for (usize i = 0; i < m_size; ++i) {
            new (&(*this)[i]) T(std::move(rhs[i]));
        }
        rhs.clear();
    }

    inline this_type& operator=(this_type&& rhs) noexcept {
        clear();
        new (this) this_type(std::forward<this_type>(rhs));
        return *this;
    }

    inline value_type& operator[](const usize index) {
        modus_assert(index < m_size);
        return (reinterpret_cast<T*>(&m_data[0]))[index];
    }

    inline const value_type& operator[](const usize index) const {
        modus_assert(index < m_size);
        return (reinterpret_cast<const T*>(&m_data[0]))[index];
    }

    inline value_type* data() { return (reinterpret_cast<T*>(&m_data[0])); }

    inline const value_type* data() const { return (reinterpret_cast<const T*>(&m_data[0])); }

    inline value_type* data_end() { return &(reinterpret_cast<T*>(&m_data[0]))[m_size]; }

    inline const value_type* data_end() const {
        return &(reinterpret_cast<const T*>(&m_data[0]))[m_size];
    }

    Optional<value_type*> back() {
        if (m_size == 0) {
            return Optional<value_type*>();
        }
        return &(*this)[m_size - 1];
    }

    Optional<const value_type*> back() const {
        if (m_size == 0) {
            return Optional<const value_type*>();
        }
        return &(*this)[m_size - 1];
    }

    inline usize size() const { return m_size; }

    constexpr usize capacity() const { return Size; }

    void resize(const usize new_size) {
        if (new_size < m_size) {
            while (m_size > new_size) {
                erase_back();
            }
        } else {
            const usize new_max_size = std::min(Size, new_size);
            const usize cur_size = m_size;
            m_size = new_max_size;
            for (usize i = cur_size; i < new_max_size; ++i) {
                new (&(*this)[i]) T();
            }
        }
    }

    inline bool is_empty() const { return m_size == 0; }

    inline void clear() {
        if constexpr (!std::is_pod_v<T>) {
            for (usize i = 0; i < m_size; ++i) {
                (*this)[i].~T();
            }
        }
        m_size = 0;
    }

    Result<> push_back(const value_type& v) {
        if (m_size == Size) {
            return Error<>();
        }
        m_size++;
        new (&(*this)[m_size - 1]) T(v);
        return Ok<>();
    }

    Result<> push_back(value_type&& v) {
        if (m_size == Size) {
            return Error<>();
        }
        m_size++;
        new (&(*this)[m_size - 1]) T(std::move(v));
        return Ok<>();
    }

    inline void erase_back() {
        if (m_size == 0) {
            return;
        }
        if constexpr (!std::is_pod_v<T>) {
            (*this)[m_size - 1].~T();
        }
        m_size--;
    }

    inline Optional<T> pop_back() {
        if (is_empty()) {
            return Optional<T>();
        }
        Optional<T> r = Optional<T>(std::move((*this)[m_size - 1]));
        erase_back();
        return r;
    }

    // Iterator
    using const_iterator = PointerIterator<const value_type>;
    using iterator = PointerIterator<value_type>;

    inline const_iterator begin() const { return const_iterator(data(), data(), data_end()); }
    inline const_iterator end() const { return const_iterator(data_end(), data(), data_end()); }
    inline iterator begin() { return iterator(data(), data(), data_end()); }
    inline iterator end() { return iterator(data_end(), data(), data_end()); }
};

}    // namespace modus
