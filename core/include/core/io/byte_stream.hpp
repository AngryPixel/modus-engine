/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/slice.hpp>

namespace modus {

enum class IOError {
    ReadOnly,
    WriteOnly,
    SeekFailed,
    IO,
    OutOfMemory,
    BadHandle,
    Interrupted,
    WriteExactly,
    ReadExactly,
    Unknown
};

template <typename T>
using IOResult = Result<T, IOError>;

class MODUS_CORE_EXPORT IByteReader {
   public:
    virtual ~IByteReader() {}

    template <typename U>
    inline IOResult<usize> read(SliceMut<U> slice) {
        SliceMut<u8> byte_slice = slice.as_bytes();
        return read(byte_slice);
    }

    virtual IOResult<usize> read(SliceMut<u8> slice) = 0;

    inline IOResult<usize> read_exactly(SliceMut<u8> slice) {
        auto r = read(slice);
        if (r && *r == slice.size()) {
            return r;
        }
        return Error(IOError::ReadExactly);
    }
};

class MODUS_CORE_EXPORT IByteWriter {
   public:
    virtual ~IByteWriter() {}

    template <typename U>
    inline IOResult<usize> write(const Slice<U> slice) {
        const Slice<u8> byte_slice = slice.as_bytes();
        return write(byte_slice);
    }

    virtual IOResult<usize> write(const Slice<u8> slice) = 0;

    inline IOResult<usize> write_exactly(const Slice<u8> slice) {
        auto r = write(slice);
        if (r && *r == slice.size()) {
            return r;
        }
        return Error(IOError::WriteExactly);
    }

    virtual void flush() {}
};

class MODUS_CORE_EXPORT ISeekableReader : public IByteReader {
   public:
    virtual ~ISeekableReader() {}

    virtual IOResult<usize> seek(const usize position) = 0;

    virtual IOResult<usize> seek_end() = 0;

    virtual IOResult<usize> position() const = 0;

    virtual usize size() const = 0;
};

class MODUS_CORE_EXPORT ISeekableWriter : public ISeekableReader, public IByteWriter {
   public:
    virtual ~ISeekableWriter() {}
};

}    // namespace modus
