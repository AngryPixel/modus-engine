/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/io/byte_stream.hpp>
#include <core/string_slice.hpp>

namespace modus {

class MODUS_CORE_EXPORT LineReader {
   private:
    static constexpr usize kBufferSize = 1024;
    IByteReader& m_reader;
    usize m_offset;
    usize m_bytes_read;
    String m_line;
    char m_buffer[kBufferSize];

   public:
    LineReader(IByteReader& reader);

    MODUS_CLASS_DISABLE_COPY_MOVE(LineReader);

    /// Will return the read line, minus the delimiter on success.
    /// When the stream can no longer produce input it will return an empty Optional.
    Result<Optional<StringSlice>, IOError> read_line(const StringSlice delimiter = "\n");
};

}    // namespace modus
