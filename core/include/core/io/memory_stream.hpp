/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/io/byte_stream.hpp>
#include <core/string_slice.hpp>

namespace modus {

/// Byte stream into a fixed size memory region defined by a slice
class MODUS_CORE_EXPORT SliceStream : public ISeekableWriter {
   private:
    enum class Mode { Read, Write };

    union {
        Slice<u8> m_readable;
        SliceMut<u8> m_writable;
    };
    usize m_offset;
    Mode m_mode;

   public:
    SliceStream() = delete;
    SliceStream(const SliceMut<u8> slice);
    SliceStream(const Slice<u8> slice);
    SliceStream(const StringSlice slice);
    SliceStream(const SliceStream&) = delete;
    SliceStream(SliceStream&&) = delete;
    SliceStream& operator=(const SliceStream&) = delete;
    SliceStream& operator=(SliceStream&&) = delete;

    template <typename U>
    SliceStream(const Slice<U>& slice) : SliceStream(slice.as_bytes()) {}

    template <typename U>
    SliceStream(const SliceMut<U>& slice) : SliceStream(slice.as_bytes()) {}

    virtual ~SliceStream() override = default;

    IOResult<usize> read(SliceMut<u8> slice) override final;

    IOResult<usize> write(const Slice<u8> slice) override final;

    IOResult<usize> seek(const usize position) override final;

    IOResult<usize> seek_end() override final;

    IOResult<usize> position() const override final;

    usize size() const override final;
};

/// Byte stream into a growing memory region.
template <typename Allocator = DefaultAllocator>
class MODUS_CORE_EXPORT RamStream final : public ISeekableWriter {
   private:
    u8* m_ptr;
    usize m_offset;
    usize m_size;
    usize m_capacity;
    Allocator m_alloc;

   public:
    RamStream(const Allocator& alloc = Allocator())
        : m_ptr(nullptr), m_offset(0), m_size(0), m_capacity(0), m_alloc(alloc) {}

    RamStream(RamStream&& rhs) noexcept : RamStream() {
        m_ptr = rhs.m_ptr;
        m_offset = rhs.m_offset;
        m_size = rhs.m_size;
        m_capacity = rhs.m_capacity;
        rhs.m_ptr = nullptr;
        rhs.m_offset = 0;
        rhs.m_size = 0;
        rhs.m_capacity = 0;
    }
    RamStream& operator=(const RamStream&) = delete;
    RamStream& operator=(RamStream&& rhs) noexcept {
        if (this != &rhs) {
            reset();
            new (this) RamStream(std::forward<RamStream>(rhs));
        }
        return *this;
    }
    virtual ~RamStream() override {
        if (m_ptr != nullptr) {
            m_alloc.deallocate(m_ptr);
        }
    };

    IOResult<usize> populate_from_stream(IByteReader& reader) {
        clear();
        static constexpr usize k_read_buffer_size = 512;
        u8 buffer[k_read_buffer_size];
        usize bytes_read = 0;
        while (true) {
            auto result = reader.read(buffer);
            if (!result) {
                return result;
            }
            if (result.value() == 0) {
                break;
            }
            bytes_read += result.value();
            auto this_write_result = this->write(Slice<u8>(buffer, result.value()));
            if (!this_write_result) {
                return this_write_result;
            }
        }
        return Ok(bytes_read);
    }

    IOResult<usize> populate_from_stream(ISeekableReader& reader) {
        auto reserve_result = reserve(reader.size());
        if (!reserve_result) {
            return Error(reserve_result.error());
        }
        return populate_from_stream(static_cast<IByteReader&>(reader));
    }

    IOResult<void> reserve(const usize size) {
        if (size > m_capacity) {
            const usize new_size = usize(size * 1.5);
            u8* new_ptr = reinterpret_cast<u8*>(m_alloc.realloc(m_ptr, new_size));
            if (new_ptr == nullptr) {
                return Error(IOError::OutOfMemory);
            }
            m_ptr = new_ptr;
            m_capacity = new_size;
        }
        return Ok<>();
    }

    IOResult<usize> read(SliceMut<u8> slice) override {
        usize bytes_to_read = slice.size();
        if (m_offset + bytes_to_read > m_size) {
            bytes_to_read = m_size - m_offset;
        }

        if (bytes_to_read > 0) {
            u8* dst = slice.data();
            const u8* src = m_ptr + m_offset;
            memcpy(dst, src, bytes_to_read);
            m_offset += bytes_to_read;
        }

        return Ok(bytes_to_read);
    }

    IOResult<usize> write(const Slice<u8> slice) override {
        usize bytes_to_write = slice.size();
        IOResult<void> result = reserve(m_offset + bytes_to_write);
        if (result.is_error()) {
            return Error(result.error());
        }

        if (bytes_to_write > 0) {
            memcpy(m_ptr + m_offset, slice.data(), bytes_to_write);
            m_offset += bytes_to_write;
            m_size = m_offset;
        }
        return Ok(bytes_to_write);
    }

    IOResult<usize> seek(const usize position) override {
        if (position > m_size) {
            return Error(IOError::SeekFailed);
        }
        m_offset = position;
        return Ok(m_offset);
    }

    IOResult<usize> seek_end() override {
        m_offset = m_size;
        return Ok(m_offset);
    }

    void clear() { m_offset = 0; }

    void reset() {
        if (m_ptr != nullptr) {
            m_alloc.deallocate(m_ptr);
            m_ptr = nullptr;
            m_offset = 0;
            m_size = 0;
        }
    }

    IOResult<usize> position() const override { return Ok(m_offset); }

    usize size() const override { return m_size; }

    Slice<u8> as_bytes() const { return Slice<u8>(m_ptr, m_size); }

    SliceMut<u8> as_bytes() { return SliceMut<u8>(m_ptr, m_size); }

    StringSlice as_string_slice() const {
        return StringSlice(reinterpret_cast<const char*>(m_ptr), m_size);
    }
};
}    // namespace modus
