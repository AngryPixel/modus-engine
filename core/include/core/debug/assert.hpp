/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/builtin_types.hpp>
#include <core/module_config.hpp>

namespace modus {

[[noreturn]] MODUS_CORE_EXPORT void assert_impl(const char* expression,
                                                const char* filename,
                                                const u32 line);

[[noreturn]] MODUS_CORE_EXPORT void assert_impl(const char* expression,
                                                const char* message,
                                                const char* filename,
                                                const u32 line);

[[noreturn]] MODUS_CORE_EXPORT void panic_impl(const char* reason,
                                               const char* filename,
                                               const u32 line);

}    // namespace modus

#if defined(MODUS_DEBUG)
#define MODUS_ENABLE_ASSERTS
#endif

// Setup asserts
#if defined(MODUS_ENABLE_ASSERTS)
#define modus_assert(x)                             \
    if (!(x)) {                                     \
        modus::assert_impl(#x, __FILE__, __LINE__); \
    }
#define modus_assert_message(x, m)                     \
    if (!(x)) {                                        \
        modus::assert_impl(#x, m, __FILE__, __LINE__); \
    }
#else
#define modus_assert(x)
#define modus_assert_message(x, m)
#endif    // defined(NDEBUG)

#define modus_panic(x) modus::panic_impl(x, __FILE__, __LINE__)
