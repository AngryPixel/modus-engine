/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <core/core.hpp>

namespace modus {

template <typename T, typename Allocator = DefaultAllocator>
class MODUS_CORE_EXPORT RefData {
   private:
    volatile u32 m_strong_count;
    volatile u32 m_weak_count;
    T* m_ptr;

   public:
    using this_type = RefData<T, Allocator>;
    using allocator_type = Allocator;

    template <typename... Args>
    RefData(Args&&... args) : m_strong_count(0), m_weak_count(0) {
        m_ptr = new (this + 1) T(std::forward<Args>(args)...);
    }

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(RefData, this_type);

    ~RefData() {
        modus_assert(m_weak_count == 0);
        modus_assert(m_strong_count == 0);
        Allocator::deallocate(this);
    }

    inline void strong_increment() { m_strong_count++; }

    inline void strong_decrement() {
        modus_assert(m_strong_count > 0);
        m_strong_count--;
        if (m_strong_count == 0) {
            ptr()->~T();
        }
        if (m_strong_count == 0 && m_weak_count == 0) {
            this->~RefData();
        }
    }

    inline void weak_increment() { m_weak_count++; }

    inline void weak_decrement() {
        modus_assert(m_weak_count > 0);
        m_weak_count--;
        if (m_weak_count == 0 && m_strong_count == 0) {
            this->~RefData();
        }
    }

    inline bool is_valid() const { return m_strong_count > 0; }

    inline T* ptr() { return m_ptr; }

    inline const T* ptr() const { m_ptr; }

    inline u32 weak_count() const { return m_weak_count; }

    inline u32 strong_count() const { return m_strong_count; }

    template <typename U>
    static this_type* cast(RefData<U, Allocator>* rhs) {
        if (rhs == nullptr) {
            return nullptr;
        }
        [[maybe_unused]] T* cast = static_cast<T*>(rhs->ptr());
        return reinterpret_cast<this_type*>(rhs);
    }

    template <typename U>
    static this_type* cast(const RefData<U, Allocator>* rhs) {
        if (rhs == nullptr) {
            return nullptr;
        }
        [[maybe_unused]] T* cast = static_cast<T*>(rhs->ptr());
        return const_cast<this_type*>(reinterpret_cast<const this_type*>(rhs));
    }

    static this_type* from_pointer(T* ptr) { return reinterpret_cast<this_type*>(ptr) - 1; }
};

/// Non-thread safe reference counted ptr type. Unlike std::shared, this smart pointer owns the
/// memory and can't be rebound to arbitrary pointers, only those created through make_refptr<>().
template <bool IsWeak, typename T, typename Allocator = DefaultAllocator>
class MODUS_CORE_EXPORT RefPtrBase {
    using RefDataType = RefData<T, Allocator>;

    template <bool B, typename U, typename A>
    friend class RefPtrBase;

   private:
    mutable RefDataType* m_ref_data;

   public:
    using this_type = RefPtrBase<IsWeak, T, Allocator>;
    using opposite_type = RefPtrBase<!IsWeak, T, Allocator>;
    friend class RefPtrBase<!IsWeak, T, Allocator>;

    inline RefPtrBase() : m_ref_data(nullptr) {}

    inline RefPtrBase(std::nullptr_t) : m_ref_data(nullptr) {}

    inline ~RefPtrBase() {
        if (m_ref_data != nullptr) {
            decrement();
        }
    }

    inline RefPtrBase(RefDataType* ref_data) : m_ref_data(ref_data) {
        if (m_ref_data != nullptr) {
            increment();
        }
    }

    inline RefPtrBase(const this_type& rhs) : m_ref_data(rhs.m_ref_data) {
        if (m_ref_data != nullptr) {
            increment();
        }
    }

    inline RefPtrBase(this_type&& rhs) noexcept : m_ref_data(rhs.m_ref_data) {
        rhs.m_ref_data = nullptr;
    }

    inline RefPtrBase& operator=(const this_type& rhs) {
        if (this != &rhs) {
            if (m_ref_data != nullptr) {
                decrement();
            }
            m_ref_data = rhs.m_ref_data;
            if (m_ref_data != nullptr) {
                increment();
            }
        }
        return *this;
    }

    inline RefPtrBase& operator=(this_type&& rhs) noexcept {
        if (this != &rhs) {
            if (m_ref_data != nullptr) {
                decrement();
            }
            m_ref_data = rhs.m_ref_data;
            rhs.m_ref_data = nullptr;
        }
        return *this;
    }

    inline RefPtrBase(const opposite_type& rhs) : m_ref_data(rhs.m_ref_data) {
        if (m_ref_data != nullptr) {
            if constexpr (IsWeak) {
                // opposite == strong
                m_ref_data->weak_increment();
            } else {
                // opposite == weak
                if (m_ref_data->strong_count() != 0) {
                    m_ref_data->strong_increment();
                } else {
                    m_ref_data = nullptr;
                }
            }
        }
    }

    inline RefPtrBase& operator=(const opposite_type& rhs) {
        if (m_ref_data != nullptr) {
            decrement();
        }

        m_ref_data = rhs.m_ref_data;
        if (m_ref_data != nullptr) {
            if constexpr (IsWeak) {
                // opposite == strong
                m_ref_data->weak_increment();
            } else {
                // opposite == weak
                if (m_ref_data->strong_count() != 0) {
                    m_ref_data->strong_increment();
                } else {
                    m_ref_data = nullptr;
                }
            }
        }
        return *this;
    }

    template <typename U>
    inline RefPtrBase(const RefPtrBase<IsWeak, U, Allocator>& rhs)
        : m_ref_data(RefDataType::cast(rhs.m_ref_data)) {
        if (m_ref_data != nullptr) {
            increment();
        }
    }

    template <typename U>
    inline RefPtrBase& operator=(const RefPtrBase<IsWeak, U, Allocator>& rhs) {
        if (m_ref_data != nullptr) {
            decrement();
        }
        m_ref_data = RefDataType::cast(rhs.m_ref_data);
        if (m_ref_data != nullptr) {
            increment();
        }
    }

    inline explicit operator bool() const { return m_ref_data != nullptr; }

    template <bool B = IsWeak, typename std::enable_if_t<!B, int> = 0>
    inline T& operator*() {
        modus_assert(m_ref_data != nullptr);
        return *m_ref_data->ptr();
    }

    template <bool B = IsWeak, typename std::enable_if_t<!B, int> = 0>
    inline const T& operator*() const {
        modus_assert(m_ref_data != nullptr);
        return *m_ref_data->ptr();
    }

    template <bool B = IsWeak, typename std::enable_if_t<!B, int> = 0>
    inline T* operator->() {
        modus_assert(m_ref_data != nullptr);
        return m_ref_data->ptr();
    }

    template <bool B = IsWeak, typename std::enable_if_t<!B, int> = 0>
    inline const T* operator->() const {
        modus_assert(m_ref_data != nullptr);
        return m_ref_data->ptr();
    }

    template <bool B = IsWeak, typename std::enable_if_t<!B, int> = 0>
    inline T* get() const {
        modus_assert(m_ref_data != nullptr);
        return m_ref_data->ptr();
    }

    inline void reset() {
        if (m_ref_data != nullptr) {
            decrement();
            m_ref_data = nullptr;
        }
    }

    inline bool operator==(const this_type& rhs) const { return m_ref_data == rhs.m_ref_data; }

    inline bool operator!=(const this_type& rhs) const { return m_ref_data == rhs.m_ref_data; }

    inline bool operator==(const T* rhs) const {
        return (m_ref_data != nullptr) ? m_ref_data->ptr() == rhs : false;
    }

    inline bool operator!=(const T* rhs) const {
        return (m_ref_data != nullptr) ? m_ref_data->ptr() != rhs : true;
    }

    inline bool operator==(const std::nullptr_t) const { return m_ref_data == nullptr; }

    inline bool operator!=(const std::nullptr_t) const { return m_ref_data != nullptr; }

    inline u32 ref_count() const {
        if constexpr (IsWeak) {
            return (m_ref_data != nullptr) ? m_ref_data->weak_count() : 0;
        } else {
            return (m_ref_data != nullptr) ? m_ref_data->strong_count() : 0;
        }
    }

    static this_type from_pointer(T* ptr) { return this_type(RefDataType::from_pointer(ptr)); }

   private:
    inline void increment() {
        if constexpr (IsWeak) {
            m_ref_data->weak_increment();
        } else {
            m_ref_data->strong_increment();
        }
    }

    inline void decrement() {
        if constexpr (IsWeak) {
            m_ref_data->weak_decrement();
        } else {
            m_ref_data->strong_decrement();
        }
    }
};

/// Non-thread safe reference counted ptr type. Unlike std::shared, this smart pointer owns the
/// memory and can't be rebound to arbitrary pointers, only those created through make_refptr<>().
/// RefPtr is a strong reference
template <typename T, typename Allocator = DefaultAllocator>
using RefPtr = RefPtrBase<false, T, Allocator>;

/// Non-thread safe reference counted ptr type. Unlike std::shared, this smart pointer owns the
/// memory and can't be rebound to arbitrary pointers, only those created through make_refptr<>().
/// WeakRefPtr is a weak reference
template <typename T, typename Allocator = DefaultAllocator>
using WeakRefPtr = RefPtrBase<true, T, Allocator>;

template <typename T, typename... Args>
RefPtr<T> make_refptr(Args&&... args) {
    using RFD = RefData<T>;
    void* ptr = RFD::allocator_type::allocate(sizeof(RFD) + sizeof(T));
    RFD* ref_data = new (ptr) RFD(std::forward<Args>(args)...);
    return RefPtr<T>(ref_data);
}

template <typename T, typename Allocator, typename... Args>
RefPtr<T, Allocator> make_refptr_with_allocator(Args&&... args) {
    using RFD = RefData<T, Allocator>;
    void* ptr = Allocator::allocate(sizeof(RFD) + sizeof(T));
    RFD* ref_data = new (ptr) RFD(std::forward<Args>(args)...);
    return RefPtr<T>(ref_data);
}

template <typename T, typename U, typename Allocator = DefaultAllocator>
RefPtr<T, Allocator> refptr_cast(RefPtr<U, Allocator>& rhs) {
    return RefPtr<T, Allocator>(rhs);
}

}    // namespace modus
