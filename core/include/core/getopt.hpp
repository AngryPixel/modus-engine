/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

#include <core/containers.hpp>
#include <core/result.hpp>
#include <core/string_slice.hpp>

namespace modus {

enum class CmdOptionError {
    InvalidParameter,
    MissingParameterValue,
};

template <typename T>
using CmdOptionResult = Result<T, CmdOptionError>;

class MODUS_CORE_EXPORT CmdOption {
    friend class CmdOptionParser;

   private:
    const StringSlice m_id_short;
    const StringSlice m_id_long;
    const StringSlice m_help;
    bool m_found;

   protected:
    CmdOption(const StringSlice id_short, const StringSlice id_long, const StringSlice help);

   public:
    virtual ~CmdOption() = default;

    /// Parse option from option list
    /// \param argc Number of arguments left
    /// \param argv Argument list
    virtual CmdOptionResult<int> parse(const int argc, const char** argv) = 0;
    bool parsed() const { return m_found; }
    inline bool has_short_id() const { return m_id_short.size() != 0; }
};

class MODUS_CORE_EXPORT CmdOptionEmpty final : public CmdOption {
   public:
    CmdOptionEmpty(const StringSlice id_short, const StringSlice id_long, const StringSlice help);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;
};

class MODUS_CORE_EXPORT CmdOptionParser final {
   private:
    Vector<NotMyPtr<CmdOption>> m_options;
    usize m_max_option_len = 0;
    CmdOptionEmpty m_opt_help;

   public:
    CmdOptionParser();

    Result<> add(CmdOption& option);

    // Returns the location of the first argument that is not
    // an option
    Result<Slice<const char*>, String> parse(const int argc, const char** argv);

    bool help_requested() const { return m_opt_help.parsed(); }

    String help_string(const StringSlice& intro) const;
};

class MODUS_CORE_EXPORT CmdOptionBool final : public CmdOption {
   private:
    bool m_value;

   public:
    CmdOptionBool(const StringSlice id_short,
                  const StringSlice id_long,
                  const StringSlice help,
                  const bool default_value);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    bool value() const { return m_value; }
};

class MODUS_CORE_EXPORT CmdOptionString final : public CmdOption {
   private:
    String m_value;

   public:
    CmdOptionString(const StringSlice id_short,
                    const StringSlice id_long,
                    const StringSlice help,
                    const StringSlice& default_value);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    const StringSlice value() const { return m_value; }

    const String& value_str() const { return m_value; }

    const char* value_cstr() const { return m_value.c_str(); }
};

class MODUS_CORE_EXPORT CmdOptionStringList final : public CmdOption {
   public:
    using ListType = Vector<String>;

   private:
    ListType m_value;

   public:
    CmdOptionStringList(const StringSlice id_short,
                        const StringSlice id_long,
                        const StringSlice help);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    const ListType& value() const { return m_value; }

    ListType::const_iterator begin() const { return m_value.cbegin(); }

    ListType::const_iterator end() const { return m_value.cend(); }
};

class MODUS_CORE_EXPORT CmdOptionStringMap final : public CmdOption {
   public:
    using MapType = HashMap<String, String>;

   private:
    MapType m_value;

   public:
    CmdOptionStringMap(const StringSlice id_short,
                       const StringSlice id_long,
                       const StringSlice help);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    const MapType& value() const { return m_value; }

    MapType::const_iterator begin() const { return m_value.cbegin(); }

    MapType::const_iterator end() const { return m_value.cend(); }
};

class MODUS_CORE_EXPORT CmdOptionF32 final : public CmdOption {
   private:
    f32 m_value;

   public:
    CmdOptionF32(const StringSlice id_short,
                 const StringSlice id_long,
                 const StringSlice help,
                 const f32 default_value);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    f32 value() const { return m_value; }
};

class MODUS_CORE_EXPORT CmdOptionI32 final : public CmdOption {
   private:
    i32 m_value;

   public:
    CmdOptionI32(const StringSlice id_short,
                 const StringSlice id_long,
                 const StringSlice help,
                 const i32 default_value);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    i32 value() const { return m_value; }
};

class MODUS_CORE_EXPORT CmdOptionU32 final : public CmdOption {
   private:
    u32 m_value;

   public:
    CmdOptionU32(const StringSlice id_short,
                 const StringSlice id_long,
                 const StringSlice help,
                 const u32 default_value);

    CmdOptionResult<int> parse(const int argc, const char** argv) override;

    u32 value() const { return m_value; }
};
}    // namespace modus
