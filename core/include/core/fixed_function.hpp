/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

/// Fixed Function is like std::function, but it does not allocate memory and
/// is not copiable.
template <size_t DataSize, typename FunctionSignature>
class FixedFunction;

template <size_t DataSize, typename ReturnType, typename... Args>
class FixedFunction<DataSize, ReturnType(Args...)> {
   private:
    using FuncType = ReturnType (*)(void*, Args...);
    void (*m_fn_dtor)(void*);
    void (*m_fn_mov)(void*, void*);
    FuncType m_func;
    struct alignas(void*) {
        u8 data[DataSize];
    } m_instance;

   public:
    static_assert(DataSize >= sizeof(void*));

    FixedFunction() : m_fn_dtor(nullptr), m_fn_mov(nullptr), m_func(nullptr) {}

    ~FixedFunction() {
        if (m_fn_dtor != nullptr) {
            m_fn_dtor(&m_instance);
        }
    }

    FixedFunction(ReturnType (*fn)(Args...)) : FixedFunction() { capture(fn); }

    template <typename LAMBDA>
    FixedFunction(LAMBDA&& l) : FixedFunction() {
        capture(std::forward<LAMBDA>(l));
    }

    FixedFunction(const FixedFunction& rhs) = delete;
    FixedFunction& operator=(const FixedFunction& rhs) = delete;

    FixedFunction(FixedFunction&& rhs) noexcept
        : m_fn_dtor(rhs.m_fn_dtor), m_fn_mov(rhs.m_fn_mov), m_func(rhs.m_func) {
        if (m_fn_mov) {
            m_fn_mov(&m_instance, &rhs.m_instance);
        }
        rhs.m_fn_dtor = nullptr;
        rhs.m_fn_mov = nullptr;
        rhs.m_func = nullptr;
    }

    FixedFunction& operator=(FixedFunction&& rhs) noexcept {
        if (this != &rhs) {
            if (m_fn_dtor != nullptr) {
                m_fn_dtor(&m_instance);
            }
            m_fn_dtor = rhs.m_fn_dtor;
            m_fn_mov = rhs.m_fn_mov;
            m_func = rhs.m_func;
            if (m_fn_mov != nullptr) {
                m_fn_mov(&m_instance, &rhs.m_instance);
            }
            rhs.m_fn_dtor = nullptr;
            rhs.m_fn_mov = nullptr;
            rhs.m_func = nullptr;
        }
        return *this;
    }

    FixedFunction& operator=(ReturnType (*fn)(Args...)) {
        if (m_fn_dtor != nullptr) {
            m_fn_dtor(&m_instance);
        }
        capture(fn);
        return *this;
    }

    template <typename LAMBDA>
    FixedFunction& operator=(LAMBDA&& l) {
        if (m_fn_dtor != nullptr) {
            m_fn_dtor(&m_instance);
        }
        capture(std::forward<LAMBDA>(l));
        return *this;
    }

    ReturnType operator()(Args... args) {
        modus_assert(m_func != nullptr);
        if constexpr (std::is_same_v<ReturnType, void>) {
            (*m_func)(&m_instance, std::forward<Args>(args)...);
        } else {
            return (*m_func)(&m_instance, std::forward<Args>(args)...);
        }
    }

    explicit operator bool() const { return m_func != nullptr; }

    void reset() {
        if (m_fn_dtor != nullptr) {
            m_fn_dtor(&m_instance);
        }
        m_fn_dtor = nullptr;
        m_fn_mov = nullptr;
        m_func = nullptr;
    }

   private:
    template <typename LAMBDA>
    void capture(LAMBDA&& l) {
        static_assert(sizeof(LAMBDA) <= DataSize, "Can't find current Lambda in allocated storage");
        static_assert(std::is_nothrow_move_constructible<LAMBDA>::value ||
                      std::is_move_constructible<LAMBDA>::value);
        m_fn_dtor = [](void* ptr) { reinterpret_cast<LAMBDA*>(ptr)->~LAMBDA(); };
        m_fn_mov = [](void* ptr1, void* ptr2) {
            new (ptr1) LAMBDA(std::move(*reinterpret_cast<LAMBDA*>(ptr2)));
        };
        struct _ {
            static ReturnType wrapper(void* ptr, Args... a) {
                LAMBDA* l = reinterpret_cast<LAMBDA*>(ptr);
                l->operator()(std::forward<Args>(a)...);
            }
        };
        m_func = _::wrapper;
        new (&m_instance) LAMBDA(std::move(l));
    }

    void capture(ReturnType (*fn)(Args...)) {
        m_fn_dtor = nullptr;
        m_fn_mov = [](void* ptr1, void* ptr2) { memcpy(ptr1, ptr2, sizeof(void*)); };
        m_func = [](void* ptr, Args... args) {
            return (*reinterpret_cast<ReturnType (**)(Args...)>(ptr))(std::forward<Args>(args)...);
        };
        auto data_ptr = reinterpret_cast<ReturnType (**)(Args...)>(&m_instance);
        *data_ptr = fn;
    }
};

}    // namespace modus
