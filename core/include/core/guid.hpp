/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/hash.hpp>
#include <core/string_slice.hpp>

namespace modus {

// GUID. But the name conflicts with Win32's GUID define.
class MODUS_CORE_EXPORT GID {
   public:
    using ByteStorageType = Array<u8, 16>;

   private:
    ByteStorageType m_bytes;
    GID(const ByteSlice& slice);

   public:
    static Result<GID, void> from_string(const StringSlice& slice);
    static Result<GID, void> from_slice(const ByteSlice& slice);

    GID();

    explicit GID(const ByteStorageType& bytes);

    MODUS_CLASS_DEFAULT_COPY_MOVE(GID);

    ByteSlice as_bytes() const { return ByteSlice(m_bytes.data(), m_bytes.size()); }

    String to_str() const;

    bool operator==(const GID& rhs) const;
    bool operator!=(const GID& rhs) const;
};

template <>
struct hash<GID> {
    inline std::size_t operator()(const GID& type) const {
        const ByteSlice slice = type.as_bytes();
        return hash_data(slice.data(), slice.size());
    }
};

}    // namespace modus

namespace fmt {
template <>
struct formatter<modus::GID> : formatter<fmt::string_view> {
    template <typename FormatContext>
    auto format(const modus::GID& p, FormatContext& ctx) {
        const modus::String str = p.to_str();
        return formatter<fmt::string_view>::format(fmt::string_view(str.c_str()), ctx);
    }
};
}    // namespace fmt
