/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <core/handle_pool_shared.hpp>

#if defined(MODUS_DEBUG)
#define MODUS_FIXED_HANDLE_POOL_DEBUG
#endif

namespace modus {

/// Generational Handle Pool with a fixed number of handles that can be
/// allocated.
template <typename T, u32 Size, typename HandleType = Handle>
class MODUS_CORE_EXPORT FixedHandlePool {
    static_assert(sizeof(HandleType) == sizeof(Handle));
    static_assert(Size < handle_pool_shared::k_max_size);
    static_assert(std::is_move_constructible_v<T>);

   private:
    handle_pool_shared::HandlePrivate m_handles[Size];
    handle_pool_shared::Blob<T> m_data[Size];
    u32 m_count;
    u32 m_next_free;

   public:
    using this_type = FixedHandlePool<T, Size>;
    explicit FixedHandlePool() { initialize(); }

    FixedHandlePool(const this_type&) = delete;
    FixedHandlePool(this_type&&) = delete;
    FixedHandlePool& operator=(const this_type&) = delete;
    FixedHandlePool& operator=(this_type&&) = delete;

    ~FixedHandlePool() {
        using namespace handle_pool_shared;
        for (u32 i = 0; i < Size; ++i) {
            HandlePrivate& priv = m_handles[i];
            if (priv.active) {
                m_data[i].destroy();
            }
        }
    }

    Result<Pair<HandleType, NotMyPtr<T>>, void> create(T&& value) {
        using namespace handle_pool_shared;
        auto result = create_internal();
        if (result) {
            HandlePublic public_hdl = handle_to_handle_public(result.value());
            const u32 index = public_hdl.index;
            m_data[index].construct(std::forward<T>(value));
            return Ok(Pair<HandleType, NotMyPtr<T>>(result.value(), m_data[index].to_ptr()));
        }
        return Error<>();
    }

    Result<Pair<HandleType, NotMyPtr<T>>, void> create(const T& value) {
        using namespace handle_pool_shared;
        auto result = create_internal();
        if (result) {
            HandlePublic public_hdl = handle_to_handle_public(result.value());
            const u32 index = public_hdl.index;
            m_data[index].construct(value);
            return Ok(Pair<HandleType, NotMyPtr<T>>(result.value(), m_data[index].to_ptr()));
        }
        return Error<>();
    }

    Result<> erase(const HandleType hdl) {
        using namespace handle_pool_shared;
        Result<u32, void> lookup_result = contains_internal(hdl);
        if (!lookup_result) {
            return Error<>();
        }
        const u32 index = lookup_result.value();
        modus_assert(index < Size);

        HandlePrivate& priv_hdl = m_handles[index];
        m_data[index].destroy();
        priv_hdl.active = 0;
        priv_hdl.counter++;
        priv_hdl.next_free = m_next_free;
        m_next_free = index;
        m_count--;
        return Ok<>();
    }

    Result<NotMyPtr<T>, void> get(const HandleType hdl) {
        Result<u32, void> lookup_result = contains_internal(hdl);
        if (!lookup_result) {
            return Error<>();
        }
        T* ptr = m_data[lookup_result.value()].to_ptr();
        return Ok(NotMyPtr<T>(ptr));
    }

    Result<NotMyPtr<const T>, void> get(const HandleType hdl) const {
        Result<u32, void> lookup_result = contains_internal(hdl);
        if (!lookup_result) {
            return Error<>();
        }
        const T* ptr = m_data[lookup_result.value()].to_ptr();
        return Ok(NotMyPtr<const T>(ptr));
    }

    bool contains(const HandleType hdl) const { return contains_internal(hdl).is_value(); }

    inline u32 count() const { return m_count; }

    inline void clear() { initialize(true); }

    inline constexpr u32 capacity() const { return Size; }

    template <typename Callable>
    inline void for_each(Callable&& fn) {
        using namespace handle_pool_shared;
        for (u32 i = 0; i < Size; ++i) {
            HandlePrivate& priv_hdl = m_handles[i];
            if (priv_hdl.active) {
                T& t = *(m_data[i].to_ptr());
                fn(t);
            }
        }
    }

    template <typename Callable>
    inline void for_each(Callable&& fn) const {
        using namespace handle_pool_shared;
        for (u32 i = 0; i < Size; ++i) {
            const HandlePrivate& priv_hdl = m_handles[i];
            if (priv_hdl.active) {
                const T& t = *(m_data[i].to_ptr());
                fn(t);
            }
        }
    }

    inline bool is_full() const { return m_count == Size; }

   private:
    Result<u32, void> contains_internal(const HandleType hdl) const {
        using namespace handle_pool_shared;
        const HandlePublic hdl_pub = handle_to_handle_public(hdl);
        const u32 index = hdl_pub.index;
        const u32 counter = hdl_pub.counter;
        if (index >= Size) {
            return Error<>();
        }
        const HandlePrivate& priv_hdl = m_handles[index];
        return (priv_hdl.active && priv_hdl.counter == counter) ? Result<u32, void>(Ok(index))
                                                                : Result<u32, void>(Error<>());
    }

    Result<HandleType, void> create_internal() {
        using namespace handle_pool_shared;
        // Check out of handles
        modus_assert(m_next_free <= Size);
        if (m_next_free == Size) {
            return Error<>();
        }
        const u32 hdl_index = m_next_free;
        HandlePrivate& hdl = m_handles[hdl_index];
        // Update nextfree
        m_next_free = hdl.next_free;
        // Update internal hdl
        hdl.active = 1;
        m_count++;

        // Prepare public handle
        HandlePublic public_hdl;
        public_hdl.__pad__ = 0;
        public_hdl.index = hdl_index;
        public_hdl.counter = hdl.counter;
        return Ok(handle_public_to_handle<HandleType>(public_hdl));
    }

    void initialize(const bool destroy = false) {
        using namespace handle_pool_shared;
        m_count = 0;
        m_next_free = 0;
        for (u32 i = 0; i < Size; ++i) {
            HandlePrivate& priv = m_handles[i];
            if (destroy && priv.active) {
                m_data[i].destroy();
            }
            priv.next_free = i + 1;
            priv.counter = 0;
            priv.active = 0;
        }
    }
};
}    // namespace modus
