/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

namespace modus {

template <typename Allocator>
struct MODUS_CORE_EXPORT AllocatorTraits {
    template <typename T>
    using pointer_type = typename Allocator::template pointer_type<T>;
    template <typename T>
    using const_pointer_type = typename Allocator::template const_pointer_type<T>;
    using void_pointer_type = typename Allocator::void_pointer_type;
    using const_void_pointer_type = typename Allocator::const_void_pointer_type;
    using size_type = typename Allocator::size_type;

    inline static void_pointer_type allocate(const size_type size) {
        return Allocator::allocate(size);
    }

    inline static void_pointer_type realloc(void_pointer_type ptr, const size_type new_size) {
        return Allocator::realloc(ptr, new_size);
    }

    inline static void deallocate(void_pointer_type ptr) { Allocator::deallocate(ptr); }

    template <typename T, typename... Args>
    inline static void construct(pointer_type<T> ptr, Args&&... args) {
        MODUS_INPLACE_NEW(ptr) T(std::forward<Args>(args)...);
    }

    template <typename T>
    inline static void destroy(pointer_type<T> ptr) {
        ptr->~T();
    }

    template <typename T, typename... Args>
    inline static pointer_type<T> allocate_and_construct(Args&&... args) {
        void_pointer_type ptr = Allocator::allocate(sizeof(T));
        MODUS_INPLACE_NEW(ptr) T(std::forward<Args>(args)...);
        return pointer_type<T>(reinterpret_cast<T*>(ptr));
    }

    template <typename T>
    inline static void destroy_and_free(pointer_type<T> ptr) {
        ptr->~T();
        Allocator::deallocate(ptr);
    }
};

template <typename T, typename Allocator>
struct MODUS_CORE_EXPORT StdAllocatorWrapperStateless {
    using value_type = T;
    using pointer_type = typename Allocator::template pointer_type<T>;
    using const_pointer_type = typename Allocator::template const_pointer_type<T>;
    using void_pointer_type = typename Allocator::void_pointer_type;
    using const_void_pointer_type = typename Allocator::const_void_pointer_type;
    using size_type = typename Allocator::size_type;
    using difference_type = typename Allocator::difference_type;
    using this_type = StdAllocatorWrapperStateless<T, Allocator>;

    inline pointer_type allocate(const size_type size) {
        return reinterpret_cast<T*>(Allocator::allocate(size * sizeof(T)));
    }

    inline void deallocate(pointer_type ptr, const size_type) { Allocator::deallocate(ptr); }

    StdAllocatorWrapperStateless() = default;
    StdAllocatorWrapperStateless(const this_type&) = default;
    template <typename U>
    StdAllocatorWrapperStateless(const StdAllocatorWrapperStateless<U, Allocator>&) {}
    StdAllocatorWrapperStateless(this_type&&) noexcept = default;
    this_type& operator=(const this_type&) = default;
    this_type& operator=(this_type&&) = default;
    bool operator==(const this_type& rhs) const { return this == &rhs; }
    bool operator!=(const this_type& rhs) const { return this != &rhs; }
};

}    // namespace modus
