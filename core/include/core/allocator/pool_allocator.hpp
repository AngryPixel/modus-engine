/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/allocator/allocator_traits.hpp>
#include <core/core.hpp>

namespace modus {

struct MODUS_CORE_EXPORT FreeListNode {
    FreeListNode* next = nullptr;
};

template <typename Allocator>
class MODUS_CORE_EXPORT PoolAllocator {
   private:
    FreeListNode* m_chunks;
    FreeListNode* m_blocks;
    const u32 m_chunks_per_block;
    const u32 m_type_size;
    const u32 m_block_size;
    const u32 m_chunk_alignment;

   public:
    using this_type = PoolAllocator<Allocator>;

    PoolAllocator(const u32 type_size, const u32 chunks_per_block, const u32 chunk_alignment)
        : m_chunks(nullptr),
          m_blocks(nullptr),
          m_chunks_per_block(chunks_per_block),
          // Safeguard against types < sizeof(FreeListNode)
          m_type_size(std::max(sizeof(FreeListNode), usize(type_size)) + (chunk_alignment - 1)),
          m_block_size(sizeof(FreeListNode) + (m_type_size * m_chunks_per_block)),
          m_chunk_alignment(chunk_alignment) {
        modus_assert(m_chunks_per_block > 0);
        modus_assert(m_type_size > 0);
    }

    virtual ~PoolAllocator() {
        while (m_blocks != nullptr) {
            FreeListNode* block = m_blocks;
            m_blocks = block->next;
            AllocatorTraits<Allocator>::deallocate(block);
        }
    }

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(PoolAllocator, this_type);

    [[nodiscard]] void* allocate() {
        if (m_chunks == nullptr) {
            m_chunks = allocate_block();
        }
        if (m_chunks == nullptr) {
            return nullptr;
        }
        FreeListNode* node = m_chunks;
        m_chunks = m_chunks->next;
        return node;
    }

    void deallocate(void* ptr) {
        FreeListNode* node_ptr = reinterpret_cast<FreeListNode*>(ptr);
        node_ptr->next = m_chunks;
        m_chunks = node_ptr;
    }

   private:
    [[nodiscard]] FreeListNode* allocate_block() {
        // Allocate memory
        void* ptr = AllocatorTraits<Allocator>::allocate(m_block_size);
        if (ptr == nullptr) {
            return nullptr;
        }

        FreeListNode* block = reinterpret_cast<FreeListNode*>(ptr);
        block->next = nullptr;
        FreeListNode* start_node = block + 1;

        // Update Free List pointers inside block
        FreeListNode* node = start_node;
        FreeListNode* aligned_node = modus::align_ptr(node, m_chunk_alignment);
        FreeListNode* aligned_node_start = aligned_node;
        for (usize i = 0; i < m_chunks_per_block - 1; ++i) {
            node->next =
                reinterpret_cast<FreeListNode*>((reinterpret_cast<uintptr_t>(node) + m_type_size));
            node = node->next;
            aligned_node->next = modus::align_ptr(node, m_chunk_alignment);
            aligned_node = aligned_node->next;
        }
        node->next = nullptr;
        aligned_node->next = nullptr;

        // Update BlockList
        if (m_blocks != nullptr) {
            block->next = m_blocks;
            m_blocks = block;
        } else {
            m_blocks = block;
        }

        return aligned_node_start;
    }
};

template <typename T, typename Allocator>
class MODUS_CORE_EXPORT PoolAllocatorTyped final : public PoolAllocator<Allocator> {
   public:
    using value_type = T;
    using this_type = PoolAllocatorTyped<T, Allocator>;

    PoolAllocatorTyped(const u32 chuncks_per_block)
        : PoolAllocator<Allocator>(sizeof(T), chuncks_per_block, std::alignment_of_v<T>) {}

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(PoolAllocatorTyped, this_type);

    template <typename... Args>
    [[nodiscard]] T* construct(Args&&... args) {
        T* ptr = reinterpret_cast<T*>(this->allocate());
        if (ptr != nullptr) {
            new (ptr) T(std::forward<Args>(args)...);
        }
        return ptr;
    }

    void destroy(T* ptr) {
        modus_assert(ptr != nullptr);
        ptr->~T();
        this->deallocate(ptr);
    }
};

}    // namespace modus
