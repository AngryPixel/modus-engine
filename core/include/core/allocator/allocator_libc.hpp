/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>

namespace modus {

/// System's libc allocator
struct MODUS_CORE_EXPORT AllocatorLibC {
    template <typename T>
    using pointer_type = T*;

    template <typename T>
    using const_pointer_type = const T&;

    template <typename T>
    using reference_type = T*;
    template <typename T>

    using const_reference_type = const T&;

    using void_pointer_type = void*;

    using const_void_pointer_type = const void*;

    using size_type = usize;

    using difference_type = isize;

    static void* allocate(const usize size);

    static void* realloc(void* ptr, const usize size);

    static void deallocate(void* ptr);
};
}    // namespace modus
