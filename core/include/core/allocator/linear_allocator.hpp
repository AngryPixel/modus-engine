/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

template <bool ThreadSafe, usize Alignment, typename Allocator>
class MODUS_CORE_EXPORT LinearAllocatorBase {
   private:
    u8* m_bytes;
    u8* m_bytes_original;
    usize m_size;
    std::conditional_t<ThreadSafe, std::atomic<usize>, usize> m_offset;

   public:
    template <typename T>
    using pointer_type = T*;
    template <typename T>
    using const_pointer_type = const T&;
    template <typename T>
    using reference_type = T*;
    template <typename T>
    using const_reference_type = const T&;
    using void_pointer_type = void*;
    using const_void_pointer_type = const void*;
    using size_type = usize;
    using difference_type = isize;

    LinearAllocatorBase() : m_bytes(nullptr), m_bytes_original(nullptr), m_size(0), m_offset(0) {}

    ~LinearAllocatorBase() { modus_assert(m_bytes == nullptr); }

    inline void* allocate(const usize size) {
        modus_assert(m_bytes != nullptr);
        // Out of memory

        usize size_aligned = size;
        if constexpr (Alignment != 0) {
            size_aligned += (Alignment - (size % Alignment));
        }

        usize cur_offset;
        if constexpr (ThreadSafe) {
            cur_offset = m_offset.fetch_add(size_aligned);
        } else {
            cur_offset = m_offset;
            m_offset += size_aligned;
        }
        if (m_offset > m_size) {
            return nullptr;
        }
        void* result = m_bytes + cur_offset;
        return result;
    }

    inline void* realloc(void*, const usize) {
        modus_assert_message(false, "Not supported");
        return nullptr;
    }

    inline void deallocate(void*) { modus_assert_message(false, "Not supported"); }

    inline Result<> initialize(const usize size) {
        if (m_bytes != nullptr) {
            return Error<>();
        }
        if constexpr (Alignment == 0) {
            m_bytes = reinterpret_cast<u8*>(Allocator::allocate(size));
            if (m_bytes == nullptr) {
                return Error<>();
            }
            m_bytes_original = m_bytes;
            m_size = size;
        } else {
            const usize alloc_size = size + (Alignment - 1);
            m_bytes_original = reinterpret_cast<u8*>(Allocator::allocate(alloc_size));
            if (m_bytes_original == nullptr) {
                return Error<>();
            }
            m_bytes = reinterpret_cast<u8*>(
                (reinterpret_cast<usize>(m_bytes_original) + Alignment - 1) & ~(Alignment - 1));
            m_size = size;
        }
        return Ok<>();
    }

    inline void shutdown() {
        if (m_bytes_original != nullptr) {
            Allocator::deallocate(m_bytes_original);
            m_bytes = nullptr;
            m_bytes_original = nullptr;
        }
        m_size = 0;
        m_offset = 0;
    }

    inline usize size() const { return m_size; }

    const u8* data() const { return m_bytes; }

    u8* data() { return m_bytes; }

    void clear() { m_offset = 0; }
};

template <typename Allocator>
using LinearAllocator = LinearAllocatorBase<false, 0, Allocator>;
template <typename Allocator>
using ThreadSafeLinearAllocator = LinearAllocatorBase<true, 0, Allocator>;

template <usize Alignment, typename Allocator>
using AlignedLinearAllocator = LinearAllocatorBase<false, Alignment, Allocator>;
template <usize Alignment, typename Allocator>
using ThreadSafeAlignedLinearAllocator = LinearAllocatorBase<true, Alignment, Allocator>;

}    // namespace modus
