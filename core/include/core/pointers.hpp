/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

template <typename T>
using PimplPtr = std::unique_ptr<T, void (*)(T*)>;

template <typename T>
inline PimplPtr<T> make_pimpl_ptr(T* data) {
    void (*deleter)(T*) = [](T* ptr) { MODUS_DELETE ptr; };
    return PimplPtr<T>(data, deleter);
}

template <typename T>
inline PimplPtr<T> make_pimpl_ptr_empty() {
    void (*deleter)(T*) = [](T* ptr) { MODUS_DELETE ptr; };
    return PimplPtr<T>(nullptr, deleter);
}

template <typename T, typename... Args>
inline PimplPtr<T> make_pimpl_ptr(Args&&... args) {
    void (*deleter)(T*) = [](T* ptr) { MODUS_DELETE ptr; };
    T* ptr = MODUS_NEW T(std::forward<Args>(args)...);
    if (ptr == nullptr) {
        // TODO: Return result?
        modus_panic("Failed to allocate make_pimpl_ptr");
    }
    return PimplPtr<T>(ptr, deleter);
}

}    // namespace modus
