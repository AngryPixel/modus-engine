/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

template <typename T>
class MODUS_CORE_EXPORT NonOwningPtr {
   private:
    T* m_ptr = nullptr;

   public:
    NonOwningPtr() = default;
    ~NonOwningPtr() = default;

    explicit inline NonOwningPtr(std::nullptr_t) : NonOwningPtr() {}

    explicit inline NonOwningPtr(T* ptr) noexcept : m_ptr(ptr) {}

    template <typename U>
    inline NonOwningPtr(U* ptr) noexcept : NonOwningPtr<T>() {
        m_ptr = static_cast<T*>(ptr);
    }

    template <typename U>
    inline NonOwningPtr(const U* ptr) noexcept
        : NonOwningPtr<const T>(static_cast<const T*>(ptr)) {}

    NonOwningPtr(const NonOwningPtr<T>& rhs) : m_ptr(rhs.m_ptr) {}

    NonOwningPtr<T>& operator=(const NonOwningPtr<T>& rhs) {
        m_ptr = rhs.m_ptr;
        return *this;
    }

    template <typename U>
    NonOwningPtr(const NonOwningPtr<U>& rhs) noexcept : NonOwningPtr<T>() {
        m_ptr = static_cast<T*>(rhs.get());
    }

    template <typename U>
    NonOwningPtr<T>& operator=(const NonOwningPtr<U>& rhs) noexcept {
        *this = NonOwningPtr<T>(rhs);
        return *this;
    }

    inline NonOwningPtr(NonOwningPtr<T>&& rhs) noexcept : m_ptr(rhs.m_ptr) { rhs.reset(); }

    template <typename U>
    inline NonOwningPtr(NonOwningPtr<U>&& rhs) noexcept : NonOwningPtr<T>(rhs.get()) {
        rhs.reset();
    }

    inline NonOwningPtr<T>& operator=(NonOwningPtr<T>&& rhs) noexcept {
        if (this != &rhs) {
            m_ptr = rhs.m_ptr;
            rhs.reset();
        }
        return *this;
    }

    template <typename U>
    inline NonOwningPtr<T>& operator=(NonOwningPtr<U>&& rhs) noexcept {
        if (this != &rhs) {
            m_ptr = static_cast<T*>(rhs.get());
            rhs.reset();
        }
        return *this;
    }

    inline bool is_valid() const { return m_ptr != nullptr; }

    inline explicit operator bool() const { return m_ptr != nullptr; }

    T* get() const { return m_ptr; }

    template <typename U = T>
    inline typename std::enable_if<!std::is_same<U, void>::value, U>::type& operator*() {
        modus_assert(m_ptr != nullptr);
        return *m_ptr;
    }

    template <typename U = T>
    inline typename std::enable_if<!std::is_same<U, void>::value, const U>::type& operator*()
        const {
        modus_assert(m_ptr != nullptr);
        return *m_ptr;
    }

    inline T* operator->() {
        modus_assert(m_ptr != nullptr);
        return m_ptr;
    }

    inline const T* operator->() const {
        modus_assert(m_ptr != nullptr);
        return m_ptr;
    }

    void reset(T* ptr = nullptr) { m_ptr = ptr; }

    inline bool operator==(const NonOwningPtr<T>& rhs) const { return m_ptr == rhs.m_ptr; }

    inline bool operator!=(const NonOwningPtr<T>& rhs) const { return m_ptr != rhs.m_ptr; }

    inline bool operator==(const T* rhs) const { return m_ptr == rhs; }

    inline bool operator!=(const T* rhs) const { return m_ptr != rhs; }

    template <typename U>
    inline bool operator==(const NonOwningPtr<U>& rhs) const {
        return m_ptr == rhs.get();
    }

    template <typename U>
    inline bool operator!=(const NonOwningPtr<U>& rhs) const {
        return m_ptr != rhs.get();
    }
};

}    // namespace modus
