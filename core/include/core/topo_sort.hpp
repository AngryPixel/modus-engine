/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

/***
 * Topological sort
 */

enum class TopologicalSortError {
    None,
    DuplicateNode,
    DependencyCycle,
    DependencyNotFound,
};

template <typename T>
struct TopologicalSorterDefaultKeyCompare {
    constexpr bool operator()(const T& key1, const T& key2) const { return key1 == key2; }
};

template <typename T, typename Key, typename KeyCompare = TopologicalSorterDefaultKeyCompare<Key>>
class TopologicalSorter {
   private:
    enum class State { None, Visiting, Visited };
    struct Node;
    struct Dependency {
        Key m_key;
        NotMyPtr<Node> m_node;
    };

    struct Node {
        NotMyPtr<T> m_type;
        Key m_key;
        Vector<Dependency> m_dependencies;
        State m_state = State::None;
    };
    Vector<Node> m_nodes;
    Optional<Key> m_error_arg1;
    Optional<Key> m_error_arg2;

   public:
    using this_type = TopologicalSorter<T, Key, KeyCompare>;
    using ptr_type = NotMyPtr<T>;

    TopologicalSorter() = default;
    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(TopologicalSorter, this_type);

    void reserve(const size_t size) { m_nodes.reserve(size); }

    [[nodiscard]] TopologicalSortError add(ptr_type value,
                                           const Key& key,
                                           Slice<Key> dependencies) {
        if (find_node(key).has_value()) {
            return TopologicalSortError::DuplicateNode;
        }

        Node node;
        node.m_key = key;
        node.m_type = value;
        node.m_dependencies.reserve(dependencies.size());
        for (auto& dep : dependencies) {
            node.m_dependencies.emplace_back(Dependency{dep, NotMyPtr<Node>()});
        }

        m_nodes.emplace_back(std::move(node));
        return TopologicalSortError::None;
    }

    [[nodiscard]] TopologicalSortError evaluate(Vector<ptr_type>& output) {
        m_error_arg1.reset();
        m_error_arg2.reset();

        if (m_nodes.empty()) {
            return TopologicalSortError::None;
        }

        if (m_nodes.size() == 1) {
            output.push_back(m_nodes[0].m_type);
            return TopologicalSortError::None;
        }

        Vector<NotMyPtr<Node>> queue;
        queue.reserve(m_nodes.size());

        // Reset state & search for the dependencies
        for (auto it = m_nodes.rbegin(); it != m_nodes.rend(); ++it) {
            auto& node = *it;
            node.m_state = State::None;
            for (auto& dep : node.m_dependencies) {
                auto dep_opt = find_node(dep.m_key);
                if (!dep_opt.has_value()) {
                    m_error_arg1 = dep.m_key;
                    return TopologicalSortError::DependencyNotFound;
                }
                dep.m_node = *dep_opt;
            }
        }

        // Start calculation
        output.reserve(m_nodes.size());

        for (auto& node : m_nodes) {
            if (node.m_state == State::None) {
                queue.push_back(&node);
            }
            while (!queue.empty()) {
                // pop a node
                auto node_ptr = queue.back();
                queue.erase(queue.begin() + (queue.size() - 1));

                // Already visited, continue
                if (node_ptr->m_state == State::Visited) {
                    continue;
                }

                // Visiting, finalize
                if (node_ptr->m_state == State::Visiting) {
                    node_ptr->m_state = State::Visited;
                    output.push_back(node_ptr->m_type);
                    continue;
                }

                // First visit
                node_ptr->m_state = State::Visiting;
                queue.push_back(node_ptr);

                for (auto& dep : node_ptr->m_dependencies) {
                    if (dep.m_node->m_state == State::Visiting) {
                        m_error_arg1 = dep.m_node->m_key;
                        m_error_arg2 = node_ptr->m_key;
                        return TopologicalSortError::DependencyCycle;
                    }
                    queue.push_back(dep.m_node);
                }
            }
        }
        return TopologicalSortError::None;
    }

    void clear() { m_nodes.clear(); }

    Optional<Key> node_where_cycle_was_detected() const { return m_error_arg1; }

    Optional<Key> node_being_processed_when_cycle_was_detected() const { return m_error_arg2; }

    Optional<Key> missing_dependency() const { return m_error_arg1; }

   private:
    Optional<Node*> find_node(const Key& key) {
        for (auto& node : m_nodes) {
            if (KeyCompare()(node.m_key, key)) {
                return &node;
            }
        }
        return Optional<Node*>();
    }
};

}    // namespace modus
