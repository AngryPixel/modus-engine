/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/string_slice.hpp>

namespace modus {

template <usize Size>
class FixedStackString {
    static_assert(Size > 2);

   private:
    usize m_size;
    char m_str[Size];

   public:
    using value_type = char;
    using size_type = usize;
    using this_type = FixedStackString<Size>;

    inline FixedStackString() : m_size(0) { m_str[0] = '\0'; }

    inline FixedStackString(const char* str) : FixedStackString(StringSlice(str)) {}

    inline FixedStackString(const StringSlice& slice) {
        m_size = std::min((Size - 1), slice.size());
        memcpy(m_str, slice.data(), m_size);
        m_str[m_size] = '\0';
    }

    inline FixedStackString(const this_type& rhs) : FixedStackString() {
        if (rhs.m_size > 0) {
            m_size = rhs.m_size;
            modus_assert(m_size + 1 <= Size);
            memcpy(m_str, rhs.m_str, m_size + 1);
        }
    }

    inline FixedStackString(this_type&& rhs) noexcept : FixedStackString() {
        if (rhs.m_size > 0) {
            m_size = rhs.m_size;
            modus_assert(m_size + 1 <= Size);
            memcpy(m_str, rhs.m_str, m_size + 1);
        }
        rhs.m_size = 0;
        rhs.m_str[0] = '\0';
    }

    inline FixedStackString& operator=(const this_type& rhs) {
        if (this != &rhs) {
            MODUS_INPLACE_NEW(this) this_type(rhs);
        }
        return *this;
    }

    inline FixedStackString& operator=(this_type&& rhs) noexcept {
        if (this != &rhs) {
            MODUS_INPLACE_NEW(this) this_type(std::forward<this_type>(rhs));
        }
        return *this;
    }

    inline FixedStackString& operator=(const char* rhs) {
        if (&this->m_str[0] != rhs) {
            MODUS_INPLACE_NEW(this) this_type(rhs);
        }
        return *this;
    }

    inline FixedStackString& operator=(const StringSlice& rhs) {
        if (&this->m_str[0] != rhs.data()) {
            MODUS_INPLACE_NEW(this) this_type(rhs);
        }
        return *this;
    }

    inline size_type size() const { return m_size; }

    constexpr size_type capacity() const { return Size; }

    inline void clear() {
        m_size = 0;
        m_str[0] = '\0';
    }

    inline StringSlice as_string_slice() const { return StringSlice(m_str, m_size); }

    inline ByteSlice as_bytes() const {
        return ByteSlice(reinterpret_cast<const u8*>(&m_str[0], m_size));
    }

    inline bool operator==(const this_type& rhs) const {
        return as_string_slice() == rhs.as_string_slice();
    }

    inline bool operator!=(const this_type& rhs) const {
        return as_string_slice() != rhs.as_string_slice();
    }

    inline bool operator==(const StringSlice& rhs) const { return as_string_slice() == rhs; }

    inline bool operator!=(const StringSlice& rhs) const { return as_string_slice() != rhs; }

    inline const char* data() const { return m_str; }

    inline const char* c_str() const { return m_str; }

    String to_str() const { return String(m_str, m_size); }

    using iterator = PointerIterator<char>;
    using const_iterator = PointerIterator<const char>;

    inline iterator begin() { return iterator(m_str, m_str, m_str + m_size); }

    inline iterator end() { return iterator(m_str + m_size, m_str, m_str + m_size); }

    inline const_iterator begin() const { return const_iterator(m_str, m_str, m_str + m_size); }

    inline const_iterator end() const {
        return const_iterator(m_str + m_size, m_str, m_str + m_size);
    }
};

}    // namespace modus

namespace fmt {
template <modus::usize Size>
struct formatter<modus::FixedStackString<Size>> : formatter<fmt::string_view> {
    template <typename FormatContext>
    auto format(const modus::FixedStackString<Size>& p, FormatContext& ctx) {
        const fmt::string_view view(p.data(), p.size());
        return formatter<fmt::string_view>::format(view, ctx);
    }
};
}    // namespace fmt
