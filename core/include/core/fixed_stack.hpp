/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/fixed_vector.hpp>

namespace modus {

/// A stack allocated Stack with a fixed size
template <typename T, usize Size>
class FixedStack {
   private:
    FixedVector<T, Size> m_stack;

   public:
    using value_type = T;
    using this_type = FixedStack<T, Size>;

    FixedStack() = default;

    MODUS_CLASS_TEMPLATE_DEFAULT_COPY(FixedStack, this_type);
    MODUS_CLASS_TEMPLATE_DISABLE_MOVE(FixedStack, this_type);

    inline bool is_empty() const { return m_stack.is_empty(); }

    inline usize size() const { return m_stack.size(); }

    inline bool at_max_capacity() const { return m_stack.size() >= Size; }

    inline Optional<T*> top() { return m_stack.back(); }

    inline Optional<const T*> top() const { return m_stack.back(); }

    inline Optional<T> pop() { return m_stack.pop_back(); }

    inline Result<> push(const T& value) { return m_stack.push_back(value); }

    inline Result<> push(T&& value) { return m_stack.push_back(std::forward<T>(value)); }

    inline void clear() {
        while (pop()) {
        }
    }
};

}    // namespace modus
