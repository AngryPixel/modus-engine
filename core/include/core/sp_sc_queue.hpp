/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus {

/// Lockfree single producer single consumer queue. Based on the article
/// http://kjellkod.wordpress.com/2012/11/28/c-debt-paid-in-full-wait-free-lock-free-queue/
template <class T, usize Size>
class MODUS_CORE_EXPORT SpScQueue {
   private:
    std::atomic<usize> m_index_read;
    Array<T, Size> m_queue;
    std::atomic<usize> m_index_write;

   public:
    using value_type = T;
    using this_type = SpScQueue<T, Size>;

    SpScQueue() : m_index_read(0), m_queue(), m_index_write(0) {}

    ~SpScQueue() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(SpScQueue, this_type);

    Result<T, void> dequeue() {
        const usize cur_index_read = m_index_read.load(std::memory_order_relaxed);
        if (cur_index_read == m_index_write.load(std::memory_order_acquire)) {
            // Queue is empty
            return Error<>();
        }

        Result<T, void> result = Ok(std::move(m_queue[cur_index_read]));
        const usize next_index_read = (cur_index_read + 1) % Size;
        m_index_read.store(next_index_read, std::memory_order_release);
        return result;
    }

    Result<> queue(const T& value) {
        const usize cur_index_write = m_index_write.load(std::memory_order_relaxed);
        const usize next_index_write = (cur_index_write + 1) % Size;

        if (next_index_write == m_index_read.load(std::memory_order_acquire)) {
            // queue full
            return Error<>();
        }

        m_queue[cur_index_write] = value;
        m_index_write.store(next_index_write, std::memory_order_release);
        return Ok<>();
    }

    Result<> queue(T&& value) {
        const usize cur_index_write = m_index_write.load(std::memory_order_relaxed);
        const usize next_index_write = (cur_index_write + 1) % Size;

        if (next_index_write == m_index_read.load(std::memory_order_acquire)) {
            // queue full
            return Error<>();
        }

        m_queue[cur_index_write] = std::move(value);
        m_index_write.store(next_index_write, std::memory_order_release);
        return Ok<>();
    }

    bool was_empty() const { return (m_index_read.load() == m_index_write.load()); }

    bool was_full() const {
        const usize next_index_write = (m_index_write.load() + 1) % Size;
        return (next_index_write == m_index_read.load());
    }
};

}    // namespace modus
