/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>

#include <catch2/catch.hpp>
#include <core/slice.hpp>

struct Custom {
    using value_type = int;
    int x;
};

namespace modus {
template <>
struct AsSlice<Custom> {
    using value_type = int;
    static Slice<int> to_slice(Custom& x) { return Slice<int>(&x.x, 1); }
};
}    // namespace modus

using namespace modus;

TEST_CASE("slice from fixed array with make_slice", "[Slice]") {
    const int array[4] = {1, 2, 3, 4};
    auto slice = make_slice(array);
    REQUIRE(slice.size() == 4);
}

TEST_CASE("slice from fixed array", "[slice]") {
    const int array[4] = {1, 2, 3, 4};
    auto slice = Slice<int>(array);
    REQUIRE(slice.size() == 4);
    REQUIRE(slice[0] == 1);
    REQUIRE(slice[1] == 2);
    REQUIRE(slice[2] == 3);
    REQUIRE(slice[3] == 4);
}

TEST_CASE("Iterator on empty Slice", "[Slice]") {
    const Slice<i32> slice;

    auto it = slice.begin();
    REQUIRE(it == slice.end());
    REQUIRE(slice.is_empty());
}

TEST_CASE("Iterator on Slice", "[Slice]") {
    const int array[4] = {1, 2, 3, 4};
    auto slice = Slice<int>(array);

    usize index = 0;
    for (auto& v : slice) {
        REQUIRE(v == array[index]);
        index++;
    }
    REQUIRE(index == slice.size());
}

TEST_CASE("Subslice tests", "[Slice]") {
    const i32 array[4] = {0, 1, 3, 4};
    Slice<i32> slice = make_slice(array);

    auto sub_slice = slice.sub_slice(2);

    REQUIRE(2 == sub_slice.size());
    REQUIRE(0 == sub_slice[0]);
    REQUIRE(1 == sub_slice[1]);

    sub_slice = slice.sub_slice(2, 2);

    REQUIRE(2 == sub_slice.size());
    REQUIRE(3 == sub_slice[0]);
    REQUIRE(4 == sub_slice[1]);
}

TEST_CASE("Find tests", "[Slice]") {
    const i32 array[4] = {0, 1, 3, 4};
    Slice<i32> slice(array);

    auto result = slice.find(5);
    REQUIRE(result.is_error());

    result = slice.find(4);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 3);

    result = slice.find(4, 2);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 3);

    result = slice.find(4, 5);
    REQUIRE(result.is_error());

    result = slice.rfind(4);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 3);

    result = slice.rfind(0);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 0);

    result = slice.rfind(0, 2);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 0);

    result = slice.rfind(1, 2);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 1);
}
