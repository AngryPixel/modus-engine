/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/action_queue.hpp>
#include <core/containers.hpp>
#include <iostream>

using namespace modus;

void queue_fn() {
    std::cout << "From Function\n";
}

TEST_CASE("Basic Test", "[ActionQueue]") {
    ActionQueue<void(), DefaultAllocator> queue;
    {
        const auto r = queue.initialize(4096);
        REQUIRE(r);
    }

    {
        const auto r = queue.queue<queue_fn>();
        REQUIRE(r);
    }

    {
        const auto r = queue.queue([]() { std::cout << "From Lambda\n"; });
        REQUIRE(r);
    }

    CHECK(queue.queued_action_count() == 2);

    queue.execute();
}

TEST_CASE("Queue With Lamda Captures", "[ActionQueue]") {
    ActionQueue<void(), DefaultAllocator> queue;
    {
        const auto r = queue.initialize(4096);
        REQUIRE(r);
    }

    {
        std::string str =
            "My very long string guarateed to allocate some memory, because it is too long";
        const auto r = queue.queue([str]() { std::cout << "Capture: " << str << '\n'; });
        REQUIRE(r);
    }

    CHECK(queue.queued_action_count() == 1);

    queue.execute();
}
