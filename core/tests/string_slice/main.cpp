/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>

#include <catch2/catch.hpp>
#include <core/string_slice.hpp>

using namespace modus;

static constexpr usize kCostumStrLen = 16;

struct CustomString {
    char str[kCostumStrLen] = {'H', 'e', 'l', 'l', 'o', 'W', 'o', 'r',
                               'l', 'd', '!', '1', '2', '3', '4', '5'};
};

/*
namespace modus {
template <>
struct AsStringSlice<CustomString> {
    static StringSlice to_string_slice(const CustomString& x) {
        return StringSlice(x.str, sizeof(x.str));
    }
};

}    // namespace modus

using namespace modus;

void implicit_conversion_compile_test(StringSlice) {}

TEST_CASE("StringSlice from custom type", "[StringSlice]") {
    CustomString cs;
    StringSlice slice(cs);
    REQUIRE(slice.size() == kCostumStrLen);
    implicit_conversion_compile_test(cs);
}
*/
TEST_CASE("StringSlice from const char*", "[StringSlice]") {
    const char* cstr = "hello";
    StringSlice slice(cstr);
    REQUIRE(slice.size() == 5);
}

TEST_CASE("Find char", "[StringSlice]") {
    const char* cstr = "hello";
    StringSlice slice(cstr);

    auto r = slice.find('l');
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 2);
    r = slice.find('l', 3);

    REQUIRE(r.is_value());
    REQUIRE(r.value() == 3);

    r = slice.find('f');
    REQUIRE(r.is_error());
}

TEST_CASE("Reverse Find char", "[StringSlice]") {
    const char* cstr = "hello";
    StringSlice slice(cstr);

    auto r = slice.rfind('l');
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 3);
    r = slice.rfind('l', 2);

    REQUIRE(r.is_value());
    REQUIRE(r.value() == 2);

    r = slice.rfind('f');
    REQUIRE(r.is_error());
}

TEST_CASE("Find String", "[StringSlice]") {
    const char* cstr = "HelloWorld";
    StringSlice slice(cstr);

    const char* str1 = "Hello";
    auto r = slice.find(str1);
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 0);

    r = slice.find(str1, 4);
    REQUIRE(r.is_error());

    const char* str2 = "World";
    r = slice.find(str2);
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 5);

    const char* str3 = "oWo";
    r = slice.find(str3);
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 4);
}

TEST_CASE("Reverse Find String", "[StringSlice]") {
    const char* cstr = "HelloWorld";
    StringSlice slice(cstr);

    const char* str1 = "Hello";
    auto r = slice.rfind(str1);
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 0);

    r = slice.rfind(str1, 8);
    REQUIRE(r.is_error());

    const char* str2 = "World";
    r = slice.rfind(str2);
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 5);

    const char* str3 = "oWo";
    r = slice.rfind(str3);
    REQUIRE(r.is_value());
    REQUIRE(r.value() == 4);
}

TEST_CASE("Starts With", "[StringSlice]") {
    const char* cstr = "HelloWorld";
    StringSlice slice(cstr);

    const char* str1 = "Hello";
    const char* str2 = "World";
    const char* str3 = "ello";
    bool b = slice.starts_with(str1);
    REQUIRE(b);

    b = slice.starts_with(str2);
    REQUIRE_FALSE(b);

    b = slice.starts_with(str3);
    REQUIRE_FALSE(b);
}

TEST_CASE("Ends With", "[StringSlice]") {
    const char* cstr = "HelloWorld";
    StringSlice slice(cstr);

    const char* str1 = "Hello";
    const char* str2 = "World";
    const char* str3 = "Worl";
    bool b = slice.ends_with(str2);
    REQUIRE(b);

    b = slice.ends_with(str1);
    REQUIRE_FALSE(b);

    b = slice.ends_with(str3);
    REQUIRE_FALSE(b);
}

TEST_CASE("Replace", "[StringSlice]") {
    const char* cstr = "HelloWorld";
    StringSlice slice(cstr);

    const String r1 = slice.replace("llo", "Abc");
    const String r2 = slice.replace("World", "Me");
    const String r3 = slice.replace("Hello", "Hi");
    const String r4 = slice.replace("Foo", "Bar");
    REQUIRE(r1 == "HeAbcWorld");
    REQUIRE(r2 == "HelloMe");
    REQUIRE(r3 == "HiWorld");
    REQUIRE(r4 == "HelloWorld");
}
