/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <iostream>

#include <core/disjoint_set.hpp>
#include <core/topo_sort.hpp>

using namespace modus;

using TopoSorter = modus::TopologicalSorter<i32, i32>;

TEST_CASE("Simple Test", "[TopoSort]") {
    i32 v1 = 100;
    i32 k1 = 0;
    TopoSorter::ptr_type p1(&v1);
    i32 v2 = 200;
    i32 k2 = 1;
    TopoSorter::ptr_type p2(&v2);
    i32 v3 = 300;
    i32 k3 = 2;
    TopoSorter::ptr_type p3(&v3);
    i32 v4 = 400;
    i32 k4 = 3;
    TopoSorter::ptr_type p4(&v4);

    TopoSorter sorter;
    REQUIRE(sorter.add(p1, k1, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p2, k2, Slice<i32>({k3, k4})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p3, k3, Slice<i32>({k1})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p4, k4, Slice<i32>({k3})) == TopologicalSortError::None);

    Vector<TopoSorter::ptr_type> output;
    REQUIRE(sorter.evaluate(output) == TopologicalSortError::None);

    TopoSorter::ptr_type expected[] = {p1, p3, p4, p2};
    REQUIRE(Slice(expected) == make_slice(output));
}

TEST_CASE("Simple Test (Reversed Insert)", "[TopoSort]") {
    i32 v1 = 100;
    i32 k1 = 0;
    TopoSorter::ptr_type p1(&v1);
    i32 v2 = 200;
    i32 k2 = 1;
    TopoSorter::ptr_type p2(&v2);
    i32 v3 = 300;
    i32 k3 = 2;
    TopoSorter::ptr_type p3(&v3);
    i32 v4 = 400;
    i32 k4 = 3;
    TopoSorter::ptr_type p4(&v4);

    TopoSorter sorter;
    REQUIRE(sorter.add(p4, k4, Slice<i32>({k3})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p3, k3, Slice<i32>({k1})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p2, k2, Slice<i32>({k3, k4})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p1, k1, Slice<i32>()) == TopologicalSortError::None);

    Vector<TopoSorter::ptr_type> output;
    REQUIRE(sorter.evaluate(output) == TopologicalSortError::None);

    TopoSorter::ptr_type expected[] = {p1, p3, p4, p2};
    REQUIRE(Slice(expected) == make_slice(output));
}

TEST_CASE("Cycle", "[TopoSort]") {
    i32 v1 = 100;
    i32 k1 = 0;
    TopoSorter::ptr_type p1(&v1);
    i32 v2 = 200;
    i32 k2 = 1;
    TopoSorter::ptr_type p2(&v2);
    i32 v3 = 300;
    i32 k3 = 2;
    TopoSorter::ptr_type p3(&v3);
    i32 v4 = 400;
    i32 k4 = 3;
    TopoSorter::ptr_type p4(&v4);

    TopoSorter sorter;
    REQUIRE(sorter.add(p1, k1, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p2, k2, Slice<i32>({k3, k4})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p3, k3, Slice<i32>({k1})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p4, k4, Slice<i32>({k3, k2})) == TopologicalSortError::None);

    Vector<TopoSorter::ptr_type> output;
    REQUIRE(sorter.evaluate(output) == TopologicalSortError::DependencyCycle);

    auto node_with_cycle = sorter.node_where_cycle_was_detected();
    auto node_being_processed = sorter.node_being_processed_when_cycle_was_detected();

    REQUIRE(node_with_cycle);
    REQUIRE(node_being_processed);
    REQUIRE(*node_with_cycle == k2);
    REQUIRE(*node_being_processed == k4);
}

TEST_CASE("Entry without any dependencies", "[TopoSort]") {
    i32 v1 = 100;
    i32 k1 = 0;
    TopoSorter::ptr_type p1(&v1);
    i32 v2 = 200;
    i32 k2 = 1;
    TopoSorter::ptr_type p2(&v2);
    i32 v3 = 300;
    i32 k3 = 2;
    TopoSorter::ptr_type p3(&v3);
    i32 v4 = 400;
    i32 k4 = 3;
    TopoSorter::ptr_type p4(&v4);

    TopoSorter sorter;
    REQUIRE(sorter.add(p1, k1, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p2, k2, Slice<i32>({k3, k4})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p3, k3, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p4, k4, Slice<i32>({k3})) == TopologicalSortError::None);

    Vector<TopoSorter::ptr_type> output;
    REQUIRE(sorter.evaluate(output) == TopologicalSortError::None);

    const TopoSorter::ptr_type expected[] = {p1, p3, p4, p2};
    REQUIRE(Slice(expected) == make_slice(output));

    output.clear();
    sorter.clear();

    REQUIRE(sorter.add(p4, k4, Slice<i32>({k3})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p2, k2, Slice<i32>({k3, k4})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p3, k3, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p1, k1, Slice<i32>()) == TopologicalSortError::None);

    REQUIRE(sorter.evaluate(output) == TopologicalSortError::None);

    const TopoSorter::ptr_type expected_reverse[] = {p3, p4, p2, p1};
    REQUIRE(Slice(expected_reverse) == make_slice(output));

    output.clear();
    sorter.clear();

    REQUIRE(sorter.add(p3, k3, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p1, k1, Slice<i32>()) == TopologicalSortError::None);
    REQUIRE(sorter.add(p4, k4, Slice<i32>({k3})) == TopologicalSortError::None);
    REQUIRE(sorter.add(p2, k2, Slice<i32>({k3, k4})) == TopologicalSortError::None);

    REQUIRE(sorter.evaluate(output) == TopologicalSortError::None);

    const TopoSorter::ptr_type expected_crafted[] = {p3, p1, p4, p2};
    REQUIRE(Slice(expected_crafted) == make_slice(output));
}

TEST_CASE("Disjoint Set/Union Find", "DisjointSet") {
    using DisjointSet_t = modus::DisjointSet<const i32, i32>;
    DisjointSet_t ds;

    const i32 t0 = 0;
    const i32 t1 = 1;
    const i32 t2 = 2;
    const i32 t3 = 3;
    const i32 t4 = 4;
    const i32 t5 = 5;
    const i32 t6 = 6;
    const i32 t7 = 7;

    REQUIRE(ds.add(&t0, 0, Slice<i32>()) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t2, 2, Slice<i32>()) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t3, 3, Slice<i32>({2})) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t1, 1, Slice<i32>({3, 2})) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t4, 4, Slice<i32>({1, 0})) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t6, 6, Slice<i32>()) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t5, 5, Slice<i32>({6})) == modus::DisjointSetError::Ok);
    REQUIRE(ds.add(&t7, 7, Slice<i32>()) == modus::DisjointSetError::Ok);

    ds.evaluate();

    REQUIRE(ds.are_in_set(5, 6));
    REQUIRE_FALSE(ds.are_in_set(5, 7));
    REQUIRE_FALSE(ds.are_in_set(3, 7));
    REQUIRE_FALSE(ds.are_in_set(5, 0));
    REQUIRE(ds.are_in_set(1, 4));
    REQUIRE(ds.are_in_set(0, 4));
    REQUIRE(ds.are_in_set(2, 3));
    REQUIRE(ds.are_in_set(2, 4));
    REQUIRE(ds.subset_count() == 3);

    ds.build_sets();

    Slice<modus::Vector<DisjointSet_t::ptr_type>> sets = ds.subsets();

    std::cout << "Sets:\n";
    for (auto& set : sets) {
        for (auto& ptr : set) {
            std::cout << *ptr << ' ';
        }
        std::cout << '\n';
    }
}
