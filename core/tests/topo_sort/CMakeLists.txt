#
# IO tests
#

modus_add_test(test_topo_sort NAME TopologicalSort)

target_sources(test_topo_sort PRIVATE
    test_topo_sort.cpp
)

target_link_libraries(test_topo_sort PRIVATE core)
