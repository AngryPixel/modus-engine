/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/fixed_handle_pool.hpp>

using namespace modus;

TEST_CASE("Basic Tests With POD Type", "[FixedHandlePool]") {
    using Pool = FixedHandlePool<u32, 4>;
    Pool pool;

    const u32 v1 = 10;
    const u32 v2 = 20;
    const u32 v3 = 30;
    const u32 v4 = 40;
    const u32 v5 = 50;

    // Add Values
    auto r1 = pool.create(v1);
    REQUIRE(r1);
    auto r2 = pool.create(v2);
    REQUIRE(r2);
    auto r3 = pool.create(v3);
    REQUIRE(r3);
    auto r4 = pool.create(v4);
    REQUIRE(r4);
    auto r5 = pool.create(v5);
    REQUIRE_FALSE(r5);
    REQUIRE(pool.count() == 4);

    // Check Values
    auto gr1 = pool.get(r1.value().first);
    REQUIRE(gr1);
    REQUIRE(*gr1.value() == v1);

    auto gr2 = pool.get(r2.value().first);
    REQUIRE(gr2);
    REQUIRE(*gr2.value() == v2);

    auto gr3 = pool.get(r3.value().first);
    REQUIRE(gr3);
    REQUIRE(*gr3.value() == v3);

    auto gr4 = pool.get(r4.value().first);
    REQUIRE(gr4);
    REQUIRE(*gr4.value() == v4);

    // Remove 2 & 3;
    auto rem2 = pool.erase(r2.value().first);
    REQUIRE(rem2);
    auto rem3 = pool.erase(r3.value().first);
    REQUIRE(rem3);
    REQUIRE(pool.count() == 2);

    REQUIRE(pool.contains(r1.value().first));
    REQUIRE(pool.contains(r4.value().first));

    // Re add r2 and r3;
    auto r2_v2 = pool.create(v2);
    REQUIRE(r2);
    auto r3_v2 = pool.create(v3);
    REQUIRE(r3);

    // validate new r2 and r3
    auto gr2_v2 = pool.get(r2_v2.value().first);
    REQUIRE(gr2_v2);
    REQUIRE(*gr2_v2.value() == v2);

    auto gr3_v2 = pool.get(r3_v2.value().first);
    REQUIRE(gr3_v2);
    REQUIRE(*gr3_v2.value() == v3);

    // Make sure v1 r2 and r2 no longe exist
    REQUIRE_FALSE(pool.contains(r2.value().first));
    REQUIRE_FALSE(pool.contains(r3.value().first));

    // Remove everything
    auto rem1 = pool.erase(r1.value().first);
    REQUIRE(rem1);
    auto rem4 = pool.erase(r4.value().first);
    REQUIRE(rem4);
    auto rem2_v2 = pool.erase(r2_v2.value().first);
    REQUIRE(rem2_v2);
    auto rem3_v2 = pool.erase(r3_v2.value().first);
    REQUIRE(rem3_v2);
    REQUIRE(pool.count() == 0);

    // Fill it up again
    auto r1_v3 = pool.create(v1);
    REQUIRE(r1_v3);
    auto r2_v3 = pool.create(v2);
    REQUIRE(r2_v3);
    auto r3_v3 = pool.create(v3);
    REQUIRE(r3_v3);
    auto r4_v3 = pool.create(v4);
    REQUIRE(r4_v3);
    auto r5_v3 = pool.create(v5);
    REQUIRE_FALSE(r5_v3);

    // Check Values
    auto gr1_v3 = pool.get(r1_v3.value().first);
    REQUIRE(gr1_v3);
    REQUIRE(*gr1_v3.value() == v1);

    auto gr2_v3 = pool.get(r2_v3.value().first);
    REQUIRE(gr2_v3);
    REQUIRE(*gr2_v3.value() == v2);

    auto gr3_v3 = pool.get(r3_v3.value().first);
    REQUIRE(gr3_v3);
    REQUIRE(*gr3_v3.value() == v3);

    auto gr4_v3 = pool.get(r4_v3.value().first);
    REQUIRE(gr4_v3);
    REQUIRE(*gr4_v3.value() == v4);

    REQUIRE(pool.count() == 4);

    pool.clear();

    REQUIRE(pool.count() == 0);
}

static u32 k_counter = 0;

class Dummy {
   public:
    Dummy() { k_counter++; }
    Dummy(const Dummy&) { k_counter++; }

    Dummy(Dummy&&) noexcept { k_counter++; }
    ~Dummy() { k_counter--; }
};

TEST_CASE("Test with class", "[FixedHandlePool]") {
    using Pool = FixedHandlePool<Dummy, 4>;
    Pool pool;

    REQUIRE(k_counter == 0);
    auto r1 = pool.create(Dummy());
    REQUIRE(r1);
    REQUIRE(k_counter == 1);
    {
        Dummy d;
        auto r2 = pool.create(d);
        REQUIRE(r2);
        REQUIRE(k_counter == 3);
    }
    REQUIRE(k_counter == 2);
    auto rem1 = pool.erase(r1.value().first);
    REQUIRE(rem1);
    REQUIRE(k_counter == 1);
    pool.clear();
    REQUIRE(k_counter == 0);
}
