/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>

#include <core/allocator/allocator_libc.hpp>
#include <core/allocator/allocator_traits.hpp>
#include <core/allocator/linear_allocator.hpp>
#include <core/containers.hpp>
#include <vector>

#include <catch2/catch.hpp>

using namespace modus;

struct Foo {
    Foo(int z) : y(z) {}

    int y;
};

TEST_CASE("LibC Allocator Compile Check", "[Allocator]") {
    using AllocTraits = AllocatorTraits<AllocatorLibC>;
    void* ptr = AllocTraits::allocate(8);
    REQUIRE(ptr != nullptr);

    void* ptr2 = AllocTraits::realloc(ptr, 20);
    REQUIRE(ptr2 != nullptr);

    Foo* f = reinterpret_cast<Foo*>(ptr2);
    AllocTraits::construct(f, 1024);
    REQUIRE(f != nullptr);
    REQUIRE(f->y == 1024);

    AllocTraits::destroy(f);
    AllocTraits::deallocate(ptr2);
}

TEST_CASE("Override std::vector allocator", "[Allocator]") {
    modus::Vector<int> vector;
    vector.resize(128);
}

TEST_CASE("Override std::string allocator", "[Allocator]") {
    modus::String str("HELLO WORLD THIS IS A LONGER TEXT");
}

TEST_CASE("HashMap allocator", "[Allocator]") {
    modus::HashMap<int, int> map;
    map.insert({20, 30});

    modus::HashSet<int> set;
    set.insert(20);
}

TEST_CASE("Fixed Aligned Linear Allocator", "[Allocator]") {
    AlignedLinearAllocator<16, AllocatorLibC> allocator;
    REQUIRE(allocator.initialize(1024));

    void* ptr = allocator.allocate(25);
    REQUIRE(reinterpret_cast<usize>(ptr) % 16 == 0);
    void* ptr2 = allocator.allocate(1);
    REQUIRE(reinterpret_cast<usize>(ptr2) % 16 == 0);
    void* ptr3 = allocator.allocate(25);
    REQUIRE(reinterpret_cast<usize>(ptr3) % 16 == 0);
    allocator.shutdown();
}
TEST_CASE("Fixed Aligned Linear Allocator (ThreadSafe) ", "[Allocator]") {
    ThreadSafeAlignedLinearAllocator<16, AllocatorLibC> allocator;
    REQUIRE(allocator.initialize(1024));

    void* ptr = allocator.allocate(25);
    REQUIRE(reinterpret_cast<usize>(ptr) % 16 == 0);
    void* ptr2 = allocator.allocate(1);
    REQUIRE(reinterpret_cast<usize>(ptr2) % 16 == 0);
    void* ptr3 = allocator.allocate(25);
    REQUIRE(reinterpret_cast<usize>(ptr3) % 16 == 0);
    allocator.shutdown();
}
