/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/ref_pointer.hpp>

using namespace modus;

TEST_CASE("Basic Test", "[RefPtr]") {
    RefPtr<int> ptr = make_refptr<int>(20);
    CHECK(*ptr == 20);
    CHECK(ptr.ref_count() == 1);
    {
        RefPtr<int> ptr2 = ptr;
        *ptr2 = 10;
        CHECK(ptr.ref_count() == 2);
    }
    CHECK(*ptr == 10);
    CHECK(ptr.ref_count() == 1);
    {
        WeakRefPtr<int> w = ptr;
        CHECK(ptr.ref_count() == 1);
        CHECK(w.ref_count() == 1);
        RefPtr<int> ptr3 = w;
        CHECK(ptr.ref_count() == 2);
        *ptr3 = 30;
    }
    CHECK(*ptr == 30);
    CHECK(ptr.ref_count() == 1);
}

TEST_CASE("Weak to Shared Test", "[RefPtr]") {
    WeakRefPtr<int> wptr;
    {
        RefPtr<int> ptr = make_refptr<int>(20);
        wptr = ptr;
        RefPtr<int> ptr2 = wptr;
        REQUIRE(ptr2);
        CHECK(*ptr2 == 20);
    }
    RefPtr<int> ptr2 = wptr;
    CHECK_FALSE(ptr2);
}

struct Foo {
    virtual ~Foo() = default;
};

struct Bar final : public Foo {};

TEST_CASE("Inheritance Casting", "[RefPtr]") {
    {
        RefPtr<Bar> bar_ptr = make_refptr<Bar>();
        RefPtr<Foo> foo_ptr = refptr_cast<Foo>(bar_ptr);
        CHECK(bar_ptr.ref_count() == 2);
    }
    {
        RefPtr<Bar> bar_ptr = make_refptr<Bar>();
        RefPtr<Foo> foo_ptr = bar_ptr;
        CHECK(bar_ptr.ref_count() == 2);
    }
}

TEST_CASE("Inheritance Construction", "[RefPtr]") {
    RefPtr<Foo> foo_ptr = make_refptr<Bar>();
    CHECK(foo_ptr.ref_count() == 1);
}

TEST_CASE("Container Construction", "[RefPtr]") {
    Vector<RefPtr<Foo>> vec;
    vec.reserve(20);
}
