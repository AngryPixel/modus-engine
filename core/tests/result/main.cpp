/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/core.hpp>
#include <core/result.hpp>
#include <memory>

enum class Error { Error1, Error2, Error3 };

using Result = modus::Result<int, Error>;

static inline Result get_error() {
    return modus::Error(Error::Error2);
}

TEST_CASE("Error type return", "[Result]") {
    Result r = get_error();
    REQUIRE(r.is_error());
    REQUIRE_FALSE(r.is_value());
    REQUIRE(Error::Error2 == r.error());
}

static inline Result get_value() {
    return modus::Ok(4024);
}

TEST_CASE("Value type return", "[Result]") {
    Result r = get_value();
    REQUIRE_FALSE(r.is_error());
    REQUIRE(r.is_value());
    REQUIRE(4024 == r.value());
    REQUIRE(4024 == *r);
}

TEST_CASE("Empty value return", "[Result]") {
    modus::Result<void, Error> r = modus::Ok<>();
    REQUIRE_FALSE(r.is_error());
    REQUIRE(r.is_value());
}

TEST_CASE("Empty error return", "[Result]") {
    modus::Result<int, void> r = modus::Error<>();
    REQUIRE(r.is_error());
    REQUIRE_FALSE(r.is_value());
}

TEST_CASE("Empty error and value return", "[Result]") {
    modus::Result<> r = modus::Error<>();
    REQUIRE(r.is_error());
    REQUIRE_FALSE(r.is_value());
}

#if defined(_MSC_VER)
#define NO_INLINE_PRE __declspec(noinline)
#define NO_INLINE_ATTR
#else
#define NO_INLINE_PRE
#define NO_INLINE_ATTR __attribute__((noinline))
#endif

using PtrResult = modus::Result<std::unique_ptr<int>, void>;

NO_INLINE_PRE static PtrResult NO_INLINE_ATTR get_ptr_result() {
    return modus::Ok(std::make_unique<int>(20));
}

TEST_CASE("unique_ptr result", "[Result]") {
    auto r = get_ptr_result();
    REQUIRE(r.is_value());
    REQUIRE(*(r.value()) == 20);
}

struct Foo {
    int bar;
};

using StructResult = modus::Result<Foo, void>;

TEST_CASE("Struct Result operator ->", "[Result]") {
    StructResult r = modus::Ok(Foo{20});
    REQUIRE(r->bar == 20);
}
