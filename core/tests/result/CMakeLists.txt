#
# Result tests
#

modus_add_test(test_result NAME Result)

target_sources(test_result PRIVATE
    main.cpp
)

target_link_libraries(test_result PRIVATE core)
