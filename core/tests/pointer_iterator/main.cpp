/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#define MODUS_POINTER_ITERATOR_STL_COMPATABILITY
#include <algorithm>
#include <catch2/catch.hpp>
#include <core/slice.hpp>

using namespace modus;

TEST_CASE("Basic Iterator behaviour", "[PointerIterator]") {
    const i32 array[4] = {0, 1, 3, 4};

    auto it = pointer_iterator_begin(array);
    REQUIRE(it != pointer_iterator_end(array));
    REQUIRE(0 == *it);

    const auto it_postfix = it++;
    REQUIRE(0 == *it_postfix);
    REQUIRE(1 == *it);

    const auto it_prefix = ++it;
    REQUIRE(3 == *it_prefix);
    REQUIRE(3 == *it);

    ++it;
    REQUIRE(4 == *it);

    ++it;
    REQUIRE(it == pointer_iterator_end(array));
}

TEST_CASE("Move from end -1 to begin", "[PointerIterator]") {
    const i32 array[4] = {0, 1, 3, 4};

    auto it = pointer_iterator_begin(array);
    it += 3;
    REQUIRE(it != pointer_iterator_end(array));
    REQUIRE(4 == *it);

    const auto it_postfix = it--;
    REQUIRE(4 == *it_postfix);
    REQUIRE(3 == *it);

    const auto it_prefix = --it;
    REQUIRE(1 == *it_prefix);
    REQUIRE(1 == *it);

    --it;
    REQUIRE(0 == *it);
    REQUIRE(it == pointer_iterator_begin(array));
}

TEST_CASE("Iterator Diff", "[PointerIterator]") {
    const i32 array[4] = {0, 1, 3, 4};
    auto it = pointer_iterator_begin(array);
    auto it2 = it + 3;
    auto diff = it2 - it;
    REQUIRE(diff == 3);
}

TEST_CASE("std::sort even", "[PointerIterator]") {
    i32 array[4] = {0, 8, 1, 10};
    std::sort(pointer_iterator_begin(array), pointer_iterator_end(array));
    REQUIRE(array[0] == 0);
    REQUIRE(array[1] == 1);
    REQUIRE(array[2] == 8);
    REQUIRE(array[3] == 10);
}

TEST_CASE("std::sort odd", "[PointerIterator]") {
    i32 array[5] = {0, 8, -1, 1, 10};
    std::sort(pointer_iterator_begin(array), pointer_iterator_end(array));
    REQUIRE(array[0] == -1);
    REQUIRE(array[1] == 0);
    REQUIRE(array[2] == 1);
    REQUIRE(array[3] == 8);
    REQUIRE(array[4] == 10);
}
