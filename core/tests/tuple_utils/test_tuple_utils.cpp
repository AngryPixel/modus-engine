/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>

#include <catch2/catch.hpp>
#include <core/tuple_utils.hpp>

using namespace modus;
using TupleType = std::tuple<int, float, double>;
using MaskType = TupleTypeMask<int, float, double>;

TEST_CASE("slice from fixed array with make_slice", "[TupleTypesMask]") {
    constexpr usize int_index = 0;
    constexpr usize float_index = 1;
    constexpr usize double_index = 2;
    MaskType mask;

    REQUIRE(TupleTypeIndex<int, TupleType>::value == int_index);
    REQUIRE(TupleTypeIndex<float, TupleType>::value == float_index);
    REQUIRE(TupleTypeIndex<double, TupleType>::value == double_index);

    mask.mark<int>();
    REQUIRE(mask == MaskType::make_mask<int>());
    REQUIRE(mask.is_set<int>());
    REQUIRE(mask.is_set(int_index));
    REQUIRE_FALSE(mask.is_set<float>());
    REQUIRE_FALSE(mask.is_set<double>());
    REQUIRE_FALSE(mask.is_set(float_index));
    REQUIRE_FALSE(mask.is_set(double_index));

    mask.mark<float>();
    REQUIRE(mask.is_set<int>());
    REQUIRE(mask.is_set(int_index));
    REQUIRE(mask.is_set<float>());
    REQUIRE(mask.is_set(float_index));

    mask.unmark<int>();
    REQUIRE_FALSE(mask == MaskType::make_mask<int>());
    REQUIRE_FALSE(mask.is_set<int>());
    REQUIRE_FALSE(mask.is_set(int_index));
    REQUIRE(mask.is_set<float>());
    REQUIRE(mask.is_set(float_index));
}
