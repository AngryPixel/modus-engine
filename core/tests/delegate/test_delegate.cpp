/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/containers.hpp>
#include <core/delegate.hpp>
#include <core/fixed_function.hpp>
#include <iostream>

using namespace modus;

struct Foo {
    int value = 0;

    void sum(int other) { value += other; }

    int sum_return(int other) const { return value + other; }
};
struct Foo2 {
    int* t;
    Foo2(int i) : t(new int(i)) { std::cout << "Foo2 new\n"; }
    ~Foo2() {
        if (t != nullptr) {
            std::cout << "Foo2 delete\n";
            delete t;
        }
    }
    Foo2(const Foo2&) = delete;
    Foo2& operator=(const Foo2&) = delete;

    Foo2(Foo2&& rhs) noexcept : t(rhs.t) { rhs.t = nullptr; }
    Foo2& operator=(Foo2&& rhs) noexcept {
        if (this != &rhs) {
            if (t != nullptr) {
                delete t;
            }
            t = rhs.t;
            rhs.t = nullptr;
        }
        return *this;
    }
};

/*
using FooCallbackBuilder = DelegateBuilder<void, int>;
using FooCallbackBuilder2 = DelegateBuilder<int, int>;

TEST_CASE("Basic Test", "[Delegate]") {
    Foo f;

    // Delegate<void(int)> d = make_delegate_instance<void, Foo, int,
    // &Foo::sum>(f);
    Delegate<void(int)> d = FooCallbackBuilder::build<Foo, &Foo::sum>(f);
    d(30);
    REQUIRE(f.value == 30);

    Delegate<int(int)> d2 = FooCallbackBuilder2::build<Foo, &Foo::sum_return>(f);
    REQUIRE(d2(30) == 60);
}
*/

void sum_standalone(int& v) {
    v += 40;
}

TEST_CASE("Basic Test2", "[Delegate]") {
    Foo f;

    // Delegate<void(int)> d = make_delegate_instance<void, Foo, int,
    // &Foo::sum>(f);
    Delegate<void(int)> d;
    d.bind<Foo, &Foo::sum>(&f);
    d(30);
    REQUIRE(f.value == 30);

    Delegate<int(int)> d2;
    d2.bind<Foo, &Foo::sum_return>(&f);
    REQUIRE(d2(30) == 60);

    Delegate<void(int&)> d3;
    d3.bind<sum_standalone>();
    int s = 0;
    d3(s);
    REQUIRE(s == 40);
}

TEST_CASE("Fixed Function", "[FixedFunction]") {
    FixedFunction<8, void(int&)> func([](int& i) { i = 30; });
    int i = 60;
    func(i);
    CHECK(i == 30);
}

static void test_fn() {
    std::cout << "THREE\n";
}
TEST_CASE("Fixed Function move operator", "[FixedFunction]") {
    using FType = FixedFunction<8, void()>;
    Vector<FType> vec;

    vec.emplace_back(FType([]() { std::cout << "ONE\n"; }));
    vec.emplace_back([]() { std::cout << "TWO\n"; });
    vec.emplace_back(test_fn);

    Foo2 foo(20);
    FType f;
    f = [local_foo = std::move(foo)]() { std::cout << fmt::format("FOUR: {}\n", *local_foo.t); };
    vec.push_back(std::move(f));
    for (auto& fn : vec) {
        fn();
    }
}
