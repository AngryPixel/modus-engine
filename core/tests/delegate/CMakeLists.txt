#
# Delegate test
#

modus_add_test(test_delegate NAME Delegate)

target_sources(test_delegate PRIVATE
    test_delegate.cpp
)

target_link_libraries(test_delegate PRIVATE core)
