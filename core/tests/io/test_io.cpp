/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/io/memory_stream.hpp>
#include <core/string_slice.hpp>

using namespace modus;

TEST_CASE("Read from SliceStream", "[IO]") {
    const StringSlice str_slice = "Hello World";
    u8 buffer[5];
    SliceMut<u8> buffer_slice(buffer);
    SliceStream stream(str_slice.as_bytes());

    auto result = stream.read(buffer_slice);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 5);

    result = stream.read(buffer_slice);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 5);

    result = stream.read(buffer_slice);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 1);

    result = stream.read(buffer_slice);
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 0);

    result = stream.write(buffer_slice);
    REQUIRE(result.is_error());
    REQUIRE(result.error() == IOError::ReadOnly);
}

TEST_CASE("Write to SliceStream", "[IO]") {
    i8 output[11];
    SliceMut<i8> output_slice(output);
    SliceStream stream(output_slice);

    auto result = stream.write(StringSlice("hello").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 5);

    result = stream.write(StringSlice(" Worl").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 5);

    result = stream.write(StringSlice("d!!").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 1);

    result = stream.write(StringSlice("!!").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 0);
}

TEST_CASE("Write to RamStream", "[IO]") {
    i8 output[256];
    SliceMut<i8> buffer_slice(output);

    RamStream stream;

    auto result = stream.write(StringSlice("hello").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 5);

    result = stream.write(StringSlice(" Worl").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 5);

    result = stream.write(StringSlice("d!!").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 3);

    result = stream.write(StringSlice("!!").as_bytes());
    REQUIRE(result.is_value());
    REQUIRE(result.value() == 2);

    auto seek_result = stream.seek(0);
    REQUIRE(seek_result.is_value());

    const StringSlice expected = "hello World!!!!";
    IByteReader& byte_reader = stream;
    result = byte_reader.read(buffer_slice);
    REQUIRE(result.is_value());
    REQUIRE(result.value() >= expected.size());
    const auto sub_slice = buffer_slice.sub_slice(0, expected.size());
    REQUIRE(sub_slice.as_bytes() == expected.as_bytes());
}
