/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <iostream>

#include <catch2/catch.hpp>
#include <core/getopt.hpp>

using namespace modus;

TEST_CASE("Option bool", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionBool opt_bool("-b", "--bool", "bool value", false);
    CmdOptionBool opt_bool2("-c", "--bool2", "bool value 2", false);
    REQUIRE(parser.add(opt_bool));
    REQUIRE(parser.add(opt_bool2));
    const char* options[] = {"f", "-b", "true", "--bool2", "false"};
    auto result = parser.parse(5, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt_bool.value() == true);
    REQUIRE(opt_bool.parsed());
    REQUIRE(opt_bool2.value() == false);
    REQUIRE(opt_bool2.parsed());
}

TEST_CASE("Option Empty", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionEmpty opt_empty("-e", "--empty", "Empty Option");
    REQUIRE(parser.add(opt_empty));
    const char* options[] = {"f", "-e"};
    auto result = parser.parse(2, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt_empty.parsed());
}

TEST_CASE("Option String", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionString opt_str("-s", "--str", "String Option", "");
    REQUIRE(parser.add(opt_str));
    const char* options[] = {"f", "-s", "true"};
    auto result = parser.parse(3, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt_str.value() == "true");
    REQUIRE(opt_str.parsed());
}

TEST_CASE("Option F32", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionF32 opt("-f", "--float", "F32 Option", 2.0f);
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "-f", "1.0"};
    auto result = parser.parse(3, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt.value() >= 1.0f);
    REQUIRE(opt.parsed());
}

TEST_CASE("Option I32", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionI32 opt("-i", "--i32", "I32 Option", 4);
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "-i", "-1024"};
    auto result = parser.parse(3, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt.value() == -1024);
    REQUIRE(opt.parsed());
}

TEST_CASE("Option U32", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionU32 opt("-u", "--u32", "u32 Option", 4);
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "--u32", "1024"};
    auto result = parser.parse(3, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt.value() == 1024u);
    REQUIRE(opt.parsed());
}

TEST_CASE("Remaining Args", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionU32 opt("-u", "--u32", "u32 Option", 4);
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "--u32", "1024", "foo", "bar"};
    auto result = parser.parse(5, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt.value() == 1024u);
    REQUIRE(opt.parsed());
    REQUIRE(result->size() == 2);
    REQUIRE(StringSlice(result->at(0)) == "foo");
    REQUIRE(StringSlice(result->at(1)) == "bar");
}

TEST_CASE("Option StringMap", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionStringMap opt("-m", "--map", "StringMap Option");
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "--map", "foo:1024", "bar:204"};
    auto result = parser.parse(4, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    CHECK(opt.parsed());
    CHECK(result->size() == 0);
    const auto& map = opt.value();
    auto foo_it = map.find("foo");
    REQUIRE(foo_it != map.end());
    CHECK(foo_it->second == "1024");
    auto bar_it = map.find("bar");
    REQUIRE(bar_it != map.end());
    CHECK(bar_it->second == "204");
}

TEST_CASE("Option StringMap Invalid KeyValue", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionStringMap opt("-m", "--map", "StringMap Option");
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "--map", "foo1024"};
    auto result = parser.parse(3, options);
    REQUIRE(!result);
}

TEST_CASE("Option StringMap Detect map end", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionStringMap opt("-m", "--map", "StringMap Option");
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "--map", "foo:1024", "bar"};
    auto result = parser.parse(4, options);
    REQUIRE(result);
    REQUIRE(result->size() == 1);
    CHECK(StringSlice((*result)[0]) == "bar");
}

TEST_CASE("Optional options", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionU32 opt("-u", "--u32", "u32 Option", 4);
    CmdOptionEmpty opt_empty("-e", "--empty", "empty");
    REQUIRE(parser.add(opt));
    REQUIRE(parser.add(opt_empty));
    const char* options[] = {"f", "--u32", "1024", "foo", "bar"};
    auto result = parser.parse(5, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt.parsed());
    REQUIRE_FALSE(opt_empty.parsed());
    REQUIRE(result->size() == 2);
}

TEST_CASE("No Remaing Args", "[GetOpts]") {
    CmdOptionParser parser;
    CmdOptionU32 opt("-u", "--u32", "u32 Option", 4);
    REQUIRE(parser.add(opt));
    const char* options[] = {"f", "--u32", "1024"};
    auto result = parser.parse(3, options);
    if (!result) {
        std::cout << result.error().c_str() << std::endl;
    }
    REQUIRE(result);
    REQUIRE(opt.parsed());
    REQUIRE(result->size() == 0);
}
