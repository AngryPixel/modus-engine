/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>

#include <core/allocator/allocator_libc.hpp>
#include <core/allocator/allocator_traits.hpp>
#include <core/allocator/pool_allocator.hpp>

#include <catch2/catch.hpp>

using namespace modus;

struct Foo {
    Foo(u32 z) : y(z) {}

    u32 y;
};

TEST_CASE("Basic Compile Check", "[PoolAllocator]") {
    PoolAllocatorTyped<Foo, AllocatorLibC> allocator(4);

    constexpr u32 iterations = 16;
    Foo* ptrs[iterations];
    for (u32 i = 0; i < iterations; ++i) {
        Foo* f = allocator.construct(i);
        REQUIRE(f != nullptr);
        REQUIRE(f->y == i);
        ptrs[i] = f;
    }

    for (u32 i = 0; i < iterations; ++i) {
        allocator.destroy(ptrs[i]);
    }
}

struct alignas(32) Bar {
    Bar(u32 z) : y(z) {}

    u32 y;
};

TEST_CASE("Basic Compile Check Aligned", "[PoolAllocator]") {
    PoolAllocatorTyped<Bar, AllocatorLibC> allocator(4);

    constexpr u32 iterations = 16;
    Bar* ptrs[iterations];
    for (u32 i = 0; i < iterations; ++i) {
        Bar* f = allocator.construct(i);
        REQUIRE(f != nullptr);
        REQUIRE(f->y == i);
        ptrs[i] = f;
    }

    for (u32 i = 0; i < iterations; ++i) {
        allocator.destroy(ptrs[i]);
    }
}
