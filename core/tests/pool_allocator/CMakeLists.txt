#
# Pool Allocator tests
#

modus_add_test(test_pool_allocator NAME PoolAllocator)

target_sources(test_pool_allocator PRIVATE
    test_pool_allocator.cpp
)

target_link_libraries(test_pool_allocator PRIVATE core)
