/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <catch2/catch.hpp>
#include <core/guid.hpp>
#include <core/hex.hpp>
#include <core/string_slice.hpp>
using namespace modus;

// These test only check if the code is compilable

TEST_CASE("Format from StringSlice", "[String]") {
    const char* cstr = "hello";
    const char* expected = "hello world!";
    auto string = fmt::format("{} world!", StringSlice(cstr));
    REQUIRE(string == expected);
}

TEST_CASE("Format from String::format", "[String]") {
    Result<int, void> r1 = Error<>();
    Result<int, void> r2 = Ok(10);
    Result<void, int> r3 = Error(20);
    auto string = fmt::format("{}:{}:{}", r1, r2, r3);
    const char* expected = "Error():Ok(10):Error(20)";
    REQUIRE(string == expected);
}

TEST_CASE("Test GUID from str", "[GUID]") {
    const char* guid = "ce4de0d5-bbce-4bc8-a01a-16f4e0f69c0a";
    auto guid_result = modus::GID::from_string(guid);
    REQUIRE(guid_result);

    Array<u8, 16> expected_bytes;
    expected_bytes[0] = hex_to_byte('c', 'e').value_or_panic();
    expected_bytes[1] = hex_to_byte('4', 'd').value_or_panic();
    expected_bytes[2] = hex_to_byte('e', '0').value_or_panic();
    expected_bytes[3] = hex_to_byte('d', '5').value_or_panic();
    expected_bytes[4] = hex_to_byte('b', 'b').value_or_panic();
    expected_bytes[5] = hex_to_byte('c', 'e').value_or_panic();
    expected_bytes[6] = hex_to_byte('4', 'b').value_or_panic();
    expected_bytes[7] = hex_to_byte('c', '8').value_or_panic();
    expected_bytes[8] = hex_to_byte('a', '0').value_or_panic();
    expected_bytes[9] = hex_to_byte('1', 'a').value_or_panic();
    expected_bytes[10] = hex_to_byte('1', '6').value_or_panic();
    expected_bytes[11] = hex_to_byte('f', '4').value_or_panic();
    expected_bytes[12] = hex_to_byte('e', '0').value_or_panic();
    expected_bytes[13] = hex_to_byte('f', '6').value_or_panic();
    expected_bytes[14] = hex_to_byte('9', 'c').value_or_panic();
    expected_bytes[15] = hex_to_byte('0', 'a').value_or_panic();

    const modus::GID expected(expected_bytes);
    REQUIRE(expected == guid_result.value());
}
