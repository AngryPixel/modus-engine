/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/animation/animator_manager.hpp>
#include <engine/animation/skeleton_animator.hpp>
#include <engine/engine.hpp>

namespace modus::engine::animation {
static constexpr u32 kAnimatorsPerBlock = 32;
AnimatorManager::AnimatorManager(AnimationCatalogDB& anim_catalog_db)
    : m_anim_catalog_db(anim_catalog_db),
      m_animator_pool(kAnimatorsPerBlock),
      m_handles(kAnimatorsPerBlock),
      m_active_count(0),
      m_size(0) {}

AnimatorManager::~AnimatorManager() {}

Result<> AnimatorManager::initialize() {
    return Ok<>();
}

void AnimatorManager::shutdown() {
    clear();
}

void AnimatorManager::update(Engine& engine) {
    MODUS_PROFILE_ANIM_BLOCK("AnimatorManager::Update");
    if (m_active_count == 0) {
        return;
    }

    m_size = m_active_count = update_internal();
    const FSeconds tick_sec = engine.tick_sec();
    for (u32 i = 0; i < m_active_count; ++i) {
        MODUS_PROFILE_ANIM_BLOCK("Animation");
        AnimatorData& anim_data = m_animators[i];
        modus_assert(anim_data.ptr);
        anim_data.ptr->update(engine, tick_sec);
    }
}

Result<AnimatorHandle, void> AnimatorManager::create(const AnimationCatalogHandle handle) {
    if (!grow_or_expand(m_active_count + 1)) {
        return Error<>();
    }

    auto r_catalog = m_anim_catalog_db.get(handle);
    if (!r_catalog) {
        return Error<>();
    }

    const u32 index = m_active_count++;
    AnimatorData& animator = m_animators[index];
    auto r_handle = m_handles.create(index);
    if (!r_handle) {
        return Error<>();
    }

    auto ptr = m_animator_pool.construct(*r_catalog);
    if (ptr == nullptr) {
        (void)m_handles.erase(r_handle->first);
        return Error<>();
    }

    animator.handle = r_handle->first;
    animator.alive = true;
    animator.ptr = ptr;
    return Ok(animator.handle);
}

void AnimatorManager::destroy(const AnimatorHandle handle) {
    auto r_index = m_handles.get(handle);
    if (r_index) {
        m_animators[**r_index].alive = false;
    }
}

void AnimatorManager::clear() {
    for (auto& animator : m_animators) {
        if (animator.ptr) {
            m_animator_pool.destroy(animator.ptr.get());
        }
    }
    m_animators.clear();
    m_handles.clear();
    m_active_count = 0;
    m_size = 0;
}

Result<> AnimatorManager::play(const AnimatorHandle animator, const PlayAnimationParams& params) {
    auto r_animator = m_handles.get(animator);
    if (!r_animator) {
        return Error<>();
    }

    const SkeletonAnimatorParams animation_params = {params.animation_name, params.speed,
                                                     params.loop, params.on_animation_completed};

    return m_animators[**r_animator].ptr->play(animation_params);
}

Result<> AnimatorManager::set_animation_speed(const AnimatorHandle handle,
                                              const StringSlice name,
                                              const f32 animation_speed) {
    auto r_animator = m_handles.get(handle);
    if (!r_animator) {
        return Error<>();
    }

    m_animators[**r_animator].ptr->set_animation_speed(name, animation_speed);
    return Ok<>();
}

Result<NotMyPtr<const Skeleton>, void> AnimatorManager::animation_skeleton(
    const AnimatorHandle handle) const {
    auto r_animator = m_handles.get(handle);
    if (!r_animator) {
        return Error<>();
    }

    auto skeleton = m_animators[**r_animator].ptr->skeleton();
    if (!skeleton) {
        return Error<>();
    }
    return Ok(skeleton);
}

Result<Slice<glm::mat4>, void> AnimatorManager::animation_data(
    const AnimatorHandle animator) const {
    auto r_animator = m_handles.get(animator);
    if (!r_animator) {
        return Error<>();
    }

    return Ok<Slice<glm::mat4>>(m_animators[**r_animator].ptr->animation_data());
}

Result<> AnimatorManager::grow_or_expand(const usize expected) {
    if (expected < m_animators.size()) {
        return Ok<>();
    }

    const usize current_size = m_animators.size();
    const usize new_size = expected * 2;
    m_animators.resize(new_size);

    for (usize i = current_size; i < new_size; ++i) {
        m_animators[i].alive = false;
        m_animators[i].handle = AnimatorHandle();
        m_animators[i].ptr.reset();
    }

    return Ok<>();
}

void AnimatorManager::on_destroy(AnimatorData& animator) {
    if (!m_handles.erase(animator.handle)) {
        MODUS_LOGE("AnimatorManager: Failed to destroy handle");
    }
    m_animator_pool.destroy(animator.ptr.get());
    animator.ptr.reset();
    animator.handle = AnimatorHandle();
}

u32 AnimatorManager::update_internal() {
    u32 index_dead = 0;
    u32 index_alive = m_active_count - 1;

    while (true) {
        for (; true; ++index_dead) {
            if (index_dead > index_alive) {
                return index_dead;
            }
            if (!m_animators[index_dead].alive) {
                break;
            }
        }

        for (; true; --index_alive) {
            if (m_animators[index_alive].alive) {
                break;
            }
            // Erase dead items that can't be moved
            AnimatorData& anim_data = m_animators[index_alive];
            on_destroy(anim_data);

            if (index_alive <= index_dead) {
                return index_dead;
            }
        }

        // Swap dead for alive
        AnimatorData& anim_alive = m_animators[index_alive];
        AnimatorData& anim_dead = m_animators[index_dead];
        modus_assert(anim_alive.alive);
        modus_assert(!anim_dead.alive);
        NotMyPtr<u32> anim_alive_index =
            m_handles.get(anim_alive.handle).value_or_panic("Failed to get alive entity handle\n");
        on_destroy(anim_dead);
        *anim_alive_index = index_dead;
        std::swap(anim_alive, anim_dead);

        ++index_dead;
        --index_alive;
    }

    return index_dead;
}

}    // namespace modus::engine::animation
