/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/animation/skeleton_animator.hpp>
#include <engine/animation/skeleton.hpp>
#include <engine/animation/animation_catalog.hpp>
#include <engine/animation/skeleton_animation.hpp>

namespace modus::engine::animation {

SkeletonAnimator::SkeletonAnimator(NotMyPtr<const AnimationCatalog> catalog) {
    modus_assert(catalog);
    m_catalog = catalog;
}

Result<> SkeletonAnimator::play(const SkeletonAnimatorParams& params) {
    Instance* instance = nullptr;
    for (auto& i : m_instances) {
        if (!i.m_animation) {
            instance = &i;
            break;
        }
    }

    if (instance == nullptr) {
        MODUS_LOGE("SkeletonAnimator: No more available animation slots");
        return Error<>();
    }

    auto r_animation = m_catalog->animation(params.animation_name);
    if (!r_animation) {
        return Error<>();
    }

    auto r_skeleton = m_catalog->skeleton((*r_animation)->skeleton_index());
    if (!r_skeleton) {
        return Error<>();
    }

    if (!m_skeleton) {
        m_skeleton = *r_skeleton;
    } else if (m_skeleton != *r_skeleton) {
        MODUS_LOGE("SkeletonAnimator: Skeletons don't match");
        return Error<>();
    }

    m_animation_data.fill(glm::mat4(1.0f));
    instance->m_keyframe_cache.fill(0);
    instance->m_elapsed_time = FSeconds(0.f);
    instance->m_animation = *r_animation;
    instance->m_animation_speed = params.animation_speed;
    instance->m_loop = params.loop;
    instance->m_pause = false;
    instance->m_on_animation_completed = params.on_animation_completed;
    return Ok<>();
}

static f32 calculate_progression(const FSeconds animation_time,
                                 const BoneTransform& current,
                                 const BoneTransform& next) {
    const FSeconds total_time = next.m_timestamp - current.m_timestamp;
    const FSeconds current_time = animation_time - current.m_timestamp;
    return current_time.count() / total_time.count();
}

static glm::mat4 interpolate(const f32 progression,
                             const BoneTransform& current,
                             const BoneTransform& next) {
    math::Transform t = math::Transform::lerp(current.m_transform, next.m_transform, progression);
    return t.to_matrix();
}

struct KeyframeLookupResult {
    u32 prev;
    u32 next;
};

static KeyframeLookupResult find_next_bone_keyframe_transform(const Slice<BoneTransform> transforms,
                                                              const FSeconds elasped_time_sec,
                                                              const u32 start_key_frame) {
    u32 prev = start_key_frame;
    u32 next = start_key_frame;
    for (u32 i = start_key_frame; i < transforms.size(); ++i) {
        next = i;
        if (transforms[i].m_timestamp > elasped_time_sec) {
            break;
        }
        prev = i;
    }
    return KeyframeLookupResult{prev, next};
}

void SkeletonAnimator::update(Engine& engine, const FSeconds elapsed_time_sec) {
    MODUS_PROFILE_ANIM_BLOCK("SkeletonAnimator::update");
    const Slice<SkeletonBone> bones = m_skeleton->bones();

    bool has_animation = false;
    for (auto& instance : m_instances) {
        if (instance.m_animation && !instance.m_pause) {
            has_animation = true;
            break;
        }
    }

    if (has_animation) {
        // If the bones have no animation, we need to assign them their original
        // transform value or they will not be transformed correctly.
        for (u32 i = 0; i < bones.size(); ++i) {
            m_animation_data[i] = bones[i].m_bone_transform.to_matrix();
        }
    }

    for (auto& instance : m_instances) {
        if (!instance.m_animation) {
            continue;
        }
        if (instance.m_pause) {
            continue;
        }
        instance.m_elapsed_time += elapsed_time_sec * instance.m_animation_speed;
        if (instance.m_elapsed_time > instance.m_animation->duration_sec()) {
            if (instance.m_on_animation_completed) {
                instance.m_on_animation_completed(engine);
            }
            if (!instance.m_loop) {
                instance.m_animation.reset();
                instance.m_on_animation_completed = AnimationFinishedDelegate();
                continue;
            }
            instance.m_elapsed_time -= instance.m_animation->duration_sec();
            instance.m_keyframe_cache.fill(0);
        }

        // Update key frames
        const Slice<BoneKeyframe> keyframes = instance.m_animation->bone_keyframes();
        for (u32 i = 0; i < keyframes.size(); ++i) {
            const BoneKeyframe& bkf = keyframes[i];
            const Slice<BoneTransform> transforms = instance.m_animation->bone_transforms(bkf);
            const KeyframeLookupResult keyframe_result = find_next_bone_keyframe_transform(
                transforms, instance.m_elapsed_time, instance.m_keyframe_cache[bkf.m_bone_id]);
            instance.m_keyframe_cache[bkf.m_bone_id] = keyframe_result.prev;
            const BoneTransform& prev_transform = transforms[keyframe_result.prev];
            const BoneTransform& next_transform = transforms[keyframe_result.next];
            const f32 progression =
                calculate_progression(instance.m_elapsed_time, prev_transform, next_transform);
            m_animation_data[bkf.m_bone_id.value()] =
                interpolate(progression, prev_transform, next_transform);
        }
    }

    // Progagate hierarchy changes
    for (u32 i = 0; i < bones.size(); ++i) {
        const SkeletonBone& cur_bone = bones[i];
        // Apply bone transform
        if (cur_bone.m_parent_index) {
            m_animation_data[i] =
                m_animation_data[cur_bone.m_parent_index.value()] * m_animation_data[i];
        }
    }
    for (u32 i = 0; i < bones.size(); ++i) {
        const SkeletonBone& cur_bone = bones[i];
        // Apply inverted transform to convert to model space
        m_animation_data[i] *= cur_bone.m_bind_transform_inv.to_matrix();
    }
}

void SkeletonAnimator::set_animation_speed(const StringSlice name, const f32 speed) {
    auto r_instance = instance_by_name(name);
    if (r_instance) {
        modus_assert(speed > 0.0f);
        (*r_instance)->m_animation_speed = speed;
    }
}

Result<SkeletonAnimator::Instance*, void> SkeletonAnimator::instance_by_name(
    const StringSlice& name) {
    for (auto& i : m_instances) {
        if (i.m_animation && i.m_animation->name() == name) {
            return Ok(&i);
        }
    }
    return Error<>();
}
}    // namespace modus::engine::animation
