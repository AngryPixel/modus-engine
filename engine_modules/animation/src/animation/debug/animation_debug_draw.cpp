/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/animation/debug/animation_debug_draw.hpp>
#include <engine/animation/skeleton.hpp>

namespace modus::engine::animation {
static const glm::vec4 kBoneColors[] = {
    {1.f, 0.f, 0.f, 1.f},   {0.f, 1.f, 0.f, 1.f},   {0.f, 0.f, 1.f, 1.f},   {1.f, 0.f, 1.f, 1.f},
    {0.f, 1.f, 1.f, 1.f},   {1.f, 1.f, 0.f, 1.f},   {0.5f, 0.f, 0.f, 1.f},  {0.f, 0.5f, 0.f, 1.f},
    {0.f, 0.f, 0.5f, 1.f},  {0.5f, 0.f, 0.5f, 1.f}, {0.f, 0.5f, 0.5f, 1.f}, {0.5f, 0.5f, 0.f, 1.f},
    {0.7f, 0.f, 0.f, 1.f},  {0.f, 0.7f, 0.f, 1.f},  {0.f, 0.f, 0.7f, 1.f},  {0.7f, 0.f, 0.7f, 1.f},
    {0.f, 0.7f, 0.7f, 1.f}, {0.7f, 0.7f, 0.f, 1.f}, {0.4f, 0.f, 0.f, 1.f},  {0.f, 0.4f, 0.f, 1.f},
    {0.f, 0.f, 0.4f, 1.f},  {0.4f, 0.f, 0.4f, 1.f}, {0.f, 0.4f, 0.4f, 1.f}, {0.4f, 0.4f, 0.f, 1.f},
};

void DebugDrawer::draw_skeleton(const glm::mat4& world_transform,
                                const Skeleton& skeleton,
                                Optional<Slice<glm::mat4>> opt_bone_transforms) {
    const auto skbones = skeleton.bones();
    glm::vec3 points[kMaxSkeletonBones];
    if (opt_bone_transforms) {
        const Slice<glm::mat4> bone_transforms = *opt_bone_transforms;
        // Fill points with default values first
        if (skbones.size() != bone_transforms.size()) {
            MODUS_LOGE(
                "draw_skeleton: Can't debug draw bones if bone_transforms do "
                "not have the same number of bones as the skeleton");
        }

        for (u32 i = 0; i < bone_transforms.size() && i < skbones.size(); ++i) {
            const auto& m = bone_transforms[i];
            points[i] = (world_transform * m) *
                        skbones[i].m_bind_transform_inv.inverted().to_matrix() *
                        glm::vec4(0.f, 0.f, 0.f, 1.0);
            draw_sphere(points[i], 0.2f - (0.01f * f32(i)), kBoneColors[i]);
            const auto& b = skbones[i];
            if (b.m_parent_index) {
                draw_line(points[b.m_parent_index.value()], points[i], glm::vec4(1.0f));
            }
        }

    } else {
        // Draw regular skeleton
        for (u32 i = 0; i < skbones.size(); ++i) {
            const auto& b = skbones[i];
            points[i] =
                (world_transform * b.m_bind_transform.to_matrix()) * glm::vec4(0.f, 0.f, 0.f, 1.0f);
            draw_sphere(points[i], 0.2f - (0.01f * f32(i)), kBoneColors[i]);
            if (b.m_parent_index) {
                draw_line(points[b.m_parent_index.value()], points[i], glm::vec4(1.0f));
            }
        }
    }

    for (u32 i = 0; i < skbones.size(); ++i) {
        draw_text3d(skeleton.bone_name(i).value_or_panic("Failed to get skeleton bone"), points[i],
                    1.0f, glm::vec3(0.9f));
    }
}

}    // namespace modus::engine::animation
