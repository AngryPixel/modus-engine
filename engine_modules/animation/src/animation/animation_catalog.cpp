/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/animation/animation_catalog.hpp>
#include <engine/animation/animation_generated.h>

namespace modus::engine::animation {

Result<> AnimationCatalog::initialize(const fbs::AnimationCatalog& catalog) {
    m_skeletons.clear();
    m_animations.clear();
    m_animations_by_name.clear();

    auto fbs_skeletons = catalog.skeletons();
    if (fbs_skeletons == nullptr) {
        MODUS_LOGE("AnimationCatalog: No skeletons in animation catalog");
        return Error<>();
    }

    auto fbs_animations = catalog.skeleton_animations();
    if (fbs_animations == nullptr) {
        MODUS_LOGE("AnimationCatalog: No animation in animation catalog");
        return Error<>();
    }

    if (fbs_skeletons->size() > kMaxSkeletons) {
        MODUS_LOGE(
            "AnimationCatalog: Catalog has {:02} skeletons, a maxium of {:02} "
            "are currenlty supported\n",
            fbs_skeletons->size(), kMaxSkeletons);
        return Error<>();
    }

    m_skeletons.resize(fbs_skeletons->size());
    for (usize i = 0; i < fbs_skeletons->size(); ++i) {
        Skeleton& skeleton = m_skeletons[i];
        const auto fbs_skeleton = fbs_skeletons->Get(i);
        if (fbs_skeleton == nullptr) {
            MODUS_LOGE("AnimationCatalog: Skeleton {:02} was not defined", i);
            return Error<>();
        }

        if (!skeleton.initialize(*fbs_skeleton)) {
            MODUS_LOGE("AnimationCatalog: Failed to initialize skeleton {:02}", i);
            return Error<>();
        }
    }

    m_animations.resize(fbs_animations->size());
    for (usize i = 0; i < fbs_animations->size(); ++i) {
        SkeletonAnimation& animation = m_animations[i];
        const auto fbs_animation = fbs_animations->Get(i);
        if (fbs_animation == nullptr) {
            MODUS_LOGE("AnimationCatalog: Animation {:02} was not defined", i);
            return Error<>();
        }

        if (fbs_animation->skeleton_index() > m_skeletons.size()) {
            MODUS_LOGE(
                "AnimationCatalog: Animation {:02} references undefined "
                "skeleton {:02}\n",
                i, fbs_animation->skeleton_index());
            return Error<>();
        }

        if (!animation.initialize(*fbs_animation)) {
            MODUS_LOGE("AnimationCatalog: Failed to initialize animation {:02}", i);
            return Error<>();
        }

        String animation_name = animation.name().to_str();
        if (m_animations_by_name.find(animation_name) != m_animations_by_name.end()) {
            MODUS_LOGE("AnimationCatalog: Animation with name {} already exists", animation_name);
            return Error<>();
        }
        m_animations_by_name[animation_name] = i;
    }
    return Ok<>();
}

Result<NotMyPtr<const SkeletonAnimation>, void> AnimationCatalog::animation(
    const StringSlice name) const {
    const auto it = m_animations_by_name.find(name.to_str());
    if (it == m_animations_by_name.end()) {
        return Error<>();
    }
    if (it->second >= m_animations.size()) {
        return Error<>();
    }
    return Ok<NotMyPtr<const SkeletonAnimation>>(&m_animations[it->second]);
}

Result<NotMyPtr<const SkeletonAnimation>, void> AnimationCatalog::animation(const u32 index) const {
    if (index >= m_animations.size()) {
        return Error<>();
    }
    return Ok<NotMyPtr<const SkeletonAnimation>>(&m_animations[index]);
}

Result<NotMyPtr<const Skeleton>, void> AnimationCatalog::skeleton(const u32 index) const {
    if (index >= m_skeletons.size()) {
        return Error<>();
    }
    return Ok<NotMyPtr<const Skeleton>>(&m_skeletons[index]);
}

}    // namespace modus::engine::animation
