/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/animation/skeleton_animation.hpp>
#include <engine/animation/animation_generated.h>

namespace modus::engine::animation {
Result<> SkeletonAnimation::initialize(const fbs::SkeletonAnimation& animation) {
    m_name.clear();
    m_skeleton_index = std::numeric_limits<u32>::max();
    m_bone_keyframes.clear();
    m_total_bone_transforms.clear();
    m_duration_sec = FSeconds(0.f);

    auto fbs_bone_keyframes = animation.bone_keyframes();
    modus_assert(fbs_bone_keyframes);

    const usize bone_keyframes_size = fbs_bone_keyframes->size();
    if (bone_keyframes_size > kMaxSkeletonBones) {
        MODUS_LOGE(
            "Animation: Animation {} has {:02} bone keyframes and the maximum "
            "support count is {:02}\n",
            m_name, bone_keyframes_size, kMaxSkeletonBones);
        return Error<>();
    }

    m_bone_keyframes.resize(bone_keyframes_size);
    usize total_bone_transforms_count = 0;
    for (const auto& bkf : *fbs_bone_keyframes) {
        total_bone_transforms_count += bkf->bone_transforms()->size();
    }
    m_total_bone_transforms.reserve(total_bone_transforms_count);
    usize bone_transform_counter = 0;
    for (usize i = 0; i < bone_keyframes_size; ++i) {
        const auto fbs_bone_keyframe = fbs_bone_keyframes->Get(i);
        const u32 bone_index = fbs_bone_keyframe->bone_index();
        m_bone_keyframes[i].m_bone_id = BoneId(bone_index);
        m_bone_keyframes[i].m_start_offset = bone_transform_counter;
        m_bone_keyframes[i].m_count = fbs_bone_keyframe->bone_transforms()->size();
        for (const auto& fbs_bt : *fbs_bone_keyframe->bone_transforms()) {
            BoneTransform bt;
            bt.m_timestamp = FSeconds(fbs_bt->timestamp());
            const auto fbs_transform = fbs_bt->bone_transform();
            bt.m_transform.set_scale(fbs_transform->scale());
            bt.m_transform.set_rotation(
                glm::quat(fbs_transform->rotation().w(), fbs_transform->rotation().x(),
                          fbs_transform->rotation().y(), fbs_transform->rotation().z()));
            bt.m_transform.set_translation(glm::vec3(fbs_transform->position().x(),
                                                     fbs_transform->position().y(),
                                                     fbs_transform->position().z()));
            m_total_bone_transforms.push_back(bt);
            bone_transform_counter++;
        }
    }

    m_name = animation.name()->c_str();
    m_skeleton_index = animation.skeleton_index();
    m_duration_sec = FSeconds(animation.duration_sec());

    MODUS_UNUSED(animation);
    return Ok<>();
}

Slice<BoneTransform> SkeletonAnimation::bone_transforms(const BoneKeyframe& key_frame) const {
    modus_assert(key_frame.m_start_offset < m_total_bone_transforms.size());
    Slice<BoneTransform> slice = make_slice(m_total_bone_transforms);
    return slice.sub_slice(key_frame.m_start_offset, key_frame.m_count);
}

}    // namespace modus::engine::animation
