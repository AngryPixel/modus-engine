/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/animation/skeleton.hpp>
#include <engine/animation/animation_generated.h>

namespace modus::engine::animation {

Result<> Skeleton::initialize(const fbs::Skeleton& skeleton) {
    MODUS_UNUSED(skeleton);

    const auto fbs_bones = skeleton.bones();
    if (fbs_bones == nullptr) {
        MODUS_LOGE("Skeleton: No bones in skeleton");
        return Error<>();
    }

    if (fbs_bones->size() > kMaxSkeletonBones) {
        MODUS_LOGE(
            "Skeleton: Skeleton has {:02} bone and the maximum support count "
            "is {:02}",
            kMaxSkeletonBones);
        return Error<>();
    }

    m_bones.resize(fbs_bones->size());
    m_bone_names.resize(fbs_bones->size());
    for (usize i = 0; i < fbs_bones->size(); ++i) {
        const auto fbs_bone = fbs_bones->Get(i);
        modus_assert(fbs_bone != nullptr);

        SkeletonBone& bone = m_bones[i];
        bone.m_index = BoneId(i);
        if (const u32 parent_index = fbs_bone->parent_index(); parent_index < m_bones.size()) {
            bone.m_parent_index = BoneId(parent_index);
        }

        auto& bone_name = m_bone_names[i];
        bone_name = StringSlice(fbs_bone->name()->c_str());

        auto fn_to_transform = [](const auto& fbs_transform) -> math::Transform {
            math::Transform transform;
            transform.set_scale(fbs_transform->scale());
            transform.set_rotation(
                glm::quat(fbs_transform->rotation().w(), fbs_transform->rotation().x(),
                          fbs_transform->rotation().y(), fbs_transform->rotation().z()));
            transform.set_translation(glm::vec3(fbs_transform->position().x(),
                                                fbs_transform->position().y(),
                                                fbs_transform->position().z()));
            return transform;
        };

        const auto fbs_inv_bind_transform = fbs_bone->inv_bind_transform();
        const auto fbs_bone_transform = fbs_bone->bone_transform();
        bone.m_bind_transform_inv = fn_to_transform(fbs_inv_bind_transform);
        bone.m_bone_transform = fn_to_transform(fbs_bone_transform);
        bone.m_bind_transform = math::Transform();
    }
    for (usize i = 0; i < m_bones.size(); ++i) {
        SkeletonBone& bone = m_bones[i];
        if (bone.m_parent_index) {
            modus_assert(bone.m_parent_index.value() < m_bones.size());
            const SkeletonBone& parent_bone = m_bones[bone.m_parent_index.value()];
            bone.m_bind_transform = parent_bone.m_bind_transform.transformed(bone.m_bone_transform);
        } else {
            bone.m_bind_transform = bone.m_bone_transform;
        }
    }
    return Ok<>();
}

Result<> Skeleton::add_bone(const SkeletonBone& bone, const StringSlice name) {
    // At max capacity?
    if (m_bones.size() == kMaxSkeletonBones) {
        return Error<>();
    }

    // Name already exists?
    if (bone_by_name(name)) {
        return Error<>();
    }

    m_bone_names.push_back(name).expect("Failed to add bone name");
    m_bones.push_back(bone).expect("Failed to add bone");
    return Ok<>();
}

Result<NotMyPtr<const SkeletonBone>, void> Skeleton::bone_by_name(const StringSlice name) const {
    auto r_idx = bone_index_by_name(name);
    if (r_idx) {
        return Error<>();
    }

    return Ok<NotMyPtr<const SkeletonBone>>(&m_bones[*r_idx]);
}

Result<StringSlice, void> Skeleton::bone_name(const u32 bone_index) const {
    if (bone_index >= m_bones.size()) {
        return Error<>();
    }
    return Ok(m_bone_names[bone_index].as_string_slice());
}

Result<u32, void> Skeleton::bone_index_by_name(const StringSlice name) const {
    u32 idx = 0;
    for (auto& bone_name : m_bone_names) {
        if (name == bone_name.as_string_slice()) {
            return Ok(idx);
        }
        idx++;
    }
    return Error<>();
}

}    // namespace modus::engine::animation
