/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format on
#include <pch.h>
// clang-format off

#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/engine.hpp>
namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleAnimation, "0cecdf34-a8f7-4117-878e-e76a75ec13e8");
static constexpr u32 kAnimationCatalogPoolSize = 32;

ModuleAnimation::ModuleAnimation() : Module(module_guid<ModuleAnimation>(), "Animation", ModuleUpdateCategory::Animation),
m_animation_catalog_db(kAnimationCatalogPoolSize),
m_animator_manager(m_animation_catalog_db) {

}
Result<> ModuleAnimation::initialize(Engine&) {
    MODUS_PROFILE_ANIM_BLOCK("Animation Init");
    if (!m_animation_catalog_db.reserve(kAnimationCatalogPoolSize)) {
        return Error<>();
    }

    if (!m_animator_manager.initialize()) {
        return Error<>();
    }

    return Ok<>();
}

Result<> ModuleAnimation::shutdown(Engine&) {
    MODUS_PROFILE_ANIM_BLOCK("Animation Shutdown");
    m_animator_manager.shutdown();
    m_animation_catalog_db.clear();
    return Ok<>();
}

void ModuleAnimation::tick(Engine& engine) {
    MODUS_PROFILE_ANIM_BLOCK("Animation Update");
    m_animator_manager.update(engine);
}

Slice<GID> ModuleAnimation::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleGameplay>()};
    return Slice(deps);
}

}
