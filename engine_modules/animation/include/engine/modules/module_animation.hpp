/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

#include <engine/animation/animation_catalog.hpp>
#include <engine/animation/animator_manager.hpp>
#include <engine/modules/module.hpp>

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleAnimation final : public Module {
   private:
    animation::AnimationCatalogDB m_animation_catalog_db;
    animation::AnimatorManager m_animator_manager;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleAnimation();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    void tick(Engine&) override;

    Slice<GID> module_dependencies() const override;

    animation::AnimationCatalogDB& animation_catalog_db() { return m_animation_catalog_db; }

    const animation::AnimationCatalogDB& animation_catalog_db() const {
        return m_animation_catalog_db;
    }

    animation::AnimatorManager& manager() { return m_animator_manager; }

    const animation::AnimatorManager& manager() const { return m_animator_manager; }
};

}    // namespace modus::engine
