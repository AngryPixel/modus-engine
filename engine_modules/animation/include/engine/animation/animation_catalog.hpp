/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

#include <core/fixed_vector.hpp>
#include <core/handle_pool.hpp>
#include <engine/animation/skeleton.hpp>
#include <engine/animation/skeleton_animation.hpp>

namespace modus::engine::animation {

namespace fbs {
struct AnimationCatalog;
}

class MODUS_ENGINE_EXPORT AnimationCatalog {
   private:
    static constexpr u32 kMaxSkeletons = 4;
    FixedVector<Skeleton, kMaxSkeletons> m_skeletons;
    Vector<SkeletonAnimation> m_animations;
    HashMap<String, u32> m_animations_by_name;

   public:
    Result<> initialize(const fbs::AnimationCatalog& catalog);

    Result<NotMyPtr<const SkeletonAnimation>, void> animation(const StringSlice name) const;

    Result<NotMyPtr<const SkeletonAnimation>, void> animation(const u32 index) const;

    Result<NotMyPtr<const Skeleton>, void> skeleton(const u32 index) const;
};

using AnimationCatalogDB = HandlePool<AnimationCatalog, AnimationAllocator, AnimationCatalogHandle>;

}    // namespace modus::engine::animation
