/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

#include <core/delegate.hpp>
#include <core/handle_pool_shared.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::animation {

static constexpr u32 kMaxSkeletonBones = 16;
static constexpr u32 kMaxSkeletonBoneNameLength = 16;

#define MODUS_DECLARE_ANIMATION_HANDLE(Name, Invalid)           \
    MODUS_STRONG_TYPE_CLASS(Name, u32, Invalid)                 \
    inline bool is_valid() const { return value() != Invalid; } \
    inline operator bool() const { return is_valid(); }         \
    }

MODUS_DECLARE_ANIMATION_HANDLE(BoneId, kMaxSkeletonBones);
MODUS_DECLARE_ANIMATION_HANDLE(AnimatorHandle, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(AnimationCatalogHandle, std::numeric_limits<u32>::max());

using AnimationFinishedDelegate = Delegate<void(Engine&)>;

}    // namespace modus::engine::animation
