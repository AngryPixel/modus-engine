/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

#include <core/allocator/pool_allocator.hpp>
#include <core/handle_pool.hpp>
#include <engine/animation/animation_catalog.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::animation {

class SkeletonAnimator;

struct MODUS_ENGINE_EXPORT PlayAnimationParams {
    StringSlice animation_name;
    f32 speed = 1.0f;
    bool loop = false;
    AnimationFinishedDelegate on_animation_completed = AnimationFinishedDelegate();
};

class MODUS_ENGINE_EXPORT AnimatorManager {
   private:
    struct AnimatorData {
        NotMyPtr<SkeletonAnimator> ptr;
        AnimatorHandle handle;
        bool alive = false;
    };
    using StorageType = animation::Vector<AnimatorData>;
    using PoolType = PoolAllocatorTyped<SkeletonAnimator, AnimationAllocator>;
    using HandleStorage = HandlePool<u32, AnimationAllocator, AnimatorHandle>;

    AnimationCatalogDB& m_anim_catalog_db;
    StorageType m_animators;
    PoolType m_animator_pool;
    HandleStorage m_handles;
    u32 m_active_count;
    u32 m_size;

   public:
    AnimatorManager(AnimationCatalogDB& anim_catalog_db);

    ~AnimatorManager();

    MODUS_CLASS_DISABLE_COPY_MOVE(AnimatorManager);

    Result<> initialize();

    void shutdown();

    void update(Engine& engine);

    Result<AnimatorHandle, void> create(const AnimationCatalogHandle handle);

    void destroy(const AnimatorHandle handle);

    void clear();

    Result<> play(const AnimatorHandle, const PlayAnimationParams& params);

    Result<> set_animation_speed(const AnimatorHandle handle,
                                 const StringSlice name,
                                 const f32 animation_speed);

    Result<Slice<glm::mat4>, void> animation_data(const AnimatorHandle) const;

    Result<NotMyPtr<const Skeleton>, void> animation_skeleton(const AnimatorHandle) const;

   private:
    Result<> grow_or_expand(const usize new_size);

    void on_destroy(AnimatorData& animator);

    u32 update_internal();
};
}    // namespace modus::engine::animation
