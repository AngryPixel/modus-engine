/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

namespace modus::engine::animation {
class Skeleton;
class SkeletonAnimation;
class AnimationCatalog;

struct MODUS_ENGINE_EXPORT SkeletonAnimatorParams {
    const StringSlice animation_name;
    f32 animation_speed = 1.0f;
    bool loop = false;
    AnimationFinishedDelegate on_animation_completed = AnimationFinishedDelegate();
};

class MODUS_ENGINE_EXPORT SkeletonAnimator {
   private:
    static constexpr u32 kMaxConcurrentAnimations = 4;
    struct Instance {
        Array<u32, kMaxSkeletonBones> m_keyframe_cache;
        NotMyPtr<const SkeletonAnimation> m_animation;
        FSeconds m_elapsed_time = FSeconds(1.0f);
        f32 m_animation_speed = 1.0f;
        AnimationFinishedDelegate m_on_animation_completed;
        bool m_loop = false;
        bool m_pause = false;
    };
    NotMyPtr<const AnimationCatalog> m_catalog;
    NotMyPtr<const Skeleton> m_skeleton;
    Array<Instance, kMaxConcurrentAnimations> m_instances;
    Array<glm::mat4, kMaxSkeletonBones> m_animation_data;

   public:
    SkeletonAnimator(NotMyPtr<const AnimationCatalog> catalog);

    Result<> play(const SkeletonAnimatorParams& params);

    void update(Engine& engine, const FSeconds elapsed_time_sec);

    void set_animation_speed(const StringSlice name, const f32 speed);

    NotMyPtr<const Skeleton> skeleton() const { return m_skeleton; }

    const Slice<glm::mat4> animation_data() const { return make_slice(m_animation_data); }

   private:
    Result<Instance*, void> instance_by_name(const StringSlice& name);
};

}    // namespace modus::engine::animation
