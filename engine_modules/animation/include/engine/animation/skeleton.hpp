/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

#include <core/fixed_stack_string.hpp>
#include <core/fixed_vector.hpp>

namespace modus::engine::animation {

namespace fbs {
struct Skeleton;
}

struct MODUS_ENGINE_EXPORT SkeletonBone {
    BoneId m_index;
    BoneId m_parent_index;
    math::Transform m_bind_transform_inv;
    math::Transform m_bone_transform;
    math::Transform m_bind_transform;
};

class MODUS_ENGINE_EXPORT Skeleton {
   private:
    FixedVector<FixedStackString<kMaxSkeletonBoneNameLength>, kMaxSkeletonBones> m_bone_names;
    FixedVector<SkeletonBone, kMaxSkeletonBones> m_bones;

   public:
    Skeleton() = default;

    Result<> initialize(const fbs::Skeleton& skeleton);

    Result<> add_bone(const SkeletonBone& bone, const StringSlice name);

    Slice<SkeletonBone> bones() const { return make_slice(m_bones); }

    Result<NotMyPtr<const SkeletonBone>, void> bone_by_name(const StringSlice name) const;

    Result<StringSlice, void> bone_name(const u32 bone_index) const;

    const SkeletonBone& root_bone() const {
        modus_assert(m_bones.size() != 0);
        return m_bones[0];
    }

   protected:
    Result<u32, void> bone_index_by_name(const StringSlice name) const;
};

}    // namespace modus::engine::animation
