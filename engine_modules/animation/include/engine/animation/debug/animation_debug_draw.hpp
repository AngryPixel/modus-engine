/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

namespace modus::engine::animation {
class Skeleton;

class MODUS_ENGINE_EXPORT DebugDrawer {
   public:
    virtual ~DebugDrawer() = default;

    void draw_skeleton(const glm::mat4& world_transform,
                       const Skeleton& skeleton,
                       Optional<Slice<glm::mat4>> bone_transforms);

   protected:
    virtual void draw_line(const glm::vec3& from, const glm::vec3& to, const glm::vec4& color) = 0;

    virtual void draw_text3d(const StringSlice text,
                             const glm::vec3& point,
                             const f32 scale,
                             const glm::vec3& color) = 0;

    virtual void draw_sphere(const glm::vec3& center, const f32 radius, const glm::vec4& color) = 0;
};

}    // namespace modus::engine::animation
