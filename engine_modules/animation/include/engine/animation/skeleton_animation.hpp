/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/animation/module_includes.hpp>

#include <core/fixed_stack_string.hpp>
#include <core/fixed_vector.hpp>

namespace modus::engine::animation {

namespace fbs {
struct SkeletonAnimation;
}

struct MODUS_ENGINE_EXPORT BoneTransform {
    FSeconds m_timestamp;
    math::Transform m_transform;
};

struct MODUS_ENGINE_EXPORT BoneKeyframe {
    u32 m_start_offset;
    u32 m_count;
    BoneId m_bone_id;
};

class MODUS_ENGINE_EXPORT SkeletonAnimation {
   private:
    static constexpr u32 kAnimationNameLength = 32;
    FixedStackString<kAnimationNameLength> m_name;
    FixedVector<BoneKeyframe, kMaxSkeletonBones> m_bone_keyframes;
    Vector<BoneTransform> m_total_bone_transforms;
    FSeconds m_duration_sec;
    u32 m_skeleton_index;

   public:
    SkeletonAnimation() = default;

    MODUS_CLASS_DISABLE_COPY(SkeletonAnimation);
    MODUS_CLASS_DEFAULT_MOVE(SkeletonAnimation);

    Result<> initialize(const fbs::SkeletonAnimation& animation);

    StringSlice name() const { return m_name.as_string_slice(); }

    Slice<BoneKeyframe> bone_keyframes() const { return make_slice(m_bone_keyframes); }

    Slice<BoneTransform> bone_transforms(const BoneKeyframe& key_frame) const;

    FSeconds duration_sec() const { return m_duration_sec; }

    u32 skeleton_index() const { return m_skeleton_index; }
};

}    // namespace modus::engine::animation
