/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <core/io/memory_stream.hpp>
#include <engine/engine.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/modules/module_ui.hpp>
#include <engine/ui/uifont_asset.hpp>

#include <ui/context.hpp>
#include <ui/text/font.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::ui::UIFontAsset)

namespace modus::engine::ui {

UIFontAsset::UIFontAsset(const modus::ui::UIRefPtr<modus::ui::Font>& font)
    : assets::AssetBase(assets::AssetTraits<UIFontAsset>::type_id()), m_font(font) {}

UIFontAsset::~UIFontAsset() {}

UIFontAssetFactory::UIFontAssetFactory()
    : assets::PooledAssetFactory<UIFontAsset, UIAllocator>(8) {}

StringSlice UIFontAssetFactory::file_extension() const {
    return "uifont";
}

std::unique_ptr<assets::AssetData> UIFontAssetFactory::create_asset_data() {
    return modus::make_unique<assets::RamAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> UIFontAssetFactory::create(Engine& engine,
                                                               assets::AssetData& data) {
    const engine::assets::RamAssetData& font_data = static_cast<const assets::RamAssetData&>(data);
    MODUS_PROFILE_UI_BLOCK("UIFontAssetCreate");

    SliceStream stream = font_data.stream();
    auto ui_module = engine.module<ModuleUI>();
    auto r = modus::ui::Font::create(&ui_module->context(), stream);
    if (!r) {
        StringSlice reason;
        switch (r.error()) {
            case modus::ui::FontError::IO:
                reason = "io";
                break;
            case modus::ui::FontError::Format:
                reason = "font format";
                break;
            case modus::ui::FontError::Content:
                reason = "font content";
                break;
            case modus::ui::FontError::Painter:
                reason = "painter";
                break;
            default:
                reason = "unkown";
                break;
        }
        MODUS_LOGE("UIFontAsset: Failed to load font due to a(n) {} error.", reason);
        return Error<>();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(m_asset_pool.construct(*r));
}

Result<> UIFontAssetFactory::destroy(Engine&, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_UI_BLOCK("UIFontAssetDestroy");
    auto r_font_asset = assets::assetv2_cast<UIFontAsset>(asset);
    if (!r_font_asset) {
        return Error<>();
    }
    m_asset_pool.destroy(r_font_asset->get());
    return Ok<>();
}

}    // namespace modus::engine::ui
