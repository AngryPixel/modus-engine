/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/modules/module_ui.hpp>
// clang-format on

#include <engine/engine.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_event.hpp>
#include <engine/modules/module_input.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/input/input_manager.hpp>
#include <engine/input/default_input_priorities.hpp>

#include <ui/context.hpp>
#include <ui/painters/threed/threed_painter.hpp>
#include <ui/event.hpp>

#include <threed/compositor.hpp>
#include <app/event.hpp>

namespace modus::engine {

// Have the game configure the update order as the dependecy list
// is no longer enough to determine which modules need to run when

namespace ui {

class UIInputContext final : public input::InputContext {
   private:
    NotMyPtr<modus::ui::Context> m_context;

   public:
    UIInputContext(NotMyPtr<modus::ui::Context> context)
        : input::InputContext(input::kUIInputPriorityMin), m_context(context) {}

    StringSlice name() const override { return "UIInput"; }

    bool handle_event(Engine&, const app::InputEvent& event) override {
        MODUS_PROFILE_UI_BLOCK("UIInputContex::handle_event");
        modus::ui::Event ui_event;
        switch (event.type) {
            case app::InputEventType::MouseMove: {
                ui_event.type = modus::ui::EventType::MouseMove;
                ui_event.mouse_move.x = f32(event.mouse_move.x);
                ui_event.mouse_move.y = f32(event.mouse_move.y);
                return m_context->on_event(ui_event);
            }
            case app::InputEventType::MouseButton: {
                switch (event.mouse_button.button) {
                    case app::MouseButton::Left:
                        ui_event.mouse_button.button = modus::ui::MouseButton::Left;
                        break;
                    case app::MouseButton::Middle:
                        ui_event.mouse_button.button = modus::ui::MouseButton::Middle;
                        break;
                    case app::MouseButton::Right:
                        ui_event.mouse_button.button = modus::ui::MouseButton::Right;
                        break;
                    default:
                        return false;
                }
                ui_event.type = modus::ui::EventType::MouseButton;
                ui_event.mouse_button.x = f32(event.mouse_button.x);
                ui_event.mouse_button.y = f32(event.mouse_button.y);
                ui_event.mouse_button.is_down = event.mouse_button.is_mouse_down();
                return m_context->on_event(ui_event);
            }
            case app::InputEventType::MouseScroll: {
                ui_event.type = modus::ui::EventType::MouseScroll;
                ui_event.mouse_scroll.x = event.mouse_scroll.xvalue;
                ui_event.mouse_scroll.y = event.mouse_scroll.yvalue;
                return m_context->on_event(ui_event);
            }
            default:
                return false;
        }
    }
};

}    // namespace ui

MODUS_ENGINE_IMPL_MODULE(ModuleUI, "fab3bc35-564e-4e28-a62f-8bae1f106991");

ModuleUI::ModuleUI() : Module(module_guid<ModuleUI>(), "UI", ModuleUpdateCategory::UI) {}

ModuleUI::~ModuleUI() {}

Result<> ModuleUI::initialize(Engine& engine) {
    MODUS_PROFILE_UI_BLOCK("ModuleUI::initialize");
    MODUS_LOGV("Initializing UI");

    auto graphics_module = engine.module<ModuleGraphics>();
    if (!graphics_module) {
        MODUS_LOGE("Couldn't locate graphics module");
        return Error<>();
    }
    auto event_module = engine.module<ModuleEvent>();
    if (!event_module) {
        MODUS_LOGE("Coudlnt locate event module");
        return Error<>();
    }
    auto app_module = engine.module<ModuleApp>();
    if (!app_module) {
        MODUS_LOGE("Coudlnt locate app module");
        return Error<>();
    }
    auto asset_module = engine.module<ModuleAssets>();
    if (!asset_module) {
        MODUS_LOGE("Coudlnt locate asset module");
        return Error<>();
    }
    threed::Device& device = graphics_module->device();
    threed::Compositor& compositor = graphics_module->default_compositor();

    m_painter_device =
        modus::make_unique<modus::ui::ThreedPainterDeviceEmbedded>(&device, &compositor);
    m_context = modus::make_unique<modus::ui::Context>(m_painter_device.get());
    m_context->set_drawable_size({f32(compositor.width()), f32(compositor.height())});

    if (!m_context->initialize()) {
        MODUS_LOGV("Failed to initialize ui context");
        return Error<>();
    }
    m_input_context = modus::make_unique<ui::UIInputContext>(m_context.get());

    if (!asset_module->loader().register_factory(&m_font_asset_factory)) {
        MODUS_LOGE("Failed to register font asset context");
        return Error<>();
    }

    assets::AsyncLoadParams params;
    params.path = "font:hack.uifont";
    params.callback = [this](Engine&, const assets::AsyncCallbackResult& result) {
        if (result.error != assets::AssetErrorV2::None) {
            MODUS_LOGE("UIModule: Failed to load default font, no default font will be available");
            return;
        }
        NotMyPtr<const ui::UIFontAsset> font_asset =
            assets::assetv2_cast<const ui::UIFontAsset>(result.asset)
                .value_or_panic("Asset types do not match");
        m_context->set_default_font(font_asset->m_font);
    };

    if (auto r = engine.load_startup_asset(params); !r) {
        MODUS_LOGE("Failed to load default font");
        return Error<>();
    }
    modus::engine::event::EventListener listener;
    listener.bind<ModuleUI, &ModuleUI::on_window_resize>(this);

    if (auto r = event_module->manager().register_event_listener(
            app_module->window_resize_event_handle(), listener);
        !r) {
        MODUS_LOGE("Failed to register app window resize event listener");
        return Error<>();
    } else {
        m_listerner_handle = *r;
    }

    graphics_module->set_ui_renderer(this);

    m_context->set_visible(false);
    auto input_module = engine.module<ModuleInput>();
    m_input_context->set_enabled(false);
    input_module->manager().add_context(m_input_context.get());
    MODUS_LOGV("UI Initialized");
    return Ok<>();
}

Result<> ModuleUI::shutdown(Engine& engine) {
    MODUS_PROFILE_UI_BLOCK("ModuleUI::shutdown");
    MODUS_LOGV("Shutting down UI");
    MODUS_UNUSED(engine);
    auto input_module = engine.module<ModuleInput>();
    input_module->manager().remove_context(m_input_context.get());

    if (auto m = engine.module<ModuleGraphics>(); m) {
        m->set_ui_renderer(NotMyPtr<UIRenderer>());
    }

    if (m_listerner_handle) {
        auto app_module = engine.module<ModuleApp>();
        auto event_module = engine.module<ModuleEvent>();
        if (!event_module->manager().unregister_event_listner(
                app_module->window_resize_event_handle(), m_listerner_handle)) {
            MODUS_LOGW("Failed to unregister event handler");
        }
    }

    if (auto asset_module = engine.module<ModuleAssets>(); asset_module) {
        asset_module->loader().unregister_factory(engine, &m_font_asset_factory);
    }

    m_input_context.reset();

    if (m_context) {
        m_context->shutdown();
        m_context.reset();
    }
    m_painter_device.reset();

    MODUS_LOGV("UI shutdown complete");
    return Ok<>();
}

void ModuleUI::tick(Engine& engine) {
    MODUS_PROFILE_UI_BLOCK("ModuleUI::tick");
    m_context->update(engine.elapsed_time_ms());
}

Slice<GID> ModuleUI::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleGraphics>(), module_guid<ModuleApp>(),
                               module_guid<ModuleEvent>(), module_guid<ModuleInput>(),
                               module_guid<ModuleAssets>()};
    return Slice(deps);
}

void ModuleUI::show(Engine&, const bool input_enabled) {
    m_input_context->set_enabled(input_enabled);
    if (!m_context->visible()) {
        m_context->set_visible(true);
    }
}

void ModuleUI::hide(Engine&) {
    if (m_context->visible()) {
        m_context->set_visible(false);
    }
}

void ModuleUI::render(Engine&, threed::Pipeline& pipeline) {
    if (m_context->visible()) {
        MODUS_PROFILE_UI_BLOCK("ModuleUI::render");
        modus::ui::ThreedPainterDeviceEmbedded* painter =
            static_cast<modus::ui::ThreedPainterDeviceEmbedded*>(m_painter_device.get());
        painter->set_pipeline(&pipeline);
        m_context->paint(false);
    }
}

void ModuleUI::set_input_enabled(const bool v) {
    m_input_context->set_enabled(v);
}

bool ModuleUI::is_input_enabled() const {
    return m_input_context->enabled();
}

void ModuleUI::on_window_resize(Engine&, engine::event::Event& evt) {
    const AppWindowResizeEvent& event = static_cast<AppWindowResizeEvent&>(evt);
    if (m_context) {
        modus::ui::Event ui_event;
        ui_event.type = modus::ui::EventType::ContextResize;
        ui_event.context_resize.width = f32(event.m_width);
        ui_event.context_resize.height = f32(event.m_height);
        m_context->on_event(ui_event);
    }
}

}    // namespace modus::engine
