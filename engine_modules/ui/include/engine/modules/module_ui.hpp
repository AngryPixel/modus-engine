/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/ui/module_includes.hpp>

#include <core/fixed_stack.hpp>
#include <engine/event/event_types.hpp>
#include <engine/modules/module.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/ui/uifont_asset.hpp>
#include <engine_ui_module/engine_module_config.hpp>

namespace modus::ui {
class Context;
class PainterDevice;
}    // namespace modus::ui

namespace modus::engine {

namespace ui {
class UIInputContext;
}

namespace assets {
struct AsyncCallbackResult;
}

namespace event {
class Event;
}
class MODUS_ENGINE_EXPORT ModuleUI final : public Module, public UIRenderer {
   private:
    std::unique_ptr<modus::ui::PainterDevice> m_painter_device;
    std::unique_ptr<modus::ui::Context> m_context;
    std::unique_ptr<ui::UIInputContext> m_input_context;
    ui::UIFontAssetFactory m_font_asset_factory;
    modus::engine::event::ListenerHandle m_listerner_handle;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleUI();

    ~ModuleUI();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    void tick(Engine&) override;

    Slice<GID> module_dependencies() const override;

    modus::ui::Context& context() {
        modus_assert(m_context);
        return *m_context;
    }

    const modus::ui::Context& context() const {
        modus_assert(m_context);
        return *m_context;
    }

    void show(Engine& engine, const bool input_enabled);

    void hide(Engine& engine);

    void render(Engine&, threed::Pipeline&) override;

    void set_input_enabled(const bool v);

    bool is_input_enabled() const;

   private:
    void on_window_resize(Engine& engine, engine::event::Event& event);
};

}    // namespace modus::engine
