/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/engine.hpp>
#include <engine/modules/module_audio.hpp>

#include <audio/audio_instance.hpp>
#include <audio/audio_device.hpp>
#include <engine/modules/module_gameplay.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleAudio, "e3b96f9b-597b-47a9-9ce0-3ed6c1c54815");

ModuleAudio::ModuleAudio()
    : Module(module_guid<ModuleAudio>(), "Audio", ModuleUpdateCategory::Audio) {}

ModuleAudio::~ModuleAudio() {}

Result<> ModuleAudio::initialize(Engine& engine) {
    MODUS_PROFILE_AUDIO_BLOCK("ModuleAudio::initialize");
    MODUS_LOGV("Initializing Audio");
    MODUS_UNUSED(engine);
    // create & init instance
    m_instance = ::modus::audio::create_default_audio_instance();
    if (!m_instance) {
        MODUS_LOGE("ModuleAudio: Failed to create audio instance");
        return Error<>();
    }

    if (!m_instance->initialize()) {
        MODUS_LOGE("ModuleAudio: Failed to initialize audio instance");
        return Error<>();
    }

    // create & init device
    const auto devices = m_instance->list_audio_devices();
    if (devices.is_empty()) {
        MODUS_LOGE("ModuleAudio: Instance does not have any audio devices");
        return Error<>();
    }

    if (auto r_device = m_instance->create_device(devices[0]); !r_device) {
        MODUS_LOGE("ModuleAudio: Failed to create audio device: {}", devices[0].name);
        return Error<>();
    } else {
        m_device = r_device.release_value();
    }

    if (auto r_context = m_device->create_context(); !r_context) {
        MODUS_LOGE("ModuleAudio: Failed to create audi context");
        return Error<>();
    } else {
        m_context = r_context.release_value();
    }

    if (!m_context->bind_to_current_thread()) {
        MODUS_LOGE("ModuleAudion: Failed to bind context to current thread");
        return Error<>();
    }

    MODUS_LOGV("Audio Initialized");
    return Ok<>();
}

Result<> ModuleAudio::shutdown(Engine& engine) {
    MODUS_PROFILE_AUDIO_BLOCK("ModuleAudio::shutdown");
    MODUS_LOGV("Shutting down Audio");
    MODUS_UNUSED(engine);
    m_context.reset();
    m_device.reset();
    if (m_instance) {
        if (!m_instance->shutdown()) {
            MODUS_LOGE("ModuleAudio: Failed to shutdown audio instance");
        }
        m_instance.reset();
    }
    MODUS_LOGV("Audio shutdown complete");
    return Ok<>();
}

void ModuleAudio::tick(Engine& engine) {
    MODUS_PROFILE_AUDIO_BLOCK("ModuleAudio::tick");
    MODUS_UNUSED(engine);
    m_context->update();
}

Slice<GID> ModuleAudio::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleGameplay>()};
    return Slice(deps);
}

}    // namespace modus::engine
