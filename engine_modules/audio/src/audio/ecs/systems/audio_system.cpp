/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/audio/ecs/systems/audio_system.hpp>
// clang-format on
#include <engine/audio/ecs/components/audio_components.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/components/velocity_component.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/modules/module_audio.hpp>
#include <audio/audio_device.hpp>

#include <engine/audio/ecs/audio_components_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::engine::audio::AudioSourceComponent)
MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(modus::engine::audio::AudioListenerComponent)

namespace modus::engine::gameplay {

Result<modus::engine::audio::AudioSourceComponent>
ComponentLoaderTraits<modus::engine::audio::AudioSourceComponent>::load(Engine&,
                                                                        const StorageType& fbs) {
    modus::engine::audio::AudioSourceComponent out;
    out.directionless = fbs.directionless();
    return Ok(out);
}
}    // namespace modus::engine::gameplay

namespace modus::engine::audio {

AudioSystem::AudioSystem(const bool handle_velocity)
    : engine::gameplay::System("Audio"), m_handle_velocity(handle_velocity) {}

Result<> AudioSystem::initialize(Engine& engine,
                                 gameplay::GameWorld&,
                                 gameplay::EntityManager& em) {
    auto listener = engine::gameplay::ComponentDestroyedDelegate::build<
        AudioSystem, &AudioSystem::on_component_destroyed>(this);

    auto audio_module = engine.module<ModuleAudio>();
    if (!audio_module) {
        MODUS_LOGE("AudioSystem: Audio module {} is not in module list",
                   module_guid<ModuleAudio>());
        return Error<>();
    }

    if (!em.register_component_destroy_callback<AudioSourceComponent>(listener)) {
        return Error<>();
    }
    return Ok<>();
}

void AudioSystem::shutdown(Engine&, gameplay::GameWorld&, gameplay::EntityManager& em) {
    em.unregister_component_destroy_callback<AudioSourceComponent>();
}

void AudioSystem::update(Engine& engine,
                         const IMilisec,
                         gameplay::GameWorld&,
                         gameplay::EntityManager& entity_manager) {
    auto& audio_context = engine.module<engine::ModuleAudio>()->context();
    {
        MODUS_PROFILE_AUDIO_BLOCK("Update Audio Source Components");
        engine::gameplay::EntityManagerViewMut<const gameplay::TransformComponent,
                                               AudioSourceComponent>
            view(entity_manager);
        view.for_each([&audio_context](const gameplay::EntityId id,
                                       const gameplay::TransformComponent& tc,
                                       AudioSourceComponent& ac) {
            const math::Transform& world_transform = tc.world_transform();
            if (ac.audio_source && tc.did_world_transform_update()) {
                // update audio source position & orientation
                if (!audio_context.set_source_position(ac.audio_source,
                                                       world_transform.translation())) {
                    MODUS_LOGE(
                        "AudioSystem: Failed to set audio source "
                        "position for Entity {}",
                        id);
                }
                if (!ac.directionless) {
                    if (!audio_context.set_source_direction(ac.audio_source,
                                                            world_transform.forward())) {
                        MODUS_LOGE(
                            "AudioSystem: Failed to set audio source "
                            "direction for Entity {}",
                            id);
                    }
                }
            }
        });
    }
    {
        MODUS_PROFILE_AUDIO_BLOCK("Update Audio Listener Components");
        engine::gameplay::EntityManagerViewMut<const gameplay::TransformComponent,
                                               AudioListenerComponent>
            view(entity_manager);
        view.for_each([&audio_context](const gameplay::EntityId id,
                                       const gameplay::TransformComponent& tc,
                                       AudioListenerComponent&) {
            const math::Transform& world_transform = tc.world_transform();
            if (tc.did_world_transform_update()) {
                if (!audio_context.set_listener_position(world_transform.translation())) {
                    MODUS_LOGE(
                        "AudioSystem: Failed to set listener "
                        "position for Entity {}",
                        id);
                }
                const glm::vec3 up = world_transform.up();
                const glm::vec3 fwd = world_transform.forward();
                if (!audio_context.set_listener_orientation(fwd, up)) {
                    MODUS_LOGE(
                        "AudioSystem: Failed to set listener "
                        "orientation for Entity{}",
                        id);
                }
            }
        });
    }

    if (!m_handle_velocity) {
        return;
    }

    {
        MODUS_PROFILE_BLOCK("Update Audio Source Components Velocity", profiler::color::Green300);
        engine::gameplay::EntityManagerViewMut<const gameplay::VelocityComponent,
                                               AudioSourceComponent>
            view(entity_manager);
        view.for_each([&audio_context](const gameplay::EntityId id,
                                       const engine::gameplay::VelocityComponent& vc,
                                       AudioSourceComponent& ac) {
            if (!audio_context.set_source_velocity(ac.audio_source, vc.velocity)) {
                MODUS_LOGE(
                    "AudioVelocitySystem: Failed to set audio source "
                    "velocity for Entity {}",
                    id);
            }
        });
    }
    {
        MODUS_PROFILE_AUDIO_BLOCK("Update Audio Listener Components Velocity");
        engine::gameplay::EntityManagerViewMut<const gameplay::VelocityComponent,
                                               AudioListenerComponent>
            view(entity_manager);
        view.for_each([&audio_context](const gameplay::EntityId id,
                                       const gameplay::VelocityComponent& vc,
                                       AudioListenerComponent&) {
            if (!audio_context.set_listener_velocity(vc.velocity)) {
                MODUS_LOGE(
                    "AudioVelocitySystem: Failed to set audio listener "
                    "velocity for Entity {}",
                    id);
            }
        });
    }
}

void AudioSystem::update_threaded(ThreadSafeEngineAccessor&,
                                  const IMilisec,
                                  gameplay::ThreadSafeGameWorldAccessor&,
                                  gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("Audio System Can't currently be run threaded");
}

void AudioSystem::on_component_destroyed(Engine& engine, const engine::gameplay::Component& c) {
    const AudioSourceComponent& asc = static_cast<const AudioSourceComponent&>(c);
    auto& audio_context = engine.module<engine::ModuleAudio>()->context();
    (void)audio_context.destroy_source(asc.audio_source);
}
}    // namespace modus::engine::audio
