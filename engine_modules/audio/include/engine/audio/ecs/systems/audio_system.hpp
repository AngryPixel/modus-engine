/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/audio/module_includes.hpp>

#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::audio {

class MODUS_ENGINE_EXPORT AudioSystem final : public engine::gameplay::System {
   public:
   private:
    bool m_handle_velocity;

   public:
    AudioSystem(const bool handle_velocity);

    Result<> initialize(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager& em) override;

    void shutdown(Engine&, gameplay::GameWorld&, gameplay::EntityManager& em) override;

    void update(Engine& engine,
                const IMilisec tick,
                gameplay::GameWorld&,
                gameplay::EntityManager& entity_manager) override;

    void update_threaded(
        engine::ThreadSafeEngineAccessor& engine,
        const IMilisec tick,
        engine::gameplay::ThreadSafeGameWorldAccessor& game_world,
        engine::gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

   private:
    void on_component_destroyed(Engine& engine, const engine::gameplay::Component& c);
};

}    // namespace modus::engine::audio
