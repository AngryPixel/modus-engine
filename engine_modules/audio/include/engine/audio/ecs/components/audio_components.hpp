/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/audio/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>
#include <engine/gameplay/ecs/component_storage/unique_storage.hpp>

namespace modus::engine::audio::fbs::ecs {
struct AudioListenerComponent;
struct AudioSourceComponent;
}    // namespace modus::engine::audio::fbs::ecs

namespace modus::engine::audio {

struct MODUS_ENGINE_EXPORT AudioSourceComponent final : public gameplay::Component {
    ::modus::audio::AudioSourceHandle audio_source;
    bool directionless = true;

    void on_create(const gameplay::EntityId) override {
        audio_source = ::modus::audio::AudioSourceHandle();
        directionless = true;
    }
};

struct MODUS_ENGINE_EXPORT AudioListenerComponent final : public gameplay::Component {
    void on_create(const gameplay::EntityId) override {}
};

}    // namespace modus::engine::audio

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::engine::audio::AudioSourceComponent)
MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::engine::audio::AudioListenerComponent)

namespace modus::engine::gameplay {

template <>
struct ComponentTraits<modus::engine::audio::AudioSourceComponent> {
    using StorageType = SparseStorage<engine::audio::AudioSourceComponent>;
    using FlatbufferType = modus::engine::audio::fbs::ecs::AudioSourceComponent;
};
template <>
struct ComponentTraits<modus::engine::audio::AudioListenerComponent> {
    using StorageType = UniqueStorage<engine::audio::AudioListenerComponent>;
    using FlatbufferType = modus::engine::audio::fbs::ecs::AudioListenerComponent;
};
template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<modus::engine::audio::AudioSourceComponent> {
    using ComponentType = modus::engine::audio::AudioSourceComponent;
    using IntermediateType = modus::engine::audio::AudioSourceComponent;
    using StorageType = ComponentTraits<modus::engine::audio::AudioSourceComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.audio_source();
    }
};

template <>
struct ComponentLoaderTraits<modus::engine::audio::AudioListenerComponent> {
    using ComponentType = modus::engine::audio::AudioListenerComponent;
    using IntermediateType = modus::engine::audio::AudioListenerComponent;
    using StorageType =
        ComponentTraits<modus::engine::audio::AudioListenerComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&) {
        return Ok(IntermediateType());
    }
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&, ComponentType&, const IntermediateType&, const EntityId) {
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.audio_listener();
    }
};
}    // namespace modus::engine::gameplay
