/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/audio/module_includes.hpp>

#include <core/fixed_stack.hpp>
#include <engine/modules/module.hpp>

namespace modus::audio {
class AudioInstance;
class AudioDevice;
class AudioContext;
}    // namespace modus::audio

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleAudio final : public Module {
   private:
    std::unique_ptr<::modus::audio::AudioInstance> m_instance;
    std::unique_ptr<::modus::audio::AudioDevice> m_device;
    std::unique_ptr<::modus::audio::AudioContext> m_context;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleAudio();

    ~ModuleAudio();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    void tick(Engine&) override;

    Slice<GID> module_dependencies() const override;

    ::modus::audio::AudioDevice& device() {
        modus_assert(m_device);
        return *m_device;
    }

    const ::modus::audio::AudioDevice& device() const {
        modus_assert(m_device);
        return *m_device;
    }

    ::modus::audio::AudioContext& context() {
        modus_assert(m_device);
        return *m_context;
    }

    const ::modus::audio::AudioContext& context() const {
        modus_assert(m_device);
        return *m_context;
    }
};
}    // namespace modus::engine
