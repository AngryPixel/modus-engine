/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/physics/ecs/components/physics_component.hpp>
// clang-format on

#include <engine/modules/module_physics.hpp>
#include <engine/modules/module_assets.hpp>
#include <physics/world.hpp>
#include <physics/instance.hpp>
#include <physics/rigid_body.hpp>
#include <engine/engine.hpp>
#include <engine/physics/collision_mesh_asset.hpp>

#include <engine/physics/ecs/components/physics_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(engine::physics::PhysicsComponent)

namespace modus::engine::physics {

Result<modus::physics::CollisionShapeHandle> create_collision_shape(
    Engine& engine,
    const fbs::ecs::PhysicsComponent& fbs) {
    auto physics_module = engine.module<ModulePhysics>();

    auto& instance = physics_module->instance();

    switch (fbs.collision_shape_type()) {
        case fbs::ecs::CollisionShape::Box: {
            const auto* box = fbs.collision_shape_as_Box();
            if (box == nullptr) {
                MODUS_LOGE("Collison shape data for box type is not defined");
                return Error<>();
            }
            glm::vec3 extent = glm::vec3(1.0f);
            const auto* fbs_extent = box->extent();
            if (fbs_extent != nullptr) {
                extent.x = fbs_extent->x();
                extent.y = fbs_extent->y();
                extent.z = fbs_extent->z();
            }
            return instance.create_collision_shape_box(extent);
        }
        case fbs::ecs::CollisionShape::Sphere: {
            const auto* sphere = fbs.collision_shape_as_Sphere();
            if (sphere == nullptr) {
                MODUS_LOGE("Collison shape data for sphere type is not defined");
                return Error<>();
            }
            const float radius = sphere->radius();
            return instance.create_collision_shape_sphere(radius);
        }

        case fbs::ecs::CollisionShape::Plane: {
            const auto* plane = fbs.collision_shape_as_Plane();
            if (plane == nullptr) {
                MODUS_LOGE("Collison shape data for plane type is not defined");
                return Error<>();
            }
            glm::vec3 direction = glm::vec3(0.f, 1.0f, 0.f);
            const f32 offset = plane->offset();
            const auto* fbs_direction = plane->direction();
            if (fbs_direction != nullptr) {
                direction.x = fbs_direction->x();
                direction.y = fbs_direction->y();
                direction.z = fbs_direction->z();
                direction = glm::normalize(direction);
            }
            return instance.create_collision_shape_plane(direction, offset);
        }

        case fbs::ecs::CollisionShape::Mesh: {
            const auto* mesh = fbs.collision_shape_as_Mesh();
            if (mesh == nullptr) {
                MODUS_LOGE("Collision shape data for mesh type is not defiend");
                return Error<>();
            }

            const auto* fbs_asset_path = mesh->asset();
            if (fbs_asset_path == nullptr) {
                MODUS_LOGE("Collision shape has no path for for mesh data");
                return Error<>();
            }
            const StringSlice asset_path(fbs_asset_path->c_str(), fbs_asset_path->size());
            auto asset_module = engine.module<ModuleAssets>();
            auto r_asset =
                asset_module->loader().resolve_by_path_typed<engine::physics::CollisionMeshAsset>(
                    asset_path);

            if (!r_asset) {
                MODUS_LOGE("Could not locate collision mesh asset: {}", asset_path);
                return Error<>();
            }
            return Ok((*r_asset)->shape_handle());
        }

        default:
            MODUS_LOGE("Unknown collision shape type: {}", fbs.collision_shape_type());
            return Error<>();
    }
}

static inline math::Transform to_transform(const fbs::ecs::CollisionObjectRigidBody& rg) {
    math::Transform out = math::Transform::kIdentity;
    const auto* translation = rg.translation();
    if (translation != nullptr) {
        out.set_translation(translation->x(), translation->y(), translation->z());
    }
    const auto* rotation = rg.rotation();
    if (rotation != nullptr) {
        glm::quat rot(rotation->w(), rotation->x(), rotation->y(), rotation->z());
        out.set_rotation(rot);
    }
    out.set_scale(rg.scale());
    return out;
}

static inline glm::vec3 to_factor_vec(const modus::math::fbs::Vec3* vec) {
    glm::vec3 out = glm::vec3(1.f);
    if (vec != nullptr) {
        out.x = vec->x();
        out.y = vec->y();
        out.z = vec->z();
    }
    return out;
}
}    // namespace modus::engine::physics

namespace modus::engine::gameplay {

Result<engine::physics::PhysicsComponentCreateParams>
ComponentLoaderTraits<engine::physics::PhysicsComponent>::load(Engine& engine,
                                                               const StorageType& storage) {
    engine::physics::PhysicsComponentCreateParams params;

    if (storage.collision_object_type() != engine::physics::fbs::ecs::CollisionObject::RigidBody) {
        MODUS_LOGE("Only rigid bodies are supported");
        return Error<>();
    }

    const auto fbs_rigid_body = storage.collision_object_as_RigidBody();
    if (fbs_rigid_body == nullptr) {
        MODUS_LOGE("Rigid body parameters are not defined");
        return Error<>();
    }

    params.rigid_body_params.transform = engine::physics::to_transform(*fbs_rigid_body);
    params.rigid_body_params.mass = fbs_rigid_body->mass();
    params.rigid_body_params.restitution = fbs_rigid_body->restitution();
    params.rigid_body_params.friction = fbs_rigid_body->friction();
    params.rigid_body_params.angular_damping = fbs_rigid_body->angular_damping();
    params.rigid_body_params.linear_damping = fbs_rigid_body->linear_damping();
    params.rigid_body_params.angular_factor =
        engine::physics::to_factor_vec(fbs_rigid_body->angular_factor());
    params.rigid_body_params.linear_factor =
        engine::physics::to_factor_vec(fbs_rigid_body->linear_factor());
    params.rigid_body_params.disable_collision_response =
        fbs_rigid_body->disable_collision_response();
    params.rigid_body_params.kinematic = fbs_rigid_body->kinematic();

    const auto fbs_rigid_body_ccd = fbs_rigid_body->ccd();
    if (fbs_rigid_body_ccd != nullptr) {
        params.rigid_body_params.continuous_collision_detection =
            modus::physics::RigidBodyCreateParams::CCD{fbs_rigid_body_ccd->motion_threshold(),
                                                       fbs_rigid_body_ccd->sphere_radius()};
    }

    // Create collision shape
    auto r_shape = engine::physics::create_collision_shape(engine, storage);
    if (!r_shape) {
        return Error<>();
    }

    params.rigid_body_params.shape_handle = *r_shape;
    return Ok(params);
}

void ComponentLoaderTraits<engine::physics::PhysicsComponent>::unload(Engine& engine,
                                                                      const IntermediateType& in) {
    if (in.rigid_body_params.shape_handle) {
        auto physics_module = engine.module<ModulePhysics>();
        if (!physics_module->instance().destroy_collision_shape(
                in.rigid_body_params.shape_handle)) {
            MODUS_LOGW("Failed to destroy collision shape");
        }
    }
}

void ComponentLoaderTraits<engine::physics::PhysicsComponent>::extract_asset_paths(
    Vector<String>& out,
    const StorageType& storage) {
    if (storage.collision_shape_type() == engine::physics::fbs::ecs::CollisionShape::Mesh) {
        const auto* fbs_mesh = storage.collision_shape_as_Mesh();
        if (fbs_mesh != nullptr) {
            const auto* fbs_string = fbs_mesh->asset();
            modus_assert(fbs_string);
            out.push_back(String(fbs_string->data(), fbs_string->size()));
        }
    }
}

Result<> ComponentLoaderTraits<engine::physics::PhysicsComponent>::to_component(
    Engine& engine,
    ComponentType& out,
    const IntermediateType& in,
    const EntityId id) {
    auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();

    modus::physics::RigidBodyCreateParams params = in.rigid_body_params;
    params.user_data = reinterpret_cast<void*>(usize(id.value()));

    auto r = physics_instance.create_rigid_body(params);
    if (!r) {
        MODUS_LOGE("Failed to create rigid body");
        return Error<>();
    }

    out.m_handle = *r;
    return Ok<>();
}
}    // namespace modus::engine::gameplay
