/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/physics/ecs/components/physics_component.hpp>
#include <engine/physics/ecs/systems/physics_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/components/velocity_component.hpp>
#include <engine/modules/module_physics.hpp>
#include <physics/world.hpp>
#include <physics/instance.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/gameplay/gameworld.hpp>

MODUS_GAMEPLAY_GAMEWORLD_STORAGE_IMPL(modus::physics::PhysicsWorld)

namespace modus::engine::physics {

PhysicsSystem::PhysicsSystem() : gameplay::System("Physics") {}

Result<> PhysicsSystem::initialize(Engine& engine,
                                   gameplay::GameWorld&,
                                   gameplay::EntityManager& em) {
    auto listener = engine::gameplay::ComponentDestroyedDelegate::build<
        PhysicsSystem, &PhysicsSystem::on_component_destroyed>(this);

    auto physics_module = engine.module<ModulePhysics>();
    if (!physics_module) {
        MODUS_LOGE("PhysicsSystem: Physics module {} is not in module list",
                   module_guid<ModulePhysics>());
        return Error<>();
    }

    if (!em.register_component_destroy_callback<PhysicsComponent>(listener)) {
        MODUS_LOGE(
            "PhysicsSystem: Failed to register component destroyed "
            "callback");
        return Error<>();
    }
    return Ok<>();
}

void PhysicsSystem::shutdown(Engine&, gameplay::GameWorld&, gameplay::EntityManager& em) {
    em.unregister_component_destroy_callback<PhysicsComponent>();
}

void PhysicsSystem::update(Engine& engine,
                           const IMilisec tick,
                           gameplay::GameWorld& game_world,
                           gameplay::EntityManager& entity_manager) {
    auto physics_module = engine.module<ModulePhysics>();
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("PhysicsSystem: Physics World has not been set!");
        return;
    }
    auto& physics_instance = physics_module->instance();

    {
        MODUS_PROFILE_PHYSICS_BLOCK("PhysicsWorld::Update");
        physics_world->update(FSeconds(tick).count());
    }
    {
        MODUS_PROFILE_PHYSICS_BLOCK("Update Physics Component");
        engine::gameplay::EntityManagerViewMut<gameplay::TransformComponent, const PhysicsComponent>
            view(entity_manager);
        view.for_each([&physics_instance](const gameplay::EntityId,
                                          gameplay::TransformComponent& tc,
                                          const PhysicsComponent& pc) {
            auto r_transform = physics_instance.rigid_body_transform(pc.m_handle);
            if (r_transform) {
                tc.set_translation(r_transform->translation());
                tc.set_rotation(r_transform->rotation());
            }
        });
    }
    {
        MODUS_PROFILE_PHYSICS_BLOCK("Update Velocity Component");
        engine::gameplay::EntityManagerViewMut<gameplay::VelocityComponent, const PhysicsComponent>
            view(entity_manager);
        view.for_each([&physics_instance](const gameplay::EntityId, gameplay::VelocityComponent& vc,
                                          const PhysicsComponent& pc) {
            auto r_velocity = physics_instance.rigid_body_linear_velocity(pc.m_handle);
            if (r_velocity) {
                vc.velocity = *r_velocity;
            }
        });
    }
}

void PhysicsSystem::update_threaded(ThreadSafeEngineAccessor&,
                                    const IMilisec,
                                    gameplay::ThreadSafeGameWorldAccessor&,
                                    gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("Physics system can't be used from threads!");
}

void PhysicsSystem::on_component_destroyed(Engine& engine, const engine::gameplay::Component& c) {
    const PhysicsComponent& pc = static_cast<const PhysicsComponent&>(c);
    auto physics_module = engine.module<ModulePhysics>();
    auto& physics_instance = physics_module->instance();
    (void)physics_instance.destroy_rigid_body(pc.m_handle);
}

}    // namespace modus::engine::physics
