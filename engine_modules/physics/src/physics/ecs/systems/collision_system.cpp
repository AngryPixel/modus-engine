/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/physics/ecs/systems/collision_system.hpp>
#include <engine/modules/module_physics.hpp>
#include <engine/physics/ecs/components/collision_component.hpp>
#include <physics/world.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/gameplay/gameworld.hpp>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(engine::physics::CollisionComponent)

namespace modus::engine::physics {

CollisionSystem::CollisionSystem() : engine::gameplay::System("Collision") {}

Result<> CollisionSystem::initialize(Engine&,
                                     gameplay::GameWorld& game_world,
                                     gameplay::EntityManager& entity_manager) {
    m_collisions.reserve(32);

    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (!physics_world) {
        MODUS_LOGE("CollisionSystem: Physics world has not been set!");
        return Error<>();
    }
    auto physics_callback =
        modus::physics::CollisionBeginDelegate::build<CollisionSystem,
                                                      &CollisionSystem::on_physics_collision>(this);
    physics_world->set_collision_callbacks(physics_callback,
                                           modus::physics::CollisionEndDelegate());
    m_entity_manager = &entity_manager;
    return Ok<>();
}

void CollisionSystem::shutdown(Engine&, gameplay::GameWorld& game_world, gameplay::EntityManager&) {
    auto physics_world = game_world.from_storage<modus::physics::PhysicsWorld>();
    if (physics_world) {
        physics_world->set_collision_callbacks(modus::physics::CollisionBeginDelegate(),
                                               modus::physics::CollisionEndDelegate());
    }
    m_entity_manager.reset();
}

void CollisionSystem::update(Engine&,
                             const IMilisec,
                             gameplay::GameWorld&,
                             gameplay::EntityManager&) {
    // Nothing to do
}

void CollisionSystem::update_threaded(ThreadSafeEngineAccessor&,
                                      const IMilisec,
                                      gameplay::ThreadSafeGameWorldAccessor&,
                                      gameplay::ThreadSafeEntityManagerAccessor&) {
    // nothing to do
}

static inline glm::vec3 contact_point_average(Slice<glm::vec3> contact_points) {
    if (contact_points.size() == 1) {
        return contact_points[0];
    }
    glm::vec3 sum(0.f);
    for (const auto& point : contact_points) {
        sum += point;
    }
    return sum / f32(contact_points.size());
}

void CollisionSystem::on_physics_collision(modus::physics::CollisionBeginInfo& collision_info) {
    MODUS_PROFILE_PHYSICS_BLOCK("CollisionSystem::on_physics_collision");
    const usize entity_id_value1 = reinterpret_cast<usize>(collision_info.object1.user_data.get());
    const usize entity_id_value2 = reinterpret_cast<usize>(collision_info.object2.user_data.get());
    const engine::gameplay::EntityId id1(entity_id_value1);
    const engine::gameplay::EntityId id2(entity_id_value2);

    modus_assert(m_entity_manager);

    auto r_cc1 = m_entity_manager->component<CollisionComponent>(id1);
    auto r_cc2 = m_entity_manager->component<CollisionComponent>(id2);

    if (!r_cc1 && !r_cc2) {
        return;
    }

    CollisionInfoV2 collision;
    collision.contact_point = contact_point_average(collision_info.contact_points);
    collision.sum_normal_force = collision_info.sum_normal_force;
    collision.sum_friction_force = collision_info.sum_friction_force;
    m_collisions.push_back(collision);

    if (r_cc1 &&
        !(*r_cc1)->m_collisions.push_back(CollisionComponent::Info{&m_collisions.back(), id2})) {
        MODUS_LOGE(
            "CollisionSystem: Could not add collision info, max collision "
            "records reached");
    }

    if (r_cc2 &&
        !(*r_cc2)->m_collisions.push_back(CollisionComponent::Info{&m_collisions.back(), id1})) {
        MODUS_LOGE(
            "CollisionSystem: Could not add collision info, max collision "
            "records reached");
    }
}

void CollisionSystem::post_update(Engine&,
                                  gameplay::GameWorld&,
                                  gameplay::EntityManager& entity_manager) {
    engine::gameplay::EntityManagerViewMut<CollisionComponent> view(entity_manager);
    view.for_each(
        [](const engine::gameplay::EntityId, CollisionComponent& cc) { cc.m_collisions.clear(); });
}
}    // namespace modus::engine::physics
