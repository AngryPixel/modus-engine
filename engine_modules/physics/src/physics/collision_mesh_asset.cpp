/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/physics/collision_mesh_asset.hpp>
// clang-format on

#include <engine/physics/collision_mesh_generated.h>
#include <engine/modules/module_physics.hpp>
#include <engine/engine.hpp>
#include <physics/instance.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::physics::CollisionMeshAsset)

namespace modus::engine::physics {

CollisionMeshAsset::CollisionMeshAsset(const modus::physics::CollisionShapeHandle handle,
                                       RamStream<PhysicsAllocator>&& data)
    : assets::AssetBase(assets::AssetTraits<CollisionMeshAsset>::type_id()),
      m_shape(handle),
      m_data(std::move(data)) {}

CollisionMeshAssetFactory::CollisionMeshAssetFactory() : factory_type(32) {}

class CollisionMeshAssetData final : public assets::AssetData {
   public:
    RamStream<assets::AssetsAllocator> m_ram_stream;
    GID m_guid;
    modus::physics::TriangleMeshShapeCreateParams m_params;
    bool m_is_static = false;

   public:
    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        MODUS_PROFILE_PHYSICS_BLOCK("CollisionMeshAssetLoad");
        auto r = m_ram_stream.populate_from_stream(reader);
        if (!r) {
            return Error<>();
        }
        m_ram_stream.seek(0).expect("Failed to seek beginning of stream");

        const ByteSlice bytes = m_ram_stream.as_bytes();
        flatbuffers::Verifier v(bytes.data(), bytes.size());
        if (!fbs::VerifyCollisionMeshBuffer(v)) {
            MODUS_LOGE("CollisionMeshAsset: File is not a collision mesh!");
            return Error<>();
        }

        const fbs::CollisionMesh* fbs_mesh = fbs::GetCollisionMesh(bytes.data());

        const ByteSlice guid_slice(fbs_mesh->guid()->value()->data(),
                                   fbs_mesh->guid()->value()->size());
        auto r_guid = GID::from_slice(guid_slice);
        if (!r_guid) {
            MODUS_LOGE("CollisionMeshAsset: Asset contains invalid guid");
            return Error<>();
        }
        m_guid = *r_guid;

        auto fbs_vertices = fbs_mesh->vertices();
        if (fbs_vertices == nullptr) {
            MODUS_LOGE("CollisionMeshAsset: Asset does not contain vertices");
            return Error<>();
        }

        if (fbs_vertices->size() == 0) {
            MODUS_LOGE("CollisionMeshAsset: Asset contains an empty vertices vector");
            return Error<>();
        }

        m_params.vertices = Slice<f32>(fbs_vertices->data(), fbs_vertices->size());
        m_params.vertices_stride = 0;
        m_params.triangle_count = fbs_vertices->size() / 3;
        m_params.num_vertices = m_params.triangle_count;
        m_params.indices_stride = 3 * sizeof(u32);
        m_params.vertices_stride = 3 * sizeof(f32);
        m_params.index_type = modus::physics::TriangleMeshShapeCreateParams::IndexType::None;

        if (auto fbs_indices = fbs_mesh->indices(); fbs_indices != nullptr) {
            m_params.index_type = modus::physics::TriangleMeshShapeCreateParams::IndexType::U32;
            m_params.indices = Slice<u32>(fbs_indices->data(), fbs_indices->size()).as_bytes();
            m_params.triangle_count = fbs_indices->size() / 3;
        }
        m_is_static = fbs_mesh->static_();
        return Ok(assets::AssetLoadResult::Done);
    }
};

StringSlice CollisionMeshAssetFactory::file_extension() const {
    return "cmesh";
}

std::unique_ptr<assets::AssetData> CollisionMeshAssetFactory::create_asset_data() {
    return std::make_unique<CollisionMeshAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> CollisionMeshAssetFactory::create(Engine& engine,
                                                                      assets::AssetData& data) {
    MODUS_PROFILE_PHYSICS_BLOCK("CollisionMeshAssetCreate");
    CollisionMeshAssetData& cdata = static_cast<CollisionMeshAssetData&>(data);

    auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();

    Result<modus::physics::CollisionShapeHandle> r_shape = Error<>();
    if (cdata.m_is_static) {
        r_shape = physics_instance.create_collision_shape_static_triangle_mesh(cdata.m_params);
    } else {
        r_shape = physics_instance.create_collision_shape_convex_triangle_mesh(cdata.m_params);
    }
    if (!r_shape) {
        MODUS_LOGE("CollisionMeshAsset: Failed to create collision shape\n");
        return Error<>();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(
        m_asset_pool.construct(*r_shape, std::move(cdata.m_ram_stream)));
}

Result<> CollisionMeshAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_PHYSICS_BLOCK("CollisionMeshAssetDestroy");
    auto r_asset = assets::assetv2_cast<CollisionMeshAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    auto& physics_instance = engine.module<engine::ModulePhysics>()->instance();
    if ((*r_asset)->m_shape) {
        if (!physics_instance.destroy_collision_shape((*r_asset)->m_shape)) {
            MODUS_LOGW("CollisionMeshAsset: Failed to destroy collision shape");
        }
    }
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}

}    // namespace modus::engine::physics
