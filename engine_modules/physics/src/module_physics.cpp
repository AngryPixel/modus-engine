/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_physics.hpp>
#include <physics/instance.hpp>
#include <physics/world.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModulePhysics, "5bf9bea0-e14e-49c5-bb91-13dbf3073b53");

ModulePhysics::ModulePhysics()
    : Module(module_guid<ModulePhysics>(), "Physics", ModuleUpdateCategory::Total) {}

ModulePhysics::~ModulePhysics() {}

Result<> ModulePhysics::initialize(Engine& engine) {
    MODUS_PROFILE_PHYSICS_BLOCK("ModulePhysics::initialize");
    m_instance = modus::physics::create_default_instance();

    if (!m_instance->initialize()) {
        MODUS_LOGE("Physics: Failed to initialize physics instance");
        return Error<>();
    }

    auto asset_module = engine.module<ModuleAssets>();
    if (!asset_module) {
        MODUS_LOGE("Expected Assets module {} is not in module list {}",
                   module_guid<ModuleAssets>());
        return Error<>();
    }

    assets::AssetLoader& asset_loader = asset_module->loader();
    if (!asset_loader.register_factory(&m_cmesh_asset_context)) {
        MODUS_LOGE("Physics: Failed to register collision mesh asset factory");
        return Error<>();
    }

    return Ok<>();
}

Result<> ModulePhysics::shutdown(Engine& engine) {
    MODUS_PROFILE_PHYSICS_BLOCK("ModulePhysics::shutdown");

    if (m_instance) {
        if (!m_instance->shutdown()) {
            MODUS_LOGW("ModulePysics: Failed to properly shutdown instance");
        }
    }

    auto asset_module = engine.module<ModuleAssets>();
    if (asset_module) {
        assets::AssetLoader& asset_loader = asset_module->loader();
        asset_loader.unregister_factory(engine, &m_cmesh_asset_context);
    }

    m_instance.reset();
    return Ok<>();
}

void ModulePhysics::tick(Engine&) {}

Slice<GID> ModulePhysics::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleAssets>()};
    return Slice(deps);
}

std::unique_ptr<modus::physics::PhysicsWorld> ModulePhysics::create_world() {
    modus_assert(m_instance);
    return m_instance->create_world();
}

}    // namespace modus::engine
