/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/physics/module_includes.hpp>

#include <core/fixed_stack.hpp>
#include <engine/modules/module.hpp>
#include <engine/physics/collision_mesh_asset.hpp>

namespace modus::physics {
class Instance;
class PhysicsWorld;
class CollisionShapeDB;
}    // namespace modus::physics

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModulePhysics final : public Module {
   private:
    std::unique_ptr<modus::physics::Instance> m_instance;
    physics::CollisionMeshAssetFactory m_cmesh_asset_context;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModulePhysics();

    ~ModulePhysics();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    void tick(Engine&) override;

    Slice<GID> module_dependencies() const override;

    modus::physics::Instance& instance() {
        modus_assert(m_instance);
        return *m_instance;
    }

    std::unique_ptr<modus::physics::PhysicsWorld> create_world();
};

}    // namespace modus::engine
