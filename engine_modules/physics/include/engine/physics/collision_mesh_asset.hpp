/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/physics/module_includes.hpp>

#include <core/io/memory_stream.hpp>
#include <engine/assets/asset_types.hpp>
#include <physics/collision_shape.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::physics {
class CollisionMeshDB;

class MODUS_ENGINE_EXPORT CollisionMeshAsset final : public assets::AssetBase {
    friend class CollisionMeshAssetFactory;

   private:
    modus::physics::CollisionShapeHandle m_shape;
    RamStream<PhysicsAllocator> m_data;

   public:
    CollisionMeshAsset(const modus::physics::CollisionShapeHandle handle,
                       RamStream<PhysicsAllocator>&& data);

    MODUS_CLASS_DISABLE_COPY_MOVE(CollisionMeshAsset);

    modus::physics::CollisionShapeHandle shape_handle() const { return m_shape; }
};

class CollisionMeshAssetFactory final
    : public assets::PooledAssetFactory<CollisionMeshAsset, PhysicsAllocator> {
   private:
   public:
    CollisionMeshAssetFactory();

    MODUS_CLASS_DISABLE_COPY_MOVE(CollisionMeshAssetFactory);

    StringSlice file_extension() const override;

    std::unique_ptr<assets::AssetData> create_asset_data() override;

    Result<NotMyPtr<assets::AssetBase>> create(Engine&, assets::AssetData& data) override;

    Result<> destroy(Engine&, NotMyPtr<assets::AssetBase> asset) override;
};

}    // namespace modus::engine::physics

MODUS_ENGINE_ASSET_TYPE_DECLARE(modus::engine::physics::CollisionMeshAsset)
