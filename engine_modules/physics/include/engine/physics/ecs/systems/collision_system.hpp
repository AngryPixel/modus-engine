/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/physics/module_includes.hpp>

#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::physics {
struct CollisionBeginInfo;
}

namespace modus::engine::physics {

struct CollisionInfoV2 {
    glm::vec3 contact_point;
    glm::vec3 sum_normal_force;
    glm::vec3 sum_friction_force;
};

class MODUS_ENGINE_EXPORT CollisionSystem final : public engine::gameplay::System {
   private:
    physics::Vector<CollisionInfoV2> m_collisions;
    NotMyPtr<engine::gameplay::EntityManager> m_entity_manager;

   public:
    CollisionSystem();

    Result<> initialize(Engine&,
                        gameplay::GameWorld&,
                        gameplay::EntityManager& entity_manager) override;

    void shutdown(Engine& engine,
                  gameplay::GameWorld&,
                  gameplay::EntityManager& entity_manager) override;

    void update(Engine& engine,
                const IMilisec tick,
                gameplay::GameWorld&,
                gameplay::EntityManager& entity_manager) override;

    void update_threaded(ThreadSafeEngineAccessor& engine,
                         const IMilisec tick,
                         gameplay::ThreadSafeGameWorldAccessor& game_world,
                         gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void post_update(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) override;

   private:
    void on_physics_collision(modus::physics::CollisionBeginInfo& collision);
};
}    // namespace modus::engine::physics
