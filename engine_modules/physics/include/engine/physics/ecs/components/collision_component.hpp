/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/physics/module_includes.hpp>

#include <core/delegate.hpp>
#include <core/fixed_vector.hpp>
#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>

namespace modus::engine::physics {
namespace fbs::ecs {
struct CollisionComponent;
}

struct CollisionInfoV2;

struct CollisionComponent final : public engine::gameplay::Component {
    struct Info {
        NotMyPtr<const CollisionInfoV2> m_info;
        engine::gameplay::EntityId m_other_id;
    };
    using ResponseDelegate = Delegate<void(engine::Engine&,
                                           engine::gameplay::EntityManager&,
                                           const Info&,
                                           const engine::gameplay::EntityId)>;
    using ResponseFn = void (*)(engine::Engine&, const Info&, const engine::gameplay::EntityId);
    static constexpr u32 kMaxCollisionRecords = 4;

    FixedVector<Info, kMaxCollisionRecords> m_collisions;

    void on_create(const engine::gameplay::EntityId) override { m_collisions.clear(); }
};

}    // namespace modus::engine::physics

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<engine::physics::CollisionComponent> {
    using StorageType = SparseStorage<engine::physics::CollisionComponent>;
    using FlatbufferType = engine::physics::fbs::ecs::CollisionComponent;
};

template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<modus::engine::physics::CollisionComponent> {
    struct Empty {};
    using AllowMissingComponent = std::true_type;
    using ComponentType = modus::engine::physics::CollisionComponent;
    using IntermediateType = Empty;
    using StorageType = ComponentTraits<modus::engine::physics::CollisionComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&) {
        return Ok(IntermediateType());
    }
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&, ComponentType&, const IntermediateType&, const EntityId) {
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.collision();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::engine::physics::CollisionComponent)
