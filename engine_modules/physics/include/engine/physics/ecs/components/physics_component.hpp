/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/physics/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>

#include <physics/rigid_body.hpp>

namespace modus::engine::physics {

struct MODUS_ENGINE_EXPORT PhysicsComponentCreateParams {
    modus::physics::RigidBodyCreateParams rigid_body_params;
};

namespace fbs::ecs {
struct PhysicsComponent;
}

struct MODUS_ENGINE_EXPORT PhysicsComponent final : public gameplay::Component {
    modus::physics::RigidBodyHandle m_handle;
    gameplay::EntityId m_this_id;

    void on_create(const gameplay::EntityId id) override {
        m_this_id = id;
        m_handle = modus::physics::RigidBodyHandle();
    }
};

}    // namespace modus::engine::physics

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<engine::physics::PhysicsComponent> {
    using StorageType = SparseStorage<engine::physics::PhysicsComponent>;
    using FlatbufferType = engine::physics::fbs::ecs::PhysicsComponent;
};

template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<modus::engine::physics::PhysicsComponent> {
    using ComponentType = modus::engine::physics::PhysicsComponent;
    using IntermediateType = modus::engine::physics::PhysicsComponentCreateParams;
    using StorageType = ComponentTraits<modus::engine::physics::PhysicsComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&);
    static void extract_asset_paths(Vector<String>&, const StorageType&);
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId);
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.physics();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(modus::engine::physics::PhysicsComponent)
