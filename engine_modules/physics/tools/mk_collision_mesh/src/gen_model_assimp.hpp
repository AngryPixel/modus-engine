/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

struct AssimpOptions {
    const char* input_file = nullptr;
    bool verbose = false;
    f32 scale = 1.0f;
};

Result<> gen_model_assimp(modus::Vector<f32>& vertices_out,
                          modus::Vector<u32>& indices_out,
                          const GID& guid,
                          const AssimpOptions& options);
