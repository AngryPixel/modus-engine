/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include "gen_model_assimp.hpp"

using namespace modus;

static inline core::fbs::GUID to_fbs_guid(const GID& guid) {
    core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }
    return fbs_guid;
}

static const StringSlice kIntro =
    "Generate Collision Meshes for the Engine.\n modus_mk_collision_mesh "
    "[options] ...";

int main(const int argc, const char** argv) {
    CmdOptionString opt_input("-i", "--input", "Path to input file", "");
    CmdOptionString opt_output("-o", "--output", "Path for output file", "");
    CmdOptionF32 opt_scale("-s", "--scale", "Scale to apply to model. Default = 1.0f", 1.0f);
    CmdOptionString opt_guid("-g", "--guid", "Use provided guid instead of generating one", "");
    CmdOptionEmpty opt_static("", "--static",
                              "Indicate that this should be treated as a static collision mesh");
    CmdOptionEmpty opt_verbose("-v", "--verbose", "Verbose ouptput");

    CmdOptionParser opt_parser;

    opt_parser.add(opt_output).expect("Failed to add cmd parser option");
    opt_parser.add(opt_input).expect("Failed to add cmd parser option");
    opt_parser.add(opt_guid).expect("Failed to add cmd parser option");
    opt_parser.add(opt_scale).expect("Failed to add cmd parser option");
    opt_parser.add(opt_static).expect("Failed to add cmd parser option");

    opt_parser.add(opt_verbose).expect("Failed to add cmd parser option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (!opt_input.parsed()) {
        std::cerr << fmt::format("No input file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (!opt_output.parsed()) {
        std::cerr << fmt::format("No output file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    GID guid;
    if (!opt_guid.parsed()) {
        auto r_guid = os::guid::generate();
        if (!r_guid) {
            std::cerr << "Failed to generate GUID\n";
            return EXIT_FAILURE;
        }
        guid = *r_guid;
    } else {
        auto r_guid = GID::from_string(opt_guid.value());
        if (!r_guid) {
            std::cerr << fmt::format("GUID '{}' is not a valid GUID type\n", opt_guid.value());
            return EXIT_FAILURE;
        };
        guid = r_guid.value();
    }

    modus::Vector<f32> vertices;
    modus::Vector<u32> indices;

    AssimpOptions options;
    options.input_file = opt_input.value_cstr();
    options.verbose = opt_verbose.parsed();
    options.scale = opt_scale.value();
    auto r_gen = gen_model_assimp(vertices, indices, guid, options);
    if (!r_gen) {
        return EXIT_FAILURE;
    }

    if (opt_verbose.parsed()) {
        std::cout << fmt::format("Vertices: {}\n", vertices.size());
        std::cout << fmt::format("Indices : {}\n", indices.size());
    }

    flatbuffers::FlatBufferBuilder builder;
    auto fbs_guid = to_fbs_guid(guid);
    auto fbs_vertices = builder.CreateVector<f32>(vertices.data(), vertices.size());
    flatbuffers::Offset<flatbuffers::Vector<u32>> fbs_indices;
    if (!indices.empty()) {
        fbs_indices = builder.CreateVector<u32>(indices.data(), indices.size());
    }

    fbs::CollisionMeshBuilder cmesh_builder(builder);
    cmesh_builder.add_guid(&fbs_guid);
    cmesh_builder.add_vertices(fbs_vertices);
    cmesh_builder.add_indices(fbs_indices);
    cmesh_builder.add_static_(opt_static.parsed());
    auto fbs_cmesh = cmesh_builder.Finish();
    // Finish build
    builder.Finish(fbs_cmesh, "MPCM");

    auto r_output = os::FileBuilder().write().create().open(opt_output.value());
    if (!r_output) {
        std::cerr << fmt::format("Failed to open output file: {}\n", opt_output.value());
        return EXIT_FAILURE;
    }

    const ByteSlice builder_slice = ByteSlice(builder.GetBufferPointer(), builder.GetSize());

    auto r_write = r_output.value().write(builder_slice);
    if (!r_write || r_write.value() != builder_slice.size()) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", opt_output.value());
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
