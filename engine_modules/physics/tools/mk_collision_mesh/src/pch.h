/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/engine_pch.h>
#include <engine/physics/collision_mesh_generated.h>

#include <core/getopt.hpp>
#include <core/io/byte_stream.hpp>
#include <core/io/memory_stream.hpp>
#include <iostream>
#include <math/bounding_volumes.hpp>
#include <os/file.hpp>
#include <os/guid_gen.hpp>
#include <os/path.hpp>

using namespace modus;
using namespace modus::engine::physics;
