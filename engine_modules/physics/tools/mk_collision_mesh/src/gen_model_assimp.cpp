/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include "gen_model_assimp.hpp"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

struct AssimpMeshInfo {
    modus::Vector<f32> vertices;
    modus::Vector<u32> indices;
    math::bv::Sphere bv_sphere;
    math::bv::AABB bv_aabb;
};

Result<> process_mesh(AssimpMeshInfo& info, const aiMesh& mesh) {
    info.vertices.resize(mesh.mNumVertices * 3);
    u32 data_idx = 0;
    for (u32 i = 0; i < mesh.mNumVertices; ++i) {
        // copy vertex
        const aiVector3D& v = mesh.mVertices[i];
        info.vertices[data_idx++] = v.x;
        info.vertices[data_idx++] = v.y;
        info.vertices[data_idx++] = v.z;
    }
    modus_assert(data_idx == info.vertices.size());

    modus_assert(mesh.HasFaces());
    if (mesh.HasFaces()) {
        info.indices.reserve(mesh.mNumFaces * 3);
        for (u32 i = 0; i < mesh.mNumFaces; ++i) {
            const aiFace& face = mesh.mFaces[i];
            if (face.mNumIndices == 3) {
                for (u32 j = 0; j < face.mNumIndices; ++j) {
                    info.indices.push_back(face.mIndices[j]);
                }
            } else {
                std::cerr << fmt::format("Mesh Face {} does not have 3 indices: nIndices={}\n", i,
                                         face.mNumIndices);
                return Error<>();
            }
        }
    }

    // Bounding volume extraction;
    const glm::vec3 aabb_max = glm::vec3(mesh.mAABB.mMax.x, mesh.mAABB.mMax.y, mesh.mAABB.mMax.z);
    const glm::vec3 aabb_min = glm::vec3(mesh.mAABB.mMin.x, mesh.mAABB.mMin.y, mesh.mAABB.mMin.z);
    info.bv_aabb = math::bv::make_aabb(aabb_max, aabb_min);
    info.bv_sphere = math::bv::make_sphere(info.bv_aabb);
    return Ok<>();
}

Result<> gen_model_assimp(modus::Vector<f32>& vertices_out,
                          modus::Vector<u32>& indices_out,
                          const GID& guid,
                          const AssimpOptions& options) {
    MODUS_UNUSED(guid);
    modus_assert(options.input_file != nullptr);

    Assimp::Importer importer;

    // Apply scale to model
    importer.SetPropertyFloat(AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, options.scale);

    // Generate bounding box
    unsigned int assimp_flags = aiProcess_GenBoundingBoxes;

    // Apply global scale value
    assimp_flags |= aiProcess_GlobalScale;

    // Ensure we always get triangles to draw
    assimp_flags |= aiProcess_Triangulate;

    // Reduce draw calls (indexing)
    assimp_flags |= aiProcess_JoinIdenticalVertices;

    // Exclude lines and points
    assimp_flags |= aiProcess_SortByPType;

    // Improve cache locality
    assimp_flags |= aiProcess_ImproveCacheLocality;

    // Fix invalid data
    assimp_flags |= aiProcess_FindInvalidData;

    // Reduce mesh duplication
    assimp_flags |= aiProcess_FindInstances;

    // Optimize mesh
    assimp_flags |= aiProcess_OptimizeMeshes;
    assimp_flags |= aiProcess_OptimizeGraph;

    // Verify Model
    assimp_flags |= aiProcess_ValidateDataStructure;

    const aiScene* scene = importer.ReadFile(options.input_file, assimp_flags);
    if (scene == nullptr) {
        std::cerr << fmt::format("Failed to read model: {}\n", options.input_file);
        return Error<>();
    }

    if (scene->mNumMeshes > 1) {
        std::cerr << fmt::format(
            "Collision meshes can only have {} submesh per model at "
            "the moment. {} were detected.\n",
            1, scene->mNumMeshes);
        return Error<>();
    }

    if (scene->mNumMeshes > 1) {
        std::cerr << fmt::format("Currently Hard Coded to 1 submesh\n");
        return Error<>();
    }

    // Process materials

    AssimpMeshInfo mesh_info;
    for (u32 m = 0; m < scene->mNumMeshes; ++m) {
        const aiMesh* mesh = scene->mMeshes[m];

        if (!process_mesh(mesh_info, *mesh)) {
            std::cerr << fmt::format("Failed to process sub mesh {}\n", m);
            return Error<>();
        }
    }

    // Calculate BV
    math::bv::Sphere bv_sphere;
    bv_sphere = mesh_info.bv_sphere;
    for (u32 m = 1; m < scene->mNumMeshes; ++m) {
        math::bv::merge(bv_sphere, mesh_info.bv_sphere);
    }

    // TODO: this probably breaks multi-mesh setups!!
    // Center model on y = 0;
    const f32 y_translation = bv_sphere.center.y > 0.0f ? -bv_sphere.center.y : bv_sphere.center.y;
    bv_sphere.center.y += y_translation;
    for (u32 m = 0; m < scene->mNumMeshes; ++m) {
        AssimpMeshInfo& info = mesh_info;
        usize index = 0;
        for (; index < info.vertices.size(); index += 3) {
            // Apply translation to y component
            info.vertices[index + 1] += y_translation;
        }
        modus_assert(index == info.vertices.size());
    }

    // TODO: Allow multiple submesh collision shapes and then create a
    // compound shape???
    vertices_out = std::move(mesh_info.vertices);
    indices_out = std::move(mesh_info.indices);
    return Ok<>();
}
