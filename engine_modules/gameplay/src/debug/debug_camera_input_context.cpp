/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/gameplay/debug/debug_camera_input_context.hpp>
#include <engine/input/default_input_priorities.hpp>

namespace modus::engine {

static constexpr u32 kActionMovementYId = 0;
static constexpr u32 kActionMovementXId = 1;
static constexpr u32 kActionLookYMouseId = 2;
static constexpr u32 kActionLookXMouseId = 3;
static constexpr u32 kActionLookYPadId = 4;
static constexpr u32 kActionLookXPadId = 5;
static constexpr u32 kActionDoubleSpeedId = 6;

DebugCameraInputContext::DebugCameraInputContext(const f32 movement_speed)
    : MappedInputContext(input::kDebugInputPriorityMin),
      m_movement_speed(movement_speed),
      m_boost_factor(1.0f) {
    {
        auto r_action = m_input_mapper.action(kActionMovementYId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("MovementY");
        action->bind_positive_button(app::InputKey::KeyW);
        action->bind_negative_button(app::InputKey::KeyS);
        action->bind_axis(input::AxisBindingType::GameControllerLeftY);
        action->bind_alt_positive_button(app::GameControllerButton::DPadUp);
        action->bind_alt_negative_button(app::GameControllerButton::DPadDown);
        action->bind_on_update(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_forward>(this));
        action->set_enabled(true);
    }
    {
        auto r_action = m_input_mapper.action(kActionMovementXId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("MovementX");
        action->bind_positive_button(app::InputKey::KeyD);
        action->bind_negative_button(app::InputKey::KeyA);
        action->bind_axis(input::AxisBindingType::GameControllerLeftX);
        action->bind_alt_positive_button(app::GameControllerButton::DPadRight);
        action->bind_alt_negative_button(app::GameControllerButton::DPadLeft);
        action->bind_on_update(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_lateral>(this));
        action->set_enabled(true);
    }
    {
        auto r_action = m_input_mapper.action(kActionDoubleSpeedId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("Boost");
        action->bind_positive_button(app::InputKey::KeyLeftShift);
        action->bind_alt_positive_button(app::GameControllerButton::LeftStick);
        action->bind_on_button_down(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_double_start>(this));
        action->bind_on_button_up(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_double_end>(this));
        action->set_enabled(true);
    }
    {
        auto r_action = m_input_mapper.action(kActionLookYMouseId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("MouseY");
        action->bind_axis(input::AxisBindingType::MouseY);
        action->bind_on_update(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_mouse_y>(this));
        action->set_enabled(true);
    }
    {
        auto r_action = m_input_mapper.action(kActionLookXMouseId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("MouseX");
        action->bind_axis(input::AxisBindingType::MouseX);
        action->bind_on_update(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_mouse_x>(this));
        action->set_enabled(true);
    }
    {
        auto r_action = m_input_mapper.action(kActionLookYPadId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("GamePadY");
        action->bind_axis(input::AxisBindingType::GameControllerRightY);
        action->bind_on_update(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_pad_y>(this));
        action->set_enabled(true);
    }
    {
        auto r_action = m_input_mapper.action(kActionLookXPadId);
        if (!r_action) {
            modus_panic("Failed to retrieve action");
        }
        auto& action = *r_action;
        action->set_name("GamePadX");
        action->bind_axis(input::AxisBindingType::GameControllerRightX);
        action->bind_on_update(
            input::AxisMap::Callback::build<DebugCameraInputContext,
                                            &DebugCameraInputContext::on_pad_x>(this));
        action->set_enabled(true);
    }
}

StringSlice DebugCameraInputContext::name() const {
    return "DebugCameraInputContext";
}

void DebugCameraInputContext::update_camera(Engine& engine, math::Transform& camera_transform) {
    m_input_mapper.update(engine);
    m_controller.to_camera_transform(camera_transform);
}

void DebugCameraInputContext::set_position(const glm::vec3& position) {
    m_controller.set_position(position);
}

void DebugCameraInputContext::on_forward(Engine& engine, const input::AxisMap& map) {
    const f32 movement_delta =
        engine.tick_sec().count() * m_movement_speed * m_boost_factor * map.value_normalized();
    m_controller.forward(movement_delta);
}

void DebugCameraInputContext::on_lateral(Engine& engine, const input::AxisMap& map) {
    const f32 movement_delta =
        engine.tick_sec().count() * m_movement_speed * m_boost_factor * map.value_normalized();
    m_controller.strafe_right(movement_delta);
}

void DebugCameraInputContext::on_double_start(Engine&, const input::AxisMap&) {
    m_boost_factor = 3.0f;
}
void DebugCameraInputContext::on_double_end(Engine&, const input::AxisMap&) {
    m_boost_factor = 1.0f;
}
void DebugCameraInputContext::on_mouse_y(Engine&, const input::AxisMap& map) {
    m_controller.rotate_y_axis(f32(-map.value()));
}
void DebugCameraInputContext::on_mouse_x(Engine&, const input::AxisMap& map) {
    m_controller.rotate_x_axis(f32(map.value()));
}

void DebugCameraInputContext::on_pad_y(Engine& engine, const input::AxisMap& map) {
    MODUS_UNUSED(engine);
    const f32 movement_delta =
        engine.tick_sec().count() * m_movement_speed * map.value_normalized();
    m_controller.rotate_y_axis(movement_delta);
}
void DebugCameraInputContext::on_pad_x(Engine& engine, const input::AxisMap& map) {
    MODUS_UNUSED(engine);
    const f32 movement_delta =
        engine.tick_sec().count() * m_movement_speed * map.value_normalized();
    m_controller.rotate_x_axis(movement_delta);
}

}    // namespace modus::engine
