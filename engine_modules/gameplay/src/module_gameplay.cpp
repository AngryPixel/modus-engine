/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_input.hpp>

namespace modus::engine {

MODUS_ENGINE_IMPL_MODULE(ModuleGameplay, "5ed633e5-d44e-442f-aa2c-3ff4bc4cf24d");

ModuleGameplay::ModuleGameplay()
    : Module(module_guid<ModuleGameplay>(), "Gameplay", ModuleUpdateCategory::Gameplay),
      m_entity_template_registry(),
      m_entity_template_asset_factory(m_entity_template_registry),
      m_fixed_update_tick(chrono_cast<IMilisec>(FSeconds(1.0f / 60.0f))),
      m_accumulator(IMilisec(0)) {}

Result<> ModuleGameplay::initialize(Engine& engine) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("Gameplay Init");
    auto asset_module = engine.module<ModuleAssets>();
    if (!asset_module->loader().register_factory(&m_entity_template_asset_factory)) {
        MODUS_LOGE("Gameplay: Failed to register Entity Template Asset Factory");
        return Error<>();
    }

    if (m_world) {
        MODUS_PROFILE_GAMEPLAY_BLOCK("World enter");
        m_world->on_enter(engine);
    }
    return Ok<>();
}

Result<> ModuleGameplay::shutdown(Engine& engine) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("Gameplay Shutdown");
    if (m_world) {
        m_world->on_exit(engine);
    }

    auto asset_module = engine.module<ModuleAssets>();
    asset_module->loader().unregister_factory(engine, &m_entity_template_asset_factory);
    return Ok<>();
}

void ModuleGameplay::tick(Engine& engine) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("Gameplay");
    m_accumulator += engine.tick_ms();
    if (m_world) {
        constexpr const u32 kMaxFixedIterations = 4;
        u32 num_iterations = 0;
        while (m_accumulator >= m_fixed_update_tick) {
            MODUS_PROFILE_GAMEPLAY_BLOCK("Fixed Tick");
            m_world->tick_fixed(engine, m_fixed_update_tick);
            m_accumulator -= m_fixed_update_tick;
            ++num_iterations;
        }
        if (num_iterations == kMaxFixedIterations) {
            m_accumulator = IMilisec(0);
        }
        {
            MODUS_PROFILE_GAMEPLAY_BLOCK("Variable Tick");
            m_world->tick(engine, engine.tick_ms());
        }
    }
}

Slice<GID> ModuleGameplay::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleInput>(), module_guid<ModuleAssets>()};
    return Slice(deps);
}

bool ModuleGameplay::has_world() const {
    return m_world.is_valid();
}

void ModuleGameplay::set_world(Engine& engine, NotMyPtr<gameplay::GameWorld> world) {
    if (m_world) {
        m_world->on_exit(engine);
    }
    m_world = world;
    if (m_world) {
        m_world->on_enter(engine);
    }
}

}    // namespace modus::engine
