/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/gameplay/gameworld.hpp>

namespace modus::engine::gameplay {

namespace gameworld_detail {
static u32 kGameWorldStorageIndex = 0;
u32 next_gameworld_storage_index() {
    return kGameWorldStorageIndex++;
}
}    // namespace gameworld_detail

GameWorld::GameWorld() {}

Result<> GameWorld::initialize(Engine&) {
    return Ok<>();
}

Result<> GameWorld::shutdown(Engine&) {
    return Ok<>();
}

engine::gameplay::EntityId GameWorld::main_camera() const {
    return EntityId();
}

void GameWorld::on_enter(Engine&) {}

void GameWorld::on_exit(Engine&) {}

Result<> GameWorld::initialize_command_queue(const usize allocator_capacity) {
#if defined(MODUS_GAMEPLAY_GAMEWORLD_COMMAND_QUEUE_DEBUG)
    if (m_processing_queue) {
        modus_panic("Attempting to add a command to queue while the queue is being processed");
    }
#endif
    return m_command_queue.initialize(allocator_capacity);
}
void GameWorld::process_command_queue(Engine& engine) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("GameWorld::ProcessCommandQueue");
#if defined(MODUS_GAMEPLAY_GAMEWORLD_COMMAND_QUEUE_DEBUG)
    m_processing_queue = true;
#endif
    m_command_queue.execute(engine, *this);
#if defined(MODUS_GAMEPLAY_GAMEWORLD_COMMAND_QUEUE_DEBUG)
    m_processing_queue = false;
#endif
}

/*
void GameWorld::tick(Engine& engine) {
    MODUS_UNUSED(engine);

    // Do entity add/removals from last frame
    m_entity_manager.update();

    // Test transform update
    update_transform_components(m_entity_manager);

    // Update graphics components
    update_graphics_components(m_entity_manager);
}
*/
}    // namespace modus::engine::gameplay
