/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/gameplay/fsm/fsm.hpp>

namespace modus::engine::gameplay {

Result<> FSM::start(Engine& engine) {
    if (m_initial_state >= m_states.size() || !m_states[m_initial_state]) {
        return Error<>();
    }

    if (m_active_state != kMaxStateCount) {
        return Error<>();
    }

    m_active_state = m_initial_state;
    m_states[m_active_state]->on_enter(engine);
    return Ok<>();
}

void FSM::update(Engine& engine) {
    if (m_active_state != kMaxStateCount) {
        modus_assert(m_active_state < m_states.size() && m_states[m_active_state]);
        NotMyPtr<FSMState> active_state = m_states[m_active_state];
        auto transition_opt = active_state->update(engine);
        if (transition_opt.has_value()) {
            active_state->on_exit(engine);
            m_active_state = *transition_opt;
            modus_assert(m_active_state < m_states.size());
            if (m_active_state < m_states.size() && m_states[m_active_state]) {
                m_states[m_active_state]->on_enter(engine);
            }
        }
    }
}

void FSM::reset(Engine& engine) {
    if (m_active_state != kMaxStateCount) {
        m_states[m_active_state]->on_exit(engine);
    }
    m_active_state = kMaxStateCount;
}

bool FSM::did_finish() const {
    return !m_active_state;
}

void FSM::set_initial_state(const u32 index) {
    modus_assert(index < m_states.size());
    m_initial_state = index;
}

void FSM::set_state(const u32 index, NotMyPtr<FSMState> state) {
    modus_assert(index < m_states.size());
    m_states[index] = state;
}

}    // namespace modus::engine::gameplay
