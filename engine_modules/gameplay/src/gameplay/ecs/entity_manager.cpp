/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/gameplay/ecs/entity_manager.hpp>

namespace modus::engine::gameplay {

static constexpr u32 kEntityIndexesPerBlock = 512;

EntityManager::EntityManager()
    : m_entity_handles(kEntityIndexesPerBlock), m_entities(), m_active(0), m_size() {}

Result<> EntityManager::initialize(Engine&, const usize entity_count) {
    if (!grow_or_expand(entity_count)) {
        return Error<>();
    }
    return Ok<>();
}

Result<> EntityManager::shutdown(Engine& engine) {
    clear(engine);
    return Ok<>();
}

void EntityManager::clear(Engine& engine) {
    for (auto& entity : m_entities) {
        release_components(engine, entity);
        entity.mask = MaskType();
        entity.alive = false;
#if defined(MODUS_DEBUG)
        entity.name.clear();
#endif
    }
}

void EntityManager::add_entity_destroyed_callback(EntityDestroyedDelegate callback) {
    modus_assert(callback);
    modus_assert(std::find(m_entity_destroyed_callbacks.begin(), m_entity_destroyed_callbacks.end(),
                           callback) == m_entity_destroyed_callbacks.end());
    m_entity_destroyed_callbacks.push_back(callback);
}

void EntityManager::remove_entity_destroyed_callback(EntityDestroyedDelegate callback) {
    auto it = std::find(m_entity_destroyed_callbacks.begin(), m_entity_destroyed_callbacks.end(),
                        callback);
    if (it != m_entity_destroyed_callbacks.end()) {
        m_entity_destroyed_callbacks.erase(it);
    }
}

Result<EntityId, void> EntityManager::create(const StringSlice& name, const u32 tag) {
    if (!grow_or_expand(m_active + 1)) {
        return Error<>();
    }

    const usize entity_index = allocate_entity();
    auto add_result = m_entity_handles.create(entity_index);
    modus_assert(add_result);
    modus_assert(entity_index < m_entities.size());
    Entity& entity = m_entities[entity_index];
    MODUS_UNUSED(name);
#if defined(MODUS_DEBUG)
    if (name.size() != 0) {
        entity.name = name.to_str();
    }
#endif
    entity.tag = tag;
    modus_assert(entity.index.is_valid());
    modus_assert(entity.index.value() < m_entities.size());
    entity.alive = true;
    entity.handle = add_result.value().first;
    entity.mask.reset();

    return Ok(EntityId(entity.handle.value()));
}

Result<> EntityManager::update_entity_tag(const EntityId id, const u32 tag) {
    auto lookup_result = m_entity_handles.get(id);
    if (lookup_result) {
        const u32 index = *lookup_result.value();
        modus_assert(index < m_active);
        modus_assert(m_entities[index].index.value() < m_entities.size());
        m_entities[index].tag = tag;
        return Ok<>();
    }
    return Error<>();
}

void EntityManager::destroy(const EntityId id) {
    auto lookup_result = m_entity_handles.get(id);
    if (lookup_result) {
        const u32 index = *lookup_result.value();
        modus_assert(index < m_active);
        modus_assert(m_entities[index].index.value() < m_entities.size());
        m_entities[index].alive = false;
    }
}

Result<> EntityManager::swap_update_order(const EntityId id1, const EntityId id2) {
    if (id1 == id2) {
        return Error<>();
    }
    auto r_entity1 = m_entity_handles.get(id1);
    auto r_entity2 = m_entity_handles.get(id2);
    if (!r_entity1 || !r_entity2) {
        return Error<>();
    }

    modus_assert(**r_entity1 < m_entities.size());
    modus_assert(**r_entity2 < m_entities.size());

    Entity& entity1 = m_entities[**r_entity1];
    Entity& entity2 = m_entities[**r_entity2];

    swap_update_order_internal(entity1, entity2);
    // Swap entity lookup table
    std::swap(**r_entity1, **r_entity2);

    return Ok<>();
}

Result<EntityIndex, void> EntityManager::entity_index(const EntityId id) const {
    auto r_entity = m_entity_handles.get(id);
    if (!r_entity) {
        return Error<>();
    }
    modus_assert(**r_entity < m_entities.size());
    const Entity& entity = m_entities[**r_entity];
    return Ok(entity.index);
}

Result<u32, void> EntityManager::entity_tag(const EntityId id) const {
    auto r_entity = m_entity_handles.get(id);
    if (!r_entity) {
        return Error<>();
    }
    modus_assert(**r_entity < m_entities.size());
    const Entity& entity = m_entities[**r_entity];
    return Ok(entity.tag);
}

bool EntityManager::is_alive(const EntityId id) const {
    auto r_entity = m_entity_handles.get(id);
    if (!r_entity) {
        return false;
    }
    modus_assert(**r_entity < m_entities.size());
    const Entity& entity = m_entities[**r_entity];
    return entity.alive;
}

void EntityManager::update(Engine& engine) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("Update Entity Manager");
    // No entities could be created
    // during update
    modus_assert(m_entity_handles.count() == m_active);
    if (m_active == 0) {
        return;
    }
    m_size = m_active = update_internal(engine);
    // No entities entities can be created
    // during update
    modus_assert(m_entity_handles.count() == m_size);
#if defined(MODUS_ENGINE_ENTITY_MANAGER_DEBUG)
    for (u32 i = m_size; i < m_entities.size(); ++i) {
        modus_assert(m_entities[i].alive == false);
    }
#endif
}

bool EntityManager::is_valid_entity(const EntityId id) const {
    return m_entity_handles.get(id).is_value();
}

void EntityManager::on_destroy_entity(Engine& engine, const EntityId id) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("EntityManager::on_destroy_entity");
    for (auto& delegate : m_entity_destroyed_callbacks) {
        delegate(engine, id, *this);
    }
}

Result<NotMyPtr<ComponentStorageV2>, void> EntityManager::component_storage_for_type(
    const u32 index) const {
    if (index >= m_components.size() || !m_components[index].m_pool) {
        return Error<>();
    }
    return Ok<NotMyPtr<ComponentStorageV2>>(m_components[index].m_pool.get());
}

void EntityManager::release_components(Engine& engine, Entity& entity) {
    for (auto& component_storage : m_components) {
        if (entity.mask.test(component_storage.m_component_index) && component_storage.m_pool) {
            if (component_storage.m_destroy_callback) {
                auto ptr = component_storage.m_pool->get(entity.index);
                component_storage.m_destroy_callback(engine, *ptr);
            }
            component_storage.m_pool->release_component(entity.index);
        }
    }
    entity.mask.reset();
}

usize EntityManager::update_internal(Engine& engine) {
    // Rearrange entities so that all the dead ones are at the end and
    // all the alive ones are at the beginning
    usize index_dead = 0;
    usize index_alive = m_active - 1;

    while (true) {
        for (; true; ++index_dead) {
            if (index_dead > index_alive) {
                return index_dead;
            }
            if (!m_entities[index_dead].alive) {
                break;
            }
        }

        for (; true; --index_alive) {
            if (m_entities[index_alive].alive) {
                break;
            }

            // Erase handle for dead entities which don't need to be moved
            Entity& entity = m_entities[index_alive];
            on_destroy_entity(engine, entity.handle);
            release_components(engine, entity);
            auto entity_dead_erase_result = m_entity_handles.erase(entity.handle);
            MODUS_UNUSED(entity_dead_erase_result);
            modus_assert(entity_dead_erase_result);

            if (index_alive <= index_dead) {
                return index_dead;
            }
        }

        // Update handle data
        {
            Entity& entity_dead = m_entities[index_dead];
            Entity& entity_alive = m_entities[index_alive];
            modus_assert(entity_alive.alive);
            modus_assert(!entity_dead.alive);
            // Erase entity handle for dead entity
            auto entity_alive_lr = m_entity_handles.get(entity_alive.handle);
            modus_assert(entity_alive_lr);
            on_destroy_entity(engine, entity_dead.handle);
            release_components(engine, entity_dead);
            auto entity_dead_erase_result = m_entity_handles.erase(entity_dead.handle);
            MODUS_UNUSED(entity_dead_erase_result);
            modus_assert(entity_dead_erase_result);
            // update active entity result and swap components for better
            // cache locallity
            NotMyPtr<u32> ptr = entity_alive_lr.release_value();
            *ptr = index_dead;
        }

        // Swap entities
        swap_update_order_internal(m_entities[index_dead], m_entities[index_alive]);

        ++index_dead;
        --index_alive;
    }
    return index_dead;
}

Result<> EntityManager::grow_or_expand(const usize expected) {
    // Early exit
    if (expected < m_entities.size()) {
        return Ok<>();
    }

    const usize new_size = usize(f64(expected) * 1.5);
    const usize current_size = m_entities.size();
    m_entities.resize(new_size);
    for (auto& component_storage : m_components) {
        if (component_storage.m_pool) {
            if (!component_storage.m_pool->grow_to(new_size)) {
                return Error<>();
            }
        }
    }

    for (u32 index = current_size; index < new_size; ++index) {
        modus_assert(index < m_entities.size());
        m_entities[index].index = EntityIndex(index);
        m_entities[index].alive = false;
    }

    return Ok<>();
}

void EntityManager::swap_update_order_internal(Entity& entity1, Entity& entity2) {
    // Swap components
    for (auto& component_storage : m_components) {
        if (component_storage.m_pool) {
            component_storage.m_pool->swap_components(entity1.index, entity2.index);
        }
    }
    // Swap components indices
    std::swap(entity1.index, entity2.index);
    // Swap entity location in vector
    std::swap(entity1, entity2);
}
}    // namespace modus::engine::gameplay
