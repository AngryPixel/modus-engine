/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format on
#include <pch.h>
// clang-format off
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <core/topo_sort.hpp>
#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::gameplay {

using TopoSorter = TopologicalSorter<System, GID>;

Result<> DefaultSystemRunner::register_systems(SliceMut<Registration> systems) {

    TopoSorter sorter;
    for (auto& sys : systems) {
        if (sorter.add(sys.system, sys.system->guid(), sys.dependencies) != TopologicalSortError::None) {
            MODUS_LOGE("Failed to register system: {} :: {}", sys.system->guid(), sys.system->name());
            return Error<>();
        }
#if defined(MODUS_DEFAULT_SYSTEM_RUNNER_DEBUG)
        MODUS_LOGD("Registering System: {} :: {}",sys.system->guid(), sys.system->name());
        for (auto& dep : sys.dependencies) {
            MODUS_LOGD("    > {}", dep);
        }
#endif
    }

    Vector<NotMyPtr<System>> final_list;
    const TopologicalSortError error = sorter.evaluate(final_list);
    if (error != TopologicalSortError::None) {
        if (error == TopologicalSortError::DependencyNotFound) {
            MODUS_LOGE("A System dependency has not been found");
        } else if (error == TopologicalSortError::DependencyCycle) {
            MODUS_LOGE("There's a cyclic dependency in the current system list.");
            MODUS_LOGE("Detected while processing {} with dependency {}.",
                       sorter.node_being_processed_when_cycle_was_detected().value_or(GID()),
                       sorter.node_where_cycle_was_detected().value_or(GID()));
        } else {
            MODUS_LOGE("Uknown error during dependency order evaluation");
        }
        return Error<>();
    }


    if (final_list.size() >= m_systems.capacity()) {
        MODUS_LOGE("{} Systems detected, but we can only track {} at the moment. Update the capacity",
                   final_list.size(), m_systems.capacity());
        return Error<>();
    }

    MODUS_LOGD("Gameplay Systems Update Order:");
    u32 idx = 0;
    for (auto& ptr : final_list) {
        MODUS_LOGD("    [{:02}] {} :: {}", idx, ptr->guid(), ptr->name());
        m_systems.push_back(ptr).expect("Failed to instert system");
        ++idx;
    }
    return Ok<>();
}

Result<> DefaultSystemRunner::initialize(Engine&engine, GameWorld& game_world, EntityManager&entity_manager) {

    for (auto& system : m_systems) {
        if (!system->initialize(engine, game_world, entity_manager)) {
            MODUS_LOGE("Failed to initialize system: {} :: {}", system->guid(), system->name());
            (void)system->shutdown(engine, game_world, entity_manager);
            return Error<>();
        }
        m_init_count++;
    }

    return Ok<>();
}

void DefaultSystemRunner::shutdown(Engine& engine, GameWorld& game_world, EntityManager& entity_manager) {
    for (u32 i = 0; i < m_init_count && m_systems.size(); i++) {
        const usize index = m_init_count - 1 - i;
        if (m_systems[index] != nullptr) {
            m_systems[index]->shutdown(engine, game_world, entity_manager);
        }
    }
}

void DefaultSystemRunner::update(Engine& engine, const IMilisec tick, GameWorld& game_world, EntityManager& entity_manager) {
    for (auto& system : m_systems) {
        system->pre_update(engine, game_world, entity_manager);
    }
    for (auto& system : m_systems) {
        system->update(engine, tick, game_world, entity_manager);
    }
    for (auto& system : m_systems) {
        system->post_update(engine, game_world, entity_manager);
    }
}

void InlineSystemRunner::update(Engine&engine, const IMilisec tick, GameWorld& game_world, EntityManager& entity_manager, SliceMut<NotMyPtr<System>> systems){
    for (auto& system : systems) {
        system->pre_update(engine, game_world, entity_manager);
    }
    for (auto& system : systems) {
        system->update(engine, tick, game_world, entity_manager);
    }
    for (auto& system : systems) {
        system->post_update(engine, game_world, entity_manager);
    }
}

}
