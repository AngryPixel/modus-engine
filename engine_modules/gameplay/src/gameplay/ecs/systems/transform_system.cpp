/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>

#include <engine/gameplay/ecs/components/transform_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(TransformComponent)

namespace modus::engine::gameplay {

Result<math::Transform> ComponentLoaderTraits<TransformComponent>::load(Engine&,
                                                                        const StorageType& fbs) {
    math::Transform out = math::Transform::kIdentity;
    const auto* translation = fbs.translation();
    if (translation != nullptr) {
        out.set_translation(translation->x(), translation->y(), translation->z());
    }
    const auto* rotation = fbs.rotation();
    if (rotation != nullptr) {
        glm::quat rot(rotation->w(), rotation->x(), rotation->y(), rotation->z());
        out.set_rotation(rot);
    }
    out.set_scale(fbs.scale());
    return Ok(out);
}

TransformSystem::TransformSystem() : System("Transform+Attachments") {}

AttachResult TransformComponent::attach(EntityManager& entity_manager, const EntityId dest) {
    // Already attached
    if (m_parent_id.is_valid()) {
        return Error(AttachError::AlreadyAttached);
    }

    // Detect attach to self
    if (dest == m_this_id) {
        return Error(AttachError::AttachingToSelf);
    }

    // Check for transform component at destination
    EntityId cur_entity_id = dest;
    auto r_parent_transform = entity_manager.component<TransformComponent>(cur_entity_id);
    if (!r_parent_transform) {
        return Error(AttachError::NoTransformComponent);
    }

    // Try and detect cycle
    cur_entity_id = r_parent_transform.value()->m_parent_id;
    while (cur_entity_id.is_valid()) {
        auto r_next_parent = entity_manager.component<TransformComponent>(cur_entity_id);
        if (!r_next_parent) {
            break;
        }

        TransformComponent& next_parent_transform = *(r_next_parent.value());
        if (next_parent_transform.m_this_id == m_this_id) {
            // Attachment cycle;
            return Error(AttachError::CyclicAttachment);
        }
        cur_entity_id = next_parent_transform.m_parent_id;
    }
    m_parent_id = dest;
    m_world_dirty = true;
    m_attachment_dirty = true;
    return Ok<>();
}

void TransformComponent::deattach(EntityManager&) {
    m_parent_id = EntityId();
    m_local_dirty = true;
}

void TransformSystem::update(Engine&, const IMilisec, GameWorld&, EntityManager& entity_manager) {
    {
        MODUS_PROFILE_GAMEPLAY_BLOCK("Attachment Reordering");
        EntityManagerViewMut<TransformComponent> view(entity_manager);
        view.for_each([&entity_manager](const EntityId, TransformComponent& component) {
            if (!component.m_attachment_dirty) {
                return;
            }
            NotMyPtr<TransformComponent> cur_component = &component;
            while (cur_component->m_parent_id.is_valid()) {
                const EntityId parent_id = cur_component->m_parent_id;
                auto r_parent_index = entity_manager.entity_index(parent_id);
                if (!r_parent_index) {
                    MODUS_LOGE("Parent for entity {} no longer exists", cur_component->m_this_id);
                    break;
                }
                const EntityIndex parent_index = *r_parent_index;
                const EntityIndex entity_index =
                    entity_manager.entity_index(cur_component->m_this_id).value_or_panic();
                // Swap component locations if the parent is going to be
                // update later then the child
                if (parent_index.value() > entity_index.value()) {
                    auto r_swap =
                        entity_manager.swap_update_order(cur_component->m_this_id, parent_id);
                    modus_assert(r_swap);
                    MODUS_UNUSED(r_swap);
                } else {
                    break;
                }
                cur_component =
                    entity_manager.component<TransformComponent>(parent_id).value_or_panic(
                        "Failed to retrieve parent we know exists");
            }
        });
    }

    MODUS_PROFILE_GAMEPLAY_BLOCK("Update Transform Component");

    EntityManagerViewMut<TransformComponent> view(entity_manager);
    view.for_each([&entity_manager](const EntityId, TransformComponent& tr) {
        const bool local_is_dirty = tr.m_local_dirty;
        if (!tr.m_parent_id.is_valid()) {
            if (local_is_dirty) {
                tr.m_world.set_scale(tr.m_local.m_scale);
                tr.m_world.set_rotation(tr.m_local.m_rotation);
                tr.m_world.set_translation(tr.m_local.m_position);
                tr.m_world_dirty = true;
            }
        } else {
            auto r_parent = entity_manager.template component<TransformComponent>(tr.m_parent_id);
            if (r_parent) {
                const TransformComponent& parent_transform = **r_parent;
                if (parent_transform.m_local_dirty || local_is_dirty) {
                    tr.m_world.set_transform(parent_transform.m_world.transformed(tr.m_local));
                    tr.m_world_dirty = true;
                }
            } else {
                MODUS_LOGE("Parent for entity {} no longer exists", tr.m_this_id);
            }
        }
    });
}
void TransformSystem::update_threaded(ThreadSafeEngineAccessor&,
                                      const IMilisec,
                                      ThreadSafeGameWorldAccessor&,
                                      ThreadSafeEntityManagerAccessor&) {
    modus_panic("Transform System can't be run in parallel at the moment");
}
void TransformSystem::post_update(Engine&, GameWorld&, EntityManager& entity_manager) {
    EntityManagerViewMut<TransformComponent> view(entity_manager);
    view.for_each([](const EntityId, TransformComponent& tr) { tr.clear_dirty(); });
}
}    // namespace modus::engine::gameplay
