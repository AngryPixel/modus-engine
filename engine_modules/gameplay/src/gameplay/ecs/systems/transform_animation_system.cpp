/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/gameplay/ecs/components/transform_animation_component.hpp>
// clang-format on
#include <engine/gameplay/ecs/systems/transform_animation_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/engine.hpp>

#include <engine/gameplay/ecs/components/transform_animation_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(TransformAnimationComponent)

namespace modus::engine::gameplay {

Result<TransformAnimationComponent> ComponentLoaderTraits<TransformAnimationComponent>::load(
    Engine&,
    const StorageType& fbs) {
    TransformAnimationComponent out;
    out.m_duration_sec = FSeconds(fbs.duration_sec());
    out.m_elapsed_sec = FSeconds(fbs.elapsed_sec());
    out.m_loop = fbs.loop();
    out.m_paused = fbs.paused();
    out.m_animation = nullptr;
    return Ok(out);
}

TransformAnimationSystem::TransformAnimationSystem() : System("TransformAnimation") {}

MODUS_FORCE_INLINE static void do_component_update(const FSeconds tick_sec,
                                                   TransformAnimationComponent& ta,
                                                   TransformComponent& tr) {
    if (ta.m_paused) {
        return;
    }
    ta.m_elapsed_sec += tick_sec;
    if (ta.m_elapsed_sec >= ta.m_duration_sec) {
        if (!ta.m_loop) {
            ta.m_paused = true;
            ta.m_elapsed_sec = FSeconds(0.0f);
            return;
        }

        ta.m_elapsed_sec -= ta.m_duration_sec;
    }
    ta.m_animation(tr, ta.m_elapsed_sec, ta.m_duration_sec);
}

void TransformAnimationSystem::update(Engine&,
                                      const IMilisec tick,
                                      GameWorld&,
                                      EntityManager& entity_manager) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("TransformAnimation");

    EntityManagerViewMut<TransformAnimationComponent, TransformComponent> view(entity_manager);
    view.for_each([tick_sec = FSeconds(tick)](const EntityId, TransformAnimationComponent& ta,
                                              TransformComponent& tr) {
        do_component_update(tick_sec, ta, tr);
    });
}

void TransformAnimationSystem::update_threaded(ThreadSafeEngineAccessor&,
                                               const IMilisec tick,
                                               ThreadSafeGameWorldAccessor&,
                                               ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("TransformAnimation");
    EntityManagerViewMut<TransformAnimationComponent, TransformComponent> view(entity_manager);
    view.for_each([tick_sec = FSeconds(tick)](const EntityId, TransformAnimationComponent& ta,
                                              TransformComponent& tr) {
        do_component_update(tick_sec, ta, tr);
    });
}

}    // namespace modus::engine::gameplay
