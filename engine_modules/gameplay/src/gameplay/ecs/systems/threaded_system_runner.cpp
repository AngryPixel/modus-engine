/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/gameplay/ecs/systems/threaded_system_runner.hpp>
// clang-format on

#include <engine/gameplay/ecs/ecs_types.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/modules/module_parallel.hpp>
#include <engine/engine.hpp>

#include <os/thread.hpp>

namespace modus::engine::gameplay {

void ThreadedSystemRunner::set_job(ThreadedSystemRunnerJob&& job) {
    m_job = std::move(job);
}

Result<> ThreadedSystemRunner::initialize(Engine& engine,
                                          GameWorld& game_world,
                                          EntityManager& entity_manager) {
    m_job_stack.reserve(8);
    const bool result = for_each_job(
        [&engine, &game_world, &entity_manager](NotMyPtr<ThreadedSystemRunnerJob> job) -> bool {
            NotMyPtr<System> system = job->m_system;
            if (!system->initialize(engine, game_world, entity_manager)) {
                MODUS_LOGE("Failed to initialize system: {} :: {}", system->guid(), system->name());
                return false;
            }
            return true;
        });
    if (result) {
        return Ok<>();
    } else {
        return Error<>();
    }
}

void ThreadedSystemRunner::shutdown(Engine& engine,
                                    GameWorld& game_world,
                                    EntityManager& entity_manager) {
    for_each_job(
        [&engine, &game_world, &entity_manager](NotMyPtr<ThreadedSystemRunnerJob> job) -> bool {
            NotMyPtr<System> system = job->m_system;
            if (system) {
                system->shutdown(engine, game_world, entity_manager);
            }
            return true;
        });
}

struct ThreadedSystemJob {
    ThreadSafeEngineAccessor engine;
    NotMyPtr<System> system;
    ThreadSafeGameWorldAccessor game_world;
    ThreadSafeEntityManagerAccessor entity_manager;
    IMilisec tick;

    ThreadedSystemJob(Engine& e, const IMilisec t, GameWorld& world, EntityManager& em)
        : engine(e), game_world(world), entity_manager(em), tick(t) {}

    void operator()() {
        // MODUS_LOGD("Running system on thread {}: {}", os::Thread::current_thread_id(),
        // this->system->name());
        this->system->update_threaded(engine, tick, game_world, entity_manager);
    }
};

struct MainThreadJob {
    NotMyPtr<Engine> engine;
    NotMyPtr<GameWorld> game_world;
    NotMyPtr<EntityManager> entity_manager;
    NotMyPtr<System> system;
    NotMyPtr<std::atomic<u32>> counter;
    IMilisec tick;
    void operator()() {
        // MODUS_LOGD("Running system on thread {}: {} (MAIN)", os::Thread::current_thread_id(),
        // this->system->name());
        (*this->counter)++;
        if (this->counter->load(std::memory_order_acquire) != 1) {
            modus_panic(modus::format("More than one main thread job executing at the same time "
                                      "when running system:{} tid:{}",
                                      system->name(), os::Thread::current_thread_id())
                            .c_str());
        }
        system->update(*this->engine, this->tick, *this->game_world, *this->entity_manager);
        (*this->counter)--;
    }
};

void ThreadedSystemRunner::update(Engine& engine,
                                  const IMilisec tick,
                                  GameWorld& game_world,
                                  EntityManager& entity_manager) {
    {
        MODUS_PROFILE_GAMEPLAY_BLOCK("Threaded Runner Pre Update");
        for_each_job(
            [&engine, &game_world, &entity_manager](NotMyPtr<ThreadedSystemRunnerJob> job) -> bool {
                NotMyPtr<System> system = job->m_system;
                system->pre_update(engine, game_world, entity_manager);
                return true;
            });
    }

    {
        MODUS_PROFILE_GAMEPLAY_BLOCK("Threaded Runner Update");
        auto& job_system = engine.module<ModuleParallel>()->job_system();
        m_job_stack.clear();

        jobsys::JobHandle root_job = job_system.create([]() {});
        std::atomic<u32> concurrent_main_thread_counter = 0;
        m_job.m_parent_job_handle = root_job;
        m_job_stack.push_back(NotMyPtr<ThreadedSystemRunnerJob>(&m_job));
        {
            MODUS_PROFILE_GAMEPLAY_BLOCK("Job Creation");
            while (!m_job_stack.empty()) {
                NotMyPtr<ThreadedSystemRunnerJob> runner_job = m_job_stack.back();
                m_job_stack.erase(m_job_stack.begin() + m_job_stack.size() - 1);
                jobsys::JobHandle job;
                if (runner_job->m_run_on_main_thread) {
                    MainThreadJob main_job;
                    main_job.engine = &engine;
                    main_job.counter = &concurrent_main_thread_counter;
                    main_job.entity_manager = &entity_manager;
                    main_job.game_world = &game_world;
                    main_job.tick = tick;
                    main_job.system = runner_job->m_system;
                    job = job_system.create_as_chained_child(runner_job->m_parent_job_handle,
                                                             std::move(main_job));
                } else {
                    ThreadedSystemJob job_fn(engine, tick, game_world, entity_manager);
                    job_fn.system = runner_job->m_system;
                    // Create all the systems jobs as children of the root job so we can use the
                    // root job as a sync point later.
                    job = job_system.create_as_chained_child(runner_job->m_parent_job_handle,
                                                             std::move(job_fn));
                }
                for (auto& next : runner_job->m_next) {
                    next.m_parent_job_handle = job;
                    m_job_stack.emplace_back(&next);
                }
            }
        }

        job_system.run(root_job);
        {
            MODUS_PROFILE_GAMEPLAY_BLOCK("Waiting on root job");
            job_system.wait(root_job);
        }
    }
    {
        MODUS_PROFILE_GAMEPLAY_BLOCK("Threaded Runner Post Update");
        for_each_job(
            [&engine, &game_world, &entity_manager](NotMyPtr<ThreadedSystemRunnerJob> job) -> bool {
                NotMyPtr<System> system = job->m_system;
                system->post_update(engine, game_world, entity_manager);
                return true;
            });
    }
}

template <typename T>
bool ThreadedSystemRunner::for_each_job(T&& fn) {
    static_assert(std::is_invocable_r_v<bool, T, NotMyPtr<ThreadedSystemRunnerJob>>,
                  "Invalid function/callable signature");
    m_job_stack.clear();
    m_job_stack.push_back(NotMyPtr<ThreadedSystemRunnerJob>(&m_job));
    while (!m_job_stack.empty()) {
        NotMyPtr<ThreadedSystemRunnerJob> job = m_job_stack.back();
        m_job_stack.erase(m_job_stack.begin() + m_job_stack.size() - 1);
        if (!fn(job)) {
            return false;
        }
        for (auto& next : job->m_next) {
            m_job_stack.emplace_back(&next);
        }
    }
    return true;
}

}    // namespace modus::engine::gameplay