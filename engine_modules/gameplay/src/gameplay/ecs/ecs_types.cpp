/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/gameplay/ecs/ecs_types.hpp>
#include <os/guid_gen.hpp>
namespace modus::engine::gameplay::component_detail {
u32 generate_component_index() {
    static u32 kComponentCounter = 0;
    const u32 new_component_index = kComponentCounter++;
    modus_assert(new_component_index < kMaxExpectedComponentTypes);
    return new_component_index;
}
}    // namespace modus::engine::gameplay::component_detail

namespace modus::engine::gameplay {

System::System(StringSlice guid, StringSlice name)
    : m_guid(GID::from_string(guid).value_or_panic("Invalid System GUID")), m_name(name) {}
System::System(StringSlice name)
    : m_guid(os::guid::generate().value_or_panic("Failed to generate GUID")), m_name(name) {}
}    // namespace modus::engine::gameplay
