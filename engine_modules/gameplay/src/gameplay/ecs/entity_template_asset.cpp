/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clang-format off
#include <pch.h>
#include <engine/gameplay/ecs/entity_template_asset.hpp>
// clang-format on

#include <engine/assets/asset_waiter.hpp>
#include <engine/gameplay/ecs/entity_template.hpp>
#include <engine/gameplay/ecs/entity_template_registry.hpp>
#include <engine/engine.hpp>
#include <engine/modules/module_assets.hpp>

#include <engine/gameplay/ecs/entity_template_generated.h>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::gameplay::EntityTemplateAsset)

namespace modus::engine::gameplay {

EntityTemplateAsset::EntityTemplateAsset(NotMyPtr<const EntityTemplate> tmpl,
                                         NotMyPtr<EntityTemplateData> tmpl_data,
                                         Vector<assets::AssetHandleV2>&& assets)
    : assets::AssetBase(assets::AssetTraits<EntityTemplateAsset>::type_id()),
      m_template(tmpl),
      m_template_data(tmpl_data),
      m_dependencies(std::move(assets)) {}

Result<EntityId> EntityTemplateAsset::spawn(Engine& engine,
                                            EntityManager& entity_manager,
                                            const StringSlice name,
                                            const u32 tag) const {
    EntityTemplate::SpawnParams params{entity_manager, name, tag};
    return m_template->spawn(engine, params, m_template_data);
}

class EntityTemplateAssetData final : public assets::RamAssetData {
   public:
    RamStream<assets::AssetsAllocator> m_stream;
    NotMyPtr<const EntityTemplate> m_template;
    assets::AssetGroupWaiter m_asset_waiter;
    EntityTemplateRegistry& m_registry;

    EntityTemplateAssetData(EntityTemplateRegistry& registry) : m_registry(registry) {}

    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        MODUS_PROFILE_GAMEPLAY_BLOCK("EntityTemplateAssetData");
        if (!m_stream.populate_from_stream(reader)) {
            MODUS_LOGE("EntityTemplateAsset: Failed to load template into memory");
            return Error<>();
        }

        // Use reflection to extract the schema namespace
        const ByteSlice bytes = m_stream.as_bytes();
        flatbuffers::Verifier fbs_verifier(bytes.data(), bytes.size());
        if (!ecs::fbs::VerifyEntityTemplateBuffer(fbs_verifier)) {
            MODUS_LOGE("EntityTemplateAsset: Data is not a valid entity template");
            return Error<>();
        }

        const ecs::fbs::EntityTemplate* fbs_template = ecs::fbs::GetEntityTemplate(bytes.data());
        const auto fbs_namespace = fbs_template->namespace_();
        const auto fbs_template_data = fbs_template->template_();
        if (fbs_namespace == nullptr) {
            MODUS_LOGE("EntityTemplateAsset: Template does not have namespace field");
            return Error<>();
        }
        if (fbs_template_data == nullptr) {
            MODUS_LOGE("EntityTemplateAsset: Template does not have a data field");
            return Error<>();
        }

        const StringSlice table_name_slice(fbs_namespace->c_str());
        if (auto r = m_registry.resolve(table_name_slice); !r) {
            MODUS_LOGE("EntityTemplateAsset: No template registered for namespace '{}'",
                       table_name_slice);
            return Error<>();
        } else {
            m_template = *r;
        }
        const ByteSlice data_bytes =
            ByteSlice(fbs_template_data->data(), fbs_template_data->size());
        if (!m_template->verify_contents(data_bytes)) {
            MODUS_LOGE("EntityTemplateAsset: Contents are not valid for template '{}'",
                       table_name_slice);
            return Error<>();
        }

        Vector<String> assets;
        m_template->extract_asset_paths(assets, data_bytes);
        if (!assets.empty()) {
            for (auto& asset : assets) {
                m_asset_waiter.add(std::move(asset));
            }
            return Ok(assets::AssetLoadResult::HasDependencies);
        }
        return Ok(assets::AssetLoadResult::Done);
    }

    Result<bool> eval_dependencies(Engine& engine, assets::AssetLoader& loader) override {
        MODUS_PROFILE_GAMEPLAY_BLOCK("EntityTemplateAssetEvalDep");
        const assets::AssetGroupWaiterState state = m_asset_waiter.update(engine, loader);
        if (state.error_count != 0) {
            MODUS_LOGE("EntityTemplateAsset: One of the entity template assets failed to load");
            return Error<>();
        }
        return Ok<bool>(state.finished_count == m_asset_waiter.count());
    }
};

EntityTemplateAssetFactory::EntityTemplateAssetFactory(EntityTemplateRegistry& registry)
    : factory_type(64), m_registry(registry) {}
StringSlice EntityTemplateAssetFactory::file_extension() const {
    return "etpl";
}
std::unique_ptr<assets::AssetData> EntityTemplateAssetFactory::create_asset_data() {
    return modus::make_unique<EntityTemplateAssetData>(m_registry);
}
Result<NotMyPtr<assets::AssetBase>> EntityTemplateAssetFactory::create(Engine& engine,
                                                                       assets::AssetData& data) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("EntityTemplateAssetCreate");
    EntityTemplateAssetData& etmpl_data = static_cast<EntityTemplateAssetData&>(data);

    const ByteSlice bytes = etmpl_data.m_stream.as_bytes();
    const ecs::fbs::EntityTemplate* fbs_template = ecs::fbs::GetEntityTemplate(bytes.data());
    const auto fbs_template_data = fbs_template->template_();
    const ByteSlice data_bytes = ByteSlice(fbs_template_data->data(), fbs_template_data->size());
    auto r_create = etmpl_data.m_template->load(engine, data_bytes);
    if (!r_create) {
        MODUS_LOGE("EntityTemplateAsset: Failed to load entity template {} from data",
                   etmpl_data.m_template->template_namespace());
        return Error<>();
    }

    const auto waiters = etmpl_data.m_asset_waiter.waiters();
    Vector<assets::AssetHandleV2> assets;
    assets.reserve(waiters.size());
    for (const auto& waiter : waiters) {
        assets.push_back(waiter.handle());
    }

    return Ok(NotMyPtr<assets::AssetBase>(
        m_asset_pool.construct(etmpl_data.m_template, *r_create, std::move(assets))));
}
Result<> EntityTemplateAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GAMEPLAY_BLOCK("EntityTemplateAssetDestroy");
    auto r_asset = assets::assetv2_cast<EntityTemplateAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    auto& asset_loader = engine.module<ModuleAssets>()->loader();
    for (const auto& handle : (*r_asset)->m_dependencies) {
        asset_loader.destroy_async(handle);
    }
    (*r_asset)->m_template->unload(engine, (*r_asset)->m_template_data);
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}
}    // namespace modus::engine::gameplay
