/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/gameplay/ecs/entity_template_registry.hpp>
// clang-format on

#include <engine/gameplay/ecs/entity_template.hpp>

namespace modus::engine::gameplay {

EntityTemplateRegistry::EntityTemplateRegistry() {
    constexpr size_t kInitialCapacity = 32;
    m_templates.reserve(kInitialCapacity);
}

EntityTemplateRegistry::~EntityTemplateRegistry() {
    modus_assert_message(m_templates.empty(), "Not all templates have been unregistered");
}

Result<> EntityTemplateRegistry::register_template(NotMyPtr<EntityTemplate> entity_template) {
    modus_assert(entity_template);
    const String& namespace_str = entity_template->template_namespace();
    auto it = m_templates.find(namespace_str);
    if (it != m_templates.end()) {
        return Error<>();
    }
    m_templates[namespace_str] = entity_template;
    return Ok<>();
}
void EntityTemplateRegistry::unregister_template(NotMyPtr<EntityTemplate> entity_template) {
    modus_assert(entity_template);
    m_templates.erase(entity_template->template_namespace());
}
Result<NotMyPtr<const EntityTemplate>> EntityTemplateRegistry::resolve(
    const StringSlice template_namespace) const {
    const String namespace_str = template_namespace.to_str();
    auto it = m_templates.find(namespace_str);
    if (it == m_templates.end()) {
        return Error<>();
    }
    return Ok(NotMyPtr<const EntityTemplate>(it->second));
}
}    // namespace modus::engine::gameplay
