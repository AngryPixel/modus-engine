/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/gameplay/ecs/components/velocity_component.hpp>
// clang-format on

#include <engine/gameplay/ecs/components/velocity_component_generated.h>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(VelocityComponent)

namespace modus::engine::gameplay {

Result<glm::vec3> ComponentLoaderTraits<VelocityComponent>::load(Engine&, const StorageType& fbs) {
    glm::vec3 out = glm::vec3();
    const auto* velocity = fbs.velocity();
    if (velocity != nullptr) {
        out = glm::vec3(velocity->x(), velocity->y(), velocity->z());
    } else {
        out = glm::vec3(0.0f);
    }
    return Ok(out);
}
}    // namespace modus::engine::gameplay
