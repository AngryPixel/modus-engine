/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/gameplay/camera_controllers.hpp>
#include <math/transform.hpp>

namespace modus::engine::gameplay {

void FreeRoamCameraController::reset(const glm::vec3& position) {
    m_position = position;
    m_forward = glm::vec3(0.f, 0.f, -1.f);
    m_up = glm::vec3(0.0f, 1.f, 0.f);
    m_right = glm::vec3(1.f, 0.f, 0.f);
    m_pitch = 0.0f;
    m_yaw = -90.f;
}

void FreeRoamCameraController::set_position(const glm::vec3& position) {
    m_position = position;
}

void FreeRoamCameraController::forward(const f32 amount) {
    m_position += m_forward * amount;
}

void FreeRoamCameraController::backward(const f32 amount) {
    m_position -= m_forward * amount;
}

void FreeRoamCameraController::strafe_left(const f32 amount) {
    m_position -= m_right * amount;
}

void FreeRoamCameraController::strafe_right(const f32 amount) {
    m_position += m_right * amount;
}

void FreeRoamCameraController::rotate(const f32 angle_x_axis_degrees,
                                      const f32 angle_y_axis_degrees) {
    m_yaw += angle_x_axis_degrees;
    m_pitch += angle_y_axis_degrees;

    // Make sure the camera doesn't flip on the x axis
    static const f32 kMaxPitchAngle = 89.0f;
    if (m_pitch > kMaxPitchAngle) {
        m_pitch = kMaxPitchAngle;
    }

    if (m_pitch < -kMaxPitchAngle) {
        m_pitch = -kMaxPitchAngle;
    }

    // Calculate new front direction
    glm::vec3 front;
    const f32 pitch_radians = glm::radians(m_pitch);
    const f32 yaw_radians = glm::radians(m_yaw);
    const f32 cos_pitch = std::cos(pitch_radians);
    const f32 sin_pitch = std::sin(pitch_radians);
    const f32 sin_yaw = std::sin(yaw_radians);
    const f32 cos_yaw = std::cos(yaw_radians);
    front.x = cos_yaw * cos_pitch;
    front.y = sin_pitch;
    front.z = sin_yaw * cos_pitch;
    m_forward = glm::normalize(front);

    // Calculate new right direction
    m_right = glm::normalize(glm::cross(m_forward, m_up));
}

void FreeRoamCameraController::to_camera_transform(math::Transform& t) {
    t.set_look_at(m_position, m_position + m_forward, m_up);
}

}    // namespace modus::engine::gameplay
