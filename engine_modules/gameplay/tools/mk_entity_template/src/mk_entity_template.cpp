/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
// clang-format on
#include <flatbuffers/idl.h>

#include <core/getopt.hpp>
#include <core/io/byte_stream.hpp>
#include <core/io/memory_stream.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/guid_gen.hpp>
#include <engine/gameplay/ecs/entity_template_generated.h>

using namespace modus;

static Result<String> load_input_file(const StringSlice path) {
    auto r_file = os::FileBuilder().read().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open input file: {}\n", path);
        return Error<>();
    }

    constexpr usize kBufferSize = 1024;
    u8 buffer[kBufferSize];
    ByteSliceMut buffer_slice(buffer);
    String result;
    while (true) {
        auto r_read = r_file->read(buffer_slice);
        if (!r_read) {
            std::cerr << fmt::format("Failed to read from input file: {}\n", path);
            return Error<>();
        }
        if (*r_read == 0) {
            break;
        }
        StringSlice str_slice(buffer_slice);
        result.append(str_slice.data(), *r_read);
    }
    return Ok(std::move(result));
}

static const StringSlice kIntro =
    "Convert a json description for an entity template to it's binary representation."
    "modus_mk_entity_template [options] -o <output> -s <path_to_schema> <path_to_json>";

int main(const int argc, const char** argv) {
    CmdOptionString opt_output("-o", "--output", "Path for the generated output", "");
    CmdOptionString opt_schema_path("-s", "--schema",
                                    "Path to the flatbuffer schema for this template.", "");
    CmdOptionString opt_include_path(
        "", "--include-path", "Overwrite default include path for the flatbuffer schemas", "");
    CmdOptionParser opt_parser;
    opt_parser.add(opt_output).expect("Failed to add cmd parser option");
    opt_parser.add(opt_schema_path).expect("Failed to add cmd parser option");
    opt_parser.add(opt_include_path).expect("Failed to add cmd parser option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (!opt_output.parsed()) {
        std::cerr << fmt::format("No output file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (!opt_schema_path.parsed()) {
        std::cerr << "At schema is required for this process.\n";
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_result.value().size() == 0) {
        std::cerr << "No input file has been specified.\n";
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    auto r_exec_dir = os::Path::executable_directory();
    if (!r_exec_dir) {
        std::cerr << "Could not retrieve executable directory\n";
        return EXIT_FAILURE;
    }

    String include_dir;
    if (!opt_include_path.parsed()) {
        include_dir = os::Path::join(*r_exec_dir, "fbs");
    } else {
        include_dir = opt_include_path.value_str();
    }

    auto r_schema_contents = load_input_file(opt_schema_path.value());
    if (!r_schema_contents) {
        // try to load from the template directory if the above failed
        std::cout << fmt::format("Could not locate schema at: {}\n.", opt_schema_path.value());
        const String alternative_path = os::Path::join(include_dir, opt_schema_path.value());
        std::cout << fmt::format("Attempting to load schema from the include directory: {}\n",
                                 alternative_path);
        r_schema_contents = load_input_file(alternative_path);
        if (!r_schema_contents) {
            return EXIT_FAILURE;
        }
    }

    auto r_file_contents = load_input_file(opt_result.value()[0]);
    if (!r_file_contents) {
        return EXIT_FAILURE;
    }

    const char* include_paths[] = {include_dir.c_str(), nullptr};

    flatbuffers::Parser parser;
    // Load schema
    if (!parser.Parse(r_schema_contents->c_str(), include_paths)) {
        std::cerr << fmt::format("Failed to parse flatbuffer schema input:\n{}\n", parser.error_);
        return EXIT_FAILURE;
    }
    // Load json
    if (!parser.Parse(r_file_contents->c_str(), include_paths)) {
        std::cerr << fmt::format("Failed to convert json input:\n{}\n", parser.error_);
        return EXIT_FAILURE;
    }
    const std::string namespace_name =
        parser.root_struct_def_->defined_namespace->GetFullyQualifiedName(
            parser.root_struct_def_->name);

    // write to file
    flatbuffers::FlatBufferBuilder fbs_builder;

    auto fbs_vec =
        fbs_builder.CreateVector(parser.builder_.GetBufferPointer(), parser.builder_.GetSize());
    auto fbs_name = fbs_builder.CreateString(namespace_name);
    engine::gameplay::ecs::fbs::EntityTemplateBuilder builder(fbs_builder);
    builder.add_namespace_(fbs_name);
    builder.add_template_(fbs_vec);
    auto fbs_entity_template = builder.Finish();
    fbs_builder.Finish(fbs_entity_template, "ETPL");

    auto r_file = os::FileBuilder().write().create().open(opt_output.value());
    if (!r_file) {
        std::cerr << fmt::format("Failed to open output file:{}\n", opt_output.value());
        return EXIT_FAILURE;
    }

    // Write data to file
    const ByteSlice builder_slice =
        ByteSlice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = r_file->write_exactly(builder_slice);
    if (!r_write) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", opt_output.value());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
