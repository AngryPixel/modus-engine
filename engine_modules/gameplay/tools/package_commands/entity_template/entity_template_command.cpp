/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <os/os_pch.h>
#include <package/command.hpp>
#include <package/status_reporter.hpp>
// clang-format on
#include <os/path.hpp>

namespace modus::graphics {

static constexpr const char* kType = "entity_template";

class EntityTemplateCommandCreator;

class EntityTemplateCommand final : public package::ICommand {
   public:
    std::vector<std::string> m_args;
    size_t m_schema_index;

   public:
    const char* type() const override { return kType; }

    Result<> execute(package::StatusReporter& reporter,
                     const package::CommandContext&) const override {
        return package::detail::execute_process(m_args, "", reporter);
    }

    u64 command_hash(const package::CommandContext&) const override {
        Hasher64 hasher;
        for (const auto& arg : m_args) {
            hasher.update(arg.data(), arg.size());
        }
        return hasher.digest();
    }

    Vector<String> command_dependencies() const override {
        return {String(m_args[m_schema_index]), String(m_args.back())};
    }
};

class EntityTemplateCommandCreator final : public package::ICommandCreator {
   public:
    package::CommandPool<EntityTemplateCommand, DefaultAllocator> m_commands;
    String m_mk_template_bin;
    String m_fbs_dir;

    EntityTemplateCommandCreator() : m_commands(16) {}

    const char* type() const override { return kType; }

    Result<> initialize(package::StatusReporter& reporter) override {
        auto r_exec_dir = os::Path::executable_directory();
        if (!r_exec_dir) {
            package::ReportError(reporter, "Failed to get executable directory");
            return Error<>();
        }

#if defined(MODUS_OS_WIN32)
        constexpr const char* kExecName = "modus_mk_entity_template.exe";
#else
        constexpr const char* kExecName = "modus_mk_entity_template";
#endif

        String bin_file = os::Path::join(*r_exec_dir, kExecName);
        if (!os::Path::is_file(bin_file)) {
            package::ReportError(reporter, "Could not find '{}'", bin_file);
            return Error<>();
        }
        m_mk_template_bin = std::move(bin_file);
        m_fbs_dir = os::Path::join(os::Path::get_path(m_mk_template_bin), "fbs");
        if (!os::Path::is_directory(m_fbs_dir)) {
            package::ReportError(reporter, "Could not locate flatbuffer schema directory '{}'",
                                 m_fbs_dir);
            return Error<>();
        }
        return Ok<>();
    }

    Result<package::ICommand*> create(package::StatusReporter& reporter,
                                      const package::CommandCreateParams& params) override {
        static constexpr const char* kInputElem = "input";
        static constexpr const char* kSchemaElem = "schema";

        ReportDebug(reporter, "Creating EntityTemplate Command instance");
        std::vector<std::string> args;
        args.reserve(8);
        args.push_back(m_mk_template_bin.c_str());
        size_t schema_index = 0;

        if (auto r = params.parser.read_string(kSchemaElem); !r) {
            package::ReportError(reporter, "EntityTemplateCommandCreator: {}", r.error());
            return Error<>();
        } else {
            const String schema_path = os::Path::join(m_fbs_dir, *r);
            if (!os::Path::is_file(schema_path)) {
                package::ReportError(reporter,
                                     "EntityTemplateCommandCreator: Could not find schema file {}",
                                     schema_path);
                return Error<>();
            }
            args.push_back("-s");
            schema_index = args.size();
            args.push_back(schema_path.c_str());
        }

        args.push_back("-o");
        args.push_back(fmt::to_string(params.output_file));

        if (auto r = params.parser.read_string(kInputElem); !r) {
            package::ReportError(reporter, "EntityTemplateCommandCreator: {}", r.error());
            return Error<>();
        } else {
            String file = os::Path::join(params.current_directory, *r);
            if (!os::Path::is_file(file)) {
                package::ReportError(reporter,
                                     "EntityTemplateCommandCreator: File {} does note exist", file);
                return Error<>();
            }
            args.push_back(file.c_str());
        }

        EntityTemplateCommand* command = m_commands.create();
        command->m_args = std::move(args);
        command->m_schema_index = schema_index;
        return Ok<package::ICommand*>(command);
    }

    void shutdown() override { m_commands.clear(); }

    const char* help_string() const override {
        return "EntityTemplate Command: Create Modus entity template files.\n"
               "Arguments:\n"
               "    input:str   EntityTemplate input file(required)\n"
               "    schema:str  EntityTemplate schema path relative to binary flatbuffer directory "
               "(required)\n";
    }
};

}    // namespace modus::graphics

extern "C" {
MODUS_PACKAGE_COMMAND_EXPORT modus::package::ICommandCreator*
MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME() {
    return new modus::graphics::EntityTemplateCommandCreator();
}

MODUS_PACKAGE_COMMAND_EXPORT void MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME(
    modus::package::ICommandCreator* ptr) {
    delete ptr;
}
}
