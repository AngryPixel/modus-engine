/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

namespace modus::math {
struct Transform;
}

namespace modus::engine::gameplay {

class MODUS_ENGINE_EXPORT FreeRoamCameraController {
   private:
    glm::vec3 m_position;
    glm::vec3 m_forward;
    glm::vec3 m_right;
    glm::vec3 m_up;
    f32 m_yaw;
    f32 m_pitch;

   public:
    FreeRoamCameraController() { reset(); }

    void reset(const glm::vec3& position = glm::vec3(0.f));

    void set_position(const glm::vec3& position);

    void forward(const f32 amount);

    void backward(const f32 amount);

    void strafe_left(const f32 amount);

    void strafe_right(const f32 amount);

    void rotate(const f32 angle_x_axis_degrees, const f32 angle_y_axis_degrees);

    void rotate_y_axis(const f32 angle_degrees) { rotate(0.0f, angle_degrees); }

    void rotate_x_axis(const f32 angle_degrees) { rotate(angle_degrees, 0.0f); }

    void to_camera_transform(math::Transform& t);
};

}    // namespace modus::engine::gameplay
