/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <core/action_queue.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>

namespace modus::engine {
class Engine;
class ModuleGameplay;
}    // namespace modus::engine
namespace modus::engine::gameplay {

class GameWorldCommand;

#if defined(MODUS_DEBUG)
#define MODUS_GAMEPLAY_GAMEWORLD_COMMAND_QUEUE_DEBUG
#endif

namespace gameworld_detail {
MODUS_ENGINE_EXPORT u32 next_gameworld_storage_index();
}

template <typename T>
struct GameWorldStorageTrait {
    MODUS_ENGINE_EXPORT static u32 storage_index();
};

class MODUS_ENGINE_EXPORT GameWorld {
   protected:
    EntityManager m_entity_manager;
    ThreadSafeActionQueue<void(Engine&, GameWorld&), GameplayAllocator> m_command_queue;
#if defined(MODUS_GAMEPLAY_GAMEWORLD_COMMAND_QUEUE_DEBUG)
    bool m_processing_queue = false;
#endif
   private:
    Vector<void*> m_gameworld_storage;

   public:
    GameWorld();

    virtual ~GameWorld() = default;

    virtual Result<> initialize(Engine&);

    virtual Result<> shutdown(Engine&);

    virtual void on_enter(Engine&);

    virtual void on_exit(Engine&);

    virtual void tick(Engine& engine, const IMilisec tick) = 0;

    virtual void tick_fixed(Engine& engine, const IMilisec tick) = 0;

    virtual EntityId main_camera() const;

    EntityManager& entity_manager() { return m_entity_manager; }

    const EntityManager& entity_manager() const { return m_entity_manager; }
    template <typename T>
    void push_command(T&& command) {
        if (!m_command_queue.queue(std::forward<T>(command))) {
            modus_assert_message(false, "Failed to push command to queue");
        }
    }

    Result<> initialize_command_queue(const usize allocator_capacity = 4096);

    void process_command_queue(Engine& engine);

    /// Store an arbitrary pointer in the GameWorld to be used by any gameplay system
    template <typename T>
    void set_storage(NotMyPtr<std::remove_cv_t<T>> ptr) {
        const u32 index = GameWorldStorageTrait<std::remove_cv_t<T>>::storage_index();
        m_gameworld_storage.resize(std::max(usize(index + 1), m_gameworld_storage.size()), nullptr);
        m_gameworld_storage[index] = ptr.get();
    }

    /// Reset an arbitrary pointer stored in the GameWorld
    template <typename T>
    void reset_storage() {
        const u32 index = GameWorldStorageTrait<std::remove_cv_t<T>>::storage_index();
        m_gameworld_storage.resize(std::max(usize(index + 1), m_gameworld_storage.size()), nullptr);
        m_gameworld_storage[index] = nullptr;
    }

    /// Retrieve an arbitrary pointer stored in the GameWorld. May return nullptr if no value
    /// has been stored.
    template <typename T>
    NotMyPtr<std::remove_cv_t<T>> from_storage() {
        const u32 index = GameWorldStorageTrait<std::remove_cv_t<T>>::storage_index();
        modus_assert(index < m_gameworld_storage.size());
        return reinterpret_cast<std::remove_cv_t<T>*>(m_gameworld_storage[index]);
    }

    /// Retrieve an arbitrary pointer stored in the GameWorld. May return nullptr if no value
    /// has been stored.
    template <typename T>
    NotMyPtr<const std::remove_cv_t<T>> from_storage() const {
        const u32 index = GameWorldStorageTrait<std::remove_cv_t<T>>::storage_index();
        modus_assert(index < m_gameworld_storage.size());
        return reinterpret_cast<const std::remove_cv_t<T>*>(m_gameworld_storage[index]);
    }
};

class MODUS_ENGINE_EXPORT ThreadSafeGameWorldAccessor final {
   private:
    NotMyPtr<GameWorld> m_game_world;

   public:
    ThreadSafeGameWorldAccessor(GameWorld& game_world) : m_game_world(&game_world) {}

    template <typename T>
    void push_command(T&& command) {
        m_game_world->push_command(std::forward<T>(command));
    }
};

}    // namespace modus::engine::gameplay

#define MODUS_GAMEPLAY_GAMEWORLD_STORAGE_IMPL(X)                                    \
    namespace modus::engine::gameplay {                                             \
    template <>                                                                     \
    MODUS_ENGINE_EXPORT u32 GameWorldStorageTrait<X>::storage_index() {             \
        static const u32 kIndex = gameworld_detail::next_gameworld_storage_index(); \
        return kIndex;                                                              \
    }                                                                               \
    }
