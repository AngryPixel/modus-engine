/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>
#include <math/transform.hpp>

namespace modus::engine::gameplay {
struct TransformComponent;
struct MODUS_ENGINE_EXPORT TransformAnimationComponent final : public Component {
    using AnimationFn = void (*)(TransformComponent&, const FSeconds, const FSeconds);
    FSeconds m_duration_sec;
    FSeconds m_elapsed_sec;
    AnimationFn m_animation;
    bool m_loop;
    bool m_paused;

    void on_create(const EntityId) override {
        m_duration_sec = FSeconds(0.f);
        m_elapsed_sec = FSeconds(0.f);
        m_animation = [](TransformComponent&, const FSeconds, const FSeconds) -> void {};
        m_loop = false;
        m_paused = true;
    }
};

}    // namespace modus::engine::gameplay
namespace modus::engine::gameplay::fbs::ecs {
struct TransformAnimationComponent;
}
MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(TransformAnimationComponent)

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<TransformAnimationComponent> {
    using StorageType = SparseStorage<TransformAnimationComponent>;
    using FlatbufferType = fbs::ecs::TransformAnimationComponent;
};

template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<TransformAnimationComponent> {
    using ComponentType = TransformAnimationComponent;
    using IntermediateType = TransformAnimationComponent;
    using StorageType = ComponentTraits<TransformAnimationComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.transform_animation();
    }
};
}    // namespace modus::engine::gameplay