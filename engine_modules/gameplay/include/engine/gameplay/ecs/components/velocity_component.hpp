/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/component_storage/vector_storage.hpp>
#include <engine/gameplay/module_includes.hpp>
#include <math/math.hpp>

namespace modus::engine::gameplay {

struct MODUS_ENGINE_EXPORT VelocityComponent final : public Component {
   public:
    glm::vec3 velocity;
    void on_create(const EntityId) override { velocity = glm::vec3(0.0f); }
};
}    // namespace modus::engine::gameplay

namespace modus::engine::gameplay::fbs::ecs {
struct VelocityComponent;
}

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(VelocityComponent)

namespace modus::engine::gameplay {

template <>
struct ComponentTraits<VelocityComponent> {
    using StorageType = VectorStorage<VelocityComponent>;
    using FlatbufferType = fbs::ecs::VelocityComponent;
};

template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<VelocityComponent> {
    using ComponentType = VelocityComponent;
    using IntermediateType = glm::vec3;
    using StorageType = ComponentTraits<VelocityComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out.velocity = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.velocity();
    }
};

}    // namespace modus::engine::gameplay
