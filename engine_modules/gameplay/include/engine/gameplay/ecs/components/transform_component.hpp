/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/vector_storage.hpp>
#include <math/transform.hpp>

namespace modus::engine::gameplay {

enum class AttachError { AlreadyAttached, NoTransformComponent, CyclicAttachment, AttachingToSelf };

using AttachResult = Result<void, AttachError>;

struct MODUS_ENGINE_EXPORT TransformComponent final : public Component {
    friend class TransformSystem;

   private:
    math::Transform m_local;
    math::Transform m_world;
    EntityId m_this_id;
    EntityId m_parent_id;
    bool m_attachment_dirty;
    bool m_local_dirty;
    bool m_world_dirty;

   public:
    void on_create(const EntityId id) override {
        m_this_id = id;
        m_parent_id = EntityId();
        m_local.reset();
        m_world.reset();
        m_attachment_dirty = false;
        m_local_dirty = false;
        m_world_dirty = false;
    }

    const math::Transform& local_transform() const { return m_local; }

    const math::Transform& world_transform() const { return m_world; }

    inline void set_transform(const math::Transform& transform) {
        m_local = transform;
        m_local_dirty = true;
    }

    inline void translate(const glm::vec3& translation) {
        m_local.translate(translation);
        m_local_dirty = true;
    }

    inline void translate(const f32 x, const f32 y, const f32 z) {
        m_local.translate(x, y, z);
        m_local_dirty = true;
    }

    inline void set_translation(const glm::vec3& translation) {
        m_local.set_translation(translation);
        m_local_dirty = true;
    }

    inline void set_translation(const f32 x, const f32 y, const f32 z) {
        m_local.set_translation(x, y, z);
        m_local_dirty = true;
    }

    inline void rotate(const glm::quat& rotation) {
        m_local.rotate(rotation);
        m_local_dirty = true;
    }

    inline void rotate_prefix(const glm::quat& rotation) {
        m_local.rotate_prefix(rotation);
        m_local_dirty = true;
    }

    inline void set_rotation(const glm::quat& rotation) {
        m_local.set_rotation(rotation);
        m_local_dirty = true;
    }

    inline void scale(const f32 scale) {
        m_local.scale(scale);
        m_local_dirty = true;
    }

    inline void set_scale(const f32 scale) {
        m_local.set_scale(scale);
        m_local_dirty = true;
    }

    inline void transform(const math::Transform& t) {
        m_local.transform(t);
        m_local_dirty = true;
    }

    inline void invert() {
        m_local.invert();
        m_local_dirty = true;
    }

    inline void clear_dirty() {
        m_local_dirty = false;
        m_world_dirty = false;
    }

    bool did_world_transform_update() const { return m_world_dirty; }

    AttachResult attach(EntityManager& entity_manager, const EntityId dest);

    void deattach(EntityManager&);
};
}    // namespace modus::engine::gameplay
namespace modus::engine::gameplay::fbs::ecs {
struct TransformComponent;
}

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(TransformComponent)

namespace modus::engine::gameplay {

template <>
struct ComponentTraits<TransformComponent> {
    using StorageType = VectorStorage<TransformComponent>;
    using FlatbufferType = fbs::ecs::TransformComponent;
};

template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<TransformComponent> {
    using ComponentType = TransformComponent;
    using IntermediateType = math::Transform;
    using StorageType = ComponentTraits<TransformComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out.set_transform(in);
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.transform();
    }
};

}    // namespace modus::engine::gameplay