/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::gameplay {

class EntityTemplate;
class MODUS_ENGINE_EXPORT EntityTemplateRegistry {
   private:
    HashMap<String, NotMyPtr<EntityTemplate>> m_templates;

   public:
    EntityTemplateRegistry();
    ~EntityTemplateRegistry();

    MODUS_CLASS_DISABLE_COPY_MOVE(EntityTemplateRegistry);

    Result<> register_template(NotMyPtr<EntityTemplate> entity_template);

    void unregister_template(NotMyPtr<EntityTemplate> entity_template);

    Result<NotMyPtr<const EntityTemplate>> resolve(const StringSlice template_namespace) const;
};

}    // namespace modus::engine::gameplay
