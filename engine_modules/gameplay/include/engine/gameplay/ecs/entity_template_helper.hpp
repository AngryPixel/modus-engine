/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/allocator/pool_allocator.hpp>
#include <engine/gameplay/ecs/entity_template.hpp>
#include <engine/gameplay/ecs/entity_template_component_loader.hpp>

#include <flatbuffers/flatbuffers.h>

namespace modus::engine::gameplay {

template <typename T>
struct EntityTemplateFlatbufferHelperTraits {
    static bool verifiy(flatbuffers::Verifier&);
    static const T* get(const u8*);
};

template <typename FlatbufferType, typename Allocator, typename... Components>
class EntityTemplateFlatbufferHelper : public EntityTemplate {
   private:
    using LoaderType = EntityTemplateComponentLoader<Components...>;
    struct Data final : public EntityTemplateData {
        typename LoaderType::StorageType storage;
        Data(typename LoaderType::StorageType&& in_storage) : storage(std::move(in_storage)) {}
    };
    mutable PoolAllocatorTyped<Data, Allocator> m_allocator;

   public:
    EntityTemplateFlatbufferHelper(const StringSlice template_namespace, const u32 items_per_block)
        : EntityTemplate(template_namespace), m_allocator(items_per_block) {}

    Result<> verify_contents(const ByteSlice bytes) const override {
        flatbuffers::Verifier verifier(bytes.data(), bytes.size());
        const bool result = EntityTemplateFlatbufferHelperTraits<FlatbufferType>::verify(verifier);
        if (!result) {
            return Error<>();
        }
        return Ok<>();
    }

    void extract_asset_paths(Vector<String>& out_assets, const ByteSlice bytes) const override {
        const FlatbufferType* fb_type =
            EntityTemplateFlatbufferHelperTraits<FlatbufferType>::get(bytes.data());
        LoaderType::extract_asset_paths(out_assets, *fb_type);
    }

    Result<NotMyPtr<engine::gameplay::EntityTemplateData>> load(
        engine::Engine& engine,
        const ByteSlice bytes) const override {
        const FlatbufferType* fb_type =
            EntityTemplateFlatbufferHelperTraits<FlatbufferType>::get(bytes.data());
        typename LoaderType::StorageType storage;
        if (!LoaderType ::load(engine, storage, *fb_type)) {
            return Error<>();
        }
        return Ok(NotMyPtr<engine::gameplay::EntityTemplateData>(
            m_allocator.construct(std::move(storage))));
    }
    void unload(engine::Engine& engine,
                NotMyPtr<engine::gameplay::EntityTemplateData> data) const override {
        NotMyPtr<Data> typed_data = data;
        LoaderType::unload(engine, typed_data->storage);
        m_allocator.destroy(typed_data.get());
    }

    Result<engine::gameplay::EntityId> spawn(
        engine::Engine& engine,
        engine::gameplay::EntityTemplate::SpawnParams& params,
        NotMyPtr<const engine::gameplay::EntityTemplateData> data) const override {
        NotMyPtr<const Data> typed_data = data;
        return LoaderType::spawn(engine, params, typed_data->storage);
    }
};

}    // namespace modus::engine::gameplay

#define MODUS_GAMPLEY_ENTITY_TEMPLATE_FLATBUFFER_HELPER_TRAIT(N, F)                      \
    namespace modus::engine::gameplay {                                                  \
    template <>                                                                          \
    struct EntityTemplateFlatbufferHelperTraits<N::F> {                                  \
        static bool verify(flatbuffers::Verifier& v) { return N::Verify##F##Buffer(v); } \
        static const N::F* get(const u8* bytes) { return N::Get##F(bytes); }             \
    };                                                                                   \
    }
