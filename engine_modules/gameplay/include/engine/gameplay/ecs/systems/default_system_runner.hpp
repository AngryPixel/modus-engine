/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <core/fixed_vector.hpp>

namespace modus {
class GID;
}

namespace modus::engine {
class Engine;
}

namespace modus::engine::gameplay {

class System;
class EntityManager;
class GameWorld;

class MODUS_ENGINE_EXPORT DefaultSystemRunner {
   private:
    static constexpr u32 kMaxSystems = 32;
    FixedVector<NotMyPtr<System>, kMaxSystems> m_systems;
    u32 m_init_count = 0;

   public:
    DefaultSystemRunner() = default;

    struct Registration {
        NotMyPtr<System> system;
        Slice<GID> dependencies;
    };

    MODUS_CLASS_DISABLE_COPY_MOVE(DefaultSystemRunner);

    Result<> register_systems(SliceMut<Registration> systems);

    Result<> initialize(Engine&, GameWorld&, EntityManager&);

    void shutdown(Engine&, GameWorld&, EntityManager&);

    void update(Engine&, const IMilisec tick, GameWorld&, EntityManager&);
};

class MODUS_ENGINE_EXPORT InlineSystemRunner {
   public:
    void update(Engine& engine,
                const IMilisec tick,
                GameWorld&,
                EntityManager&,
                SliceMut<NotMyPtr<System>>);
};

}    // namespace modus::engine::gameplay
