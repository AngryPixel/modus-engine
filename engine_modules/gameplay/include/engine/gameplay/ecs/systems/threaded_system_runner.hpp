/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <jobsys/jobsys_types.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::gameplay {

class System;
class EntityManager;
class GameWorld;

class MODUS_ENGINE_EXPORT ThreadedSystemRunnerJob {
    friend class ThreadedSystemRunner;

   private:
    Vector<ThreadedSystemRunnerJob> m_next;
    NotMyPtr<System> m_system;
    jobsys::JobHandle m_parent_job_handle;
    bool m_run_on_main_thread = false;

   public:
    ThreadedSystemRunnerJob() = default;
    ThreadedSystemRunnerJob(NotMyPtr<System> system, const bool run_on_main_thread = false)
        : m_system(system), m_run_on_main_thread(run_on_main_thread) {}

    [[nodiscard]] ThreadedSystemRunnerJob&& chain(ThreadedSystemRunnerJob&& job) {
        m_next.push_back(std::move(job));
        return std::move(*this);
    }

    [[nodiscard]] ThreadedSystemRunnerJob&& chain_parallel(
        std::initializer_list<ThreadedSystemRunnerJob> jobs) {
        m_next.reserve(jobs.size());
        for (auto& job : jobs) {
            m_next.push_back(std::move(job));
        }
        return std::move(*this);
    }
};

class MODUS_ENGINE_EXPORT ThreadedSystemRunner {
   private:
    ThreadedSystemRunnerJob m_job;
    Vector<NotMyPtr<ThreadedSystemRunnerJob>> m_job_stack;

   public:
    ThreadedSystemRunner() = default;
    MODUS_CLASS_DEFAULT_COPY_MOVE(ThreadedSystemRunner);

    void set_job(ThreadedSystemRunnerJob&& job);

    Result<> initialize(Engine&, GameWorld&, EntityManager&);

    void shutdown(Engine&, GameWorld&, EntityManager&);

    void update(Engine&, const IMilisec tick, GameWorld&, EntityManager&);

   private:
    template <typename T>
    bool for_each_job(T&&);
};

}    // namespace modus::engine::gameplay