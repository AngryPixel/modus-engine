/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <core/guid.hpp>
#include <core/handle_pool_shared.hpp>

namespace modus::engine {
class Engine;
class ThreadSafeEngineAccessor;
}    // namespace modus::engine

namespace modus::engine::gameplay {

MODUS_DECLARE_HANDLE_TYPE(EntityId, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(EntityIndex, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(ComponentDestroyedCallbackHandle, std::numeric_limits<u32>::max());

// Expect traits
//      using StorageType = ...

/// ComponentTraits
/// Example:
/// struct ComponentTraits<T> {
///     using StorageType = HashMapStorage<T>;
///     using FlatbufferType = ...;
/// }
template <typename T>
struct ComponentTraits;

///
/// e.g.: ComponentNameTrait<Component> { static consexpr const char* name = "";}
template <typename T>
struct ComponentNameTrait;

/// Component loader specifies how a component should load itself from a given flatbuffer storage
/// schema and whether there are assets that require loading.
template <typename C>
struct ComponentLoaderTraits {
    /// Component Type
    using ComponentType = C;
    /// Intermediate representation that will be used to create new component of later. Could be the
    /// same as the component or a different data type
    using IntermediateType = C;
    /// Flatbuffer storage type
    using StorageType = typename ComponentTraits<C>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&) {
        return Ok(IntermediateType());
    }
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&, ComponentType&, const IntermediateType&, const EntityId);
    template <typename T>
    static const StorageType* from_collection(const T&);
};

template <typename T>
using ComponentTraitsV2 = ComponentTraits<T>;

class MODUS_ENGINE_EXPORT Component {
   public:
    virtual ~Component() = default;

    virtual void on_create(const EntityId id) = 0;
};

class GameWorld;
class EntityManager;
class ThreadSafeEntityManagerAccessor;
class ThreadSafeGameWorldAccessor;

class MODUS_ENGINE_EXPORT System {
   private:
    const GID m_guid;
    const StringSlice m_name;

   protected:
    System(StringSlice guid, StringSlice name);
    System(StringSlice name);

   public:
    System() = delete;

    virtual ~System() = default;

    const GID& guid() const { return m_guid; }

    StringSlice name() const { return m_name; }

    virtual Result<> initialize(Engine&, GameWorld&, EntityManager&) { return Ok<>(); }

    virtual void shutdown(Engine&, GameWorld&, EntityManager&) {}

    virtual void update(Engine& engine, const IMilisec tick, GameWorld&, EntityManager&) = 0;

    virtual void update_threaded(ThreadSafeEngineAccessor&,
                                 const IMilisec tick,
                                 ThreadSafeGameWorldAccessor&,
                                 ThreadSafeEntityManagerAccessor&) = 0;

    virtual void pre_update(Engine&, GameWorld&, EntityManager&){};

    virtual void post_update(Engine&, GameWorld&, EntityManager&){};
};

namespace component_detail {
static constexpr u32 kMaxExpectedComponentTypes = 32;
MODUS_ENGINE_EXPORT u32 generate_component_index();
}    // namespace component_detail

template <typename T>
struct MODUS_ENGINE_EXPORT ComponentIdTraits;

}    // namespace modus::engine::gameplay

#define MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(C)                                \
    namespace modus::engine::gameplay {                                           \
    template <>                                                                   \
    struct MODUS_ENGINE_EXPORT ComponentIdTraits<C> {                             \
        static_assert(std::is_base_of_v<Component, C>, #C " is not a component"); \
        MODUS_ENGINE_EXPORT static u32 index();                                   \
    };                                                                            \
    template <>                                                                   \
    struct ComponentNameTrait<C> {                                                \
        static constexpr const char* value = #C;                                  \
    };                                                                            \
    }

#define MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(C)                                          \
    namespace modus::engine::gameplay {                                                  \
    u32 ComponentIdTraits<C>::index() {                                                  \
        static const u32 kComponentIndex = component_detail::generate_component_index(); \
        return kComponentIndex;                                                          \
    }                                                                                    \
    }

namespace modus {
template <>
struct hash<engine::gameplay::EntityIndex> {
    std::size_t operator()(const engine::gameplay::EntityIndex& type) const {
        return modus::hash<engine::gameplay::EntityIndex::value_type>{}(type.value());
    }
};
}    // namespace modus

namespace modus {
template <>
struct hash<engine::gameplay::EntityId> {
    std::size_t operator()(const engine::gameplay::EntityId& type) const {
        return modus::hash<engine::gameplay::EntityId::value_type>{}(type.value());
    }
};
}    // namespace modus

namespace fmt {
template <>
struct formatter<modus::engine::gameplay::EntityId> : formatter<modus::u32> {
    template <typename FormatContext>
    auto format(const modus::engine::gameplay::EntityId id, FormatContext& ctx) {
        return formatter<modus::u32>::format(id.value(), ctx);
    }
};
}    // namespace fmt
