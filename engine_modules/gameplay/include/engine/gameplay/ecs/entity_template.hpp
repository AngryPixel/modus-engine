/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::gameplay {

class EntityManager;

struct MODUS_ENGINE_EXPORT EntityTemplateData {
    virtual ~EntityTemplateData() = default;
};

/// Entity Template Interface required to be implemented by a specific entity template type.
/// The entity template payload is stored as a binary blob inside a well know EntityTemplate Asset
/// type. It is the responsibility of the deriver to verify and unpack that binary data so it can
/// be used to create a new entity.
///
/// By default all components will have a flatbuffer schema file form which they can be initialized.
/// The EntityTemplateComponentLoader class can be used to auto-generate all the boilerplate code
/// for a list of components. It also possible to perform custom loading steps or even embedded
/// different data into the binary blob. The only requirement is to make sure the type is registered
/// with EnityTemplateRegistry and that the template namespace identifier matches the one used
/// during the registration.
class MODUS_ENGINE_EXPORT EntityTemplate {
   private:
    String m_namespace;

   protected:
    EntityTemplate(const StringSlice template_namespace)
        : m_namespace(template_namespace.to_str()) {}

   public:
    virtual ~EntityTemplate() = default;
    const String& template_namespace() const { return m_namespace; }
    virtual Result<> verify_contents(const ByteSlice bytes) const = 0;
    virtual void extract_asset_paths(Vector<String>& out_assets, const ByteSlice bytes) const = 0;
    virtual Result<NotMyPtr<EntityTemplateData>> load(Engine& engine,
                                                      const ByteSlice bytes) const = 0;
    virtual void unload(Engine& engine, NotMyPtr<EntityTemplateData> data) const = 0;

    struct SpawnParams {
        EntityManager& entiy_manager;
        StringSlice name;
        u32 tag = 0;
    };

    virtual Result<EntityId> spawn(Engine& engine,
                                   SpawnParams& param,
                                   NotMyPtr<const EntityTemplateData> data) const = 0;
};

}    // namespace modus::engine::gameplay
