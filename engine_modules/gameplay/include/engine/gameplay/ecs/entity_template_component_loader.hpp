/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/ecs/ecs_types.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>

namespace modus::engine::gameplay {

namespace data_loader_detail {
template <typename>
struct void_ {
    typedef void type;
};
template <typename T, typename = void>
struct has_component_type {
    static constexpr bool const value = false;
};
template <typename T>
struct has_component_type<T,
                          typename void_<typename ComponentLoaderTraits<T>::ComponentType>::type> {
    static constexpr bool const value = true;
};

template <typename T, typename = void>
struct allow_empty_storage {
    static constexpr bool const value = false;
};
template <typename T>
struct allow_empty_storage<
    T,
    typename void_<typename ComponentLoaderTraits<T>::AllowMissingComponent>::type> {
    static constexpr bool const value = true;
};

template <typename Component>
void extract_assets(Vector<String>& out,
                    const typename ComponentLoaderTraits<Component>::StorageType* storage) {
    if (storage != nullptr) {
        ComponentLoaderTraits<Component>::extract_asset_paths(out, *storage);
    }
}

template <typename FBS, typename C>
[[nodiscard]] bool create_and_load(Engine& engine,
                                   EntityManager& entityManager,
                                   const EntityId id,
                                   const FBS& fbs) {
    if (auto r = entityManager.add_component<C>(id); !r) {
        MODUS_LOGE("Failed to create component {}", ComponentNameTrait<C>::value);
        return false;
    } else {
        return ComponentTraits<C>::LoaderType::load(engine, **r, fbs).is_value();
    }
}
template <typename Flatbuffer, typename Component>
bool load_component(Engine& engine,
                    typename ComponentLoaderTraits<Component>::IntermediateType& out,
                    const Flatbuffer& data) {
    const auto* storage = ComponentLoaderTraits<Component>::from_collection(data);
    if (storage == nullptr) {
        if constexpr (!data_loader_detail::allow_empty_storage<Component>::value) {
            MODUS_LOGE("Couldn't locate storage for {}", ComponentNameTrait<Component>::value);
            return false;
        } else {
            return true;
        }
    }

    if (auto r = ComponentLoaderTraits<Component>::load(engine, *storage); r) {
        out = r.release_value();
        return true;
    }
    MODUS_LOGE("Failed to load component {}", ComponentNameTrait<Component>::value);
    return false;
}

template <typename Component>
[[nodiscard]] bool create_and_set_component(
    Engine& engine,
    EntityManager& entity_manager,
    const EntityId id,
    const typename ComponentLoaderTraits<Component>::IntermediateType& in) {
    if (auto r = entity_manager.add_component<Component>(id); !r) {
        MODUS_LOGE("Failed to create component {}", ComponentNameTrait<Component>::value);
        return false;
    } else {
        auto r_cmp = ComponentLoaderTraits<Component>::to_component(engine, **r, in, id);
        if (!r_cmp) {
            MODUS_LOGE("Failed to setup component {}", ComponentNameTrait<Component>::value);
            entity_manager.destroy(id);
            return false;
        }
    }
    return true;
}
}    // namespace data_loader_detail

/// Helper utility to load a given list of components from a generate flatbuffer schema. e.g:
/// FlattbufferType& fbs_type;
/// EntityTemplateComponentLoader<TransformComponent>::form_fbs(..., fbs_type);
/// The generate code will verify at compile time if all the expected component members are there.
template <typename... Components>
struct EntityTemplateComponentLoader {
    static_assert((... && data_loader_detail::has_component_type<Components>::value),
                  "One of the components does not have a valid ComponentLoaderTrait");

    using StorageType = std::tuple<typename ComponentLoaderTraits<Components>::IntermediateType...>;

    template <typename Flatbuffer>
    [[nodiscard]] static bool load(Engine& engine, StorageType& storage, const Flatbuffer& data) {
        const bool result =
            (... &&
             data_loader_detail::load_component<Flatbuffer, Components>(
                 engine,
                 std::get<typename ComponentLoaderTraits<Components>::IntermediateType>(storage),
                 data));
        return result;
    }

    static void unload(Engine& engine, StorageType& storage) {
        (ComponentLoaderTraits<Components>::unload(
             engine,
             std::get<typename ComponentLoaderTraits<Components>::IntermediateType>(storage)),
         ...);
    }

    static Result<EntityId> spawn(Engine& engine,
                                  EntityTemplate::SpawnParams& params,
                                  const StorageType& storage) {
        auto r_entity = params.entiy_manager.create(params.name, params.tag);
        if (!r_entity) {
            MODUS_LOGE("Failed to create entity");
            return Error<>();
        }
        const bool result =
            (... &&
             data_loader_detail::create_and_set_component<Components>(
                 engine, params.entiy_manager, *r_entity,
                 std::get<typename ComponentLoaderTraits<Components>::IntermediateType>(storage)));
        if (!result) {
            params.entiy_manager.destroy(*r_entity);
            return Error<>();
        }
        return r_entity;
    }

    template <typename FBS>
    static void extract_asset_paths(Vector<String>& out, const FBS& data) {
        (data_loader_detail::extract_assets<Components>(
             out, ComponentLoaderTraits<Components>::from_collection(data)),
         ...);
    }
};

}    // namespace modus::engine::gameplay