/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/gameplay/module_includes.hpp>

#include <core/handle_pool.hpp>
#include <core/tuple_utils.hpp>
#include <engine/engine.hpp>
#include <engine/event/event_manager.hpp>
#include <engine/gameplay/ecs/component_storage/component_storage_v2.hpp>
#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::gameplay {

using ComponentDestroyedDelegate = Delegate<void(Engine&, const Component&)>;
using EntityDestroyedDelegate = Delegate<void(Engine&, const EntityId, EntityManager&)>;

/// Entity manager which holds all the entities and storage for their
/// components.
class MODUS_ENGINE_EXPORT EntityManager {
    template <bool IsConst, typename... Components>
    friend class EntityManagerViewBase;

   public:
    struct ComponentStorage {
        std::unique_ptr<ComponentStorageV2> m_pool;
        ComponentDestroyedDelegate m_destroy_callback;
        u32 m_component_index = std::numeric_limits<u32>::max();
    };

    using ComponentPools = gameplay::Vector<ComponentStorage>;
    static constexpr u32 kMaxComponentCount = component_detail::kMaxExpectedComponentTypes;
    using MaskType = std::bitset<kMaxComponentCount>;

   private:
    struct Entity {
        EntityId handle;
        EntityIndex index;
        u32 tag = 0;
        MaskType mask;
#if defined(MODUS_DEBUG)
        String name = String();
#endif
        volatile bool alive = true;
    };

    using EntityStorage = gameplay::Vector<Entity>;
    using EntityHandlePool = HandlePool<u32, GameplayAllocator, EntityId>;
    EntityHandlePool m_entity_handles;
    EntityStorage m_entities;
    mutable ComponentPools m_components;
    u32 m_active;
    u32 m_size;
    Vector<EntityDestroyedDelegate> m_entity_destroyed_callbacks;

    /// Validate access to component storage by using the reader writer counters in the component
    /// storages. Many readers can access the storage concurrently, but only one exclusive writer
    /// can have writable access to the storage.
    /// Note: This is not a fail proof validation strategy, but it should be able to catch obvious
    /// problems.
    template <typename... C>
    struct ComponentStorageAccessor {
        EntityManager& em;
        ComponentStorageAccessor(EntityManager& manager) : em(manager) {
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
            (increment_helper<std::is_const_v<C>>(
                 em.component_storage_for_type(ComponentIdTraits<std::remove_cv_t<C>>::index())
                     .value_or_panic()),
             ...);
#endif
        }
        ~ComponentStorageAccessor() {
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
            (decrement_helper<std::is_const_v<C>>(
                 em.component_storage_for_type(ComponentIdTraits<std::remove_cv_t<C>>::index())
                     .value_or_panic()),
             ...);
#endif
        }
        void validate() const {
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
            const bool valid =
                (em.component_storage_for_type(ComponentIdTraits<std::remove_cv_t<C>>::index())
                     .value_or_panic()
                     ->validate_reader_writer_access() &&
                 ...);
            if (!valid) {
                modus_panic(
                    "Illegal access to component storage. There can only be one writeable access "
                    "without any readers at the same time");
            }
#endif
        }
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
        template <bool IsConst>
        void increment_helper(NotMyPtr<ComponentStorageV2> storage) {
            if constexpr (IsConst) {
                storage->increment_reader_counter();
            } else {
                storage->increment_writer_counter();
            }
        }
        template <bool IsConst>
        void decrement_helper(NotMyPtr<ComponentStorageV2> storage) {
            if constexpr (IsConst) {
                storage->decrement_reader_counter();
            } else {
                storage->decrement_writer_counter();
            }
        }
#endif
    };
    /// Validate access to component storage by using the reader writer counters in the component
    /// storages. Many readers can access the storage concurrently, but only one exclusive writer
    /// can have writable access to the storage.
    /// Note: This is not a fail proof validation strategy, but it should be able to catch obvious
    /// problems.
    template <typename... C>
    struct ConstComponentStorageAccessor {
        const EntityManager& em;
        ConstComponentStorageAccessor(const EntityManager& manager) : em(manager) {
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
            (em.component_storage_for_type(ComponentIdTraits<std::remove_cv_t<C>>::index())
                 .value_or_panic()
                 ->increment_reader_counter(),
             ...);
#endif
        }
        ~ConstComponentStorageAccessor() {
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
            (em.component_storage_for_type(ComponentIdTraits<std::remove_cv_t<C>>::index())
                 .value_or_panic()
                 ->decrement_reader_counter(),
             ...);
#endif
        }
        void validate() const {
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
            const bool valid =
                (em.component_storage_for_type(ComponentIdTraits<std::remove_cv_t<C>>::index())
                     .value_or_panic()
                     ->validate_reader_writer_access() &&
                 ...);
            if (!valid) {
                modus_panic(
                    "Illegal access to component storage. There can only be one writeable access "
                    "without any readers at the same time");
            }
#endif
        }
    };

   public:
    EntityManager();

    template <typename T>
    Result<> register_component() {
        static_assert(std::is_base_of_v<Component, T>);

        const u32 cmp_idx = ComponentIdTraits<T>::index();
        if (cmp_idx >= kMaxComponentCount) {
            MODUS_LOGE(
                "EntityManager: Can't register component since the component indes {} is higher "
                "than the current count {}",
                cmp_idx, kMaxComponentCount);
            return Error<>();
        }

        if (m_components.size() < cmp_idx + 1) {
            m_components.resize(cmp_idx + 1);
        }

        if (m_components[cmp_idx].m_pool) {
            // Component already registered
            return Ok<>();
        }

        ComponentStorage& storage = m_components[cmp_idx];
        storage.m_pool = modus::make_unique<typename ComponentTraitsV2<T>::StorageType>();
        storage.m_component_index = cmp_idx;
        return Ok<>();
    }

    Result<> initialize(Engine& engine, const usize entity_count);

    Result<> shutdown(Engine& engine);

    void clear(Engine& engine);

    void add_entity_destroyed_callback(EntityDestroyedDelegate callback);

    void remove_entity_destroyed_callback(EntityDestroyedDelegate callback);

    Result<EntityId, void> create(const u32 tag = 0) { return create("NoName", tag); }

    Result<EntityId, void> create(const StringSlice& name, const u32 tag = 0);

    Result<> update_entity_tag(const EntityId id, const u32 tag);

    void destroy(const EntityId id);

    /// Change the update order for entities in the entity manager. This will
    /// also cause all components to shift their location in the storage where
    /// applicable.
    /// This can be used to ensure entities update in the correct order. E.g.:
    /// attachments.
    Result<> swap_update_order(const EntityId id1, const EntityId id2);

    Result<EntityIndex, void> entity_index(const EntityId id) const;

    Result<u32, void> entity_tag(const EntityId id) const;

    bool is_alive(const EntityId id) const;

    void update(Engine& engine);

    bool is_valid_entity(const EntityId id) const;

    u32 entity_count() const { return m_size; }

    template <typename T>
    inline Result<NotMyPtr<T>, void> component(const EntityId id) {
        auto entity_result = m_entity_handles.get(id);
        if (!entity_result) {
            return Error<>();
        }
        const Entity& entity = m_entities[*(entity_result.value())];
        const u32 cmp_index = ComponentIdTraits<T>::index();
        if (!entity.mask.test(cmp_index)) {
            return Error<>();
        }
        auto r_storage = component_storage_for_type(cmp_index);
        if (!r_storage) {
            return Error<>();
        }

        NotMyPtr<typename ComponentTraitsV2<T>::StorageType> storage_ptr = (*r_storage);
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
        if (!storage_ptr->validate_reader_writer_access()) {
            modus_panic(
                "Illegal access to component storage. There can only be one writeable access "
                "without any readers at the same time");
        }
#endif
        return Ok<NotMyPtr<T>>(storage_ptr->get(entity.index));
    }

    template <typename T>
    inline Result<NotMyPtr<const T>, void> component(const EntityId id) const {
        auto entity_result = m_entity_handles.get(id);
        if (!entity_result) {
            return Error<>();
        }
        const Entity& entity = m_entities[*(entity_result.value())];
        const u32 cmp_index = ComponentIdTraits<T>::index();
        if (!entity.mask.test(cmp_index)) {
            return Error<>();
        }
        auto r_storage = component_storage_for_type(cmp_index);
        if (!r_storage) {
            return Error<>();
        }
        NotMyPtr<typename ComponentTraitsV2<T>::StorageType> storage_ptr = (*r_storage);
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
        if (!storage_ptr->validate_reader_writer_access()) {
            modus_panic(
                "Illegal access to component storage. There can only be one writeable access "
                "without any readers at the same time");
        }
#endif
        return Ok<NotMyPtr<const T>>(storage_ptr->get(entity.index));
    }

    template <typename T>
    inline Result<NotMyPtr<T>, void> add_component(const EntityId id) {
        auto entity_result = m_entity_handles.get(id);
        if (!entity_result) {
            return Error<>();
        }

        const u32 cmp_index = ComponentIdTraits<T>::index();

        auto r_storage = component_storage_for_type(cmp_index);
        if (!r_storage) {
            return Error<>();
        }

        NotMyPtr<typename ComponentTraitsV2<T>::StorageType> storage_ptr = (*r_storage);
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
        if (!storage_ptr->validate_reader_writer_access()) {
            modus_panic(
                "Illegal access to component storage. There can only be one writeable access "
                "without any readers at the same time");
        }
#endif

        Entity& entity = m_entities[*(entity_result.value())];
        if (entity.mask.test(cmp_index)) {
            NotMyPtr<T> instance = storage_ptr->get(entity.index);
            return Ok<NotMyPtr<T>>(instance);
        }

        entity.mask.set(cmp_index, true);
        NotMyPtr<T> component = storage_ptr->new_component(entity.index);
        component->on_create(id);
        return Ok<NotMyPtr<T>>(component);
    }

    template <typename T>
    inline void remove_component(Engine& engine, const EntityId id) {
        MODUS_UNUSED(engine);
        auto entity_result = m_entity_handles.get(id);
        if (!entity_result) {
            return;
        }
        Entity& entity = m_entities[*(entity_result.value())];
        const u32 cmp_idx = ComponentIdTraits<T>::index();
        if (!entity.mask.test(cmp_idx)) {
            return;
        }

        auto r_component = component<T>(entity);
        if (!r_component) {
            return;
        }
        m_components[cmp_idx].m_destroy_callback(engine, **r_component);
        entity.mask.set(cmp_idx, false);
        auto storage = component_storage_for_type(cmp_idx).value_or_panic();
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
        if (!storage->validate_reader_writer_access()) {
            modus_panic(
                "Illegal access to component storage. There can only be one writeable access "
                "without any readers at the same time");
        }
#endif
        storage->release_component(entity.index);
    }

    template <typename T>
    inline bool has_component(const EntityId id) const {
        auto entity_result = m_entity_handles.get(id);
        if (!entity_result) {
            return false;
        }
        const Entity& entity = m_entities[*(entity_result.value())];
        const u32 cmp_idx = ComponentIdTraits<T>::index();
        return entity.mask.test(cmp_idx);
    }

    template <typename T>
    inline Result<> register_component_destroy_callback(ComponentDestroyedDelegate callback) {
        const u32 cmp_idx = ComponentIdTraits<T>::index();
        if (!callback || cmp_idx >= m_components.size() || !m_components[cmp_idx].m_pool) {
            return Error<>();
        }

        if (m_components[cmp_idx].m_destroy_callback) {
            return Error<>();
        }

        m_components[cmp_idx].m_destroy_callback = callback;
        return Ok<>();
    }

    template <typename T>
    inline void unregister_component_destroy_callback() {
        const u32 cmp_idx = ComponentIdTraits<T>::index();
        if (cmp_idx >= m_components.size() || !m_components[cmp_idx].m_pool) {
            return;
        }
        m_components[cmp_idx].m_destroy_callback = ComponentDestroyedDelegate();
    }

    template <typename T>
    inline void for_each(T& fn) {
        for (u32 i = 0; i < m_size; ++i) {
            fn(*this, m_entities[i]);
        }
    }

    template <typename T>
    inline void for_each(T&& fn) const {
        for (u32 i = 0; i < m_size; ++i) {
            fn(*this, m_entities[i]);
        }
    }

    template <typename... C>
    static MaskType make_mask() {
        MaskType mask;
        (mask.set(ComponentIdTraits<std::remove_cv_t<C>>::index(), true), ...);
        return mask;
    }

   private:
    template <typename T>
    inline T& component_or_panic(const Entity& e) {
        const u32 cmp_idx = ComponentIdTraits<std::remove_cv_t<T>>::index();
        auto storage = component_storage_for_type(cmp_idx).value_or_panic(
            "Failed to get storage for component");
        NotMyPtr<typename ComponentTraitsV2<std::remove_cv_t<T>>::StorageType> storage_ptr(
            (storage.get()));
        return static_cast<T&>(*(storage_ptr->get(e.index)));
    }

    template <typename T>
    inline const T& component_or_panic(const Entity& e) const {
        const u32 cmp_idx = ComponentIdTraits<std::remove_cv_t<T>>::index();
        auto storage = component_storage_for_type(cmp_idx).value_or_panic(
            "Failed to get storage for component");
        NotMyPtr<typename ComponentTraitsV2<std::remove_cv_t<T>>::StorageType> storage_ptr(
            (storage.get()));
        return static_cast<const T&>(*(storage_ptr->get(e.index)));
        (*storage);
    }

    void on_destroy_entity(Engine& engine, const EntityId id);

    Result<NotMyPtr<ComponentStorageV2>, void> component_storage_for_type(const u32 index) const;

    void release_components(Engine& engine, Entity& entity);

    usize update_internal(Engine& engine);

    usize allocate_entity() { return m_active++; }

    Result<> grow_or_expand(const usize expected);

    void swap_update_order_internal(Entity& entity1, Entity& entity2);
};

class MODUS_ENGINE_EXPORT ThreadSafeEntityManagerAccessor final {
    template <bool IsConst, typename... C>
    friend class EntityManagerViewBase;

   private:
    NotMyPtr<EntityManager> m_entity_manager;

   public:
    ThreadSafeEntityManagerAccessor(EntityManager& entity_manager)
        : m_entity_manager(&entity_manager) {}

    bool is_alive(const EntityId id) const { return m_entity_manager->is_alive(id); }

    Result<u32, void> entity_tag(const EntityId id) const {
        return m_entity_manager->entity_tag(id);
    }

    void destroy(const EntityId id) { m_entity_manager->destroy(id); }

    template <typename T>
    inline Result<NotMyPtr<const T>, void> component(const EntityId id) const {
        return m_entity_manager->component<T>(id);
    }
};

template <bool IsConst, typename... Components>
class MODUS_ENGINE_EXPORT EntityManagerViewBase {
   public:
    using EntityManagerType = std::conditional_t<IsConst, const EntityManager, EntityManager>;
    using ThreadSafeEntityManagerAccessorType =
        std::conditional_t<IsConst,
                           const ThreadSafeEntityManagerAccessor,
                           ThreadSafeEntityManagerAccessor>;

   private:
    NotMyPtr<EntityManagerType> m_manager;
    u32 m_range_start;
    u32 m_range_end;

    static_assert(!IsConst || (IsConst && (std::is_const_v<Components> && ...)),
                  "When IsConst=True, all component types must be const");

   public:
    EntityManagerViewBase() = delete;
    EntityManagerViewBase(EntityManagerType& manager)
        : m_manager(&manager), m_range_start(0), m_range_end(manager.entity_count()) {}
    EntityManagerViewBase(EntityManagerType& manager, const u32 start)
        : m_manager(&manager),
          m_range_start(std::min(start, manager.entity_count())),
          m_range_end(manager.entity_count()) {}
    EntityManagerViewBase(EntityManagerType& manager, const u32 start, const u32 end)
        : m_manager(&manager),
          m_range_start(std::min(start, manager.entity_count())),
          m_range_end(std::min(std::max(end, m_range_start), manager.entity_count())) {}
    EntityManagerViewBase(ThreadSafeEntityManagerAccessorType& accessor)
        : EntityManagerViewBase(*accessor.m_entity_manager) {}
    EntityManagerViewBase(ThreadSafeEntityManagerAccessorType& accessor, const u32 start)
        : EntityManagerViewBase(*accessor.m_entity_manager, start) {}
    EntityManagerViewBase(ThreadSafeEntityManagerAccessorType& accessor,
                          const u32 start,
                          const u32 end)
        : EntityManagerViewBase(*accessor.m_entity_manager, start, end) {}

    struct MODUS_ENGINE_EXPORT EntityAccessor {
        friend class Iterator;
        friend class EntityManagerViewBase;

       private:
        EntityId m_entity_id;
        u32 m_index;

       private:
        EntityAccessor() : m_entity_id(), m_index(std::numeric_limits<u32>::max()) {}
        EntityAccessor(const EntityId id, const u32 index) : m_entity_id(id), m_index(index) {}

       public:
        EntityId id() const { return m_entity_id; }
    };

    class MODUS_ENGINE_EXPORT Iterator {
       public:
        using this_type = Iterator;
        using difference_type = isize;

       private:
        std::conditional_t<IsConst,
                           EntityManager::ConstComponentStorageAccessor<Components...>,
                           EntityManager::ComponentStorageAccessor<Components...>>
            m_access_validator;
        NotMyPtr<EntityManagerType> m_manager;
        EntityAccessor m_accessor;
        u32 m_index_end;
        EntityManager::MaskType m_mask;
        EntityManager::MaskType m_mask_with_exclusion;

       public:
        Iterator() = delete;

        Iterator(NotMyPtr<EntityManagerType> entity_manager,
                 const u32 start_index,
                 const u32 end_index)
            : m_access_validator(*entity_manager),
              m_manager(entity_manager),
              m_accessor(EntityId(), start_index),
              m_index_end(end_index) {
            m_mask = EntityManager::make_mask<Components...>();
            m_mask_with_exclusion = m_mask;
            modus_assert(start_index <= m_index_end);
            if (start_index < m_index_end) {
                if (is_valid_entity(start_index)) {
                    update_accessor();
                } else {
                    advance();
                }
            }
        }

        Iterator(const Iterator& rhs)
            : m_manager(rhs.m_manager),
              m_accessor(rhs.m_accessor()),
              m_index_end(rhs.m_index_end),
              m_mask(rhs.m_mask),
              m_mask_with_exclusion(rhs.m_mask_with_exclusion) {
            m_access_validator.validate();
        }

        Iterator& operator=(const Iterator& rhs) {
            m_manager = rhs.m_manager;
            m_accessor = rhs.m_accessor;
            m_mask = rhs.m_mask;
            m_mask_with_exclusion = rhs.m_mask_with_exclusion;
            return *this;
        }

        ~Iterator() {}

        void set_exclusion_mask(const EntityManager::MaskType mask) {
            m_mask_with_exclusion |= mask;
        }

        bool operator==(const this_type& rhs) const {
            return m_manager == rhs.m_manager && m_accessor.m_index == rhs.m_accessor.m_index &&
                   m_index_end == rhs.m_index_end;
        }

        bool operator!=(const this_type& rhs) const { return !(*this == rhs); }

        this_type& operator++() {
            advance();
            return *this;
        }

        this_type operator++(int) {
            auto copy = *this;
            advance();
            return copy;
        }

        EntityAccessor& operator*() { return m_accessor; }

        const EntityAccessor& operator*() const { return m_accessor; }

        EntityAccessor* operator->() { return &m_accessor; }

        const EntityAccessor* operator->() const { return &m_accessor; }

       private:
        bool is_valid_entity(const u32 index) const {
            return (m_manager->m_entities[index].mask & m_mask_with_exclusion) == m_mask;
        }

        void advance() {
            while (++m_accessor.m_index < m_index_end && !is_valid_entity(m_accessor.m_index)) {
            }
            if (m_accessor.m_index < m_index_end) {
                update_accessor();
            }
        }

        inline void update_accessor() {
            m_access_validator.validate();
            m_accessor.m_entity_id = m_manager->m_entities[m_accessor.m_index].handle;
        }
    };
    using iterator = Iterator;

    iterator begin() { return iterator(m_manager, m_range_start, m_range_end); }

    iterator end() { return iterator(m_manager, m_range_end, m_range_end); }

    template <typename Component, bool B = IsConst, std::enable_if_t<!B, int> = 0>
    Component& get(const EntityAccessor& accessor) {
        static_assert((std::is_same_v<Component, Components> || ...),
                      "Component is not int the component list");
        modus_assert(accessor.m_index >= m_range_start);
        modus_assert(accessor.m_index < m_range_end);
        return m_manager->template component_or_panic<Component>(
            m_manager->m_entities[accessor.m_index]);
    }

    template <typename Component>
    const Component& get(const EntityAccessor& accessor) const {
        static_assert(std::is_const_v<Component>, "This method can only be used for read access");
        static_assert((std::is_same_v<Component, Components> || ...),
                      "Component is not int the component list");
        modus_assert(accessor.m_index >= m_range_start);
        modus_assert(accessor.m_index < m_range_end);
        return m_manager->template component_or_panic<Component>(
            m_manager->m_entities[accessor.m_index]);
    }

    template <typename T>
    inline void for_each(T&& fn) {
        static_assert(std::is_invocable_v<T, EntityId, Components&...>,
                      "Invalid function/callable signature");
        for (auto& entity : *this) {
            fn(entity.id(), this->get<Components>(entity)...);
        }
    }

    template <typename T>
    inline void for_each_excluding(const EntityManager::MaskType exclusion_mask, T&& fn) {
        static_assert(std::is_invocable_v<T, EntityId, Components&...>,
                      "Invalid function/callable signature");
        auto it = begin();
        auto it_end = end();
        it.set_exclusion_mask(exclusion_mask);
        for (; it != it_end; ++it) {
            fn(it->id(), this->get<Components>(*it)...);
        }
    }

    template <typename T>
    inline void for_each_with_tag(const u32 tag, T&& fn) {
        static_assert(std::is_invocable_v<T, EntityId, Components&...>,
                      "Invalid function/callable signature");
        for (auto& entity : *this) {
            if (m_manager->m_entities[entity.m_index].tag == tag) {
                fn(entity.id(), this->get<Components>(entity)...);
            }
        }
    }
};

template <typename... Components>
using EntityManagerView = EntityManagerViewBase<true, Components...>;
template <typename... Components>
using EntityManagerViewMut = EntityManagerViewBase<false, Components...>;

}    // namespace modus::engine::gameplay
