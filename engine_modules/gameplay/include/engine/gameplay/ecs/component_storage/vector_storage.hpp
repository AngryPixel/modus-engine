/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/component_storage_v2.hpp>

namespace modus::engine::gameplay {

/// Vector Storage, use for components which are used on every entity
template <typename T>
class MODUS_ENGINE_EXPORT VectorStorageV2 : public ComponentStorageV2 {
    static_assert(std::is_base_of_v<Component, T> && !std::is_same_v<T, Component>,
                  "T must be a sublcass of Component");

   public:
    using value_type = T;
    using index_type = u16;
    static constexpr index_type kIndexInvalid = std::numeric_limits<index_type>::max();
    static constexpr index_type kMaxCapacity = kIndexInvalid - 1;

   protected:
    gameplay::Vector<T> m_components;

   public:
    VectorStorageV2() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(VectorStorageV2, VectorStorageV2<T>);

    Result<> grow_to(const usize size) override final {
        usize new_size = size;
        if (size > usize(kMaxCapacity)) {
            new_size = kMaxCapacity;
            if (new_size == m_components.size()) {
                modus_assert_message(false, "Max component capacity reached");
                return Error<>();
            }
        }
        m_components.resize(new_size);
        return Ok<>();
    }

    NotMyPtr<Component> new_component(const EntityIndex index) override final {
        modus_assert(index.value() < m_components.size());
        return &m_components[index.value()];
    }

    void release_component([[maybe_unused]] const EntityIndex index) override final {
        modus_assert(index.value() < m_components.size());
    }

    NotMyPtr<Component> get(const EntityIndex index) override final {
        modus_assert(index.value() < m_components.size());
        return &m_components[index.value()];
    }

    void swap_components(const EntityIndex index1, const EntityIndex index2) override final {
        modus_assert(index1.value() < m_components.size());
        modus_assert(index2.value() < m_components.size());
        std::swap(m_components[index1.value()], m_components[index2.value()]);
    }
};

template <typename T>
using VectorStorage = VectorStorageV2<T>;

}    // namespace modus::engine::gameplay
