/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/component_storage_v2.hpp>

namespace modus::engine::gameplay {

/// Unique Storage, use for components which should only have one instance
template <typename T>
class MODUS_ENGINE_EXPORT UniqueStorageV2 final : public ComponentStorageV2 {
    static_assert(std::is_base_of_v<Component, T> && !std::is_same_v<T, Component>,
                  "T must be a sublcass of Component");

   public:
    EntityIndex m_entity_index;
    T m_component;

   public:
    UniqueStorageV2() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(UniqueStorageV2, UniqueStorageV2<T>);

    Result<> grow_to(const usize) override { return Ok<>(); }

    NotMyPtr<Component> new_component(const EntityIndex index) override {
        if (m_entity_index) {
            modus_panic(
                "UniqueStorage: Attempting to create more than one comoponent "
                "of the same type\n");
        }

        m_entity_index = index;
        return &m_component;
    }

    void release_component(const EntityIndex entity_index) override {
        if (m_entity_index == entity_index) {
            m_entity_index = EntityIndex();
        }
    }

    NotMyPtr<Component> get(const EntityIndex entity_index) override {
        MODUS_UNUSED(entity_index);
        modus_assert(m_entity_index == entity_index);
        return &m_component;
    }

    void swap_components(const EntityIndex i1, const EntityIndex i2) override {
        if (i1 == m_entity_index) {
            m_entity_index = i2;
        } else if (i2 == m_entity_index) {
            m_entity_index = i1;
        }
#if defined(MODUS_ENABLE_ASSERTS)
        else {
            modus_assert("Unhandled swap case");
        }
#endif
    }
};

template <typename T>
using UniqueStorage = UniqueStorageV2<T>;

}    // namespace modus::engine::gameplay
