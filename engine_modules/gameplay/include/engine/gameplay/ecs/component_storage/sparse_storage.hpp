/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/component_storage_v2.hpp>

namespace modus::engine::gameplay {

/// Spare Storage, use for components which are use frequently
template <typename T>
class MODUS_ENGINE_EXPORT SparseStorageV2 final : public ComponentStorageV2 {
   public:
    static_assert(std::is_base_of_v<Component, T> && !std::is_same_v<T, Component>,
                  "T must be a sublcass of Component");
    using value_type = T;
    using index_type = u16;
    static constexpr index_type kIndexInvalid = std::numeric_limits<index_type>::max();
    static constexpr index_type kMaxCapacity = kIndexInvalid - 1;
    static constexpr index_type kInitialCapacity = 64;

   protected:
    gameplay::Vector<T> m_components;
    gameplay::Vector<index_type> m_free_list;
    // TODO: Can we be smarter about this
    gameplay::Vector<index_type> m_index_map;

   public:
    SparseStorageV2() : m_components(), m_free_list(), m_index_map() {
        m_components.reserve(kInitialCapacity);
        m_index_map.reserve(kInitialCapacity);
        m_free_list.reserve(kInitialCapacity);
    }

    ~SparseStorageV2() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(SparseStorageV2, SparseStorageV2<T>);

    Result<> grow_to(const usize size) override {
        m_index_map.resize(size, kIndexInvalid);
        return Ok<>();
    }

    NotMyPtr<Component> new_component(const EntityIndex index) override {
        modus_assert(index.value() < m_index_map.size() &&
                     m_components.size() + 1 < usize(kMaxCapacity));
        const index_type component_index = allocate_component();
        m_index_map[index.value()] = component_index;
        return &m_components[component_index];
    }

    void release_component(const EntityIndex entity) override {
        modus_assert(entity.value() < m_index_map.size() &&
                     m_index_map[entity.value()] != kIndexInvalid);
        const u32 component_index = m_index_map[entity.value()];
        m_index_map[entity.value()] = kIndexInvalid;
        m_free_list.push_back(component_index);
    }

    NotMyPtr<Component> get(const EntityIndex entity) override {
        modus_assert(entity.value() < m_index_map.size());
        const index_type component_index = m_index_map[entity.value()];
        modus_assert(component_index != kIndexInvalid && component_index < m_components.size());
        return &m_components[component_index];
        ;
    }

    void swap_components(const EntityIndex index1, const EntityIndex index2) override {
        modus_assert(index1.value() < m_index_map.size());
        modus_assert(index2.value() < m_index_map.size());
        index_type& cmp_index1 = m_index_map[index1.value()];
        index_type& cmp_index2 = m_index_map[index2.value()];

        if (cmp_index1 != kIndexInvalid && cmp_index2 != kIndexInvalid) {
            std::swap(m_components[cmp_index1], m_components[cmp_index2]);
        }

        // Swap component inderection if one of these is invalid as the other
        // one holds the correct index.
        if (cmp_index1 == kIndexInvalid || cmp_index2 == kIndexInvalid) {
            std::swap(cmp_index1, cmp_index2);
        }
    }

   private:
    index_type allocate_component() {
        index_type index = kIndexInvalid;
        if (!m_free_list.empty()) {
            auto it = m_free_list.begin() + (m_free_list.size() - 1);
            index = *it;
            m_free_list.erase(it);
            return index;
        } else {
            index = index_type(m_components.size());
            m_components.emplace_back();
            return index;
        }
    }
};

template <typename T>
using SparseStorage = SparseStorageV2<T>;

}    // namespace modus::engine::gameplay
