/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::gameplay {

#if !defined(MODUS_RTM) && !defined(MODUS_PROFILE)
#define MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS
#endif

class MODUS_ENGINE_EXPORT ComponentStorageV2 {
   private:
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
    std::atomic<u32> m_reader_counter;
    std::atomic<u32> m_writer_counter;
#endif
   public:
#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
    ComponentStorageV2() : m_reader_counter(0), m_writer_counter(0) {}
#else
    ComponentStorageV2() = default;
#endif
    virtual ~ComponentStorageV2() = default;

    MODUS_CLASS_DISABLE_COPY_MOVE(ComponentStorageV2);

    virtual Result<> grow_to(usize size) = 0;

    virtual NotMyPtr<Component> new_component(const EntityIndex index) = 0;

    virtual void release_component(const EntityIndex entity) = 0;

    virtual NotMyPtr<Component> get(const EntityIndex entity) = 0;

    virtual void swap_components(const EntityIndex index1, const EntityIndex index2) = 0;

#if defined(MODUS_GAMEPLAY_VALIDATE_COMPONENT_STORAGE_ACCESS)
    void increment_reader_counter() { m_reader_counter.fetch_add(1, std::memory_order_release); }
    void decrement_reader_counter() { m_reader_counter.fetch_sub(1, std::memory_order_release); }

    void increment_writer_counter() { m_writer_counter.fetch_add(1, std::memory_order_release); }
    void decrement_writer_counter() { m_writer_counter.fetch_sub(1, std::memory_order_release); }

    bool validate_reader_writer_access() const {
        const u32 reader_count = m_reader_counter.load(std::memory_order_acquire);
        const u32 writer_count = m_writer_counter.load(std::memory_order_acquire);
        return (reader_count == 0 && writer_count > 0) || (reader_count > 0 && writer_count == 0) ||
               (reader_count == 0 && writer_count == 0);
    }
#endif
};

}    // namespace modus::engine::gameplay
