/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/component_storage_v2.hpp>

namespace modus::engine::gameplay {

/// HashMap Storage, use for components which are used infrequently
template <typename T>
class MODUS_ENGINE_EXPORT HashMapStorageV2 final : public ComponentStorageV2 {
    static_assert(std::is_base_of_v<Component, T> && !std::is_same_v<T, Component>,
                  "T must be a sublcass of Component");

   public:
    using value_type = T;

   protected:
    gameplay::HashMap<EntityIndex, T> m_components;

   public:
    HashMapStorageV2() = default;
    ~HashMapStorageV2() = default;

    MODUS_CLASS_TEMPLATE_DISABLE_COPY_MOVE(HashMapStorageV2, HashMapStorageV2<T>);

    Result<> grow_to(const usize) override { return Ok<>(); }

    NotMyPtr<Component> new_component(const EntityIndex index) override {
        T& component = m_components[index];
        return NotMyPtr<Component>(&component);
    }

    void release_component(const EntityIndex entity) override {
        if (auto it = m_components.find(entity); it != m_components.end()) {
            m_components.erase(it);
        }
    }

    NotMyPtr<Component> get(const EntityIndex entity) override {
        if (auto it = m_components.find(entity); it == m_components.end()) {
            modus_panic("Component not found");
        } else {
            return NotMyPtr<Component>(&it->second);
        }
    }

    void swap_components(const EntityIndex index1, const EntityIndex index2) override {
        auto it1 = m_components.find(index1);
        auto it2 = m_components.find(index2);

        const bool exists1 = it1 != m_components.end();
        const bool exists2 = it2 != m_components.end();

        if (exists1 && exists2) {
            std::swap(it1->second, it2->second);
        } else if (exists1 && !exists2) {
            std::swap(it1->second, m_components[index2]);
            m_components.erase(it1);
        } else if (!exists1 && exists2) {
            std::swap(it2->second, m_components[index1]);
            m_components.erase(it2);
        }
    }
};

template <typename T>
using HashMapStorage = HashMapStorageV2<T>;

}    // namespace modus::engine::gameplay
