/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/assets/asset_types.hpp>
#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::gameplay {
class EntityTemplateRegistry;
class EntityTemplate;
struct EntityTemplateData;

class MODUS_ENGINE_EXPORT EntityTemplateAsset final : public assets::AssetBase {
    friend class EntityTemplateAssetFactory;

   private:
    NotMyPtr<const EntityTemplate> m_template;
    NotMyPtr<EntityTemplateData> m_template_data;
    Vector<assets::AssetHandleV2> m_dependencies;

   public:
    EntityTemplateAsset(NotMyPtr<const EntityTemplate> tmpl,
                        NotMyPtr<EntityTemplateData> data,
                        Vector<assets::AssetHandleV2>&& assets);

    Result<EntityId> spawn(Engine& engine,
                           EntityManager& entity_manager,
                           const StringSlice name = StringSlice(),
                           const u32 tag = 0) const;

    MODUS_CLASS_DISABLE_COPY_MOVE(EntityTemplateAsset);
};

class MODUS_ENGINE_EXPORT EntityTemplateAssetFactory final
    : public assets::PooledAssetFactory<EntityTemplateAsset, GameplayAllocator> {
   private:
    EntityTemplateRegistry& m_registry;

   public:
    EntityTemplateAssetFactory(EntityTemplateRegistry& registry);

    MODUS_CLASS_DISABLE_COPY_MOVE(EntityTemplateAssetFactory);

    StringSlice file_extension() const override;

    std::unique_ptr<assets::AssetData> create_asset_data() override;

    Result<NotMyPtr<assets::AssetBase>> create(Engine&, assets::AssetData& data) override;

    Result<> destroy(Engine&, NotMyPtr<assets::AssetBase> asset) override;
};

}    // namespace modus::engine::gameplay

MODUS_ENGINE_ASSET_TYPE_DECLARE(modus::engine::gameplay::EntityTemplateAsset)
