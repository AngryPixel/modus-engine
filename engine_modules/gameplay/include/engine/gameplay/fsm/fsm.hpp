/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <core/fixed_vector.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::gameplay {

class MODUS_ENGINE_EXPORT FSMState {
   public:
    virtual ~FSMState() = default;

    virtual void on_enter(Engine& engine) = 0;

    virtual Optional<u32> update(Engine& engine) = 0;

    virtual void on_exit(Engine& engine) = 0;
};

/// Finite State Machine
class MODUS_ENGINE_EXPORT FSM {
   private:
    static constexpr usize kMaxStateCount = 16;
    Array<NotMyPtr<FSMState>, kMaxStateCount> m_states;
    u32 m_initial_state = kMaxStateCount;
    u32 m_active_state = kMaxStateCount;

   public:
    static constexpr u32 invalid_state() { return kMaxStateCount; }
    virtual ~FSM() = default;

    virtual Result<> initialize(Engine& engine) = 0;

    virtual Result<> shutdown(Engine& engine) = 0;

    Result<> start(Engine& engine);

    void update(Engine& engine);

    void reset(Engine& engine);

    bool did_finish() const;

   protected:
    FSM() = default;

    void set_initial_state(const u32 index);

    void set_state(const u32 index, NotMyPtr<FSMState> state);
};

}    // namespace modus::engine::gameplay
