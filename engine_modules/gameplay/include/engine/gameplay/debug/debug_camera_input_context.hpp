/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/camera_controllers.hpp>
#include <engine/input/input_manager.hpp>
#include <engine/input/input_mapper.hpp>

namespace modus::engine {
class Engine;

class MODUS_ENGINE_EXPORT DebugCameraInputContext final : public engine::input::MappedInputContext {
   private:
    engine::gameplay::FreeRoamCameraController m_controller;
    f32 m_movement_speed;
    f32 m_boost_factor;

   public:
    DebugCameraInputContext(const f32 movement_speed = 50.0f);

    StringSlice name() const override;

    void set_position(const glm::vec3& position);

    void update_camera(Engine& engine, math::Transform& camera_transform);

   private:
    void on_forward(Engine& engine, const input::AxisMap& map);
    void on_lateral(Engine& engine, const input::AxisMap& map);
    void on_double_start(Engine& engine, const input::AxisMap&);
    void on_double_end(Engine& engine, const input::AxisMap&);
    void on_mouse_y(Engine& engine, const input::AxisMap&);
    void on_mouse_x(Engine& engine, const input::AxisMap&);
    void on_pad_y(Engine& engine, const input::AxisMap&);
    void on_pad_x(Engine& engine, const input::AxisMap&);
};

}    // namespace modus::engine
