/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/gameplay/module_includes.hpp>

#include <engine/gameplay/ecs/entity_template_asset.hpp>
#include <engine/gameplay/ecs/entity_template_registry.hpp>

#include <engine/modules/module.hpp>

namespace modus::engine::gameplay {
class GameWorld;
}

namespace modus::engine {

class MODUS_ENGINE_EXPORT ModuleGameplay final : public Module {
   private:
    gameplay::EntityTemplateRegistry m_entity_template_registry;
    gameplay::EntityTemplateAssetFactory m_entity_template_asset_factory;
    NotMyPtr<gameplay::GameWorld> m_world;
    IMilisec m_fixed_update_tick;
    IMilisec m_accumulator;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleGameplay();

    Result<> initialize(Engine&) override;

    Result<> shutdown(Engine&) override;

    void tick(Engine&) override;

    Slice<GID> module_dependencies() const override;

    gameplay::EntityTemplateRegistry& entity_template_registry() {
        return m_entity_template_registry;
    }

    const gameplay::EntityTemplateRegistry& entity_template_registry() const {
        return m_entity_template_registry;
    }

    bool has_world() const;

    void set_world(Engine& engine, NotMyPtr<gameplay::GameWorld> world);

    NotMyPtr<gameplay::GameWorld> world() { return m_world; }

    NotMyPtr<const gameplay::GameWorld> world() const { return m_world.get(); }

    inline IMilisec fixed_tick() const { return m_fixed_update_tick; }

    inline void set_fixed_tick(const IMilisec fixed_tick) { m_fixed_update_tick = fixed_tick; }
};

}    // namespace modus::engine
