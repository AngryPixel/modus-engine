/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/modules/module_gameplay_pch.h>

#include <engine/engine.hpp>
#include <engine/gameplay/ecs/component_storage/hashmap_storage.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/entity_template.hpp>
#include <engine/gameplay/ecs/entity_template_component_loader.hpp>
#include <engine/gameplay/ecs/entity_template_helper.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <math/random.hpp>

#include <test_prefab_generated.h>
#include <engine/assets/asset_loader.hpp>

using namespace modus;

struct TestComponent final : public engine::gameplay::Component {
    void on_create(const engine::gameplay::EntityId) override {}
};
MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(TestComponent)
MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(TestComponent)

namespace modus::engine::gameplay {
template <>
struct EntityTemplateFlatbufferHelperTraits<test::TestPrefab> {
    static bool verify(flatbuffers::Verifier& v) { return test::VerifyTestPrefabBuffer(v); }
    static const test::TestPrefab* get(const u8* bytes) { return test::GetTestPrefab(bytes); }
};
}    // namespace modus::engine::gameplay

using TestTemplate =
    engine::gameplay::EntityTemplateFlatbufferHelper<test::TestPrefab,
                                                     DefaultAllocator,
                                                     engine::gameplay::TransformComponent>;

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<TestComponent> {
    using StorageType = HashMapStorageV2<TestComponent>;
};
}    // namespace modus::engine::gameplay

class GameWorldECS : public modus::engine::gameplay::GameWorld {
   public:
    engine::gameplay::EntityManager m_entity_manager;
    Vector<engine::gameplay::EntityId> m_entities;
    math::random::RandomGenerator m_rand;

    Result<> initialize(engine::Engine& engine) override {
        m_rand.init(7);
        if (!m_entity_manager.register_component<TestComponent>()) {
            return Error<>();
        }
        if (!m_entity_manager.register_component<engine::gameplay::TransformComponent>()) {
            return Error<>();
        }
        m_entities.reserve(30);

#if defined(HAS_MK_ENTITY_TEMPLATE_TOOL)
        engine::assets::AsyncLoadParams load_params;
        load_params.path = "data:test_template.etpl";
        load_params.callback = [this](engine::Engine& eng,
                                      const engine::assets::AsyncCallbackResult& r) {
            if (r.asset) {
                NotMyPtr<const engine::gameplay::EntityTemplateAsset> asset =
                    engine::assets::unsafe_assetv2_cast<
                        const engine::gameplay::EntityTemplateAsset>(r.asset);
                if (auto r_enitity = asset->spawn(eng, m_entity_manager); !r_enitity) {
                    modus_panic("Failed to spawn entity");
                } else {
                    MODUS_LOGI("Entity Template Spawned");
                    m_entity_manager.destroy(*r_enitity);
                }
            } else {
                modus_panic("Failed to load template");
            }
        };
        if (!engine.load_startup_asset(load_params)) {
            return Error<>();
        }
#endif
        return m_entity_manager.initialize(engine, 100);
    }

    void on_enter(engine::Engine&) override {
        for (u32 i = 0; i < 10; ++i) {
            auto entity_create = m_entity_manager.create();
            if (!entity_create) {
                modus_panic("Failed to create entity");
            }
            m_entities.push_back(entity_create.value());
        }
    }

    void tick(engine::Engine& engine, const IMilisec) override {
        m_entity_manager.update(engine);
        random_spawn_kill();
    }

    void tick_fixed(engine::Engine&, const IMilisec) override {}

    void random_spawn_kill() {
        const usize entities_size = m_entities.size();
        const usize entiy_man_count = m_entity_manager.entity_count();
        MODUS_UNUSED(entities_size);
        MODUS_UNUSED(entiy_man_count);
        modus_assert(entiy_man_count == entities_size);
        Vector<engine::gameplay::EntityId> tmp;
        for (u32 i = 0; i < 20; ++i) {
            const u32 action = (m_rand.generate() % 2);
            if (action == 0 && !m_entities.empty()) {
                const auto last = m_entities.back();
                m_entity_manager.destroy(last);
                m_entities.erase(m_entities.begin() + (m_entities.size() - 1));
            } else if (m_entity_manager.entity_count() < 230) {
                auto entity_create = m_entity_manager.create();
                if (!entity_create) {
                    modus_panic("Failed to create entity");
                }
                auto transform_result =
                    m_entity_manager.add_component<TestComponent>(entity_create.value());
                if (!transform_result) {
                    modus_panic("Failed to add entity component");
                }
                tmp.push_back(entity_create.value());
            }
        }
        m_entities.insert(m_entities.end(), tmp.begin(), tmp.end());
    }
};

class TestAssetLoader final : public engine::IGame {
   private:
    IMilisec m_elapsed_ms = IMilisec(0);
    GameWorldECS m_world;
    engine::ModuleGameplay m_mod_gameplay;
    TestTemplate m_test_template;

   public:
    TestAssetLoader() : m_test_template("test.TestPrefab", 5) {}

    StringSlice name() const override { return "Engine ECS Test"; }
    StringSlice name_preferences() const override { return "modus.test.engine_ecs"; }

    Result<> add_cmd_options(modus::CmdOptionParser&) override { return Ok<>(); }

    Result<> register_modules(engine::ModuleRegistrator& registrator) override {
        if (!registrator.register_module<decltype(m_mod_gameplay)>(&m_mod_gameplay)) {
            return Error<>();
        }
        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) override {
        vfs::VirtualFileSystem& vfs = engine.module<engine::ModuleFileSystem>()->vfs();
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "data";
        vfs_params.path = MODUS_TEST_BINARY_DIR;

        if (!vfs.mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount data path");
            return Error<>();
        }
        MODUS_LOGI("Mounted data path: {}", MODUS_TEST_BINARY_DIR);
        auto gameplay_module = engine.module<engine::ModuleGameplay>();
        if (!gameplay_module->entity_template_registry().register_template(&m_test_template)) {
            MODUS_LOGE("Failed to register entity template");
            return Error<>();
        }
        if (!m_world.initialize(engine)) {
            return Error<>();
        }
        gameplay_module->set_world(engine, &m_world);
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto gameplay_module = engine.module<engine::ModuleGameplay>();
        gameplay_module->entity_template_registry().unregister_template(&m_test_template);
        return Ok<>();
    }

    void tick(engine::Engine& engine) override {
        m_elapsed_ms += engine.tick_ms();
        if (m_elapsed_ms > IMilisec(2000)) {
            engine.quit();
        }
    }
};

int main() {
    TestAssetLoader game;
    engine::Engine engine;
    if (!engine.initialize(game)) {
        fprintf(stderr, "Failed to initialize engine\n");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr, "Failed to shutdown engine\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
