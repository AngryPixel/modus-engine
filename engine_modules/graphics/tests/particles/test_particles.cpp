/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>

#include <graphics_test_game.hpp>

#include <engine/engine.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/ecs/components/particle_emitter_component.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/graphics/ecs/systems/particle_emitter_system.hpp>
#include <engine/graphics/particles/particle_generatorsv2.hpp>
#include <engine/graphics/particles/particle_renderersv2.hpp>
#include <engine/graphics/particles/particle_updatersv2.hpp>
#include <engine/graphics/particles/particlev2.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_graphics.hpp>
#include <math/billboard.hpp>
#include <math/random.hpp>
#include <threed/buffer.hpp>
#include <threed/drawable.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <threed/program_gen/shader_generator.hpp>
#include <threed/sampler.hpp>

using namespace modus;

class ParticleWorld final : public modus::engine::gameplay::GameWorld {
   public:
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::graphics::LightSystem m_light_system;
    engine::graphics::ParticleEmitterV2System m_particle_emitterv2_system;
    engine::gameplay::EntityId m_animated_entity;
    engine::gameplay::EntityId m_light_entity;
    engine::gameplay::EntityId m_camera_id;
    engine::gameplay::EntityId m_emitter_id;
    math::Transform m_camera_transform = math::Transform::kIdentity;
    engine::graphics::ForwardPipeline m_pipeline;

    engine::gameplay::EntityId main_camera() const override { return m_camera_id; }

    engine::graphics::Camera& camera() {
        return m_entity_manager.component<engine::graphics::CameraComponent>(m_camera_id)
            .value_or_panic("Failed to get camera")
            ->m_camera;
    }

    Result<> create_light() {
        /*
        if (auto r_entity = m_entity_manager.create("Light"); !r_entity) {
            MODUS_LOGE("Failed to create light entity");
            return Error<>();
        } else {
            m_light_entity = *r_entity;
        }

        if (auto r_l =
                m_entity_manager.add_component<engine::graphics::LightComponent>(m_light_entity);
            !r_l) {
            MODUS_LOGE("Failed to create light_component");
            return Error<>();
        } else {
            engine::graphics::Light& light = (*r_l)->light;
            light.set_type(engine::graphics::LightType::Point);
            light.set_position(glm::vec3(30.f, -30.f, 30.f));
            light.set_diffuse_intensity(0.8f);
            light.set_point_light_radius(300.0f);
            (*r_l)->enabled = true;
        }*/
        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) override {
        if (!m_entity_manager.register_component<engine::gameplay::TransformComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::GraphicsComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::LightComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::CameraComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::ParticleEmitterV2Component>()) {
            MODUS_LOGE("Failed to register particle emmiterv2 component");
            return Error<>();
        }

        if (!m_particle_emitterv2_system.initialize(engine, *this, m_entity_manager)) {
            MODUS_LOGE("Failed to initialize particle emmiterv2 system");
            return Error<>();
        }

        // Create camera
        {
            m_camera_id =
                m_entity_manager.create("Camera").value_or_panic("failed to create camera");
            auto r_cc =
                m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
            if (!r_cc) {
                return Error<>();
            }
        }

        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline");
            return Error<>();
        }

        if (!create_light()) {
            MODUS_LOGE("Failed to create directional light");
            return Error<>();
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->set_pipeline(&m_pipeline);

        // Create entities
        camera().frustum().set_aspect_ratio(1280.0f / 720.0f);

        {
            engine::graphics::ParticleSystemV2 particle_system;

            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::BoxPosParticleGenerator>();
                generator->m_max_start_offset = glm::vec3(1.f);
                particle_system.add_generator(generator);
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::BasicCircleVelParticleGenerator>();
                particle_system.add_generator(generator);
                generator->m_min_velocity = 0.05f;
                generator->m_max_velocity = 0.5f;
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::BasicTimeParticleGenerator>();
                particle_system.add_generator(generator);
                generator->m_min_time = 1.0f;
                generator->m_max_time = 5.0f;
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::ColorChangeParticleGenerator>();
                particle_system.add_generator(generator);
                generator->m_start_color = glm::vec4(1.0f, 1.0f, 0.f, 1.0f);
                generator->m_end_color = glm::vec4(1.0f, 0.0f, 0.f, 1.0f);
            }
            particle_system.set_emission_rate(3000.f);

            {
                auto updater =
                    engine::graphics::make_particle_updater<engine::graphics::BasicVelUpdater>();
                particle_system.add_updater(updater);
            }
            {
                auto updater =
                    engine::graphics::make_particle_updater<engine::graphics::BasicTimeUpdater>();
                particle_system.add_updater(updater);
            }
            {
                auto updater =
                    engine::graphics::make_particle_updater<engine::graphics::BasicColorUpdater>();
                particle_system.add_updater(updater);
            }

            const u32 particle_count = 100;
            const GID system_guid = os::guid::generate().value_or_panic();

            auto r_system = graphics_module->particle_registry().register_system(
                system_guid, std::move(particle_system));
            if (!r_system) {
                MODUS_LOGE("Failed to register particle system");
                return Error<>();
            }

            engine::graphics::ParticleSystemInstanceV2 instance;
            instance.particle_count = particle_count;
            instance.system = *r_system;
            instance.renderer_type = engine::graphics::ParticleRendererType::Point;
            instance.render_data = std::unique_ptr<engine::graphics::ParticleRenderData>();
            engine::graphics::ParticleEmitterV2 emitter;
            emitter.instances.push_back(std::move(instance));

            auto r_handle = graphics_module->particle_registry().create_emitter(std::move(emitter));
            if (!r_handle) {
                MODUS_LOGE("Failed to create particle instance");
                return Error<>();
            }
            if (auto r_id = m_entity_manager.create("Emitter"); r_id) {
                m_emitter_id = *r_id;
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(m_emitter_id)
                    .expect("Failed to create transform component");
                auto pec =
                    m_entity_manager
                        .add_component<engine::graphics::ParticleEmitterV2Component>(m_emitter_id)
                        .value_or_panic("Failed to create particle component");
                pec->m_emitter = r_handle->first;
            } else {
                MODUS_LOGE("Failed to create particle entity");
                return Error<>();
            }
        }
        {
            auto asset_module = engine.module<engine::ModuleAssets>();
            auto r_asset =
                asset_module->loader().resolve_by_path_typed<engine::graphics::ImageAsset>(
                    "data:/etc2/particle.image");
            if (!r_asset) {
                MODUS_LOGE("Failed to get image asset");
                return Error<>();
            }
            engine::graphics::ParticleSystemV2 particle_system;

            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::BoxPosParticleGenerator>();
                generator->m_max_start_offset = glm::vec3(1.f);
                particle_system.add_generator(generator);
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::BasicCircleVelParticleGenerator>();
                particle_system.add_generator(generator);
                generator->m_min_velocity = 0.05f;
                generator->m_max_velocity = 0.5f;
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::BasicTimeParticleGenerator>();
                particle_system.add_generator(generator);
                generator->m_min_time = 1.0f;
                generator->m_max_time = 10.0f;
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::NullUVParticleGenerator>();
                particle_system.add_generator(generator);
            }
            {
                auto generator = engine::graphics::make_particle_generator<
                    engine::graphics::ColorChangeParticleGenerator>();
                particle_system.add_generator(generator);
                generator->m_start_color = glm::vec4(0.0f, 1.0f, 0.f, 1.0f);
                generator->m_end_color = glm::vec4(0.0f, 0.0f, 1.f, 1.0f);
            }
            particle_system.set_emission_rate(100.0f);

            {
                auto updater =
                    engine::graphics::make_particle_updater<engine::graphics::BasicVelUpdater>();
                particle_system.add_updater(updater);
            }
            {
                auto updater =
                    engine::graphics::make_particle_updater<engine::graphics::BasicTimeUpdater>();
                particle_system.add_updater(updater);
            }
            {
                auto updater =
                    engine::graphics::make_particle_updater<engine::graphics::BasicColorUpdater>();
                particle_system.add_updater(updater);
            }
            {
                auto updater = engine::graphics::make_particle_updater<
                    engine::graphics::AnimatedUVDepthUpdater>();
                updater->m_max_depth = 1.0f;
                updater->m_update_interval = 0.5f;
                particle_system.add_updater(updater);
            }

            const u32 particle_count = 10;
            const GID system_guid = os::guid::generate().value_or_panic();

            auto r_system = graphics_module->particle_registry().register_system(
                system_guid, std::move(particle_system));
            if (!r_system) {
                MODUS_LOGE("Failed to register particle system");
                return Error<>();
            }

            std::unique_ptr<engine::graphics::BillboardParticleRendererData> render_data =
                engine::graphics::BillboardParticleRenderer::create_render_data();
            render_data->texture = (*r_asset)->m_texture;
            render_data->axis = glm::vec3(1.f, 0.f, 0.f);
            render_data->alignment =
                modus::engine::graphics::BillboardParticleRendererData::AlignmentType::FixedAxis;

            engine::graphics::ParticleSystemInstanceV2 instance;
            instance.particle_count = particle_count;
            instance.system = *r_system;
            instance.renderer_type = engine::graphics::ParticleRendererType::Billboard;
            instance.render_data = std::move(render_data);
            engine::graphics::ParticleEmitterV2 emitter;
            emitter.instances.push_back(std::move(instance));
            auto r_handle = graphics_module->particle_registry().create_emitter(std::move(emitter));
            if (!r_handle) {
                MODUS_LOGE("Failed to create particle instance");
                return Error<>();
            }
            if (auto r_id = m_entity_manager.create("Emitter2"); r_id) {
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(*r_id)
                    .value_or_panic("Failed to create transform component");
                //->set_translation(-3.0f, 0.f, -3.0f);
                auto pec = m_entity_manager
                               .add_component<engine::graphics::ParticleEmitterV2Component>(*r_id)
                               .value_or_panic("Failed to create particle component");
                pec->m_emitter = r_handle->first;
            } else {
                MODUS_LOGE("Failed to create particle entity");
                return Error<>();
            }
        }
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        m_entity_manager.clear(engine);
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->unset_pipeline();
        m_pipeline.shutdown(engine);
        m_particle_emitterv2_system.shutdown(engine, *this, m_entity_manager);

        return Ok<>();
    }

    void tick(engine::Engine& engine, const IMilisec tick) override {
        // Do entity add/removals from last frame
        m_entity_manager.update(engine);
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& debug_drawer = graphics_module->debug_drawer();

        if (auto r_t =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_emitter_id);
            r_t) {
            (*r_t)->set_translation(
                glm::vec3(0.f, 0.f, 5.0 * glm::sin(engine.elapsed_time_sec().count())));
        }
        NotMyPtr<engine::gameplay::System> systems[] = {
            &m_transform_system,
            &m_graphics_system,
            &m_light_system,
            &m_particle_emitterv2_system,
        };
        engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager,
                                                      systems);

        camera().set_look_at(
            glm::vec3(5.0f * glm::sin(f32(engine.elapsed_time_sec().count())), 4.0f,
                      5.0f * glm::cos(f32(engine.elapsed_time_sec().count()))),
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.0f, 0.f));

        // camera().set_look_at(glm::vec3(5.0f, 4.0f, 5.0f), glm::vec3(0.f, 0.f, 0.f),
        //                      glm::vec3(0.f, 1.0f, 0.f));
        debug_drawer.draw_fps(engine, 10, 10, 1.0f, glm::vec3(1.0f));
    }

    void tick_fixed(engine::Engine&, const IMilisec) override {
        // Artificially lower the framerate to avoid dealing with fast ticks that can't
        // update the elapsed seconds counter
        os::sleep_ms(IMilisec(12));
    }
};

class ParticleTest final : public GraphicsTestGame {
   private:
    ParticleWorld m_world;

   public:
    ParticleTest()
        : modus::GraphicsTestGame("Engine Particle Test", "modus.test.engine_particle") {}
    NotMyPtr<engine::gameplay::GameWorld> game_world() override { return &m_world; }

    Result<> initialize(engine::Engine& engine) override {
        vfs::VirtualFileSystem& vfs = engine.module<engine::ModuleFileSystem>()->vfs();
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "data";
        vfs_params.path = DATA_DIR;

        if (!vfs.mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount data path");
            return Error<>();
        }
        MODUS_LOGI("Mounted data path: {}", DATA_DIR);
        return GraphicsTestGame::initialize(engine);
    }

    Result<> shutdown(engine::Engine& engine) override {
        return GraphicsTestGame::shutdown(engine);
    }

    Result<> load_startup_assets(engine::Engine& engine) override {
        // Load particle test image
        engine::assets::AsyncLoadParams params;
        params.path = "data:/etc2/particle.image";
        params.callback = [](engine::Engine&, const engine::assets::AsyncCallbackResult& r) {
            if (!r.asset) {
                MODUS_LOGE("Failed to load particle image {}", r.path);
            }
        };
        if (!engine.load_startup_asset(params)) {
            return Error<>();
        }
        return Ok<>();
    }
};

GRAPHICS_TEST_MAIN(ParticleTest)
