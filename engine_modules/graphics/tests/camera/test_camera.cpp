/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/modules/module_gameplay_pch.h>
#include <engine/modules/module_graphics_pch.h>

#include <graphics_test_game.hpp>

#include <engine/engine.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/builtin_materials.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/graphics/image.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/mesh_generator.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/graphics/texture_util.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>
#include <threed/buffer.hpp>
#include <threed/compositor.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/program.hpp>

using namespace modus;

class ViewerWorld final : public modus::engine::gameplay::GameWorld {
   public:
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::graphics::LightSystem m_light_system;
    engine::gameplay::EntityId m_icosphere;
    engine::gameplay::EntityId m_cube;
    engine::gameplay::EntityId m_icosahedron;
    engine::gameplay::EntityId m_sphere;
    engine::gameplay::EntityId m_camera_id;
    threed::TextureHandle m_skybox_texture;
    math::Transform m_camera_transform = math::Transform::kIdentity;
    engine::assets::AssetHandle m_cubemap_asset;
    engine::assets::AssetHandle m_cube_asset;
    engine::assets::AssetHandle m_sphere_asset;
    engine::assets::AssetHandle m_icosahedron_asset;
    engine::assets::AssetHandle m_icosphere_asset;
    engine::graphics::ForwardPipeline m_pipeline;
    engine::graphics::MeshHandle m_cube_mesh_handle;
    engine::graphics::MeshHandle m_sphere_mesh_handle;
    engine::graphics::MeshHandle m_icosahedron_mesh_handle;
    engine::graphics::MeshHandle m_icosphere_mesh_handle;
    engine::graphics::BuiltinMaterials m_engine_materials;

    engine::gameplay::EntityId main_camera() const override { return m_camera_id; }

    engine::graphics::Camera& camera() {
        return m_entity_manager.component<engine::graphics::CameraComponent>(m_camera_id)
            .value_or_panic("Failed to get camera")
            ->m_camera;
    }
    Result<> create_directional_light() {
        engine::gameplay::EntityId light_entity;
        if (auto r_entity = m_entity_manager.create("Light"); !r_entity) {
            MODUS_LOGE("Failed to create light entity");
            return Error<>();
        } else {
            light_entity = *r_entity;
        }

        if (auto r_l =
                m_entity_manager.add_component<engine::graphics::LightComponent>(light_entity);
            !r_l) {
            MODUS_LOGE("Failed to create light_component");
            return Error<>();
        } else {
            engine::graphics::Light& light = (*r_l)->light;
            light.set_type(engine::graphics::LightType::Directional);
            light.set_position(glm::vec3(-5.f));
            light.set_diffuse_intensity(1.f);
            light.set_diffuse_color(glm::vec3(1.f));
            (*r_l)->enabled = true;
        }
        return Ok<>();
    }

    Result<> load_meshes(engine::Engine& engine) {
        MODUS_UNUSED(engine);
        {
            engine::assets::AsyncLoadParams params;
            params.path = "mesh:engine/basic_shapes/cube.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_cube_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load cube mesh");
                return Error<>();
            }
            m_cube_asset = *r_asset;
        }
        {
            engine::assets::AsyncLoadParams params;
            params.path = "mesh:engine/basic_shapes/sphere.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_sphere_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load sphere mesh");
                return Error<>();
            }
            m_sphere_asset = *r_asset;
        }

        {
            engine::assets::AsyncLoadParams params;
            params.path = "mesh:engine/basic_shapes/icosahedron.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_icosahedron_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load icosahedron mesh");
                return Error<>();
            }
            m_icosahedron_asset = *r_asset;
        }
        {
            engine::assets::AsyncLoadParams params;
            params.path = "data:icosphere.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_icosphere_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load icosphere mesh");
                return Error<>();
            }
            m_icosphere_asset = *r_asset;
        }
        return Ok<>();
    }

    Result<> load_cubemap(engine::Engine& engine) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();
        const auto device_limits = device.device_limits();

        StringSlice image_path;
        if (device_limits.compressed_texture_support.astc) {
            image_path = "data:astc/cubemap.image";
        } else if (device_limits.compressed_texture_support.etc2) {
            image_path = "data:etc2/cubemap.image";
        } else {
            MODUS_LOGE("Not supported texture format");
            return Error<>();
        }

        engine::assets::AsyncLoadParams params;
        params.path = image_path;
        params.callback = [this](engine::Engine&, const engine::assets::AsyncCallbackResult& r) {
            if (r.asset) {
                auto cubemap =
                    engine::assets::unsafe_assetv2_cast<const engine::graphics::ImageAsset>(
                        r.asset);
                m_skybox_texture = cubemap->m_texture;
            } else {
                MODUS_LOGE("Failed to load image: {}", r.path);
            }
        };

        auto r_asset = engine.load_startup_asset(params);
        if (!r_asset) {
            MODUS_LOGE("Failed to load cubemap");
            return Error<>();
        }
        m_cubemap_asset = *r_asset;
        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) override {
        if (!m_entity_manager.register_component<engine::gameplay::TransformComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::GraphicsComponent>()) {
            MODUS_LOGE("Failed to register graphics component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::CameraComponent>()) {
            MODUS_LOGE("Failed to register Camera component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::LightComponent>()) {
            MODUS_LOGE("Failed to register light component");
            return Error<>();
        }

        // Create camera
        {
            m_camera_id =
                m_entity_manager.create("Camera").value_or_panic("failed to create camera");
            auto r_cc =
                m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
            if (!r_cc) {
                return Error<>();
            }
        }
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->set_pipeline(&m_pipeline);

        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline");
            return Error<>();
        }

        if (!m_engine_materials.initialize(engine, graphics_module->material_db())) {
            return Error<>();
        }

        // Create entities

        auto entity_create_fn = [this, &graphics_module](
                                    const StringSlice& name,
                                    const glm::vec3& color) -> engine::gameplay::EntityId {
            auto r_entity_create = m_entity_manager.create(name);
            if (!r_entity_create) {
                modus_panic("Failed to create entity");
            }

            auto r_graphics_component =
                m_entity_manager.add_component<engine::graphics::GraphicsComponent>(
                    r_entity_create.value());
            if (!r_graphics_component) {
                modus_panic("Failed to create graphics component");
            }

            const modus::GID guid = modus::GID::from_string("61f0aec4-92f3-492e-b517-7f9e7e76ae6a")
                                        .value_or_panic("Invalid GUID");
            if (auto r_mat_instance = graphics_module->material_db().create_instance(guid);
                !r_mat_instance) {
                modus_panic("Failed to create material asset");
            } else {
                (*r_graphics_component)->material_instance = *r_mat_instance;
                auto r_instance_ptr = graphics_module->material_db().instance(*r_mat_instance);
                modus_assert(r_instance_ptr);
                if (!(*r_instance_ptr)->set_property("diffuse", glm::vec4(color, 1.f))) {
                    MODUS_LOGE("Failed to set diffuse property");
                }
            }

            auto r_transform_component =
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(
                    r_entity_create.value());
            if (!r_transform_component) {
                modus_panic("Failed to create transform component");
            }

            return r_entity_create.value();
        };

        m_sphere = entity_create_fn("Sphere", glm::vec3(0.f, 1.f, 0.f));
        m_cube = entity_create_fn("Cube", glm::vec3(1.0f, 1.f, 1.f));
        m_icosahedron = entity_create_fn("Icosahedron", glm::vec3(0.f, 0.f, 1.0f));
        m_icosphere = entity_create_fn("Icosphere", glm::vec3(1.f, 0.f, 0.f));

        NotMyPtr<engine::graphics::GraphicsComponent> icosphere_g =
            m_entity_manager.component<engine::graphics::GraphicsComponent>(m_icosphere)
                .value_or_panic();
        NotMyPtr<engine::graphics::GraphicsComponent> cube_g =
            m_entity_manager.component<engine::graphics::GraphicsComponent>(m_cube)
                .value_or_panic();
        NotMyPtr<engine::graphics::GraphicsComponent> icosahedron_g =
            m_entity_manager.component<engine::graphics::GraphicsComponent>(m_icosahedron)
                .value_or_panic();
        NotMyPtr<engine::graphics::GraphicsComponent> sphere_g =
            m_entity_manager.component<engine::graphics::GraphicsComponent>(m_sphere)
                .value_or_panic();

        const engine::graphics::MeshDB& mesh_db = graphics_module->mesh_db();

        // auto r_mesh_instance =
        auto r_mesh_instance = mesh_db.new_instance(m_sphere_mesh_handle);
        if (!r_mesh_instance) {
            return Error<>();
        }
        sphere_g->mesh_instance = r_mesh_instance.value();

        r_mesh_instance = mesh_db.new_instance(m_cube_mesh_handle);
        if (!r_mesh_instance) {
            return Error<>();
        }
        cube_g->mesh_instance = r_mesh_instance.value();

        r_mesh_instance = mesh_db.new_instance(m_icosahedron_mesh_handle);
        if (!r_mesh_instance) {
            return Error<>();
        }
        icosahedron_g->mesh_instance = r_mesh_instance.value();

        r_mesh_instance = mesh_db.new_instance(m_icosphere_mesh_handle);
        if (!r_mesh_instance) {
            return Error<>();
        }
        icosphere_g->mesh_instance = r_mesh_instance.value();

        camera().set_look_at(glm::vec3(0, 7, -10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
        camera().frustum().set_aspect_ratio(1280.0f / 720.0f);

        // Attach cube
        {
            auto r_attachment_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_cube);
            if (r_attachment_component) {
                engine::gameplay::TransformComponent& component = *(r_attachment_component.value());
                if (!component.attach(m_entity_manager, m_icosahedron)) {
                    modus_panic("Failed to attach cube to icosahedron");
                    return Error<>();
                }
            }
        }
        // Attach icosphere
        {
            auto r_attachment_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_icosphere);
            if (r_attachment_component) {
                engine::gameplay::TransformComponent& component = *(r_attachment_component.value());
                if (!component.attach(m_entity_manager, m_icosahedron)) {
                    modus_panic("Failed to attach icosphere to icosahedron");
                    return Error<>();
                }
            }
        }
        // Attach sphere
        {
            auto r_attachment_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_sphere);
            if (r_attachment_component) {
                engine::gameplay::TransformComponent& component = *(r_attachment_component.value());
                component.translate(2.0f, -3.0f, 0.0);
                if (!component.attach(m_entity_manager, m_cube)) {
                    modus_panic("Failed to attach icosphere to icosahedron");
                    return Error<>();
                }
            }
        }

        {
            auto r_transform_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_cube);
            if (r_transform_component) {
                r_transform_component.value()->set_translation(-3.0f, 3.0f, 0.0f);
            }
        }
        {
            auto r_transform_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_icosphere);
            if (r_transform_component) {
                r_transform_component.value()->set_translation(-3.0f, -3.0f, 0.0f);
            }
        }

        // Create Cube Map
        {
            auto r_entity_create = m_entity_manager.create("cubemap");
            if (!r_entity_create) {
                modus_panic("Failed to create entity");
            }

            if (auto r = m_entity_manager.add_component<engine::gameplay::TransformComponent>(
                    *r_entity_create);
                !r) {
                return Error<>();
            }

            auto r_graphics_component =
                m_entity_manager.add_component<engine::graphics::GraphicsComponent>(
                    r_entity_create.value());
            if (!r_graphics_component) {
                modus_panic("Failed to create graphics component");
            }
            (*r_graphics_component)->mesh_instance.mesh = m_cube_mesh_handle;
            (*r_graphics_component)->tag = engine::graphics::GraphicsComponentTag::SkyBox;

            const modus::GID guid = modus::GID::from_string("58aaf618-0919-4a62-a8c8-8e06c59ef335")
                                        .value_or_panic("Invalid GUID");
            if (auto r_mat_instance = graphics_module->material_db().create_instance(guid);
                !r_mat_instance) {
                modus_panic("Failed to create material asset");
            } else {
                (*r_graphics_component)->material_instance = *r_mat_instance;
                auto r_instance_ptr = graphics_module->material_db().instance(*r_mat_instance);
                modus_assert(r_instance_ptr);
                if (!(*r_instance_ptr)->set_property("texture", m_skybox_texture)) {
                    MODUS_LOGE("Failed to set texture property");
                }
            }
        }
        if (!create_directional_light()) {
            return Error<>();
        }
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->unset_pipeline();

        if (!m_engine_materials.shutdown(engine, graphics_module->material_db())) {
            return Error<>();
        }

        m_pipeline.shutdown(engine);
        auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();

        asset_loader.destroy_async(m_cubemap_asset);
        asset_loader.destroy_async(m_cube_asset);
        asset_loader.destroy_async(m_sphere_asset);
        asset_loader.destroy_async(m_icosahedron_asset);
        asset_loader.destroy_async(m_icosphere_asset);
        return Ok<>();
    }

    void tick(engine::Engine& engine, const IMilisec tick) override {
        // Do entity add/removals from last frame
        m_entity_manager.update(engine);

        /*{
            auto r_transform_component =
                m_entity_manager
                    .component<engine::gameplay::TransformComponent>(
                        m_icosahedron);
            if (r_transform_component) {
                r_transform_component.value()->rotate(
                    glm::angleAxis(glm::radians(-60.0f * engine.tick_sec()),
                                   glm::vec3(0.0f, 1.0f, .0f)));
            }
        }*/

        {
            auto r_transform_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_cube);
            if (r_transform_component) {
                r_transform_component.value()->rotate(glm::angleAxis(
                    glm::radians(60.0f * engine.tick_sec().count()), glm::vec3(0.0f, 0.0f, 1.0f)));
                r_transform_component.value()->set_translation(
                    glm::vec3(4.0f - sinf(engine.elapsed_time_sec().count()), 3.0f, 0.0f));
            }
        }
        {
            auto r_transform_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_sphere);
            if (r_transform_component) {
                r_transform_component.value()->rotate(glm::angleAxis(
                    glm::radians(180.0f * engine.tick_sec().count()), glm::vec3(0.0f, 1.0f, 0.0f)));
                r_transform_component.value()->set_translation(
                    r_transform_component.value()->local_transform().rotation() *
                    glm::vec3(0.0f, 0.0f, -3.0f));
            }
        }

        {
            auto r_transform_component =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_icosphere);
            if (r_transform_component) {
                r_transform_component.value()->set_scale(sinf(engine.elapsed_time_sec().count()));
            }
        }

        {
            m_camera_transform.rotate(glm::angleAxis(
                glm::radians(25.0f * engine.tick_sec().count()), glm::vec3(0.0f, 1.0f, 0.0f)));
            const glm::vec3 camera_pos = m_camera_transform.transform(
                glm::vec3(0, 3.0f + f32(std::sin(engine.elapsed_time_sec().count())), -10));
            camera().set_look_at(camera_pos, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
        }

        NotMyPtr<engine::gameplay::System> systems[] = {
            &m_transform_system,
            &m_graphics_system,
            &m_light_system,
        };
        engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager,
                                                      systems);

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& debug_drawer = graphics_module->debug_drawer();

        debug_drawer.draw_line(glm::vec3(0.f), glm::vec3(10.f, 0.f, 0.f),
                               glm::vec4(1.0f, 0.f, 0.f, 1.0f));

        debug_drawer.draw_line(glm::vec3(0.f), glm::vec3(0.f, 10.f, 0.f),
                               glm::vec4(0.0f, 1.f, 0.f, 1.0f));

        debug_drawer.draw_line(glm::vec3(0.f), glm::vec3(0.f, 0.f, 10.f),
                               glm::vec4(0.0f, 0.f, 1.f, 1.0f));

        debug_drawer.draw_fps(engine, 10, 10, 1.0f, glm::vec3(1.0f));
    }

    void tick_fixed(engine::Engine&, const IMilisec) override {}
};

class CameraTest final : public GraphicsTestGame {
   private:
    ViewerWorld m_world;

   public:
    CameraTest() : modus::GraphicsTestGame("Engine Camera Test", "modus.test.engine_camera") {}
    NotMyPtr<engine::gameplay::GameWorld> game_world() override { return &m_world; }

    Result<> initialize(engine::Engine& engine) override {
        vfs::VirtualFileSystem& vfs = engine.module<engine::ModuleFileSystem>()->vfs();
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "data";
        vfs_params.path = DATA_DIR;

        if (!vfs.mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount data path");
            return Error<>();
        }
        MODUS_LOGI("Mounted data path: {}", DATA_DIR);
        return GraphicsTestGame::initialize(engine);
    }

    Result<> load_startup_assets(engine::Engine& engine) override {
        if (!m_world.load_cubemap(engine)) {
            MODUS_LOGE("Failed to load cubemap");
            return Error<>();
        }
        if (!m_world.load_meshes(engine)) {
            MODUS_LOGE("Failed to load meshes");
            return Error<>();
        }
        return Ok<>();
    }
};

GRAPHICS_TEST_MAIN(CameraTest)
