/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/modules/module_gameplay_pch.h>
#include <engine/modules/module_graphics_pch.h>

#include <engine/engine.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>

namespace modus {

class LoadingWorld;

class GraphicsTestGame : public engine::IGame {
   private:
    String m_game_name;
    String m_game_name_pref;
    FSeconds m_elapsed_sec = FSeconds(0.0f);
    CmdOptionF32 m_opt_timeout;
    engine::ModuleGraphics m_mod_graphics;
    engine::ModulePhysics m_mod_physics;
    engine::ModuleGameplay m_mod_gameplay;
    engine::ModuleAnimation m_mod_animation;
    std::unique_ptr<LoadingWorld> m_loading_world;

   public:
    GraphicsTestGame(StringSlice name, StringSlice name_pref);

    virtual ~GraphicsTestGame();

    StringSlice name() const override final;
    StringSlice name_preferences() const override final;

    Result<> add_cmd_options(modus::CmdOptionParser& opt_parser) override;

    Result<> register_modules(engine::ModuleRegistrator& registrator) override final;

    Result<> initialize(engine::Engine& engine) override;

    Result<> shutdown(engine::Engine& engine) override;

    void tick(engine::Engine& engine) override final;

   protected:
    virtual NotMyPtr<engine::gameplay::GameWorld> game_world() = 0;

    virtual Result<> load_startup_assets(engine::Engine& engine) = 0;
};
}    // namespace modus

#define GRAPHICS_TEST_MAIN(GAME)                                       \
    int main(const int argc, const char** argv) {                      \
        GAME game;                                                     \
        engine::Engine engine;                                         \
        auto r_parse = engine.parse_command_options(argc, argv, game); \
        if (!r_parse) {                                                \
            return EXIT_FAILURE;                                       \
        } else if (r_parse.value()) {                                  \
            return EXIT_SUCCESS;                                       \
        }                                                              \
        if (!engine.initialize(game)) {                                \
            fprintf(stderr, "Failed to initialize engine\n");          \
            (void)engine.shutdown();                                   \
            return EXIT_FAILURE;                                       \
        }                                                              \
        engine.run();                                                  \
        if (!engine.shutdown()) {                                      \
            fprintf(stderr, "Failed to shutdown engine\n");            \
            return EXIT_FAILURE;                                       \
        }                                                              \
        return EXIT_SUCCESS;                                           \
    }
