/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <graphics_test_game.hpp>
#include <engine/gameplay/gameworld.hpp>
// clang-format on

namespace modus {

class LoadingWorld final : public engine::gameplay::GameWorld {
    NotMyPtr<engine::gameplay::GameWorld> m_world;

   public:
    LoadingWorld(NotMyPtr<engine::gameplay::GameWorld> world) : m_world(world) {}

    Result<> initialize(engine::Engine&) override { return Ok<>(); }

    Result<> shutdown(engine::Engine&) override { return Ok<>(); }

    void on_enter(engine::Engine&) override {}

    void on_exit(engine::Engine& engine) override {
        if (!m_world->initialize(engine)) {
            MODUS_LOGE("Failed to initialize game world");
            engine.quit();
        }
    }

    void tick(engine::Engine& engine, const IMilisec) override {
        auto r_state = engine.startup_asset_state();
        if (!r_state) {
            MODUS_LOGE("Failed to load one of the required startup assets");
            engine.quit();
            return;
        }
        if (r_state->load_count == r_state->total_count) {
            auto gameplay_module = engine.module<engine::ModuleGameplay>();
            gameplay_module->set_world(engine, m_world);
        }
    }
    void tick_fixed(engine::Engine&, const IMilisec) override {}
};

GraphicsTestGame::GraphicsTestGame(StringSlice name, StringSlice name_pref)
    : m_game_name(name.to_str()),
      m_game_name_pref(name_pref.to_str()),
      m_opt_timeout("", "--timeout", "Time in seconds to run this test for.", 5.f) {}

GraphicsTestGame::~GraphicsTestGame() {}

StringSlice GraphicsTestGame::name() const {
    return m_game_name;
}
StringSlice GraphicsTestGame::name_preferences() const {
    return m_game_name_pref;
}

Result<> GraphicsTestGame::add_cmd_options(modus::CmdOptionParser& opt_parser) {
    return opt_parser.add(m_opt_timeout);
}

Result<> GraphicsTestGame::register_modules(engine::ModuleRegistrator& registrator) {
    if (!registrator.register_module<decltype(m_mod_gameplay)>(&m_mod_gameplay)) {
        return Error<>();
    }
    if (!registrator.register_module<decltype(m_mod_graphics)>(&m_mod_graphics)) {
        return Error<>();
    }
    if (!registrator.register_module<decltype(m_mod_physics)>(&m_mod_physics)) {
        return Error<>();
    }
    if (!registrator.register_module<decltype(m_mod_animation)>(&m_mod_animation)) {
        return Error<>();
    }
    return Ok<>();
}

Result<> GraphicsTestGame::initialize(engine::Engine& engine) {
    m_loading_world = modus::make_unique<LoadingWorld>(game_world());
    if (!load_startup_assets(engine)) {
        MODUS_LOGE("Failed to load startup assets");
        return Error<>();
    }
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    gameplay_module->set_world(engine, m_loading_world.get());
    return Ok<>();
}

Result<> GraphicsTestGame::shutdown(engine::Engine& engine) {
    auto world = game_world();
    auto gameplay_module = engine.module<engine::ModuleGameplay>();
    gameplay_module->set_world(engine, NotMyPtr<engine::gameplay::GameWorld>());
    m_loading_world.reset();
    return world->shutdown(engine);
}

void GraphicsTestGame::tick(engine::Engine& engine) {
    m_elapsed_sec += engine.tick_sec();
    if (m_elapsed_sec > FSeconds(m_opt_timeout.value())) {
        engine.quit();
    }
}
}    // namespace modus
