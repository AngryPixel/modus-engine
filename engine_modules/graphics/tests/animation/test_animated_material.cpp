/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <engine/engine_pch.h>
// clang-format on
#include "test_animated_material.hpp"
#include <engine/engine.hpp>
#include <threed/device.hpp>
#include <threed/program.hpp>
#include <threed/effect.hpp>
#include <threed/pipeline.hpp>
#include <threed/buffer.hpp>
#include <threed/program_gen/shader_generator.hpp>

#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/graphics/default_program_inputs.hpp>

TestAnimatedMaterialInstance::TestAnimatedMaterialInstance(const engine::graphics::MaterialHandle h,
                                                           const u32 flags)
    : engine::graphics::MaterialInstance(h, flags),
      m_prop_diffuse(kPropDiffuseName, glm::vec4(0.6f, 0.6f, 0.6f, 1.0f)),
      m_prop_specular(kPropSpecularName, 0.f) {}

void TestAnimatedMaterialInstance::apply(engine::Engine& engine,
                                         engine::graphics::MaterialApplyParams& params) const {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(params);

    MODUS_UNUSED(engine);
    modus_assert(params.base_material.material_handle() == material_handle());
    const AnimatedMaterial& material_type =
        static_cast<const AnimatedMaterial&>(params.base_material);
    modus_assert(material_type.effect_handle());

    const glm::mat4 mvp =
        params.frame_globals.constant_block().mat_project_view * params.renderable_transform;
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    auto& animation_cache = graphics_module->animation_buffer_cache();
    if (params.animator) {
        if (!m_cbuffer) {
            if (auto r = animation_cache.create_buffer(device); r) {
                m_cbuffer = *r;
            }
        }
        auto animation_module = engine.module<engine::ModuleAnimation>();
        auto r_data = animation_module->manager().animation_data(params.animator);
        if (r_data) {
            auto r_buffer = animation_cache.update_buffer(device, m_cbuffer, *r_data);
            if (r_buffer) {
                material_type.m_anim_cbuffer_binder = m_cbuffer;
            }
        }
    } else {
        material_type.m_anim_cbuffer_binder = animation_cache.identity_buffer();
    }
    material_type.m_mvp_binder = mvp;
    material_type.m_mv_binder = params.renderable_transform;
    material_type.m_diffuse_binder = m_prop_diffuse.value();
    material_type.m_specular_binder = m_prop_specular.value();
    params.command.program = material_type.forward_program_handle();
    params.command.effect = material_type.effect_handle();
    material_type.m_binders.bind(params.command, device);
}

Result<NotMyPtr<engine::graphics::MaterialProperty>, void>
TestAnimatedMaterialInstance::find_property(const StringSlice name) {
    if (name == kPropDiffuseName) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_diffuse);
    } else if (name == kPropSpecularName) {
        return Ok<NotMyPtr<engine::graphics::MaterialProperty>>(&m_prop_specular);
    }
    return Error<>();
}
Result<NotMyPtr<const engine::graphics::MaterialProperty>, void>
TestAnimatedMaterialInstance::find_property(const StringSlice name) const {
    if (name == kPropDiffuseName) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_diffuse);
    } else if (name == kPropSpecularName) {
        return Ok<NotMyPtr<const engine::graphics::MaterialProperty>>(&m_prop_specular);
    }
    return Error<>();
}

glm::vec3 AnimatedMaterial::sLightDir = glm::vec3(-1.f);

AnimatedMaterial::AnimatedMaterial()
    : engine::graphics::MaterialHelper<TestAnimatedMaterialInstance>(kMaterialGUID,
                                                                     "ForwardTest",
                                                                     32) {}

Result<> AnimatedMaterial::initialize(engine::Engine& engine) {
    MODUS_UNUSED(engine);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    auto& shader_snippet_db = graphics_module->shader_snippet_db();
    const auto& shader_generator = device.shader_generator();

    threed::dynamic::ProgramGenerator generator;
    threed::dynamic::ProgramGenParams params;

    threed::dynamic::TypedDefaultConstantInput<glm::mat4> mvp_input("u_mvp", &m_mvp_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::mat4> mv_input("u_mv", &m_mv_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::vec4> diffuse_input("u_diffuse",
                                                                        &m_diffuse_binder);
    threed::dynamic::TypedDefaultConstantInput<f32> specular_input("u_specular",
                                                                   &m_specular_binder);
    engine::graphics::AnimationCBufferInput anim_cbuffer_input(&m_anim_cbuffer_binder);

    if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                 "engine.frame_globals")) {
        MODUS_LOGE("Default Material: Failed to locate frame_globals shader snippet");
        return Error<>();
    }
    if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                 "engine.tiled_light")) {
        MODUS_LOGE("Default Material: Failed to locate tiled_light shader snippet");
        return Error<>();
    }
    params.varying
        .push_back({"v_position", threed::DataType::Vec3F32,
                    threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Medium})
        .expect();
    params.varying
        .push_back({"v_normal", threed::DataType::Vec3F32,
                    threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Medium})
        .expect();
    params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_vertex_input())
        .expect();
    params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_normal_input())
        .expect();
    params.vertex_stage.inputs
        .push_back(engine::graphics::shader_desc::default_bone_indices_input())
        .expect();
    params.vertex_stage.inputs
        .push_back(engine::graphics::shader_desc::default_bone_weights_input())
        .expect();
    params.vertex_stage.constants.push_back(&mvp_input).expect();
    params.vertex_stage.constants.push_back(&mv_input).expect();
    params.vertex_stage.constant_buffers.push_back(&anim_cbuffer_input).expect();
    params.vertex_stage.main = R"R(
void main() {
    mat4 bone_transform = get_bone_transform();
    vec4 position = bone_transform * VERTEX;
    vec3 normal = (bone_transform * vec4(NORMAL,0)).xyz;
    v_normal = normalize(normal);
    v_position = vec3(u_mv * position);
    gl_Position = u_mvp * position;
})R";
    params.fragment_stage.fragment_outputs
        .push_back(engine::graphics::shader_desc::default_frag_output())
        .expect();
    params.fragment_stage.constants.push_back(&diffuse_input).expect();
    params.fragment_stage.constants.push_back(&specular_input).expect();
    params.fragment_stage.main = R"R(
void main() {
    vec3 normal = normalize(v_normal);
    vec4 color = u_diffuse * calculate_lights_tiled(v_position, normal, u_specular, vec3(0.0), 0.0);
    OUT_COLOR = color;
}
)R";
    params.skip_instanced_variant = true;

    if (auto r = generator.generate(params, shader_generator); !r) {
        MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
        return Error<>();
    }

    auto result = generator.result(0).value_or_panic();
    m_binders = result->binders;

    if (auto r = device.create_program(result->create_params); !r) {
        MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
        return Error<>();
    } else {
        m_prog_forward_handle = *r;
    }

    // create effect
    {
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = true;
        effect_params.state.raster.cull_enabled = true;
        effect_params.state.raster.cull_mode = threed::CullMode::Back;
        effect_params.state.raster.face_counter_clockwise = true;

        auto r_effect = device.create_effect(effect_params);
        if (!r_effect) {
            MODUS_LOGE("Failed to create test material effect: {}", r_effect.error());
            return Error<>();
        }
        m_effect_handle = *r_effect;
    }
    return Ok<>();
}

Result<> AnimatedMaterial::shutdown(engine::Engine& engine) {
    MODUS_UNUSED(engine);
    return Ok<>();
}
