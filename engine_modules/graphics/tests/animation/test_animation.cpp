/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/modules/module_gameplay_pch.h>
#include <engine/modules/module_graphics_pch.h>

#include <graphics_test_game.hpp>

#include <engine/animation/skeleton_animator.hpp>
#include <engine/engine.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/debug_drawers/animation_debug_drawer.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/graphics/image.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/mesh_generator.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/graphics/texture_util.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>
#include <threed/buffer.hpp>
#include <threed/compositor.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/program.hpp>

#include "test_animated_material.hpp"

using namespace modus;

void on_animation_finished(engine::Engine&) {
    MODUS_LOGI("Animation Finished");
}

class AnimatedWorld final : public modus::engine::gameplay::GameWorld {
   public:
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::graphics::LightSystem m_light_system;
    engine::gameplay::EntityId m_animated_entity;
    engine::gameplay::EntityId m_light_entity;
    engine::gameplay::EntityId m_camera_id;
    math::Transform m_camera_transform = math::Transform::kIdentity;
    engine::assets::AssetHandle m_aninmated_asset;
    engine::graphics::ForwardPipeline m_pipeline;
    engine::graphics::MeshHandle m_animated_mesh;
    engine::animation::AnimationCatalogHandle m_animation_catalog;
    engine::animation::AnimatorHandle m_animator;
    AnimatedMaterial m_material;

    engine::gameplay::EntityId main_camera() const override { return m_camera_id; }

    engine::graphics::Camera& camera() {
        return m_entity_manager.component<engine::graphics::CameraComponent>(m_camera_id)
            .value_or_panic("Failed to get camera")
            ->m_camera;
    }

    Result<> load_meshes(engine::Engine& engine) {
        MODUS_UNUSED(engine);
        engine::assets::AsyncLoadParams params;
        params.path = "data:running_char.mesh";
        params.callback = [this](engine::Engine&, const engine::assets::AsyncCallbackResult& r) {
            if (r.asset) {
                auto mesh =
                    engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(r.asset);
                m_animated_mesh = mesh->m_mesh_handle;
                m_animation_catalog = mesh->m_animation_catalog;
            } else {
                MODUS_LOGE("Failed to load mesh: {}", r.path);
            }
        };
        auto r_asset = engine.load_startup_asset(params);
        if (!r_asset) {
            MODUS_LOGE("Failed to load animated mesh");
            return Error<>();
        }
        m_aninmated_asset = *r_asset;
        return Ok<>();
    }

    Result<> create_light() {
        if (auto r_entity = m_entity_manager.create("Light"); !r_entity) {
            MODUS_LOGE("Failed to create light entity");
            return Error<>();
        } else {
            m_light_entity = *r_entity;
        }

        if (auto r_l =
                m_entity_manager.add_component<engine::graphics::LightComponent>(m_light_entity);
            !r_l) {
            MODUS_LOGE("Failed to create light_component");
            return Error<>();
        } else {
            engine::graphics::Light& light = (*r_l)->light;
            light.set_type(engine::graphics::LightType::Point);
            light.set_position(glm::vec3(30.f, -30.f, 30.f));
            light.set_diffuse_intensity(0.8f);
            light.set_point_light_radius(300.0f);
            (*r_l)->enabled = true;
        }
        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) override {
        if (!m_entity_manager.register_component<engine::gameplay::TransformComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::GraphicsComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::LightComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::CameraComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        // Create camera
        {
            m_camera_id =
                m_entity_manager.create("Camera").value_or_panic("failed to create camera");
            auto r_cc =
                m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
            if (!r_cc) {
                return Error<>();
            }
        }

        if (!load_meshes(engine)) {
            MODUS_LOGE("Failed to load meshes");
            return Error<>();
        }

        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline");
            return Error<>();
        }

        if (!m_material.initialize(engine)) {
            MODUS_LOGE("Failed to init material");
            return Error<>();
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        if (!graphics_module->material_db().add(&m_material)) {
            MODUS_LOGE("Failed to add material ");
            return Error<>();
        }
        if (!create_light()) {
            MODUS_LOGE("Failed to create directional light");
            return Error<>();
        }

        graphics_module->set_pipeline(&m_pipeline);

        // Create entities

        auto entity_create_fn = [this, &engine, &graphics_module](
                                    const StringSlice& name, const StringSlice material_guid,
                                    const engine::graphics::MeshHandle mesh, const glm::vec3& color,
                                    const f32 specular_factor,
                                    const glm::vec3& pos =
                                        glm::vec3(0.f)) -> engine::gameplay::EntityId {
            auto r_entity_create = m_entity_manager.create(name);
            if (!r_entity_create) {
                modus_panic("Failed to create entity");
            }

            auto r_graphics_component =
                m_entity_manager.add_component<engine::graphics::GraphicsComponent>(
                    r_entity_create.value());
            if (!r_graphics_component) {
                modus_panic("Failed to create graphics component");
            }
            auto r_mesh_instance = graphics_module->mesh_db().new_instance(mesh);
            if (!r_mesh_instance) {
                modus_panic("Failed to create mesh instance");
            }
            (*r_graphics_component)->mesh_instance = *r_mesh_instance;

            const modus::GID guid =
                modus::GID::from_string(material_guid).value_or_panic("Invalid GUID");
            if (auto r_mat_instance = graphics_module->material_db().create_instance(guid);
                !r_mat_instance) {
                modus_panic("Failed to create material asset");
            } else {
                (*r_graphics_component)->material_instance = *r_mat_instance;
                auto r_instance_ptr = graphics_module->material_db().instance(*r_mat_instance);
                modus_assert(r_instance_ptr);
                if (!(*r_instance_ptr)->set_property("diffuse", glm::vec4(color, 1.f))) {
                    MODUS_LOGE("Failed to set diffuse property");
                }

                if (!(*r_instance_ptr)->set_property("specular", specular_factor)) {
                    MODUS_LOGE("Failed to set specular property");
                }
            }

            auto r_transform_component =
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(
                    r_entity_create.value());
            if (!r_transform_component) {
                modus_panic("Failed to create transform component");
            }
            (*r_transform_component)->set_translation(pos);
            /* (*r_transform_component)
                 ->m_local.set_rotation(glm::angleAxis(
                     glm::radians(-90.f), glm::vec3(1.0f, 0.f, 0.f)));*/
            auto animation_module = engine.module<engine::ModuleAnimation>();

            auto r_animator = animation_module->manager().create(m_animation_catalog);
            if (!r_animator) {
                modus_panic("Failed to create animator");
            }
            m_animator = *r_animator;

            const engine::animation::PlayAnimationParams anim_params = {
                "Armature", 1.f, true,
                engine::animation::AnimationFinishedDelegate::build<on_animation_finished>()};

            if (!animation_module->manager().play(m_animator, anim_params)) {
                modus_panic("Failed to setup animator");
            }

            (*r_graphics_component)->animator = m_animator;

            return r_entity_create.value();
        };

        m_animated_entity = entity_create_fn("Model", kMaterialGUID, m_animated_mesh,
                                             glm::vec3(0.6f, 0.6f, 0.6f), 0.f);

        camera().frustum().set_aspect_ratio(1280.0f / 720.0f);

        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->unset_pipeline();

        graphics_module->material_db().erase(&m_material);

        (void)m_material.shutdown(engine);
        m_pipeline.shutdown(engine);

        auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();
        asset_loader.destroy_async(m_aninmated_asset);
        return Ok<>();
    }

    void tick(engine::Engine& engine, const IMilisec tick) override {
        // Do entity add/removals from last frame
        m_entity_manager.update(engine);
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& debug_drawer = graphics_module->debug_drawer();

        if (auto r_t =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_light_entity);
            r_t) {
            debug_drawer.draw_cube((*r_t)->world_transform().translation(), 0.10f, glm::vec4(1),
                                   false);
        }

        auto transform_component =
            m_entity_manager.add_component<engine::gameplay::TransformComponent>(m_animated_entity)
                .value_or_panic("Failed to get transform component");
        transform_component->rotate(glm::angleAxis(glm::radians(30.0f * engine.tick_sec().count()),
                                                   glm::vec3(0.f, 0.f, 1.0f)));
        NotMyPtr<engine::gameplay::System> systems[] = {
            &m_transform_system,
            &m_graphics_system,
            &m_light_system,
        };
        engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager,
                                                      systems);

        camera().set_look_at(glm::vec3(8.0f, -8.0f, 5.0f), glm::vec3(0.f, 0.f, 4.f),
                             glm::vec3(0.f, 0.0f, 1.f));
        debug_drawer.draw_fps(engine, 10, 10, 1.0f, glm::vec3(1.0f));

        auto animation_module = engine.module<engine::ModuleAnimation>();

        if (auto r_data = animation_module->manager().animation_data(m_animator); r_data) {
            auto r_animation_catalog =
                animation_module->animation_catalog_db().get(m_animation_catalog);
            if (r_animation_catalog) {
                auto r_sk = (*r_animation_catalog)->skeleton(0);
                if (r_sk) {
                    engine::graphics::AnimationDebugDrawer dd(engine);
                    dd.draw_skeleton(transform_component->world_transform().to_matrix(), **r_sk,
                                     *r_data);
                }
            }
        }
    }

    void tick_fixed(engine::Engine&, const IMilisec) override {}
};

class TestAnimation final : public engine::IGame {
   private:
    FSeconds m_elapsed_sec = FSeconds(0.f);
    AnimatedWorld m_world;
    CmdOptionF32 m_opt_timeout;
    AnimatedMaterial m_material;
    engine::ModuleGraphics m_mod_graphics;
    engine::ModulePhysics m_mod_physics;
    engine::ModuleGameplay m_mod_gameplay;
    engine::ModuleAnimation m_mod_animation;

   public:
    TestAnimation()
        : m_opt_timeout("", "--timeout", "Time in seconds to run this test for.", 5.f) {}

    StringSlice name() const override { return "Engine Lighting Test"; }
    StringSlice name_preferences() const override { return "modus.test.engine_lighting"; }

    Result<> add_cmd_options(modus::CmdOptionParser& opt_parser) override {
        return opt_parser.add(m_opt_timeout);
    }

    Result<> register_modules(engine::ModuleRegistrator& registrator) override {
        if (!registrator.register_module<decltype(m_mod_gameplay)>(&m_mod_gameplay)) {
            return Error<>();
        }
        if (!registrator.register_module<decltype(m_mod_graphics)>(&m_mod_graphics)) {
            return Error<>();
        }
        if (!registrator.register_module<decltype(m_mod_physics)>(&m_mod_physics)) {
            return Error<>();
        }
        if (!registrator.register_module<decltype(m_mod_animation)>(&m_mod_animation)) {
            return Error<>();
        }
        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) override {
        vfs::VirtualFileSystem& vfs = engine.module<engine::ModuleFileSystem>()->vfs();
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "data";
        vfs_params.path = DATA_DIR;

        if (!vfs.mount_path(vfs_params)) {
            MODUS_LOGW("Failed to mount data path:{}", DATA_DIR);
        } else {
            MODUS_LOGI("Mounted data path: {}", DATA_DIR);
        }

        if (!m_material.initialize(engine)) {
            MODUS_LOGE("Failed to init material");
            return Error<>();
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        if (!graphics_module->material_db().add(&m_material)) {
            MODUS_LOGE("Failed to add material ");
            return Error<>();
        }

        if (!m_world.initialize(engine)) {
            return Error<>();
        }
        auto gameplay_module = engine.module<engine::ModuleGameplay>();
        gameplay_module->set_world(engine, &m_world);
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        (void)m_world.shutdown(engine);

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->material_db().erase(&m_material);

        (void)m_material.shutdown(engine);

        return Ok<>();
    }

    void tick(engine::Engine& engine) override {
        m_elapsed_sec += engine.tick_sec();
        if (m_elapsed_sec > FSeconds(m_opt_timeout.value())) {
            engine.quit();
        }
    }
};

class AnimationTest final : public GraphicsTestGame {
   private:
    AnimatedWorld m_world;

   public:
    AnimationTest()
        : modus::GraphicsTestGame("Engine Animation Test", "modus.test.engine_animation") {}
    NotMyPtr<engine::gameplay::GameWorld> game_world() override { return &m_world; }

    Result<> initialize(engine::Engine& engine) override {
        vfs::VirtualFileSystem& vfs = engine.module<engine::ModuleFileSystem>()->vfs();
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "data";
        vfs_params.path = DATA_DIR;

        if (!vfs.mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount data path");
            return Error<>();
        }
        MODUS_LOGI("Mounted data path: {}", DATA_DIR);

        return GraphicsTestGame::initialize(engine);
    }

    Result<> shutdown(engine::Engine& engine) override {
        return GraphicsTestGame::shutdown(engine);
    }

    Result<> load_startup_assets(engine::Engine& engine) override {
        if (!m_world.load_meshes(engine)) {
            MODUS_LOGE("Failed to load meshes");
            return Error<>();
        }
        return Ok<>();
    }
};

GRAPHICS_TEST_MAIN(AnimationTest)
