/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/modules/module_gameplay_pch.h>
#include <engine/modules/module_graphics_pch.h>

#include <graphics_test_game.hpp>

#include <engine/engine.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/systems/default_system_runner.hpp>
#include <engine/gameplay/ecs/systems/transform_system.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/graphics/image.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/mesh_generator.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/graphics/texture_util.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>
#include <threed/buffer.hpp>
#include <threed/compositor.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/program.hpp>

#include "test_material.hpp"

using namespace modus;

static constexpr usize kNumPointLights = 4;
class TestWorld final : public modus::engine::gameplay::GameWorld {
   public:
    engine::graphics::GraphicsSystem m_graphics_system;
    engine::gameplay::TransformSystem m_transform_system;
    engine::graphics::LightSystem m_light_system;
    engine::gameplay::EntityId m_cube;
    engine::gameplay::EntityId m_icosahedron;
    engine::gameplay::EntityId m_sphere;
    engine::gameplay::EntityId m_light_entity;
    FixedVector<engine::gameplay::EntityId, kNumPointLights> m_point_lights;
    engine::gameplay::EntityId m_camera_id;
    math::Transform m_camera_transform = math::Transform::kIdentity;
    engine::assets::AssetHandle m_cube_asset;
    engine::assets::AssetHandle m_sphere_asset;
    engine::assets::AssetHandle m_icosahedron_asset;
    engine::graphics::ForwardPipeline m_pipeline;
    engine::graphics::MeshHandle m_cube_mesh_handle;

    engine::graphics::MeshHandle m_sphere_mesh_handle;
    engine::graphics::MeshHandle m_icosahedron_mesh_handle;
    ForwardMaterial m_material;

    engine::gameplay::EntityId main_camera() const override { return m_camera_id; }

    engine::graphics::Camera& camera() {
        return m_entity_manager.component<engine::graphics::CameraComponent>(m_camera_id)
            .value_or_panic("Failed to get camera")
            ->m_camera;
    }

    Result<> load_meshes(engine::Engine& engine) {
        MODUS_UNUSED(engine);
        {
            engine::assets::AsyncLoadParams params;
            params.path = "mesh:engine/basic_shapes/cube.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_cube_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load cube mesh");
                return Error<>();
            }
            m_cube_asset = *r_asset;
        }
        {
            engine::assets::AsyncLoadParams params;
            params.path = "mesh:engine/basic_shapes/sphere.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_sphere_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load sphere mesh");
                return Error<>();
            }
            m_sphere_asset = *r_asset;
        }

        {
            engine::assets::AsyncLoadParams params;
            params.path = "mesh:engine/basic_shapes/icosahedron.mesh";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& r) {
                if (r.asset) {
                    auto mesh =
                        engine::assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(
                            r.asset);
                    m_icosahedron_mesh_handle = mesh->m_mesh_handle;
                } else {
                    MODUS_LOGE("Failed to load mesh: {}", r.path);
                }
            };
            auto r_asset = engine.load_startup_asset(params);
            if (!r_asset) {
                MODUS_LOGE("Failed to load icosahedron mesh");
                return Error<>();
            }
            m_icosahedron_asset = *r_asset;
        }
        return Ok<>();
    }

    Result<> create_directional_light() {
        if (auto r_entity = m_entity_manager.create("Light"); !r_entity) {
            MODUS_LOGE("Failed to create light entity");
            return Error<>();
        } else {
            m_light_entity = *r_entity;
        }

        if (auto r_tc = m_entity_manager.add_component<engine::gameplay::TransformComponent>(
                m_light_entity);
            !r_tc) {
            MODUS_LOGE("Failed to create transform component");
            return Error<>();
        }

        if (auto r_l =
                m_entity_manager.add_component<engine::graphics::LightComponent>(m_light_entity);
            !r_l) {
            MODUS_LOGE("Failed to create light_component");
            return Error<>();
        } else {
            engine::graphics::Light& light = (*r_l)->light;
            light.set_type(engine::graphics::LightType::Directional);
            light.set_position(glm::vec3(5.f));
            light.set_diffuse_intensity(0.1f);
            (*r_l)->enabled = false;
        }
        return Ok<>();
    }

    Result<engine::gameplay::EntityId, void> create_point_light(
        const engine::graphics::Light& light) {
        engine::gameplay::EntityId entity;
        if (auto r_entity = m_entity_manager.create("Light"); !r_entity) {
            MODUS_LOGE("Failed to create light entity");
            return Error<>();
        } else {
            entity = *r_entity;
        }

        if (auto r_tc =
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(entity);
            !r_tc) {
            MODUS_LOGE("Failed to create transform component");
            return Error<>();
        }

        if (auto r_l = m_entity_manager.add_component<engine::graphics::LightComponent>(entity);
            !r_l) {
            MODUS_LOGE("Failed to create light_component");
            return Error<>();
        } else {
            engine::graphics::Light& this_light = (*r_l)->light;
            this_light = light;
        }
        return Ok(entity);
    }

    Result<> initialize(engine::Engine& engine) override {
        if (!m_entity_manager.register_component<engine::gameplay::TransformComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::GraphicsComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::LightComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        if (!m_entity_manager.register_component<engine::graphics::CameraComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        // Create camera
        {
            m_camera_id =
                m_entity_manager.create("Camera").value_or_panic("failed to create camera");
            auto r_cc =
                m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
            if (!r_cc) {
                return Error<>();
            }
        }

        if (!load_meshes(engine)) {
            MODUS_LOGE("Failed to load meshes");
            return Error<>();
        }

        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline");
            return Error<>();
        }

        if (!m_material.initialize(engine)) {
            MODUS_LOGE("Failed to init material");
            return Error<>();
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        if (!graphics_module->material_db().add(&m_material)) {
            MODUS_LOGE("Failed to add material ");
            return Error<>();
        }

        if (!create_directional_light()) {
            MODUS_LOGE("Failed to create directional light");
            return Error<>();
        }

        for (usize i = 0; i < kNumPointLights; ++i) {
            engine::graphics::Light light;
            light.set_type(engine::graphics::LightType::Point);
            light.set_diffuse_color(glm::vec3(1.0f, 1.0f, 1.0f));
            light.set_diffuse_intensity(1.0f);
            // light.set_attenuation(1.0f, 1.2f, 1.f);
            light.set_point_light_radius(3.0f);

            if (i == 0) {
                // light.set_diffuse_intensity(0.f);
            }

            if (i == 1) {
                light.set_position(glm::vec3(4.f, 4.f, -4.f));
                light.set_point_light_radius(5.5f);
            }
            if (i == 2) {
                light.set_position(glm::vec3(2.f, 0., -2.f));
            }

            if (i == 3) {
                light.set_position(glm::vec3(-1.f));
            }

            if (auto r_light = create_point_light(light); !r_light) {
                MODUS_LOGE("Failed to create light");
                return Error<>();
            } else {
                m_point_lights.push_back(*r_light).expect("Failed to add point light");
            }
        }
        {
            /*
            engine::graphics::Light light;
            light.set_type(engine::graphics::LightType::Point);
            light.set_diffuse_color(glm::vec3(1.0f, 1.0f, 1.0f));
            light.set_diffuse_intensity(1.0f);
            light.set_point_light_radius(5.0f);
            // light.set_attenuation(1.0f, 1.2f, 1.f);
            if (auto r_light = create_point_light(light); !r_light) {
                MODUS_LOGE("Failed to create light");
                return Error<>();
            }*/
        }

        graphics_module->set_pipeline(&m_pipeline);

        // Create entities

        auto entity_create_fn = [this, &graphics_module](
                                    const StringSlice& name, const StringSlice material_guid,
                                    const engine::graphics::MeshHandle mesh, const glm::vec3& color,
                                    const f32 specular_factor,
                                    const glm::vec3& pos =
                                        glm::vec3(0.f)) -> engine::gameplay::EntityId {
            auto r_entity_create = m_entity_manager.create(name);
            if (!r_entity_create) {
                modus_panic("Failed to create entity");
            }

            auto r_graphics_component =
                m_entity_manager.add_component<engine::graphics::GraphicsComponent>(
                    r_entity_create.value());
            if (!r_graphics_component) {
                modus_panic("Failed to create graphics component");
            }
            auto r_mesh_instance = graphics_module->mesh_db().new_instance(mesh);
            if (!r_mesh_instance) {
                modus_panic("Failed to create mesh instance");
            }
            (*r_graphics_component)->mesh_instance = *r_mesh_instance;

            const modus::GID guid =
                modus::GID::from_string(material_guid).value_or_panic("Invalid GUID");
            if (auto r_mat_instance = graphics_module->material_db().create_instance(guid);
                !r_mat_instance) {
                modus_panic("Failed to create material asset");
            } else {
                (*r_graphics_component)->material_instance = *r_mat_instance;
                auto r_instance_ptr = graphics_module->material_db().instance(*r_mat_instance);
                modus_assert(r_instance_ptr);
                if (!(*r_instance_ptr)->set_property("diffuse", glm::vec4(color, 1.f))) {
                    MODUS_LOGE("Failed to set diffuse property");
                }

                if (!(*r_instance_ptr)->set_property("specular", specular_factor)) {
                    MODUS_LOGE("Failed to set specular property");
                }
            }

            auto r_transform_component =
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(
                    r_entity_create.value());
            if (!r_transform_component) {
                modus_panic("Failed to create transform component");
            }
            (*r_transform_component)->set_translation(pos);

            return r_entity_create.value();
        };

        m_sphere = entity_create_fn("Sphere", kMaterialGUID, m_sphere_mesh_handle,
                                    glm::vec3(0.f, 1.f, 0.f), 1.0, glm::vec3(3.f, 3.f, 0.f));
        m_cube = entity_create_fn("Cube", kMaterialGUID, m_cube_mesh_handle,
                                  glm::vec3(1.0f, 1.f, 1.f), 0.5f, glm::vec3(-3.0f, 3.f, 0.f));
        m_icosahedron = entity_create_fn("Icosahedron", kMaterialGUID, m_icosahedron_mesh_handle,
                                         glm::vec3(0.f, 0.f, 1.0f), 0.f, glm::vec3(0.f, 3.f, 0.f));

        entity_create_fn("Sphere", kMaterialGUID, m_sphere_mesh_handle, glm::vec3(1.0f, 0.f, 0.f),
                         1.0f, glm::vec3(3.f, 0.f, 0.f));
        entity_create_fn("Cube", kMaterialGUID, m_cube_mesh_handle, glm::vec3(1.0f, 1.f, 0.f), 0.5f,
                         glm::vec3(-3.0f, 0.f, 0.f));
        entity_create_fn("Icosahedron", kMaterialGUID, m_icosahedron_mesh_handle,
                         glm::vec3(0.f, 1.0f, 1.f), 0.f);

        camera().frustum().set_aspect_ratio(1280.0f / 720.0f);
        if (auto r_ambient_light = m_entity_manager.create("AmbientLight"); !r_ambient_light) {
            return Error<>();
        } else {
            if (auto r_lc = m_entity_manager.add_component<engine::graphics::LightComponent>(
                    *r_ambient_light);
                r_lc) {
                auto& light = (*r_lc)->light;
                light.set_type(engine::graphics::LightType::Ambient);
                light.set_diffuse_color(glm::vec3(1.f));
                light.set_diffuse_intensity(0.0f);
                (*r_lc)->enabled = true;
            }
        }
        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->unset_pipeline();

        graphics_module->material_db().erase(&m_material);

        (void)m_material.shutdown(engine);
        m_pipeline.shutdown(engine);

        auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();

        asset_loader.destroy_async(m_cube_asset);
        asset_loader.destroy_async(m_sphere_asset);
        asset_loader.destroy_async(m_icosahedron_asset);
        return Ok<>();
    }

    void tick(engine::Engine& engine, const IMilisec tick) override {
        // Do entity add/removals from last frame
        m_entity_manager.update(engine);
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& debug_drawer = graphics_module->debug_drawer();

        if (auto r_t =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_light_entity);
            r_t) {
            const glm::vec3 position =
                glm::vec3(0.f, 10.0f * f32(glm::cos(engine.elapsed_time_sec().count())),
                          10 * f32(glm::sin(engine.elapsed_time_sec().count())));
            (*r_t)->set_translation(position);

            debug_drawer.draw_cube(position, 0.10f, glm::vec4(1), false);
        }

        if (auto r_t =
                m_entity_manager.component<engine::gameplay::TransformComponent>(m_point_lights[0]);
            r_t) {
            const glm::vec3 position =
                glm::vec3(2.0f * f32(glm::cos(engine.elapsed_time_sec().count())), 1.5f,
                          2.0f * f32(glm::sin(engine.elapsed_time_sec().count())));
            (*r_t)->set_translation(position);
        }

        NotMyPtr<engine::gameplay::System> systems[] = {&m_transform_system, &m_graphics_system,
                                                        &m_light_system};
        engine::gameplay::InlineSystemRunner().update(engine, tick, *this, m_entity_manager,
                                                      systems);

        for (const auto& id : m_point_lights) {
            if (auto r_lc = m_entity_manager.component<engine::graphics::LightComponent>(id);
                r_lc) {
                debug_drawer.draw_cube((*r_lc)->light.position(), 0.10f, glm::vec4(glm::vec3(1), 1),
                                       false);
            }
        }

        debug_drawer.draw_line(glm::vec3(0.f), glm::vec3(10.f, 0.f, 0.f),
                               glm::vec4(1.0f, 0.f, 0.f, 1.0f));

        debug_drawer.draw_line(glm::vec3(0.f), glm::vec3(0.f, 10.f, 0.f),
                               glm::vec4(0.0f, 1.f, 0.f, 1.0f));

        debug_drawer.draw_line(glm::vec3(0.f), glm::vec3(0.f, 0.f, 10.f),
                               glm::vec4(0.0f, 0.f, 1.f, 1.0f));

        /*
        m_camera.transform.set_look_at(
            glm::vec3(7.5f * f32(glm::sin(engine.elapsed_time_sec())), 1.5f,
                      7.5f * f32(glm::cos(engine.elapsed_time_sec()))),
            glm::vec3(0.f), glm::vec3(0.f, 1.0f, 0.f));
*/
        camera().set_look_at(glm::vec3(0, 1.5, -15.0f), glm::vec3(0.f), glm::vec3(0.f, 1.0f, 0.f));
        debug_drawer.draw_fps(engine, 10, 10, 1.0f, glm::vec3(1.0f));
    }

    void tick_fixed(engine::Engine&, const IMilisec) override {}
};

class LightingTest final : public GraphicsTestGame {
   private:
    TestWorld m_world;

   public:
    LightingTest()
        : modus::GraphicsTestGame("Engine Lighting Test", "modus.test.engine_lighting") {}
    NotMyPtr<engine::gameplay::GameWorld> game_world() override { return &m_world; }

    Result<> initialize(engine::Engine& engine) override {
        return GraphicsTestGame::initialize(engine);
    }

    Result<> shutdown(engine::Engine& engine) override {
        return GraphicsTestGame::shutdown(engine);
    }

    Result<> load_startup_assets(engine::Engine& engine) override {
        if (!m_world.load_meshes(engine)) {
            MODUS_LOGE("Failed to load meshes");
            return Error<>();
        }
        return Ok<>();
    }
};

GRAPHICS_TEST_MAIN(LightingTest)
