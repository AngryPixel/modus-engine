/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/modules/module_graphics_pch.h>

#include <graphics_test_game.hpp>

#include <engine/assets/asset_waiter.hpp>
#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <engine/graphics/font.hpp>
#include <engine/graphics/font_asset.hpp>
#include <engine/graphics/pipeline/null_pipeline.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_physics.hpp>
#include <math/random.hpp>
#include <threed/buffer.hpp>
#include <threed/compositor.hpp>
#include <threed/device.hpp>
#include <threed/drawable.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/pipeline.hpp>
#include <threed/program.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <threed/program_gen/program_gen.hpp>
#include <threed/program_gen/shader_generator.hpp>

using namespace modus;

constexpr const char* kDrawFontVertexSrc =
    R"R(
void main(){
    gl_Position = u_mvp * vec4(VERTEX.xy, 0.0, 1.0);
    v_uv = UV;
})R";

constexpr const char* kDrawFontFragSrc =
    R"R(
// With bigger sizes,increase width, decrease edge
// Any distance within this value will have a solid color
const float width = 0.8;

// Any distance outside ch_width + edge_transition is transparent
// char who fall in between transition between 0 and 1
const float edge = 0.08;

const float border_width = 0.8;
const float border_edge = 0.4;
const vec3 border_color = vec3(1.0, 0.0, 0.0);

void main() {
    // In the atlas the distance decreases the further away we get from the distance
    float distance = texture(s_font_atlas, v_uv).r;
    float alpha = smoothstep(width - edge, width, distance);
    // Border calculation
    // float distance = texture(font_atlas, var_tex_coord).r;
    float border_alpha = smoothstep(border_width - border_edge, border_width, distance);
    float final_alpha = alpha + (1.0 - alpha) * border_alpha;
    vec3 final_color = mix(border_color, u_color.xyz, alpha / final_alpha);
    OUT_COLOR = vec4(final_color, final_alpha);
})R";

class SDFPipeline final : public engine::graphics::IPipeline {
   private:
    engine::assets::AssetGroupWaiter m_asset_waiter;
    engine::assets::AssetHandleV2 m_font_asset;
    engine::assets::AssetHandle m_font_sdf_gpuprog_asset;
    NotMyPtr<const engine::graphics::Font> m_font;
    threed::TextureHandle m_font_atlas;
    threed::BufferHandle m_text_buffer;
    threed::ProgramHandle m_prog;
    threed::EffectHandle m_effect;
    threed::DrawableHandle m_drawable;

    threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_proj_binder;
    threed::dynamic::TypedDefaultConstantInputBinder<glm::vec4> m_color_binder;
    threed::dynamic::DefaultSamplerInputBinder m_sampler_binder;
    threed::dynamic::RuntimeBinders m_binders;

   public:
    MODUS_ENGINE_DECLARE_PIPELINE_ID(5);

    SDFPipeline() : engine::graphics::IPipeline(kPipelineId) {}

    Result<> preload(engine::Engine& engine) {
        {
            engine::assets::AsyncLoadParams params;
            params.path = "font:hack_sdf.font";
            params.callback = [this](engine::Engine&,
                                     const engine::assets::AsyncCallbackResult& result) {
                if (result.asset) {
                    auto font = engine::assets::assetv2_cast<const engine::graphics::FontAsset>(
                        result.asset);
                    if (font) {
                        m_font = &(*font)->m_font;
                        m_font_atlas = (*font)->m_texture;
                        return;
                    }
                }
                MODUS_LOGE("Failed to load font");
            };

            if (auto r = engine.load_startup_asset(params); !r) {
                MODUS_LOGE("Failed to load font");
                return Error<>();
            } else {
                m_font_asset = (*r);
            }
        }

        auto graphics = engine.module<engine::ModuleGraphics>();
        auto& device = graphics->device();
        const auto& shader_generator = device.shader_generator();
        threed::dynamic::TypedDefaultConstantInput<glm::mat4> proj_desc("u_mvp", &m_proj_binder);
        threed::dynamic::TypedDefaultConstantInput<glm::vec4> color_desc("u_color",
                                                                         &m_color_binder);
        threed::dynamic::DefaultSamplerInput sampler_desc("s_font_atlas", &m_sampler_binder);
        threed::dynamic::ProgramGenerator generator;
        {
            threed::dynamic::ProgramGenParams params;
            params.varying
                .push_back({"v_uv", threed::DataType::Vec2F32,
                            threed::dynamic::VaryingDesc::Type::Smooth,
                            threed::dynamic::PrecisionQualifier::Medium})
                .expect();
            params.vertex_stage.constants.push_back(&proj_desc).expect();
            params.vertex_stage.inputs
                .push_back(engine::graphics::shader_desc::default_vertex_input())
                .expect();
            params.vertex_stage.inputs.push_back(engine::graphics::shader_desc::default_uv_input())
                .expect();
            params.vertex_stage.main = kDrawFontVertexSrc;
            params.fragment_stage.constants.push_back(&color_desc).expect();
            params.fragment_stage.fragment_outputs
                .push_back(engine::graphics::shader_desc::default_frag_output())
                .expect();
            params.fragment_stage.samplers.push_back(&sampler_desc).expect();
            params.fragment_stage.main = kDrawFontFragSrc;
            params.skip_instanced_variant = true;

            if (auto r = generator.generate(params, shader_generator); !r) {
                MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
                return Error<>();
            }

            auto result = generator.result(0).value_or_panic();
            m_binders = result->binders;

            if (auto r = graphics->device().create_program(result->create_params); !r) {
                MODUS_LOGE("Failed to create debug draw font program: {}", r.error());
                return Error<>();
            } else {
                m_prog = *r;
            }
            generator.reset();
        }

        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();

        // Generate text buffers
        engine::graphics::FontAsciiGenParams font_params;
        font_params.text = "Hello World! 1@$!@%5^7&*(";
        font_params.scale = 1.0;
        font_params.x_loc = 10;
        font_params.y_loc = 10;
        font_params.padding = 5.0f;

        Vector<f32> data_vec;
        data_vec.resize(engine::graphics::Font::buffer_size_for_text_size(font_params.text.size()));
        auto vec_slice = make_slice_mut(data_vec);
        auto r_tex_gen = m_font->generate_ascii_string_v2(vec_slice, font_params);
        if (!r_tex_gen) {
            MODUS_LOGE("Failed to generate text buffer");
            return Error<void>();
        }

        threed::BufferCreateParams buffer_params;
        buffer_params.size = data_vec.size() * sizeof(f32);
        buffer_params.data = data_vec.data();
        buffer_params.usage = threed::BufferUsage::Static;
        buffer_params.type = threed::BufferType::Data;

        auto r_buffer = device.create_buffer(buffer_params);
        if (!r_buffer) {
            MODUS_LOGE("Failed to create text buffer");
            return Error<>();
        }

        // Create Drawable
        threed::DrawableCreateParams drawable_params;
        drawable_params.data_buffers[0].buffer = *r_buffer;
        drawable_params.data_buffers[0].stride = sizeof(glm::vec4);
        drawable_params.data[engine::graphics::kVertexSlot].offset = 0;
        drawable_params.data[engine::graphics::kVertexSlot].data_type = threed::DataType::Vec2F32;

        drawable_params.data[engine::graphics::kTextureSlot].offset = sizeof(glm::vec2);
        drawable_params.data[engine::graphics::kTextureSlot].data_type = threed::DataType::Vec2F32;

        drawable_params.start = 0;
        drawable_params.count = r_tex_gen->array_count;
        drawable_params.primitive = threed::Primitive::Triangles;
        drawable_params.index_type = threed::IndicesType::None;

        auto r_drawable = device.create_drawable(drawable_params);
        if (!r_drawable) {
            MODUS_LOGE("Failed to create text drawable: {}", r_drawable.error());
            return Error<>();
        }
        m_drawable = *r_drawable;

        // Create Effect
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = false;
        effect_params.state.blend.targets[0].enabled = true;
        effect_params.state.blend.targets[0].eq_source = threed::BlendEquation::SourceAlpha;
        effect_params.state.blend.targets[0].eq_destination =
            threed::BlendEquation::OneMinusSourceAlpha;

        effect_params.state.raster.cull_mode = threed::CullMode::Back;
        effect_params.state.raster.cull_enabled = true;
        effect_params.state.raster.face_counter_clockwise = true;

        // effect_params.state.sampler[0].filter_min = threed::TextureFilter::Linear;
        // effect_params.state.sampler[0].filter_mag = threed::TextureFilter::Linear;
        // effect_params.state.sampler[0].enabled = true;
        auto effect_create_result = device.create_effect(effect_params);
        if (!effect_create_result) {
            MODUS_LOGE("Failed to create text2d effect: {}", effect_create_result.error());
            return Error<>();
        }
        m_effect = *effect_create_result;

        return Ok<>();
    }

    void shutdown(engine::Engine& engine) {
        auto asset_module = engine.module<engine::ModuleAssets>();

        if (m_font_asset) {
            asset_module->loader().destroy_async(m_font_asset);
        }
        if (m_font_sdf_gpuprog_asset) {
            asset_module->loader().destroy_async(m_font_sdf_gpuprog_asset);
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();
        if (m_text_buffer) {
            device.destroy_buffer(m_text_buffer);
        }

        if (m_effect) {
            device.destroy_effect(m_effect);
        }
        if (m_drawable) {
            device.destroy_drawable(m_drawable);
        }
    }

    Result<> build(engine::Engine& engine, threed::PipelinePtr pipeline) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();
        const auto& compositor = graphics_module->default_compositor();
        threed::Pass* pass_clear = device.allocate_pass(*pipeline, "Clear");
        pass_clear->viewport.width = compositor.width();
        pass_clear->viewport.height = compositor.height();
        pass_clear->frame_buffer = compositor.frame_buffer();
        pass_clear->state.clear.colour[0].clear = true;
        pass_clear->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
        pass_clear->color_targets = threed::kFramebufferColorTarget0;

        const glm::mat4 proj =
            glm::ortho(0.0f, f32(compositor.width()), 0.0f, f32(compositor.height()));

        m_color_binder.m_value = glm::vec4(1.f);
        m_proj_binder.m_value = proj;
        m_sampler_binder.m_texture = m_font_atlas;

        threed::Command* command = device.allocate_command(*pass_clear);
        command->effect = m_effect;
        command->program = m_prog;
        command->drawable = m_drawable;
        m_binders.bind(*command, device);
        return Ok<>();
    }

    void on_compositor_update(engine::Engine&, const threed::Compositor&) override {}
};

class NullWorld final : public engine::gameplay::GameWorld {
   private:
    SDFPipeline& m_pipeline;

   public:
    NullWorld(SDFPipeline& pipeline) : m_pipeline(pipeline) {}

    Result<> initialize(engine::Engine&) override { return Ok<>(); }

    Result<> shutdown(engine::Engine&) override { return Ok<>(); }

    void on_enter(engine::Engine& engine) override {
        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline");
            engine.quit();
            return;
        }
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->set_pipeline(&m_pipeline);
    }
    void on_exit(engine::Engine& engine) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->unset_pipeline();
        m_pipeline.shutdown(engine);
    }

    void tick(engine::Engine&, const IMilisec) override {}
    void tick_fixed(engine::Engine&, const IMilisec) override {}
};

class SDFTest final : public GraphicsTestGame {
   private:
    SDFPipeline m_pipeline;
    NullWorld m_world;

   public:
    SDFTest()
        : modus::GraphicsTestGame("Engine Signed Distance Field Font Test",
                                  "modus.test.engine_sdffont"),
          m_pipeline(),
          m_world(m_pipeline) {}
    NotMyPtr<engine::gameplay::GameWorld> game_world() override { return &m_world; }

    Result<> load_startup_assets(engine::Engine& engine) override {
        return m_pipeline.preload(engine);
    }
};

GRAPHICS_TEST_MAIN(SDFTest)
