#
# Graphics module tests
#

link_libraries(engine_graphics_module)

add_subdirectory(test_shared)
add_subdirectory(hello_triangle)
add_subdirectory(camera)
add_subdirectory(lighting)
add_subdirectory(shadows)
add_subdirectory(animation)
add_subdirectory(particles)
add_subdirectory(sdf_font)
