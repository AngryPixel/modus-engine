/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>

#include <graphics_test_game.hpp>

#include <app/app.hpp>
#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/pipeline/pipeline.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/buffer.hpp>
#include <threed/compositor.hpp>
#include <threed/device.hpp>
#include <threed/drawable.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/instance.hpp>
#include <threed/program.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <threed/program_gen/shader_generator.hpp>

//#define USE_MANUAL_MODE

static const char* k_vert =
    R"(
#version 300 es
uniform highp float u_offset;

layout(location = 0) in highp vec3 Position;

void main()
{
    gl_Position = vec4(Position + vec3(u_offset, 0.f, 0.f), 1.0);
}
)";

static const char* k_frag =
    R"(
#version 300 es

layout(std140) uniform MyColour {
    highp vec4 colour;
};

layout(location = 0) out highp vec4 FragColor;
void main ()
{
  FragColor = colour;
}
)";

static const char* kDynamicVertexMain = R"R(
void main()
{
    gl_Position = vec4(Position, 1.0);
#if !defined(APPLY_Y)
    gl_Position.x += u_offset;
#else
    gl_Position.y += u_offset;
#endif
})R";

static const char* kDynamicFragmentMain = R"R(
void main()
{
    OUT_COLOR = colour;
})R";

using namespace modus;

class CustomPipeline final : public engine::graphics::IPipeline {
   public:
    threed::EffectHandle m_effect;
    threed::BufferHandle m_buffer;
    threed::BufferHandle m_buffer_index;
    threed::DrawableHandle m_drawable_first;
    threed::DrawableHandle m_drawable_second;
    threed::ConstantBufferHandle m_buffer_constant;
    threed::ProgramHandle m_program;
    threed::dynamic::TypedDefaultConstantInputBinder<f32> m_offset_binder;
    threed::dynamic::DefaultConstantBufferInputBinder m_cbuffer_binder;
    threed::dynamic::RuntimeBinders m_runtime_binders;

    CustomPipeline() : engine::graphics::IPipeline(1024) {}

    Result<> create_program_old(threed::Device& device) {
        threed::ProgramCreateParams params;
        params.vertex = StringSlice(k_vert).as_bytes();
        params.fragment = StringSlice(k_frag).as_bytes();
        threed::ProgramCBufferInputParam constant;
        constant.name = "MyColour";
        threed::ProgramConstantInputParam constant2;
        constant2.name = "u_offset";
        constant2.data_type = threed::DataType::F32;
        params.cbuffers.push_back(constant).expect("Failed to push back constant");
        params.constants.push_back(constant2).expect("Failed to push back constant");
        auto program_create_result = device.create_program(params);
        if (!program_create_result) {
            MODUS_LOGE("Failed to create Program: {}", program_create_result.error());
            return Error<>();
        };
        m_program = *program_create_result;
        return Ok<>();
    }

    Result<> create_program_new(engine::Engine& engine, threed::Device& device) {
        MODUS_UNUSED(engine);
        MODUS_UNUSED(device);

        const auto& shader_generator = device.shader_generator();
        threed::dynamic::TypedDefaultConstantInput<f32> offset_input("u_offset", &m_offset_binder);
        threed::dynamic::DefaultConstantBufferInput cb_input("MyColour", &m_cbuffer_binder);

        // cbuffer
        threed::dynamic::BufferMemberDesc member_desc;
        member_desc.data_type = threed::DataType::Vec4F32;
        member_desc.name = "colour";
        cb_input.m_desc.buffer_members.push_back(member_desc);

        // vertex input
        threed::dynamic::VertexInput vertex_input;
        vertex_input.data_type = threed::DataType::Vec3F32;
        vertex_input.name = "Position";
        vertex_input.slot = 0;

        // fragment output
        threed::dynamic::FragmentOutput frag_output =
            threed::dynamic::FragmentOutput::create_default();

        threed::dynamic::ProgramGenParams params;

        params.fragment_stage.constant_buffers.push_back(&cb_input).expect();
        params.vertex_stage.constants.push_back(&offset_input).expect();

        params.vertex_stage.inputs.push_back(vertex_input).expect();
        params.vertex_stage.main = kDynamicVertexMain;

        params.fragment_stage.fragment_outputs.push_back(frag_output).expect();
        params.fragment_stage.main = kDynamicFragmentMain;

        const threed::dynamic::ProgramGeneratorCondition conditions[] = {
            {"APPLY_Y", threed::dynamic::ShaderStage::Vertex, false, false}};

        // create program
        threed::dynamic::ProgramGenerator generator;
        if (auto r = generator.generate(params, shader_generator, make_slice(conditions)); !r) {
            MODUS_LOGE("Failed to generate shader: {}", r.error());
            return Error<>();
        } else {
            MODUS_LOGI("Generated {} variants", *r);
        }

        auto r_result = generator.result(0);
        if (!r_result) {
            MODUS_LOGE("Failed to access generated variant 0");
            return Error<>();
        }

        const threed::dynamic::ProgramGeneratorResult& result = **r_result;
        m_runtime_binders = result.binders;

        MODUS_LOGI("VertexShader:\n{}", StringSlice(result.create_params.vertex));
        MODUS_LOGI("FragmentShader:\n{}", StringSlice(result.create_params.fragment));

        auto r_program = device.create_program(result.create_params);
        if (!r_program) {
            MODUS_LOGE("Failed to create program: {}", r_program.error());
            return Error<>();
        }
        m_program = *r_program;
        return Ok<>();
    }

    Result<> initialize(engine::Engine& engine) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        threed::Device& device = graphics_module->device();
#if !defined(USE_MANUAL_MODE)
        if (!create_program_new(engine, device)) {
            return Error<>();
        }
#else
        if (!create_program_old(device)) {
            return Error<>();
        }
#endif
        // Effect
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = true;

        auto effect_create_result = device.create_effect(effect_params);
        if (!effect_create_result) {
            MODUS_LOGE("Failed to create effect: {}\n", effect_create_result.error());
            modus_panic("Test Failed");
        }
        m_effect = effect_create_result.value();

        // Buffer
        const f32 points[] = {
            -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 0.0f,
            -0.3f, -0.3f, 0.0f, 0.3f, -0.3f, 0.0f, 0.0f, 0.3f, 0.0f,
        };

        const u8 indices[] = {0, 1, 2, 2, 0, 1};

        threed::BufferCreateParams buffer_params;
        buffer_params.type = threed::BufferType::Data;
        buffer_params.usage = threed::BufferUsage::Static;
        buffer_params.size = sizeof(points);
        buffer_params.data = points;
        auto buffer_create_result = device.create_buffer(buffer_params);
        if (!buffer_create_result) {
            MODUS_LOGE("Failed to create buffer");
            return Error<>();
        }
        m_buffer = buffer_create_result.value();
        buffer_params.type = threed::BufferType::Indices;
        buffer_params.size = sizeof(indices);
        buffer_params.data = indices;
        if (auto r = device.create_buffer(buffer_params); !r) {
            MODUS_LOGE("Failed to create index buffer");
            return Error<>();
        } else {
            m_buffer_index = *r;
        }

        // Constant buffer
        threed::ConstantBufferCreateParams cbuffer_params;
        cbuffer_params.usage = threed::BufferUsage::Stream;
        cbuffer_params.size = sizeof(glm::vec4);
        cbuffer_params.data = nullptr;
        auto cbuffer_create_result = device.create_constant_buffer(cbuffer_params);
        if (!cbuffer_create_result) {
            MODUS_LOGE("Failed to create constant buffer");
            return Error<>();
        }
        m_buffer_constant = cbuffer_create_result.value();
#if !defined(USE_MANUAL_MODE)
        m_cbuffer_binder.m_buffer = m_buffer_constant;
#endif

        threed::DrawableCreateParams draw_params;
        draw_params.primitive = threed::Primitive::Triangles;
        draw_params.count = 3;
        draw_params.start = 0;
        draw_params.index_type = threed::IndicesType::U8;
        draw_params.index_buffer.buffer = m_buffer_index;
        draw_params.data_buffers[0].buffer = m_buffer;
        draw_params.data_buffers[0].stride = sizeof(glm::vec3);
        draw_params.data[0].data_type = threed::DataType::Vec3F32;
        draw_params.data[0].offset = 0;
        draw_params.data[0].normalized = false;

        if (auto r = device.create_drawable(draw_params); !r) {
            MODUS_LOGE("Failed to create drawable: {}", r.error());
            return Error<>();
        } else {
            m_drawable_first = *r;
        }

        draw_params.data_buffers[0].offset = sizeof(points) / 2;
        draw_params.index_buffer.offset = sizeof(indices) / 2;
        if (auto r = device.create_drawable(draw_params); !r) {
            MODUS_LOGE("Failed to create drawable: {}", r.error());
            return Error<>();
        } else {
            m_drawable_second = *r;
        }

        return Ok<>();
    }

    Result<> build(engine::Engine& engine, threed::PipelinePtr pipeline) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        threed::Device& device = graphics_module->device();
        threed::Compositor& comp = graphics_module->default_compositor();
        const DSeconds elapsed_sec = engine.elapsed_time_sec();

        auto pass = device.allocate_pass(*pipeline, "HelloTriangle");
        // Pass
        glm::vec4 colour(0.f, 0.f, 0.f, 1.0f);
        pass->viewport.width = comp.width();
        pass->viewport.height = comp.height();
        pass->frame_buffer = comp.frame_buffer();
        pass->state.clear.colour[0].clear = true;
        pass->state.clear.colour[0].colour = colour;
        pass->state.clear.clear_depth = true;
        pass->color_targets = threed::PassFramebufferColorTarget::kFramebufferColorTarget0;

        // Uniform
        glm::vec4 const_colour(0);
        const_colour.x = 1.0f;    // std::sin(elapsed_sec);
        const_colour.y = std::cos(elapsed_sec.count());
        const_colour.z = std::tan(elapsed_sec.count());
        const_colour.w = 1.0f;
        const f32 offset = std::sin(elapsed_sec.count());
        const f32 offset2 = std::cos(elapsed_sec.count());

        if (!device.update_constant_buffer(m_buffer_constant, const_colour)) {
            MODUS_LOGE("Failed to update constant buffer");
        }

        {
#if defined(USE_MANUAL_MODE)
            auto constant_handle = device.upload_constant(offset);
#else
            m_offset_binder.m_value = offset;
#endif
            auto command = device.allocate_command(*pass);
            command->drawable = m_drawable_first;
            command->program = m_program;
            command->effect = m_effect;
#if defined(USE_MANUAL_MODE)
            command->inputs.constants[0] = m_buffer_constant;
            command->inputs.constants[1] = constant_handle;
#else
            m_runtime_binders.bind(*command, device);
#endif
        }

        {
#if defined(USE_MANUAL_MODE)
            auto constant_handle = device.upload_constant(offset2);
#else
            m_offset_binder.m_value = offset2;
#endif
            auto command = device.allocate_command(*pass);
            command->drawable = m_drawable_second;
            command->program = m_program;
            command->effect = m_effect;
#if defined(USE_MANUAL_MODE)
            command->inputs.constants[0] = m_buffer_constant;
            command->inputs.constants[1] = constant_handle;
#else
            m_runtime_binders.bind(*command, device);
#endif
        }

        return Ok<>();
    }

    void shutdown(engine::Engine& engine) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        threed::Device& device = graphics_module->device();
        if (m_drawable_first) {
            device.destroy_drawable(m_drawable_first);
        }
        if (m_drawable_second) {
            device.destroy_drawable(m_drawable_second);
        }
        if (m_buffer) {
            device.destroy_buffer(m_buffer);
        }
        if (m_buffer_constant) {
            device.destroy_constant_buffer(m_buffer_constant);
        }
        if (m_effect) {
            device.destroy_effect(m_effect);
        }
        if (m_program) {
            device.destroy_program(m_program);
        }
    }

    void on_compositor_update(engine::Engine&, const threed::Compositor&) override {}
};

class HelloTriangleWorld final : public modus::engine::gameplay::GameWorld {
   public:
    engine::gameplay::EntityId m_camera_id;
    CustomPipeline m_pipeline;

    engine::gameplay::EntityId main_camera() const override { return m_camera_id; }

    engine::graphics::Camera& camera() {
        return m_entity_manager.component<engine::graphics::CameraComponent>(m_camera_id)
            .value_or_panic("Failed to get camera")
            ->m_camera;
    }

    Result<> initialize(engine::Engine& engine) override {
        if (!m_entity_manager.register_component<engine::graphics::CameraComponent>()) {
            MODUS_LOGE("Failed to register transform component");
            return Error<>();
        }

        // Create camera
        {
            m_camera_id =
                m_entity_manager.create("Camera").value_or_panic("failed to create camera");
            auto r_cc =
                m_entity_manager.add_component<engine::graphics::CameraComponent>(m_camera_id);
            if (!r_cc) {
                return Error<>();
            }
        }

        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline");
            return Error<>();
        }
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->set_pipeline(&m_pipeline);
        camera().set_look_at(glm::vec3(0, 7, -10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
        camera().frustum().set_aspect_ratio(1280.0f / 720.0f);

        return Ok<>();
    }

    Result<> shutdown(engine::Engine& engine) override {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        graphics_module->unset_pipeline();

        m_pipeline.shutdown(engine);
        return Ok<>();
    }

    void tick(engine::Engine& engine, const IMilisec) override {
        // Do entity add/removals from last frame
        m_entity_manager.update(engine);
    }
    void tick_fixed(engine::Engine&, const IMilisec) override {}
};

class HelloTriangleTest final : public GraphicsTestGame {
   private:
    HelloTriangleWorld m_world;

   public:
    HelloTriangleTest()
        : modus::GraphicsTestGame("Graphics Hello Triangl", "modus.test.graphics_hello_triangle") {}
    NotMyPtr<engine::gameplay::GameWorld> game_world() override { return &m_world; }

    Result<> initialize(engine::Engine& engine) override {
        /*
        vfs::VirtualFileSystem& vfs =
            engine.module<engine::ModuleFileSystem>(engine.modules().filesystem())
                ->vfs();
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "data";
        vfs_params.path = DATA_DIR;

        if (!vfs.mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount data path");
            return Error<>();
        }
        MODUS_LOGI("Mounted data path: {}", DATA_DIR);*/
        return GraphicsTestGame::initialize(engine);
    }

    Result<> load_startup_assets(engine::Engine&) override { return Ok<>(); }
};

GRAPHICS_TEST_MAIN(HelloTriangleTest)
