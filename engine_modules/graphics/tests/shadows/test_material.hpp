/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/modules/module_graphics_pch.h>

#include <engine/graphics/material.hpp>
#include <engine/graphics/material_properties.hpp>
#include <threed/program_gen/default_inputs.hpp>

using namespace modus;

static constexpr const char* kMaterialGUID = "08c5a884-6238-4e52-b6a8-7fbb8817bbdb";

static constexpr const char* kMaterialDeferredGUID = "ca5ef649-c318-41c5-9251-771d289c1c70";

class ForwardMaterialInstance final : public engine::graphics::MaterialInstance {
   private:
    engine::graphics::Vec4MaterialProperty m_prop_diffuse;
    static constexpr const char* kPropDiffuseName = "diffuse";

   public:
    ForwardMaterialInstance(const engine::graphics::MaterialHandle h, const u32 flags);

    void apply(engine::Engine& engine,
               engine::graphics::MaterialApplyParams& params) const override;

   private:
    Result<NotMyPtr<engine::graphics::MaterialProperty>, void> find_property(
        const StringSlice name) override;
    Result<NotMyPtr<const engine::graphics::MaterialProperty>, void> find_property(
        const StringSlice name) const override;
};

class ForwardMaterial final : public engine::graphics::MaterialHelper<ForwardMaterialInstance> {
    friend class ForwardMaterialInstance;

   public:
    static glm::vec3 sLightDir;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_mvp_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_mv_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat3> m_nm_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::vec4> m_diffuse_binder;
    threed::dynamic::RuntimeBinders m_binders;

    ForwardMaterial();

    Result<> initialize(engine::Engine& engine) override;

    Result<> shutdown(engine::Engine& engine) override;
};
