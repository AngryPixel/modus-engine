precision highp float;

//
// Engine Materials
//

uniform int u_material_flags;

#define MODUS_DIFFUSE_MAP_BIT (1<< 0)
#define MODUS_SPECULAR_MAP_BIT (1<< 1)
#define MODUS_NORMAL_MAP_BIT (1<< 2)
#define MODUS_EMISSIVE_MAP_BIT (1<< 3)

bool modus_material_has_diffuse_map(const int flags) {
    return (flags & MODUS_DIFFUSE_MAP_BIT) == MODUS_DIFFUSE_MAP_BIT;
}

bool modus_material_has_specular_map(const int flags) {
    return (flags & MODUS_SPECULAR_MAP_BIT) == MODUS_SPECULAR_MAP_BIT;
}

bool modus_material_has_normal_map(const int flags) {
    return (flags & MODUS_NORMAL_MAP_BIT) == MODUS_NORMAL_MAP_BIT;
}

bool modus_material_has_emissive_map(const int flags) {
    return (flags & MODUS_EMISSIVE_MAP_BIT) == MODUS_EMISSIVE_MAP_BIT;
}

#undef MODUS_DIFFUSE_MAP_BIT
#undef MODUS_SPECULAR_MAP_BIT
#undef MODUS_NORMAL_MAP_BIT
#undef MODUS_EMISSIVE_MAP_BIT
