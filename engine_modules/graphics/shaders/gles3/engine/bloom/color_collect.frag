#include "engine/api.glsl"

layout(location = 0) out vec4 frag_color;
in vec2 texture_coords;
uniform sampler2D u_input;

void main() {
    vec3 color = texture(u_input, texture_coords).xyz;
    float brightness = dot(color, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 0.7) {
        frag_color = vec4(color, 1.0);
    } else {
        frag_color = vec4(0.0);
    }
}
