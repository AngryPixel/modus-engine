#include "engine/api.glsl"

layout(location = 0) out vec4 frag_color;
uniform sampler2D u_input;
in vec2 texture_coords;
uniform int horizontal;

const float gaussian_weights[] = float[] (
    0.2270270270,
    0.3162162162,
    0.0702702703
);

void main()
{
    vec4 total_color = vec4(0.0);
    const float blur_radius = 1.0;
    float image_resolution = float((textureSize(u_input, 0)).x);
    float blur_step        = blur_radius / image_resolution;
    if (horizontal == 1) {
        /* Calculate blurred colour. */
        /* Blur a texel on the right. */
        total_color = texture(u_input, vec2(texture_coords.x + 1.0 * blur_step, texture_coords.y)) * gaussian_weights[0] +
                      texture(u_input, vec2(texture_coords.x + 2.0 * blur_step, texture_coords.y)) * gaussian_weights[1] +
                      texture(u_input, vec2(texture_coords.x + 3.0 * blur_step, texture_coords.y)) * gaussian_weights[2];
        /* Blur a texel on the left. */
        total_color += texture(u_input, vec2(texture_coords.x - 1.0 * blur_step, texture_coords.y)) * gaussian_weights[0] +
                       texture(u_input, vec2(texture_coords.x - 2.0 * blur_step, texture_coords.y)) * gaussian_weights[1] +
                       texture(u_input, vec2(texture_coords.x - 3.0 * blur_step, texture_coords.y)) * gaussian_weights[2];
    } else {
        /* Blur a texel to the top. */
        total_color = texture(u_input, vec2(texture_coords.x, texture_coords.y + 1.0 * blur_step)) * gaussian_weights[0] +
                      texture(u_input, vec2(texture_coords.x, texture_coords.y + 2.0 * blur_step)) * gaussian_weights[1] +
                      texture(u_input, vec2(texture_coords.x, texture_coords.y + 3.0 * blur_step)) * gaussian_weights[2];
        /* Blur a texel to the bottom. */
        total_color += texture(u_input, vec2(texture_coords.x, texture_coords.y - 1.0 * blur_step)) * gaussian_weights[0] +
                       texture(u_input, vec2(texture_coords.x, texture_coords.y - 2.0 * blur_step)) * gaussian_weights[1] +
                       texture(u_input, vec2(texture_coords.x, texture_coords.y - 3.0 * blur_step)) * gaussian_weights[2];
    }

    /* Set the output colour. */
    frag_color = vec4(total_color.xyz, 1.0);
}
