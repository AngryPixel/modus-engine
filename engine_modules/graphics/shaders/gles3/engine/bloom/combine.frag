#include "engine/api.glsl"

layout(location = 0) out vec4 frag_color;
uniform sampler2D u_color;
uniform sampler2D u_blur;
uniform sampler2D u_blur_prev;
in vec2 texture_coords;

void main() {
    vec3 color = texture(u_color, texture_coords).xyz;
    vec3 bloom_color = texture(u_blur, texture_coords).xyz;
    vec3 bloom_color_prev = texture(u_blur_prev, texture_coords).xyz;
    color += mix(bloom_color, bloom_color_prev, 0.5); // additive blending
    frag_color = vec4(color, 1.0);
}
