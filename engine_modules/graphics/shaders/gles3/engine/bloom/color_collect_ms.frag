#include "engine/api.glsl"

layout(location = 0) out vec4 frag_color;
in vec2 texture_coords;
uniform highp sampler2DMS u_input;

void main() {
    vec3 color = vec3(0.0);
    for (int i = 0; i < 4; ++i) {
        vec3 color += texelFetch(u_input, texture_coords, i).xyz;
    }
    float brightness = dot(color, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 0.5) {
        frag_color = vec4(color, 1.0);
    } else {
        frag_color = vec4(0.0);
    }
}

