#include "engine/api.glsl"
#include "engine/materials.glsl"

// Example forward rendering fragment shader with fixed directional light

layout(location = 0) out vec4 frag_colour;

in vec3 var_position;
in vec3 var_normal;
in vec2 var_texture;
in mat3 var_tbn;

uniform vec4 u_diffuse;

void main() {
    // hard coded global light
    vec3 light_dir = vec3(1);
    vec3 normal = var_normal;
    normal = texture(u_normal_map, var_texture).rgb;
    normal = normalize(normal * 2.0 - 1.0);
    light_dir = var_tbn * light_dir;

    float light_contribution = max(dot(normal, light_dir), 0.0);
    vec4 diffuse = u_diffuse;
    const vec4 ambient_light = vec4(0.1, 0.1,0.1, 1);
    frag_colour = (ambient_light + light_contribution) * diffuse;
}

