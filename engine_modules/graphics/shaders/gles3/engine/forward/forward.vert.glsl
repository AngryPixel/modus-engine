#include "engine/api.glsl"
#include "engine/globals.glsl"
#include "engine/frame_globals.glsl"
#include "engine/lighting/normal_mapping.glsl"

layout(location = MODUS_VERTEX_SLOT) in vec4 POSITION;
layout(location = MODUS_NORMAL_SLOT) in vec3 NORMAL;
layout(location = MODUS_TEXTURE_SLOT) in vec2 TEXTURE;
layout(location = MODUS_TANGENT_SLOT) in vec3 TANGENT;

#ifndef FORWARD_INSTANCED
uniform mat4 u_mvp;
uniform mat4 u_model_matrix;
uniform mat3 u_normal_matrix;
#else
layout(location = 6) in mat4 u_model_matrix;
layout(location = 12) in mat3 u_normal_matrix;
#endif

out vec3 var_position;
out vec3 var_normal;
out vec2 var_texture;
out mat3 var_tbn;

void main() {
#ifdef FORWARD_INSTANCED
    mat4 u_mvp = projection_view_matrix * u_model_matrix;
#endif
    var_normal = normalize(u_normal_matrix * NORMAL);
    var_position = vec3(u_model_matrix * POSITION);
    var_texture = TEXTURE;
    vec3 BINORMAL = cross(NORMAL, TANGENT);
    var_tbn = generate_tbn_matrix2(u_normal_matrix, NORMAL, TANGENT, BINORMAL);

    gl_Position = u_mvp * POSITION;
}

