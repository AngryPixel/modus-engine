#include "engine/api.glsl"
#include "engine/globals.glsl"
layout(location = MODUS_VERTEX_SLOT) in vec4 POSITION;

uniform mat4 u_mvp;

void main() {
    gl_Position = u_mvp * POSITION;
}
