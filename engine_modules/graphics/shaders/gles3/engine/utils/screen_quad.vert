#include "engine/api.glsl"
#include "engine/globals.glsl"


// Generate a quad that covers the screen
// Use with draw(TRIANGLE_LIST, 4)

out vec2 screen_location;
out vec2 texture_coords;
flat out highp int instance_id;

void main()
{
    const vec2 positions[4] = vec2[](
        vec2(-1, -1),
        vec2(+1, -1),
        vec2(-1, +1),
        vec2(+1, +1)
    );
    const vec2 coords[4] = vec2[](
        vec2(0, 0),
        vec2(1, 0),
        vec2(0, 1),
        vec2(1, 1)
    );

    texture_coords = coords[gl_VertexID];
    screen_location = positions[gl_VertexID];
    gl_Position = vec4(positions[gl_VertexID], 0.0, 1.0);
    instance_id = gl_InstanceID;
}
