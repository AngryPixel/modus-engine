#include "engine/api.glsl"
#include "engine/frame_globals.glsl"
#include "engine/materials.glsl"
#include "engine/lighting/lighting.glsl"

uniform sampler2D u_diffuse_map;
uniform sampler2D u_normal_map;
uniform sampler2D u_specular_map;
uniform vec4 u_diffuse;
uniform vec3 u_emissive;
uniform float u_specular_factor;
uniform vec3 u_diffuse_mix;

in vec3 var_position;
in vec3 var_normal;
in vec2 var_texture;
in mat3 var_tbn;

layout(location = 0) out vec4 frag_color;

void main() {

    // Get Normal
    vec3 normal = normalize(var_normal);
    if (modus_material_has_normal_map(u_material_flags)) {
        normal = texture(u_normal_map, var_texture).rgb;
        normal =((normal * 2.0) - vec3(1.0));
        normal = normalize(var_tbn * normal);
    }

    float specular = u_specular_factor;
    if (modus_material_has_specular_map(u_material_flags)) {
        specular = texture(u_specular_map, var_texture).r;
    }

    // Get Diffuse
    vec4 diffuse = u_diffuse;
    if (modus_material_has_diffuse_map(u_material_flags)) {
        vec4 diffuse_map = texture(u_diffuse_map, var_texture);
        diffuse = vec4(mix(u_diffuse.xyz, diffuse_map.xyz, u_diffuse_mix), 1.0);
    }

    frag_color = diffuse *
        calculate_lights_forward(var_position, normal, specular, u_emissive);
}

