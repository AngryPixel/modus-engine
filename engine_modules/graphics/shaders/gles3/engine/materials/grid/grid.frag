#extension GL_OES_standard_derivatives : enable
#include "engine/api.glsl"

// From http://www.madebyevan.com/shaders/grid/

layout(location = 0) out highp vec4 frag_colour;
in highp vec3 vertex;

void main() {
  // Pick a coordinate to visualize in a grid
  highp vec2 coord = vertex.xz;

  // Compute anti-aliased world-space grid lines
  highp vec2 grid = abs(fract(coord - 0.5) - 0.5) / fwidth(coord);
  highp float line = min(grid.x, grid.y);

  // Just visualize the grid lines directly
  frag_colour = vec4((vec3(1.0 - min(line, 1.0))), 0.9);
}

