#include "engine/api.glsl"
#include "engine/globals.glsl"
#include "engine/frame_globals.glsl"

layout(location = MODUS_VERTEX_SLOT) in highp vec3 vertex_position;

uniform mat4 view_mat;

out highp vec3 vertex;

void main() {
    gl_Position = projection_view_matrix * view_mat * vec4 (vertex_position, 1.0);
    vertex = vertex_position;
}
