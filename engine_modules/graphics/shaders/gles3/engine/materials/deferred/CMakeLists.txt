#
# Deferred Material Shaders
#

modus_make_gl_program(engine_graphics_module
    VERTEX deferred.vert
    FRAGMENT deferred.frag
    CPP_HEADER "${cpp_header_dir}/materials/deferred.hpp"
    CPP_NAMESPACE ${cpp_namespace}
    OUTPUT ${program_output}/materials/deferred.gpuprog
    INCLUDE_PATHS "${engine_include_path}"
    GEN_DIR "${gen_dir}"
)

modus_make_gl_program(engine_graphics_module
    VERTEX deferred_instanced.vert
    FRAGMENT deferred.frag
    CPP_HEADER "${cpp_header_dir}/materials/deferred_instanced.hpp"
    CPP_NAMESPACE ${cpp_namespace}
    OUTPUT ${program_output}/materials/deferred_instanced.gpuprog
    INCLUDE_PATHS "${engine_include_path}"
    GEN_DIR "${gen_dir}"
)
