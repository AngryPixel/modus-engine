#include "engine/api.glsl"
//#define USE_FORWARD
#ifndef USE_FORWARD
#include "engine/deferred/deferred.frag.glsl"
#else
layout(location = 0) out vec4 frag_colour;
#endif
#include "engine/materials.glsl"

uniform sampler2D u_diffuse_map;
uniform sampler2D u_normal_map;
uniform sampler2D u_specular_map;
uniform vec4 u_diffuse;
uniform vec3 u_emissive;
uniform float u_specular_factor;
uniform vec3 u_diffuse_mix;

in vec3 var_position;
in vec3 var_normal;
in vec2 var_texture;
in mat3 var_tbn;

void main() {

    // Get Normal
    vec3 normal = normalize(var_normal);
    if (modus_material_has_normal_map(u_material_flags)) {
        normal = texture(u_normal_map, var_texture).rgb;
        normal =((normal * 2.0) - vec3(1.0));
        normal = normalize(var_tbn * normal);
    }

    float specular = u_specular_factor;
    if (modus_material_has_specular_map(u_material_flags)) {
        specular = texture(u_specular_map, var_texture).r;
    }

    // Get Diffuse
    vec4 diffuse = u_diffuse;
    if (modus_material_has_diffuse_map(u_material_flags)) {
        vec4 diffuse_map = texture(u_diffuse_map, var_texture);
        diffuse = vec4(mix(u_diffuse.xyz, diffuse_map.xyz, u_diffuse_mix), 1.0);
    }

#ifndef USE_FORWARD
    set_gbuffer_color(vec3(diffuse), specular);
    set_gbuffer_normal(normal);
    set_gbuffer_emissive(vec3(u_emissive));
# else
    vec3 light_dir = vec3(1);
    float light_contribution = max(dot(normal, light_dir), 0.0);
    const vec4 ambient_light = vec4(0.1, 0.1,0.1, 1);
    frag_colour = (ambient_light + light_contribution) * diffuse;
#endif
}

