#include "engine/api.glsl"

in highp vec3 tex_coord;
layout(location = 0) out highp vec4 frag_colour;
uniform samplerCube skybox;

void main() {
    frag_colour = texture(skybox, tex_coord);
}

