#include "engine/api.glsl"
#include "engine/globals.glsl"
#include "engine/frame_globals.glsl"

layout(location = MODUS_VERTEX_SLOT) in highp vec3 vertex_position;

uniform highp mat4 view_skybox;
out highp vec3 tex_coord;

void main() {
    highp vec4 pos = projection_matrix * view_skybox * vec4 (vertex_position, 1.0);
    gl_Position = pos.xyww;
// NOTE: Astc cubemaps needs to be inverted before processing as the cubemaps
// do not require the images to be flipped to be displayed correctly.
// Alternatively we can also change the coordinates of the shader and
// the order in which the images are processed so this is not really required.
// I chose the latter
    tex_coord = vec3(vertex_position.x, -vertex_position.y, vertex_position.z);
}
