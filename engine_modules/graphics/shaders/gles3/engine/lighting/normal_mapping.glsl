// Normal mapping utilities

mat3 generate_tbn_matrix2(in mat3 normal_matrix, in vec3 normal, in vec3 tangent, in vec3 binormal) {
    vec3 N = normalize(normal_matrix * normal);
    vec3 T = normalize(normal_matrix * tangent);
    vec3 B = normalize(normal_matrix * binormal);
    return (mat3(T,B, N));
}

// Generate TBN matrix without binormal vector
// More expensive but also increases quality
mat3 generate_tbn_matrix_no_binormal(in mat3 normal_matrix, in vec3 normal, in vec3 tangent) {
    vec3 N = normalize(normal_matrix * normal);
    vec3 T = normalize(normal_matrix * tangent);
    T = normalize(T - dot(T,N)* N);
    vec3 B = cross(N,T);
    return mat3(T,B, N);
}


//uniform extern float4x4 mWorldView;
//Vertex shader:
// Transform TBN to view space
//    TBN[0] = mul(tangentL, gWorldView);
//    TBN[1] = mul(binormalL, gWorldView);
//    TBN[2] = mul(normalL, gWorldView);

//Pixel shader:
// Sample normal map and transform it to view space
//    float3 normalT = tex2D(NormalMapS, TexCoord);
// Expand from [0, 1] compressed interval to true [-1, 1] interval.
//    normalT = 2.0f * normalT - 1.0f;
// Transform normal to view space
//    float3 viewNormal = mul(normalT, TangentToViewMatrix);
