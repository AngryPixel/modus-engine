//
// Everything Shadow Related
//

uniform sampler2D u_shadow_map;

layout(std140) uniform ShadowBlock {
    mat4 u_shadow_vp;
    vec4 u_shadow_light_dir;
    int num_shadow_casters;
};

// Calculate whether the light space project coordinate has shadow coverage
// \return 1.0 on true, 0.0 on false.
float calculate_shadows(in vec3 proj_coord, in vec3 normal) {
    // Sample depth buffer
    float closest_depth = texture(u_shadow_map, proj_coord.xy).r;
    float current_depth = proj_coord.z;

    // early exit
    if (current_depth > 1.0) {
        return 0.0;
    }

    // check whether current position is in shadow
    // Apply bias based on the original light direction and the current normal
    // Helps with round objects
    float cos_theta = clamp(dot(normal, u_shadow_light_dir.xyz), 0.0, 1.0);
    float bias = 0.005*tan(acos(cos_theta));
    bias = clamp(bias, 0.0,0.005);

#if defined(USE_PCF)
    // Apply PCF Filtering with 16 samples
    float shadow = 0.0;
    vec2 texel_size = 1.0 / vec2(textureSize(u_shadow_map, 0));
    for(float x = -1.5; x <= 1.5; x+=1.0)
    {
        for(float y = -1.5; y <= 1.5; y+=1.0)
        {
            float pcf_depth = texture(u_shadow_map, proj_coord.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth - bias > pcf_depth ? 1.0 : 0.0;
        }
    }
    shadow /= 16.0;
    return shadow;
# else
    float pcf_depth = texture(u_shadow_map, proj_coord.xy).r;
    return current_depth - bias > pcf_depth ? 1.0 : 0.0;
#endif
}

float calculate_shadows_light_space(in vec4 position_light_space, in vec3 normal) {
    if (num_shadow_casters == 0) {
        return 0.0;
    }
    // perspective divide to convert to light screen space
    vec3 proj_coord = position_light_space.xyz / position_light_space.w;
    // normalize to [0 - 1]
    proj_coord = (proj_coord + 1.0) * 0.5;
    return calculate_shadows(proj_coord, normal);
}

float calculate_shadows_world_pos(in vec3 world_position, in vec3 normal) {
    if (num_shadow_casters == 0) {
        return 0.0;
    }
    // TODO: Bias Matrix ??
    // convert to light space
    vec4 position_light_space = u_shadow_vp * vec4(world_position, 1.0);
    // perspective divide to convert to light screen space
    vec3 proj_coord = position_light_space.xyz / position_light_space.w;
    // normalize to [0 - 1]
    proj_coord = (proj_coord + 1.0) * 0.5;
    return calculate_shadows(proj_coord, normal);
}
