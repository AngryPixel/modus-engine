#include "engine/api.glsl"

layout(location = 0) out vec4 frag_color;

in vec2 texture_coords;

uniform highp usampler2D u_grid;
uniform highp usampler2D u_indices;
uniform float u_num_lights;
#define USE_TEXEL_FETCH 1
//#define DISPLAY_LIGHTIDX
void main() {
#ifdef USE_TEXEL_FETCH
    ivec2 tile_coord = ivec2(gl_FragCoord.xy / vec2(64, 64));
    uvec2 grid_info = texelFetch(u_grid, tile_coord,0).xy;
#else
    uvec2 grid_info = texture(u_grid, texture_coords).xy;
#endif
    vec2 indices_size = vec2(textureSize(u_indices, 0));

#ifdef DISPLAY_LIGHTIDX
    uint index_offset = grid_info.x;
    uint light_count = grid_info.y;
    vec3 color = vec3(0);
    for(uint i = 0u; i < light_count && i < 3u; i++) {
        ivec2 index_coord = ivec2(index_offset + i, 0);
        uint light_index = texelFetch(u_indices, index_coord, 0).r;
        color[i] = float(light_index)/u_num_lights;
    }
    frag_color = vec4(color, 0.5);
#else
    frag_color = vec4(0., (vec2(grid_info)/vec2(indices_size.x,u_num_lights)).y, 0, 0.5);
#endif

    if (uint(gl_FragCoord.x) % 64u == 0u || uint(gl_FragCoord.y) % 64u == 0u) {
        frag_color = vec4(1, 1,1,0.3);
    }
}

