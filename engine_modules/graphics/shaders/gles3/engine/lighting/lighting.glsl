// Light Calculation routines

#if !defined(SKIP_SHADOWS)
#include "engine/lighting/shadows.glsl"
#endif

#define TILE_WIDTH 64
#define TILE_HEIGHT 64
#define USE_TILED_FORWARD 1

struct DirectionalLight {
    vec3 diffuse_color;
    float diffuse_intensity;
    vec3 direction;
    float paddingxx;
};

struct PointLight {
    vec3 diffuse_color;
    float diffuse_intensity;
    vec3 position;
    float radius;
};

layout(std140) uniform Lights {
    DirectionalLight u_directional_lights[4];
    PointLight u_point_lights[128];
    vec4 u_ambient;
    int u_num_directional_lights;
    int u_num_point_lights;
};

//#define USE_TOON_SHADING

// Generic light calculations, diffuse only
vec4 calculate_light(in vec3 color,
    in vec3 world_position,
    in vec3 normal,
    in vec3 light_dir,
    in float diffuse_intensity,
    in float specular_factor) {

#ifndef USE_TOON_SHADING
    float n_dot_l = dot(normal, light_dir);
    float diffuse_factor = n_dot_l;

    // calculate diffuse
    vec4 diffuse_color = vec4(0);
    if (diffuse_factor > 0.0) {
        diffuse_color = vec4(color
                * diffuse_intensity
                * diffuse_factor, 1.0);
    }

    // calculate specular
    vec3 view_dir = normalize(get_camera_position() - world_position);
    vec3 halfway_dir = normalize(light_dir + view_dir);

    //TODO: Specify shininess property
    const float shininess = 16.0;
    float specular = pow(max(dot(normal, halfway_dir), 0.0), shininess * shininess);
    //float specular = pow(max(dot(view_dir, reflect_dir), 0.0), shininess);
    vec4 specular_color = vec4(vec3(specular_factor * specular * color), 1.0);
    return diffuse_color + specular_color;
#else

    float n_dot_l = dot(normal, light_dir);
    float diffuse_factor = smoothstep(0.0, 0.05, n_dot_l);

    // calculate diffuse
    vec4 diffuse_color = vec4(0);
    if (diffuse_factor > 0.0) {
        diffuse_color = vec4(color
                * diffuse_intensity
                * diffuse_factor, 1.0);
    }

    // calculate specular
    vec3 view_dir = normalize(get_camera_position() - world_position);
    vec3 halfway_dir = normalize(light_dir + view_dir);

    //TODO: Specify shininess property
    const float shininess = 16.0;
    float specular = pow(max(dot(normal, halfway_dir), 0.0), shininess * shininess);
    float specular_smooth = smoothstep(0.005, 0.01, specular);
    vec4 specular_color = vec4(vec3(specular_factor * specular_smooth * color), 1.0);

#define USE_RIM
#ifdef USE_RIM
    // Calculate rim
    float rim_dot = 1.0 - max(dot(view_dir, normal), 0.0);
    const float rim_amount = 0.8;
    // We only want rim to appear on the lit side of the surface,
    // so multiply it by NdotL, raised to a power to smoothly blend it.
    const float rim_threshold = 0.1;
	float rim_intensity = rim_dot * pow(n_dot_l, rim_threshold);
    rim_intensity = smoothstep(rim_amount - 0.01, rim_amount + 0.01, rim_intensity);
    vec4 rim = vec4(vec3(rim_intensity), 1.0);
#else
    vec4 rim = vec4(0.0);
#endif
    return diffuse_color + specular_color + rim;

#endif // USE_TOON_SHADING
}

vec4 calculate_directional_light(in DirectionalLight light, in vec3 normal,
    in vec3 world_position, float specular_factor) {
    return calculate_light(light.diffuse_color,
        world_position,
        normal,
        light.direction,
        light.diffuse_intensity,
        specular_factor);
}

vec4 calculate_point_light(in PointLight light, in vec3 normal,
    in vec3 world_position, float specular_factor) {

    vec3 light_dir = light.position - world_position;
    float dist = length(light_dir);
    light_dir = normalize(light_dir);

    vec4 color = calculate_light(light.diffuse_color,
        world_position,
        normal,
        light_dir,
        light.diffuse_intensity,
        specular_factor);

    /*float attenuation = light.a_constant +
        (light.a_linear * dist) +
        (light.a_exp * (dist * dist));*/

    // Alternative
    //float attenuation = clamp(1.0 - (dist*dist)/(light.radius * light.radius), 0.0, 1.0);
    float attenuation = clamp(1.0 - dist/light.radius, 0.0, 1.0);
    attenuation *= attenuation;

    return color * attenuation;
}

#if defined(USE_TILED_FORWARD)
uniform highp usampler2D u_light_grid;
uniform highp usampler2D u_light_indices;
#endif

vec4 calculate_lights_forward(in vec3 position, in vec3 normal,
    float specular_factor, in vec3 emissive) {
    vec4 final_color = vec4(0);
    for(int i = 0; i < u_num_directional_lights; ++i) {
        final_color += calculate_directional_light(u_directional_lights[i], normal, position, specular_factor);
    }
#if defined(USE_TILED_FORWARD)
    if (u_num_point_lights != 0) {
        ivec2 tile_coord = ivec2(gl_FragCoord.xy / vec2(TILE_WIDTH, TILE_HEIGHT));
        uvec2 tile_info = texelFetch(u_light_grid, tile_coord,0).xy;

        uint index_offset = tile_info.x;
        uint light_count = tile_info.y;
        for(uint i = 0u; i < light_count; i++) {
            ivec2 index_coord = ivec2(index_offset + i, 0);
            uint light_index = texelFetch(u_light_indices, index_coord, 0).r;
            final_color += calculate_point_light(u_point_lights[light_index], normal,
                position, specular_factor);
        }
    }
#else
    for (int i = 0; i < u_num_point_lights;++i) {
        final_color += calculate_point_light(u_point_lights[i], normal, position, specular_factor);
    }
#endif
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
#if !defined(SKIP_SHADOWS)
    float shadow_factor = calculate_shadows_world_pos(position, normal);
#else
    float shadow_factor = 0.0;
#endif
    return ambient_color + ((1.0 - shadow_factor) * final_color) + vec4(emissive, 1.0);
}

vec4 calculate_directional_light_deferred(in vec3 world_position, in vec3 normal,
    int instance_id, float specular_factor, in vec3 emissive) {
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
#if !defined(SKIP_SHADOWS)
    float shadow_factor = calculate_shadows_world_pos(world_position, normal);
#else
    float shadow_factor = 0.0;
#endif
    return ambient_color + vec4(emissive, 1.0)
        + ((1.0 - shadow_factor)
            * calculate_directional_light(u_directional_lights[instance_id],
            normal, world_position, specular_factor));
}

vec4 calculate_point_light_deferred(in vec3 position, in vec3 normal,
        int instance_id, float specular_factor, in vec3 emissive) {
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
    return ambient_color + vec4(emissive,1.0)
        + calculate_point_light(u_point_lights[instance_id], normal, position, specular_factor);
}
