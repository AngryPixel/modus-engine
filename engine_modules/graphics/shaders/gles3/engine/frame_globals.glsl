//
// Shared Global Data
//
layout(std140) uniform FrameGlobals {
    mat4 projection_matrix;
    mat4 view_matrix;
    mat4 projection_view_matrix;
    mat4 inv_view_matrix;
    mat4 inv_proj_view_matrix;
    float tick_sec;
    float elapsed_sec;
    int screen_width;
    int screen_height;
    vec2 u_near_far;
};

vec3 get_camera_position() {
    return vec3(inv_view_matrix[3][0], inv_view_matrix[3][1], inv_view_matrix[3][2]);
}

