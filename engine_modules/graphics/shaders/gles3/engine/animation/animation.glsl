#include "engine/globals.glsl"
layout(location = MODUS_BONE_INDICES_SLOT) in uvec4 BONE_INDICES;
layout(location = MODUS_BONE_WEIGHTS_SLOT) in vec4 BONE_WEIGHTS;

#define MODUS_ANIMATION_MAX_SKELETON_BONES 16

layout(std140) uniform SkeletonAnimData {
    mat4[MODUS_ANIMATION_MAX_SKELETON_BONES] u_bone_transforms;
};

mat4 get_bone_transform() {
    mat4 bone_transform = u_bone_transforms[BONE_INDICES[0]] * BONE_WEIGHTS[0];
    bone_transform += u_bone_transforms[BONE_INDICES[1]] * BONE_WEIGHTS[1];
    bone_transform += u_bone_transforms[BONE_INDICES[2]] * BONE_WEIGHTS[2];
    bone_transform += u_bone_transforms[BONE_INDICES[3]] * BONE_WEIGHTS[3];
    return bone_transform;
}
