#include "engine/api.glsl"
#include "engine/deferred/deferred_shading.glsl"
#include "engine/lighting/lighting.glsl"

layout(location = 0) out vec4 frag_color;

flat in int instance_id;


float calculate_shadows_deferred(in vec3 position, in vec3 normal) {

    mat4 project_to_light_view = u_shadow_vp; //*inv_view_matrix;
    vec4 projected_eye_dir = project_to_light_view * vec4(position, 1.0);
    vec3 projected_coord = projected_eye_dir.xyz / projected_eye_dir.w;
    projected_coord = (projected_coord + 1.0) * 0.5;
    return calculate_shadows(projected_coord, normal);
}


void main() {

    vec2 texture_coords = get_gbuffer_tex_coordinates();
    vec3 normal = get_gbuffer_normal(texture_coords).xyz;
    vec4 diffuse = get_gbuffer_color_specular(texture_coords);
    vec3 position = get_gbuffer_position_simple(texture_coords);
    vec3 emissive = get_gbuffer_emissive(texture_coords);

    vec4 final_color = calculate_directional_light_deferred(position, normal, instance_id, diffuse.w, emissive);
    frag_color = final_color * vec4(diffuse.xyz, 1.0f);
}
