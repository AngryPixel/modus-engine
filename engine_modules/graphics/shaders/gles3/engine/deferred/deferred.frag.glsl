#include "engine/api.glsl"

// Default deferred fragment shader
layout(location = 0) out vec4 frag_color;
layout(location = 1) out vec4 frag_normal;
layout(location = 2) out vec4 frag_emissive;

void set_gbuffer_color(in vec3 color, float specular) {
    frag_color = vec4(color, specular);
}

void set_gbuffer_normal(in vec3 normal) {
    // Assumes normal storage is GL_RGB10_A2
    frag_normal = vec4((vec3(1) + normal) * 0.5,0);
}

void set_gbuffer_emissive(in vec3 emissive) {
    frag_emissive = vec4(emissive, 0);
}
