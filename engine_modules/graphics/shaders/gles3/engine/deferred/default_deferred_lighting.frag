#include "engine/api.glsl"
#include "engine/deferred/deferred_shading.glsl"


layout(location = 0) out vec4 frag_color;

in vec2 texture_coords;

uniform vec3 u_light_dir;

void main() {

    //vec3 position = get_gbuffer_position(texture_coords);
    vec3 normal = get_gbuffer_normal(texture_coords).xyz;
    vec4 diffuse = get_gbuffer_color_specular(texture_coords);

    // hard coded global light
    vec3 light_dir = normalize(-u_light_dir);
    float light_contribution = dot(normal, light_dir);
    vec4 final_color = vec4(0);
    if (light_contribution>0.0) {
        vec4 light_color = vec4(1.0);
        const vec4 ambient_light = vec4(0.1, 0.1,0.1, 1);
        final_color = (light_contribution * light_color);
    } else {
        //final_color = vec4(((vec3(1) + light_dir) *0.5), 1.0);
    }
    frag_color = diffuse * (final_color + vec4(0.05,0.05,0.05,1.0));
}
