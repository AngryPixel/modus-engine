#include "engine/api.glsl"
#include "engine/deferred/deferred_shading.glsl"
#include "engine/lighting/lighting.glsl"


layout(location = 0) out vec4 frag_color;

uniform int instance_id;

void main() {

    vec2 texture_coords = get_gbuffer_tex_coordinates();
    vec3 normal = get_gbuffer_normal(texture_coords).xyz;
    vec3 position = get_gbuffer_position_simple(texture_coords);
    vec4 diffuse = get_gbuffer_color_specular(texture_coords);
    vec3 emissive = get_gbuffer_emissive(texture_coords);

    vec4 final_color = calculate_point_light_deferred(position, normal, instance_id, diffuse.w, emissive);
    frag_color = final_color * vec4(diffuse.xyz, 1.0f);
}

