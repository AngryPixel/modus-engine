#include "engine/frame_globals.glsl"

uniform sampler2D u_gbuffer_color;
uniform sampler2D u_gbuffer_normal;
uniform sampler2D u_gbuffer_depth;
uniform sampler2D u_gbuffer_emissive;

vec2 get_gbuffer_tex_coordinates() {
    return gl_FragCoord.xy / vec2(textureSize(u_gbuffer_color,0));
}

vec4 get_gbuffer_color_specular(in vec2 coords) {
    return texture(u_gbuffer_color, coords);
}

vec4 get_gbuffer_normal(in vec2 coords) {
    // Assumes normal storage is GL_RGB10_A2
    vec4 normal = texture(u_gbuffer_normal, coords);
    return vec4(normalize((normal.xyz * 2.0) - vec3(1.0)), normal.a);
}

vec3 get_gbuffer_emissive(in vec2 coords) {
    return texture(u_gbuffer_emissive, coords).xyz;
}

float depth_to_z_position(in float depth) {
    // camerarange {near, far}
    return u_near_far.x / (u_near_far.y - depth * (u_near_far.y - u_near_far.x)) *
    u_near_far.y;
}

vec3 get_gbuffer_position(in vec2 coords) {
    float depth = texture(u_gbuffer_depth, coords).r;
    depth = (depth * 2.0) - 1.0;
    vec2 u_buffer_size = vec2(textureSize(u_gbuffer_color, 0));
    vec3 screen_coord = vec3(
        ((gl_FragCoord.x/u_buffer_size.x)-0.5) * 2.0,
        (((-gl_FragCoord.y/u_buffer_size.y)+0.5) * 2.0) / (u_buffer_size.x/u_buffer_size.y),
        depth_to_z_position( depth ));
    screen_coord.x *= screen_coord.z;
    screen_coord.y *= -screen_coord.z;
    return screen_coord;
}

vec3 get_gbuffer_position_2(in vec2 coords) {
    float depth = texture(u_gbuffer_depth, coords).r;
    //depth = (depth * 2.0) - 1.0;
    vec2 adjusted = (coords * 2.0) - 1.0;
    vec2 u_buffer_size = vec2(textureSize(u_gbuffer_color, 0));
    vec3 screen_coord = vec3(adjusted.x,
        adjusted.y / (u_buffer_size.x/u_buffer_size.y),
        depth_to_z_position( depth ));
    screen_coord.x *= screen_coord.z;
    screen_coord.y *= -screen_coord.z;
    return screen_coord;
}


// Convert screen space coordinates back to world position coordinates.
// Simple but not the fastest one.
vec3 get_gbuffer_position_simple(in vec2 coords) {
    // For D3D maybe?
    // vec2 adjusted = vec2((coords.x * 2.0) - 1.0, (1.0-coords.y) * 2.0 - 1.0);
    vec2 adjusted = (coords * 2.0) - 1.0;
    float depth = texture(u_gbuffer_depth, coords).r;
    depth = (depth * 2.0) - 1.0;
    vec4 screen_space = vec4(adjusted, depth, 1);
    vec4 world_space = inv_proj_view_matrix * screen_space;
    return (world_space.xyz)/world_space.w;
}
