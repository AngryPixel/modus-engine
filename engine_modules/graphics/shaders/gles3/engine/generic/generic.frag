#include "engine/api.glsl"
#include "engine/materials.glsl"

layout(location = 0) out vec4 frag_colour;
layout(location = 1) out vec4 frag_normal;

in vec3 var_position;
in vec3 var_normal;
in vec2 var_texture;
in mat3 var_tbn;

uniform sampler2D u_diffuse_map;
uniform sampler2D u_normal_map;
uniform vec4 u_diffuse;

void main() {

    // enable deferred path
    // if (true) {
    //    vec3 normal = var_normal;
    //    if (modus_material_has_normal_map(u_material_flags)) {
    //       normal = texture(u_normal_map, var_texture).rgb;
    //       normal = normalize(normal * 2.0 - 1.0);
    //       normal = var_tbn * normal;
    //    }

    //    vec4 diffuse = u_diffuse;
    //    if (modus_material_has_diffuse_map(u_material_flags)) {
    //        diffuse = texture(u_diffuse_map, var_texture);
    //    }

   //     frag_colour = diffuse;
    //    frag_normal = vec4(normal, 0.f);
    //} else {
        // hard coded global light
        vec3 light_dir = vec3(1);
        vec3 normal = var_normal;
        if (modus_material_has_normal_map(u_material_flags)) {
           normal = texture(u_normal_map, var_texture).rgb;
           normal = normalize(normal * 2.0 - 1.0);
           light_dir = var_tbn * light_dir;
        }

        float light_contribution = max(dot(normal, light_dir), 0.0);
        vec4 diffuse = u_diffuse;
        if (modus_material_has_diffuse_map(u_material_flags)) {
            diffuse = texture(u_diffuse_map, var_texture);
        }

        const vec4 ambient_light = vec4(0.1, 0.1,0.1, 1);
        frag_colour = (ambient_light + light_contribution) * diffuse;
    //}
}

