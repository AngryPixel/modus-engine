#include "engine/api.glsl"
#include "engine/globals.glsl"

#include "engine/lighting/normal_mapping.glsl"

layout(location = MODUS_VERTEX_SLOT) in vec3 POSITION;
layout(location = MODUS_NORMAL_SLOT) in vec3 NORMAL;
layout(location = MODUS_TEXTURE_SLOT) in vec2 TEXTURE;
layout(location = MODUS_TANGENT_SLOT) in vec3 TANGENT;
layout(location = MODUS_BINORMAL_SLOT) in vec3 BINORMAL;

uniform mat4 u_mvp;
uniform mat4 u_model_matrix;
uniform mat3 u_normal_matrix;

out vec3 var_position;
out vec3 var_normal;
out vec2 var_texture;
out mat3 var_tbn;

void main() {
    var_normal = normalize(u_normal_matrix * NORMAL);
    var_position = vec3(u_model_matrix * vec4(POSITION, 1));
    var_texture = TEXTURE;
    var_tbn = generate_tbn_matrix(u_model_matrix, NORMAL, TANGENT, BINORMAL);

    gl_Position = u_mvp * vec4(POSITION, 1);
}

