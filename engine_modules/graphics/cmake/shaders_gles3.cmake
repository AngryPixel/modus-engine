#
# GLES3 Asset conversion utils
#

if (NOT TARGET modus_glsl_include)
    message(FATAL_ERROR "Could not find modus_glsl_include target")
endif()

if (NOT TARGET modus_gpuprog_reflect)
    message(FATAL_ERROR "Could not find modus_gpuprog_reflect")
endif()

find_program(glsl_validator_bin glslangValidator)

function(modus_gles3_process_shader_include input output)
    cmake_parse_arguments(arg ""
        "DEP_FILE"
        "INCLUDE_PATHS" ${ARGN})

    set(validator_command)
    if (glsl_validator_bin)
        set(validator_command
            COMMAND ${glsl_validator_bin} "${output}"
        )
    endif()

    set(dep_file_args)
    set(dep_command_args)
    if (CMAKE_GENERATOR STREQUAL "Ninja" OR CMAKE_GENERATOR STREQUAL "Ninja Ninja Multi-Config")
        set(dep_file_args DEPFILE "${arg_DEP_FILE}")

        file(RELATIVE_PATH dep_file "${${CMAKE_PROJECT_NAME}_BINARY_DIR}" "${output}")
        set(dep_command_args
            --dep-output-file "${arg_DEP_FILE}"
            --dep-file "${dep_file}"
        )
        get_filename_component(dep_file_dir ${arg_DEP_FILE} DIRECTORY)
        file(MAKE_DIRECTORY ${dep_file_dir})
    endif()

    add_custom_command(OUTPUT ${output}
        DEPENDS ${input}
        COMMAND modus_glsl_include
            ${dep_command_args}
            -i ${arg_INCLUDE_PATHS}
            -o "${output}"
            "${input}"
        ${validator_command}
        COMMENT "Processing includes for GLSL file: ${input}"
        WORKING_DIRECTORY
        ${dep_file_args}
   )
endfunction()

set(__total_shader_list)
function(modus_make_gl_program target)

    cmake_parse_arguments(arg ""
        "OUTPUT;VERTEX;FRAGMENT;CPP_HEADER;CPP_NAMESPACE;CPP_NAMESPACE_NAME;GEN_DIR"
        "SHADERS;INCLUDE_PATHS" ${ARGN})

    if (NOT arg_GEN_DIR)
        message(FATAL_ERROR "No gen directory provided")
    endif()

    get_filename_component(output_dir ${arg_OUTPUT} DIRECTORY)
    get_filename_component(output_name_noext ${arg_OUTPUT} NAME_WLE)

    set(GEN_DIR "${arg_GEN_DIR}/${output_name_noext}")

    get_target_property(target_bin_dir ${target} BINARY_DIR)

    set(arg_CPP_HEADER "${target_bin_dir}/gles3gen/${arg_CPP_HEADER}")
    target_include_directories(${target} PRIVATE "${target_bin_dir}/gles3gen")

    set(program_args "")
    set(reflect_args)
    set(dependencies "")
    set(shader_type_list "VERTEX" "FRAGMENT")
    foreach(shader_type IN LISTS shader_type_list)
        if (NOT arg_${shader_type})
            continue()
        endif()

        set(shader ${arg_${shader_type}})
        if (NOT IS_ABSOLUTE shader)
            set(shader "${CMAKE_CURRENT_SOURCE_DIR}/${shader}")
        endif()
        get_filename_component(shader_name ${shader} NAME)
        set(output_shader_dir "${GEN_DIR}")
        set(shader_processed "${output_shader_dir}/${shader_name}")
        if ("${shader_type}" STREQUAL "VERTEX")
            list(APPEND program_args --vert ${shader_processed})
            list(APPEND reflect_args --vert ${shader_processed})
        elseif("${shader_type}" STREQUAL "FRAGMENT")
            list(APPEND program_args --frag ${shader_processed})
            list(APPEND reflect_args --frag ${shader_processed})
        else()
            message(FATAL_ERROR "Unknown shader type")
        endif()

        list(APPEND dependencies ${shader_processed})
        modus_gles3_process_shader_include(${shader} ${shader_processed}
            INCLUDE_PATHS ${arg_INCLUDE_PATHS}
            DEP_FILE "${CMAKE_CURRENT_BINARY_DIR}/${output_name_noext}/${shader_name}.dep"
        )
    endforeach()

    get_filename_component(output_name ${arg_OUTPUT} NAME)
    set(shader_constant "${GEN_DIR}/${output_name}.constants.json")

    set(cpp_args "")
    set(cpp_output "")
    set(cpp_copy_arg)
    if (arg_CPP_HEADER)
        get_filename_component(header_name "${arg_CPP_HEADER}" NAME)
        list(APPEND cpp_args "--cpp-header" "${arg_CPP_HEADER}.tmp")
        if (arg_CPP_NAMESPACE)
            list(APPEND cpp_args "--cpp-namespace" "${arg_CPP_NAMESPACE}")
        endif()
        if (arg_CPP_NAMESPACE_NAME)
            list(APPEND cpp_args "--cpp-namespace-name" "${arg_CPP_NAMESPACE_NAME}")
        endif()
        set(cpp_copy_arg
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${arg_CPP_HEADER}.tmp"
                "${arg_CPP_HEADER}"
        )
    endif()

    add_custom_command(OUTPUT "${arg_CPP_HEADER}" "${shader_constant}"
        DEPENDS ${dependencies}
        COMMAND modus_gpuprog_reflect
            -o ${shader_constant}
            ${reflect_args}
            ${cpp_args}
            ${cpp_copy_arg}
    )

    add_custom_target(${target}_shader_${output_name_noext}
        DEPENDS ${arg_CPP_HEADER} ${shader_constant}
    )
    add_dependencies(${target} ${target}_shader_${output_name_noext})

    set(package_json_str)
    string(APPEND package_json_str "{\n")
    string(APPEND package_json_str "    \"assets\": [{\n")
    string(APPEND package_json_str "        \"output\": \"${arg_OUTPUT}\",\n")
    string(APPEND package_json_str "        \"deps\": [\n")
    set(is_first TRUE)
    foreach(dep IN LISTS dependencies shader_constant)
        file(RELATIVE_PATH dep_relative "${GEN_DIR}" "${dep}")
        if (NOT is_first)
            string(APPEND package_json_str ",\n")
        else()
            set(is_first FALSE)
        endif()
        string(APPEND package_json_str "            \"${dep_relative}\"")
    endforeach()
    string(APPEND package_json_str "\n")
    string(APPEND package_json_str "        ],\n")
    string(APPEND package_json_str "        \"process\": {\n")
    string(APPEND package_json_str "            \"bin\": \"modus_mk_gpu_program\",\n")
    string(APPEND package_json_str "            \"args\": [\n")
    set(is_first TRUE)
    set(main_args -t gles3 -o "%OUT%" -c "${shader_constant}")
    foreach(arg IN LISTS main_args  program_args)
        set(arg_rel ${arg})
        if (EXISTS arg)
            file(RELATIVE_PATH arg_rel "${GEN_DIR}" "${arg}")
        endif()
        if (NOT is_first)
            string(APPEND package_json_str ",\n")
        else()
            set(is_first FALSE)
        endif()
        string(APPEND package_json_str "                \"${arg_rel}\"")
    endforeach()
    string(APPEND package_json_str "\n")
    string(APPEND package_json_str "            ]\n")
    string(APPEND package_json_str "        }\n")
    string(APPEND package_json_str "    }]\n")
    string(APPEND package_json_str "}\n")

    file(WRITE "${GEN_DIR}/package.json" "${package_json_str}")
    list(APPEND __total_shader_list "${output_name_noext}")
    set(__total_shader_list ${__total_shader_list} CACHE STRING ""FORCE)

endfunction()


function(modus_begin_gl_program_list)
    set(__total_shader_list "" CACHE STRING "" FORCE)
endfunction()

function(modus_end_gl_program_list output_dir)
    list(JOIN __total_shader_list "\",\n      \"" subdirs)
    set(txt "{\n")
    string(APPEND txt "    \"subdirs\": [\n")
    string(APPEND txt "      \"")
    string(APPEND txt ${subdirs})
    string(APPEND txt "\"\n")
    string(APPEND txt "    ]\n}")
    file(WRITE "${output_dir}/package.json" ${txt})
endfunction()
