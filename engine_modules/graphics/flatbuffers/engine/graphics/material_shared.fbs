include "core/guid.fbs";
include "math/math.fbs";

namespace modus.engine.graphics.fbs;

table BoolProperty {
    v: bool = false;
}

table UIntProperty {
    v: uint = 0;
}

table FloatProperty {
    v: float = 0;
}

table Vec2Property {
    v: modus.math.fbs.Vec2 (required);
}

table Vec3Property {
    v: modus.math.fbs.Vec3 (required);
}

table Vec4Property {
    v: modus.math.fbs.Vec4 (required);
}

table ImageProperty {
    v: string (required);
}

union PropertyValue {
    bool: BoolProperty,
    u32: UIntProperty,
    f32: FloatProperty,
    vec2: Vec2Property,
    vec3: Vec3Property,
    vec4: Vec4Property,
    image: ImageProperty
}

table Property {
    name: string (required);
    value: PropertyValue (required);
}

table Material {
    instance_guid: string (required);
    name: string (required);
    properties: [Property];
}
