/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <app/app.hpp>
#include <app/window.hpp>
#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/mesh_generator.hpp>
#include <engine/igame.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_config.hpp>
#include <engine/modules/module_filesystem.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <os/path.hpp>
#include <threed/compositor.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/threed.hpp>

namespace modus::engine {

static const StringSlice kGraphicsWindowWidthPath = "/engine/graphics/window_width";
static const StringSlice kGraphicsWindowHeightPath = "/engine/graphics/window_height";

static const StringSlice kGraphicsFramebufferWidthPath = "/engine/graphics/fb_width";
static const StringSlice kGraphicsFramebufferHeightPath = "/engine/graphics/fb_height";

MODUS_ENGINE_IMPL_MODULE(ModuleGraphics, "684d3d8d-c1b4-4af2-a891-9a77453d2c7d");

ModuleGraphics::ModuleGraphics()
    : Module(module_guid<ModuleGraphics>(), "Graphics", ModuleUpdateCategory::Graphics),
      m_device(),
      m_compositor(),
      m_mesh_asset_factory(m_mesh_db),
      m_mesh_atlas_asset_factory(m_mesh_db),
      m_material_asset_factory(m_material_db) {}

void ModuleGraphics::set_pipeline(NotMyPtr<graphics::IPipeline> pipeline) {
    m_pipeline = pipeline;
}

void ModuleGraphics::unset_pipeline() {
    m_pipeline = NotMyPtr<graphics::IPipeline>();
}

Result<> ModuleGraphics::initialize(Engine& engine) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ModuleGraphics::initialize");
    MODUS_LOGV("Initializing Graphics");

    auto app_module = engine.module<ModuleApp>();
    if (!app_module) {
        MODUS_LOGE("Expected App module {} is not in module list {}", module_guid<ModuleApp>());
        return Error<>();
    }

    auto asset_module = engine.module<ModuleAssets>();
    if (!asset_module) {
        MODUS_LOGE("Expected Assets module {} is not in module list {}",
                   module_guid<ModuleAssets>());
        return Error<>();
    }

    auto config_module = engine.module<ModuleConfig>();
    if (!config_module) {
        MODUS_LOGE("Expected Config module {} is not in module list {}",
                   module_guid<ModuleConfig>());
        return Error<>();
    }

    // Register Asset context
    assets::AssetLoader& asset_loader = asset_module->loader();
    if (!asset_loader.register_factory(&m_image_asset_factory)) {
        MODUS_LOGE("Failed to register image asset factory");
        return Error<>();
    }

    if (!asset_loader.register_factory(&m_font_asset_factory)) {
        MODUS_LOGE("Failed to register font asset factory");
        return Error<>();
    }

    if (!asset_loader.register_factory(&m_gpuprog_asset_factory)) {
        MODUS_LOGE("Failed to register gpu program asset factory");
        return Error<>();
    }

    if (!asset_loader.register_factory(&m_mesh_asset_factory)) {
        MODUS_LOGE("Failed to register mesh asset context");
        return Error<>();
    }

    if (!asset_loader.register_factory(&m_mesh_atlas_asset_factory)) {
        MODUS_LOGE("Failed to register mesh asset context");
        return Error<>();
    }

    if (!asset_loader.register_factory(&m_material_asset_factory)) {
        MODUS_LOGE("Failed to register mesh asset factory");
        return Error<>();
    }

    if (!asset_loader.register_factory(&m_model_asset_factory)) {
        MODUS_LOGE("Failed to register model asset factory");
        return Error<>();
    }

    {
        // Mount font dir
        String font_path;
        os::Path::join_inplace(font_path, MODUS_DEV_DATA_DIR, "fonts");
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "font";
        vfs_params.read_only = true;
        vfs_params.path = font_path;

        if (!engine.module<ModuleFileSystem>()->vfs().mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount font path: {}", font_path);
            return Error<>();
        }

        MODUS_LOGD("Mounted Font path: {}", font_path);
    }

    {
        // Mount model/mesh dir
        String model_path = os::Path::join(MODUS_DEV_DATA_DIR, "models");
        vfs::VFSMountParams vfs_params;
        vfs_params.tag = "mesh";
        vfs_params.read_only = true;
        vfs_params.path = model_path;

        if (!engine.module<ModuleFileSystem>()->vfs().mount_path(vfs_params)) {
            MODUS_LOGE("Failed to mount model path: {}", model_path);
            return Error<>();
        }

        MODUS_LOGD("Mounted model path: {}", model_path);
    }

    config::Config& cfg = config_module->cfg();

    // Read config file settings
    {
        auto window_width_query = cfg.query_i32(kGraphicsWindowWidthPath);
        if (window_width_query) {
            m_settings.window_width = window_width_query.value();
        }

        auto window_height_query = cfg.query_i32(kGraphicsWindowHeightPath);
        if (window_height_query) {
            m_settings.window_height = window_height_query.value();
        }

        auto fb_width_query = cfg.query_i32(kGraphicsFramebufferWidthPath);
        if (fb_width_query) {
            m_settings.framebuffer_width = fb_width_query.value();
        }

        auto fb_height_query = cfg.query_i32(kGraphicsFramebufferHeightPath);
        if (fb_height_query) {
            m_settings.framebuffer_height = fb_height_query.value();
        }
    }

    // Create Window
    // TODO: Read from configuration
    app::WindowCreateParams window_params;
    window_params.title = engine.game().name();
    window_params.height = m_settings.window_height;
    window_params.width = m_settings.window_width;
    window_params.framebuffer.srgb = false;
#if defined(MODUS_ENGINE_TARGET_MOBILE)
    window_params.framebuffer.depth_bits = 16;
    window_params.framebuffer.red_bits = 5;
    window_params.framebuffer.green_bits = 6;
    window_params.framebuffer.blue_bits = 5;
#else
    window_params.framebuffer.depth_bits = 24;
    window_params.framebuffer.red_bits = 8;
    window_params.framebuffer.green_bits = 8;
    window_params.framebuffer.blue_bits = 8;
#endif
#if defined(MODUS_DEBUG)
    window_params.debug = true;
#endif
    window_params.fullscreen = app::WindowFullscreenMode::Borderless;

    app::App& app = app_module->app();

    auto window_create_result = app.create_window(window_params);
    if (!window_create_result) {
        MODUS_LOGE("Graphics: Failed to create window: {}", window_create_result.error());
        return Error<>();
    }

    m_window = window_create_result.release_value();

    m_device = app.threed_instance().create_device();
    if (!m_device->initialize()) {
        MODUS_LOGE("Graphics: Failed to initialize Threed device");
        return Error<>();
    }
    m_threed_instance = &app.threed_instance();

    const threed::DeviceLimits limits = m_device->device_limits();
    MODUS_LOGD("DeviceLimits:");
    MODUS_LOGD("    Max texture size px     : {}", limits.max_texture_size_px);
    MODUS_LOGD("    Max constant buffer size: {}", limits.max_constant_buffer_size);
    MODUS_LOGD("    Compressed ASTC         : {}", limits.compressed_texture_support.astc);
    MODUS_LOGD("    Compressed DXT1         : {}", limits.compressed_texture_support.dxt1);
    MODUS_LOGD("    Compressed ETC1         : {}", limits.compressed_texture_support.etc1);
    MODUS_LOGD("    Compressed ETC2         : {}", limits.compressed_texture_support.etc2);
    MODUS_LOGD("    Compressed S3TC         : {}", limits.compressed_texture_support.s3tc);
    MODUS_LOGD("    Compressed S3TC_SRGB    : {}", limits.compressed_texture_support.s3tc_srgb);

    Result<NotMyPtr<threed::Compositor>, void> create_compositor_result =
        m_window->create_compositor(*m_device, false);

    if (!create_compositor_result) {
        MODUS_LOGE("Graphics: Failed to create compositor from window");
        return Error<>();
    }

    m_compositor = create_compositor_result.release_value();

    // This causes weird behaviour when set during rendering
    // m_compositor->set_vsync(false);

    if (!m_debug_drawer.initialize(engine)) {
        MODUS_LOGE("Graphics: Failed to init debug drawer");
        return Error<>();
    }

    if (!m_default_frame_globals.initialize(engine)) {
        MODUS_LOGE("Graphics: Failed to initialize default frame globals");
        return Error<>();
    }

    if (!m_frame_globals.push(&m_default_frame_globals)) {
        MODUS_LOGE("Graphics: Failed to push default frame globals");
        return Error<>();
    }

    if (!m_particle_registry.initialize()) {
        MODUS_LOGE("Graphics: Failed to init particle registry");
        return Error<>();
    }

    if (!m_animation_cache.initialize(device())) {
        MODUS_LOGE("Graphics: Failed to initialize animation buffer cache");
        return Error<>();
    }

    MODUS_LOGV("Graphics Initialized");
    return Ok<>();
}

Result<> ModuleGraphics::shutdown(Engine& engine) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ModuleGraphics::shutdown");
    MODUS_LOGV("Shutting down Graphics");

    m_frame_globals.clear();

    m_animation_cache.shutdown(device());

    m_particle_registry.shutdown();

    if (!m_default_frame_globals.shutdown(engine)) {
        MODUS_LOGW("Failed to shutdown default frame globals");
    }

    if (!m_debug_drawer.shutdown(engine)) {
        MODUS_LOGW("Failed to shutdown debug drawer");
    }

    m_material_db.clear();

    auto asset_module = engine.module<ModuleAssets>();
    if (asset_module) {
        assets::AssetLoader& asset_loader = asset_module->loader();
        asset_loader.unregister_factory(engine, &m_image_asset_factory);
        asset_loader.unregister_factory(engine, &m_font_asset_factory);
        asset_loader.unregister_factory(engine, &m_gpuprog_asset_factory);
        asset_loader.unregister_factory(engine, &m_mesh_asset_factory);
        asset_loader.unregister_factory(engine, &m_mesh_atlas_asset_factory);
        asset_loader.unregister_factory(engine, &m_material_asset_factory);
        asset_loader.unregister_factory(engine, &m_model_asset_factory);
    }

    if (m_compositor) {
        const i32 window_width = m_compositor->width();
        const i32 window_height = m_compositor->height();

        config::Config& cfg = engine.module<ModuleConfig>()->cfg();
        if (window_width > 0 && window_width != m_settings.window_width) {
            if (!cfg.store_i32(kGraphicsWindowWidthPath, window_width, true)) {
                MODUS_LOGE("Failed to store graphics settings 'window_width'");
            }
        }
        if (window_height > 0 && window_height != m_settings.window_height) {
            if (!cfg.store_i32(kGraphicsWindowHeightPath, window_height, true)) {
                MODUS_LOGE("Failed to store graphics settings 'window_height'");
            }
        }

        if (!cfg.store_i32(kGraphicsFramebufferWidthPath, m_settings.framebuffer_width, true)) {
            MODUS_LOGE("Failed to store graphics settings 'framebuffer_width'");
        }

        if (!cfg.store_i32(kGraphicsFramebufferHeightPath, m_settings.framebuffer_height, true)) {
            MODUS_LOGE("Failed to store graphics settings 'framebuffer_height'");
        }
    }
    m_mesh_db.clear();
    m_compositor = NotMyPtr<threed::Compositor>();
    if (m_device) {
        m_device->shutdown();
        m_device.reset();
    }
    m_threed_instance.reset();
    MODUS_LOGV("Graphics Shutdown complete");
    return Ok<>();
}

void ModuleGraphics::tick(Engine& engine) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ModuleGraphics::tick")
    if (m_compositor->requires_update()) {
        MODUS_PROFILE_GRAPHICS_BLOCK("Compositor Update");
        MODUS_LOGD("Graphics: Compositor Requires Update");
        auto compositor_update_result = m_compositor->update(*m_device);
        if (!compositor_update_result) {
            MODUS_LOGE("Graphics: Failed to update compositor: {}",
                       compositor_update_result.error());
            return;
        }

        if (m_pipeline) {
            m_pipeline->on_compositor_update(engine, *m_compositor);
        }
    }

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("device::begin_frame");
        m_device->begin_frame();
    }

    NotMyPtr<const graphics::Camera> world_camera_ptr;
    // update frame defaults
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Update Frame Globals");
        auto gameplay_module = engine.module<engine::ModuleGameplay>();
        NotMyPtr<gameplay::GameWorld> world = gameplay_module->world();
        if (world) {
            auto r_camera = world->entity_manager().component<engine::graphics::CameraComponent>(
                world->main_camera());
            if (r_camera) {
                const engine::graphics::Camera& world_camera = (*r_camera)->m_camera;
                const graphics::FrameGlobalsParams params = {
                    world_camera, u32(m_compositor->width()), u32(m_compositor->height())};
                if (!m_default_frame_globals.update(engine, params)) {
                    MODUS_LOGE("Failed to update default frame globals");
                }
                world_camera_ptr = &world_camera;
            }
        }
    }
    threed::PipelinePtr threed_pipeline = m_device->allocate_pipeline("Graphics");

    if (m_pipeline) {
        {
            MODUS_PROFILE_GRAPHICS_BLOCK("build pipeline");
            if (!m_pipeline->build(engine, threed_pipeline)) {
                MODUS_LOGE("Failed to build pipeline");
            }
        }
    }

    // Debug draw commands
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("device::debug_draw");
        graphics::DebugPipelineState state;
        state.camera = world_camera_ptr;
        state.framebuffer.handle = m_compositor->frame_buffer();
        state.framebuffer.width = m_compositor->width();
        state.framebuffer.height = m_compositor->height();

        m_debug_drawer.submit_passes(engine, state, threed_pipeline);
    }

    if (m_ui_renderer) {
        MODUS_PROFILE_GRAPHICS_BLOCK("ui render");
        m_ui_renderer->render(engine, *threed_pipeline);
    }

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("submit pipeline");
        m_device->execute_pipeline(*threed_pipeline);
    }

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("device::end_frame");
        m_device->end_frame();
    }
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Swap Buffers");
        m_compositor->swap_buffers();
    }
}

Slice<GID> ModuleGraphics::module_dependencies() const {
    static const GID deps[] = {module_guid<ModuleApp>(),        module_guid<ModuleAssets>(),
                               module_guid<ModuleGameplay>(),   module_guid<ModuleConfig>(),
                               module_guid<ModuleFileSystem>(), module_guid<ModuleAnimation>()};
    return Slice(deps);
}

Optional<NotMyPtr<const graphics::FrameGlobals>> ModuleGraphics::frame_globals() const {
    auto top = m_frame_globals.top();
    if (!top) {
        return NotMyPtr<const graphics::FrameGlobals>();
    }
    return **top;
}

Result<> ModuleGraphics::push_frame_globals(NotMyPtr<const graphics::FrameGlobals> globals) {
    modus_assert(globals);
    return m_frame_globals.push(globals);
}

void ModuleGraphics::pop_frame_globals() {}

}    // namespace modus::engine
