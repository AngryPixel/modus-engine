/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/model_asset.hpp>
// clang-format on

#include <engine/graphics/model_generated.h>
#include <engine/engine.hpp>
#include <engine/graphics/material_asset.hpp>
#include <engine/graphics/mesh_asset.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/physics/collision_mesh_asset.hpp>
#include <engine/assets/asset_waiter.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::ModelAsset)

namespace modus::engine::graphics {

ModelAsset::ModelAsset(const assets::AssetHandleV2 mesh_asset,
                       const MeshHandle mesh_handle,
                       const MaterialArray& materials,
                       Optional<assets::AssetHandleV2> collision_mesh_asset)
    : assets::AssetBase(assets::AssetTraits<ModelAsset>::type_id()),
      m_material_assets(materials),
      m_mesh_asset(mesh_asset),
      m_mesh_handle(mesh_handle),
      m_collision_mesh_asset(collision_mesh_asset.value_or(assets::AssetHandleV2())) {}

ModelAssetFactory::ModelAssetFactory() : factory_type(32) {}

StringSlice ModelAssetFactory::file_extension() const {
    return "model";
}

class ModelAssetData final : public assets::RamAssetData {
   public:
    assets::AssetGroupWaiter m_asset_waiter;
    const fbs::Model* m_model = nullptr;

    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        if (!assets::RamAssetData::load(reader)) {
            return Error<>();
        }
        const ByteSlice bytes = this->bytes();

        flatbuffers::Verifier verifier(bytes.data(), bytes.size());
        if (!fbs::VerifyModelBuffer(verifier)) {
            MODUS_LOGE("ModelAssetFactory: Data is not a model");
            return Error<>();
        }

        const fbs::Model* fbs_model = fbs::GetModel(bytes.data());
        m_model = fbs_model;

        // load mesh;

        auto fbs_mesh_path = fbs_model->mesh();
        if (fbs_mesh_path == nullptr) {
            MODUS_LOGE("ModelAssetFactory: No mesh path specified");
            return Error<>();
        }

        // load materials first
        if (auto fbs_materials = fbs_model->materials(); fbs_materials != nullptr) {
            u32 material_index = 0;
            if (fbs_materials->size() > ModelAsset::kMaxMaterialHandles) {
                MODUS_LOGE(
                    "ModelAssetFactory: Model has more materials than we can "
                    "handle {:02}, max={:02}",
                    fbs_materials->size(), ModelAsset::kMaxMaterialHandles);
                return Error<>();
            }
            for (const auto& fbs_material : *fbs_materials) {
                if (fbs_material == nullptr) {
                    MODUS_LOGE("ModelAssetFactory: Material [{:02}] has not path", material_index);
                    return Error<>();
                }

                const StringSlice asset_path =
                    StringSlice(fbs_material->c_str(), fbs_material->size());
                m_asset_waiter.add(asset_path);
            }
        }

        auto fbs_mesh_atlas_path = fbs_model->mesh_atlas();
        const StringSlice asset_path =
            fbs_mesh_atlas_path != nullptr
                ? StringSlice(fbs_mesh_atlas_path->c_str(), fbs_mesh_atlas_path->size())
                : StringSlice(fbs_mesh_path->c_str(), fbs_mesh_path->size());
        m_asset_waiter.add(asset_path);

        if (fbs_model->collision_shape_type() == fbs::CollisionShapeType::Mesh) {
            auto cmesh_asset_path = fbs_model->collision_mesh();
            if (cmesh_asset_path == nullptr) {
                MODUS_LOGE(
                    "ModelAssetFactory: Model specified collision shape type as "
                    "mesh, but no mesh path was provided");
                return Error<>();
            }
            const StringSlice cmesh_asset_path_slice =
                StringSlice(cmesh_asset_path->c_str(), cmesh_asset_path->size());
            m_asset_waiter.add(cmesh_asset_path_slice);
        }
        return Ok(assets::AssetLoadResult::HasDependencies);
    }

    Result<bool> eval_dependencies(Engine& engine, assets::AssetLoader& loader) override {
        const assets::AssetGroupWaiterState state = m_asset_waiter.update(engine, loader);
        if (state.error_count != 0) {
            MODUS_LOGE("ModelAsset: One of the images failed to load");
            return Error<>();
        }
        return Ok<bool>(state.finished_count == m_asset_waiter.count());
    }
};
std::unique_ptr<assets::AssetData> ModelAssetFactory::create_asset_data() {
    return modus::make_unique<ModelAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> ModelAssetFactory::create(Engine& engine,
                                                              assets::AssetData& data) {
    const ModelAssetData& material_data = static_cast<const ModelAssetData&>(data);

    const fbs::Model* fbs_model = material_data.m_model;
    auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();

    assets::AssetHandleV2 mesh_asset;
    ModelAsset::MaterialArray material_assets;

    // load materials first
    if (auto fbs_materials = fbs_model->materials(); fbs_materials != nullptr) {
        u32 material_index = 0;
        for (const auto& fbs_material : *fbs_materials) {
            const StringSlice asset_path = StringSlice(fbs_material->c_str(), fbs_material->size());
            auto r_asset = asset_loader.resolve_by_path_typed<MaterialAsset>(asset_path);
            if (!r_asset) {
                MODUS_LOGE("ModelAssetFactory: Material did not load [{:02}]: {}", material_index,
                           asset_path);
                return Error<>();
            }
            if (!material_assets.push_back((*r_asset)->asset_handle())) {
                MODUS_LOGE("ModelAssetFactory: Failed to inset Material [{:02}]: {}",
                           material_index, asset_path);
                return Error<>();
            }
            material_index++;
        }
    }

    const auto fbs_mesh_atlas_path = fbs_model->mesh_atlas();
    const auto fbs_mesh_path = fbs_model->mesh();
    const StringSlice asset_path = StringSlice(fbs_mesh_path->c_str(), fbs_mesh_path->size());

    MeshHandle mesh_handle;
    if (fbs_mesh_atlas_path != nullptr) {
        const StringSlice atlas_path =
            StringSlice(fbs_mesh_atlas_path->c_str(), fbs_mesh_atlas_path->size());
        auto r_mesh_atlas = asset_loader.resolve_by_path_typed<MeshAtlasAsset>(atlas_path);
        if (!r_mesh_atlas) {
            MODUS_LOGE("ModelAssetFactory: MeshAtlas '{}' was not loaded ", atlas_path);
            return Error<>();
        }

        for (const auto& mesh_entry : (*r_mesh_atlas)->m_meshes) {
            if (StringSlice(mesh_entry.name) == asset_path) {
                mesh_handle = mesh_entry.mesh_handle;
                break;
            }
        }
        if (!mesh_handle) {
            MODUS_LOGE("ModelAssetFactory: Could not find mesh {} in mesh atlas {}", asset_path,
                       atlas_path);
            return Error<>();
        }
    } else {
        auto r_mesh_asset = asset_loader.resolve_by_path_typed<MeshAsset>(asset_path);
        if (!r_mesh_asset) {
            MODUS_LOGE("ModelAssetFactory: Mesh '{}' was not loaded ", asset_path);
            return Error<>();
        }
        mesh_asset = (*r_mesh_asset)->asset_handle();
        mesh_handle = (*r_mesh_asset)->m_mesh_handle;
    }

    Optional<assets::AssetHandleV2> collision_mesh_asset;
    if (fbs_model->collision_shape_type() == fbs::CollisionShapeType::Mesh) {
        auto cmesh_asset_path = fbs_model->collision_mesh();
        const StringSlice cmesh_asset_path_slice =
            StringSlice(cmesh_asset_path->c_str(), cmesh_asset_path->size());
        auto r_collision_shape =
            asset_loader.resolve_by_path_typed<physics::CollisionMeshAsset>(cmesh_asset_path_slice);
        if (!r_collision_shape) {
            MODUS_LOGE("ModelAssetFactory: Collision mesh did not load {}", asset_path);
            return Error<>();
        }
        collision_mesh_asset = (*r_collision_shape)->asset_handle();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(
        m_asset_pool.construct(mesh_asset, mesh_handle, material_assets, collision_mesh_asset));
}

Result<> ModelAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    auto r_asset = assets::assetv2_cast<ModelAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    // Unload related image assets
    auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();

    asset_loader.destroy_async((*r_asset)->m_mesh_asset);
    const auto& material_assets = (*r_asset)->m_material_assets;
    for (auto& material_asset : material_assets) {
        asset_loader.destroy_async(material_asset);
    }

    if ((*r_asset)->m_collision_mesh_asset.is_valid()) {
        asset_loader.destroy_async((*r_asset)->m_collision_mesh_asset);
    }
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}
}    // namespace modus::engine::graphics
