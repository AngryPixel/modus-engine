/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/graphics/font_generated.h>
#include <pch.h>

#include <engine/graphics/font.hpp>

namespace modus::engine::graphics {

Result<Font, void> Font::from_stream(ISeekableReader& stream) {
    RamStream<GraphicsAllocator> ram_stream;
    if (!ram_stream.populate_from_stream(stream)) {
        MODUS_LOGE("Failed to read font from stream");
        return Error<>();
    }

    const ByteSlice font_slice = ram_stream.as_bytes();
    flatbuffers::Verifier v(font_slice.data(), font_slice.size());
    if (!fbs::VerifyFontBuffer(v)) {
        MODUS_LOGE("Content of stream is not a font file!");
        return Error<>();
    }

    auto font = fbs::GetFont(font_slice.data());

    if (font->header() == nullptr) {
        MODUS_LOGE("Font does not have a header!");
        return Error<>();
    }

    if (font->header()->atlas_height_px() == 0 || font->header()->atlas_width_px() == 0) {
        MODUS_LOGE("Invalid atlas width and/or height");
        return Error<>();
    }

    const ByteSlice guid_slice(font->guid()->value()->data(), font->guid()->value()->size());
    auto r_guid = GID::from_slice(guid_slice);
    if (!r_guid) {
        MODUS_LOGE("Font contains invalid guid");
        return Error<>();
    }

    return Ok(Font(r_guid.value(), NotMyPtr<const fbs::Font>(font), std::move(ram_stream)));
}

Font::Font(const GID& guid, NotMyPtr<const fbs::Font> font, RamStream<GraphicsAllocator>&& data)
    : m_guid(guid), m_font(font), m_data(std::move(data)) {}

Result<FontAsciiGenResult, void> Font::generate_ascii_string_v2(
    SliceMut<f32>& points,
    const FontAsciiGenParams& params) const {
    if (points.size() < buffer_size_for_text_size(params.text.size())) {
        return Error<>();
    }

    FontAsciiGenResult result = {0, 0};
    f32 cur_xloc = params.x_loc;
    f32 cur_yloc = params.y_loc;
    const f32 scale = params.scale;

    const auto render_info = m_font->meta_data()->info();

    const f32 tex_padding_x = params.padding / m_font->header()->atlas_width_px();
    const f32 tex_padding_y = params.padding / m_font->header()->atlas_height_px();
    usize i = 0;
    for (usize t = 0; t < params.text.size(); t++) {
        const i32 ascii_code = params.text[t];
        const i32 index = ascii_code - 32;
        if (ascii_code < 32 || ascii_code >= 128) {
            // Invalid char
            continue;
        }
        const auto render_info_char = render_info->Get(index);
        if (ascii_code == ' ') {
            cur_xloc += f32(render_info_char->advance_x()) * scale;
            continue;
        }

        const f32 x_pos = cur_xloc + f32(render_info_char->bearing_x()) * scale;
        const f32 y_pos =
            cur_yloc - (f32(render_info_char->height() - render_info_char->bearing_y()) * scale);
        const f32 w = f32(render_info_char->width()) * scale;
        const f32 h = f32(render_info_char->height()) * scale;

        const f32 tex_left = render_info_char->tex_left() - tex_padding_x;
        const f32 tex_right = render_info_char->tex_right() + tex_padding_x;
        const f32 tex_top = render_info_char->tex_top() + tex_padding_y;
        const f32 tex_bottom = render_info_char->tex_bottom() - tex_padding_y;

        // write vertex and texcoord
        // point 1
        points[i * 24 + 0] = x_pos;
        points[i * 24 + 1] = y_pos + h;
        points[i * 24 + 2] = tex_left;
        points[i * 24 + 3] = tex_top;

        // point 2
        points[i * 24 + 4] = x_pos;
        points[i * 24 + 5] = y_pos;
        points[i * 24 + 6] = tex_left;
        points[i * 24 + 7] = tex_bottom;

        // point 3
        points[i * 24 + 8] = x_pos + w;
        points[i * 24 + 9] = y_pos;
        points[i * 24 + 10] = tex_right;
        points[i * 24 + 11] = tex_bottom;

        // point 4
        points[i * 24 + 12] = x_pos;
        points[i * 24 + 13] = y_pos + h;
        points[i * 24 + 14] = tex_left;
        points[i * 24 + 15] = tex_top;

        // point 5
        points[i * 24 + 16] = x_pos + w;
        points[i * 24 + 17] = y_pos;
        points[i * 24 + 18] = tex_right;
        points[i * 24 + 19] = tex_bottom;

        // point 6
        points[i * 24 + 20] = x_pos + w;
        points[i * 24 + 21] = y_pos + h;
        points[i * 24 + 22] = tex_right;
        points[i * 24 + 23] = tex_top;

        cur_xloc += f32(render_info_char->advance_x()) * scale;
        ++i;
        result.array_count += 6;
    }
    result.bytes_written = result.array_count * 4 * sizeof(f32);
    return Ok(result);
}

ByteSlice Font::atlas_data() const {
    return ByteSlice(m_font->data()->data(), m_font->data()->size());
}

u32 Font::atlas_width_px() const {
    auto header = m_font->header();
    if (header == nullptr) {
        return 0;
    }
    return header->atlas_width_px();
}

u32 Font::atlas_height_px() const {
    auto header = m_font->header();
    if (header == nullptr) {
        return 0;
    }
    return header->atlas_height_px();
}

}    // namespace modus::engine::graphics
