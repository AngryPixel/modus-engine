/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

//clang-format off
#include <pch.h>
//clang-format on
#include <engine/engine.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/pipeline/frame_globals.hpp>
#include <engine/graphics/pipeline/renderable.hpp>
#include <engine/modules/module_graphics.hpp>

namespace modus::engine::graphics {

u32 component_to_renderable(graphics::Vector<Renderable>& out,
                            const MeshDB& db,
                            const gameplay::TransformComponent& tc,
                            const Camera& camera,
                            const GraphicsComponent& gc) {
    MODUS_PROFILE_BLOCK_NAMED(b3, "component_to_renderable", profiler::color::Red);
    if (!gc.visible) {
        return 0;
    }

    auto r_mesh = db.get(gc.mesh_instance.mesh);
    if (!r_mesh) {
        MODUS_LOGE("ComponentToRenderable: Failed to retrieve mesh from db");
        return 0;
    }

    u32 index = 0;
    MaterialInstanceRef material;
    const f32 z_distance = gc.z_distance.value_or(
        glm::distance(camera.position(), tc.world_transform().translation()));

    for (auto& submesh : (*r_mesh)->sub_meshes) {
        if (gc.material_instance) {
            material = gc.material_instance;
        } else {
            if (submesh.material_index >= gc.mesh_instance.materials.size()) {
                MODUS_LOGE(
                    "ComponentToRenderable: Invalid Material Index on submesh "
                    "[{:02}]",
                    index);
            } else {
                material = gc.mesh_instance.materials[submesh.material_index];
            }
        }
        out.emplace_back(
            Renderable{tc.world_transform(), submesh.drawable, material, z_distance, gc.animator});
        index++;
    }
    return index;
}

u32 component_to_renderable_shadow(graphics::Vector<Renderable>& out,
                                   const MeshDB& mesh_db,
                                   const gameplay::TransformComponent& tc,
                                   const Camera& camera,
                                   const GraphicsComponent& gc) {
    MODUS_PROFILE_BLOCK_NAMED(b3, "component_to_renderable_shadow", profiler::color::Red);
    if (!gc.visible || !gc.casts_shadow) {
        return 0;
    }

    auto r_mesh = mesh_db.get(gc.mesh_instance.mesh);
    if (!r_mesh) {
        MODUS_LOGE("ComponentToRenderable: Failed to retrieve mesh from db");
        return 0;
    }

    u32 index = 0;
    MaterialInstanceRef material;
    const f32 z_distance = gc.z_distance.value_or(
        glm::distance2(camera.position(), tc.world_transform().translation()));

    for (auto& submesh : (*r_mesh)->sub_meshes) {
        if (gc.material_instance) {
            material = gc.material_instance;
        } else {
            if (submesh.material_index >= gc.mesh_instance.materials.size()) {
                MODUS_LOGE(
                    "ComponentToRenderable: Invalid Material Index on submesh "
                    "[{:02}]",
                    index);
            } else {
                material = gc.mesh_instance.materials[submesh.material_index];
            }
        }
        if (material.has_flag(kMaterialFlagTransparent)) {
            return 0;
        }
        out.emplace_back(
            Renderable{tc.world_transform(), submesh.drawable, material, z_distance, gc.animator});
        index++;
    }
    return index;
}

bool is_component_visible(const GraphicsComponent& gc, const FrustumEvaluator& evaluator) {
    MODUS_PROFILE_BLOCK_NAMED(b2, "Check Visible", profiler::color::Red);
    if (gc.tag == GraphicsComponentTag::SkyBox) {
        return true;
    }
    return evaluator.is_visible(gc.bounding_volume);
}

void generate_commands(Engine& engine,
                       NotMyPtr<const IPipeline> pipeline,
                       threed::Pass& pass,
                       const FrameGlobals& frame_globals,
                       SliceMut<Renderable> items,
                       const bool skip_sorting) {
    if (items.size() == 0) {
        return;
    }

#if 0
    // Sort by Material
    auto sort_by_material_fn =
        [](const engine::graphics::Renderable& r1,
           const engine::graphics::Renderable& r2) -> bool {
        return r1.material_instance < r2.material_instance;
    };

    if (items.size() > 2 && !skip_sorting) {
        MODUS_PROFILE_GRAPHICS_BLOCK("Sort Renderables");
        std::sort(items.begin(), items.end(), sort_by_material_fn);
    }
#else
    if (!skip_sorting) {
        MODUS_PROFILE_GRAPHICS_BLOCK("Sort Renderables");
        std::sort(items.begin(), items.end(), [](const Renderable& r1, const Renderable& r2) {
            const f32 d1 = r1.z_distance;
            const f32 d2 = r2.z_distance;
            return d1 < d2;
        });
    }

#endif

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    usize last_check = 0;

    for (usize i = 0; i < items.size();) {
        // Find items with the same material instance
        for (; i < items.size() &&
               items[last_check].material_instance.material_handle() ==
                   items[i].material_instance.material_handle() &&
               items[i].material_instance.material_instance() ==
                   items[last_check].material_instance.material_instance();
             ++i) {
        }

        auto r_material_pair = graphics_module->material_db().instance_and_material(
            items[last_check].material_instance);
        if (!r_material_pair) {
            MODUS_LOGE("Could not locate material or instance, skipping");
            continue;
        }

        SliceMut<engine::graphics::Renderable> drawables(items.data() + last_check, i - last_check);
        /* if (drawables.size() > 2) {
             MODUS_PROFILE_GRAPHICS_BLOCK("Drawable Sort");
             // Sort Objects
             auto sort_by_drawable_fn =
                 [](const engine::graphics::Renderable& r1,
                    const engine::graphics::Renderable& r2) -> bool {
                 return r1.drawable < r2.drawable;
             };
             std::sort(drawables.begin(), drawables.end(), sort_by_drawable_fn);
         }*/
        // Sort by drawable
        // TODO: Instancing is currently broken since we sort by depth and to
        // have
        // cases where we have 1 1 2 1. The third 1 will overrwrite that data
        // for the first two items causing undefined behavior.
        /*
        if (r_material_pair->second->has_flag(
                engine::graphics::kMaterialFlagSupportsInstancing)) {
            MODUS_PROFILE_GRAPHICS_BLOCK("Instanced Command");
            usize last_drawable = 0, current_drawable = 0;
            while (current_drawable < drawables.size()) {
                while (current_drawable < drawables.size() &&
                       drawables[current_drawable].drawable ==
                           drawables[last_drawable].drawable) {
                    ++current_drawable;
                }

                const Slice<engine::graphics::Renderable> instance_slice =
                    drawables.sub_slice(last_drawable,
                                        current_drawable - last_drawable);
                threed::Command* command = device.allocate_command(pass);
                command->drawable = drawables[last_drawable].drawable;
                engine::graphics::MaterialInstancedApplyParams params = {
                    instance_slice, *r_material_pair->second, frame_globals,
                    *command};
                r_material_pair->first->apply_instanced(engine, params);
                last_drawable = current_drawable;
            }*/
        //} else {
        MODUS_PROFILE_GRAPHICS_BLOCK("NonIstancedCommand");
        for (const engine::graphics::Renderable& renderable : drawables) {
            threed::Command* command = device.allocate_command(pass);
            const glm::mat4 renderable_transform = renderable.world_transform.to_matrix();
            command->drawable = renderable.drawable;
            engine::graphics::MaterialApplyParams params = {pipeline,      *r_material_pair->second,
                                                            frame_globals, renderable_transform,
                                                            *command,      renderable.animator};
            r_material_pair->first->apply(engine, params);
        }
        //}
        last_check = i;
    }
}

}    // namespace modus::engine::graphics
