/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/pipeline/frame_globals.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/modules/module_graphics.hpp>

#include <engine/engine.hpp>
#include <threed/device.hpp>
#include <threed/buffer.hpp>
#include <threed/program_gen/shader_generator.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <threed/program_gen/shader_snippet_db.hpp>

namespace modus::engine::graphics {

static constexpr const char* kFrameGlobalsGLSnippet =
    R"R(vec3 get_camera_position() {
    return vec3(u_inv_view_matrix[3][0], u_inv_view_matrix[3][1], u_inv_view_matrix[3][2]);
}
)R";

struct FrameGlobalsInput final : public threed::dynamic::DefaultConstantBufferInput {
    FrameGlobalsInput(NotMyPtr<const threed::dynamic::DefaultConstantBufferInputBinder> binder)
        : threed::dynamic::DefaultConstantBufferInput("FrameGlobals", binder) {}
    Result<threed::dynamic::BindableInputResult> generate_code_snippet(
        threed::dynamic::GenerateParams& params) const {
        if (auto r = params.writer->write_exactly(StringSlice(kFrameGlobalsGLSnippet).as_bytes());
            !r) {
            MODUS_LOGE("Failed to write FrameGlobals code snippet");
            return Error<>();
        }
        return Ok(threed::dynamic::BindableInputResult::Generated);
    }
};

struct FrameGlobals::ShaderSnippet final : threed::dynamic::ShaderSnippet {
    threed::dynamic::DefaultConstantBufferInputBinder m_input_binder;
    FrameGlobalsInput m_input_desc;

    ShaderSnippet()
        : threed::dynamic::ShaderSnippet("engine.frame_globals"), m_input_desc(&m_input_binder) {
        threed::dynamic::BufferMemberDesc constant_desc;
        {
            constant_desc.name = "u_projection_matrix";
            constant_desc.data_type = threed::DataType::Mat4F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_view_matrix";
            constant_desc.data_type = threed::DataType::Mat4F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_projection_view_matrix";
            constant_desc.data_type = threed::DataType::Mat4F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_inv_view_matrix";
            constant_desc.data_type = threed::DataType::Mat4F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_inv_proj_view_matrix";
            constant_desc.data_type = threed::DataType::Mat4F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_tick_sec";
            constant_desc.data_type = threed::DataType::F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_elapsed_sec";
            constant_desc.data_type = threed::DataType::F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_screen_width";
            constant_desc.data_type = threed::DataType::I32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_screen_height";
            constant_desc.data_type = threed::DataType::I32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
        {
            constant_desc.name = "u_near_far";
            constant_desc.data_type = threed::DataType::Vec2F32;
            m_input_desc.m_desc.buffer_members.push_back(constant_desc);
        }
    }

    Result<> apply(threed::dynamic::ShaderVertexStage& vertex_stage) const {
        return vertex_stage.constant_buffers.push_back(&m_input_desc);
    }
    Result<> apply(threed::dynamic::ShaderFragmentStage& fragment_stage) const {
        return fragment_stage.constant_buffers.push_back(&m_input_desc);
    }
};

FrameGlobals::FrameGlobals() {
    m_snippet = modus::make_unique<ShaderSnippet>();
}

FrameGlobals::~FrameGlobals() {}

Result<> FrameGlobals::initialize(Engine& engine) {
    auto graphics_module = engine.module<ModuleGraphics>();
    if (!graphics_module->shader_snippet_db().register_snippet(m_snippet.get())) {
        MODUS_LOGE("Failed to register frame globals shader snippet");
        return Error<>();
    }

    threed::Device& device = graphics_module->device();
    // Create global constant buffer
    threed::ConstantBufferCreateParams params;
    params.data = nullptr;
    params.size = sizeof(ConstantBlock);
    params.usage = threed::BufferUsage::Stream;

    auto r_buffer_create = device.create_constant_buffer(params);
    if (!r_buffer_create) {
        MODUS_LOGE("Failed to create global constant buffer");
        return Error<>();
    }
    m_snippet->m_input_binder.m_buffer = r_buffer_create.value();

    return Ok<>();
}

Result<> FrameGlobals::shutdown(Engine& engine) {
    if (m_snippet->m_input_binder.m_buffer) {
        auto graphics_module = engine.module<ModuleGraphics>();
        threed::Device& device = graphics_module->device();
        device.destroy_constant_buffer(m_snippet->m_input_binder.m_buffer);
    }
    return Ok<>();
}

Result<> FrameGlobals::update(Engine& engine, const FrameGlobalsParams& params) {
    m_globals.mat_projection = params.camera.projection_matrix();
    m_globals.mat_view = params.camera.view_matrix();
    m_globals.mat_project_view = m_globals.mat_projection * m_globals.mat_view;
    m_globals.mat_view_inverse = glm::inverse(m_globals.mat_view);
    m_globals.mat_projection_view_inverse = glm::inverse(m_globals.mat_project_view);
    m_globals.tick_sec = engine.tick_sec().count();
    m_globals.elapsed_sec = f32(engine.elapsed_time_sec().count());
    m_globals.width = i32(params.draw_width);
    m_globals.height = i32(params.draw_height);
    m_globals.near_far =
        glm::vec2(params.camera.frustum().near_range(), params.camera.frustum().far_range());

    auto graphics_module = engine.module<ModuleGraphics>();
    threed::Device& device = graphics_module->device();
    auto r_update =
        device.update_constant_buffer<ConstantBlock>(m_snippet->m_input_binder.m_buffer, m_globals);
    modus_assert(r_update);
    if (!r_update) {
        MODUS_LOGE("FrameGlobals: Failed to update global constant buffer");
        return Error<>();
    }
    return Ok<>();
}

threed::ConstantBufferHandle FrameGlobals::constant_buffer() const {
    return m_snippet->m_input_binder.m_buffer;
}

void FrameGlobals::fill_constant_input(threed::ProgramConstantInputParam& param) {
    param.name = "FrameGlobals";
}

}    // namespace modus::engine::graphics
