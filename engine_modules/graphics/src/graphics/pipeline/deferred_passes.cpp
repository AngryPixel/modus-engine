﻿/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

//clang-format off
#include <pch.h>
//clang-format on

#include <engine/engine.hpp>
#include <engine/graphics/pipeline/deferred_passes.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <gpuprog_reflect/gl/deferred_directional_light.hpp>
#include <gpuprog_reflect/gl/deferred_point_light.hpp>
#include <threed/buffer.hpp>
#include <threed/compositor.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/texture.hpp>

namespace modus::engine::graphics {
GBufferPasses::GBufferPasses() {
    m_active_buffer = 0;
}

Result<> GBufferPasses::initialize(Engine& engine) {
    auto graphics_module = engine.module<ModuleGraphics>(engine.modules().graphics());
    const auto& comp = graphics_module->default_compositor();
    if (!build_gbuffer(engine, comp.width(), comp.height())) {
        return Error<>();
    }

    // Directional lighting
    {
        // Load Shader Program
        modus_panic("missing shader load");
        /*
        auto r_program = asset_manager.load_immediate_typed<GPUProgramAsset>(
            engine, "gpuprog:engine/deferred_directional_light.gpuprog");
        if (!r_program) {
            MODUS_LOGE("GBufferBuildPass: Failed to load gpu program");
            return Error<>();
        }
        m_lighting.directional.program = (*r_program)->m_program;
        */
    }
    {
        using namespace gpuprogreflect;
        // create effect
        threed::EffectCreateParams params;
        params.state.depth_stencil.depth.enabled = false;
        params.state.depth_stencil.depth.mask = false;
        params.state.depth_stencil.stencil.enabled = false;
        params.state.blend.targets[0].enabled = true;
        params.state.blend.targets[0].eq_source = threed::BlendEquation::One;
        params.state.blend.targets[0].eq_destination = threed::BlendEquation::One;
        params.state.blend.targets[0].op_colour = threed::BlendOperation::Add;
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_directional_light::kSamplerIndex_u_gbuffer_color];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_directional_light::kSamplerIndex_u_gbuffer_normal];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_directional_light::kSamplerIndex_u_gbuffer_depth];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_directional_light::kSamplerIndex_u_gbuffer_emissive];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_directional_light::kSamplerIndex_u_shadow_map];
            sampler.enabled = true;
            sampler.filter_mag = threed::TextureFilter::Linear;
            sampler.filter_min = threed::TextureFilter::Linear;
        }
        auto r_effect = graphics_module->device().create_effect(params);
        if (!r_effect) {
            return Error<>();
        }
        m_lighting.directional.effect = *r_effect;
    }

    // Point Lighting
    {
        // Load Shader Program
        modus_panic("missing shader load");
        /*auto r_program = asset_manager.load_immediate_typed<GPUProgramAsset>(
            engine, "gpuprog:engine/deferred_point_light.gpuprog");
        if (!r_program) {
            MODUS_LOGE("GBufferBuildPass: Failed to load gpu program\n");
            return Error<>();
        }
        m_lighting.point.program = (*r_program)->m_program;*/
    }
    {
        using namespace gpuprogreflect;
        // create effect
        threed::EffectCreateParams params;
        params.state.depth_stencil.depth.enabled = false;
        params.state.depth_stencil.depth.mask = false;
        params.state.depth_stencil.stencil.enabled = true;
        params.state.depth_stencil.stencil.front.compare = threed::CompareFunction::NotEqual;
        params.state.depth_stencil.stencil.front.pass = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.front.fail = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.front.fail_depth = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.back.compare = threed::CompareFunction::NotEqual;
        params.state.depth_stencil.stencil.back.pass = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.back.fail = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.back.fail_depth = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.ref = 0;
        params.state.depth_stencil.stencil.mask = 0xFF;
        params.state.raster.cull_mode = threed::CullMode::Front;
        params.state.raster.cull_enabled = true;
        params.state.blend.targets[0].enabled = true;
        params.state.blend.targets[0].eq_source = threed::BlendEquation::One;
        params.state.blend.targets[0].eq_destination = threed::BlendEquation::One;
        params.state.blend.targets[0].op_colour = threed::BlendOperation::Add;
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_point_light::kSamplerIndex_u_gbuffer_color];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_point_light::kSamplerIndex_u_gbuffer_normal];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_directional_light::kSamplerIndex_u_gbuffer_emissive];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        {
            threed::SamplerState& sampler =
                params.state.sampler[deferred_point_light::kSamplerIndex_u_gbuffer_depth];
            sampler.enabled = true;
            sampler.filter_min = threed::TextureFilter::Nearest;
            sampler.filter_mag = threed::TextureFilter::Nearest;
        }
        auto r_effect = graphics_module->device().create_effect(params);
        if (!r_effect) {
            return Error<>();
        }
        m_lighting.point.effect = *r_effect;
    }
    // Point light sphere mesh
    {
        modus_panic("todo: mesh asset");
        /*
        auto r_asset = asset_manager.load_immediate_typed<engine::graphics::MeshAsset>(
            engine, "mesh:engine/basic_shapes/sphere_low.mesh");
        if (!r_asset) {
            MODUS_LOGE("Failed to load sphere mesh\n");
            return Error<>();
        }
        m_lighting.point.sphere_mesh = (*r_asset)->m_mesh.lods[0].sub_meshes[0].drawable;
        */
    }

    // setup null effect program for stencil testing
    {
        // Load Shader Program

        modus_panic("missing shader load");
        /*auto r_program = asset_manager.load_immediate_typed<GPUProgramAsset>(
            engine, "gpuprog:engine/null.gpuprog");
        if (!r_program) {
            MODUS_LOGE("GBufferBuildPass: Failed to load gpu program\n");
            return Error<>();
        }
        m_lighting.point.null_program = (*r_program)->m_program;
        */
    }
    {
        using namespace gpuprogreflect;
        // create effect
        threed::EffectCreateParams params;
        params.state.depth_stencil.depth.enabled = true;
        params.state.depth_stencil.depth.mask = false;
        params.state.raster.cull_enabled = false;
        params.state.depth_stencil.stencil.enabled = true;
        params.state.depth_stencil.stencil.front.compare = threed::CompareFunction::Always;
        params.state.depth_stencil.stencil.front.pass = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.front.fail = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.front.fail_depth = threed::StencilOp::IncrementWrap;
        params.state.depth_stencil.stencil.back.compare = threed::CompareFunction::Always;
        params.state.depth_stencil.stencil.back.pass = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.back.fail = threed::StencilOp::Keep;
        params.state.depth_stencil.stencil.back.fail_depth = threed::StencilOp::DecrementWrap;
        params.state.depth_stencil.stencil.ref = 0;
        params.state.depth_stencil.stencil.mask = 0;
        params.state.blend.targets[0].enabled = true;
        params.state.blend.targets[0].eq_source = threed::BlendEquation::One;
        params.state.blend.targets[0].eq_destination = threed::BlendEquation::One;
        params.state.blend.targets[0].op_colour = threed::BlendOperation::Add;
        auto r_effect = graphics_module->device().create_effect(params);
        if (!r_effect) {
            return Error<>();
        }
        m_lighting.point.null_effect = *r_effect;
    }

    return Ok<>();
}

Result<> GBufferPasses::shutdown(Engine& engine) {
    auto& device = engine.module<ModuleGraphics>(engine.modules().graphics())->device();

    for (u32 i = 0; i < kBufferCount; ++i) {
        if (m_gbuffer[i]) {
            device.destroy_framebuffer(m_gbuffer[i]);
        }
        if (m_depth_texture[i]) {
            device.destroy_texture(m_depth_texture[i]);
        }
        if (m_color_texture[i]) {
            device.destroy_texture(m_color_texture[i]);
        }
        if (m_normal_texture[i]) {
            device.destroy_texture(m_normal_texture[i]);
        }
        if (m_emissive_texture[i]) {
            device.destroy_texture(m_emissive_texture[i]);
        }
    }
    return Ok<>();
}

Result<> GBufferPasses::build(Engine& engine,
                              NotMyPtr<const IPipeline> graphics_pipeline,
                              threed::PipelinePtr pipeline,
                              const FrameGlobals& frame_globals,
                              SliceMut<Renderable> renderables,
                              threed::FramebufferHandle fbo,
                              const u32 w,
                              const u32 h) {
    m_active_buffer = (m_active_buffer + 1) % kBufferCount;
    auto& device = engine.module<ModuleGraphics>(engine.modules().graphics())->device();
    threed::Pass* pass = device.allocate_pass(*pipeline, "gbuffer");
    pass->frame_buffer = m_gbuffer[m_active_buffer];
    pass->state.clear.colour[0].clear = true;
    pass->state.clear.colour[1].clear = true;
    pass->state.clear.colour[2].clear = true;
    pass->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
    pass->state.clear.colour[1].colour = glm::vec4(0.f);
    pass->state.clear.colour[2].colour = glm::vec4(0.f);
    pass->state.clear.clear_depth = true;
    pass->viewport.width = w;
    pass->viewport.height = h;
    pass->color_targets = threed::kFramebufferColorTarget0 | threed::kFramebufferColorTarget1 |
                          threed::kFramebufferColorTarget2;

    threed::PassFramebufferCopy fbo_copy;
    fbo_copy.copy_color = false;
    fbo_copy.copy_depth = true;
    fbo_copy.other.width = w;
    fbo_copy.other.height = h;
    fbo_copy.pass.width = w;
    fbo_copy.pass.height = h;
    fbo_copy.fast = true;
    fbo_copy.other.handle = fbo;
    pass->framebuffer_copy.post_pass = fbo_copy;
    generate_commands(engine, graphics_pipeline, *pass, frame_globals, renderables, false);
    return Ok<>();
}

Result<> GBufferPasses::build_light_directional(Engine& engine,
                                                threed::PipelinePtr pipeline,
                                                const FrameGlobals& frame_globals,
                                                threed::FramebufferHandle fbo,
                                                const u32 w,
                                                const u32 h) {
    MODUS_UNUSED(frame_globals);
    MODUS_UNUSED(engine);
    MODUS_UNUSED(pipeline);
    MODUS_UNUSED(fbo);
    MODUS_UNUSED(w);
    MODUS_UNUSED(h);
    modus_panic("This section is no longer maintained");

    /*auto graphics_module =
        engine.module<ModuleGraphics>(engine.modules().graphics());
    auto& device = graphics_module->device();
    const auto& light_manager = graphics_module->light_manager();
    const auto& shadow_manager = graphics_module->shadow_manager();

    using namespace gpuprogreflect;
    threed::Pass* pass = device.allocate_pass(*pipeline, "deferredLightDir");
    pass->frame_buffer = fbo;
    pass->viewport.width = w;
    pass->viewport.height = h;

    threed::Command* command = device.allocate_command(*pass);

    command->effect = m_lighting.directional.effect;
    command->program = m_lighting.directional.program;

    command->draw.type = threed::DrawType::Custom;
    command->draw.range_count = 4;
    command->draw.primitive = threed::Primitive::TriangleStrip;
    command->draw.instance_count = light_manager.num_directional_lights();

    deferred_directional_light::set_constant_FrameGlobals(
        *command, frame_globals.constant_buffer());
    deferred_directional_light::set_sampler_u_gbuffer_color(
        *command, m_color_texture[m_active_buffer]);
    deferred_directional_light::set_sampler_u_gbuffer_normal(
        *command, m_normal_texture[m_active_buffer]);

    deferred_directional_light::set_sampler_u_gbuffer_depth(
        *command, m_depth_texture[m_active_buffer]);

    deferred_directional_light::set_constant_Lights(
        *command, light_manager.constant_buffer());

    deferred_directional_light::set_constant_ShadowBlock(
        *command, shadow_manager.constant_buffer());

    deferred_directional_light::set_sampler_u_shadow_map(
        *command, shadow_manager.shadow_map());

    deferred_directional_light::set_sampler_u_gbuffer_emissive(
        *command, m_emissive_texture[m_active_buffer]);
*/
    return Ok<>();
}

Result<> GBufferPasses::build_light_point(Engine& engine,
                                          threed::PipelinePtr pipeline,
                                          const FrameGlobals& frame_globals,
                                          threed::FramebufferHandle fbo,
                                          const u32 w,
                                          const u32 h) {
    MODUS_UNUSED(frame_globals);
    MODUS_UNUSED(engine);
    MODUS_UNUSED(pipeline);
    MODUS_UNUSED(fbo);
    MODUS_UNUSED(w);
    MODUS_UNUSED(h);
    modus_panic("This section is no longer maintained");
    /*
        auto graphics_module =
            engine.module<ModuleGraphics>(engine.modules().graphics());
        auto& device = graphics_module->device();
        const auto& light_manager = graphics_module->light_manager();
        const auto light_slice = light_manager.point_lights();

        if (light_slice.is_empty()) {
            // No lights, nothing to shade
            return Error<>();
        }

        using namespace gpuprogreflect;

        for (usize i = 0; i < light_slice.size(); ++i) {
            const auto& light = light_slice[i];

            math::Transform transform;
            transform.set_translation(light.position);
            // transform.set_scale(LightManager::point_light_radius(light));
            transform.set_scale(light.radius * 1.5f);
            const glm::mat4 mvp =
       frame_globals.constant_block().mat_project_view * transform.to_matrix();
            const auto mvp_constant = device.upload_constant(mvp);

            // Stencil Pass
            {
                threed::Pass* pass =
                    device.allocate_pass(*pipeline, "LightStencilPass");
                pass->frame_buffer = fbo;
                pass->state.clear.clear_stencil = true;
                pass->color_targets = 0;
                pass->viewport.width = w;
                pass->viewport.height = h;
                threed::Command* command = device.allocate_command(*pass);

                command->effect = m_lighting.point.null_effect;
                command->program = m_lighting.point.null_program;

                command->draw.type = threed::DrawType::Default;
                command->draw.instance_count = 0;
                command->drawable = m_lighting.point.sphere_mesh;

                deferred_point_light::set_constant_u_mvp(*command,
       mvp_constant);
            }

            {
                // Lighting Pass
                threed::Pass* pass =
                    device.allocate_pass(*pipeline, "DeferredPointLight");
                pass->frame_buffer = fbo;
                pass->viewport.width = w;
                pass->viewport.height = h;
                pass->color_targets = threed::kFramebufferColorTarget0;
                threed::Command* command = device.allocate_command(*pass);

                command->effect = m_lighting.point.effect;
                command->program = m_lighting.point.program;

                command->draw.type = threed::DrawType::Default;
                command->draw.instance_count = 0;
                command->drawable = m_lighting.point.sphere_mesh;

                const auto instanced_id_constant =
       device.upload_constant(i32(i));
                deferred_point_light::set_constant_u_mvp(*command,
       mvp_constant); deferred_point_light::set_constant_instance_id( *command,
       instanced_id_constant);

                deferred_point_light::set_constant_FrameGlobals(
                    *command, frame_globals.constant_buffer());
                deferred_point_light::set_sampler_u_gbuffer_color(
                    *command, m_color_texture[m_active_buffer]);
                deferred_point_light::set_sampler_u_gbuffer_normal(
                    *command, m_normal_texture[m_active_buffer]);
                deferred_point_light::set_sampler_u_gbuffer_depth(
                    *command, m_depth_texture[m_active_buffer]);

                deferred_point_light::set_constant_Lights(
                    *command, light_manager.constant_buffer());
                deferred_point_light::set_sampler_u_gbuffer_emissive(
                    *command, m_emissive_texture[m_active_buffer]);
            }
        }
    */
    return Ok<>();
}

void GBufferPasses::on_compositor_update(Engine& engine, const threed::Compositor& compositor) {
    if (!build_gbuffer(engine, compositor.width(), compositor.height())) {
        MODUS_LOGE("Failed to rebuild gbuffer framebuffer\n");
    }
}

Result<> GBufferPasses::build_gbuffer(Engine& engine, const u32 w, const u32 h) {
    auto graphics_module = engine.module<ModuleGraphics>(engine.modules().graphics());
    auto& device = graphics_module->device();

    for (u32 i = 0; i < kBufferCount; ++i) {
        if (m_gbuffer[i]) {
            device.destroy_framebuffer(m_gbuffer[i]);
            device.destroy_texture(m_depth_texture[i]);
            device.destroy_texture(m_color_texture[i]);
            device.destroy_texture(m_normal_texture[i]);
        }

        // create depth texture
        {
            threed::TextureCreateParams params;
            params.format = threed::TextureFormat::Depth24;
            params.width = w;
            params.height = h;
            params.depth = 1;
            params.mip_map_levels = 1;
            params.type = threed::TextureType::T2D;

            auto r_texture = device.create_texture(params);
            if (!r_texture) {
                MODUS_LOGE("GBufferBuildPass: Failed to create depth texture");
                return Error<>();
            }
            m_depth_texture[i] = *r_texture;
        }

        // Create color texture
        {
            threed::TextureCreateParams params;
            params.format = threed::TextureFormat::R8G8B8A8;
            params.width = w;
            params.height = h;
            params.depth = 1;
            params.mip_map_levels = 1;
            params.type = threed::TextureType::T2D;

            auto r_texture = device.create_texture(params);
            if (!r_texture) {
                MODUS_LOGE("GBufferBuildPass: Failed to create color texture");
                return Error<>();
            }
            m_color_texture[i] = *r_texture;
        }

        // Create normal texture
        {
            threed::TextureCreateParams params;
            params.format = threed::TextureFormat::R10G10B10A2_I32;
            params.width = w;
            params.height = h;
            params.depth = 1;
            params.mip_map_levels = 1;
            params.type = threed::TextureType::T2D;

            auto r_texture = device.create_texture(params);
            if (!r_texture) {
                MODUS_LOGE("GBufferBuildPass: Failed to create normal texture");
                return Error<>();
            }
            m_normal_texture[i] = *r_texture;
        }
        // Create emissive texture
        {
            threed::TextureCreateParams params;
            params.format = threed::TextureFormat::R5G5B5A1;
            params.width = w;
            params.height = h;
            params.depth = 1;
            params.mip_map_levels = 1;
            params.type = threed::TextureType::T2D;

            auto r_texture = device.create_texture(params);
            if (!r_texture) {
                MODUS_LOGE("GBufferBuildPass: Failed to create emissive texture");
                return Error<>();
            }
            m_emissive_texture[i] = *r_texture;
        }

        // Create Framebuffer
        {
            threed::FramebufferCreateParams params;
            params.depth.handle = m_depth_texture[i];
            params.colour[0].handle = m_color_texture[i];
            params.colour[0].index = 0;
            params.colour[0].mip_map_level = 0;
            params.colour[1].handle = m_normal_texture[i];
            params.colour[1].index = 0;
            params.colour[1].mip_map_level = 0;
            params.colour[2].handle = m_emissive_texture[i];
            params.colour[2].index = 0;
            params.colour[2].mip_map_level = 0;

            auto r_fbo = device.create_framebuffer(params);
            if (!r_fbo) {
                MODUS_LOGE("GBufferBuildPass: Failed to create framebuffer");
                return Error<>();
            }
            m_gbuffer[i] = *r_fbo;
        }
    }

    return Ok<>();
}

}    // namespace modus::engine::graphics
