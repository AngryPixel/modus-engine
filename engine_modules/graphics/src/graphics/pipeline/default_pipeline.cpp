/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>
#include <engine/graphics/light_manager.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>
#include <math/frustum.hpp>

namespace modus::engine::graphics {

/* math::FrustumPoints points;
 math::extract_frustum_points(points,vp);
 MODUS_LOGD("Far Top Right: x:{} y:{} z:{}\n", points[0].x, points[0].y,
 points[0].z); MODUS_LOGD("Far Btm Right: x:{} y:{} z:{}\n", points[1].x,
 points[1].y, points[1].z); MODUS_LOGD("Far Btm Left : x:{} y:{} z:{}\n",
 points[2].x, points[2].y, points[2].z); MODUS_LOGD("Far Top Left : x:{}
 y:{} z:{}\n", points[3].x, points[3].y, points[3].z); MODUS_LOGD("Ner Top
 Right: x:{} y:{} z:{}\n", points[4].x, points[4].y, points[4].z);
 MODUS_LOGD("Ner Btm Right: x:{} y:{} z:{}\n", points[5].x, points[5].y,
 points[5].z); MODUS_LOGD("Ner Btm Left : x:{} y:{} z:{}\n", points[6].x,
 points[6].y, points[6].z); MODUS_LOGD("Ner Top Left : x:{} y:{} z:{}\n",
 points[7].x, points[7].y, points[7].z); MODUS_LOGD("\n");

 math::bv::Plane far = math::bv::make_plane(points[0], points[1],
 points[2]); const u32 idx = math::FrustumPlanes::kFar; const
 math::bv::Plane vp_far = frustum_planes.planes[idx];

 auto mk_plane = [](const glm::vec3& p0, const glm::vec3& p1, const
 glm::vec3& p2) { math::bv::Plane plane; glm::vec3 v1 = p1 - p0; glm::vec3
 v2 = p2 - p0; plane.direction = glm::normalize(glm::cross(v1, v2));
     plane.offset = -glm::dot(p0, plane.direction);
     return plane;
 };
 math::bv::Plane far3 = mk_plane(points[0], points[1], points[2]);

 MODUS_LOGD("Plane VP : x:{} y:{} z:{} d:{}\n", vp_far.direction.x,
 vp_far.direction.y, vp_far.direction.z, vp_far.offset); MODUS_LOGD("Plane
 P3 : x:{} y:{} z:{} d:{}\n", far.direction.x, far.direction.y,
 far.direction.z, far.offset); MODUS_LOGD("Plane P32: x:{} y:{} z:{}
 d:{}\n", far3.direction.x, far3.direction.y, far3.direction.z,
 far3.offset); MODUS_LOGD("\n");*/
/*
Result<> DeferredPipeline::initialize(Engine& engine) {
    MODUS_UNUSED(engine);
    return m_gbuffer_pass.initialize(engine);
}

void DeferredPipeline::shutdown(Engine& engine) {
    MODUS_UNUSED(engine);
    (void)m_gbuffer_pass.shutdown(engine);
}

Result<> DeferredPipeline::build(Engine& engine, threed::PipelinePtr pipeline) {
    // Check if there's anything to render
    auto gameplay_module = engine.module<ModuleGameplay>(engine.modules().gameplay());
    NotMyPtr<const gameplay::GameWorld> world = gameplay_module->world();
    if (world.get() == nullptr) {
        return Error<>();
    }

    auto graphics_module = engine.module<ModuleGraphics>(engine.modules().graphics());
    auto& device = graphics_module->device();

    // Update pipeline state
    m_renderables.clear();
    m_transparent.clear();
    m_opaque.clear();
    m_opaque_non_deferred.clear();
    m_skybox.reset();
    m_lights.clear();

    const auto& compositor = graphics_module->default_compositor();
    threed::Pass* pass_clear = device.allocate_pass(*pipeline, "Clear");
    pass_clear->viewport.width = compositor.width();
    pass_clear->viewport.height = compositor.height();
    pass_clear->frame_buffer = compositor.frame_buffer();
    pass_clear->state.clear.colour[0].clear = true;
    pass_clear->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
    pass_clear->color_targets = threed::kFramebufferColorTarget0;

    const engine::gameplay::EntityManager& entity_manager = world->entity_manager();
    auto r_camera = entity_manager.component<CameraComponent>(world->main_camera());
    if (!r_camera) {
        MODUS_LOGE("DefaultPipeline: Couldn't not retrieve world camera");
        return Error<>();
    }
    const engine::graphics::Camera& world_camera = (*r_camera)->m_camera;
    {
        MODUS_PROFILE_BLOCK("Collect Visibles", profiler::color::Red600);
        m_renderables.reserve(entity_manager.entity_count());
        engine::graphics::FrustumEvaluator evaluator(world_camera);
        const auto& mesh_db = graphics_module->mesh_db();
        entity_manager.for_each_matching<gameplay::TransformComponent, GraphicsComponent>(
            [this, &mesh_db, &evaluator, &world_camera](
                const engine::gameplay::EntityManager&, const engine::gameplay::EntityId,
                const gameplay::TransformComponent& tc, const GraphicsComponent& gc) {
                if (is_component_visible(gc, evaluator)) {
                    component_to_renderable(m_renderables, mesh_db, tc, world_camera, gc);
                }
            });
    }

    {
        MODUS_PROFILE_BLOCK("Collect Lights", profiler::color::Red600);
        engine::graphics::FrustumEvaluator evaluator(world_camera);
        m_lights.reserve(64);    // magic number!
        entity_manager.for_each_matching<LightComponent>(
            [&evaluator, this](const engine::gameplay::EntityManager&, const gameplay::EntityId,
                               const LightComponent& lc) {
                if (lc.enabled && lc.light.evaluate_visibility(evaluator)) {
                    m_lights.push_back(lc.light);
                }
            });
    }

        // Update light & shadow manager
        {
            const auto light_slice = make_slice(m_lights);
            if (!m_light_manager().update(
                    engine, compositor, world_camera, light_slice)) {
                MODUS_LOGE("Failed to update light manager");
                return threed::PipelinePtr();
            }

            if (!graphics_module->shadow_manager().update(engine, light_slice))
       { MODUS_LOGE("Failed to update shadow manager"); return
       threed::PipelinePtr();
            }
        }

    MODUS_PROFILE_PLOT("Renderables", m_renderables.size());
    MODUS_PROFILE_PLOT("Lights ", m_lights.size());

    // Split into opaque non-opaque
    {
        MODUS_PROFILE_BLOCK("Categorize", profiler::color::Red600);
        m_opaque.reserve(m_renderables.size());
        m_transparent.reserve(m_renderables.size() / 3);
        for (auto& renderable : m_renderables) {
            if (!renderable.material_instance.has_flag(
                    engine::graphics::kMaterialFlagTransparent)) {
                if (renderable.material_instance.has_flag(engine::graphics::KMaterialFlagSkyBox)) {
                    m_skybox = renderable;
                } else if (renderable.material_instance.has_flag(
                               engine::graphics::kMaterialFlagDeferredRendering)) {
                    m_opaque.push_back(renderable);
                } else {
                    m_opaque_non_deferred.push_back(renderable);
                }
            } else {
                m_transparent.push_back(renderable);
            }
        }
    }

    // Retrieve all visible objects from the game world
    const auto frame_globals = graphics_module->frame_globals();
    if (!frame_globals) {
        MODUS_LOGE("No Frame Globals avaialble");
        return Error<>();
    }

        // Shadows
        {
            MODUS_PROFILE_BLOCK("Shadows", profiler::color::Red800);
            auto& shadow_manager = graphics_module->shadow_manager();

            ShadowMapParams shadow_params{*world, *pipeline,
                                          world_camera.frustum().near_range(),
                                          world_camera.frustum().far_range()};
            shadow_manager.build_pass(engine, shadow_params);
        }
    // Draw
    {
        MODUS_PROFILE_BLOCK("Prepare Commands", profiler::color::Red500);
        {
            MODUS_PROFILE_BLOCK("Deferred-gbuffer", profiler::color::Red900);
            if (auto r_pass = m_gbuffer_pass.build(
                    engine, this, pipeline, **frame_globals, make_slice_mut(m_opaque),
                    compositor.frame_buffer(), compositor.width(), compositor.height());
                r_pass) {
            }
        }
        {
            MODUS_PROFILE_BLOCK("Deferred-Lighting-Point", profiler::color::Red900);
            if (auto r_pass = m_gbuffer_pass.build_light_point(
                    engine, pipeline, **frame_globals, compositor.frame_buffer(),
                    compositor.width(), compositor.height());
                r_pass) {
            }
        }
        {
            MODUS_PROFILE_BLOCK("Deferred-Lighting-Directional", profiler::color::Red900);
            if (auto r_pass = m_gbuffer_pass.build_light_directional(
                    engine, pipeline, **frame_globals, compositor.frame_buffer(),
                    compositor.width(), compositor.height());
                r_pass) {
            }
        }

        threed::Pass* forward_pass = device.allocate_pass(*pipeline, "Forward");
        forward_pass->viewport.width = compositor.width();
        forward_pass->viewport.height = compositor.height();
        forward_pass->frame_buffer = compositor.frame_buffer();
        forward_pass->color_targets = threed::kFramebufferColorTarget0;

        {
            MODUS_PROFILE_BLOCK("Forward", profiler::color::Red900);
            generate_commands(engine, this, *forward_pass, **frame_globals,
                              make_slice_mut(m_opaque_non_deferred), false);
        }
        if (m_skybox.has_value()) {
            MODUS_PROFILE_BLOCK("SkyBox", profiler::color::Red900);
            generate_commands(engine, this, *forward_pass, **frame_globals,
                              SliceMut(&m_skybox.value(), 1), false);
        }
        {
            MODUS_PROFILE_BLOCK("Transparent", profiler::color::Red900);

            std::sort(m_transparent.begin(), m_transparent.end(),
                      [](const Renderable& r1, const Renderable& r2) {
                          const f32 d1 = r1.z_distance;
                          const f32 d2 = r2.z_distance;
                          return d1 > d2;
                      });

            generate_commands(engine, this, *forward_pass, **frame_globals,
                              make_slice_mut(m_transparent), true);
        }
    }
    return Ok<>();
}

void DeferredPipeline::on_compositor_update(Engine& engine, const threed::Compositor& compositor) {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(compositor);
    m_gbuffer_pass.on_compositor_update(engine, compositor);
}
*/
Result<> ForwardPipeline::initialize(Engine& engine) {
    if (!m_light_manager.initialize(engine)) {
        MODUS_LOGE("ForwardPipeline: Failed to initialize light manager");
        return Error<>();
    }

    if (!m_shadow_manager.initialize(engine)) {
        MODUS_LOGE("ForwardPipeline: Failed to initialize shadow manager");
        return Error<>();
    }
    auto graphics_module = engine.module<ModuleGraphics>();
    if (!m_particle_command_generator.initialize(graphics_module->device(), 1024 * 1024, 0)) {
        MODUS_LOGE("ForwardPipline: Failed to initialize particle command generator");
    }
    return Ok<>();
}

void ForwardPipeline::shutdown(Engine& engine) {
    if (!m_shadow_manager.shutdown(engine)) {
        MODUS_LOGE("ForwardPipeline: Failed to shutdown shadow manager");
    }
    if (!m_light_manager.shutdown(engine)) {
        MODUS_LOGE("ForwardPipeline: Failed to shutdown light manager");
    }
    auto graphics_module = engine.module<ModuleGraphics>();
    m_particle_command_generator.shutdown(graphics_module->device());
}

Result<> ForwardPipeline::build(Engine& engine, threed::PipelinePtr pipeline) {
    // Check if there's anything to render
    auto gameplay_module = engine.module<ModuleGameplay>();
    NotMyPtr<const gameplay::GameWorld> world = gameplay_module->world();
    if (world.get() == nullptr) {
        return Error<>();
    }

    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();

    // Update pipeline state
    m_renderables.clear();
    m_transparent.clear();
    m_opaque.clear();
    m_skybox.reset();
    m_lights.clear();

    const auto& compositor = graphics_module->default_compositor();
    const engine::gameplay::EntityManager& entity_manager = world->entity_manager();
    auto r_camera = entity_manager.component<CameraComponent>(world->main_camera());
    if (!r_camera) {
        MODUS_LOGE("ForwardPipeline: Couldn't not retrieve world camera");
        return Error<>();
    }
    const engine::graphics::Camera& world_camera = (*r_camera)->m_camera;
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Collect Visibles");
        m_renderables.reserve(entity_manager.entity_count());
        engine::graphics::FrustumEvaluator evaluator(world_camera);
        const auto& mesh_db = graphics_module->mesh_db();
        gameplay::EntityManagerView<const gameplay::TransformComponent, const GraphicsComponent>
            view(entity_manager);
        view.for_each([this, &mesh_db, &evaluator, &world_camera](
                          const engine::gameplay::EntityId, const gameplay::TransformComponent& tc,
                          const GraphicsComponent& gc) {
            if (is_component_visible(gc, evaluator)) {
                component_to_renderable(m_renderables, mesh_db, tc, world_camera, gc);
            }
        });
    }

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Collect Lights");
        engine::graphics::FrustumEvaluator evaluator(world_camera);
        m_lights.reserve(64);    // magic number!
        gameplay::EntityManagerView<const LightComponent> view(entity_manager);
        view.for_each([&evaluator, this](const gameplay::EntityId, const LightComponent& lc) {
            if (lc.enabled && lc.light.evaluate_visibility(evaluator)) {
                m_lights.push_back(lc.light);
            }
        });
    }

    // Update light & shadow manager
    {
        const auto light_slice = make_slice(m_lights);
        if (!m_light_manager.update(engine, compositor, world_camera, light_slice)) {
            MODUS_LOGE("Failed to update light manager");
            return Error<>();
        }

        if (!m_shadow_manager.update(engine, light_slice)) {
            MODUS_LOGE("Failed to update shadow manager");
            return Error<>();
        }
    }

    // Update Particles
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Update Particles V2");
        graphics_module->particle_registry().update_emitters(engine);
    }

    MODUS_PROFILE_PLOT("Renderables", m_renderables.size());
    MODUS_PROFILE_PLOT("Lights ", m_lights.size());

    // Split into opaque non-opaque
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Categorize");
        m_opaque.reserve(m_renderables.size());
        m_transparent.reserve(m_renderables.size() / 3);
        for (auto& renderable : m_renderables) {
            if (!renderable.material_instance.has_flag(
                    engine::graphics::kMaterialFlagTransparent)) {
                if (renderable.material_instance.has_flag(engine::graphics::KMaterialFlagSkyBox)) {
                    m_skybox = renderable;
                } else {
                    m_opaque.push_back(renderable);
                }
            } else {
                m_transparent.push_back(renderable);
            }
        }
    }

    // Retrieve all visible objects from the game world
    const auto frame_globals = graphics_module->frame_globals();
    if (!frame_globals) {
        MODUS_LOGE("No Frame Globals avaialble");
        return Error<>();
    }

    // Shadows
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Shadows");

        ShadowMapParams shadow_params{*world, *pipeline, world_camera.frustum().near_range(),
                                      world_camera.frustum().far_range()};
        m_shadow_manager.build_pass(engine, shadow_params);
    }
    // Draw
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Prepare Commands");
        threed::Pass* forward_pass = device.allocate_pass(*pipeline, "Forward");
        forward_pass->viewport.width = compositor.width();
        forward_pass->viewport.height = compositor.height();
        forward_pass->frame_buffer = compositor.frame_buffer();
        forward_pass->color_targets = threed::kFramebufferColorTarget0;
        forward_pass->state.clear.colour[0].clear = true;
        forward_pass->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
        forward_pass->state.clear.clear_depth = true;
        forward_pass->state.clear.clear_stencil = true;
        forward_pass->color_targets = threed::kFramebufferColorTarget0;

        {
            MODUS_PROFILE_GRAPHICS_BLOCK("Forward");
            generate_commands(engine, this, *forward_pass, **frame_globals,
                              make_slice_mut(m_opaque), false);
        }
        if (m_skybox.has_value()) {
            MODUS_PROFILE_GRAPHICS_BLOCK("SkyBox");
            generate_commands(engine, this, *forward_pass, **frame_globals,
                              SliceMut(&m_skybox.value(), 1), false);
        }
        {
            MODUS_PROFILE_GRAPHICS_BLOCK("ParticlesV2");
            m_particle_command_generator.generate_commands(graphics_module->particle_registry(),
                                                           device, *forward_pass, world_camera);
        }
        {
            MODUS_PROFILE_GRAPHICS_BLOCK("Transparent");
            std::sort(m_transparent.begin(), m_transparent.end(),
                      [](const Renderable& r1, const Renderable& r2) {
                          const f32 d1 = r1.z_distance;
                          const f32 d2 = r2.z_distance;
                          return d1 > d2;
                      });

            generate_commands(engine, this, *forward_pass, **frame_globals,
                              make_slice_mut(m_transparent), true);
        }
    }
    return Ok<>();
}

void ForwardPipeline::on_compositor_update(Engine& engine, const threed::Compositor& compositor) {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(compositor);
}

}    // namespace modus::engine::graphics
