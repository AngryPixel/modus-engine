/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/pipeline/pass_clear.hpp>
#include <engine/modules/module_gameplay.hpp>
#include <engine/modules/module_graphics.hpp>

namespace modus::engine::graphics {

Result<> PassClear::initialize(Engine&) {
    return Ok<>();
}

Result<> PassClear::shutdown(Engine&) {
    return Ok<>();
}

Result<> PassClear::build(Engine& engine, const PassState& state) {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(state);
    auto graphics_module = engine.module<ModuleGraphics>();
    threed::Pass* pass = graphics_module->device().allocate_pass(*state.pipeline, "Clear");
    pass->frame_buffer = state.framebuffer.handle;
    pass->state.clear.colour[0].clear = true;
    pass->state.clear.colour[0].colour = glm::vec4(0.f);
    pass->viewport.width = state.framebuffer.width_px;
    pass->viewport.height = state.framebuffer.height_px;
    pass->state.clear.clear_depth = true;
    return Ok<>();
}

}    // namespace modus::engine::graphics
