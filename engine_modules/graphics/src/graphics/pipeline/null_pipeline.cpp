/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/pipeline/null_pipeline.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/engine.hpp>

#include <threed/compositor.hpp>

namespace modus::engine::graphics {

Result<> NullPipeline::initialize(Engine&) {
    return Ok<>();
}

void NullPipeline::shutdown(Engine&) {}

Result<> NullPipeline::build(Engine& engine, threed::PipelinePtr pipeline) {
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    const auto& compositor = graphics_module->default_compositor();
    threed::Pass* pass_clear = device.allocate_pass(*pipeline, "Clear");
    pass_clear->viewport.width = compositor.width();
    pass_clear->viewport.height = compositor.height();
    pass_clear->frame_buffer = compositor.frame_buffer();
    pass_clear->state.clear.colour[0].clear = true;
    pass_clear->state.clear.colour[0].colour = glm::vec4(0.f, 0.f, 0.f, 1.0f);
    pass_clear->color_targets = threed::kFramebufferColorTarget0;
    return Ok<>();
}

void NullPipeline::on_compositor_update(Engine&, const threed::Compositor&) {}

}    // namespace modus::engine::graphics
