/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/particles/particle_registryv2.hpp>
// clang-format on

#include <engine/engine.hpp>

namespace modus::engine::graphics {
static constexpr u32 kNumInstancesPerBlock = 32;

ParticleRegistryV2::ParticleRegistryV2() : m_instances(kNumInstancesPerBlock) {}

ParticleRegistryV2::~ParticleRegistryV2() {}

Result<> ParticleRegistryV2::initialize() {
    return Ok<>();
}

void ParticleRegistryV2::shutdown() {
    m_instances.clear();
    m_particle_systems.clear();
}

Result<ParticleSystemV2Handle> ParticleRegistryV2::register_system(const GID& guid,
                                                                   ParticleSystemV2&& system) {
    return m_particle_systems.add(guid, std::forward<ParticleSystemV2>(system));
}
Result<std::pair<ParticleEmitterV2Handle, NotMyPtr<ParticleEmitterV2>>>
ParticleRegistryV2::create_emitter(ParticleEmitterV2&& emitter) {
    if (emitter.instances.empty()) {
        MODUS_LOGE("At least one particle system instance is requred");
        return Error<>();
    }

    for (const auto& instance : emitter.instances) {
        if (!m_particle_systems.get(instance.system)) {
            MODUS_LOGE("Invalid Particle System handle");
            return Error<>();
        }
        if (instance.renderer_type == ParticleRendererType::Total) {
            MODUS_LOGE("Invalid Particle Renderer type");
            return Error<>();
        }
    }

    for (auto& instance : emitter.instances) {
        if (!instance.data.generate(instance.particle_count)) {
            MODUS_LOGE("Failed to setup particle data");
            return Error<>();
        }
    }

    return m_instances.create(std::move(emitter));
}

void ParticleRegistryV2::destroy_emitter(const ParticleEmitterV2Handle handle) {
    (void)m_instances.erase(handle);
}

Result<> ParticleRegistryV2::update_emitter_transform(const ParticleEmitterV2Handle handle,
                                                      const math::Transform& transform) {
    auto r_instance = m_instances.get(handle);
    if (!r_instance) {
        return Error<>();
    }
    (*r_instance)->transform = transform;
    return Ok<>();
}
void ParticleRegistryV2::update_emitters(const Engine& engine) {
    m_instances.for_each([this, &engine](ParticleEmitterV2& emitter) {
        for (auto& instance : emitter.instances) {
            auto r_system = m_particle_systems.get(instance.system);
            if (!r_system) {
                modus_assert_message(false, "Failed to find particle system");
                return;
            }
            (*r_system)->update(engine.tick_sec(), instance.data, emitter.transform);
        }
    });
}

}    // namespace modus::engine::graphics