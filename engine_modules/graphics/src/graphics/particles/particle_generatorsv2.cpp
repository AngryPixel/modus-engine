/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/particles/particle_generatorsv2.hpp>
#include <math/math.hpp>
#include <glm/gtc/random.hpp>

namespace modus::engine::graphics {

void NullPosParticleGenerator::generate(const ParticleDTType,
                                        ParticleData& particles,
                                        const math::Transform& transform,
                                        const usize start_index,
                                        const usize end_index) {
    for (usize i = start_index; i < end_index; ++i) {
        particles.m_position[i] = glm::vec4(transform.translation(), 1.f);
    }
}

void BoxPosParticleGenerator::generate(const ParticleDTType,
                                       ParticleData& particles,
                                       const math::Transform& transform,
                                       const usize start_index,
                                       const usize end_index) {
    const glm::vec4 min_pos = glm::vec4(transform.transform(m_pos - m_max_start_offset), 1.0f);
    const glm::vec4 max_pos = glm::vec4(transform.transform(m_pos + m_max_start_offset), 1.0f);

    for (usize i = start_index; i < end_index; ++i) {
        particles.m_position[i] = glm::linearRand(min_pos, max_pos);
    }
}
void CirclePosParticleGenerator::generate(const ParticleDTType,
                                          ParticleData& particles,
                                          const math::Transform& transform,
                                          const usize start_index,
                                          const usize end_index) {
    for (usize i = start_index; i < end_index; ++i) {
        const f32 angle = glm::linearRand(0.f, math::kPI * 2.0f);
        particles.m_position[i] =
            glm::vec4(transform.transform(m_center + glm::vec3(m_rad_x * glm::sin(angle),
                                                               m_rad_y * glm::cos(angle), 0.f)),
                      1.0f);
    }
}

void ColorChangeParticleGenerator::generate(const ParticleDTType,
                                            ParticleData& particles,
                                            const math::Transform&,
                                            const usize start_index,
                                            const usize end_index) {
    for (usize i = start_index; i < end_index; ++i) {
        particles.m_start_color[i] = m_start_color;
        particles.m_end_color[i] = m_end_color;
    }
}

void BasicVelParticleGenerator::generate(const ParticleDTType,
                                         ParticleData& particles,
                                         const math::Transform&,
                                         const usize start_index,
                                         const usize end_index) {
    for (usize i = start_index; i < end_index; ++i) {
        particles.m_velocity[i] = glm::vec4(glm::linearRand(m_min_velocity, m_max_velocity), 0.f);
    }
}
void BasicCircleVelParticleGenerator::generate(const ParticleDTType,
                                               ParticleData& particles,
                                               const math::Transform&,
                                               const usize start_index,
                                               const usize end_index) {
    f32 phi, theta, v, r;
    for (usize i = start_index; i < end_index; ++i) {
        phi = glm::linearRand(-math::kPI, math::kPI);
        theta = glm::linearRand(-math::kPI, math::kPI);
        v = glm::linearRand(m_min_velocity, m_max_velocity);
        r = v * glm::sin(phi);
        particles.m_velocity[i] =
            glm::vec4(v * glm::cos(phi), r * glm::cos(theta), r * glm::sin(theta), 0.f);
    }
}
void BasicTimeParticleGenerator::generate(const ParticleDTType,
                                          ParticleData& particles,
                                          const math::Transform&,
                                          const usize start_index,
                                          const usize end_index) {
    for (usize i = start_index; i < end_index; ++i) {
        glm::vec4& time = particles.m_time[i];
        time.x = time.y = glm::linearRand(m_min_time, m_max_time);
        time.z = 0.f;
        time.w = 1.0f / time.x;
        particles.m_time[i] = time;
    }
}
void NullUVParticleGenerator::generate(const ParticleDTType,
                                       ParticleData& particles,
                                       const math::Transform&,
                                       const usize start_index,
                                       const usize end_index) {
    for (usize i = start_index; i < end_index; ++i) {
        particles.m_uv[i] = glm::vec4(m_transform.x, m_transform.y, m_depth, 0.f);
    }
}
}    // namespace modus::engine::graphics
