/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/particles/particle_renderersv2.hpp>
// clang-format on

#include <engine/modules/module_graphics.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <threed/drawable.hpp>
#include <threed/effect.hpp>
#include <threed/buffer.hpp>
#include <threed/sampler.hpp>
#include <threed/device.hpp>
#include <threed/program_gen/shader_generator.hpp>
#include <engine/engine.hpp>
#include <math/billboard.hpp>
#include <math/packing.hpp>

namespace modus::engine::graphics {

PointParticleRenderer::PointParticleRenderer() {}

Result<> PointParticleRenderer::initialize(threed::Device& device) {
    // program
    {
        threed::dynamic::ProgramGenParams params;
        threed::dynamic::TypedDefaultConstantInput<glm::mat4> vp_input("u_vp", &m_vp_binder);
        params.varying
            .push_back({"v_color", threed::DataType::Vec4F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Medium})
            .expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_color_input()).expect();
        params.vertex_stage.constants.push_back(&vp_input).expect();
        params.vertex_stage.main = R"R(
    void main() {
        gl_Position = u_vp * vec4(VERTEX.xyz, 1.0);
        v_color = COLOR;
        gl_PointSize = VERTEX.w;
    }
    )R";
        params.fragment_stage.fragment_outputs
            .push_back(engine::graphics::shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.main = R"R(
    void main(){
        OUT_COLOR = v_color;
    })R";

        const auto& shader_generator = device.shader_generator();
        threed::dynamic::ProgramGenerator generator;
        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("PointParticleRenderer: Failed to generate particle program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("PointParticleRenderer: Failed to create particle program: {}", r.error());
            return Error<>();
        } else {
            m_gpuprog = *r;
        }
    }

    // Drawable
    {
        threed::DrawableCreateParams draw_params;
        draw_params.start = 0;
        draw_params.count = 1;
        draw_params.primitive = threed::Primitive::Points;

        {
            threed::DrawableDataParams& desc = draw_params.data[engine::graphics::kVertexSlot];
            desc.offset = 0;
            desc.data_type = threed::DataType::Vec4F32;
        }
        {
            threed::DrawableDataParams& desc = draw_params.data[engine::graphics::kColourSlot];
            desc.offset = sizeof(glm::vec4);
            desc.data_type = threed::DataType::Vec4U8;
            desc.normalized = true;
        }

        if (auto r = device.create_drawable(draw_params); !r) {
            MODUS_LOGE("PointParticleRenderer: Failed to create drawable: {}", r.error());
            return Error<>();
        } else {
            m_drawable = *r;
        }
    }

    // Effect
    {
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = false;
        effect_params.state.depth_stencil.stencil.enabled = false;
        if (auto r = device.create_effect(effect_params); !r) {
            MODUS_LOGE("PointParticleRenderer: Failed to create effect");
            return Error<>();
        } else {
            m_effect = *r;
        }
    }

    return Ok<>();
}

void PointParticleRenderer::shutdown(threed::Device& device) {
    if (m_effect) {
        device.destroy_effect(m_effect);
    }
    if (m_drawable) {
        device.destroy_drawable(m_drawable);
    }
    if (m_gpuprog) {
        device.destroy_program(m_gpuprog);
    }
}

usize PointParticleRenderer::render_data_size(const ParticleData& data,
                                              const ParticleRenderData*) const {
    return sizeof(Data) * data.alive_end_index();
}

void PointParticleRenderer::fill_buffer(ByteSliceMut buffer,
                                        const ParticleData& data,
                                        const math::Transform&,
                                        const ParticleRenderData*,
                                        const Camera&) const {
    const usize end_index = data.alive_end_index();
    modus_assert(buffer.size() == sizeof(Data) * end_index);
    Data* data_slice = reinterpret_cast<Data*>((void*)buffer.data());
    const glm::vec4* __restrict position = data.m_position;
    const glm::vec4* __restrict color = data.m_color;

    for (usize i = 0; i < end_index; ++i) {
        data_slice[i].position = position[i];
        data_slice[i].color = math::pack_unorm_8888(color[i]);
    }
}

Result<NotMyPtr<threed::Command>> PointParticleRenderer::create_commands(
    const ParticleData& data,
    const ParticleRenderData*,
    CommandData& command_data) const {
    const usize end_index = data.alive_end_index();
    auto command = command_data.device.allocate_command(command_data.pass);
    command->effect = m_effect;
    command->program = m_gpuprog;
    command->drawable = m_drawable;
    command->draw.type = threed::DrawType::CustomRange;
    command->draw.range_start = 0;
    command->draw.range_count = end_index;

    m_vp_binder = command_data.camera.projection_view_matrix();
    m_binders.bind(*command, command_data.device);

    threed::DrawableUpdateParams update_params;
    update_params.data_buffers[0].buffer = command_data.buffer;
    update_params.data_buffers[0].offset = command_data.buffer_offset;
    update_params.data_buffers[0].stride = sizeof(glm::vec4) * 2;

    auto r_update_handle = command_data.device.allocate_drawable_update(update_params);
    if (!r_update_handle) {
        MODUS_LOGE("Failed to allocate drawable update handle");
        return Error<>();
    }
    command->drawable_update = *r_update_handle;
    return Ok(NotMyPtr<threed::Command>(command));
}

BillboardParticleRenderer::BillboardParticleRenderer() {}

Result<> BillboardParticleRenderer::initialize(threed::Device& device) {
    constexpr u32 kAxisVertexInputSlot = 10;
    {
        threed::dynamic::ProgramGenParams params;
        // program
        threed::dynamic::TypedDefaultConstantInput<glm::mat4> vp_input("u_vp", &m_vp_binder);
        threed::dynamic::TypedDefaultConstantInput<glm::vec3> camera_pos_input(
            "u_camera_pos", &m_camera_pos_binder);
        threed::dynamic::TypedDefaultConstantInput<u32> mode_input("u_mode", &m_mode_binder);
        threed::dynamic::TypedDefaultConstantInput<glm::mat3> view_transposed_input(
            "u_view_transposed", &m_view_transposed_binder);
        threed::dynamic::DefaultSamplerInput image_input("s_image", &m_image_binder);
        image_input.m_desc.stype = threed::dynamic::SamplerType::ST2DArray;

        params.varying
            .push_back({"v_uv", threed::DataType::Vec3F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Medium})
            .expect();
        params.varying
            .push_back({"v_color", threed::DataType::Vec4F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Medium})
            .expect();

        params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_color_input()).expect();
        {
            threed::dynamic::VertexInput axis_input_desc;
            axis_input_desc.data_type = threed::DataType::Vec4F32;
            axis_input_desc.name = "AXIS";
            axis_input_desc.slot = kAxisVertexInputSlot;
            params.vertex_stage.inputs.push_back(axis_input_desc).expect();
        }
        {
            threed::dynamic::VertexInput desc;
            desc.data_type = threed::DataType::Vec4F32;
            desc.name = "UV";
            desc.slot = kCustom1Slot;
            params.vertex_stage.inputs.push_back(desc).expect();
        }

        params.vertex_stage.constants.push_back(&vp_input).expect();
        params.vertex_stage.constants.push_back(&camera_pos_input).expect();
        params.vertex_stage.constants.push_back(&mode_input).expect();
        params.vertex_stage.constants.push_back(&view_transposed_input).expect();
        params.vertex_stage.main = R"R(

mat3 get_billboard_matrix(in vec3 camera_position, in vec3 world_position, in vec3 axis) {
    mat3 result;
    vec3 direction = normalize(world_position - camera_position);
    vec3 right = normalize(cross(axis, direction));
    vec3 billboard_direction = cross(right, axis);
    vec3 billboard_up_vec = cross(billboard_direction, right);
	result[2] = -billboard_direction;
    result[0] = normalize(cross(billboard_up_vec, result[2]));
    result[1] = cross(result[2], result[0]);
    return result;
}

void main()
{
    const vec2 positions[4] = vec2[](
        vec2(-1, -1),
        vec2(+1, -1),
        vec2(-1, +1),
        vec2(+1, +1)
    );
    const vec2 coords[4] = vec2[](
        vec2(0, 0),
        vec2(1, 0),
        vec2(0, 1),
        vec2(1, 1)
    );

    mat4 transform;
    if (u_mode == 0u) {
        transform = mat4(u_view_transposed);
        transform[3] = VERTEX;
    } else if (u_mode == 1u) {
        transform = mat4(get_billboard_matrix(u_camera_pos, VERTEX.xyz, AXIS.xyz));
        transform[3] = VERTEX;
    }
    v_uv = vec3(coords[gl_VertexID] + UV.xy, UV.z);
    v_color = COLOR;
    gl_Position = u_vp * transform * vec4(positions[gl_VertexID], 0.0, 1.0);
})R";
        params.fragment_stage.fragment_outputs
            .push_back(engine::graphics::shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.samplers.push_back(&image_input).expect();
        params.fragment_stage.main = R"R(
void main(){
    float factor = texture(s_image, v_uv).r;
    OUT_COLOR = vec4(v_color.xyz, factor);
})R";

        const auto& shader_generator = device.shader_generator();
        threed::dynamic::ProgramGenerator generator;
        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("BillboardParticleRenderer: Failed to generate particle program: {}",
                       r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("BillboardParticleRenderer: Failed to create particle program: {}",
                       r.error());
            return Error<>();
        } else {
            m_gpuprog = *r;
        }
    }

    // Drawable
    {
        constexpr u32 kStride = sizeof(Data);
        threed::DrawableCreateParams draw_params;
        draw_params.start = 0;
        draw_params.count = 4;
        draw_params.primitive = threed::Primitive::TriangleStrip;
        draw_params.data_buffers[0].instance_divisor = 1;
        draw_params.data_buffers[0].stride = kStride;

        {
            threed::DrawableDataParams& desc = draw_params.data[engine::graphics::kVertexSlot];
            desc.offset = 0;
            desc.data_type = threed::DataType::Vec4F32;
        }
        {
            threed::DrawableDataParams& desc = draw_params.data[engine::graphics::kColourSlot];
            desc.offset = sizeof(glm::vec4);
            desc.data_type = threed::DataType::Vec4U8;
            desc.normalized = true;
        }
        {
            threed::DrawableDataParams& desc = draw_params.data[kAxisVertexInputSlot];
            desc.offset = sizeof(glm::vec4) + sizeof(u32);
            desc.data_type = threed::DataType::I32_2_3x10_REV;
            desc.normalized = true;
        }
        {
            threed::DrawableDataParams& desc = draw_params.data[graphics::kCustom1Slot];
            desc.offset = sizeof(glm::vec4) + (sizeof(u32) * 2);
            desc.data_type = threed::DataType::Vec4U16;
            desc.normalized = true;
        }
        if (auto r = device.create_drawable(draw_params); !r) {
            MODUS_LOGE("BillboardParticleRenderer: Failed to create drawable: {}", r.error());
            return Error<>();
        } else {
            m_drawable = *r;
        }
    }

    // Effect
    {
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = true;
        effect_params.state.depth_stencil.depth.mask = false;
        effect_params.state.blend.targets[0].enabled = true;
        effect_params.state.blend.targets[0].eq_source = threed::BlendEquation::SourceAlpha;
        effect_params.state.blend.targets[0].eq_destination =
            threed::BlendEquation::OneMinusSourceAlpha;
        effect_params.state.raster.cull_enabled = true;
        if (auto r = device.create_effect(effect_params); !r) {
            MODUS_LOGE("BillboardParticleRenderer: Failed to create effect");
            return Error<>();
        } else {
            m_effect = *r;
        }
    }

    // Sampler
    {
        threed::SamplerCreateParams params;
        params.wrap_r = threed::SamplerWrapMode::Repeat;
        params.wrap_s = threed::SamplerWrapMode::Repeat;
        params.wrap_t = threed::SamplerWrapMode::Repeat;
        params.filter_min = threed::SamplerFilter::NearestMipMapNearest;
        params.filter_mag = threed::SamplerFilter::Linear;
        if (auto r = device.create_sampler(params); !r) {
            MODUS_LOGE("BillboardParticleRenderer: Failed to create sampler");
            return Error<>();
        } else {
            m_sampler = *r;
        }
    }

    return Ok<>();
}

void BillboardParticleRenderer::shutdown(threed::Device& device) {
    if (m_effect) {
        device.destroy_effect(m_effect);
    }
    if (m_drawable) {
        device.destroy_drawable(m_drawable);
    }
    if (m_gpuprog) {
        device.destroy_program(m_gpuprog);
    }
    if (m_sampler) {
        device.destroy_sampler(m_sampler);
    }
}

usize BillboardParticleRenderer::render_data_size(const ParticleData& data,
                                                  const ParticleRenderData*) const {
    return sizeof(Data) * data.alive_end_index();
}

void BillboardParticleRenderer::fill_buffer(ByteSliceMut buffer,
                                            const ParticleData& data,
                                            const math::Transform& transform,
                                            const ParticleRenderData* render_data,
                                            const Camera&) const {
    const BillboardParticleRendererData* this_render_data =
        static_cast<const BillboardParticleRendererData*>(render_data);
    const usize end_index = data.alive_end_index();
    modus_assert(buffer.size() == sizeof(Data) * end_index);
    Data* data_slice = reinterpret_cast<Data*>((void*)buffer.data());
    const glm::vec4* __restrict position = data.m_position;
    const glm::vec4* __restrict color = data.m_color;
    const glm::vec4* __restrict uv = data.m_uv;

    for (usize i = 0; i < end_index; ++i) {
        data_slice[i].position = position[i];
        data_slice[i].color = math::pack_unorm_8888(color[i]);
        data_slice[i].uv = math::pack_unorm_16161616(uv[i]);
    }

    if (this_render_data->alignment ==
            BillboardParticleRendererData::AlignmentType::TransformedAxis ||
        this_render_data->alignment == BillboardParticleRendererData::AlignmentType::FixedAxis) {
        const glm::vec4 axis =
            glm::vec4(glm::normalize(this_render_data->alignment ==
                                             BillboardParticleRendererData::AlignmentType::FixedAxis
                                         ? this_render_data->axis
                                         : transform.transform(this_render_data->axis)),
                      0.f);
        const u32 packed_axis = math::pack_snorm_1010102_rev(axis);
        for (usize i = 0; i < end_index; ++i) {
            data_slice[i].axis = packed_axis;
        }
    } else if (this_render_data->alignment ==
               BillboardParticleRendererData::AlignmentType::Velocity) {
        const glm::vec4* __restrict velocity = data.m_velocity;
        for (usize i = 0; i < end_index; ++i) {
            data_slice[i].axis = math::pack_snorm_1010102_rev(glm::normalize(velocity[i]));
        }
    }
}

Result<NotMyPtr<threed::Command>> BillboardParticleRenderer::create_commands(
    const ParticleData& data,
    const ParticleRenderData* render_data,
    ParticleRendererV2::CommandData& command_data) const {
    const BillboardParticleRendererData* this_render_data =
        static_cast<const BillboardParticleRendererData*>(render_data);

    const usize end_index = data.alive_end_index();
    auto command = command_data.device.allocate_command(command_data.pass);
    command->effect = m_effect;
    command->program = m_gpuprog;
    command->drawable = m_drawable;
    command->draw.instance_count = end_index;

    m_vp_binder = command_data.camera.projection_view_matrix();
    m_image_binder = this_render_data->texture;
    m_image_binder.m_sampler = m_sampler;
    m_camera_pos_binder = command_data.camera.position();
    if (this_render_data->alignment == BillboardParticleRendererData::AlignmentType::Camera) {
        m_mode_binder = 0;
        m_view_transposed_binder = glm::transpose(glm::mat3(command_data.camera.view_matrix()));
    } else {
        m_mode_binder = 1;
    }
    m_binders.bind(*command, command_data.device);

    threed::DrawableUpdateParams update_params;
    update_params.data_buffers[0].buffer = command_data.buffer;
    update_params.data_buffers[0].offset = command_data.buffer_offset;
    update_params.data_buffers[0].stride = sizeof(Data);
    update_params.data_buffers[0].instance_divisor = 1;

    auto r_update_handle = command_data.device.allocate_drawable_update(update_params);
    if (!r_update_handle) {
        MODUS_LOGE("Failed to allocate drawable update handle");
        return Error<>();
    }
    command->drawable_update = *r_update_handle;
    return Ok(NotMyPtr<threed::Command>(command));
}
}    // namespace modus::engine::graphics
