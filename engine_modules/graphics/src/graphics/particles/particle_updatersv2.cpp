/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/particles/particle_updatersv2.hpp>
// clang-format on

namespace modus::engine::graphics {

void BasicVelUpdater::update(const ParticleDTType dt, ParticleData& particles) {
    glm::vec4* __restrict vel = particles.m_velocity;
    glm::vec4* __restrict pos = particles.m_position;
    const usize end_index = particles.alive_end_index();
    for (usize i = particles.alive_start_index(); i < end_index; ++i) {
        pos[i] += f32(dt.count()) * vel[i];
    }
}

void EulerUpdater::update(const ParticleDTType dt, ParticleData& particles) {
    const glm::vec4 global_acceleration = glm::vec4(f32(dt.count()) * m_global_acceleration, 0.f);
    glm::vec4* __restrict vel = particles.m_velocity;
    glm::vec4* __restrict acc = particles.m_acceleration;
    glm::vec4* __restrict pos = particles.m_position;
    const usize end_index = particles.alive_end_index();
    for (usize i = particles.alive_start_index(); i < end_index; ++i) {
        acc[i] += global_acceleration;
        vel[i] += f32(dt.count()) * acc[i];
        pos[i] += f32(dt.count()) * vel[i];
    }
}
void modus::engine::graphics::BasicColorUpdater::update(
    const modus::engine::graphics::ParticleDTType,
    modus::engine::graphics::ParticleData& particles) {
    glm::vec4* __restrict color = particles.m_color;
    glm::vec4* __restrict start_color = particles.m_start_color;
    glm::vec4* __restrict end_color = particles.m_end_color;
    glm::vec4* __restrict time = particles.m_time;
    const usize end_index = particles.alive_end_index();
    for (usize i = particles.alive_start_index(); i < end_index; ++i) {
        color[i] = glm::mix(start_color[i], end_color[i], time[i].z);
    }
}
void BasicTimeUpdater::update(const ParticleDTType dt, ParticleData& particles) {
    glm::vec4* __restrict time = particles.m_time;
    usize end_index = particles.alive_end_index();
    for (usize i = particles.alive_start_index(); i < end_index; ++i) {
        time[i].x -= f32(dt.count());
        time[i].z = 1.0f - (time[i].x * time[i].w);
        if (time[i].x < 0.f) {
            particles.kill(i);
            end_index = particles.alive_end_index();
        }
    }
}
void AnimatedUVDepthUpdater::update(const ParticleDTType, ParticleData& particles) {
    glm::vec4* __restrict time = particles.m_time;
    glm::vec4* __restrict uv = particles.m_uv;
    usize end_index = particles.alive_end_index();
    for (usize i = particles.alive_start_index(); i < end_index; ++i) {
        const f32 depth = std::min(m_max_depth, glm::floor(time[i].z / m_update_interval));
        uv[i].z = depth;
    }
}

}    // namespace modus::engine::graphics
