/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/particles/particle_render_command_generator.hpp>
// clang-format on

#include <engine/graphics/particles/particle_registryv2.hpp>
#include <engine/graphics/particles/particle_renderersv2.hpp>
#include <threed/device.hpp>
#include <threed/buffer.hpp>
#include <engine/graphics/camera.hpp>

namespace modus::engine::graphics {

ParticleRenderCommandGenerator::ParticleRenderCommandGenerator() {
    m_data_buffers.reserve(4);
    m_sorted_particles.reserve(16);
}
ParticleRenderCommandGenerator::~ParticleRenderCommandGenerator() {
    modus_assert(m_data_buffers.empty());
}
Result<> ParticleRenderCommandGenerator::initialize(threed::Device& device,
                                                    const u32 buffer_size,
                                                    const u32 alloc_count) {
    if (buffer_size == 0) {
        return Error<>();
    }
    m_buffer_size = buffer_size;
    m_current_buffer_idx = 0;
    m_staging_buffer.resize(buffer_size);

    for (u32 i = 0; i < std::max(alloc_count, 1u); ++i) {
        if (!alloc_buffer(device)) {
            return Error<>();
        }
    }

    // initialize renderers
    {
        std::unique_ptr<PointParticleRenderer> renderer =
            modus::make_unique<PointParticleRenderer>();
        if (!renderer->initialize(device)) {
            MODUS_LOGE("Failed to initialize point particle renderer");
            return Error<>();
        }
        m_renderers[u32(ParticleRendererType::Point)] = std::move(renderer);
    }
    {
        std::unique_ptr<BillboardParticleRenderer> renderer =
            modus::make_unique<BillboardParticleRenderer>();
        if (!renderer->initialize(device)) {
            MODUS_LOGE("Failed to initialize billboard particle renderer");
            return Error<>();
        }
        m_renderers[u32(ParticleRendererType::Billboard)] = std::move(renderer);
    }

#if defined(MODUS_ENABLE_ASSERTS)
    for (u32 i = 0; i < u32(ParticleRendererType::Total); ++i) {
        modus_assert_message(m_renderers[i], "Particle renderer has not been initialized");
    }
#endif
    return Ok<>();
}

void ParticleRenderCommandGenerator::shutdown(threed::Device& device) {
    for (auto& buffer : m_data_buffers) {
        device.destroy_buffer(buffer);
    }
    m_data_buffers.clear();
    m_buffer_size = 0;
    m_current_buffer_idx = 0;
    for (auto& renderer : m_renderers) {
        if (renderer) {
            renderer->shutdown(device);
            renderer.reset();
        }
    }
}

void ParticleRenderCommandGenerator::generate_commands(const ParticleRegistryV2& registry,
                                                       threed::Device& device,
                                                       threed::Pass& pass,
                                                       const Camera& camera) {
    if (registry.instance_count() == 0) {
        return;
    }

    m_sorted_particles.clear();
    m_current_buffer_idx = 0;
    u32 buffer_offset = 0;

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Particle Sort");
        // Sort particle from back to front
        registry.for_each([this, &camera](const ParticleEmitterV2& emitter) {
            const f32 z_distance =
                glm::distance(camera.position(), emitter.transform.translation());
            m_sorted_particles.push_back(ParticleSortData{&emitter, z_distance});
        });

        std::sort(m_sorted_particles.begin(), m_sorted_particles.end(),
                  [](const ParticleSortData& v1, const ParticleSortData& v2) {
                      return v1.z_distance > v2.z_distance;
                  });
    }

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("Particle Render Command gen");
        // execute render commands
        for (const auto& emitter : m_sorted_particles) {
            for (const auto& instance : emitter.emitter->instances) {
                modus_assert(instance.renderer_type != ParticleRendererType::Total);
                modus_assert(m_renderers[u32(instance.renderer_type)]);

                auto& renderer = *m_renderers[u32(instance.renderer_type)];

                const u32 required_buffer_size =
                    renderer.render_data_size(instance.data, instance.render_data.get());
                modus_assert(required_buffer_size < m_buffer_size);
                if (buffer_offset + required_buffer_size > m_buffer_size) {
                    if (m_current_buffer_idx + 1 >= m_data_buffers.size()) {
                        // allocate new buffer
                        if (!alloc_buffer(device)) {
                            MODUS_LOGE("Failed to allocate new particle data buffer");
                            return;
                        }

                        // flush current staging buffer
                        if (!device.update_buffer(
                                m_data_buffers[m_current_buffer_idx],
                                Slice(m_staging_buffer.data(), buffer_offset).as_bytes(), 0,
                                true)) {
                            MODUS_LOGE("Failed to update particle data buffer {}",
                                       m_current_buffer_idx);
                            return;
                        }
                    }
                    m_current_buffer_idx++;
                    buffer_offset = 0;
                }
                const u32 buffer_offset_before_update = buffer_offset;
                buffer_offset += required_buffer_size;
                modus_assert(buffer_offset % 4 == 0);
                ByteSliceMut slice = ByteSliceMut(
                    m_staging_buffer.data() + buffer_offset_before_update, required_buffer_size);
                renderer.fill_buffer(slice, instance.data, emitter.emitter->transform,
                                     instance.render_data.get(), camera);

                ParticleRendererV2::CommandData command_data = {
                    device, pass, camera, m_data_buffers[m_current_buffer_idx],
                    buffer_offset_before_update};

                if (!renderer.create_commands(instance.data, instance.render_data.get(),
                                              command_data)) {
                    MODUS_LOGE("Failed to generate particle draw command {}", m_current_buffer_idx);
                    return;
                }
            }
        }

        // flush any remaining changes
        if (buffer_offset != 0) {
            if (!device.update_buffer(m_data_buffers[m_current_buffer_idx],
                                      Slice(m_staging_buffer.data(), buffer_offset).as_bytes(), 0,
                                      true)) {
                MODUS_LOGE("Failed to update particle data buffer {}", m_current_buffer_idx);
                return;
            }
        }
    }
}

Result<threed::BufferHandle> ParticleRenderCommandGenerator::alloc_buffer(threed::Device& device) {
    threed::BufferCreateParams params;
    params.type = threed::BufferType::Data;
    params.usage = threed::BufferUsage::Stream;
    params.size = m_buffer_size;

    auto r_buffer = device.create_buffer(params);
    if (!r_buffer) {
        return Error<>();
    }
    m_data_buffers.push_back(*r_buffer);
    return r_buffer;
}
}    // namespace modus::engine::graphics
