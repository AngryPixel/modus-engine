/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/particles/particlev2.hpp>

namespace modus::engine::graphics {

void ParticleSystemV2::update(const ParticleDTType dt,
                              ParticleData& data,
                              const math::Transform& transform) {
    const usize max_new_particles = usize(dt.count() * m_emission_rate);
    const usize start_index = data.dead_start_index();
    const usize end_index = std::min(start_index + max_new_particles, data.count());
    if (start_index != end_index) {
        // emit new particles
        for (auto& generator : m_generators) {
            generator->generate(dt, data, transform, start_index, end_index);
        }
        // Wake particles
        for (usize i = start_index; i < end_index; ++i) {
            data.wake(i);
        }
    }

    // Update
    for (auto& up : m_updaters) {
        up->update(dt, data);
    }
}

}    // namespace modus::engine::graphics
