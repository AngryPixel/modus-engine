/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/graphics/default_program_inputs.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/mesh_generator.hpp>
#include <threed/buffer.hpp>
#include <threed/device.hpp>
#include <threed/drawable.hpp>

namespace modus::engine::graphics {

struct VertexData {
    glm::vec3 vertex;
    glm::vec3 normal;
    glm::vec2 texture;
};

Result<> generate_sphere(Mesh& mesh,
                         threed::Device& device,
                         const f32 radius,
                         const u32 sectors,
                         const u32 stacks) {
    const f32 pi = 3.1415926f;
    const u32 stack_count = std::max(u32(3), stacks);
    const u32 sector_count = std::max(u32(3), sectors);
    graphics::Vector<VertexData> data;
    graphics::Vector<u16> indices;

    // taken from https://www.songho.ca/opengl/gl_sphere.html
    // Gen sphere
    f32 x, y, z, xy;                              // vertex position
    f32 nx, ny, nz, lengthInv = 1.0f / radius;    // vertex normal
    f32 s, t;                                     // vertex texCoord

    const f32 sector_step = 2 * pi / sector_count;
    const f32 stack_step = pi / stack_count;
    f32 sector_angle, stack_angle;

    data.resize((stack_count + 1) * (sector_count + 1));
    auto vd_it = data.begin();
    for (u32 i = 0; i <= stack_count; ++i) {
        stack_angle = pi / 2.0f - f32(i) * stack_step;    // starting from pi/2 to -pi/2
        xy = radius * cosf(stack_angle);                  // r * cos(u)
        z = radius * sinf(stack_angle);                   // r * sin(u)

        // add (sector_count+1) vertices per stack
        // the first and last vertices have same position and normal, but
        // different tex coords
        for (u32 j = 0; j <= sector_count; ++j) {
            VertexData& vd = *vd_it;
            sector_angle = f32(j) * sector_step;    // starting from 0 to 2pi

            // vertex position (x, y, z)
            x = xy * cosf(sector_angle);    // r * cos(u) * cos(v)
            y = xy * sinf(sector_angle);    // r * cos(u) * sin(v)

            vd.vertex.x = x;
            vd.vertex.y = y;
            vd.vertex.z = z;

            // normalized vertex normal (nx, ny, nz)
            nx = x * lengthInv;
            ny = y * lengthInv;
            nz = z * lengthInv;

            vd.normal.x = nx;
            vd.normal.y = ny;
            vd.normal.z = nz;

            // vertex tex coord (s, t) range between [0, 1]
            s = f32(j) / sector_count;
            t = f32(i) / stack_count;

            vd.texture.x = s;
            vd.texture.y = t;
            ++vd_it;
        }
    }

    modus_assert(data.size() < usize(std::numeric_limits<u16>::max()));

    // gen indices
    indices.reserve(sector_count * stack_count * 6);
    u16 k1, k2;
    for (u16 i = 0; i < u16(stack_count); ++i) {
        k1 = i * u16(sector_count + 1);     // beginning of current stack
        k2 = k1 + u16(sector_count) + 1;    // beginning of next stack

        for (u16 j = 0; j < u16(sector_count); ++j, ++k1, ++k2) {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if (i != 0) {
                indices.push_back(k1);
                indices.push_back(k2);
                indices.push_back(k1 + 1);
            }
            // k1+1 => k2 => k2+1
            if (i != (stack_count - 1)) {
                indices.push_back(k1 + 1);
                indices.push_back(k2);
                indices.push_back(k2 + 1);
            }
        }
    }

    // Create Device data
    threed::BufferCreateParams params_vertices;
    threed::BufferCreateParams params_indices;

    params_vertices.size = sizeof(VertexData) * data.size();
    params_vertices.type = threed::BufferType::Data;
    params_vertices.usage = threed::BufferUsage::Static;
    params_vertices.data = data.data();

    params_indices.size = sizeof(u16) * indices.size();
    params_indices.type = threed::BufferType::Indices;
    params_indices.usage = threed::BufferUsage::Static;
    params_indices.data = indices.data();

    auto vertices_result = device.create_buffer(params_vertices);
    if (!vertices_result) {
        return Error<>();
    }

    auto indices_result = device.create_buffer(params_indices);
    if (!indices_result) {
        return Error<>();
    }

    math::bv::Sphere sphere;
    sphere.center = glm::vec3(0.0f);
    sphere.radius = radius;

    mesh.vertices[0] = vertices_result.value();
    mesh.indices[0] = indices_result.value();
    mesh.bounding_sphere = sphere;

    mesh.sub_meshes.push_back(SubMesh()).expect("Failed to add submesh");
    SubMesh& sub_mesh = mesh.sub_meshes[0];
    threed::DrawableCreateParams drawable_params;

    drawable_params.data_buffers[0].offset = 0;
    drawable_params.data_buffers[0].stride = sizeof(f32) * 8;
    drawable_params.data_buffers[0].buffer = *vertices_result;

    drawable_params.data[kVertexSlot].offset = 0;
    drawable_params.data[kVertexSlot].data_type = threed::DataType::Vec3F32;
    drawable_params.data[kVertexSlot].buffer_index = 0;

    drawable_params.data[kNormalSlot].offset = sizeof(glm::vec3);
    drawable_params.data[kNormalSlot].data_type = threed::DataType::Vec3F32;
    drawable_params.data[kNormalSlot].buffer_index = 0;

    drawable_params.data[kTextureSlot].offset = sizeof(glm::vec3) * 2;
    drawable_params.data[kTextureSlot].data_type = threed::DataType::Vec3F32;
    drawable_params.data[kTextureSlot].buffer_index = 0;

    drawable_params.index_type = threed::IndicesType::U16;
    drawable_params.start = 0;
    drawable_params.count = indices.size();
    drawable_params.primitive = threed::Primitive::Triangles;
    drawable_params.index_buffer.buffer = indices_result.value();

    auto r_drawable = device.create_drawable(drawable_params);
    if (!r_drawable) {
        MODUS_LOGE("Failed to create sphere drawable: {}", r_drawable.error());
        return Error<>();
    }

    sub_mesh.drawable = r_drawable.value();

    return Ok<>();
}

Result<> generate_cube(Mesh& mesh, threed::Device& device, const f32 radius) {
    const float vertices[] = {
        // positions          // normals           // texture coords
        radius,  radius,  -radius, 0.0f,    0.0f,    -1.0f,   1.0f,    1.0f,    radius,  -radius,
        -radius, 0.0f,    0.0f,    -1.0f,   1.0f,    0.0f,    -radius, -radius, -radius, 0.0f,
        0.0f,    -1.0f,   0.0f,    0.0f,    -radius, -radius, -radius, 0.0f,    0.0f,    -1.0f,
        0.0f,    0.0f,    -radius, radius,  -radius, 0.0f,    0.0f,    -1.0f,   0.0f,    1.0f,
        radius,  radius,  -radius, 0.0f,    0.0f,    -1.0f,   1.0f,    1.0f,

        -radius, -radius, radius,  0.0f,    0.0f,    1.0f,    0.0f,    0.0f,    radius,  -radius,
        radius,  0.0f,    0.0f,    1.0f,    1.0f,    0.0f,    radius,  radius,  radius,  0.0f,
        0.0f,    1.0f,    1.0f,    1.0f,    radius,  radius,  radius,  0.0f,    0.0f,    1.0f,
        1.0f,    1.0f,    -radius, radius,  radius,  0.0f,    0.0f,    1.0f,    0.0f,    1.0f,
        -radius, -radius, radius,  0.0f,    0.0f,    1.0f,    0.0f,    0.0f,

        -radius, radius,  radius,  -1.0f,   0.0f,    0.0f,    1.0f,    0.0f,    -radius, radius,
        -radius, -1.0f,   0.0f,    0.0f,    1.0f,    1.0f,    -radius, -radius, -radius, -1.0f,
        0.0f,    0.0f,    0.0f,    1.0f,    -radius, -radius, -radius, -1.0f,   0.0f,    0.0f,
        0.0f,    1.0f,    -radius, -radius, radius,  -1.0f,   0.0f,    0.0f,    0.0f,    0.0f,
        -radius, radius,  radius,  -1.0f,   0.0f,    0.0f,    1.0f,    0.0f,

        radius,  -radius, -radius, 1.0f,    0.0f,    0.0f,    0.0f,    1.0f,    radius,  radius,
        -radius, 1.0f,    0.0f,    0.0f,    1.0f,    1.0f,    radius,  radius,  radius,  1.0f,
        0.0f,    0.0f,    1.0f,    0.0f,    radius,  radius,  radius,  1.0f,    0.0f,    0.0f,
        1.0f,    0.0f,    radius,  -radius, radius,  1.0f,    0.0f,    0.0f,    0.0f,    0.0f,
        radius,  -radius, -radius, 1.0f,    0.0f,    0.0f,    0.0f,    1.0f,

        -radius, -radius, -radius, 0.0f,    -1.0f,   0.0f,    0.0f,    1.0f,    radius,  -radius,
        -radius, 0.0f,    -1.0f,   0.0f,    1.0f,    1.0f,    radius,  -radius, radius,  0.0f,
        -1.0f,   0.0f,    1.0f,    0.0f,    radius,  -radius, radius,  0.0f,    -1.0f,   0.0f,
        1.0f,    0.0f,    -radius, -radius, radius,  0.0f,    -1.0f,   0.0f,    0.0f,    0.0f,
        -radius, -radius, -radius, 0.0f,    -1.0f,   0.0f,    0.0f,    1.0f,

        radius,  radius,  radius,  0.0f,    1.0f,    0.0f,    1.0f,    0.0f,    radius,  radius,
        -radius, 0.0f,    1.0f,    0.0f,    1.0f,    1.0f,    -radius, radius,  -radius, 0.0f,
        1.0f,    0.0f,    0.0f,    1.0f,    -radius, radius,  -radius, 0.0f,    1.0f,    0.0f,
        0.0f,    1.0f,    -radius, radius,  radius,  0.0f,    1.0f,    0.0f,    0.0f,    0.0f,
        radius,  radius,  radius,  0.0f,    1.0f,    0.0f,    1.0f,    0.0f,

    };

    math::bv::Sphere sphere;

    for (u32 i = 0; i < sizeof(vertices) / sizeof(f32); i += 8) {
        math::bv::merge(sphere, glm::vec3(vertices[i], vertices[i + 1], vertices[i + 2]));
    }

    // Create Device data
    threed::BufferCreateParams params_vertices;

    params_vertices.size = sizeof(vertices);
    params_vertices.type = threed::BufferType::Data;
    params_vertices.usage = threed::BufferUsage::Static;
    params_vertices.data = vertices;

    auto vertices_result = device.create_buffer(params_vertices);
    if (!vertices_result) {
        return Error<>();
    }

    mesh.vertices[0] = vertices_result.value();
    mesh.bounding_sphere = sphere;

    mesh.sub_meshes.push_back(SubMesh()).expect("Failed to add submesh");
    SubMesh& sub_mesh = mesh.sub_meshes[0];

    threed::DrawableCreateParams drawable_params;
    drawable_params.data_buffers[0].offset = 0;
    drawable_params.data_buffers[0].stride = sizeof(f32) * 8;
    drawable_params.data_buffers[0].buffer = *vertices_result;

    drawable_params.data[kVertexSlot].offset = 0;
    drawable_params.data[kVertexSlot].data_type = threed::DataType::Vec3F32;

    drawable_params.data[kNormalSlot].offset = sizeof(glm::vec3);
    drawable_params.data[kNormalSlot].data_type = threed::DataType::Vec3F32;

    drawable_params.data[kTextureSlot].offset = sizeof(glm::vec3) * 2;
    drawable_params.data[kTextureSlot].data_type = threed::DataType::Vec2F32;

    drawable_params.index_type = threed::IndicesType::None;
    drawable_params.start = 0;
    drawable_params.count = 36;
    ;
    drawable_params.primitive = threed::Primitive::Triangles;

    auto r_drawable = device.create_drawable(drawable_params);
    if (!r_drawable) {
        MODUS_LOGE("Failed to create cube drawable: {}", r_drawable.error());
        return Error<>();
    }

    sub_mesh.drawable = r_drawable.value();

    return Ok<>();
}

}    // namespace modus::engine::graphics
