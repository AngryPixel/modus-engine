/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/animation_buffer_cache.hpp>
#include <threed/device.hpp>
#include <threed/buffer.hpp>
#include <engine/animation/animation_types.hpp>
#include <core/io/byte_stream.hpp>

namespace modus::engine::graphics {

static const char* kAnimatioShaderCode = R"R(
#define MODUS_ANIMATION_ENABLED
#define MODUS_ANIMATION_MAX_SKELETON_BONES 16

mat4 get_bone_transform() {
    mat4 bone_transform = u_bone_transforms[BONE_INDICES[0]] * BONE_WEIGHTS[0];
    bone_transform += u_bone_transforms[BONE_INDICES[1]] * BONE_WEIGHTS[1];
    bone_transform += u_bone_transforms[BONE_INDICES[2]] * BONE_WEIGHTS[2];
    bone_transform += u_bone_transforms[BONE_INDICES[3]] * BONE_WEIGHTS[3];
    return bone_transform;
}
)R";

AnimationCBufferInput::AnimationCBufferInput(NotMyPtr<const threed::dynamic::InputBinder> binder)
    : threed::dynamic::DefaultConstantBufferInput("SkeletonAnimData", binder) {
    m_desc.buffer_members.push_back({"u_bone_transforms", threed::DataType::Total,
                                     threed::dynamic::PrecisionQualifier::Default,
                                     threed::dynamic::BufferMemberDesc::Type::Custom, "mat4[16]"});
}

Result<threed::dynamic::BindableInputResult> AnimationCBufferInput::generate_code_snippet(
    threed::dynamic::GenerateParams& params) const {
    if (!params.writer->write_exactly(StringSlice(kAnimatioShaderCode).as_bytes())) {
        return Error<>();
    }
    return Ok(threed::dynamic::BindableInputResult::Generated);
}

AnimationBufferCache::~AnimationBufferCache() {
    modus_assert(!m_identity_buffer);
}

static Result<threed::ConstantBufferHandle, void> create_buffer_local(threed::Device& device) {
    threed::ConstantBufferCreateParams params;
    Array<glm::mat4, animation::kMaxSkeletonBones> identity_data;
    identity_data.fill(glm::mat4(1));
    params.usage = threed::BufferUsage::Stream;
    params.size = sizeof(identity_data);
    params.data = identity_data.data();
    return device.create_constant_buffer(params);
}

static Result<threed::ConstantBufferHandle, void> create_identity(threed::Device& device) {
    Array<glm::mat4, animation::kMaxSkeletonBones> identity_data;
    identity_data.fill(glm::mat4(1));
    threed::ConstantBufferCreateParams params;
    params.usage = threed::BufferUsage::Static;
    params.size = sizeof(identity_data);
    params.data = identity_data.data();
    return device.create_constant_buffer(params);
}

Result<> AnimationBufferCache::initialize(threed::Device& device) {
    if (auto r_buffer = create_identity(device); !r_buffer) {
        MODUS_LOGE("AnimationBufferCache: Failed to create identity_buffer");
        return Error<>();
    } else {
        m_identity_buffer = *r_buffer;
    }

    return Ok<>();
}

void AnimationBufferCache::shutdown(threed::Device& device) {
    if (m_identity_buffer) {
        device.destroy_constant_buffer(m_identity_buffer);
        m_identity_buffer = threed::ConstantBufferHandle();
    }
}

Result<threed::ConstantBufferHandle, void> AnimationBufferCache::create_buffer(
    threed::Device& device) {
    auto r_buffer = create_buffer_local(device);
    if (!r_buffer) {
        MODUS_LOGE(
            "AnimationBufferCache::request_buffer: Failed to create new "
            "buffer\n");
        return Error<>();
    }
    return Ok(*r_buffer);
}

Result<> AnimationBufferCache::update_buffer(threed::Device& device,
                                             threed::ConstantBufferHandle buffer,
                                             const Slice<glm::mat4> data) {
    if (data.size() > animation::kMaxSkeletonBones) {
        MODUS_LOGW(
            "AnimationBufferCache: Attempting to upload more bone matrices "
            "than supported, constant buffer will be truncated\n");
    }

    const u32 data_size =
        std::min(u32(data.size()), u32(animation::kMaxSkeletonBones)) * sizeof(glm::mat4);
    if (!device.update_constant_buffer(buffer, data.data(), data_size)) {
        MODUS_LOGE("AnimationBufferCache: Failed to upload data to constant buffer\n");
        return Error<>();
    }
    return Ok<>();
}

// Use when the mesh does not have any active animations
threed::ConstantBufferHandle AnimationBufferCache::identity_buffer() const {
    return m_identity_buffer;
}

}    // namespace modus::engine::graphics
