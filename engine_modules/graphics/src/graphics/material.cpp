/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/graphics/material.hpp>
#include <threed/buffer.hpp>

namespace modus::engine::graphics {

enum BlockFlags {
    kHasDiffuseMap = 1 << 0,
    kHasSpecularMap = 1 << 1,
    kHasNormalMap = 1 << 2,
    kHasEmissiveMap = 1 << 3
};

// Materials V2 --------------------------------------------------------------

Material::Material(StringSlice guid, StringSlice name)
    : m_guid(GID::from_string(guid).value_or_panic("Invalid GUID for Material")),
      m_name(name),
      m_material_handle(),
      m_flags(0),
      m_effect_handle() {}

void Material::set_material_handle(const MaterialHandle h) {
    m_material_handle = h;
}

MaterialInstance::MaterialInstance(const MaterialHandle handle, const u32 flags)
    : m_material_handle(handle), m_flags(flags) {}

Result<MaterialHandle, void> MaterialDB::add(MaterialPtr material) {
    modus_assert(material);
    auto r_add = m_materials.add(material->guid(), material);
    if (r_add) {
        material->set_material_handle(*r_add);
    }
    return r_add;
}

Result<ConstMaterialPtr, void> MaterialDB::get(const GID& guid) const {
    auto r = m_materials.get(guid);
    if (!r) {
        return Error<>();
    }
    return Ok<ConstMaterialPtr>(**r);
}

Result<ConstMaterialPtr, void> MaterialDB::get(const MaterialHandle h) const {
    auto r = m_materials.get(h);
    if (!r) {
        return Error<>();
    }
    return Ok<ConstMaterialPtr>(**r);
}

Result<> MaterialDB::set_default_material_handle(MaterialHandle h) {
    if (m_default_material_handle) {
        MODUS_LOGE("Default material has already been set, this can not be changed");
        return Error<>();
    }

    auto r = m_materials.get(h);
    if (!r) {
        return Error<>();
    }

    auto material = **r;
    auto r_instance = material->new_instance();
    if (!r_instance) {
        MODUS_LOGE("Failed to create default material instance: {}", material->name());
        return Error<>();
    }
    m_default_instance_ref = MaterialInstanceRef(h, *r_instance, material->flags());
    m_default_material_handle = h;
    return Ok<>();
}

Result<MaterialInstanceRef, void> MaterialDB::create_instance(const MaterialHandle h) {
    auto r = m_materials.get(h);
    if (!r) {
        return Error<>();
    }
    NotMyPtr<Material> material = **r;
    auto r_instance = material->new_instance();
    if (!r_instance) {
        return Error<>();
    }
    return Ok(MaterialInstanceRef(material->material_handle(), *r_instance, material->flags()));
}

Result<MaterialInstancePtr, void> MaterialDB::instance(const MaterialInstanceRef& ref) {
    auto r = m_materials.get(ref.material_handle());
    if (!r) {
        return Error<>();
    }
    NotMyPtr<Material> material = **r;
    return material->instance(ref.m_instance_handle);
}

Result<ConstMaterialInstancePtr, void> MaterialDB::instance(const MaterialInstanceRef& ref) const {
    auto r = m_materials.get(ref.material_handle());
    if (!r) {
        return Error<>();
    }
    NotMyPtr<const Material> material = **r;
    return material->instance(ref.m_instance_handle);
}

Result<MaterialInstanceRef, void> MaterialDB::create_instance(const GID& guid) {
    auto r = m_materials.get(guid);
    if (!r) {
        return Error<>();
    }
    NotMyPtr<Material> material = **r;
    auto r_instance = material->new_instance();
    if (!r_instance) {
        return Error<>();
    }
    return Ok(MaterialInstanceRef(material->material_handle(), *r_instance, material->flags()));
}

ConstMaterialPtr MaterialDB::default_material() const {
    return get(m_default_material_handle).value_or_panic("Could not locate default material");
}

void MaterialDB::erase(ConstMaterialPtr material) {
    if (material) {
        m_materials.erase(material->guid());
    }
}

void MaterialDB::destroy_instance(MaterialInstanceRef instance) {
    if (instance) {
        auto r_add = m_materials.get(instance.material_handle());
        if (!r_add) {
            MODUS_LOGE("MaterialDB: Could not locate material type for instance");
            return;
        }
        if (!(**r_add)->delete_intsance(instance.m_instance_handle)) {
            MODUS_LOGE("MaterialDB: Failed to delete instance of material type: {}",
                       (**r_add)->name());
        }
    }
}

Result<std::pair<ConstMaterialInstancePtr, ConstMaterialPtr>, void>
MaterialDB::instance_and_material(const MaterialInstanceRef& ref) const {
    if (!ref) {
        return Error<>();
    }
    const auto r_material = m_materials.get(ref.material_handle());
    if (!r_material) {
        return Error<>();
    }
    const auto r_instance = (**r_material)->instance(ref.m_instance_handle);
    if (!r_instance) {
        return Error<>();
    }
    return Ok<std::pair<ConstMaterialInstancePtr, ConstMaterialPtr>>({*r_instance, **r_material});
}

void MaterialDB::clear() {
    m_materials.clear();
}

MaterialInstanceRef::MaterialInstanceRef(const MaterialHandle mh,
                                         const MaterialInstanceHandle ih,
                                         const u32 flags)
    : m_material_handle(mh), m_instance_handle(ih), m_flags(flags) {}
// Properties

PropertyValue::PropertyValue() : m_type(Type::None) {}

PropertyValue::PropertyValue(const bool v) : m_type(Type::Bool) {
    m_data.b = v;
}
PropertyValue::PropertyValue(const u32 v) : m_type(Type::U32) {
    m_data.u = v;
}
PropertyValue::PropertyValue(const f32 f) : m_type(Type::F32) {
    m_data.f = f;
}
PropertyValue::PropertyValue(const glm::vec2& v) : m_type(Type::Vec2) {
    m_data.vec.x = v.x;
    m_data.vec.y = v.y;
}
PropertyValue::PropertyValue(const glm::vec3& v) : m_type(Type::Vec3) {
    m_data.vec.x = v.x;
    m_data.vec.y = v.y;
    m_data.vec.z = v.z;
}
PropertyValue::PropertyValue(const glm::vec4& v) : m_type(Type::Vec4) {
    m_data.vec.x = v.x;
    m_data.vec.y = v.y;
    m_data.vec.z = v.z;
    m_data.vec.w = v.w;
}

PropertyValue& PropertyValue::operator=(const bool v) {
    m_type = Type::Bool;
    m_data.b = v;
    return *this;
}

PropertyValue& PropertyValue::operator=(const u32 v) {
    m_type = Type::U32;
    m_data.u = v;
    return *this;
}

PropertyValue& PropertyValue::operator=(const f32 f) {
    m_type = Type::F32;
    m_data.f = f;
    return *this;
}
PropertyValue& PropertyValue::operator=(const glm::vec2& v) {
    m_type = Type::Vec2;
    m_data.vec.x = v.x;
    m_data.vec.y = v.y;
    return *this;
}
PropertyValue& PropertyValue::operator=(const glm::vec3& v) {
    m_type = Type::Vec3;
    m_data.vec.x = v.x;
    m_data.vec.y = v.y;
    m_data.vec.z = v.z;
    return *this;
}

PropertyValue& PropertyValue::operator=(const glm::vec4& v) {
    m_type = Type::Vec4;
    m_data.vec.x = v.x;
    m_data.vec.y = v.y;
    m_data.vec.z = v.z;
    m_data.vec.w = v.w;
    return *this;
}

Result<bool, void> PropertyValue::as_bool() const {
    if (m_type != Type::Bool) {
        return Error<>();
    }
    return Ok(m_data.b);
}
Result<u32, void> PropertyValue::as_u32() const {
    if (m_type != Type::U32) {
        return Error<>();
    }
    return Ok(m_data.u);
}
Result<f32, void> PropertyValue::as_f32() const {
    if (m_type != Type::F32) {
        return Error<>();
    }
    return Ok(m_data.f);
}
Result<glm::vec2, void> PropertyValue::as_vec2() const {
    if (m_type != Type::Vec2) {
        return Error<>();
    }
    return Ok(glm::vec2(m_data.vec.x, m_data.vec.y));
}
Result<glm::vec3, void> PropertyValue::as_vec3() const {
    if (m_type != Type::Vec3) {
        return Error<>();
    }
    return Ok(glm::vec3(m_data.vec.x, m_data.vec.y, m_data.vec.z));
}
Result<glm::vec4, void> PropertyValue::as_vec4() const {
    if (m_type != Type::Vec4) {
        return Error<>();
    }
    return Ok(glm::vec4(m_data.vec.x, m_data.vec.y, m_data.vec.z, m_data.vec.w));
}

MaterialProperty::MaterialProperty(const StringSlice name) : m_name(name) {}

Result<> MaterialInstance::set_property(const StringSlice name, const PropertyValue& property) {
    auto r_prop = find_property(name);
    if (!r_prop) {
        return Error<>();
    }
    return (*r_prop)->set(property);
}

void MaterialInstance::apply_instanced(Engine&, MaterialInstancedApplyParams&) const {
    MODUS_LOGE("Material Instanced called without implementation");
}

Result<PropertyValue, void> MaterialInstance::property(const StringSlice name) const {
    auto r_prop = find_property(name);
    if (!r_prop) {
        return Error<>();
    }
    return Ok((*r_prop)->get());
}

Result<NotMyPtr<MaterialProperty>, void> MaterialInstance::find_property(const StringSlice) {
    return Error<>();
}

Result<NotMyPtr<const MaterialProperty>, void> MaterialInstance::find_property(
    const StringSlice) const {
    return Error<>();
}

}    // namespace modus::engine::graphics
