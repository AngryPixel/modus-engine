/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/ecs/components/graphics_component.hpp>
// clang-format on

#include <engine/graphics/ecs/components/graphics_component_generated.h>

#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/engine.hpp>
#include <engine/graphics/mesh_asset.hpp>
#include <engine/graphics/material_asset.hpp>

namespace modus::engine::graphics {

void GraphicsComponent::on_create(const gameplay::EntityId) {
    mesh_instance.reset();
    material_instance = MaterialInstanceRef();
    tag = GraphicsComponentTag::None;
    visible = true;
    casts_shadow = false;
    z_distance.reset();
}
}    // namespace modus::engine::graphics

namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<engine::graphics::GraphicsComponent>;

Result<typename Traits::IntermediateType> Traits::load(Engine& engine, const StorageType& storage) {
    IntermediateType result;
    const auto* fbs_mesh_path = storage.mesh_path();
    const auto* fbs_atlas_path = storage.atlas_path();
    const auto* fbs_materials_path = storage.materials();

    auto asset_module = engine.module<ModuleAssets>();
    const auto& mesh_db = engine.module<ModuleGraphics>()->mesh_db();

    // Load Mesh
    const StringSlice mesh_path(fbs_mesh_path->c_str(), fbs_mesh_path->size());
    engine::graphics::MeshHandle mesh_handle;
    if (fbs_atlas_path != nullptr) {
        const StringSlice atlas_path(fbs_atlas_path->c_str(), fbs_atlas_path->size());
        const auto r_mesh_atlas =
            asset_module->loader().resolve_by_path_typed<engine::graphics::MeshAtlasAsset>(
                atlas_path);
        if (!r_mesh_atlas) {
            MODUS_LOGE("Couldn't locate mesh atlas: {}", atlas_path);
            return Error<>();
        }
        for (const auto& entry : (*r_mesh_atlas)->m_meshes) {
            if (StringSlice(entry.name) == mesh_path) {
                mesh_handle = entry.mesh_handle;
            }
        }
        if (!mesh_handle) {
            MODUS_LOGE("Couldn't locate mesh '{}' in atlas '{}'", mesh_path, atlas_path);
            return Error<>();
        }
    } else {
        const auto r_mesh =
            asset_module->loader().resolve_by_path_typed<engine::graphics::MeshAsset>(mesh_path);
        if (!r_mesh) {
            MODUS_LOGE("Couldn't locate mesh: {}", mesh_path);
            return Error<>();
        }
        mesh_handle = (*r_mesh)->m_mesh_handle;
    }

    auto r_mesh_instance = mesh_db.new_instance(mesh_handle);
    if (!r_mesh_instance) {
        MODUS_LOGE("Failed to create mesh instance");
        return Error<>();
    }
    result.mesh = *r_mesh_instance;

    // Load Materials
    {
        u32 index = 0;
        for (const auto& material : *fbs_materials_path) {
            const StringSlice material_path(material->c_str(), material->size());
            auto r_material =
                asset_module->loader().resolve_by_path_typed<engine::graphics::MaterialAsset>(
                    material_path);
            if (!r_material) {
                MODUS_LOGE("Couldn't locate material [:02] {}", index, material_path);
                return Error<>();
            }
            if (!result.mesh.materials.push_back((*r_material)->m_material_ref)) {
                MODUS_LOGE("Couldn't add material [:02] to mesh instance. Material limit reached",
                           index);
            }
            index++;
        }
    }

    result.cast_shadow = storage.cast_shadow();
    result.visible = storage.visible();
    const f32 z_distance = storage.z_distance();
    if (z_distance > 0.f) {
        result.z_distance = z_distance;
    }
    return Ok(result);
}

void Traits::unload(Engine&, const IntermediateType&) {
    // nothing to do
}

void Traits::extract_asset_paths(Vector<String>& out, const StorageType& storage) {
    const auto* fbs_mesh_path = storage.mesh_path();
    const auto* fbs_atlas_path = storage.atlas_path();
    const auto* fbs_materials_path = storage.materials();

    if (fbs_atlas_path != nullptr) {
        out.push_back(String(fbs_atlas_path->c_str(), fbs_atlas_path->size()));
    } else if (fbs_mesh_path != nullptr) {
        out.push_back(String(fbs_mesh_path->c_str(), fbs_mesh_path->size()));
    }

    if (fbs_materials_path != nullptr) {
        for (const auto& material_path : *fbs_materials_path) {
            out.push_back(String(material_path->c_str(), material_path->size()));
        }
    }
}

Result<> Traits::to_component(Engine&,
                              ComponentType& out,
                              const IntermediateType& in,
                              const EntityId) {
    out.mesh_instance = in.mesh;
    out.z_distance = in.z_distance;
    out.visible = in.visible;
    out.casts_shadow = in.cast_shadow;
    return Ok<>();
}
}    // namespace modus::engine::gameplay