/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/ecs/components/light_component.hpp>
// clang-format on

#include <engine/graphics/ecs/components/light_component_generated.h>

namespace modus::engine::gameplay {
using Traits = ComponentLoaderTraits<engine::graphics::LightComponent>;

Result<typename Traits::IntermediateType> Traits::load(Engine&, const StorageType& storage) {
    IntermediateType result;

    result.enabled = storage.enabled();
    const auto* fbs_color = storage.color();
    result.light.set_diffuse_color(glm::vec3(fbs_color->x(), fbs_color->y(), fbs_color->z()));
    result.light.set_diffuse_intensity(storage.intensity());
    switch (storage.light_type()) {
        case engine::graphics::fbs::ecs::Light::Ambient: {
            // nothing to do
            result.light.set_type(engine::graphics::LightType::Ambient);
            break;
        }
        case engine::graphics::fbs::ecs::Light::Point: {
            const auto* fbs_point_light = storage.light_as_Point();
            if (fbs_point_light == nullptr) {
                MODUS_LOGE("Type is Point Light, but no point light data present ");
                return Error<>();
            }
            result.light.set_type(engine::graphics::LightType::Point);
            const auto* fbs_position = fbs_point_light->position();
            if (fbs_position != nullptr) {
                result.light.set_position(
                    glm::vec3(fbs_position->x(), fbs_position->y(), fbs_position->z()));
            }
            result.light.set_point_light_radius(fbs_point_light->radius());
            break;
        }
        case engine::graphics::fbs::ecs::Light::Directional: {
            const auto* fbs_dir_light = storage.light_as_Directional();
            if (fbs_dir_light == nullptr) {
                MODUS_LOGE("Type is Directional Light, but no directional light data present ");
                return Error<>();
            }
            result.light.set_type(engine::graphics::LightType::Directional);
            result.light.set_directional_light_area(fbs_dir_light->area_x(),
                                                    fbs_dir_light->area_y());
            result.light.set_directional_light_near_far(fbs_dir_light->near_plane(),
                                                        fbs_dir_light->far_plane());
            const auto* fbs_position = fbs_dir_light->position();
            result.light.set_position(
                glm::vec3(fbs_position->x(), fbs_position->y(), fbs_position->z()));
            break;
        }
        default:
            MODUS_LOGE("Unknown light type:{}", u32(storage.light_type()));
            return Error<>();
    }
    return Ok(result);
}

}    // namespace modus::engine::gameplay
