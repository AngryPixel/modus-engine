/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/ecs/systems/graphics_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/modules/module_animation.hpp>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(engine::graphics::GraphicsComponent)

namespace modus::engine::graphics {

GraphicsSystem::GraphicsSystem() : engine::gameplay::System("Graphics") {}

Result<> GraphicsSystem::initialize(Engine&, gameplay::GameWorld&, gameplay::EntityManager& em) {
    auto listener = engine::gameplay::ComponentDestroyedDelegate::build<
        GraphicsSystem, &GraphicsSystem::on_component_destroyed>(this);

    if (!em.register_component_destroy_callback<GraphicsComponent>(listener)) {
        MODUS_LOGE(
            "GraphicsSystem: Failed to register component destroyed "
            "callback");
        return Error<>();
    }
    return Ok<>();
}

void GraphicsSystem::shutdown(Engine&, gameplay::GameWorld&, gameplay::EntityManager& em) {
    em.unregister_component_destroy_callback<GraphicsComponent>();
}

using ViewType =
    gameplay::EntityManagerViewMut<const gameplay::TransformComponent, GraphicsComponent>;
static inline void do_component_update(ViewType& view) {
    view.for_each([](const gameplay::EntityId, const gameplay::TransformComponent& tc,
                     GraphicsComponent& gc) {
        // Update bounding volume and world transform
        const math::Transform& world_transform = tc.world_transform();
        if (tc.did_world_transform_update()) {
            gc.bounding_volume =
                math::bv::transform(world_transform, gc.mesh_instance.bounding_volume);
        }
    });
}

void GraphicsSystem::update(Engine&,
                            const IMilisec,
                            gameplay::GameWorld&,
                            gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Graphics Components");
    ViewType view(entity_manager);
    do_component_update(view);
}

void GraphicsSystem::update_threaded(ThreadSafeEngineAccessor&,
                                     const IMilisec,
                                     gameplay::ThreadSafeGameWorldAccessor&,
                                     gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Graphics Components");
    ViewType view(entity_manager);
    do_component_update(view);
}

void GraphicsSystem::on_component_destroyed(Engine& engine, const engine::gameplay::Component& c) {
    const GraphicsComponent& gc = static_cast<const GraphicsComponent&>(c);
    if (gc.animator) {
        auto animation_module = engine.module<ModuleAnimation>();
        animation_module->manager().destroy(gc.animator);
    }
}

}    // namespace modus::engine::graphics
