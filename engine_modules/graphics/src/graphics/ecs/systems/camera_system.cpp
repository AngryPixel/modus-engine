/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/ecs/systems/camera_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/event/event_manager.hpp>
#include <engine/modules/module_app.hpp>
#include <engine/modules/module_event.hpp>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(engine::graphics::CameraComponent)

namespace modus::engine::graphics {

CameraSystem::CameraSystem() : engine::gameplay::System("Camera") {}

Result<> CameraSystem::initialize(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) {
    auto app_module = engine.module<engine::ModuleApp>();
    auto event_module = engine.module<engine::ModuleEvent>();
    auto r_listener = event_module->manager().register_event_listener(
        app_module->window_resize_event_handle(),
        engine::event::EventListener::build<CameraSystem, &CameraSystem::on_window_resize>(this));
    if (!r_listener) {
        MODUS_LOGE("CameraSystem: Failed to register window resize event");
        return Error<>();
    }
    m_window_resize_listener = *r_listener;
    return Ok<>();
}

static inline void do_camera_aspect_ratio_update(
    gameplay::EntityManagerViewMut<CameraComponent>& view,
    const f32 aspect_ratio) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Camera Components Aspect ratio");
    view.for_each([aspect_ratio](const gameplay::EntityId, CameraComponent& cc) {
        if (cc.m_camera.frustum().is_perspective()) {
            cc.m_camera.frustum().set_aspect_ratio(aspect_ratio);
        }
    });
}

static inline void do_camera_update(
    gameplay::EntityManagerViewMut<const gameplay::TransformComponent, CameraComponent>& view) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Camera Components");
    view.for_each(
        [](const gameplay::EntityId, const gameplay::TransformComponent& tc, CameraComponent& cc) {
            // Update bounding volume and world transform
            const math::Transform& world_transform = tc.world_transform();
            if (tc.did_world_transform_update()) {
                const glm::vec3& translation = world_transform.translation();
                const glm::quat& rotation = world_transform.rotation();
                const glm::vec3 eye = translation + (rotation * cc.m_forward_vec);
                const glm::vec3 up = rotation * cc.m_up_vec;
                cc.m_camera.set_look_at(translation, eye, up);
            }
        });
}

void CameraSystem::update(Engine&,
                          const IMilisec,
                          gameplay::GameWorld&,
                          gameplay::EntityManager& entity_manager) {
    if (m_new_aspect_ratio.has_value()) {
        gameplay::EntityManagerViewMut<CameraComponent> view(entity_manager);
        do_camera_aspect_ratio_update(view, m_new_aspect_ratio.value());
        m_new_aspect_ratio.reset();
    }
    {
        gameplay::EntityManagerViewMut<const gameplay::TransformComponent, CameraComponent> view(
            entity_manager);
        do_camera_update(view);
    }
}

void CameraSystem::update_threaded(ThreadSafeEngineAccessor&,
                                   const IMilisec,
                                   gameplay::ThreadSafeGameWorldAccessor&,
                                   gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    if (m_new_aspect_ratio.has_value()) {
        gameplay::EntityManagerViewMut<CameraComponent> view(entity_manager);
        do_camera_aspect_ratio_update(view, m_new_aspect_ratio.value());
        m_new_aspect_ratio.reset();
    }
    {
        gameplay::EntityManagerViewMut<const gameplay::TransformComponent, CameraComponent> view(
            entity_manager);
        do_camera_update(view);
    }
}

void CameraSystem::shutdown(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) {
    auto app_module = engine.module<engine::ModuleApp>();
    auto event_module = engine.module<engine::ModuleEvent>();
    if (!event_module->manager().unregister_event_listner(app_module->window_resize_event_handle(),
                                                          m_window_resize_listener)) {
        MODUS_LOGE(
            "TestWorldBase: Failed to unregister window resize event "
            "handler");
    }
}

void CameraSystem::on_window_resize(engine::Engine& engine, engine::event::Event& event) {
    MODUS_UNUSED(engine);
    const engine::AppWindowResizeEvent& window_resize_event =
        static_cast<const engine::AppWindowResizeEvent&>(event);
    m_new_aspect_ratio = (f32(window_resize_event.m_width) / f32(window_resize_event.m_height));
    MODUS_LOGD("CameraSystem: New aspect ratio = {}", *m_new_aspect_ratio);
}

IsometricCameraSystem::IsometricCameraSystem() : engine::gameplay::System("IsometricCamera") {}

Result<> IsometricCameraSystem::initialize(Engine& engine,
                                           gameplay::GameWorld&,
                                           gameplay::EntityManager&) {
    auto app_module = engine.module<engine::ModuleApp>();
    auto event_module = engine.module<engine::ModuleEvent>();
    auto r_listener = event_module->manager().register_event_listener(
        app_module->window_resize_event_handle(),
        engine::event::EventListener::build<IsometricCameraSystem,
                                            &IsometricCameraSystem::on_window_resize>(this));
    if (!r_listener) {
        MODUS_LOGE("CameraSystem: Failed to register window resize event");
        return Error<>();
    }
    m_window_resize_listener = *r_listener;
    return Ok<>();
}

static inline void do_camera_isometric_update(gameplay::EntityManagerViewMut<CameraComponent>& view,
                                              const f32 width,
                                              const f32 height) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Camera Component Frustums");
    view.for_each([width, height](const gameplay::EntityId, CameraComponent& cc) {
        if (cc.m_camera.frustum().is_orthographic()) {
            cc.m_camera.frustum().set_ortographic_rect(0.f, height, 0, width);
        }
    });
}

void IsometricCameraSystem::update(Engine&,
                                   const IMilisec,
                                   gameplay::GameWorld&,
                                   gameplay::EntityManager& entity_manager) {
    if (m_new_values.has_value()) {
        m_new_values.reset();
    }
    {
        gameplay::EntityManagerViewMut<const gameplay::TransformComponent, CameraComponent> view(
            entity_manager);
        do_camera_update(view);
    }
}
void IsometricCameraSystem::update_threaded(
    ThreadSafeEngineAccessor&,
    const IMilisec,
    gameplay::ThreadSafeGameWorldAccessor&,
    gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    if (m_new_values.has_value()) {
        gameplay::EntityManagerViewMut<CameraComponent> view(entity_manager);
        do_camera_isometric_update(view, m_new_values->width, m_new_values->height);
        m_new_values.reset();
    }
    {
        gameplay::EntityManagerViewMut<const gameplay::TransformComponent, CameraComponent> view(
            entity_manager);
        do_camera_update(view);
    }
}

void IsometricCameraSystem::shutdown(Engine& engine,
                                     gameplay::GameWorld&,
                                     gameplay::EntityManager&) {
    auto app_module = engine.module<engine::ModuleApp>();
    auto event_module = engine.module<engine::ModuleEvent>();
    if (!event_module->manager().unregister_event_listner(app_module->window_resize_event_handle(),
                                                          m_window_resize_listener)) {
        MODUS_LOGE(
            "TestWorldBase: Failed to unregister window resize event "
            "handler");
    }
}

void IsometricCameraSystem::on_window_resize(engine::Engine& engine, engine::event::Event& event) {
    MODUS_UNUSED(engine);
    const engine::AppWindowResizeEvent& window_resize_event =
        static_cast<const engine::AppWindowResizeEvent&>(event);
    m_new_values = {f32(window_resize_event.m_width), f32(window_resize_event.m_height)};
}
}    // namespace modus::engine::graphics
