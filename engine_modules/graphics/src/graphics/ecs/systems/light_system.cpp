/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/ecs/systems/light_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/graphics/ecs/components/light_component.hpp>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(engine::graphics::LightComponent)

namespace modus::engine::graphics {

LightSystem::LightSystem() : engine::gameplay::System("Lights") {}

MODUS_FORCE_INLINE void do_component_update(const gameplay::TransformComponent& tc,
                                            LightComponent& lc) {
    // Update light position
    const math::Transform& world_transform = tc.world_transform();
    if (lc.enabled && lc.light.type() != LightType::Ambient && tc.did_world_transform_update()) {
        lc.light.set_position(world_transform.translation());
    }
}

void LightSystem::update(Engine&,
                         const IMilisec,
                         gameplay::GameWorld&,
                         gameplay::EntityManager& entity_manager) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Light Components");
    gameplay::EntityManagerViewMut<const gameplay::TransformComponent, LightComponent> view(
        entity_manager);
    view.for_each([](const gameplay::EntityId, const gameplay::TransformComponent& tc,
                     LightComponent& lc) { do_component_update(tc, lc); });
}
void LightSystem::update_threaded(ThreadSafeEngineAccessor&,
                                  const IMilisec,
                                  gameplay::ThreadSafeGameWorldAccessor&,
                                  gameplay::ThreadSafeEntityManagerAccessor& entity_manager) {
    MODUS_PROFILE_GRAPHICS_BLOCK("Update Light Components");
    gameplay::EntityManagerViewMut<const gameplay::TransformComponent, LightComponent> view(
        entity_manager);
    view.for_each([](const gameplay::EntityId, const gameplay::TransformComponent& tc,
                     LightComponent& lc) { do_component_update(tc, lc); });
}
}    // namespace modus::engine::graphics
