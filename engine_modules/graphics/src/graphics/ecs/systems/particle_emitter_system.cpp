/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/ecs/systems/particle_emitter_system.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/graphics/ecs/components/particle_emitter_component.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/engine.hpp>

MODUS_ENGINE_GAMEPLAY_COMPONENT_IMPL(engine::graphics::ParticleEmitterV2Component)

namespace modus::engine::graphics {

ParticleEmitterV2System::ParticleEmitterV2System()
    : engine::gameplay::System("ParticleEmitterV2") {}

Result<> ParticleEmitterV2System::initialize(Engine&,
                                             gameplay::GameWorld&,
                                             gameplay::EntityManager& em) {
    auto listener = engine::gameplay::ComponentDestroyedDelegate::build<
        ParticleEmitterV2System, &ParticleEmitterV2System::on_component_destroyed>(this);

    if (!em.register_component_destroy_callback<ParticleEmitterV2Component>(listener)) {
        MODUS_LOGE(
            "ParticleEmitterV2System: Failed to register component destroyed "
            "callback");
        return Error<>();
    }
    return Ok<>();
}

void ParticleEmitterV2System::shutdown(Engine&, gameplay::GameWorld&, gameplay::EntityManager& em) {
    em.unregister_component_destroy_callback<ParticleEmitterV2Component>();
}
void ParticleEmitterV2System::update(Engine& engine,
                                     const IMilisec,
                                     gameplay::GameWorld&,
                                     gameplay::EntityManager& entity_manager) {
    auto module_graphics = engine.module<ModuleGraphics>();
    ParticleRegistryV2& registry = module_graphics->particle_registry();
    MODUS_PROFILE_GRAPHICS_BLOCK("Update ParticleEmitterV2 Components");
    gameplay::EntityManagerViewMut<const gameplay::TransformComponent, ParticleEmitterV2Component>
        view(entity_manager);
    view.for_each([&registry](const gameplay::EntityId id, const gameplay::TransformComponent& tc,
                              ParticleEmitterV2Component& pec) {
        if (!registry.update_emitter_transform(pec.m_emitter, tc.world_transform())) {
            MODUS_LOGE(
                "ParticleEmitterV2System: Failed to update emitter position "
                "for: {}",
                id);
        }
    });
}

void ParticleEmitterV2System::update_threaded(ThreadSafeEngineAccessor&,
                                              const IMilisec,
                                              gameplay::ThreadSafeGameWorldAccessor&,
                                              gameplay::ThreadSafeEntityManagerAccessor&) {
    modus_panic("Particle Emitter can't currently be run threaded");
}

void ParticleEmitterV2System::on_component_destroyed(Engine& engine,
                                                     const engine::gameplay::Component& c) {
    const ParticleEmitterV2Component& pec = static_cast<const ParticleEmitterV2Component&>(c);
    if (pec.m_emitter) {
        auto module_graphics = engine.module<ModuleGraphics>();
        ParticleRegistryV2& registry = module_graphics->particle_registry();
        registry.destroy_emitter(pec.m_emitter);
    }
}

}    // namespace modus::engine::graphics
