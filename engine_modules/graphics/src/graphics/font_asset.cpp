/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/graphics/font_asset.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/device.hpp>
#include <threed/texture.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::FontAsset)

namespace modus::engine::graphics {

FontAsset::FontAsset(Font&& font, const threed::TextureHandle handle)
    : assets::AssetBase(assets::AssetTraits<FontAsset>::type_id()),
      m_font(std::move(font)),
      m_texture(handle) {}

FontAssetFactory::FontAssetFactory()
    : assets::PooledAssetFactory<FontAsset, GraphicsAllocator>(8) {}

StringSlice FontAssetFactory::file_extension() const {
    return "font";
}

std::unique_ptr<assets::AssetData> FontAssetFactory::create_asset_data() {
    return modus::make_unique<assets::RamAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> FontAssetFactory::create(Engine& engine,
                                                             assets::AssetData& data) {
    const engine::assets::RamAssetData& font_data = static_cast<const assets::RamAssetData&>(data);
    MODUS_PROFILE_GRAPHICS_BLOCK("FontAssetCreate");
    SliceStream stream = font_data.stream();
    auto r_font = Font::from_stream(stream);
    if (!r_font) {
        return Error<>();
    }

    threed::TextureCreateParams tex_params;
    tex_params.type = threed::TextureType::T2D;
    tex_params.depth = 1;
    tex_params.width = r_font.value().atlas_width_px();
    tex_params.height = r_font.value().atlas_height_px();
    tex_params.mip_map_levels = 1;
    tex_params.format = threed::TextureFormat::R8;

    threed::TextureData tex_data;
    tex_data.data = r_font.value().atlas_data();
    tex_data.width = tex_params.width;
    tex_data.height = tex_params.height;
    tex_data.depth = 1;
    tex_data.mip_map_level = 0;
    tex_data.unpack_alignment = 1;

    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();

    Slice<threed::TextureData> tex_data_slice(&tex_data, 1);
    auto r_texture = device.create_texture(tex_params, tex_data_slice);
    if (!r_texture) {
        MODUS_LOGE("FontAssetData: Failed to create font atlas texture");
        return Error<>();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(
        m_asset_pool.construct(r_font.release_value(), *r_texture));
}

Result<> FontAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GRAPHICS_BLOCK("FontAssetDestroy");
    auto r_font_asset = assets::assetv2_cast<FontAsset>(asset);
    if (!r_font_asset) {
        return Error<>();
    }
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    device.destroy_texture((*r_font_asset)->m_texture);
    m_asset_pool.destroy(r_font_asset->get());
    return Ok<>();
}

}    // namespace modus::engine::graphics
