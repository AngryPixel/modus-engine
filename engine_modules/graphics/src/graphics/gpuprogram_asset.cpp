/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/graphics/gpuprogram_generated.h>
#include <pch.h>

#include <core/io/memory_stream.hpp>
#include <engine/engine.hpp>
#include <engine/graphics/gpuprogram_asset.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/device.hpp>
#include <threed/program.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::GPUProgramAsset)

namespace modus::engine::graphics {

GPUProgramAsset::GPUProgramAsset(threed::ProgramHandle program)
    : assets::AssetBase(assets::AssetTraits<GPUProgramAsset>::type_id()), m_program(program) {}

GPUProgramAssetFactory::GPUProgramAssetFactory() : factory_type(32) {}

StringSlice GPUProgramAssetFactory::file_extension() const {
    return "gpuprog";
}

std::unique_ptr<assets::AssetData> GPUProgramAssetFactory::create_asset_data() {
    return modus::make_unique<assets::RamAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> GPUProgramAssetFactory::create(Engine& engine,
                                                                   assets::AssetData& data) {
    MODUS_PROFILE_GRAPHICS_BLOCK("GPUProgramAssetCreate");
    const assets::RamAssetData& ram_data = static_cast<const assets::RamAssetData&>(data);

    const ByteSlice program_bytes = ram_data.bytes();

    flatbuffers::Verifier verifier(program_bytes.data(), program_bytes.size());
    if (!fbs::VerifyGPUProgramBuffer(verifier)) {
        MODUS_LOGE("GPUProgramAsset: Data is not a gpu program.");
        return Error<>();
    }

    const fbs::GPUProgram* fbs_program = fbs::GetGPUProgram(program_bytes.data());
    if (fbs_program == nullptr) {
        MODUS_LOGE("GPUProgramAsset: Data is not a gpu program.");
        return Error<>();
    }

    threed::ProgramCreateParams program_params;

    if (const auto fbs_source = fbs_program->vert(); fbs_source != nullptr) {
        program_params.vertex = ByteSlice(fbs_source->Data(), fbs_source->size());
    } else {
        MODUS_LOGE("GPUProgramAsset: Program has no vertex shader");
        return Error<>();
    }

    if (const auto fbs_source = fbs_program->frag(); fbs_source != nullptr) {
        program_params.fragment = ByteSlice(fbs_source->Data(), fbs_source->size());
    }
    /*
        if (const auto fbs_constants = fbs_program->constants(); fbs_constants != nullptr) {
            for (const auto& fbs_constant : *fbs_constants) {
                threed::ProgramConstantInputParam constant;
                constant.name = fbs_constant->name()->c_str();
                if (fbs_constant->is_buffer()) {
                    constant.type = threed::ProgramContanstInputType::Buffer;
                } else {
                    constant.type = threed::ProgramContanstInputType::Constant;
                    constant.data_type = static_cast<threed::DataType>(fbs_constant->data_type());
                }
                if (!program_params.constants.push_back(constant)) {
                    MODUS_LOGE("GPUProgramAsset: Failed to add constant {} to params",
       constant.name); return Error<>();
                }
            }
        }*/

    if (const auto fbs_samplers = fbs_program->samplers(); fbs_samplers != nullptr) {
        for (const auto& fbs_sampler : *fbs_samplers) {
            threed::ProgramSamplerInputParam texture;
            texture.name = fbs_sampler->name()->c_str();
            if (!program_params.samplers.push_back(texture)) {
                MODUS_LOGE("GPUProgramAsset: Failed to add texture {} to params", texture.name);
                return Error<>();
            }
        }
    }

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    auto r_program = device.create_program(program_params);
    if (!r_program) {
        MODUS_LOGE("GPUProgramAssetData: Failed to create gpu program: {}", r_program.error());
        return Error<>();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(m_asset_pool.construct(*r_program));
}

Result<> GPUProgramAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GRAPHICS_BLOCK("GPUProgramAssetDestroy");
    auto r_prog_asset = assets::assetv2_cast<GPUProgramAsset>(asset);
    if (!r_prog_asset) {
        return Error<>();
    }
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    device.destroy_program(r_prog_asset.value()->m_program);
    m_asset_pool.destroy(r_prog_asset->get());
    return Ok<>();
}
}    // namespace modus::engine::graphics
