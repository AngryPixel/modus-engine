/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/graphics/image.hpp>
#include <engine/graphics/texture_util.hpp>
#include <threed/device.hpp>
#include <threed/texture.hpp>

namespace modus::engine::graphics {

Result<threed::TextureHandle, void> image_to_texture(threed::Device& device, const Image& image) {
    static constexpr usize kMaxExpectedMipMapCount = 32;
    modus_assert(image.mip_map_count() < kMaxExpectedMipMapCount);
    modus_assert(image.mip_map_count() < usize(std::numeric_limits<u8>::max()));

    Array<threed::TextureData, kMaxExpectedMipMapCount> tex_data;
    threed::TextureCreateParams tex_params;

    tex_params.width = image.width();
    tex_params.height = image.height();
    tex_params.depth = image.depth();
    tex_params.type = image.texture_type();
    tex_params.format = image.texture_format();
    tex_params.mip_map_levels =
        image.texture_type() == threed::TextureType::TCubeMap ? 1 : u8(image.mip_map_count());

    u32 input_counter = 0;
    for (u16 d = 0; d < image.depth(); ++d) {
        for (u8 i = 0; i < u8(image.mip_map_count()); ++i) {
            if (input_counter >= kMaxExpectedMipMapCount) {
                modus_assert_message(false, "increase limit");
                MODUS_LOGE("Total temporary stack storage reached for mipmap data, increase limit");
                return Error<>();
            }
            auto opt_mipmap = image.mip_map(u32(i), d);
            if (!opt_mipmap) {
                MODUS_LOGE("Failed to access mipmap {} from image", i);
                return Error<>();
            }
            const ImageMipMap& mipmap = *opt_mipmap;
            threed::TextureData& tdata = tex_data[input_counter];
            tdata.width = mipmap.width;
            tdata.height = mipmap.height;
            tdata.depth = mipmap.depth;
            tdata.mip_map_level = image.texture_type() == threed::TextureType::TCubeMap ? 0 : i;
            tdata.data = mipmap.data;
            input_counter++;
        }
    }

    const Slice<threed::TextureData> tex_data_slice =
        Slice<threed::TextureData>(tex_data.data(), input_counter);
    return device.create_texture(tex_params, tex_data_slice);
}

}    // namespace modus::engine::graphics
