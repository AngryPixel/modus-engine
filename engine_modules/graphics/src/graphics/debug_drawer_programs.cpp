/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/debug_drawer_programs.hpp>
// clang-format on

#include <engine/graphics/default_program_inputs.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/program.hpp>
#include <threed/device.hpp>
#include <threed/program_gen/shader_generator.hpp>

namespace modus::engine::graphics {

constexpr const char* kDrawVertexSrc =
    R"R(
void main(){
    gl_Position = u_mvp * VERTEX;
    v_color = u_color;
})R";

constexpr const char* kDrawLineSrc =
    R"R(
void main(){
    gl_Position = u_mvp * VERTEX;
    v_color = COLOR;
})R";

constexpr const char* kDrawFragSrc =
    R"R(
void main(){
    OUT_COLOR = v_color;
})R";

constexpr const char* kDrawFontVertexSrc =
    R"R(
void main(){
    gl_Position = u_mvp * vec4(VERTEX.xy, 0.0, 1.0);
    v_uv = UV;
})R";

constexpr const char* kDrawFontFragSrc =
    R"R(
void main(){
    OUT_COLOR = vec4(u_color.xyz, texture(s_font_atlas, v_uv).r);
})R";

Result<> DebugDrawerPrograms::initialize(NotMyPtr<ModuleGraphics> graphics) {
    threed::Device& device = graphics->device();

    const auto& shader_generator = device.shader_generator();

    threed::dynamic::TypedDefaultConstantInput<glm::mat4> proj_desc("u_mvp", &m_proj_input);
    threed::dynamic::TypedDefaultConstantInput<glm::vec4> color_desc("u_color", &m_color_input);
    threed::dynamic::DefaultSamplerInput sampler_desc("s_font_atlas", &m_font_atlas_input);
    threed::dynamic::ProgramGenerator generator;

    // Shape Program
    {
        threed::dynamic::ProgramGenParams params;
        params.varying
            .push_back({"v_color", threed::DataType::Vec4F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Low})
            .expect();
        params.vertex_stage.constants.push_back(&proj_desc).expect();
        params.vertex_stage.constants.push_back(&color_desc).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
        params.vertex_stage.main = kDrawVertexSrc;
        params.fragment_stage.fragment_outputs.push_back(shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.main = kDrawFragSrc;
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_shape_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
            return Error<>();
        } else {
            m_shape_program = *r;
        }
        generator.reset();
    }

    // Shape Program
    {
        threed::dynamic::ProgramGenParams params;
        params.varying
            .push_back({"v_color", threed::DataType::Vec4F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Low})
            .expect();
        params.vertex_stage.constants.push_back(&proj_desc).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_color_input()).expect();
        params.vertex_stage.main = kDrawLineSrc;
        params.fragment_stage.fragment_outputs.push_back(shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.main = kDrawFragSrc;
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_line_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create debug draw line program: {}", r.error());
            return Error<>();
        } else {
            m_line_program = *r;
        }
        generator.reset();
    }

    // Shape Program
    {
        threed::dynamic::ProgramGenParams params;
        params.varying
            .push_back({"v_uv", threed::DataType::Vec2F32,
                        threed::dynamic::VaryingDesc::Type::Smooth,
                        threed::dynamic::PrecisionQualifier::Medium})
            .expect();
        params.vertex_stage.constants.push_back(&proj_desc).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_uv_input()).expect();
        params.vertex_stage.main = kDrawFontVertexSrc;
        params.fragment_stage.constants.push_back(&color_desc).expect();
        params.fragment_stage.fragment_outputs.push_back(shader_desc::default_frag_output())
            .expect();
        params.fragment_stage.samplers.push_back(&sampler_desc).expect();
        params.fragment_stage.main = kDrawFontFragSrc;
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_font_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create debug draw font program: {}", r.error());
            return Error<>();
        } else {
            m_font_program = *r;
        }
        generator.reset();
    }

    return Ok<>();
}

void DebugDrawerPrograms::shutdown(NotMyPtr<ModuleGraphics> graphics) {
    threed::Device& device = graphics->device();

    if (m_shape_program) {
        device.destroy_program(m_shape_program);
    }
    if (m_line_program) {
        device.destroy_program(m_line_program);
    }
    if (m_font_program) {
        device.destroy_program(m_font_program);
    }
}

}    // namespace modus::engine::graphics
