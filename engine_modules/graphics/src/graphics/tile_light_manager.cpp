/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/tile_light_manager.hpp>
#include <engine/engine.hpp>
#include <threed/buffer.hpp>
#include <threed/device.hpp>
#include <engine/graphics/light.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/texture.hpp>
#include <threed/effect.hpp>
#include <threed/sampler.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <threed/compositor.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/program_gen/shader_snippet_db.hpp>
#include <threed/program_gen/program_gen.hpp>

namespace modus::engine::graphics {
/* Debug shader
layout(location = 0) out vec4 frag_color;

in vec2 texture_coords;

uniform highp usampler2D u_grid;
uniform highp usampler2D u_indices;
uniform float u_num_lights;
#define USE_TEXEL_FETCH 1
//#define DISPLAY_LIGHTIDX
void main() {
#ifdef USE_TEXEL_FETCH
    ivec2 tile_coord = ivec2(gl_FragCoord.xy / vec2(64, 64));
    uvec2 grid_info = texelFetch(u_grid, tile_coord,0).xy;
#else
    uvec2 grid_info = texture(u_grid, texture_coords).xy;
#endif
    vec2 indices_size = vec2(textureSize(u_indices, 0));

#ifdef DISPLAY_LIGHTIDX
    uint index_offset = grid_info.x;
    uint light_count = grid_info.y;
    vec3 color = vec3(0);
    for(uint i = 0u; i < light_count && i < 3u; i++) {
        ivec2 index_coord = ivec2(index_offset + i, 0);
        uint light_index = texelFetch(u_indices, index_coord, 0).r;
        color[i] = float(light_index)/u_num_lights;
    }
    frag_color = vec4(color, 0.5);
#else
    frag_color = vec4(0., (vec2(grid_info)/vec2(indices_size.x,u_num_lights)).y, 0, 0.5);
#endif

    if (uint(gl_FragCoord.x) % 64u == 0u || uint(gl_FragCoord.y) % 64u == 0u) {
        frag_color = vec4(1, 1,1,0.3);
    }
}

 */
static const char* kTileLightManagerCBLayout = R"R(
struct DirectionalLight {{
    vec3 diffuse_color;
    float diffuse_intensity;
    vec3 direction;
    float paddingxx;
}};
struct PointLight {{
    vec3 diffuse_color;
    float diffuse_intensity;
     vec3 position;
     float radius;
 }};
 layout(std140, binding={}) uniform Lights {{
     DirectionalLight u_directional_lights[4];
     PointLight u_point_lights[128];
     vec4 u_ambient;
     int u_num_directional_lights;
     int u_num_point_lights;
     uint u_num_tiles_x;
     uint u_num_tiles_y;
}};
)R";

static const char* kTileLightManagerCodeSnippets = R"R(
#define TILE_WIDTH 64
#define TILE_HEIGHT 64
#define ENGINE_TILED_LIGHT_MANAGER_ENABLED
#define USE_SHADER_STORAGE
vec4 calculate_light(in vec3 color,
    in vec3 world_position,
    in vec3 normal,
    in vec3 light_dir,
    in float diffuse_intensity,
    in float specular_factor) {

    float n_dot_l = dot(normal, light_dir);
    float diffuse_factor = n_dot_l;

    // calculate diffuse
    vec4 diffuse_color = vec4(0);
    if (diffuse_factor > 0.0) {
        diffuse_color = vec4(color
                * diffuse_intensity
                * diffuse_factor, 1.0);
    }

    // calculate specular
    vec3 view_dir = normalize(get_camera_position() - world_position);
    vec3 halfway_dir = normalize(light_dir + view_dir);

    //TODO: Specify shininess property
    const float shininess = 16.0;
    float specular = pow(max(dot(normal, halfway_dir), 0.0), shininess * shininess);
    //float specular = pow(max(dot(view_dir, reflect_dir), 0.0), shininess);
    vec4 specular_color = vec4(vec3(specular_factor * specular * color), 1.0);
    return diffuse_color + specular_color;
    return diffuse_color + specular_color;
}

vec4 calculate_directional_light(in DirectionalLight light, in vec3 normal,
    in vec3 world_position, float specular_factor) {
    return calculate_light(light.diffuse_color,
        world_position,
        normal,
        light.direction,
        light.diffuse_intensity,
        specular_factor);
}

vec4 calculate_point_light(in PointLight light, in vec3 normal,
    in vec3 world_position, float specular_factor) {

    vec3 light_dir = light.position - world_position;
    float dist = length(light_dir);
    light_dir = normalize(light_dir);

    vec4 color = calculate_light(light.diffuse_color,
        world_position,
        normal,
        light_dir,
        light.diffuse_intensity,
        specular_factor);

    /*float attenuation = light.a_constant +
        (light.a_linear * dist) +
        (light.a_exp * (dist * dist));*/

    // Alternative
    //float attenuation = clamp(1.0 - (dist*dist)/(light.radius * light.radius), 0.0, 1.0);
    float attenuation = clamp(1.0 - dist/light.radius, 0.0, 1.0);
    attenuation *= attenuation;

    return color * attenuation;
}

#if defined(USE_SHADER_STORAGE)
vec4 calculate_lights_tiled(in vec3 position, in vec3 normal,
    float specular_factor, in vec3 emissive, in float shadow_factor) {
    vec4 final_color = vec4(0);
    for(int i = 0; i < u_num_directional_lights; ++i) {
        final_color += calculate_directional_light(u_directional_lights[i], normal, position, specular_factor);
    }
    if (u_num_point_lights != 0) {
        ivec2 tile_coord = ivec2(floor(gl_FragCoord.xy / vec2(TILE_WIDTH, TILE_HEIGHT)));
        uint tile_info = u_light_grid[( (uint(tile_coord.y) * u_num_tiles_x)) + uint(tile_coord.x)];

        // litte endian,
        uint index_offset = tile_info & uint(0xFFFF);;
        uint light_count = tile_info >> 16;
        for(uint i = 0u; i < light_count; i++) {
            uint index_coord = index_offset + i;
            uint light_index = u_light_indices[index_coord];
            final_color += calculate_point_light(u_point_lights[light_index], normal,
                position, specular_factor);
        }
    }

    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
    return ambient_color + ((1.0 - shadow_factor) * final_color) + vec4(emissive, 1.0);
}
#else
vec4 calculate_lights_tiled(in vec3 position, in vec3 normal,
    float specular_factor, in vec3 emissive, in float shadow_factor) {
    vec4 final_color = vec4(0);
    for(int i = 0; i < u_num_directional_lights; ++i) {
        final_color += calculate_directional_light(u_directional_lights[i], normal, position, specular_factor);
    }
    if (u_num_point_lights != 0) {
        ivec2 tile_coord = ivec2(gl_FragCoord.xy / vec2(TILE_WIDTH, TILE_HEIGHT));
        uvec2 tile_info = texelFetch(u_light_grid, tile_coord,0).xy;

        uint index_offset = tile_info.x;
        uint light_count = tile_info.y;
        for(uint i = 0u; i < light_count; i++) {
            ivec2 index_coord = ivec2(index_offset + i, 0);
            uint light_index = texelFetch(u_light_indices, index_coord, 0).r;
            final_color += calculate_point_light(u_point_lights[light_index], normal,
                position, specular_factor);
        }
    }
    vec4 ambient_color = vec4(u_ambient.xyz * u_ambient.w ,1.0);
    return ambient_color + ((1.0 - shadow_factor) * final_color) + vec4(emissive, 1.0);
}
#endif
#undef USE_SHADER_STORAGE
)R";

struct LightListCBInput final : public threed::dynamic::ConstantBufferInput {
    NotMyPtr<const threed::dynamic::InputBinder> binder;

    LightListCBInput(NotMyPtr<const threed::dynamic::InputBinder> in_binder) {
        m_desc.name = "Lights";
        this->binder = in_binder;
    }

    Result<threed::dynamic::BindableInputResult> generate_declaration(
        const u8 slot,
        threed::dynamic::GenerateParams& params) const override {
        const String text = modus::format(kTileLightManagerCBLayout, slot);
        if (!params.writer->write_exactly(StringSlice(text).as_bytes())) {
            return Error<>();
        }
        return Ok(threed::dynamic::BindableInputResult::Generated);
    }
    Result<threed::dynamic::BindableInputResult> generate_type_snippet(
        threed::dynamic::GenerateParams&) const override {
        return Ok(threed::dynamic::BindableInputResult::Ignored);
    }

    Result<threed::dynamic::BindableInputResult> generate_code_snippet(
        threed::dynamic::GenerateParams& params) const override {
        if (!params.writer->write_exactly(StringSlice(kTileLightManagerCodeSnippets).as_bytes())) {
            return Error<>();
        }
        return Ok(threed::dynamic::BindableInputResult::Generated);
    }

    NotMyPtr<const threed::dynamic::InputBinder> runtime_binder() const override { return binder; }
};

struct TileLightManager::ShaderSnippet final : public threed::dynamic::ShaderSnippet {
    threed::dynamic::DefaultConstantBufferInputBinder cb_light_binder;
#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
    threed::dynamic::DefaultSamplerInputBinder light_indices_binder;
    threed::dynamic::DefaultSamplerInputBinder light_grid_binder;
    threed::dynamic::DefaultSamplerInput light_indices_input;
    threed::dynamic::DefaultSamplerInput light_grid_input;
#else
    threed::dynamic::DefaultShaderBufferInputBinder light_indices_binder;
    threed::dynamic::DefaultShaderBufferInputBinder light_grid_binder;
    threed::dynamic::DefaultShaderBufferInput light_indices_input;
    threed::dynamic::DefaultShaderBufferInput light_grid_input;
#endif

    LightListCBInput cb_light_input;

    ShaderSnippet()
        : threed::dynamic::ShaderSnippet("engine.tiled_light"),
#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
          light_indices_input("u_light_indices", &light_indices_binder),
          light_grid_input("u_light_grid", &light_grid_binder),
#else
          light_indices_input("LightIndices", &light_indices_binder),
          light_grid_input("LightGrid", &light_grid_binder),
#endif
          cb_light_input(&cb_light_binder) {
#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        light_grid_input.m_desc.sdtype = threed::dynamic::SamplerDataType::UInt;
        light_grid_input.m_desc.pqualifier = threed::dynamic::PrecisionQualifier::High;
        light_indices_input.m_desc.sdtype = threed::dynamic::SamplerDataType::UInt;
        light_indices_input.m_desc.pqualifier = threed::dynamic::PrecisionQualifier::High;
#else
        threed::dynamic::BufferMemberDesc member_desc;
        member_desc.name = "u_light_grid";
        member_desc.data_type = threed::DataType::Total,
        member_desc.type = threed::dynamic::BufferMemberDesc::Type::Custom,
        member_desc.custom_type_str = "uint[]";
        light_grid_input.m_desc.buffer_members.push_back(member_desc);
        member_desc.name = "u_light_indices";
        light_indices_input.m_desc.buffer_members.push_back(member_desc);
#endif
    }

    Result<> apply_common(threed::dynamic::ShaderStageCommon& stage) const {
        if (!stage.constant_buffers.push_back(&cb_light_input)) {
            MODUS_LOGE("Failed to add tiled light cb to stage");
            return Error<>();
        }

#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        if (!stage.samplers.push_back(&light_grid_input)) {
            MODUS_LOGE("Failed to add light grid texture to stage");
            return Error<>();
        }

        if (!stage.samplers.push_back(&light_indices_input)) {
            MODUS_LOGE("Failed to add light grid indices to stage");
            return Error<>();
        }
#else
        if (!stage.shader_buffers.push_back(&light_grid_input)) {
            MODUS_LOGE("Failed to add light grid texture to stage");
            return Error<>();
        }

        if (!stage.shader_buffers.push_back(&light_indices_input)) {
            MODUS_LOGE("Failed to add light grid indices to stage");
            return Error<>();
        }

#endif

        return Ok<>();
    }

    Result<> apply(threed::dynamic::ShaderVertexStage& vertex_stage) const override {
        return apply_common(vertex_stage);
    }

    Result<> apply(threed::dynamic::ShaderFragmentStage& fragment_stage) const override {
        return apply_common(fragment_stage);
    }
};

TileLightManager::TileLightManager() {
    m_shader_snippet = modus::make_unique<ShaderSnippet>();
}

TileLightManager::~TileLightManager() {}

Result<> TileLightManager::initialize(Engine& engine) {
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    const auto device_limits = device.device_limits();

    if (sizeof(LightManager::LightBlock) >= device_limits.min_constant_buffer_size) {
        MODUS_LOGE(
            "TileLightManager: LightBlock does not fit into device's "
            "minimum constant block size");
        return Error<>();
    }
#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
    // Create sampler
    {
        threed::SamplerCreateParams sampler_params;
        sampler_params.filter_mag = threed::SamplerFilter::Nearest;
        sampler_params.filter_min = threed::SamplerFilter::Nearest;
        if (auto r = device.create_sampler(sampler_params); !r) {
            MODUS_LOGE("Failed to create tile light manager sampler:{}", r.error());
            return Error<>();
        } else {
            m_sampler = *r;
            m_shader_snippet->light_grid_binder.m_sampler = *r;
            m_shader_snippet->light_indices_binder.m_sampler = *r;
        }
    }
#endif

    // create constant buffers
    // Points
    {
        threed::ConstantBufferCreateParams params;
        params.usage = threed::BufferUsage::Stream;
        params.size = sizeof(LightManager::LightBlock);
        params.data = nullptr;

        auto r_buffer = device.create_constant_buffer(params);
        if (!r_buffer) {
            MODUS_LOGE(
                "LightManager: Failed to create point light constant "
                "buffer");
            return Error<>();
        }
        m_buffer = *r_buffer;
    }

    if (!graphics_module->shader_snippet_db().register_snippet(m_shader_snippet.get())) {
        MODUS_LOGE("TileLightManager: Failed to register shader snippet");
        return Error<void>();
    }
    // init tiles

    m_n_tiles_x = 0;
    m_n_tiles_y = 0;

#if !defined(MODUS_RTM)
    /*
    {
        assets::AsyncLoadParams params;
        params.path = "gpuprog:engine/light_grid_debug.gpuprog";
        params.callback = [this](Engine&, const assets::AsyncCallbackResult& r) {
            if (r.asset) {
                auto program = assets::unsafe_assetv2_cast<const GPUProgramAsset>(r.asset);
                m_light_grid_debug_program = program->m_program;
            } else {
                MODUS_LOGE("TileLightManager: Failed to load gpuprogram: {}", r.path);
            }
        };
        auto r_program = engine.load_startup_asset(params);
        if (!r_program) {
            MODUS_LOGE("TileLightManager: Failed to load debug gpu program");
            return Error<>();
        }
    }
    {
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = false;
        effect_params.state.blend.targets[0].enabled = true;
        effect_params.state.blend.targets[0].eq_source = threed::BlendEquation::SourceAlpha;
        effect_params.state.blend.targets[0].eq_destination =
            threed::BlendEquation::OneMinusSourceAlpha;
        effect_params.state.raster.cull_enabled = false;

        using namespace gpuprogreflect;
        threed::SamplerState& sampler =
            effect_params.state.sampler[light_grid_debug::kSamplerIndex_u_grid];
        sampler.enabled = true;
        sampler.filter_min = threed::TextureFilter::Nearest;
        sampler.filter_mag = threed::TextureFilter::Nearest;

        if (auto r = device.create_effect(effect_params); !r) {
            MODUS_LOGE("TileLightManager: Failed to create debug effect");
            return Error<>();
        } else {
            m_light_grid_debug_effect = *r;
        }
    }
    */
#endif

    m_last_indices_count = 0;
    return Ok<>();
}

Result<> TileLightManager::shutdown(Engine& engine) {
    auto graphics_module = engine.module<ModuleGraphics>();
    if (graphics_module) {
        auto& device = graphics_module->device();
#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        if (m_sampler) {
            device.destroy_sampler(m_sampler);
        }
#endif
        if (m_buffer) {
            device.destroy_constant_buffer(m_buffer);
        }
    }
    return Ok<>();
}

struct Rect {
    f32 top, bottom, left, right;
};

// From
// https://community.khronos.org/t/scissor-region-from-bounding-sphere/41032/6
// Eric's post sugguest using projected_x = nz * focal_length /nx and
// and projected_y = nz * focal_length / (ny * h/w) but that did not produce
// results that were a good fit. Using the projection matrix seems to solve the
// problem.
Result<Rect, void> compute_sreen_rect(const Camera& camera,
                                      const glm::vec3& center,
                                      const f32 radius,
                                      const glm::vec4& viewport) {
    Rect rect;
    rect.left = -1.0f;
    rect.right = 1.0f;
    rect.bottom = -1.0f;
    rect.top = 1.0f;
    const glm::vec3 light_eye = camera.view_matrix() * glm::vec4(center, 1.0);
    MODUS_UNUSED(light_eye);
    const f32 radius2 = radius * radius;

    const f32 c_x2 = light_eye.x * light_eye.x;
    const f32 c_z2 = light_eye.z * light_eye.z;
    const f32 c_y2 = light_eye.y * light_eye.y;

    if (c_x2 + c_z2 + c_y2 < radius2) {
        // We are inside the sphere continue to projection
    } else {
        auto project_fn2 = [](const glm::mat4& proj, const glm::vec3& point) -> glm::vec3 {
            glm::vec4 tmp = proj * glm::vec4(point, 1.0);
            tmp /= tmp.w;
            return glm::vec3(tmp);
        };

        // Calculate X
        const f32 Dx = ((radius2 * c_x2) - ((c_x2 + c_z2) * (radius2 - c_z2)));
        if (Dx > 0.f) {
            auto solve_x_fn = [](const f32 lx, const f32 lz, const f32 r, const f32 d,
                                 f32& n) -> glm::vec3 {
                const f32 lx2 = lx * lx;
                const f32 lz2 = lz * lz;
                const f32 r2 = r * r;
                const f32 sqrt_d = glm::sqrt(d);
                const f32 nx = ((r * lx) + sqrt_d) / (lx2 + lz2);
                const f32 nz = (r - (nx * lx)) / lz;

                const f32 pz = (lx2 + lz2 - r2) / (lz - ((nz / nx) * lx));
                const f32 px = -((pz * nz) / nx);
                n = nx;
                return glm::vec3(px, 0.f, pz);
            };

            f32 nx1, nx2;
            const glm::vec3 x_plane1 = solve_x_fn(light_eye.x, light_eye.z, radius, Dx, nx1);
            const glm::vec3 x_plane2 = solve_x_fn(light_eye.x, light_eye.z, radius, -Dx, nx2);
            if (x_plane1.z < 0.0f) {
                const glm::vec3 x_projected1 = project_fn2(camera.projection_matrix(), x_plane1);
                if (nx1 > 0.0f) {
                    rect.left = glm::max(rect.left, x_projected1.x);
                } else {
                    rect.right = glm::min(rect.right, x_projected1.x);
                }
            }

            if (x_plane2.z < 0.0f) {
                const glm::vec3 x_projected2 = project_fn2(camera.projection_matrix(), x_plane2);
                if (nx2 > 0.0f) {
                    rect.left = glm::max(rect.left, x_projected2.x);
                } else {
                    rect.right = glm::min(rect.right, x_projected2.x);
                }
            }
        }
        // Calculate X
        const f32 Dy = (radius2 * c_y2) - ((c_y2 + c_z2) * (radius2 - c_z2));
        if (Dy > 0.f) {
            auto solve_y_fn = [](const f32 ly, const f32 lz, const f32 r, const f32 d,
                                 f32& n) -> glm::vec3 {
                const f32 ly2 = ly * ly;
                const f32 lz2 = lz * lz;
                const f32 r2 = r * r;
                const f32 sqrt_d = glm::sqrt(d);
                const f32 ny = ((r * ly) + sqrt_d) / (ly2 + lz2);
                const f32 nz = (r - (ny * ly)) / lz;

                const f32 pz = (ly2 + lz2 - r2) / (lz - ((nz / ny) * ly));
                const f32 py = -((pz * nz) / ny);
                n = ny;
                return glm::vec3(0.f, py, pz);
            };

            f32 ny1, ny2;
            const glm::vec3 y_plane1 = solve_y_fn(light_eye.y, light_eye.z, radius, Dy, ny1);
            const glm::vec3 y_plane2 = solve_y_fn(light_eye.y, light_eye.z, radius, -Dy, ny2);

            if (y_plane1.z < 0.0f) {
                const glm::vec3 y_projected1 = project_fn2(camera.projection_matrix(), y_plane1);
                if (ny1 > 0.0f) {
                    rect.bottom = glm::max(rect.bottom, y_projected1.y);
                } else {
                    rect.top = glm::min(rect.top, y_projected1.y);
                }
            }

            if (y_plane2.z < 0.0f) {
                const glm::vec3 y_projected2 = project_fn2(camera.projection_matrix(), y_plane2);
                if (ny2 > 0.0f) {
                    rect.bottom = glm::max(rect.bottom, y_projected2.y);
                } else {
                    rect.top = glm::min(rect.top, y_projected2.y);
                }
            }
        }
    }

    if (!(rect.left < rect.right) || !(rect.bottom < rect.top)) {
        // No coverage;
        return Error<>();
    }

    Rect result;
    result.left = glm::max((rect.left * 0.5f + 0.5f) * viewport.z, viewport.x);
    result.right = glm::min((rect.right * 0.5f + 0.5f) * viewport.z, viewport.z);
    result.bottom = glm::max((rect.bottom * 0.5f + 0.5f) * viewport.w, viewport.y);
    result.top = glm::min((rect.top * 0.5f + 0.5f) * viewport.w, viewport.w);
    modus_assert(result.left >= viewport.x);
    modus_assert(result.left <= viewport.z);
    modus_assert(result.right >= viewport.x);
    modus_assert(result.right <= viewport.z);
    modus_assert(result.left < result.right);
    modus_assert(result.top >= viewport.y);
    modus_assert(result.top <= viewport.w);
    modus_assert(result.bottom >= viewport.y);
    modus_assert(result.bottom <= viewport.w);
    modus_assert(result.bottom < result.top);

    return Ok(result);
}

Result<> TileLightManager::update(Engine& engine,
                                  const threed::Compositor& compositor,
                                  const Camera& camera,
                                  const Slice<Light>& lights) {
    m_block.ambient = glm::vec4(0.f);
    m_block.num_point_lights = 0;
    m_block.num_dir_lights = 0;
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("LightManager::Update");
        // Update Data
        for (const auto& current_light : lights) {
            const LightType type = current_light.type();
            if (type == LightType::Ambient) {
                m_block.ambient +=
                    glm::vec4(current_light.diffuse_color(), current_light.diffuse_intensity());
            } else if (type == LightType::Directional) {
                if (m_block.num_dir_lights >= LightManager::kMaxDirectionalLights) {
                    MODUS_LOGE(
                        "LightManager: Maximum number directional "
                        "lights reached, ignoring");
                    continue;
                }
                LightManager::DirectionalLight& light = m_block.dir_lights[m_block.num_dir_lights];
                light.direction = glm::normalize(current_light.position());
                light.diffuse_color = current_light.diffuse_color();
                light.diffuse_intensity = current_light.diffuse_intensity();
                m_block.num_dir_lights++;
            } else if (type == LightType::Point) {
                if (m_block.num_point_lights >= LightManager::kMaxPointLights) {
                    MODUS_LOGE(
                        "LightManager: Maximum number of point lights "
                        "reached, ignoring");
                    continue;
                }
                LightManager::PointLight& light = m_block.point_lights[m_block.num_point_lights];
                light.position = current_light.position();
                light.diffuse_color = current_light.diffuse_color();
                light.diffuse_intensity = current_light.diffuse_intensity();
                light.radius = current_light.point_light_radius();
                m_block.num_point_lights++;
            } else {
                modus_assert(false);
                MODUS_LOGE("Forward Light Manager: Unknown light type detected:{}", type);
            }
        }
    }

    const u32 new_n_tiles_y = u32(glm::ceil(f32(compositor.height()) / f32(kTileHeight)));
    const u32 new_n_tiles_x = u32(glm::ceil(f32(compositor.width()) / f32(kTileWidth)));

    m_block.num_tiles_x = new_n_tiles_x;
    m_block.num_tiles_y = new_n_tiles_y;
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("LightManager::Update constants");

        if (auto r_update = device.update_constant_buffer(m_buffer, m_block); !r_update) {
            MODUS_LOGE("Light Manager: Failed to update light constant buffer");
            return Error<>();
        }
        m_shader_snippet->cb_light_binder.m_buffer = m_buffer;
    }

    if (m_block.num_point_lights == 0) {
        return Ok<>();
    }

    // Update Tile list
    bool resolution_changed = false;
    if (new_n_tiles_x != m_n_tiles_x || new_n_tiles_y != m_n_tiles_y) {
        resolution_changed = true;
    }
    m_n_tiles_x = new_n_tiles_x;
    m_n_tiles_y = new_n_tiles_y;
    m_light_grid.resize(m_n_tiles_x * m_n_tiles_y);
    m_light_grid_texture_data.resize(m_n_tiles_x * m_n_tiles_y);
    m_light_indices.reserve(m_light_grid.size() * kMaxLightsPerTile);
    m_light_indices.clear();

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("TileLightManager::Build Grid");
        for (auto& tile : m_light_grid) {
            tile.clear();
        }

        const glm::vec4 viewport = glm::vec4(0, 0, compositor.width(), compositor.height());

        MODUS_UNUSED(viewport);
        for (u32 l = 0; l < m_block.num_point_lights; ++l) {
            const auto& light = m_block.point_lights[l];
            auto r_rect = compute_sreen_rect(camera, light.position, light.radius, viewport);
            if (!r_rect) {
                continue;
            }
            Rect rv2 = r_rect.value_or_panic();
            const f32 left = rv2.left;
            const f32 right = rv2.right;
            const f32 top = rv2.top;
            const f32 bottom = rv2.bottom;

            // Caclulate tile location
            const u32 left_tile = glm::max(i32(glm::floor(left / f32(kTileWidth))), 0);
            const u32 top_tile = glm::min(i32(glm::ceil(top / f32(kTileHeight))), i32(m_n_tiles_y));
            const u32 right_tile =
                glm::min(i32(glm::ceil(right / f32(kTileWidth))), i32(m_n_tiles_x));
            const u32 bottom_tile = glm::max(i32(glm::floor(bottom / f32(kTileHeight))), 0);

            for (u32 i = bottom_tile; i < top_tile; ++i) {
                for (u32 j = left_tile; j < right_tile; ++j) {
                    const u32 index = i * m_n_tiles_x + j;
                    modus_assert(index < m_light_grid.size());
                    if (!m_light_grid[index].push_back(l)) {
                        MODUS_LOGE(
                            "Tile Light Manager: Too many lights on tile "
                            "{}:{}, ignoring futher lights",
                            i, j);
                    }
                }
            }
        }
    }

    {
        MODUS_PROFILE_GRAPHICS_BLOCK("TileLightManager::Build Index list");
        // Prepare light index buffer
        u32 current_offset = 0;
        for (u32 i = 0; i < m_n_tiles_y; ++i) {
            for (u32 j = 0; j < m_n_tiles_x; ++j) {
                // Update light tile lookup texture
                // write to texture (i,j) -> current_offset,
                // m_light_grid[index].size()
                const u32 index = i * m_n_tiles_x + j;
                modus_assert(index < m_light_grid_texture_data.size());
                modus_assert(index < m_light_grid.size());
                modus_assert(current_offset < std::numeric_limits<u16>::max());
                const usize count = m_light_grid[index].size();
                modus_assert(count < usize(std::numeric_limits<u16>::max()));
                m_light_grid_texture_data[index] = {u16(current_offset), u16(count)};
                for (const auto& idx : m_light_grid[index]) {
                    m_light_indices.push_back(idx);
                }
                current_offset += m_light_grid[index].size();
            }
        }
    }

    if (m_light_indices.size() < m_last_indices_count) {
        m_light_indices.resize(m_last_indices_count, 0);
    }
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("TileLightManager::Update textures");
#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        if (resolution_changed && m_light_grid_texture) {
            device.destroy_texture(m_light_grid_texture);
            m_light_grid_texture = threed::TextureHandle();
        }
#else
        if (resolution_changed && m_light_grid_texture) {
            device.destroy_shader_buffer(m_light_grid_texture);
            m_light_grid_texture = threed::ShaderBufferHandle();
        }
#endif

#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        // Update light index texture buffer
        if (m_light_indices.size() > m_last_indices_count) {
            device.destroy_texture(m_light_indices_texture);
            m_light_indices_texture = threed::TextureHandle();
        }
#else
        if (m_light_indices.size() > m_last_indices_count && m_light_indices_texture) {
            device.destroy_shader_buffer(m_light_indices_texture);
            m_light_indices_texture = threed::ShaderBufferHandle();
        }
#endif

#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        if (!m_light_indices_texture) {
            threed::TextureCreateParams params;
            params.width = m_light_indices.size();
            params.height = 1;
            params.depth = 1;
            params.mip_map_levels = 1;
            params.type = threed::TextureType::T2D;
            params.format = threed::TextureFormat::R16_U16;

            threed::TextureData data;
            data.width = m_light_indices.size();
            data.height = 1;
            data.depth = 1;
            data.mip_map_level = 0;
            data.unpack_alignment = 1;
            data.data = make_slice(m_light_indices).as_bytes();

            auto r = device.create_texture(params, Slice(&data, 1));
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to create light index texture");
                return Error<>();
            }

            m_light_indices_texture = *r;
        } else {
            threed::TextureData data;
            data.width = m_light_indices.size();
            data.height = 1;
            data.depth = 1;
            data.mip_map_level = 0;
            data.unpack_alignment = 1;
            data.data = make_slice(m_light_indices).as_bytes();

            auto r = device.update_texture(m_light_indices_texture, Slice(&data, 1));
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to update light index texture");
                return Error<>();
            }
        }
#else

        if (!m_light_indices_texture) {
            const ByteSlice bytes = make_slice(m_light_indices).as_bytes();
            threed::ShaderBufferCreateParams params;
            params.usage = threed::BufferUsage::Stream;
            params.size = bytes.size();
            params.data = bytes.data();
            auto r = device.create_shader_buffer(params);
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to create light index buffer");
                return Error<>();
            }
            m_light_indices_texture = *r;
        } else {
            const ByteSlice bytes = make_slice(m_light_indices).as_bytes();
            auto r =
                device.update_shader_buffer(m_light_indices_texture, bytes.data(), bytes.size());
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to update light index buffer");
                return Error<>();
            }
        }
#endif

#if !defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
        // Update light grid texture
        if (!m_light_grid_texture) {
            threed::TextureCreateParams params;
            params.width = m_n_tiles_x;
            params.height = m_n_tiles_y;
            params.depth = 1;
            params.mip_map_levels = 1;
            params.type = threed::TextureType::T2D;
            params.format = threed::TextureFormat::R16G16_U16;
            auto r = device.create_texture(params);
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to create light grid texture");
                return Error<>();
            }
            m_light_grid_texture = *r;
        }

        {
            threed::TextureData data;
            data.width = m_n_tiles_x;
            data.height = m_n_tiles_y;
            data.depth = 1;
            data.mip_map_level = 0;
            data.unpack_alignment = 1;
            data.data = make_slice(m_light_grid_texture_data).as_bytes();
            auto r = device.update_texture(m_light_grid_texture, Slice(&data, 1));
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to update light grid texture");
                return Error<>();
            }
        }
#else

        // Update light grid texture
        if (!m_light_grid_texture) {
            const ByteSlice bytes = make_slice(m_light_grid_texture_data).as_bytes();
            modus_assert(m_n_tiles_x * m_n_tiles_y * sizeof(u32) == bytes.size());
            threed::ShaderBufferCreateParams params;
            params.usage = threed::BufferUsage::Stream;
            params.data = bytes.data();
            params.size = bytes.size();
            auto r = device.create_shader_buffer(params);
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to create light grid buffer");
                return Error<>();
            }
            m_light_grid_texture = *r;
        } else {
            const ByteSlice bytes = make_slice(m_light_grid_texture_data).as_bytes();
            auto r = device.update_shader_buffer(m_light_grid_texture, bytes.data(), bytes.size());
            if (!r) {
                MODUS_LOGE("TileLightManager: Failed to update light grid buffer");
                return Error<>();
            }
        }
#endif

#if 0
            //! defined(MODUS_RTM)
            {
                for (u32 i = 0; i < m_block.num_point_lights; ++i) {
                    engine.modules().graphics().debug_drawer().draw_sphere(
                        m_block.point_lights[i].position,
                        m_block.point_lights[i].radius,
                        glm::vec4(m_block.point_lights[i].diffuse_color, 1.0f),
                        true);
                }

                auto pass = device.allocate_pass(*engine.modules()
                                                      .graphics()
                                                      .debug_drawer()
                                                      .extra_pipeline(),
                                                 "LightGridDebug");
                auto command = device.allocate_command(*pass);

                pass->viewport.width = compositor.width();
                pass->viewport.height = compositor.height();
                pass->frame_buffer = compositor.frame_buffer();

                command->effect = m_light_grid_debug_effect;
                command->program = m_light_grid_debug_program;
                command->drawable = threed::DrawableHandle();
                command->draw.type = threed::DrawType::Custom;
                command->draw.primitive = threed::Primitive::TriangleStrip;
                command->draw.range_start = 0;
                command->draw.range_count = 4;
                using namespace gpuprogreflect;
                light_grid_debug::set_sampler_u_grid(*command,
                                                     m_light_grid_texture);
                light_grid_debug::set_sampler_u_indices(
                    *command, m_light_indices_texture);
                const auto num_lights_constant =
                    device.upload_constant(f32(m_block.num_point_lights));
                light_grid_debug::set_constant_u_num_lights(
                    *command, num_lights_constant);
            }

#endif
    }

    m_shader_snippet->light_grid_binder = m_light_grid_texture;
    m_shader_snippet->light_indices_binder = m_light_indices_texture;

    return Ok<>();
}
}    // namespace modus::engine::graphics
