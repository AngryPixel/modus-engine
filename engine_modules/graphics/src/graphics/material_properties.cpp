/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/material_properties.hpp>

namespace modus::engine::graphics {

BoolMaterialProperty::BoolMaterialProperty(const StringSlice name, const bool value)
    : MaterialProperty(name), m_value(value) {}

Result<> BoolMaterialProperty::set(const PropertyValue& value) {
    auto r_val = value.as_bool();
    if (!r_val) {
        return Error<>();
    }
    m_value = *r_val;
    return Ok<>();
}

PropertyValue BoolMaterialProperty::get() const {
    return PropertyValue(m_value);
}

U32MaterialProperty::U32MaterialProperty(const StringSlice name, const u32 value)
    : MaterialProperty(name), m_value(value) {}

Result<> U32MaterialProperty::set(const PropertyValue& value) {
    auto r_val = value.as_u32();
    if (!r_val) {
        return Error<>();
    }
    m_value = *r_val;
    return Ok<>();
}

PropertyValue U32MaterialProperty::get() const {
    return PropertyValue(m_value);
}

F32MaterialProperty::F32MaterialProperty(const StringSlice name, const f32 value)
    : MaterialProperty(name), m_value(value) {}

Result<> F32MaterialProperty::set(const PropertyValue& value) {
    auto r_val = value.as_f32();
    if (!r_val) {
        return Error<>();
    }
    m_value = *r_val;
    return Ok<>();
}

PropertyValue F32MaterialProperty::get() const {
    return PropertyValue(m_value);
}

Vec2MaterialProperty::Vec2MaterialProperty(const StringSlice name, const glm::vec2& value)
    : MaterialProperty(name), m_value(value) {}

Result<> Vec2MaterialProperty::set(const PropertyValue& value) {
    auto r_val = value.as_vec2();
    if (!r_val) {
        return Error<>();
    }
    m_value = *r_val;
    return Ok<>();
}

PropertyValue Vec2MaterialProperty::get() const {
    return PropertyValue(m_value);
}

Vec3MaterialProperty::Vec3MaterialProperty(const StringSlice name, const glm::vec3& value)
    : MaterialProperty(name), m_value(value) {}

Result<> Vec3MaterialProperty::set(const PropertyValue& value) {
    auto r_val = value.as_vec3();
    if (!r_val) {
        return Error<>();
    }
    m_value = *r_val;
    return Ok<>();
}

PropertyValue Vec3MaterialProperty::get() const {
    return PropertyValue(m_value);
}

Vec4MaterialProperty::Vec4MaterialProperty(const StringSlice name, const glm::vec4& value)
    : MaterialProperty(name), m_value(value) {}

Result<> Vec4MaterialProperty::set(const PropertyValue& value) {
    auto r_val = value.as_vec4();
    if (!r_val) {
        return Error<>();
    }
    m_value = *r_val;
    return Ok<>();
}

PropertyValue Vec4MaterialProperty::get() const {
    return PropertyValue(m_value);
}

RGBAMaterialProperty::RGBAMaterialProperty(const StringSlice name, const glm::vec4& value)
    : MaterialProperty(name), m_value(value) {}

Result<> RGBAMaterialProperty::set(const PropertyValue& value) {
    if (auto r_val = value.as_vec4(); r_val) {
        m_value = (*r_val) / 255.f;
        return Ok<>();
    }
    if (auto r_val = value.as_vec3(); r_val) {
        m_value = glm::vec4(*r_val / 255.f, 1.0f);
        return Ok<>();
    }
    return Error<>();
}

PropertyValue RGBAMaterialProperty::get() const {
    return PropertyValue(m_value);
}
}    // namespace modus::engine::graphics
