/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/graphics/image.hpp>
#include <engine/graphics/image_asset.hpp>
#include <engine/graphics/texture_util.hpp>
#include <engine/modules/module_graphics.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::ImageAsset)

namespace modus::engine::graphics {

ImageAsset::ImageAsset(const threed::TextureHandle handle)
    : assets::AssetBase(assets::AssetTraits<ImageAsset>::type_id()), m_texture(handle) {}

StringSlice ImageAssetFactory::file_extension() const {
    return "image";
}

ImageAssetFactory::ImageAssetFactory() : factory_type(32) {}

std::unique_ptr<assets::AssetData> ImageAssetFactory::create_asset_data() {
    return modus::make_unique<assets::RamAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> ImageAssetFactory::create(Engine& engine,
                                                              assets::AssetData& data) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ImageAssetCreate");
    const assets::RamAssetData& ram_data = static_cast<const assets::RamAssetData&>(data);
    auto r_image = Image::from_bytes(ram_data.bytes());
    if (!r_image) {
        return Error<>();
    }

    auto graphics_module = engine.module<ModuleGraphics>();
    auto r_texture = image_to_texture(graphics_module->device(), r_image.value());
    if (!r_texture) {
        return Error<>();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(m_asset_pool.construct(*r_texture));
}

Result<> ImageAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ImageAssetDestroy");
    auto r_asset = assets::assetv2_cast<ImageAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    device.destroy_texture(r_asset.value()->m_texture);
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}

}    // namespace modus::engine::graphics
