/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clang-format off
#include <pch.h>
#include <engine/graphics/mesh_asset.hpp>
// clang-format on

#include <engine/graphics/mesh_generated.h>
#include <engine/graphics/mesh_atlas_generated.h>

#include <core/io/memory_stream.hpp>
#include <engine/engine.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/modules/module_animation.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/buffer.hpp>
#include <threed/device.hpp>
#include <threed/drawable.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::MeshAsset)
MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::MeshAtlasAsset)

namespace modus::engine::graphics {

static_assert(u32(fbs::DataType::MAX) == u32(threed::DataType::Total) - 1);

MeshAsset::MeshAsset(const Mesh& mesh,
                     const MeshHandle mesh_handle,
                     const animation::AnimationCatalogHandle animation_catalog)
    : assets::AssetBase(assets::AssetTraits<MeshAsset>::type_id()),
      m_mesh(mesh),
      m_mesh_handle(mesh_handle),
      m_animation_catalog(animation_catalog) {}

MeshAtlasAsset::MeshAtlasAsset(Vector<MeshEntry>&& meshes, Vector<threed::BufferHandle>&& buffers)
    : assets::AssetBase(assets::AssetTraits<MeshAtlasAsset>::type_id()),
      m_meshes(std::move(meshes)),
      m_buffers(std::move(buffers)) {}
MeshAssetFactory::MeshAssetFactory(MeshDB& mesh_db) : factory_type(32), m_mesh_db(mesh_db) {}

StringSlice MeshAssetFactory::file_extension() const {
    return "mesh";
}

static Result<threed::BufferHandle, void> create_data_buffer(threed::Device& device,
                                                             const fbs::Buffer& buffer) {
    threed::BufferCreateParams params;
    params.data = buffer.data()->data();
    params.size = buffer.data()->size();
    params.type = threed::BufferType::Data;
    params.usage = threed::BufferUsage::Static;
    return device.create_buffer(params);
}

static Result<threed::BufferHandle, void> create_index_buffer(threed::Device& device,
                                                              const fbs::Buffer& buffer) {
    threed::BufferCreateParams params;
    params.data = buffer.data()->data();
    params.size = buffer.data()->size();
    params.type = threed::BufferType::Indices;
    params.usage = threed::BufferUsage::Static;
    return device.create_buffer(params);
}

static threed::DrawableDataParams create_drawable_data_param(const fbs::DrawableDesc& desc) {
    threed::DrawableDataParams params;
    params.offset = desc.offset();
    params.data_type = static_cast<threed::DataType>(desc.data_type());
    params.normalized = desc.normalize();
    modus_assert(desc.data_buffer_index() < threed::kMaxDrawableBufferInputs);
    params.buffer_index = desc.data_buffer_index();
    return params;
}

static threed::DrawableCreateParams create_drawable_params(
    const fbs::Drawable& drawable,
    const Slice<threed::BufferHandle> data_buffers,
    const Slice<threed::BufferHandle> index_buffers) {
    threed::DrawableCreateParams params;
    params.start = drawable.draw_start();
    params.count = drawable.draw_count();

    switch (drawable.primitive_type()) {
        case fbs::PrimitiveType::Triangles:
            params.primitive = threed::Primitive::Triangles;
            break;
        case fbs::PrimitiveType::TriangleFan:
            params.primitive = threed::Primitive::TriangleFan;
            break;
        case fbs::PrimitiveType::TriangleStrip:
            params.primitive = threed::Primitive::TriangleStrip;
            break;
        default:
            modus_assert_message(false, "Should not be reached!");
            params.primitive = threed::Primitive::Triangles;
            break;
    }

    const auto data_buffer_descs = drawable.data_buffers();
    modus_assert(data_buffer_descs != nullptr);

    for (usize i = 0; i < data_buffer_descs->size(); ++i) {
        modus_assert(i < threed::kMaxDrawableBufferInputs);
        const auto& desc = data_buffer_descs->Get(i);
        const u8 index = desc->data_buffer_index();
        modus_assert(index < data_buffers.size());
        threed::DrawableDataBufferDesc& data_desc = params.data_buffers[i];
        data_desc.buffer = data_buffers[index];
        data_desc.stride = desc->stride();
        data_desc.offset = desc->offset();
    }
    params.index_type = static_cast<threed::IndicesType>(drawable.index_type());
    if (params.index_type != threed::IndicesType::None) {
        modus_assert(drawable.index_buffer_index() < index_buffers.size());
        params.index_buffer.buffer = index_buffers[drawable.index_buffer_index()];
    }
    params.index_buffer.offset = drawable.index_buffer_offset();

    // Vertex
    const fbs::DrawableDesc* vertex_desc = drawable.vertex();
    if (vertex_desc != nullptr) {
        params.data[kVertexSlot] = create_drawable_data_param(*vertex_desc);
    }

    // Normals
    const fbs::DrawableDesc* normal_desc = drawable.normals();
    if (normal_desc != nullptr) {
        params.data[kNormalSlot] = create_drawable_data_param(*normal_desc);
    }

    // Texutre
    const fbs::DrawableDesc* texture_desc = drawable.texture();
    if (texture_desc != nullptr) {
        params.data[kTextureSlot] = create_drawable_data_param(*texture_desc);
    }

    // Tangent
    const fbs::DrawableDesc* tangent_desc = drawable.tangent();
    if (tangent_desc != nullptr) {
        params.data[kTangentSlot] = create_drawable_data_param(*tangent_desc);
    }

    // Binormal
    const fbs::DrawableDesc* binormal_desc = drawable.binormal();
    if (binormal_desc != nullptr) {
        params.data[kBinormalSlot] = create_drawable_data_param(*binormal_desc);
    }

    // custom1
    const fbs::DrawableDesc* custom1_desc = drawable.custom1();
    if (custom1_desc != nullptr) {
        params.data[kCustom1Slot] = create_drawable_data_param(*custom1_desc);
    }

    // custom2
    const fbs::DrawableDesc* custom2_desc = drawable.custom2();
    if (custom2_desc != nullptr) {
        params.data[kCustom2Slot] = create_drawable_data_param(*custom2_desc);
    }

    // custom3
    const fbs::DrawableDesc* custom3_desc = drawable.custom3();
    if (custom3_desc != nullptr) {
        params.data[kCustom3Slot] = create_drawable_data_param(*custom3_desc);
    }

    // custom4
    const fbs::DrawableDesc* custom4_desc = drawable.custom4();
    if (custom4_desc != nullptr) {
        params.data[kCustom4Slot] = create_drawable_data_param(*custom4_desc);
    }

    const fbs::DrawableDesc* joint_idx_desc = drawable.bone_indices();
    if (joint_idx_desc != nullptr) {
        params.data[kJointIndicesSlot] = create_drawable_data_param(*joint_idx_desc);
    }

    const fbs::DrawableDesc* joint_weights_desc = drawable.bone_weights();
    if (joint_weights_desc != nullptr) {
        params.data[kJointWeightSlot] = create_drawable_data_param(*joint_weights_desc);
    }
    return params;
}

class MeshAssetData final : public assets::RamAssetData {
   public:
    const fbs::Mesh* m_mesh = nullptr;
    GID m_guid;

    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        MODUS_PROFILE_GRAPHICS_BLOCK("MeshAssetLoad");
        if (!assets::RamAssetData::load(reader)) {
            return Error<>();
        }

        const ByteSlice bytes = this->bytes();
        flatbuffers::Verifier v(bytes.data(), bytes.size());
        if (!fbs::VerifyMeshBuffer(v)) {
            MODUS_LOGE("MeshAsset: File is not a mesh!");
            return Error<>();
        }

        const fbs::Mesh* fbs_mesh = fbs::GetMesh(bytes.data());

        const auto* fbs_submeshes = fbs_mesh->submeshes();
        if (fbs_submeshes == nullptr || fbs_submeshes->size() == 0) {
            MODUS_LOGE("MeshAsset: No submeshes specified in mesh, at least one is required.");
            return Error<>();
        }

        if (fbs_submeshes->size() > Mesh::kMaxSubMeshCount) {
            MODUS_LOGE(
                "MeshAsset: Mesh has {} sub meshes, we can only "
                "hanlde up to {} "
                "sub meshes",
                fbs_submeshes->size(), Mesh::kMaxSubMeshCount);
            return Error<>();
        }

        const auto* fbs_data_buffers = fbs_mesh->data_buffers();
        if (fbs_data_buffers == nullptr) {
            MODUS_LOGE("MeshAsset: No data buffer specified in mesh");
            return Error<>();
        }

        if (fbs_data_buffers->size() > Mesh::kMaxSubMeshCount) {
            MODUS_LOGE("MeshAsset: We currently only support {} data buffer(s) per mesh",
                       Mesh::kMaxSubMeshCount);
            return Error<>();
        }

        const auto* fbs_index_buffers = fbs_mesh->index_buffers();
        if (fbs_index_buffers != nullptr && fbs_index_buffers->size() > Mesh::kMaxSubMeshCount) {
            MODUS_LOGE(
                "MeshAsset: We currently only support {} index buffer(s) per "
                "mesh",
                Mesh::kMaxSubMeshCount);
            return Error<>();
        }

        const ByteSlice guid_slice(fbs_mesh->guid()->value()->data(),
                                   fbs_mesh->guid()->value()->size());
        auto r_guid = GID::from_slice(guid_slice);
        if (!r_guid) {
            MODUS_LOGE("MeshAsset: Asset contains invalid guid");
            return Error<>();
        }
        m_guid = *r_guid;

        m_mesh = fbs_mesh;
        return Ok(assets::AssetLoadResult::Done);
    }
};

std::unique_ptr<assets::AssetData> MeshAssetFactory::create_asset_data() {
    return modus::make_unique<MeshAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> MeshAssetFactory::create(Engine& engine,
                                                             assets::AssetData& data) {
    MODUS_PROFILE_GRAPHICS_BLOCK("MeshAssetCreate");
    const MeshAssetData& mesh_data = static_cast<const MeshAssetData&>(data);

    const fbs::Mesh* fbs_mesh = mesh_data.m_mesh;
    Mesh mesh;
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    const auto* fbs_data_buffers = fbs_mesh->data_buffers();
    // Create Data Buffers
    for (usize i = 0; i < fbs_data_buffers->size(); ++i) {
        modus_assert(i < Mesh::kMaxSubMeshCount);
        auto r_data_buffer = create_data_buffer(device, *fbs_data_buffers->Get(i));
        if (!r_data_buffer) {
            MODUS_LOGE("MeshAsset: Failed to create data buffer {}", i);
            return Error<>();
        }
        if (!mesh.vertices.push_back(*r_data_buffer)) {
            MODUS_LOGE("MeshAsset: Failed to add data buffer {:02}", i);
            return Error<>();
        }
    }

    // Create Index buffers
    const auto* fbs_index_buffers = fbs_mesh->index_buffers();
    if (fbs_index_buffers != nullptr) {
        for (usize i = 0; i < fbs_index_buffers->size(); ++i) {
            modus_assert(i < Mesh::kMaxSubMeshCount);
            auto r_index_buffer = create_index_buffer(device, *fbs_index_buffers->Get(i));
            if (!r_index_buffer) {
                MODUS_LOGE("MeshAsset: Failed to create index buffer {}", i);
                return Error<>();
            }
            if (!mesh.indices.push_back(*r_index_buffer)) {
                MODUS_LOGE("MeshAsset: Failed to add index buffer {:02}", i);
                return Error<>();
            }
        }
    }

    const fbs::BoundingVolumeSphere& fbs_bv_sphere = fbs_mesh->bounding_volume()->sphere();
    mesh.bounding_sphere.center.x = fbs_bv_sphere.origin().x();
    mesh.bounding_sphere.center.y = fbs_bv_sphere.origin().y();
    mesh.bounding_sphere.center.z = fbs_bv_sphere.origin().z();
    mesh.bounding_sphere.radius = fbs_bv_sphere.radius();

    const auto fbs_submeshes = fbs_mesh->submeshes();
    for (u32 i = 0; i < Mesh::kMaxSubMeshCount && i < fbs_submeshes->size(); ++i) {
        const auto* fbs_drawable = fbs_submeshes->Get(i);
        if (fbs_drawable != nullptr) {
            const threed::DrawableCreateParams drawable_params = create_drawable_params(
                *fbs_drawable, make_slice(mesh.vertices), make_slice(mesh.indices));
            auto r_drawable = device.create_drawable(drawable_params);
            if (!r_drawable) {
                MODUS_LOGE("MeshAsset: Failed to create sub mesh {}", i);
                return Error<>();
            }
            SubMesh sub_mesh;
            sub_mesh.drawable = r_drawable.value();
            sub_mesh.material_index = fbs_drawable->material_index();
            if (!mesh.sub_meshes.push_back(sub_mesh)) {
                MODUS_LOGE("MeshAsset: Failed to add sub mesh {:02}", i);
                return Error<>();
            }
        }
    }

    animation::AnimationCatalogHandle anim_catalog_handle;
    if (const auto fbs_anim_catalog = fbs_mesh->animation_catalog(); fbs_anim_catalog != nullptr) {
        auto animation_module = engine.module<ModuleAnimation>();
        if (animation_module) {
            animation::AnimationCatalog anim_catalog;
            if (!anim_catalog.initialize(*fbs_anim_catalog)) {
                MODUS_LOGE("Failed to load animation catalog");
                return Error<>();
            }

            if (auto r = animation_module->animation_catalog_db().create(std::move(anim_catalog));
                r) {
                anim_catalog_handle = (*r).first;
            } else {
                MODUS_LOGE("Failed to register animation catalog");
                return Error<>();
            }
        } else {
            MODUS_LOGW(
                "Mesh contains animation catalog but Animation module is not "
                "present");
        }
    }
    mesh.animation_catlog = anim_catalog_handle;
    auto r_mesh_handle = m_mesh_db.add(mesh_data.m_guid, mesh);
    if (!r_mesh_handle) {
        MODUS_LOGE("Failed to add mesh to mesh db");
        return Error<>();
    }

    return Ok<NotMyPtr<assets::AssetBase>>(
        m_asset_pool.construct(mesh, *r_mesh_handle, anim_catalog_handle));
}

Result<> MeshAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GRAPHICS_BLOCK("MeshAssetDestroy");
    auto r_asset = assets::assetv2_cast<MeshAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    if ((*r_asset)->m_animation_catalog) {
        auto animation_module = engine.module<ModuleAnimation>();
        if (animation_module) {
            if (!animation_module->animation_catalog_db().erase((*r_asset)->m_animation_catalog)) {
                MODUS_LOGE("Failed to delete animation catalog");
            }
        }
    }

    auto& device = graphics_module->device();

    const Mesh& mesh = (*r_asset)->m_mesh;

    for (auto& buffer : mesh.vertices) {
        if (buffer.is_valid()) {
            device.destroy_buffer(buffer);
        }
    }

    for (auto& buffer : mesh.indices) {
        if (buffer.is_valid()) {
            device.destroy_buffer(buffer);
        }
    }

    for (auto& sub_mesh : mesh.sub_meshes) {
        if (sub_mesh.drawable.is_valid()) {
            device.destroy_drawable(sub_mesh.drawable);
        }
    }

    // Remove from mesh db
    m_mesh_db.erase((*r_asset)->m_mesh_handle);
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}

// -----------------------------------------------------------------------------------------------
// MeshAtlasFactory
// -----------------------------------------------------------------------------------------------

class MeshAtlasAssetData final : public assets::RamAssetData {
   public:
    const fbs::MeshAtlas* m_atlas = nullptr;
    GID m_guid;

    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        MODUS_PROFILE_GRAPHICS_BLOCK("MeshAtlasAssetLoad");
        if (!assets::RamAssetData::load(reader)) {
            return Error<>();
        }

        const ByteSlice bytes = this->bytes();
        flatbuffers::Verifier v(bytes.data(), bytes.size());
        if (!fbs::VerifyMeshAtlasBuffer(v)) {
            MODUS_LOGE("MeshAsset: File is not a mesh!");
            return Error<>();
        }

        const fbs::MeshAtlas* fbs_atlas = fbs::GetMeshAtlas(bytes.data());

        const auto* fbs_meshes = fbs_atlas->meshes();
        if (fbs_meshes == nullptr || fbs_meshes->size() == 0) {
            MODUS_LOGE("MeshAsset: No meshes specified in mesh atlas, at least one is required.");
            return Error<>();
        }

        for (size_t i = 0; i < fbs_meshes->size(); ++i) {
            const auto& mesh = fbs_meshes->Get(i);
            if (mesh->submeshes()->size() > Mesh::kMaxSubMeshCount) {
                MODUS_LOGE(
                    "MeshAsset: Mesh {} has {} sub meshes, we can only "
                    "hanlde up to {} "
                    "sub meshes",
                    i, mesh->submeshes()->size(), Mesh::kMaxSubMeshCount);
                return Error<>();
            }
        }

        const auto* fbs_data_buffers = fbs_atlas->data_buffers();
        if (fbs_data_buffers == nullptr) {
            MODUS_LOGE("MeshAsset: No data buffer specified in mesh");
            return Error<>();
        }

        if (fbs_data_buffers->size() > Mesh::kMaxSubMeshCount) {
            MODUS_LOGE("MeshAsset: We currently only support {} data buffer(s) per mesh",
                       Mesh::kMaxSubMeshCount);
            return Error<>();
        }

        const auto* fbs_index_buffers = fbs_atlas->index_buffers();
        if (fbs_index_buffers != nullptr && fbs_index_buffers->size() > Mesh::kMaxSubMeshCount) {
            MODUS_LOGE(
                "MeshAsset: We currently only support {} index buffer(s) per "
                "mesh",
                Mesh::kMaxSubMeshCount);
            return Error<>();
        }

        const ByteSlice guid_slice(fbs_atlas->guid()->value()->data(),
                                   fbs_atlas->guid()->value()->size());
        auto r_guid = GID::from_slice(guid_slice);
        if (!r_guid) {
            MODUS_LOGE("MeshAsset: Asset contains invalid guid");
            return Error<>();
        }
        m_guid = *r_guid;

        m_atlas = fbs_atlas;
        return Ok(assets::AssetLoadResult::Done);
    }
};

std::unique_ptr<assets::AssetData> MeshAtlasAssetFactory::create_asset_data() {
    return modus::make_unique<MeshAtlasAssetData>();
}

StringSlice MeshAtlasAssetFactory::file_extension() const {
    return "meshatlas";
}

Result<NotMyPtr<assets::AssetBase>> MeshAtlasAssetFactory::create(Engine& engine,
                                                                  assets::AssetData& data) {
    MODUS_PROFILE_GRAPHICS_BLOCK("MeshAtlasAssetCreate");
    const MeshAtlasAssetData& mesh_data = static_cast<const MeshAtlasAssetData&>(data);

    const fbs::MeshAtlas* fbs_atlas = mesh_data.m_atlas;

    Vector<threed::BufferHandle> vertex_buffers;
    Vector<threed::BufferHandle> index_buffers;
    Vector<MeshAtlasAsset::MeshEntry> mesh_entries;

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    const auto* fbs_data_buffers = fbs_atlas->data_buffers();
    vertex_buffers.reserve(fbs_data_buffers->size());
    // Create Data Buffers
    for (usize i = 0; i < fbs_data_buffers->size(); ++i) {
        modus_assert(i < Mesh::kMaxSubMeshCount);
        auto r_data_buffer = create_data_buffer(device, *fbs_data_buffers->Get(i));
        if (!r_data_buffer) {
            MODUS_LOGE("MeshAtlasAsset: Failed to create data buffer {}", i);
            return Error<>();
        }
        vertex_buffers.push_back(*r_data_buffer);
    }

    // Create Index buffers
    const auto* fbs_index_buffers = fbs_atlas->index_buffers();
    if (fbs_index_buffers != nullptr) {
        index_buffers.reserve(fbs_index_buffers->size());
        for (usize i = 0; i < fbs_index_buffers->size(); ++i) {
            modus_assert(i < Mesh::kMaxSubMeshCount);
            auto r_index_buffer = create_index_buffer(device, *fbs_index_buffers->Get(i));
            if (!r_index_buffer) {
                MODUS_LOGE("MeshAtlasAsset: Failed to create index buffer {}", i);
                return Error<>();
            }
            index_buffers.push_back(*r_index_buffer);
        }
    }

    const auto* fbs_meshes = fbs_atlas->meshes();
    for (size_t m = 0; m < fbs_meshes->size(); ++m) {
        Mesh mesh;
        const auto* fbs_mesh = fbs_meshes->Get(m);
        const fbs::BoundingVolumeSphere& fbs_bv_sphere = fbs_mesh->bounding_volume()->sphere();
        mesh.bounding_sphere.center.x = fbs_bv_sphere.origin().x();
        mesh.bounding_sphere.center.y = fbs_bv_sphere.origin().y();
        mesh.bounding_sphere.center.z = fbs_bv_sphere.origin().z();
        mesh.bounding_sphere.radius = fbs_bv_sphere.radius();

        const ByteSlice guid_slice(fbs_mesh->guid()->value()->data(),
                                   fbs_mesh->guid()->value()->size());
        auto r_guid = GID::from_slice(guid_slice);
        if (!r_guid) {
            MODUS_LOGE("MeshAtlasAsset: Mesh {:02} contains invalid guid", m);
            return Error<>();
        }

        const auto fbs_submeshes = fbs_mesh->submeshes();
        for (u32 i = 0; i < Mesh::kMaxSubMeshCount && i < fbs_submeshes->size(); ++i) {
            const auto* fbs_drawable = fbs_submeshes->Get(i);
            if (fbs_drawable != nullptr) {
                const threed::DrawableCreateParams drawable_params = create_drawable_params(
                    *fbs_drawable, make_slice(vertex_buffers), make_slice(index_buffers));
                auto r_drawable = device.create_drawable(drawable_params);
                if (!r_drawable) {
                    MODUS_LOGE("MeshAtlasAsset: Failed to create sub mesh {:02} from Mesh {:02}", i,
                               m);
                    return Error<>();
                }
                SubMesh sub_mesh;
                sub_mesh.drawable = r_drawable.value();
                sub_mesh.material_index = fbs_drawable->material_index();
                if (!mesh.sub_meshes.push_back(sub_mesh)) {
                    MODUS_LOGE("MeshAtlasAsset: Failed to add sub mesh {:02} from Mesh {:02}", i,
                               m);
                    return Error<>();
                }
            }
        }

        animation::AnimationCatalogHandle anim_catalog_handle;
        if (const auto fbs_anim_catalog = fbs_mesh->animation_catalog();
            fbs_anim_catalog != nullptr) {
            auto animation_module = engine.module<ModuleAnimation>();
            if (animation_module) {
                animation::AnimationCatalog anim_catalog;
                if (!anim_catalog.initialize(*fbs_anim_catalog)) {
                    MODUS_LOGE("MeshAtlas: Failed to load animation catalog for Mesh {:02}", m);
                    return Error<>();
                }

                if (auto r =
                        animation_module->animation_catalog_db().create(std::move(anim_catalog));
                    r) {
                    anim_catalog_handle = (*r).first;
                } else {
                    MODUS_LOGE("MeshAtlas: Failed to register animation catalog for Mesh {:02}", 2);
                    return Error<>();
                }
            } else {
                MODUS_LOGW(
                    "MeshAtlas: Mesh {:02} contains animation catalog but Animation module is not "
                    "present",
                    m);
            }
        }
        mesh.animation_catlog = anim_catalog_handle;
        auto r_mesh_handle = m_mesh_db.add(*r_guid, mesh);
        if (!r_mesh_handle) {
            MODUS_LOGE("MeshAtlas: Failed to add Mesh {:02} to mesh db", m);
            return Error<>();
        }
        mesh_entries.push_back(
            {String(fbs_mesh->name()->c_str()), mesh, *r_mesh_handle, anim_catalog_handle});
    }
    vertex_buffers.insert(vertex_buffers.end(), index_buffers.begin(), index_buffers.end());
    return Ok<NotMyPtr<assets::AssetBase>>(
        m_asset_pool.construct(std::move(mesh_entries), std::move(vertex_buffers)));
}

Result<> MeshAtlasAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GRAPHICS_BLOCK("MeshAtlasAssetDestroy");
    auto r_asset = assets::assetv2_cast<MeshAtlasAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    for (auto& mesh : (*r_asset)->m_meshes) {
        if (mesh.animation_catalog) {
            auto animation_module = engine.module<ModuleAnimation>();
            if (animation_module) {
                if (!animation_module->animation_catalog_db().erase(mesh.animation_catalog)) {
                    MODUS_LOGE("Failed to delete animation catalog");
                }
            }
        }
        for (auto& sub_mesh : mesh.mesh.sub_meshes) {
            if (sub_mesh.drawable.is_valid()) {
                device.destroy_drawable(sub_mesh.drawable);
            }
        }
        m_mesh_db.erase(mesh.mesh_handle);
    }
    for (auto& buffer : (*r_asset)->m_buffers) {
        device.destroy_buffer(buffer);
    }

    // Remove from mesh db
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}
MeshAtlasAssetFactory::MeshAtlasAssetFactory(MeshDB& mesh_db)
    : factory_type(32), m_mesh_db(mesh_db) {}

}    // namespace modus::engine::graphics
