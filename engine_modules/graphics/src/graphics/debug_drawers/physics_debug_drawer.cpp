/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/graphics/debug_drawers/physics_debug_drawer.hpp>
#include <engine/modules/module_graphics.hpp>

namespace modus::engine::graphics {

PhysicsDebugDrawer::PhysicsDebugDrawer(engine::Engine& engine)
    : m_drawer(engine.module<ModuleGraphics>()->debug_drawer()) {}

void PhysicsDebugDrawer::draw_line(const glm::vec3& from,
                                   const glm::vec3& to,
                                   const glm::vec3& colour_from,
                                   const glm::vec3& colour_to) {
    m_drawer.draw_line(from, to, glm::vec4(colour_from, 1.0f), glm::vec4(colour_to, 1.0f));
}

void PhysicsDebugDrawer::draw_text3d(const glm::vec3& pos, const StringSlice text) {
    MODUS_UNUSED(pos);
    MODUS_UNUSED(text);
    MODUS_LOGW("PhysicsDebugDrawer: No current support for displaying 3D text");
}
}    // namespace modus::engine::graphics
