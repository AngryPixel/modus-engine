/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/engine.hpp>
#include <engine/graphics/debug_drawers/animation_debug_drawer.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/modules/module_animation.hpp>

namespace modus::engine::graphics {
AnimationDebugDrawer::AnimationDebugDrawer(engine::Engine& engine)
    : m_drawer(engine.module<ModuleGraphics>()->debug_drawer()) {
    m_module = engine.module<ModuleAnimation>();
}

void AnimationDebugDrawer::draw_skeleton_from_animator(
    const glm::mat4& transform,
    const engine::animation::AnimatorHandle handle,
    const bool animated) {
    auto r_skeleton = m_module->manager().animation_skeleton(handle);
    if (!r_skeleton) {
        MODUS_LOGE("AnimationDebugDrawer: Failed to retreive skeleton");
        return;
    }

    Optional<Slice<glm::mat4>> anim_data;
    if (animated) {
        auto r_data = m_module->manager().animation_data(handle);
        if (!r_data) {
            MODUS_LOGE("AnimationDebugDrawer: Failed to retreive animation data");
            return;
        }
        anim_data = *r_data;
    }
    engine::animation::DebugDrawer::draw_skeleton(transform, **r_skeleton, anim_data);
}

void AnimationDebugDrawer::draw_line(const glm::vec3& from,
                                     const glm::vec3& to,
                                     const glm::vec4& color) {
    m_drawer.draw_line(from, to, color);
}

void AnimationDebugDrawer::draw_text3d(const StringSlice text,
                                       const glm::vec3& point,
                                       const f32 scale,
                                       const glm::vec3& color) {
    m_drawer.draw_text_3d(text, point, scale, color);
}

void AnimationDebugDrawer::draw_sphere(const glm::vec3& center,
                                       const f32 radius,
                                       const glm::vec4& color) {
    m_drawer.draw_sphere(center, radius, color, true);
}

}    // namespace modus::engine::graphics
