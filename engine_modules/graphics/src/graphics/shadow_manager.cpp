/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/shadow_manager.hpp>
#include <engine/engine.hpp>
#include <threed/buffer.hpp>
#include <threed/device.hpp>
#include <engine/graphics/light.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/texture.hpp>
#include <threed/effect.hpp>
#include <threed/sampler.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/graphics/ecs/components/graphics_component.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <threed/program_gen/shader_generator.hpp>
#include <threed/program_gen/program_gen.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <engine/graphics/default_program_inputs.hpp>

namespace modus::engine::graphics {

constexpr u32 kShadowMapHeight = 512;
constexpr u32 kShadowMapWidth = 512;

static const char* kShadowCodeBlock = R"R(
#define ENGINE_SHADOW_MANAGER_ENABLED
// Calculate whether the light space project coordinate has shadow coverage
// \return 1.0 on true, 0.0 on false.
float calculate_shadows(in vec3 proj_coord, in vec3 normal) {
    // Sample depth buffer
    float closest_depth = texture(u_shadow_map, proj_coord.xy).r;
    float current_depth = proj_coord.z;

    // early exit
    if (current_depth > 1.0) {
        return 0.0;
    }

    // check whether current position is in shadow
    // Apply bias based on the original light direction and the current normal
    // Helps with round objects
    float cos_theta = clamp(dot(normal, u_shadow_light_dir.xyz), 0.0, 1.0);
    float bias = 0.005*tan(acos(cos_theta));
    bias = clamp(bias, 0.0,0.005);

#if defined(USE_PCF)
    // Apply PCF Filtering with 16 samples
    float shadow = 0.0;
    vec2 texel_size = 1.0 / vec2(textureSize(u_shadow_map, 0));
    for(float x = -1.5; x <= 1.5; x+=1.0)
    {
        for(float y = -1.5; y <= 1.5; y+=1.0)
        {
            float pcf_depth = texture(u_shadow_map, proj_coord.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth - bias > pcf_depth ? 1.0 : 0.0;
        }
    }
    shadow /= 16.0;
    return shadow;
# else
    float pcf_depth = texture(u_shadow_map, proj_coord.xy).r;
    return current_depth - bias > pcf_depth ? 1.0 : 0.0;
#endif
}

float calculate_shadows_light_space(in vec4 position_light_space, in vec3 normal) {
    if (num_shadow_casters == 0) {
        return 0.0;
    }
    // perspective divide to convert to light screen space
    vec3 proj_coord = position_light_space.xyz / position_light_space.w;
    // normalize to [0 - 1]
    proj_coord = (proj_coord + 1.0) * 0.5;
    return calculate_shadows(proj_coord, normal);
}

float calculate_shadows_world_pos(in vec3 world_position, in vec3 normal) {
    if (num_shadow_casters == 0) {
        return 0.0;
    }
    // TODO: Bias Matrix ??
    // convert to light space
    vec4 position_light_space = u_shadow_vp * vec4(world_position, 1.0);
    // perspective divide to convert to light screen space
    vec3 proj_coord = position_light_space.xyz / position_light_space.w;
    // normalize to [0 - 1]
    proj_coord = (proj_coord + 1.0) * 0.5;
    return calculate_shadows(proj_coord, normal);
}
)R";

struct ShadowCasterCBInput final : public threed::dynamic::DefaultConstantBufferInput {
    ShadowCasterCBInput(NotMyPtr<const threed::dynamic::InputBinder> binder)
        : threed::dynamic::DefaultConstantBufferInput("ShadowBlock", binder) {
        m_desc.buffer_members.push_back({"u_shadow_vp", threed::DataType::Mat4F32});
        m_desc.buffer_members.push_back({"u_shadow_light_dir", threed::DataType::Vec4F32});
        m_desc.buffer_members.push_back({"num_shadow_casters", threed::DataType::I32});
    }

    Result<threed::dynamic::BindableInputResult> generate_code_snippet(
        threed::dynamic::GenerateParams& params) const override {
        if (!params.writer->write_exactly(StringSlice(kShadowCodeBlock).as_bytes())) {
            return Error<>();
        }
        return Ok(threed::dynamic::BindableInputResult::Generated);
    }
};

struct ShadowManager::ShaderSnippet final : public threed::dynamic::ShaderSnippet {
    threed::dynamic::DefaultConstantBufferInputBinder cb_binder;
    threed::dynamic::DefaultSamplerInputBinder shadow_map_binder;

    ShadowCasterCBInput cb_input;
    threed::dynamic::DefaultSamplerInput shadow_map_input;

    ShaderSnippet()
        : threed::dynamic::ShaderSnippet("engine.shadows"),
          cb_input(&cb_binder),
          shadow_map_input("u_shadow_map", &shadow_map_binder) {}

    Result<> apply_common(threed::dynamic::ShaderStageCommon& stage) const {
        if (!stage.constant_buffers.push_back(&cb_input)) {
            MODUS_LOGE("Failed to add shadow cb to stage");
            return Error<>();
        }
        if (!stage.samplers.push_back(&shadow_map_input)) {
            MODUS_LOGE("Failed to add shadow texture to stage");
            return Error<>();
        }
        return Ok<>();
    }

    Result<> apply(threed::dynamic::ShaderVertexStage& vertex_stage) const override {
        return apply_common(vertex_stage);
    }

    Result<> apply(threed::dynamic::ShaderFragmentStage& fragment_stage) const override {
        return apply_common(fragment_stage);
    }
};

ShadowManager::ShadowManager() {
    m_shader_snippet = modus::make_unique<ShaderSnippet>();
}

ShadowManager::~ShadowManager() {}

Result<> ShadowManager::initialize(Engine& engine) {
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();

    threed::SamplerCreateParams sampler_params;
    sampler_params.filter_mag = threed::SamplerFilter::Linear;
    sampler_params.filter_min = threed::SamplerFilter::Linear;
    sampler_params.wrap_r = threed::SamplerWrapMode::Repeat;
    sampler_params.wrap_s = threed::SamplerWrapMode::ClampToEdge;
    sampler_params.wrap_t = threed::SamplerWrapMode::ClampToEdge;

    if (auto r = device.create_sampler(sampler_params); !r) {
        MODUS_LOGE("Failed to create shadow map sampler:{}", r.error());
        return Error<>();
    } else {
        m_sampler = *r;
        m_shader_snippet->shadow_map_binder.m_sampler = m_sampler;
    }

    // Shape Program
    {
        const auto& shader_generator = device.shader_generator();
        threed::dynamic::TypedDefaultConstantInput<glm::mat4> mvp_input("u_mvp", &m_mvp_input);
        threed::dynamic::ProgramGenerator generator;
        threed::dynamic::ProgramGenParams params;
        params.vertex_stage.constants.push_back(&mvp_input).expect();
        params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
        params.vertex_stage.main = R"R(
void main(){
    gl_Position = u_mvp * VERTEX;
}
)R";
        params.fragment_stage.main = "void main(){}";
        params.skip_instanced_variant = true;

        if (auto r = generator.generate(params, shader_generator); !r) {
            MODUS_LOGE("Failed to generate shadow program: {}", r.error());
            return Error<>();
        }

        auto result = generator.result(0).value_or_panic();
        m_program_binders = result->binders;

        if (auto r = device.create_program(result->create_params); !r) {
            MODUS_LOGE("Failed to create shadow program: {}", r.error());
            return Error<>();
        } else {
            m_program = *r;
        }
        generator.reset();
    }

    for (u32 i = 0; i < kBufferCount; ++i) {
        {
            threed::TextureCreateParams params;
            params.width = kShadowMapWidth;
            params.height = kShadowMapHeight;
            params.format = threed::TextureFormat::Depth32F;
            params.mip_map_levels = 1;
            params.border_color = glm::vec4(1.0);

            if (auto r_texture = device.create_texture(params); !r_texture) {
                MODUS_LOGE("ShadowManager: Failed to create shadow texture");
                return Error<>();
            } else {
                m_map[i] = *r_texture;
            }
        }
        {
            threed::FramebufferCreateParams params;
            params.depth.handle = m_map[i];

            if (auto r_fbo = device.create_framebuffer(params); !r_fbo) {
                MODUS_LOGE("ShadowManager: Failed to create shadow fbo");
                return Error<>();
            } else {
                m_fbo[i] = *r_fbo;
            }
        }
    }
    {
        threed::EffectCreateParams params;
        params.state.raster.cull_mode = threed::CullMode::Front;
        params.state.raster.cull_enabled = true;
        params.state.depth_stencil.depth.enabled = true;

        if (auto r_effect = device.create_effect(params); !r_effect) {
            MODUS_LOGE("ShadowManager: Failed to create effect");
            return Error<>();
        } else {
            m_effect = *r_effect;
        }
    }
    {
        threed::ConstantBufferCreateParams params;
        params.usage = threed::BufferUsage::Stream;
        params.size = sizeof(DirShadowCasterBlock);
        params.data = nullptr;
        if (auto r = device.create_constant_buffer(params); !r) {
            MODUS_LOGE("ShadowManager: Failed to create shadow constant buffer");
        } else {
            m_buffer = *r;
        }
    }

    if (!graphics_module->shader_snippet_db().register_snippet(m_shader_snippet.get())) {
        MODUS_LOGE("TileLightManager: Failed to register shader snippet");
        return Error<void>();
    }

    m_shader_snippet->shadow_map_binder.m_texture = m_map[0];

    return Ok<>();
}

Result<> ShadowManager::shutdown(Engine& engine) {
    if (auto r = engine.module<ModuleGraphics>(); r) {
        r->shader_snippet_db().unregister_snippet(m_shader_snippet.get());
        auto& device = r->device();
        for (u32 i = 0; i < kBufferCount; ++i) {
            if (m_fbo[i]) {
                device.destroy_framebuffer(m_fbo[i]);
            }
            if (m_map[i]) {
                device.destroy_texture(m_map[i]);
            }
        }
        if (m_effect) {
            device.destroy_effect(m_effect);
        }
        if (m_buffer) {
            device.destroy_constant_buffer(m_buffer);
        }
        if (m_sampler) {
            device.destroy_sampler(m_sampler);
        }
        if (m_program) {
            device.destroy_program(m_program);
        }
    }
    return Ok<>();
}

Result<> ShadowManager::update(Engine& engine, const Slice<Light>& lights) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ShadowManager::Update");
    MODUS_UNUSED(engine);
    m_block.num_casters = 0;
    for (const auto& current_light : lights) {
        const LightType type = current_light.type();
        if (type == LightType::Directional && current_light.emits_shadow()) {
            if (m_block.num_casters != 0) {
                MODUS_LOGE(
                    "ShadowManager: Only one directional shadow caster "
                    "supported, this one will be ignored");
            } else {
                m_block.num_casters = 1;
                const f32 half_x = current_light.directional_light_area_x() * 0.5f;
                const f32 half_y = current_light.directional_light_area_y() * 0.5f;
                const f32 near_plane = current_light.directional_light_near_plane();
                const f32 far_plane = current_light.directional_light_far_plane();
                m_camera.frustum().set_orthograhic(near_plane, far_plane, half_y, -half_y, -half_x,
                                                   half_x);
                m_camera.set_look_at(current_light.position(), glm::vec3(0.f),
                                     glm::vec3(0.f, 1.0f, 0.f));
                m_block.proj_view_matrix = m_camera.projection_view_matrix();
                m_block.direction = glm::vec4(-glm::normalize(current_light.position()), 0.0f);
            }
        }
    }

    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    if (!device.update_constant_buffer(m_buffer, m_block)) {
        MODUS_LOGE("ShadowManager: Failed to update shadow block");
        return Error<>();
    }

    m_shader_snippet->cb_binder.m_buffer = m_buffer;
    return Ok<>();
}

void ShadowManager::build_pass(Engine& engine, ShadowMapParams& params) {
    MODUS_PROFILE_GRAPHICS_BLOCK("ShadowManager::BuildPass");
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    if (m_block.num_casters != 0) {
        m_renderables.clear();
        m_active_buffer = (m_active_buffer + 1) % kBufferCount;
        m_shader_snippet->shadow_map_binder.m_texture = m_map[m_active_buffer];

        {
            MODUS_PROFILE_GRAPHICS_BLOCK("Collect Renderables");
            const engine::gameplay::EntityManager& entity_manager =
                params.game_world.entity_manager();
            m_renderables.reserve(entity_manager.entity_count() >> 2);
            engine::graphics::FrustumEvaluator evaluator(m_camera);
            const auto& mesh_db = graphics_module->mesh_db();
            gameplay::EntityManagerView<const gameplay::TransformComponent, const GraphicsComponent>
                view(entity_manager);
            view.for_each(
                [this, &mesh_db, &evaluator](const engine::gameplay::EntityId,
                                             const gameplay::TransformComponent& transform,
                                             const GraphicsComponent& graphics) {
                    if (is_component_visible(graphics, evaluator)) {
                        engine::graphics::component_to_renderable_shadow(
                            m_renderables, mesh_db, transform, m_camera, graphics);
                    }
                });
        }

        {
            MODUS_PROFILE_GRAPHICS_BLOCK("Sort Renderables");
            std::sort(m_renderables.begin(), m_renderables.end(),
                      [](const Renderable& r1, const Renderable& r2) {
                          const f32 d1 = r1.z_distance;
                          const f32 d2 = r2.z_distance;
                          return d1 < d2;
                      });
        }
        threed::Pass* pass = device.allocate_pass(params.pipeline, "ShadowMap");
        pass->viewport.width = kShadowMapWidth;
        pass->viewport.height = kShadowMapHeight;
        pass->frame_buffer = m_fbo[m_active_buffer];
        pass->color_targets = 0;
        pass->state.clear.clear_depth = true;
        for (const auto& renderable : m_renderables) {
            const glm::mat4 mvp = m_block.proj_view_matrix * renderable.world_transform.to_matrix();
            m_mvp_input = mvp;
            threed::Command* command = device.allocate_command(*pass);
            command->program = m_program;
            command->effect = m_effect;
            command->drawable = renderable.drawable;
            m_program_binders.bind(*command, device);
        }
    }
}

}    // namespace modus::engine::graphics
