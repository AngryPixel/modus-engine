﻿/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/debug_drawer.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <engine/graphics/ecs/components/camera_component.hpp>
#include <engine/graphics/font.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/buffer.hpp>
#include <threed/drawable.hpp>
#include <threed/effect.hpp>

namespace modus::engine::graphics {

enum class DebugDrawType : u8 { Cube = 0, Sphere, Line, Text2D, Text3D, Total };

struct DebugDrawer::SphereCommand {
    glm::vec3 center;
    f32 radius;
    glm::vec4 color;
    bool wireframe;
};

struct DebugDrawer::CubeCommand {
    glm::vec3 center;
    f32 radius;
    glm::vec4 color;
    bool wireframe;
};

struct DebugDrawer::TextCommand {
    glm::vec4 color;
    u32 range_start = 0;
    u32 range_count = 0;
};

struct DebugDrawer::Text3DCommand {
    glm::vec3 point;
    glm::vec3 color;
    f32 scale;
    String text;
};

DebugDrawer::DebugDrawer() {
    constexpr usize kInitialReserve = 16;
    constexpr u32 kLineBufferSize = ((sizeof(glm::vec3) * 2) + (sizeof(glm::vec4) * 2)) * 1024;
    constexpr u32 kTextBufferSize = (sizeof(glm::vec3) * 6) * 1024;
    m_tmp_lines_buffer.reserve(kLineBufferSize / sizeof(f32));
    m_tmp_text_buffer.reserve(kTextBufferSize / sizeof(f32));
    m_text_buffer_size = kTextBufferSize;
    m_line_buffer_size = kLineBufferSize;
    m_queued_cube_commands.reserve(kInitialReserve);
    m_queued_sphere_commands.reserve(kInitialReserve);
    m_queued_text_commands.reserve(kInitialReserve);
    m_queued_text3d_commands.reserve(kInitialReserve);
}

DebugDrawer::~DebugDrawer() {}

Result<> DebugDrawer::initialize(Engine& engine) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    const MeshDB& mesh_db = graphics_module->mesh_db();

    // Load cube mesh
    {
        assets::AsyncLoadParams params;
        params.path = "mesh:engine/basic_shapes/cube.mesh";
        params.callback = [this, &mesh_db](Engine&, const assets::AsyncCallbackResult& r) {
            if (r.asset) {
                auto mesh = assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(r.asset);
                auto r_mesh = mesh_db.get(mesh->m_mesh_handle);
                m_cube_drawable = (*r_mesh)->sub_meshes[0].drawable;
            } else {
                MODUS_LOGE("DebugDrawer: Failed to load mesh: {}", r.path);
            }
        };
        auto r_asset = engine.load_startup_asset(params);
        if (!r_asset) {
            MODUS_LOGE("DebugDrawer: Failed to load cube asset");
            return Error<>();
        }
        m_cube_asset_handle = (*r_asset);
    }

    // Load sphere mesh
    {
        assets::AsyncLoadParams params;
        params.path = "mesh:engine/basic_shapes/sphere.mesh";
        params.callback = [this, &mesh_db](Engine&, const assets::AsyncCallbackResult& r) {
            if (r.asset) {
                auto mesh = assets::unsafe_assetv2_cast<const engine::graphics::MeshAsset>(r.asset);
                auto r_mesh = mesh_db.get(mesh->m_mesh_handle);
                m_sphere_drawable = (*r_mesh)->sub_meshes[0].drawable;
            } else {
                MODUS_LOGE("DebugDrawer: Failed to load mesh: {}", r.path);
            }
        };
        auto r_asset = engine.load_startup_asset(params);
        if (!r_asset) {
            MODUS_LOGE("DebugDrawer: Failed to load cube asset");
            return Error<>();
        }
        m_sphere_asset_handle = *r_asset;
    }

    // Load font asset
    {
        assets::AsyncLoadParams params;
        params.path = "font:hack_12px.font";
        params.callback = [this](Engine&, const assets::AsyncCallbackResult& r) {
            if (r.asset) {
                m_font_asset = assets::unsafe_assetv2_cast<const FontAsset>(r.asset);
            }
        };

        auto r_asset = engine.load_startup_asset(params);
        if (!r_asset) {
            MODUS_LOGE("PassText: Failed to load font file");
            return Error<>();
        }
        m_font_asset_handle = *r_asset;
    }

    if (!m_programs.initialize(graphics_module)) {
        return Error<>();
    }

    if (!load_effects(engine)) {
        return Error<>();
    }

    if (!create_text_data(engine, m_text_buffer_size)) {
        return Error<>();
    }

    if (!create_line_data(engine, m_line_buffer_size)) {
        return Error<>();
    }

    m_prev_text_array_count = 0;
    return Ok<>();
}

Result<> DebugDrawer::shutdown(Engine& engine) {
    auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();
    (void)asset_loader.destroy_async(m_sphere_asset_handle);
    (void)asset_loader.destroy_async(m_cube_asset_handle);
    (void)asset_loader.destroy_async(m_font_asset_handle);
    (void)asset_loader.destroy_async(m_debug_program_handle);
    (void)asset_loader.destroy_async(m_debug_text_program_handle);
    m_font_asset.reset();
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    if (graphics_module) {
        m_programs.shutdown(graphics_module);
        auto& device = (graphics_module)->device();
        if (m_debug_draw_effect) {
            device.destroy_effect(m_debug_draw_effect);
        }
        if (m_text_draw_effect) {
            device.destroy_effect(m_text_draw_effect);
        }
        if (m_text_buffer) {
            device.destroy_buffer(m_text_buffer);
        }
        if (m_text_drawable) {
            device.destroy_drawable(m_text_drawable);
        }
        if (m_line_buffer) {
            device.destroy_buffer(m_line_buffer);
        }
        if (m_line_drawable) {
            device.destroy_drawable(m_line_drawable);
        }
    }
    return Ok<>();
}

Result<> DebugDrawer::upload_buffers(Engine& engine) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    // upload text buffer
    if (m_text_buffer_size < m_tmp_text_buffer.size() * sizeof(f32)) {
        device.destroy_buffer(m_text_buffer);
        device.destroy_drawable(m_text_drawable);
        u32 new_text_buffer_size = m_text_buffer_size;
        do {
            new_text_buffer_size *= 2;
        } while (new_text_buffer_size < m_tmp_text_buffer.size() * sizeof(f32));
        if (!create_text_data(engine, new_text_buffer_size)) {
            return Error<>();
        }
    }

    if (!m_tmp_text_buffer.empty() &&
        !device.update_buffer(m_text_buffer, make_slice(m_tmp_text_buffer).as_bytes(), 0, true)) {
        MODUS_LOGE("Failed to upload text buffer");
        return Error<>();
    }

    // upload line buffer
    if (m_line_buffer_size < m_tmp_lines_buffer.size() * sizeof(f32)) {
        device.destroy_buffer(m_line_buffer);
        device.destroy_drawable(m_line_drawable);
        u32 new_text_buffer_size = m_line_buffer_size;
        do {
            new_text_buffer_size *= 2;
        } while (new_text_buffer_size < m_tmp_lines_buffer.size() * sizeof(f32));
        if (!create_line_data(engine, new_text_buffer_size)) {
            return Error<>();
        }
    }
    if (!m_tmp_lines_buffer.empty() &&
        !device.update_buffer(m_line_buffer, make_slice(m_tmp_lines_buffer).as_bytes(), 0, true)) {
        MODUS_LOGE("Failed to upload line buffer");
        return Error<>();
    }
    return Ok<>();
}

void DebugDrawer::submit_passes(Engine& engine,
                                const DebugPipelineState& state,
                                threed::PipelinePtr pipeline) {
    MODUS_PROFILE_GRAPHICS_BLOCK("DebugDrawer::pipeline");

    Camera fallback_camera;
    FrustumEvaluator frustum_evaluator(state.camera ? *state.camera : fallback_camera);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    if (!state.camera) {
        return;
    }

    // handle text3d first, it appends to the tmp text buffer
    handle_text3d(state);

    // upload all buffers
    if (!upload_buffers(engine)) {
        return;
    }

    // generate commands
    threed::Pass* pass = device.allocate_pass(*pipeline, "DebugPass");
    pass->frame_buffer = state.framebuffer.handle;
    pass->viewport.width = state.framebuffer.width;
    pass->viewport.height = state.framebuffer.height;

    // Draw Lines
    handle_lines(engine, *state.camera, *pass);

    // draw text
    handle_text2d(engine, *pass, state);

    // draw spheres,
    handle_spheres(engine, *pass, *state.camera, frustum_evaluator);

    // draw cubes
    handle_cubes(engine, *pass, *state.camera, frustum_evaluator);

    m_queued_cube_commands.clear();
    m_queued_sphere_commands.clear();
    m_queued_text_commands.clear();
    m_queued_text3d_commands.clear();
    m_tmp_lines_buffer.clear();
    m_tmp_text_buffer.clear();
    m_prev_text_array_count = 0;
}

void DebugDrawer::draw_sphere(const glm::vec3& center,
                              const f32 radius,
                              const glm::vec4& color,
                              const bool wireframe) {
    SphereCommand cmd;
    cmd.center = center;
    cmd.radius = radius;
    cmd.color = color;
    cmd.wireframe = wireframe;
    m_queued_sphere_commands.push_back(cmd);
}

void DebugDrawer::draw_cube(const glm::vec3& center,
                            const f32 radius,
                            const glm::vec4& color,
                            const bool wireframe) {
    CubeCommand cmd;
    cmd.center = center;
    cmd.wireframe = wireframe;
    cmd.radius = radius;
    cmd.color = color;
    m_queued_cube_commands.push_back(cmd);
}

void DebugDrawer::draw_line(const glm::vec3& from,
                            const glm::vec3& to,
                            const glm::vec4& color_from,
                            const glm::vec4& color_to) {
    m_tmp_lines_buffer.push_back(from.x);
    m_tmp_lines_buffer.push_back(from.y);
    m_tmp_lines_buffer.push_back(from.z);
    m_tmp_lines_buffer.push_back(color_from.r);
    m_tmp_lines_buffer.push_back(color_from.g);
    m_tmp_lines_buffer.push_back(color_from.b);
    m_tmp_lines_buffer.push_back(color_from.a);
    m_tmp_lines_buffer.push_back(to.x);
    m_tmp_lines_buffer.push_back(to.y);
    m_tmp_lines_buffer.push_back(to.z);
    m_tmp_lines_buffer.push_back(color_to.r);
    m_tmp_lines_buffer.push_back(color_to.g);
    m_tmp_lines_buffer.push_back(color_to.b);
    m_tmp_lines_buffer.push_back(color_to.a);
}

void DebugDrawer::draw_text_2d(const StringSlice text,
                               const u32 x,
                               const u32 y,
                               const f32 scale,
                               const glm::vec3& color) {
    if (!m_font_asset) {
        return;
    }
    FontAsciiGenParams params;
    params.text = text;
    params.scale = scale;
    params.x_loc = f32(x);
    params.y_loc = f32(y);

    const u32 required_size = Font::buffer_size_for_text_size(params.text.size());
    const size_t current_buffer_size = m_tmp_text_buffer.size();
    m_tmp_text_buffer.resize(current_buffer_size + required_size);
    auto vec_slice =
        make_slice_mut(m_tmp_text_buffer).sub_slice(current_buffer_size, required_size);
    auto r_text_gen = m_font_asset->m_font.generate_ascii_string_v2(vec_slice, params);
    if (!r_text_gen) {
        MODUS_LOGE("DebugDrawer: Failed to generate text data for text: {}", params.text);
        return;
    }

    TextCommand cmd;
    cmd.color = glm::vec4(color, 1.f);
    cmd.range_start = m_prev_text_array_count;
    cmd.range_count = (*r_text_gen).array_count;
    m_prev_text_array_count += (*r_text_gen).array_count;
    m_queued_text_commands.push_back(cmd);
}

void DebugDrawer::draw_text_3d(const StringSlice text,
                               const glm::vec3& point,
                               const f32 scale,
                               const glm::vec3& color) {
    Text3DCommand cmd;
    cmd.point = point;
    cmd.scale = scale;
    cmd.color = glm::vec4(color, 1.0f);
    cmd.text = text.to_str();
    m_queued_text3d_commands.push_back(std::move(cmd));
}

void DebugDrawer::draw_fps(const Engine& engine,
                           const u32 x,
                           const u32 y,
                           const f32 scale,
                           const glm::vec3& color) {
    const String text = modus::format("FPS: {:04.2f}", engine.fps());
    draw_text_2d(text, x, y, scale, color);
}

void DebugDrawer::draw_axis(const f32 scale) {
    draw_line(glm::vec3(0.f), glm::vec3(scale, 0.f, 0.f), glm::vec4(1.0f, 0.f, 0.f, 1.0f));

    draw_line(glm::vec3(0.f), glm::vec3(0.f, scale, 0.f), glm::vec4(0.0f, 1.f, 0.f, 1.0f));

    draw_line(glm::vec3(0.f), glm::vec3(0.f, 0.f, scale), glm::vec4(0.0f, 0.f, 1.f, 1.0f));
}

Result<> DebugDrawer::load_effects(Engine& engine) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();

    {
        // Effect
        threed::EffectCreateParams effect_params;
        // Depth test disabled as the depth buffer may or may not be available
        effect_params.state.depth_stencil.depth.enabled = false;

        // disable culling
        effect_params.state.raster.cull_enabled = true;

        // Enable blending
        effect_params.state.blend.targets[0].enabled = true;
        effect_params.state.blend.targets[0].eq_source = threed::BlendEquation::SourceAlpha;
        effect_params.state.blend.targets[0].eq_destination =
            threed::BlendEquation::OneMinusSourceAlpha;

        auto effect_create_result = device.create_effect(effect_params);
        if (!effect_create_result) {
            MODUS_LOGE("DebugDrawer: Failed to create debug effect: {}",
                       effect_create_result.error());
            return Error<>();
        }
        m_debug_draw_effect = effect_create_result.release_value();
    }

    // Debug Draw Font Effect
    {
        // Effect
        threed::EffectCreateParams effect_params;
        effect_params.state.depth_stencil.depth.enabled = false;
        effect_params.state.blend.targets[0].enabled = true;
        effect_params.state.blend.targets[0].eq_source = threed::BlendEquation::SourceAlpha;
        effect_params.state.blend.targets[0].eq_destination =
            threed::BlendEquation::OneMinusSourceAlpha;

        effect_params.state.raster.cull_mode = threed::CullMode::Back;
        effect_params.state.raster.cull_enabled = true;
        effect_params.state.raster.face_counter_clockwise = true;

        auto effect_create_result = device.create_effect(effect_params);
        if (!effect_create_result) {
            MODUS_LOGE("DebugDrawer: Failed to create text2d effect: {}",
                       effect_create_result.error());
            return Error<>();
        }
        m_text_draw_effect = effect_create_result.release_value();
    }
    return Ok<>();
}

Result<> DebugDrawer::create_line_data(Engine& engine, const u32 buffer_size) {
    threed::Device& device = engine.module<ModuleGraphics>()->device();
    threed::BufferCreateParams params;
    params.data = nullptr;
    params.size = buffer_size;
    params.type = threed::BufferType::Data;
    params.usage = threed::BufferUsage::Stream;
    auto r_buffer = device.create_buffer(params);
    if (!r_buffer) {
        MODUS_LOGE("DebugDrawer: Failed to create line buffer\n");
    }
    m_line_buffer = *r_buffer;

    constexpr u32 stride = sizeof(f32) * 7;
    // create drawable
    threed::DrawableDataParams data_params;
    data_params.offset = 0;
    data_params.data_type = threed::DataType::Vec3F32;

    // Colour params
    threed::DrawableDataParams color_params;
    color_params.offset = sizeof(glm::vec3);
    color_params.data_type = threed::DataType::Vec4F32;

    threed::DrawableCreateParams drawable_params;
    drawable_params.data_buffers[0].buffer = *r_buffer;
    drawable_params.data_buffers[0].stride = stride;
    drawable_params.start = 0;
    drawable_params.count = buffer_size / 7;
    drawable_params.primitive = threed::Primitive::Lines;
    drawable_params.data[kVertexSlot] = data_params;
    drawable_params.data[kColourSlot] = color_params;

    auto r_drawable = device.create_drawable(drawable_params);
    if (!r_drawable) {
        MODUS_LOGE("DebugDrawer: Failed to create line drawable: {}", r_drawable.error());
        return Error<>();
    }
    m_line_drawable = *r_drawable;
    return Ok<>();
}

Result<> DebugDrawer::create_text_data(Engine& engine, const u32 buffer_size) {
    threed::Device& device = engine.module<ModuleGraphics>()->device();
    threed::BufferCreateParams params;
    params.data = nullptr;
    params.size = buffer_size;
    params.type = threed::BufferType::Data;
    params.usage = threed::BufferUsage::Stream;
    auto r_buffer = device.create_buffer(params);
    if (!r_buffer) {
        MODUS_LOGE("DebugDrawer: Failed to create text buffer\n");
    }
    m_text_buffer = *r_buffer;

    // Create Drawable
    threed::DrawableCreateParams drawable_params;
    drawable_params.data[kVertexSlot].offset = 0;
    drawable_params.data[kVertexSlot].data_type = threed::DataType::Vec2F32;

    drawable_params.data[kTextureSlot].offset = sizeof(glm::vec2);
    drawable_params.data[kTextureSlot].data_type = threed::DataType::Vec2F32;

    drawable_params.start = 0;
    drawable_params.count = buffer_size;
    drawable_params.primitive = threed::Primitive::Triangles;
    drawable_params.index_type = threed::IndicesType::None;
    drawable_params.data_buffers[0].buffer = *r_buffer;
    drawable_params.data_buffers[0].stride = sizeof(glm::vec4);

    auto r_drawable = device.create_drawable(drawable_params);
    if (!r_drawable) {
        MODUS_LOGE("DebugDrawer: Failed to create text drawable: {}", r_drawable.error());
        return Error<>();
    }
    m_text_drawable = *r_drawable;
    return Ok<>();
}

void DebugDrawer::handle_spheres(Engine& engine,
                                 threed::Pass& pass,
                                 const Camera& camera,
                                 const FrustumEvaluator& evaluator) {
    for (const auto& cmd : m_queued_sphere_commands) {
        math::bv::Sphere sphere;
        sphere.center = cmd.center;
        sphere.radius = cmd.radius;

        if (!evaluator.is_visible(sphere)) {
            return;
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();

        math::Transform model_transform = math::Transform::kIdentity;
        model_transform.set_translation(cmd.center);
        model_transform.set_scale(cmd.radius);

        const glm::mat4 mvp_matrix = camera.frustum().matrix() * camera.transform().to_matrix() *
                                     model_transform.to_matrix();

        m_programs.m_proj_input.m_value = mvp_matrix;
        m_programs.m_color_input.m_value = cmd.color;
        threed::Command* command = device.allocate_command(pass);
        command->effect = m_debug_draw_effect;
        command->program = m_programs.m_shape_program;
        if (cmd.wireframe) {
            command->draw.type = threed::DrawType::CustomPrimitive;
            command->draw.primitive = threed::Primitive::Lines;
        }
        command->drawable = m_sphere_drawable;
        m_programs.m_shape_binders.bind(*command, device);
    }
}

void DebugDrawer::handle_cubes(Engine& engine,
                               threed::Pass& pass,
                               const Camera& camera,
                               const FrustumEvaluator& evaluator) {
    for (const auto& cmd : m_queued_cube_commands) {
        math::bv::Sphere sphere;
        sphere.center = cmd.center;
        sphere.radius = cmd.radius;

        if (!evaluator.is_visible(sphere)) {
            return;
        }

        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();

        math::Transform model_transform = math::Transform::kIdentity;
        model_transform.set_translation(cmd.center);
        model_transform.set_scale(cmd.radius);

        const glm::mat4 mvp_matrix = camera.frustum().matrix() * camera.transform().to_matrix() *
                                     model_transform.to_matrix();
        m_programs.m_proj_input.m_value = mvp_matrix;
        m_programs.m_color_input.m_value = cmd.color;

        threed::Command* command = device.allocate_command(pass);
        command->effect = m_debug_draw_effect;
        command->program = m_programs.m_shape_program;
        if (cmd.wireframe) {
            command->draw.type = threed::DrawType::CustomPrimitive;
            command->draw.primitive = threed::Primitive::Lines;
        }
        command->drawable = m_cube_drawable;
        m_programs.m_shape_binders.bind(*command, device);
    }
}

void DebugDrawer::handle_lines(Engine& engine, const Camera& camera, threed::Pass& pass) {
    if (m_tmp_lines_buffer.size() != 0) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();

        const glm::mat4 mvp_matrix = camera.frustum().matrix() * camera.transform().to_matrix();
        threed::Command* command = device.allocate_command(pass);
        m_programs.m_proj_input.m_value = mvp_matrix;
        command->effect = m_debug_draw_effect;
        command->program = m_programs.m_line_program;
        command->drawable = m_line_drawable;
        command->draw.range_start = 0;
        command->draw.range_count = m_tmp_lines_buffer.size() / 7;
        command->draw.type = threed::DrawType::CustomRange;
        m_programs.m_line_binders.bind(*command, device);
    }
}

void DebugDrawer::handle_text2d(Engine& engine,
                                threed::Pass& pass,
                                const DebugPipelineState& state) {
    for (const auto& cmd : m_queued_text_commands) {
        auto graphics_module = engine.module<engine::ModuleGraphics>();
        auto& device = graphics_module->device();

        const glm::mat4 proj =
            glm::ortho(0.0f, f32(state.framebuffer.width), 0.0f, f32(state.framebuffer.height));

        m_programs.m_color_input.m_value = cmd.color;
        m_programs.m_proj_input.m_value = proj;
        m_programs.m_font_atlas_input.m_texture = m_font_asset->m_texture;

        threed::Command* command = device.allocate_command(pass);
        command->effect = m_text_draw_effect;
        command->program = m_programs.m_font_program;
        command->draw.type = threed::DrawType::CustomRange;
        command->draw.range_start = cmd.range_start;
        command->draw.range_count = cmd.range_count;
        command->drawable = m_text_drawable;
        m_programs.m_font_binders.bind(*command, device);
    }
}

void DebugDrawer::handle_text3d(const DebugPipelineState& state) {
    for (auto& cmd : m_queued_text3d_commands) {
        const glm::vec2 point2d = state.camera->point_to_screen_coord(
            cmd.point, state.framebuffer.width, state.framebuffer.height);
        draw_text_2d(cmd.text, u32(point2d.x), u32(point2d.y), cmd.scale, cmd.color);
    }
}

}    // namespace modus::engine::graphics
