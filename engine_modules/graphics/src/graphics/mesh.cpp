/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/graphics/mesh.hpp>

namespace modus::engine::graphics {

Result<MeshInstance, void> MeshDB::new_instance(const GID& id) const {
    auto handle_result = get_handle(id);
    if (!handle_result) {
        return Error<>();
    }
    return new_instance(handle_result.value());
}

Result<MeshInstance, void> MeshDB::new_instance(const MeshHandle handle) const {
    auto r_mesh = get(handle);
    if (!r_mesh) {
        return Error<>();
    }
    NotMyPtr<const Mesh> ptr = r_mesh.value();
    MeshInstance instance;
    instance.mesh = handle;
    instance.bounding_volume = ptr->bounding_sphere;
    return Ok(instance);
}

void MeshInstance::reset() {
    mesh = MeshHandle();
    bounding_volume.center = glm::vec3(0.f);
    bounding_volume.radius = 1.0f;
    materials.clear();
}

}    // namespace modus::engine::graphics
