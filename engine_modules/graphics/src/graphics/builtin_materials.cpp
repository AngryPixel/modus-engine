/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <engine/graphics/builtin_materials.hpp>
#include <engine/graphics/material.hpp>
#include <engine/engine.hpp>

namespace modus::engine::graphics {
class MaterialDB;

Result<> BuiltinMaterials::initialize(Engine& engine, MaterialDB& db) {
#define ADD_MATERIAL(m)                                            \
    if (!m.initialize(engine)) {                                   \
        MODUS_LOGE("Failed to initialize material: {}", m.name()); \
        return Error<>();                                          \
    }                                                              \
    if (!db.add(&m)) {                                             \
        MODUS_LOGE("Failed to add material: {}", m.name());        \
        return Error<>();                                          \
    }

    ADD_MATERIAL(m_material_default);
    ADD_MATERIAL(m_material_cubemap);

    if (!db.set_default_material_handle(m_material_default.material_handle())) {
        return Error<>();
    }

    return Ok<>();
#undef ADD_MATERIAL
}

Result<> BuiltinMaterials::shutdown(Engine& engine, MaterialDB& db) {
#define REM_MATERIAL(m)                                          \
    db.erase(&m);                                                \
    if (!m.shutdown(engine)) {                                   \
        MODUS_LOGE("Failed to shutdown material: {}", m.name()); \
    }

    REM_MATERIAL(m_material_cubemap);
    REM_MATERIAL(m_material_default);
    return Ok<>();
#undef REM_MATERIAL
}

}    // namespace modus::engine::graphics
