/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <engine/graphics/light.hpp>
#include <engine/graphics/camera.hpp>
namespace modus::engine::graphics {
/*
static inline f32 radius_for_point_light(const Light& light) {
    modus_assert(light.type() == LightType::Point);
    const auto& color = light.diffuse_color();
    const f32 max_channel = std::max(std::max(color.x, color.y), color.z);
    return (-light.attenuation_linear() + glm::sqrt(
                (light.attenuation_linear() * light.attenuation_linear())

                - (4.0f * light.attenuation_exp() *
                    (light.attenuation_constant() -
                                                  (255.0f * max_channel *
light.diffuse_intensity()))))) / (2.0f * light.attenuation_exp());
}*/

bool Light::evaluate_visibility(const FrustumEvaluator& evaluator) const {
    if (m_type == LightType::Directional || m_type == LightType::Ambient) {
        return true;
    } else {
        modus_assert_message(m_type == LightType::Point,
                             "Code needs upadting for new light types!");
        // build bv sphere
        // const f32 radius = radius_for_point_light(*this);
        const f32 radius = point_light_radius();
        math::bv::Sphere sphere;
        sphere.center = m_position;
        sphere.radius = radius;
        return evaluator.is_visible(sphere);
    }
}

math::bv::Sphere Light::bounding_sphere() const {
    math::bv::Sphere sphere;
    if (m_type == LightType::Directional) {
        sphere.center = glm::vec3(0);
        sphere.radius = std::numeric_limits<f32>::max();
    } else {
        modus_assert_message(m_type == LightType::Point,
                             "Code needs upadting for new light types!");
        // build bv sphere
        const f32 radius = point_light_radius();
        sphere.center = m_position;
        sphere.radius = radius;
    }
    return sphere;
}

}    // namespace modus::engine::graphics
