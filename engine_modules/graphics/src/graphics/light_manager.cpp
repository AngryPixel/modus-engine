/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
#include <pch.h>
// clang-format on
#include <engine/engine.hpp>
#include <engine/gameplay/gameworld.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/light.hpp>
#include <engine/graphics/light_manager.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/buffer.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/frame_buffer.hpp>
#include <threed/texture.hpp>

namespace modus::engine::graphics {

/*
f32 LightManager::point_light_radius(const PointLight& light) {
    const auto& color = light.diffuse_color;
    const f32 max_channel = std::max(std::max(color.x, color.y), color.z);
    return (-light.linear +
            +glm::sqrt((light.linear * light.linear) -
                       4 * light.exp *
                           (light.constant -
                            (256 * max_channel * light.diffuse_intensity)))) /
           (2 * light.exp);
}*/

Result<> LightManager::initialize(Engine& engine) {
    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();
    const auto device_limits = device.device_limits();
    if (sizeof(LightBlock) >= device_limits.min_constant_buffer_size) {
        MODUS_LOGE(
            "LightManager: LightBlock does not fit into device's "
            "minimum constant block size");
        return Error<>();
    }

    // create constant buffers
    // Points
    {
        threed::ConstantBufferCreateParams params;
        params.usage = threed::BufferUsage::Stream;
        params.size = sizeof(LightBlock);
        params.data = nullptr;

        auto r_buffer = device.create_constant_buffer(params);
        if (!r_buffer) {
            MODUS_LOGE(
                "LightManager: Failed to create point light constant "
                "buffer");
            return Error<>();
        }
        m_buffer = *r_buffer;
    }
    return Ok<>();
}

Result<> LightManager::shutdown(Engine& engine) {
    if (m_buffer) {
        auto graphics_module = engine.module<ModuleGraphics>();
        graphics_module->device().destroy_constant_buffer(m_buffer);
    }
    return Ok<>();
}

Result<> LightManager::update(Engine& engine, const Slice<Light>& lights) {
    m_block.num_point_lights = 0;
    m_block.num_dir_lights = 0;
    m_block.ambient = glm::vec4(0.f);
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("LightManager::Update");
        // Update Data
        for (const auto& current_light : lights) {
            const LightType type = current_light.type();
            if (type == LightType::Ambient) {
                m_block.ambient +=
                    glm::vec4(current_light.diffuse_color(), current_light.diffuse_intensity());
            } else if (type == LightType::Directional) {
                if (m_block.num_dir_lights >= kMaxDirectionalLights) {
                    MODUS_LOGE(
                        "LightManager: Maximum number directional "
                        "lights reached, ignoring");
                    continue;
                }
                DirectionalLight& light = m_block.dir_lights[m_block.num_dir_lights];
                light.direction = glm::normalize(current_light.position());
                light.diffuse_color = current_light.diffuse_color();
                light.diffuse_intensity = current_light.diffuse_intensity();
                m_block.num_dir_lights++;
            } else if (type == LightType::Point) {
                if (m_block.num_point_lights >= kMaxPointLights) {
                    MODUS_LOGE(
                        "LightManager: Maximum number of point lights "
                        "reached, ignoring");
                    continue;
                }
                PointLight& light = m_block.point_lights[m_block.num_point_lights];
                light.position = current_light.position();
                light.diffuse_color = current_light.diffuse_color();
                light.diffuse_intensity = current_light.diffuse_intensity();
                light.radius = current_light.point_light_radius();
                /*
                light.constant = current_light.attenuation_constant();
                light.exp = current_light.attenuation_exp();
                light.linear = current_light.attenuation_linear();*/
                m_block.num_point_lights++;
            } else {
                modus_assert(false);
                MODUS_LOGE("Forward Light Manager: Unknown light type detected:{}", type);
            }
        }
    }

    auto graphics_module = engine.module<ModuleGraphics>();
    {
        MODUS_PROFILE_GRAPHICS_BLOCK("LightManager::Update constants");

        if (auto r_update = graphics_module->device().update_constant_buffer(m_buffer, m_block);
            !r_update) {
            MODUS_LOGE("Light Manager: Failed to update light constant buffer");
            return Error<>();
        }
    }
    return Ok<>();
}

}    // namespace modus::engine::graphics
