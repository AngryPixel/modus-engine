/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/graphics/material_generated.h>
#include <pch.h>

#include <core/io/memory_stream.hpp>
#include <engine/assets/asset_waiter.hpp>
#include <engine/engine.hpp>
#include <engine/graphics/image_asset.hpp>
#include <engine/graphics/material_asset.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/device.hpp>
#include <threed/texture.hpp>

MODUS_ENGINE_ASSET_TYPE_IMPL(modus::engine::graphics::MaterialAsset)

namespace modus::engine::graphics {

MaterialAsset::MaterialAsset(const MaterialInstanceRef ref,
                             Vector<assets::AssetHandleV2>&& image_assets)
    : assets::AssetBase(assets::AssetTraits<MaterialAsset>::type_id()),
      m_material_ref(ref),
      m_image_assets(std::move(image_assets)) {}

MaterialAssetFactory::MaterialAssetFactory(MaterialDB& db) : factory_type(32), m_db(db) {}

StringSlice MaterialAssetFactory::file_extension() const {
    return "rmaterial";
}

class MaterialAssetData final : public assets::RamAssetData {
   public:
    const fbs::Material* m_material;
    assets::AssetGroupWaiter m_asset_waiter;
    GID m_guid;

    Result<assets::AssetLoadResult> load(ISeekableReader& reader) override {
        MODUS_PROFILE_GRAPHICS_BLOCK("MaterialAssetLoad");
        if (!assets::RamAssetData::load(reader)) {
            return Error<>();
        }
        const ByteSlice bytes = this->bytes();
        flatbuffers::Verifier v(bytes.data(), bytes.size());
        if (!fbs::VerifyMaterialBuffer(v)) {
            MODUS_LOGE("MaterialAsset: File is not a graphics material file");
            return Error<>();
        }

        const fbs::Material* fbs_material = fbs::GetMaterial(bytes.data());
        m_material = fbs_material;

        auto r_instance_guid = GID::from_string(fbs_material->instance_guid()->c_str());
        if (!r_instance_guid) {
            MODUS_LOGE("MaterialAsset: Material has invalid instance guid");
            return Error<>();
        }
        m_guid = *r_instance_guid;

        // check material for images that need to be quequed.
        auto fbs_properties = fbs_material->properties();
        if (fbs_properties != nullptr) {
            for (const auto& fbs_property : *fbs_properties) {
                if (fbs_property == nullptr) {
                    continue;
                }
                if (fbs_property->value_type() == fbs::PropertyValue::image) {
                    const auto fbs_image_prop = fbs_property->value_as_image();
                    modus_assert(fbs_image_prop != nullptr);
                    m_asset_waiter.add(StringSlice(fbs_image_prop->v()->c_str()));
                }
            }
        }

        if (m_asset_waiter.count() == 0) {
            return Ok(assets::AssetLoadResult::Done);
        }
        return Ok(assets::AssetLoadResult::HasDependencies);
    }

    Result<bool> eval_dependencies(Engine& engine, assets::AssetLoader& loader) override {
        const assets::AssetGroupWaiterState state = m_asset_waiter.update(engine, loader);
        if (state.error_count != 0) {
            MODUS_LOGE("Material: One of the images failed to load");
            return Error<>();
        }
        return Ok<bool>(state.finished_count == m_asset_waiter.count());
    }
};

std::unique_ptr<assets::AssetData> MaterialAssetFactory::create_asset_data() {
    return modus::make_unique<MaterialAssetData>();
}

Result<NotMyPtr<assets::AssetBase>> MaterialAssetFactory::create(Engine& engine,
                                                                 assets::AssetData& data) {
    MODUS_PROFILE_GRAPHICS_BLOCK("MaterialAssetCreate");
    const MaterialAssetData& material_data = static_cast<const MaterialAssetData&>(data);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& material_db = graphics_module->material_db();
    auto asset_module = engine.module<engine::ModuleAssets>();

    auto r_instance_ref = material_db.create_instance(material_data.m_guid);
    if (!r_instance_ref) {
        MODUS_LOGE("MateirialAsset: Failed to create instance of material {}",
                   material_data.m_guid);
        return Error<>();
    }

    // Process Properties
    auto r_instance = material_db.instance(*r_instance_ref);
    if (!r_instance) {
        MODUS_LOGE("MaterialAsset: Could not locate material instance");
        material_db.destroy_instance(*r_instance_ref);
        return Error<>();
    }

    Vector<assets::AssetHandleV2> image_assets;
    image_assets.reserve(4);

    // Load Textures lambda
    auto process_images = [&asset_loader = asset_module->loader()](
                              const StringSlice path) -> Result<NotMyPtr<const ImageAsset>, void> {
        auto r_asset = asset_loader.resolve_by_path_typed<ImageAsset>(path);
        if (!r_asset) {
            MODUS_LOGE("MaterialAsset: Image {} has not been loaded", path);
            return Error<>();
        }
        return r_asset;
    };

    const fbs::Material* fbs_material = material_data.m_material;
    auto fbs_properties = fbs_material->properties();
    if (fbs_properties != nullptr) {
        for (const auto& fbs_property : *fbs_properties) {
            if (fbs_property == nullptr) {
                continue;
            }
            const StringSlice property_name = fbs_property->name()->c_str();
            PropertyValue property_value;
            StringSlice property_type;
            switch (fbs_property->value_type()) {
                case fbs::PropertyValue::bool_: {
                    const auto fbs_prop = fbs_property->value_as_bool_();
                    modus_assert(fbs_prop != nullptr);
                    property_value = fbs_prop->v();
                    property_type = "bool";
                } break;
                case fbs::PropertyValue::u32: {
                    const auto fbs_prop = fbs_property->value_as_u32();
                    modus_assert(fbs_prop != nullptr);
                    property_value = fbs_prop->v();
                    property_type = "u32";
                } break;
                case fbs::PropertyValue::f32: {
                    const auto fbs_prop = fbs_property->value_as_f32();
                    modus_assert(fbs_prop != nullptr);
                    property_value = fbs_prop->v();
                    property_type = "f32";
                } break;
                case fbs::PropertyValue::vec2: {
                    const auto fbs_prop = fbs_property->value_as_vec2();
                    modus_assert(fbs_prop != nullptr);
                    const math::fbs::Vec2* vec = fbs_prop->v();
                    modus_assert(fbs_prop != nullptr);
                    property_value = glm::vec2(vec->x(), vec->y());
                    property_type = "vec2";
                } break;
                case fbs::PropertyValue::vec3: {
                    const auto fbs_prop = fbs_property->value_as_vec3();
                    modus_assert(fbs_prop != nullptr);
                    const math::fbs::Vec3* vec = fbs_prop->v();
                    modus_assert(fbs_prop != nullptr);
                    property_value = glm::vec3(vec->x(), vec->y(), vec->z());
                    property_type = "vec3";
                } break;
                case fbs::PropertyValue::vec4: {
                    const auto fbs_prop = fbs_property->value_as_vec4();
                    modus_assert(fbs_prop != nullptr);
                    const math::fbs::Vec4* vec = fbs_prop->v();
                    modus_assert(fbs_prop != nullptr);
                    property_value = glm::vec4(vec->x(), vec->y(), vec->z(), vec->w());
                    property_type = "vec4";
                } break;
                case fbs::PropertyValue::image: {
                    property_type = "image";
                    const auto fbs_image_prop = fbs_property->value_as_image();
                    modus_assert(fbs_image_prop != nullptr);
                    if (auto r_image = process_images(fbs_image_prop->v()->c_str()); r_image) {
                        image_assets.push_back((*r_image)->asset_handle());
                        property_value = (*r_image)->m_texture;
                    } else {
                        MODUS_LOGE("MaterialAsset: Failed to load image for property {}",
                                   property_name);
                        continue;
                    }
                } break;
                default:
                    MODUS_LOGE("MaterialAsset: Unknown property type for '{}'", property_name);
                    continue;
            }
            if (!(*r_instance)->set_property(property_name, property_value)) {
                MODUS_LOGE(
                    "MateirialAsset: Failed to set property value of type {} "
                    "for {}",
                    property_type, property_name);
            }
        }
    }

    return Ok<NotMyPtr<assets::AssetBase>>(
        m_asset_pool.construct(*r_instance_ref, std::move(image_assets)));
}

Result<> MaterialAssetFactory::destroy(Engine& engine, NotMyPtr<assets::AssetBase> asset) {
    MODUS_PROFILE_GRAPHICS_BLOCK("MaterialAssetDestroy");
    auto r_asset = assets::assetv2_cast<MaterialAsset>(asset);
    if (!r_asset) {
        return Error<>();
    }
    // Unload related image assets
    auto& asset_loader = engine.module<engine::ModuleAssets>()->loader();
    for (auto& asset_handle : (*r_asset)->m_image_assets) {
        asset_loader.destroy_async(asset_handle);
    }
    m_db.destroy_instance((*r_asset)->m_material_ref);
    m_asset_pool.destroy(r_asset->get());
    return Ok<>();
}

}    // namespace modus::engine::graphics
