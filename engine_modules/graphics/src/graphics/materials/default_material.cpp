/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/materials/default_material.hpp>
// clang-format on

#include <engine/engine.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/pipeline.hpp>
#include <threed/buffer.hpp>
#include <threed/sampler.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <threed/program_gen/shader_generator.hpp>
#include <engine/graphics/pipeline/default_pipeline.hpp>
#include <engine/graphics/default_program_inputs.hpp>

namespace modus::engine::graphics {

DefaultMaterialInstance::DefaultMaterialInstance(const MaterialHandle h, const u32 flags)
    : MaterialInstance(h, flags),
      m_prop_diffuse(kPropNameDiffuse, glm::vec4(0.6f, 0.6f, 0.6f, 1.0f)),
      m_prop_diffuse_map(kPropNameDiffuseMap),
      m_prop_normal_map(kPropNameNormalMap),
      m_prop_specular_map(kPropNameSpecularMap),
      m_prop_emissive(kPropNameEmissive, glm::vec3(0.f)),
      m_prop_specular(kPropNameSpecular, 0.f),
      m_prop_diffuse_mix(kPropNameDiffuseMix, glm::vec3(1.0)) {}

//#define MODUS_DIFFUSE_MAP_BIT (1<< 0)
//#define MODUS_SPECULAR_MAP_BIT (1<< 1)
//#define MODUS_NORMAL_MAP_BIT (1<< 2)
//#define MODUS_EMISSIVE_MAP_BIT (1<< 3)
static constexpr u32 kMatFlagDiffuseMap = 1 << 0;
static constexpr u32 kMatFlagNormalMap = 1 << 2;
static constexpr u32 kMatFlagSpecularMap = 1 << 1;
void DefaultMaterialInstance::apply(Engine& engine, MaterialApplyParams& params) const {
    MODUS_UNUSED(engine);
    modus_assert(params.base_material.material_handle() == material_handle());
    const DefaultMaterial& material_type =
        static_cast<const DefaultMaterial&>(params.base_material);
    modus_assert(material_type.effect_handle());

    const glm::mat4 mvp =
        params.frame_globals.constant_block().mat_project_view * params.renderable_transform;

    i32 material_flags = 0;
    if (m_prop_diffuse_map.value()) {
        material_flags |= kMatFlagDiffuseMap;
    }

    if (m_prop_normal_map.value()) {
        material_flags |= kMatFlagNormalMap;
    }

    if (m_prop_specular_map.value()) {
        material_flags |= kMatFlagSpecularMap;
    }

    auto graphics_module = engine.module<ModuleGraphics>();
    auto& device = graphics_module->device();

    material_type.m_model_matrix_binder.m_value = params.renderable_transform;
    material_type.m_mvp_binder.m_value = mvp;
    material_type.m_flags_binder.m_value = material_flags;
    material_type.m_diffuse_binder.m_value = m_prop_diffuse.value();
    material_type.m_specular_binder.m_value = m_prop_specular.value();
    material_type.m_emissive_binder.m_value = m_prop_emissive.value();
    material_type.m_diffuse_mix_binder.m_value = m_prop_diffuse_mix.value();
    material_type.m_diffuse_map_binder.m_texture = m_prop_diffuse_map.value();
    material_type.m_normal_map_binder.m_texture = m_prop_normal_map.value();
    material_type.m_specular_map_binder.m_texture = m_prop_specular_map.value();

    material_type.m_binders.bind(params.command, device);

    params.command.program = material_type.forward_program_handle();
    params.command.effect = material_type.effect_handle();
}

void DefaultMaterialInstance::apply_instanced(Engine& engine,
                                              MaterialInstancedApplyParams& params) const {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(params);
    MODUS_UNUSED(m_last_instance_buffer_size);
    /*m_instance_blocks.resize(params.renderables.size());
    usize index = 0;
    for (const auto& renderable : params.renderables) {
        m_instance_blocks[index].model = renderable.world_transform.to_matrix();
        m_instance_blocks[index].normal =
            (glm::transpose(glm::inverse(glm::mat3(renderable.world_transform.to_matrix()))));
        ++index;
    }

    auto pipeline = pipeline_cast<ForwardPipeline>(params.pipeline);
    auto graphics_module = engine.module<ModuleGraphics>(engine.modules().graphics());
    auto& device = graphics_module->device();
    const auto& light_manager = pipeline->light_manager();
    const auto& shadow_manager = pipeline->shadow_manager();

    const u32 buffer_size = sizeof(Block) * m_instance_blocks.size();
    if (m_last_instance_buffer_size < buffer_size) {
        if (m_instance_attribs) {
            device.destroy_buffer(m_instance_attribs);
        }
        threed::BufferCreateParams buffer_params;
        buffer_params.size = buffer_size;
        buffer_params.data = m_instance_blocks.data();
        buffer_params.type = threed::BufferType::Data;
        buffer_params.usage = threed::BufferUsage::Stream;
        buffer_params.buffering_count = 2;

        auto r_buffer = device.create_buffer(buffer_params);
        if (!r_buffer) {
            MODUS_LOGE("Failed to create buffer block!");
            return;
        }
        m_instance_attribs = *r_buffer;
        m_last_instance_buffer_size = buffer_size;
    } else {
        if (!device.update_buffer(m_instance_attribs, make_slice(m_instance_blocks).as_bytes(), 0,
                                  true)) {
            MODUS_LOGE("Default Material: Failed to update instance attribs buffer");
        }
    }

    modus_assert(params.base_material.material_handle() == material_handle());
    const DefaultMaterial& material_type =
        static_cast<const DefaultMaterial&>(params.base_material);
    modus_assert(material_type.effect_handle());

    i32 material_flags = 0;
    if (m_prop_diffuse_map.value()) {
        material_flags |= kMatFlagDiffuseMap;
    }

    if (m_prop_normal_map.value()) {
        material_flags |= kMatFlagNormalMap;
    }

    if (m_prop_specular_map.value()) {
        material_flags |= kMatFlagSpecularMap;
    }

    const auto material_flags_constant = device.upload_constant(material_flags);
    const auto diffuse_constant = device.upload_constant(m_prop_diffuse.value());
    const auto specular_constant = device.upload_constant(m_prop_specular.value());
    const auto emissive_constant = device.upload_constant(m_prop_emissive.value());
    const auto diffuse_mix_constant = device.upload_constant(m_prop_diffuse_mix.value());

    params.command.program = material_type.instanced_program_handle();
    params.command.effect = material_type.effect_handle();

    using namespace gpuprogreflect;

    material::default_instanced::set_constant_u_material_flags(params.command,
                                                               material_flags_constant);

    material::default_instanced::set_constant_u_diffuse(params.command, diffuse_constant);
    material::default_instanced::set_constant_u_emissive(params.command, emissive_constant);

    material::default_instanced::set_constant_u_diffuse_mix(params.command, diffuse_mix_constant);

    material::default_instanced::set_constant_FrameGlobals(params.command,
                                                           params.frame_globals.constant_buffer());

    material::default_instanced::set_sampler_u_diffuse_map(params.command,
                                                           m_prop_diffuse_map.value());
    material::default_instanced::set_sampler_u_normal_map(params.command,
                                                          m_prop_normal_map.value());
    material::default_instanced::set_sampler_u_specular_map(params.command,
                                                            m_prop_specular_map.value());
    material::default_instanced::set_constant_u_specular_factor(params.command, specular_constant);
    material::default_instanced::set_sampler_u_shadow_map(params.command,
                                                          shadow_manager.shadow_map());

    material::default_instanced::set_constant_FrameGlobals(params.command,
                                                           params.frame_globals.constant_buffer());

    material::default_instanced::set_constant_ShadowBlock(params.command,
                                                          shadow_manager.constant_buffer());
    material::default_instanced::set_constant_Lights(params.command,
                                                     light_manager.constant_buffer());

    material::default_instanced::set_sampler_u_light_grid(params.command,
                                                          light_manager.light_grid_texture());
    material::default_instanced::set_sampler_u_light_indices(params.command,
                                                             light_manager.light_indices_texture());
    // setup instance attributes
    params.command.inputs.intance_attributes[0].buffer = m_instance_attribs;
    params.command.inputs.intance_attributes[0].slot = 6;
    params.command.inputs.intance_attributes[0].offset = 0;
    params.command.inputs.intance_attributes[0].stride = sizeof(Block);
    params.command.inputs.intance_attributes[0].data_type = threed::DataType::Mat4F32;

    params.command.inputs.intance_attributes[2].buffer = m_instance_attribs;
    params.command.inputs.intance_attributes[2].slot = 12;
    params.command.inputs.intance_attributes[2].offset = sizeof(glm::mat4);
    params.command.inputs.intance_attributes[2].stride = sizeof(Block);
    params.command.inputs.intance_attributes[2].data_type = threed::DataType::Mat3F32;

    params.command.draw.type = threed::DrawType::Default;
    params.command.draw.instance_count = params.renderables.size();*/
}

Result<NotMyPtr<MaterialProperty>, void> DefaultMaterialInstance::find_property(
    const StringSlice name) {
    if (name == kPropNameDiffuse) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_diffuse);
    } else if (name == kPropNameNormalMap) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_normal_map);
    } else if (name == kPropNameDiffuseMap) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_diffuse_map);
    } else if (name == kPropNameSpecularMap) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_specular_map);
    } else if (name == kPropNameSpecular) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_specular);
    } else if (name == kPropNameEmissive) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_emissive);
    } else if (name == kPropNameDiffuseMix) {
        return Ok<NotMyPtr<MaterialProperty>>(&m_prop_diffuse_mix);
    }
    return Error<>();
}

Result<NotMyPtr<const MaterialProperty>, void> DefaultMaterialInstance::find_property(
    const StringSlice name) const {
    if (name == kPropNameDiffuse) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_diffuse);
    } else if (name == kPropNameNormalMap) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_normal_map);
    } else if (name == kPropNameDiffuseMap) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_diffuse_map);
    } else if (name == kPropNameSpecularMap) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_specular_map);
    } else if (name == kPropNameSpecular) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_specular);
    } else if (name == kPropNameEmissive) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_emissive);
    } else if (name == kPropNameDiffuseMix) {
        return Ok<NotMyPtr<const MaterialProperty>>(&m_prop_diffuse_mix);
    }
    return Error<>();
}

static constexpr u32 kChunksPerBlock = 64;
DefaultMaterial::DefaultMaterial()
    : MaterialHelper<DefaultMaterialInstance>(kGUID, "Default", kChunksPerBlock) {
    m_flags |= kMaterialFlagSupportsInstancing;
}

template <typename T>
using TDCI = threed::dynamic::TypedDefaultConstantInput<T>;

Result<> DefaultMaterial::initialize(Engine& engine) {
    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    auto& shader_snippet_db = graphics_module->shader_snippet_db();
    const auto& shader_generator = device.shader_generator();
    // Vertex input

    {
        threed::SamplerCreateParams sampler_params;
        sampler_params.filter_min = threed::SamplerFilter::LinearMipMapLinear;
        sampler_params.filter_mag = threed::SamplerFilter::Linear;
        if (auto r = device.create_sampler(sampler_params); !r) {
            MODUS_LOGE("Failed to create default material sampler:{}", r.error());
            return Error<>();
        } else {
            m_sampler = *r;
            m_diffuse_map_binder.m_sampler = *r;
            m_normal_map_binder.m_sampler = *r;
            m_specular_map_binder.m_sampler = *r;
        }
    }

    threed::dynamic::DefaultSamplerInput diffuse_map_input("s_diffuse_map", &m_diffuse_map_binder);
    threed::dynamic::DefaultSamplerInput normal_map_input("s_normal_map", &m_normal_map_binder);
    threed::dynamic::DefaultSamplerInput specular_map_input("s_specular_map",
                                                            &m_specular_map_binder);
    TDCI<glm::mat4> mvp_input("u_mvp", &m_mvp_binder);
    TDCI<glm::mat4> model_matrix_input("u_model_matrix", &m_model_matrix_binder);
    TDCI<glm::vec4> diffuse_input("u_diffuse", &m_diffuse_binder);
    TDCI<glm::vec3> emissive_input("u_emissive", &m_emissive_binder);
    TDCI<glm::vec3> diffuse_mixer_input("u_diffuse_mix", &m_diffuse_mix_binder);
    TDCI<f32> specular_input("u_specular_factor", &m_specular_binder);
    TDCI<i32> flags_input("u_material_flags", &m_flags_binder);

    threed::dynamic::ProgramGenerator generator;

    // Program

    threed::dynamic::ProgramGenParams params;

    if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                 "engine.frame_globals")) {
        MODUS_LOGE("Default Material: Failed to locate frame_globals shader snippet");
        return Error<>();
    }

    if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                 "engine.tiled_light")) {
        MODUS_LOGE("Default Material: Failed to locate tiled_light shader snippet");
        return Error<>();
    }

    if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Fragment,
                                 "engine.shadows")) {
        MODUS_LOGE("Default Material: Failed to locate shadows shader snippet");
        return Error<>();
    }

    params.varying
        .push_back({"v_position", threed::DataType::Vec3F32,
                    threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Default})
        .expect();
    params.varying
        .push_back({"v_normal", threed::DataType::Vec3F32,
                    threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Default})
        .expect();
    params.varying
        .push_back({"v_uv", threed::DataType::Vec2F32, threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Default})
        .expect();
    params.varying
        .push_back({"v_tbn", threed::DataType::Mat3F32, threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Default})
        .expect();

    params.skip_instanced_variant = true;
    params.vertex_stage.constants.push_back(&mvp_input).expect();
    params.vertex_stage.constants.push_back(&model_matrix_input).expect();
    params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
    params.vertex_stage.inputs.push_back(shader_desc::default_normal_input()).expect();
    params.vertex_stage.inputs.push_back(shader_desc::default_uv_input()).expect();
    params.vertex_stage.inputs.push_back(shader_desc::default_tangent_input()).expect();
    params.vertex_stage.main = R"R(
mat3 generate_tbn_matrix2(in vec3 normal, in vec3 tangent, in vec3 binormal) {
    return (mat3(tangent,binormal, normal));
}
void main() {
     v_normal = NORMAL;
     v_position = vec3(u_model_matrix * VERTEX);
     v_uv = UV;
     vec3 binormal = cross(NORMAL, TANGENT);
     v_tbn = generate_tbn_matrix2(NORMAL, TANGENT, binormal);
     gl_Position = u_mvp * VERTEX;
}
)R";

    params.fragment_stage.fragment_outputs.push_back(shader_desc::default_frag_output()).expect();
    params.fragment_stage.constants.push_back(&diffuse_input).expect();
    params.fragment_stage.constants.push_back(&diffuse_mixer_input).expect();
    params.fragment_stage.constants.push_back(&emissive_input).expect();
    params.fragment_stage.constants.push_back(&specular_input).expect();
    params.fragment_stage.constants.push_back(&flags_input).expect();
    params.fragment_stage.samplers.push_back(&diffuse_map_input).expect();
    params.fragment_stage.samplers.push_back(&normal_map_input).expect();
    params.fragment_stage.samplers.push_back(&specular_map_input).expect();
    params.fragment_stage.main = R"R(
#define MODUS_DIFFUSE_MAP_BIT (1<< 0)
#define MODUS_SPECULAR_MAP_BIT (1<< 1)
#define MODUS_NORMAL_MAP_BIT (1<< 2)
#define MODUS_EMISSIVE_MAP_BIT (1<<
void main() {
    // Get Normal
    vec3 normal;
    if ((u_material_flags & MODUS_NORMAL_MAP_BIT) != 0) {
        normal = texture(s_normal_map, v_uv).rgb;
        normal = ((normal * 2.0) - vec3(1.0));
        normal = normalize(v_tbn * normal);
    } else {
        normal = normalize(v_normal);
    }
    // Specular
    float specular;
    if ((u_material_flags & MODUS_SPECULAR_MAP_BIT) != 0) {
        specular = texture(s_specular_map, v_uv).r;
    } else {
        specular = u_specular_factor;
    }
    // diffuse
    vec4 diffuse = u_diffuse;
    if ((u_material_flags & MODUS_DIFFUSE_MAP_BIT) != 0) {
        vec4 diffuse_map = texture(s_diffuse_map, v_uv);
        diffuse = vec4(mix(diffuse.xyz, diffuse_map.xyz, u_diffuse_mix), 1.0);
    }
#if defined(ENGINE_SHADOW_MANAGER_ENABLED)
    float shadow_factor = calculate_shadows_world_pos(v_position, normal);
#else
    float shadow_factor = 0.0;
#endif
    OUT_COLOR = diffuse *
            calculate_lights_tiled(v_position, normal, specular, u_emissive, shadow_factor);
}
)R";

    if (auto r = generator.generate(params, shader_generator); !r) {
        MODUS_LOGE("Failed to generate default material program: {}", r.error());
        return Error<>();
    }

    auto result = generator.result(0).value_or_panic();
    m_binders = result->binders;

    if (auto r = device.create_program(result->create_params); !r) {
        MODUS_LOGE("Failed to create default material program: {}", r.error());
        return Error<>();
    } else {
        m_prog_forward_handle = *r;
    }
    generator.reset();

    // Constant input
    threed::ProgramConstantInputParam const_input;
    FrameGlobals::fill_constant_input(const_input);

    // Effect
    threed::EffectCreateParams effect_params;
    effect_params.state.depth_stencil.depth.enabled = true;

    effect_params.state.raster.cull_mode = threed::CullMode::Back;
    effect_params.state.raster.cull_enabled = true;
    effect_params.state.raster.face_counter_clockwise = true;

    auto effect_create_result = device.create_effect(effect_params);
    if (!effect_create_result) {
        MODUS_LOGE("DefaultMaterial: Failed to create effect: {}", effect_create_result.error());
        return Error<>();
    }
    m_effect_handle = effect_create_result.release_value();
    return Ok<>();
}

Result<> DefaultMaterial::shutdown(Engine& engine) {
    MODUS_UNUSED(engine);

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    if (graphics_module) {
        auto& device = graphics_module->device();
        if (m_prog_forward_handle) {
            device.destroy_program(m_prog_forward_handle);
        }
        if (m_prog_instanced_handle) {
            device.destroy_program(m_prog_instanced_handle);
        }
        if (m_effect_handle.is_valid()) {
            device.destroy_effect(m_effect_handle);
            m_effect_handle = threed::EffectHandle();
        }
        if (m_sampler) {
            device.destroy_sampler(m_sampler);
        }
    }
    return Ok<>();
}

}    // namespace modus::engine::graphics
