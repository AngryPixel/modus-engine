/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <engine/graphics/materials/cubemap_material.hpp>
// clang-format on

#include <engine/engine.hpp>
#include <threed/device.hpp>
#include <threed/effect.hpp>
#include <threed/pipeline.hpp>
#include <engine/modules/module_assets.hpp>
#include <engine/modules/module_graphics.hpp>
#include <engine/graphics/default_program_inputs.hpp>
#include <threed/program_gen/shader_generator.hpp>
#include <threed/sampler.hpp>

namespace modus::engine::graphics {

CubeMapMaterialInstance::CubeMapMaterialInstance(const MaterialHandle h, const u32 flags)
    : MaterialInstance(h, flags), m_texture(kPropertyNameTexture) {}

void CubeMapMaterialInstance::apply(Engine& engine, MaterialApplyParams& params) const {
    MODUS_UNUSED(engine);
    modus_assert(params.base_material.material_handle() == material_handle());
    modus_assert(params.base_material.effect_handle());

    const CubeMapMaterial& material_type =
        static_cast<const CubeMapMaterial&>(params.base_material);
    const glm::mat4 view = glm::mat(glm::mat3(params.frame_globals.constant_block().mat_view));

    auto graphics_module = engine.module<ModuleGraphics>();
    material_type.m_cubemap_binder = m_texture.value();
    material_type.m_matrix_binder = view;
    params.command.effect = material_type.effect_handle();
    params.command.program = material_type.forward_program_handle();
    material_type.m_binders.bind(params.command, graphics_module->device());
}

Result<NotMyPtr<MaterialProperty>, void> CubeMapMaterialInstance::find_property(
    const StringSlice name) {
    if (name != kPropertyNameTexture) {
        return Error<>();
    }
    return Ok<NotMyPtr<MaterialProperty>>(&m_texture);
}

Result<NotMyPtr<const MaterialProperty>, void> CubeMapMaterialInstance::find_property(
    const StringSlice name) const {
    if (name != kPropertyNameTexture) {
        return Error<>();
    }
    return Ok<NotMyPtr<const MaterialProperty>>(&m_texture);
}

static constexpr u32 kChunksPerBlock = 8;

CubeMapMaterial::CubeMapMaterial()
    : MaterialHelper<CubeMapMaterialInstance>("58aaf618-0919-4a62-a8c8-8e06c59ef335",
                                              "CubeMap",
                                              kChunksPerBlock) {
    m_flags |= KMaterialFlagSkyBox | kMaterialFlagIgnoreViewTransform;
}

Result<> CubeMapMaterial::initialize(Engine& engine) {
    MODUS_UNUSED(engine);

    // Load Shader Program

    auto graphics_module = engine.module<engine::ModuleGraphics>();
    auto& device = graphics_module->device();
    auto& shader_snippet_db = graphics_module->shader_snippet_db();
    const auto& shader_generator = device.shader_generator();

    threed::dynamic::ProgramGenerator generator;
    threed::dynamic::ProgramGenParams params;

    threed::dynamic::DefaultSamplerInput cubemap_input("s_skybox", &m_cubemap_binder);
    threed::dynamic::TypedDefaultConstantInput<glm::mat4> matrix_input("u_vmatrix",
                                                                       &m_matrix_binder);
    cubemap_input.m_desc.stype = threed::dynamic::SamplerType::STCubeMap;

    threed::SamplerCreateParams sampler_params;
    sampler_params.wrap_s = threed::SamplerWrapMode::ClampToEdge;
    sampler_params.wrap_r = threed::SamplerWrapMode::ClampToEdge;
    sampler_params.wrap_t = threed::SamplerWrapMode::ClampToEdge;
    sampler_params.filter_min = threed::SamplerFilter::Linear;
    sampler_params.filter_mag = threed::SamplerFilter::Linear;
    if (auto r = device.create_sampler(sampler_params); !r) {
        MODUS_LOGE("Failed to create cubemap sampler:{}", r.error());
        return Error<>();
    } else {
        m_sampler = *r;
        m_cubemap_binder.m_sampler = m_sampler;
    }

    if (!shader_snippet_db.apply(params, threed::dynamic::ShaderStage::Vertex,
                                 "engine.frame_globals")) {
        MODUS_LOGE("Default Material: Failed to locate frame_globals shader snippet");
        return Error<>();
    }
    params.varying
        .push_back({"v_uv", threed::DataType::Vec3F32, threed::dynamic::VaryingDesc::Type::Smooth,
                    threed::dynamic::PrecisionQualifier::Medium})
        .expect();
    params.vertex_stage.inputs.push_back(shader_desc::default_vertex_input()).expect();
    params.vertex_stage.constants.push_back(&matrix_input).expect();
    params.vertex_stage.main = R"R(
void main() {
    vec4 pos = u_projection_matrix * u_vmatrix * VERTEX;
    gl_Position = pos.xyww;
// NOTE: Astc cubemaps needs to be inverted before processing as the cubemaps
// do not require the images to be flipped to be displayed correctly.
// Alternatively we can also change the coordinates of the shader and
// the order in which the images are processed so this is not really required.
    v_uv = vec3(VERTEX.x, -VERTEX.y, VERTEX.z);
})R";
    params.fragment_stage.fragment_outputs.push_back(shader_desc::default_frag_output()).expect();
    params.fragment_stage.samplers.push_back(&cubemap_input).expect();
    params.fragment_stage.main = R"R(
void main() {
    OUT_COLOR = texture(s_skybox, v_uv);
}
)R";
    params.skip_instanced_variant = true;

    if (auto r = generator.generate(params, shader_generator); !r) {
        MODUS_LOGE("Failed to generate debug draw shape program: {}", r.error());
        return Error<>();
    }

    auto result = generator.result(0).value_or_panic();
    m_binders = result->binders;

    if (auto r = device.create_program(result->create_params); !r) {
        MODUS_LOGE("Failed to create debug draw shape program: {}", r.error());
        return Error<>();
    } else {
        m_prog_forward_handle = *r;
    }

    // Effect
    threed::EffectCreateParams effect_params;
    effect_params.state.depth_stencil.depth.enabled = true;
    effect_params.state.depth_stencil.depth.function = threed::CompareFunction::LessEqual;

    effect_params.state.raster.cull_mode = threed::CullMode::Front;
    effect_params.state.raster.cull_enabled = true;
    effect_params.state.raster.face_counter_clockwise = true;

    auto effect_create_result = device.create_effect(effect_params);
    if (!effect_create_result) {
        MODUS_LOGE("CubeMapMaterial: Failed to create effect: {}", effect_create_result.error());
        return Error<>();
    }
    m_effect_handle = effect_create_result.release_value();

    return Ok<>();
}

Result<> CubeMapMaterial::shutdown(Engine& engine) {
    MODUS_UNUSED(engine);
    if (m_sampler) {
        auto graphics_module = engine.module<ModuleGraphics>();
        auto& device = graphics_module->device();
        device.destroy_sampler(m_sampler);
    }
    if (m_effect_handle.is_valid()) {
        auto graphics_module = engine.module<ModuleGraphics>();
        auto& device = graphics_module->device();
        device.destroy_effect(m_effect_handle);
        m_effect_handle = threed::EffectHandle();
    }

    return Ok<>();
}

}    // namespace modus::engine::graphics
