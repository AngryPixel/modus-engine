/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/graphics/image_generated.h>
#include <pch.h>

#include <core/io/memory_stream.hpp>
#include <engine/graphics/image.hpp>

namespace modus::engine::graphics {

// If these sizes don't match we won't be able to read the files

Result<Image, void> Image::from_bytes(const ByteSlice bytes) {
    flatbuffers::Verifier v(bytes.data(), bytes.size());
    if (!fbs::VerifyImageBuffer(v)) {
        MODUS_LOGE("Content of stream is not an image file!");
        return Error<>();
    }
    auto image = fbs::GetImage(bytes.data());

    const ByteSlice guid_slice(image->guid()->value()->data(), image->guid()->value()->size());
    auto r_guid = GID::from_slice(guid_slice);
    if (!r_guid) {
        MODUS_LOGE("Image contains invalid guid");
        return Error<>();
    }

    return Ok(Image(r_guid.value(), NotMyPtr<const fbs::Image>(image)));
}

Image::Image(const GID& guid, NotMyPtr<const fbs::Image> image) : m_guid(guid), m_image(image) {
    modus_assert(image != nullptr);
    modus_assert(m_image->mip_map_count() > 0);
}

threed::TextureFormat Image::texture_format() const {
    return static_cast<threed::TextureFormat>(m_image->format());
}

threed::TextureType Image::texture_type() const {
    return static_cast<threed::TextureType>(m_image->type());
}

u32 Image::width() const {
    return m_image->width();
}

u32 Image::height() const {
    return m_image->height();
}

u32 Image::depth() const {
    return m_image->depth();
}

u32 Image::mip_map_count() const {
    return m_image->mip_map_count();
}

Optional<ImageMipMap> Image::mip_map(const u32 in_index, const u32 depth) const {
    u32 index = 0;
    if (texture_type() == threed::TextureType::T2DArray) {
        index = (depth * mip_map_count()) + in_index;
    } else {
        index = in_index;
    }
    if (index >= m_image->mips()->size()) {
        return Optional<ImageMipMap>();
    }

    auto fbs_mip_map = (*m_image->mips())[index];
    const ByteSlice img_data(m_image->data()->data(), m_image->data()->size());

    ImageMipMap mip_map;
    mip_map.width = fbs_mip_map->width();
    mip_map.height = fbs_mip_map->height();
    mip_map.depth = fbs_mip_map->depth();
    mip_map.offset = fbs_mip_map->offset();
    mip_map.data = img_data.sub_slice(mip_map.offset, fbs_mip_map->size());
    return mip_map;
}

}    // namespace modus::engine::graphics
