/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <engine/graphics/camera.hpp>

namespace modus::engine::graphics {

glm::vec2 Camera::point_to_screen_coord(const glm::vec3& point,
                                        const u32 screen_width,
                                        const u32 screen_height) const {
    const glm::vec4 viewport(0.f, 0.f, f32(screen_width), f32(screen_height));
    const glm::vec3 result =
        glm::project(point, m_transform.to_matrix(), m_frustum.matrix(), viewport);
    return glm::vec2(result);
}

}    // namespace modus::engine::graphics
