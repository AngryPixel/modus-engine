/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/fixed_stack.hpp>
#include <engine/graphics/animation_buffer_cache.hpp>
#include <engine/graphics/debug_drawer.hpp>
#include <engine/graphics/font_asset.hpp>
#include <engine/graphics/gpuprogram_asset.hpp>
#include <engine/graphics/image_asset.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/material_asset.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/mesh_asset.hpp>
#include <engine/graphics/model_asset.hpp>
#include <engine/graphics/particles/particle_registryv2.hpp>
#include <engine/graphics/pipeline/frame_globals.hpp>
#include <engine/graphics/pipeline/pipeline.hpp>
#include <engine/modules/module.hpp>
#include <os/guid_gen.hpp>
#include <threed/device.hpp>
#include <threed/instance.hpp>
#include <threed/pipeline.hpp>
#include <threed/program_gen/shader_snippet_db.hpp>

namespace modus::threed {
class Compositor;
class IContext;
class Instance;
}    // namespace modus::threed

namespace modus::app {
class Window;
}

namespace modus::engine {

class ModuleGraphics;

struct MODUS_ENGINE_EXPORT GraphicsSettings {
    i32 window_width = 1280;
    i32 window_height = 720;
    i32 framebuffer_width = 1280;
    i32 framebuffer_height = 720;
    bool vsync = true;
};

class MODUS_ENGINE_EXPORT UIRenderer {
   public:
    virtual ~UIRenderer() = default;
    virtual void render(Engine&, threed::Pipeline&) = 0;
};

class MODUS_ENGINE_EXPORT ModuleGraphics final : public Module {
    static constexpr usize kMaxFrameGlobalsStack = 8;

   private:
    std::unique_ptr<threed::Device> m_device;
    NotMyPtr<threed::Compositor> m_compositor;
    NotMyPtr<graphics::IPipeline> m_pipeline;
    NotMyPtr<const threed::Instance> m_threed_instance;
    NotMyPtr<app::Window> m_window;
    threed::dynamic::ShaderSnippetDB m_shader_snippet_db;
    graphics::FrameGlobals m_default_frame_globals;
    FixedStack<NotMyPtr<const graphics::FrameGlobals>, kMaxFrameGlobalsStack> m_frame_globals;
    graphics::MeshDB m_mesh_db;
    graphics::MaterialDB m_material_db;
    GraphicsSettings m_settings;
    graphics::ImageAssetFactory m_image_asset_factory;
    graphics::FontAssetFactory m_font_asset_factory;
    graphics::GPUProgramAssetFactory m_gpuprog_asset_factory;
    graphics::MeshAssetFactory m_mesh_asset_factory;
    graphics::MeshAtlasAssetFactory m_mesh_atlas_asset_factory;
    graphics::MaterialAssetFactory m_material_asset_factory;
    graphics::ModelAssetFactory m_model_asset_factory;
    graphics::AnimationBufferCache m_animation_cache;
    graphics::ParticleRegistryV2 m_particle_registry;
    graphics::DebugDrawer m_debug_drawer;
    NotMyPtr<UIRenderer> m_ui_renderer;

   public:
    MODUS_ENGINE_DECLARE_MODULE();

    ModuleGraphics();

    void set_pipeline(NotMyPtr<graphics::IPipeline> pipeline);

    void unset_pipeline();

    NotMyPtr<const graphics::IPipeline> pipeline() const { return m_pipeline; }

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    void tick(Engine& engine) override;

    Slice<GID> module_dependencies() const override;

    inline threed::Device& device() {
        modus_assert(m_device.get() != nullptr);
        return *m_device;
    }

    inline threed::Compositor& default_compositor() {
        modus_assert(m_compositor != nullptr);
        return *m_compositor;
    }

    const threed::Instance& threed_instance() const {
        modus_assert(m_threed_instance);
        return *m_threed_instance;
    }

    threed::dynamic::ShaderSnippetDB& shader_snippet_db() { return m_shader_snippet_db; }

    const threed::dynamic::ShaderSnippetDB& shader_snippet_db() const {
        return m_shader_snippet_db;
    }

    graphics::DebugDrawer& debug_drawer() { return m_debug_drawer; }

    const graphics::MeshDB& mesh_db() const { return m_mesh_db; }

    graphics::MaterialDB& material_db() { return m_material_db; }

    const graphics::MaterialDB& material_db() const { return m_material_db; }

    const GraphicsSettings& settings() const { return m_settings; }

    NotMyPtr<app::Window> window() { return m_window; }

    Optional<NotMyPtr<const graphics::FrameGlobals>> frame_globals() const;

    Result<> push_frame_globals(NotMyPtr<const graphics::FrameGlobals> globals);

    void pop_frame_globals();

    graphics::AnimationBufferCache& animation_buffer_cache() { return m_animation_cache; }

    graphics::ParticleRegistryV2& particle_registry() { return m_particle_registry; }

    void set_ui_renderer(NotMyPtr<UIRenderer> renderer) { m_ui_renderer = renderer; }
};

}    // namespace modus::engine
