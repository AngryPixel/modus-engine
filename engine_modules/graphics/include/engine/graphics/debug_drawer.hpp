/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/assets/asset_types.hpp>
#include <engine/graphics/debug_drawer_programs.hpp>
#include <threed/pipeline.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::assets {
struct AsyncCallbackResult;
}

namespace modus::engine::graphics {
struct Camera;
struct FrustumEvaluator;
class FontAsset;

struct DebugPipelineState {
    NotMyPtr<const Camera> camera;
    struct {
        threed::FramebufferHandle handle;
        u32 width = 0;
        u32 height = 0;
    } framebuffer;
};

class MODUS_ENGINE_EXPORT DebugDrawer {
   private:
    struct SphereCommand;
    struct CubeCommand;
    struct TextCommand;
    struct Text3DCommand;
    Vector<CubeCommand> m_queued_cube_commands;
    Vector<SphereCommand> m_queued_sphere_commands;
    Vector<TextCommand> m_queued_text_commands;
    Vector<Text3DCommand> m_queued_text3d_commands;
    Vector<f32> m_tmp_lines_buffer;
    Vector<f32> m_tmp_text_buffer;
    NotMyPtr<const FontAsset> m_font_asset;

    DebugDrawerPrograms m_programs;
    assets::AssetHandle m_sphere_asset_handle;
    assets::AssetHandle m_cube_asset_handle;
    assets::AssetHandleV2 m_font_asset_handle;
    assets::AssetHandle m_debug_program_handle;
    assets::AssetHandle m_debug_text_program_handle;
    threed::DrawableHandle m_sphere_drawable;
    threed::DrawableHandle m_cube_drawable;
    threed::DrawableHandle m_text_drawable;
    threed::DrawableHandle m_line_drawable;
    threed::EffectHandle m_debug_draw_effect;
    threed::EffectHandle m_text_draw_effect;
    threed::BufferHandle m_line_buffer;
    threed::BufferHandle m_text_buffer;
    u32 m_line_buffer_size;
    u32 m_text_buffer_size;
    u32 m_prev_text_array_count;

   public:
    DebugDrawer();

    ~DebugDrawer();

    MODUS_CLASS_DISABLE_COPY_MOVE(DebugDrawer);

    Result<> initialize(Engine& engine);

    Result<> shutdown(Engine& engine);

    void submit_passes(Engine& engine,
                       const DebugPipelineState& state,
                       threed::PipelinePtr pipeline);

    void draw_sphere(const glm::vec3& center,
                     const f32 radius,
                     const glm::vec4& color,
                     const bool wireframe);

    void draw_cube(const glm::vec3& center,
                   const f32 radius,
                   const glm::vec4& color,
                   const bool wireframe);

    void draw_line(const glm::vec3& from,
                   const glm::vec3& to,
                   const glm::vec4& color_from,
                   const glm::vec4& color_to);

    void draw_line(const glm::vec3& from, const glm::vec3& to, const glm::vec4& color) {
        draw_line(from, to, color, color);
    }

    void draw_text_2d(const StringSlice text,
                      const u32 x,
                      const u32 y,
                      const f32 scale,
                      const glm::vec3& color);

    void draw_text_3d(const StringSlice text,
                      const glm::vec3& point,
                      const f32 scale,
                      const glm::vec3& color);

    void draw_fps(const Engine& engine,
                  const u32 x,
                  const u32 y,
                  const f32 scale,
                  const glm::vec3& color);

    void draw_axis(const f32 scale);

   private:
    Result<> upload_buffers(Engine& engine);

    Result<> load_effects(Engine& engine);

    Result<> create_line_data(Engine& engine, const u32 buffer_size);

    Result<> create_text_data(Engine& engine, const u32 buffer_size);

    void handle_spheres(Engine& engine,
                        threed::Pass& pass,
                        const Camera& camera,
                        const FrustumEvaluator& evaluator);

    void handle_cubes(Engine& engine,
                      threed::Pass& pass,
                      const Camera& camera,
                      const FrustumEvaluator& evaluator);

    void handle_lines(Engine& engine, const Camera& camera, threed::Pass& pass);

    void handle_text2d(Engine& engine, threed::Pass& pass, const DebugPipelineState& state);

    void handle_text3d(const DebugPipelineState& state);
};

}    // namespace modus::engine::graphics
