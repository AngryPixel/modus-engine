/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <engine/graphics/module_includes.hpp>

#include <core/fixed_stack_string.hpp>
#include <core/guid.hpp>
#include <core/guid_handle_map.hpp>
#include <core/handle_pool.hpp>
#include <engine/animation/animation_types.hpp>
#include <threed/pipeline.hpp>
#include <threed/program_gen/program_gen.hpp>

namespace modus::threed {
struct Command;
}

namespace modus::engine {
class Engine;
}

namespace modus::engine::graphics {
class IPipeline;
struct Renderable;

MODUS_DECLARE_HANDLE_TYPE(MaterialHandle, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(MaterialInstanceHandle, std::numeric_limits<u32>::max());

enum MaterialFlags {
    kMaterialFlagTransparent = 1 << 0,
    kMaterialFlagCullingDisabled = 1 << 1,
    kMaterialFlagForwardRendering = 1 << 2,
    kMaterialFlagDeferredRendering = 1 << 3,
    kMaterialFlagIgnoreViewTransform = 1 << 4,
    KMaterialFlagSkyBox = 1 << 5,
    kMaterialFlagSupportsInstancing = 1 << 6,
};

class MaterialInstance;

struct MODUS_ENGINE_EXPORT MaterialInstanceRef {
    friend class MaterialDB;

   private:
    MaterialHandle m_material_handle;
    MaterialInstanceHandle m_instance_handle;
    u32 m_flags = 0;

   public:
    MaterialInstanceRef() = default;
    MaterialInstanceRef(const MaterialHandle mh, const MaterialInstanceHandle ih, const u32 flags);

    MODUS_CLASS_DEFAULT_COPY_MOVE(MaterialInstanceRef);

    bool has_flag(const u32 flag) const { return (m_flags & flag) == flag; }

    MaterialHandle material_handle() const { return m_material_handle; }

    MaterialInstanceHandle material_instance() const { return m_instance_handle; }

    explicit operator bool() const { return m_material_handle && m_instance_handle; }

    bool operator==(const MaterialInstanceRef& rhs) const {
        return m_material_handle == rhs.m_material_handle &&
               m_instance_handle == rhs.m_instance_handle;
    }

    bool operator!=(const MaterialInstanceRef& rhs) const {
        return m_material_handle != rhs.m_material_handle &&
               m_instance_handle != rhs.m_instance_handle;
    }

    bool operator<(const MaterialInstanceRef& rhs) const {
        if (m_material_handle == rhs.m_material_handle) {
            return m_instance_handle < rhs.m_instance_handle;
        }
        return (m_material_handle < rhs.m_material_handle);
    }
};

class MODUS_ENGINE_EXPORT Material {
   private:
    GID m_guid;
    FixedStackString<64> m_name;
    MaterialHandle m_material_handle;

   protected:
    u32 m_flags;
    threed::EffectHandle m_effect_handle;
    threed::ProgramHandle m_prog_forward_handle;
    threed::ProgramHandle m_prog_instanced_handle;
    threed::dynamic::RuntimeBinders m_binders;
    threed::dynamic::RuntimeBinders m_binders_instanced;

   public:
    Material(StringSlice guid, StringSlice name);

    virtual ~Material() = default;

    const GID& guid() const { return m_guid; }

    StringSlice name() const { return m_name.as_string_slice(); }

    u32 flags() const { return m_flags; }

    bool has_flag(const u32 flag) const { return (m_flags & flag) == flag; }

    MaterialHandle material_handle() const { return m_material_handle; }

    threed::EffectHandle effect_handle() const { return m_effect_handle; }

    threed::ProgramHandle forward_program_handle() const { return m_prog_forward_handle; }

    threed::ProgramHandle instanced_program_handle() const { return m_prog_instanced_handle; }

    virtual Result<> initialize(Engine& engine) = 0;

    virtual Result<> shutdown(Engine& engine) = 0;

    virtual Result<MaterialInstanceHandle, void> new_instance() = 0;

    virtual Result<> delete_intsance(MaterialInstanceHandle) = 0;

    virtual Result<NotMyPtr<MaterialInstance>, void> instance(const MaterialInstanceHandle h) = 0;

    virtual Result<NotMyPtr<const MaterialInstance>, void> instance(
        const MaterialInstanceHandle h) const = 0;

   private:
    friend class MaterialDB;
    friend class MaterialInstance;
    void set_material_handle(const MaterialHandle h);
};

class FrameGlobals;
struct MaterialApplyParams {
    NotMyPtr<const IPipeline> pipeline;
    const Material& base_material;
    const FrameGlobals& frame_globals;
    const glm::mat4& renderable_transform;
    threed::Command& command;
    animation::AnimatorHandle animator;
};

struct MaterialInstancedApplyParams {
    NotMyPtr<const IPipeline> pipeline;
    const Slice<Renderable> renderables;
    const Material& base_material;
    const FrameGlobals& frame_globals;
    threed::Command& command;
};

class MODUS_ENGINE_EXPORT PropertyValue {
   private:
    enum class Type {
        None,
        Bool,
        U32,
        F32,
        Vec2,
        Vec3,
        Vec4,
    };
    union {
        bool b;
        u32 u;
        f32 f;
        struct vec {
            f32 x, y, z, w;
        } vec;
    } m_data;
    Type m_type;

   public:
    PropertyValue();
    PropertyValue(const bool v);
    PropertyValue(const u32 v);
    PropertyValue(const f32 f);
    PropertyValue(const glm::vec2& v);
    PropertyValue(const glm::vec3& v);
    PropertyValue(const glm::vec4& v);

    template <typename T>
    PropertyValue(const T v) : m_type(Type::U32) {
        static_assert(std::is_base_of_v<StrongType<u32>, T>);
        m_data.u = v.value();
    }

    PropertyValue(const PropertyValue&) = default;
    PropertyValue& operator=(const PropertyValue&) = default;

    PropertyValue& operator=(const bool v);
    PropertyValue& operator=(const u32 v);
    PropertyValue& operator=(const f32 f);
    PropertyValue& operator=(const glm::vec2& v);
    PropertyValue& operator=(const glm::vec3& v);
    PropertyValue& operator=(const glm::vec4& v);

    template <typename T>
    PropertyValue& operator=(const T v) {
        static_assert(std::is_base_of_v<StrongType<u32>, T>);
        m_data.u = v.value();
        m_type = Type::U32;
        return *this;
    }

    Result<bool, void> as_bool() const;
    Result<u32, void> as_u32() const;
    Result<f32, void> as_f32() const;
    Result<glm::vec2, void> as_vec2() const;
    Result<glm::vec3, void> as_vec3() const;
    Result<glm::vec4, void> as_vec4() const;

    template <typename T>
    Result<T, void> as_handle() const {
        static_assert(std::is_base_of_v<StrongType<u32>, T>);
        if (m_type != Type::U32) {
            return Error<>();
        }
        return Ok<T>(T(m_data.u));
    }
};

class MODUS_ENGINE_EXPORT MaterialProperty {
   private:
    FixedStackString<32> m_name;

   public:
    MaterialProperty(const StringSlice name);

    virtual ~MaterialProperty() = default;

    StringSlice name() const { return m_name.as_string_slice(); }

    virtual Result<> set(const PropertyValue& value) = 0;

    virtual PropertyValue get() const = 0;
};

class MODUS_ENGINE_EXPORT MaterialInstance {
   private:
    const MaterialHandle m_material_handle;
    const u32 m_flags;

   public:
    // TODO: Property Interfaces?
    MaterialInstance(const MaterialHandle handle, const u32 flags);

    bool has_flag(const u32 flag) const { return (m_flags & flag) == flag; }

    virtual ~MaterialInstance() = default;

    MaterialHandle material_handle() const { return m_material_handle; }

    virtual void apply(Engine& engine, MaterialApplyParams& params) const = 0;

    virtual void apply_instanced(Engine& engine, MaterialInstancedApplyParams& params) const;

    Result<> set_property(const StringSlice name, const PropertyValue& property);

    Result<PropertyValue, void> property(const StringSlice name) const;

   protected:
    virtual Result<NotMyPtr<MaterialProperty>, void> find_property(const StringSlice name);
    virtual Result<NotMyPtr<const MaterialProperty>, void> find_property(
        const StringSlice name) const;
};

using MaterialInstancePtr = NotMyPtr<MaterialInstance>;
using ConstMaterialInstancePtr = NotMyPtr<const MaterialInstance>;
using MaterialPtr = NotMyPtr<Material>;
using ConstMaterialPtr = NotMyPtr<const Material>;

class MODUS_ENGINE_EXPORT MaterialDB {
    static constexpr u32 kMaxMaterialCount = 32;
    using StorageType =
        GUIDHandleMap<MaterialPtr, kMaxMaterialCount, MaterialHandle, GraphicsAllocator>;

   private:
    StorageType m_materials;
    MaterialHandle m_default_material_handle;
    MaterialInstanceRef m_default_instance_ref;

   public:
    MaterialDB() = default;

    ~MaterialDB() = default;

    Result<MaterialHandle, void> add(MaterialPtr material);

    Result<ConstMaterialPtr, void> get(const GID& guid) const;

    Result<ConstMaterialPtr, void> get(const MaterialHandle) const;

    Result<MaterialHandle, void> get_handle(const GID& guid) const;

    MaterialHandle default_material_handle() const { return m_default_material_handle; }

    Result<> set_default_material_handle(MaterialHandle h);

    MaterialInstanceRef default_instance() const { return m_default_instance_ref; }

    Result<MaterialInstanceRef, void> create_instance(const MaterialHandle h);

    Result<MaterialInstanceRef, void> create_instance(const GID& guid);

    Result<MaterialInstancePtr, void> instance(const MaterialInstanceRef& ref);

    Result<ConstMaterialInstancePtr, void> instance(const MaterialInstanceRef& ref) const;

    ConstMaterialPtr default_material() const;

    void erase(ConstMaterialPtr material);

    void destroy_instance(MaterialInstanceRef instance);

    Result<std::pair<ConstMaterialInstancePtr, ConstMaterialPtr>, void> instance_and_material(
        const MaterialInstanceRef& ref) const;

    void clear();
};

template <typename T>
class MODUS_ENGINE_EXPORT MaterialHelper : public Material {
   protected:
    HandlePool<T, GraphicsAllocator, MaterialInstanceHandle> m_pool;

   public:
    MaterialHelper(const StringSlice guid, const StringSlice name, const u32 entries_per_block)
        : Material(guid, name), m_pool(entries_per_block) {}

    Result<MaterialInstanceHandle, void> new_instance() override {
        auto r_add = m_pool.create(T(material_handle(), m_flags));
        if (!r_add) {
            return Error<>();
        }
        return Ok(r_add->first);
    }

    Result<> delete_intsance(MaterialInstanceHandle h) override { return m_pool.erase(h); }

    Result<NotMyPtr<MaterialInstance>, void> instance(const MaterialInstanceHandle h) override {
        auto r = m_pool.get(h);
        if (!r) {
            return Error<>();
        }
        return Ok<MaterialInstancePtr>((*r).get());
    }

    Result<NotMyPtr<const MaterialInstance>, void> instance(
        const MaterialInstanceHandle h) const override {
        auto r = m_pool.get(h);
        if (!r) {
            return Error<>();
        }
        return Ok<ConstMaterialInstancePtr>((*r).get());
    }
};

}    // namespace modus::engine::graphics
