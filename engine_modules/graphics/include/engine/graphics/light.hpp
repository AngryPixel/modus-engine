/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <math/bounding_volumes.hpp>

namespace modus::engine::graphics {
struct FrustumEvaluator;
/*
 http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
    Range Constant Linear Quadratic

    3250, 1.0, 0.0014, 0.000007

    600, 1.0, 0.007, 0.0002

    325, 1.0, 0.014, 0.0007

    200, 1.0, 0.022, 0.0019

    160, 1.0, 0.027, 0.0028

    100, 1.0, 0.045, 0.0075

    65, 1.0, 0.07, 0.017

    50, 1.0, 0.09, 0.032

    32, 1.0, 0.14, 0.07

    20, 1.0, 0.22, 0.20

    13, 1.0, 0.35, 0.44

    7, 1.0, 0.7, 1.8
*/

enum class LightType {
    Ambient,
    Directional,
    Point,
};

class MODUS_ENGINE_EXPORT Light {
   private:
    static constexpr u32 kAttenuationConstantIndex = 0;
    static constexpr u32 kAttenuationLinearIndex = 1;
    static constexpr u32 kAttenuationExpIndex = 2;
    static constexpr u32 kPointLightRadius = 0;
    static constexpr u32 kDirectionalLightAreaX = 0;
    static constexpr u32 kDirectionalLightAreaY = 1;
    static constexpr u32 kDirectionalLightNearPlane = 2;
    static constexpr u32 kDirectionalLightFarPlane = 3;

    LightType m_type;
    glm::vec3 m_diffuse_color;
    f32 m_diffuse_intensity;
    glm::vec4 m_attributes;
    glm::vec3 m_position;
    bool m_emits_shadow;

   public:
    Light()
        : m_type(LightType::Point),
          m_diffuse_color(1.0f),
          m_diffuse_intensity(1.0),
          m_attributes(1.0f, 0.7f, 1.8f, 0.f),
          m_position(0.f),
          m_emits_shadow(false) {}

    const glm::vec3& diffuse_color() const { return m_diffuse_color; }

    f32 diffuse_intensity() const { return m_diffuse_intensity; }

    LightType type() const { return m_type; }

    const glm::vec3& position() const { return m_position; }

    void set_diffuse_color(const glm::vec3& color) { m_diffuse_color = color; }

    void set_diffuse_intensity(const f32 intensity) { m_diffuse_intensity = intensity; }

    void set_position(const glm::vec3& position) { m_position = position; }

    void set_directional_light_area(const f32 x, const f32 y) {
        if (m_type == LightType::Directional) {
            m_attributes[kDirectionalLightAreaX] = x;
            m_attributes[kDirectionalLightAreaY] = y;
        }
    }
    void set_directional_light_near_far(const f32 near_plane, const f32 far_plane) {
        if (m_type == LightType::Directional) {
            m_attributes[kDirectionalLightNearPlane] = near_plane;
            m_attributes[kDirectionalLightFarPlane] = far_plane;
        }
    }

    f32 directional_light_area_x() const {
        modus_assert(m_type == LightType::Directional);
        return m_attributes[kDirectionalLightAreaX];
    }

    f32 directional_light_area_y() const {
        modus_assert(m_type == LightType::Directional);
        return m_attributes[kDirectionalLightAreaY];
    }

    f32 directional_light_near_plane() const {
        modus_assert(m_type == LightType::Directional);
        return m_attributes[kDirectionalLightNearPlane];
    }

    f32 directional_light_far_plane() const {
        modus_assert(m_type == LightType::Directional);
        return m_attributes[kDirectionalLightFarPlane];
    }

    void set_point_light_radius(const f32 radius) {
        modus_assert(m_type == LightType::Point);
        m_attributes[kPointLightRadius] = radius;
    }

    f32 point_light_radius() const {
        modus_assert(m_type == LightType::Point);
        return m_attributes[kPointLightRadius];
    }

    void set_type(const LightType type) { m_type = type; }

    bool evaluate_visibility(const FrustumEvaluator& evaluator) const;

    math::bv::Sphere bounding_sphere() const;

    bool emits_shadow() const { return m_emits_shadow; }

    void set_emits_shadow(const bool v) { m_emits_shadow = v; }
};

}    // namespace modus::engine::graphics
