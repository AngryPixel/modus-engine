/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

namespace modus::threed {
class Device;
}

namespace modus::engine::graphics {
struct Mesh;

MODUS_ENGINE_EXPORT Result<> generate_sphere(Mesh& mesh,
                                             threed::Device& device,
                                             const f32 radius,
                                             const u32 rings,
                                             const u32 sectors);

MODUS_ENGINE_EXPORT Result<> generate_cube(Mesh& mesh, threed::Device& device, const f32 radius);

}    // namespace modus::engine::graphics
