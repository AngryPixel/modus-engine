/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/light_manager.hpp>

namespace modus::threed {
struct SamplerState;
class Compositor;
}    // namespace modus::threed

namespace modus::engine::graphics {
#define MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER
class FrameGlobals;
struct Camera;
class MODUS_ENGINE_EXPORT TileLightManager {
   private:
    struct ShaderSnippet;
    struct alignas(4) LightGridTextureData {
        u16 index;
        u16 count;
    };

    struct alignas(threed::kConstantBufferDataAlignment) Block {
        LightManager::DirectionalLight dir_lights[LightManager::kMaxDirectionalLights];
        LightManager::PointLight point_lights[LightManager::kMaxPointLights];
        glm::vec4 ambient;
        u32 num_dir_lights;
        u32 num_point_lights;
        u32 num_tiles_x;
        u32 num_tiles_y;
    };

    static_assert(sizeof(LightGridTextureData) == sizeof(u32));
    static constexpr u32 kTileWidth = 64;
    static constexpr u32 kTileHeight = 64;
    static constexpr u32 kMaxLightsPerTile = 32;
    std::unique_ptr<ShaderSnippet> m_shader_snippet;
    Block m_block;
    threed::ConstantBufferHandle m_buffer;
    Vector<FixedVector<u16, kMaxLightsPerTile>> m_light_grid;
#if defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
    Vector<u32> m_light_indices;
#else
    Vector<u16> m_light_indices;
#endif
    Vector<LightGridTextureData> m_light_grid_texture_data;
    u32 m_n_tiles_x;
    u32 m_n_tiles_y;
#if defined(MODUS_TILE_LIGHT_MANAGER_USE_SHADER_BUFFER)
    threed::ShaderBufferHandle m_light_indices_texture;
    threed::ShaderBufferHandle m_light_grid_texture;
#else
    threed::TextureHandle m_light_indices_texture;
    threed::TextureHandle m_light_grid_texture;
    threed::SamplerHandle m_sampler;
#endif
    u32 m_last_indices_count;

#if !defined(MODUS_RTM)
    threed::ProgramHandle m_light_grid_debug_program;
    threed::EffectHandle m_light_grid_debug_effect;
#endif
   public:
    TileLightManager();

    ~TileLightManager();

    Result<> initialize(Engine& engine);

    Result<> shutdown(Engine& engine);

    Result<> update(Engine& engine,
                    const threed::Compositor& compsitor,
                    const Camera& camera,
                    const Slice<Light>& lights);

    u32 num_directional_lights() const { return m_block.num_dir_lights; }

    u32 num_point_lights() const { return m_block.num_point_lights; }

    Slice<LightManager::PointLight> point_lights() const {
        return Slice(m_block.point_lights, m_block.num_point_lights);
    }
};

}    // namespace modus::engine::graphics
