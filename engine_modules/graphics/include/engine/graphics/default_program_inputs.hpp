/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <threed/program_gen/program_gen.hpp>

namespace modus::engine::graphics {

static constexpr u8 kVertexSlot = 0;
static constexpr u8 kNormalSlot = 1;
static constexpr u8 kTextureSlot = 2;
static constexpr u8 kTangentSlot = 3;
static constexpr u8 kBinormalSlot = 4;
static constexpr u8 kColourSlot = 5;
static constexpr u8 kCustom1Slot = 6;
static constexpr u8 kCustom2Slot = 7;
static constexpr u8 kCustom3Slot = 8;
static constexpr u8 kCustom4Slot = 9;
static constexpr u8 kJointIndicesSlot = 10;
static constexpr u8 kJointWeightSlot = 11;

namespace shader_desc {
inline threed::dynamic::VertexInput default_vertex_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec4F32;
    desc.name = "VERTEX";
    desc.slot = kVertexSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_normal_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec3F32;
    desc.name = "NORMAL";
    desc.slot = kNormalSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_uv_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec2F32;
    desc.name = "UV";
    desc.slot = kTextureSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_binormal_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec3F32;
    desc.name = "BINORMAL";
    desc.slot = kBinormalSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_tangent_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec3F32;
    desc.name = "TANGENT";
    desc.slot = kTangentSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_color_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec4F32;
    desc.name = "COLOR";
    desc.slot = kColourSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_custom1_input(const threed::DataType dt) {
    threed::dynamic::VertexInput desc;
    desc.data_type = dt;
    desc.name = "CUSTOM1";
    desc.slot = kCustom1Slot;
    return desc;
}

inline threed::dynamic::VertexInput default_custom2_input(const threed::DataType dt) {
    threed::dynamic::VertexInput desc;
    desc.data_type = dt;
    desc.name = "CUSTOM2";
    desc.slot = kCustom2Slot;
    return desc;
}

inline threed::dynamic::VertexInput default_custom3_input(const threed::DataType dt) {
    threed::dynamic::VertexInput desc;
    desc.data_type = dt;
    desc.name = "CUSTOM3";
    desc.slot = kCustom3Slot;
    return desc;
}

inline threed::dynamic::VertexInput default_custom4_input(const threed::DataType dt) {
    threed::dynamic::VertexInput desc;
    desc.data_type = dt;
    desc.name = "CUSTOM4";
    desc.slot = kCustom4Slot;
    return desc;
}

inline threed::dynamic::VertexInput default_bone_indices_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec4U32;
    desc.name = "BONE_INDICES";
    desc.slot = kJointIndicesSlot;
    return desc;
}

inline threed::dynamic::VertexInput default_bone_weights_input() {
    threed::dynamic::VertexInput desc;
    desc.data_type = threed::DataType::Vec4F32;
    desc.name = "BONE_WEIGHTS";
    desc.slot = kJointWeightSlot;
    return desc;
}

inline threed::dynamic::FragmentOutput default_frag_output() {
    threed::dynamic::FragmentOutput desc;
    desc.name = "OUT_COLOR";
    desc.data_type = threed::DataType::Vec4F32;
    desc.slot = 0;
    return desc;
}

}    // namespace shader_desc

}    // namespace modus::engine::graphics
