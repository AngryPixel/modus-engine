/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/event/event_types.hpp>
#include <engine/gameplay/ecs/ecs_types.hpp>

namespace modus::engine::event {
class Event;
}

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT CameraSystem final : public engine::gameplay::System {
   private:
    engine::event::ListenerHandle m_window_resize_listener;
    Optional<f32> m_new_aspect_ratio;

   public:
    CameraSystem();

    Result<> initialize(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) override;

    void update(Engine&,
                const IMilisec tick,
                gameplay::GameWorld&,
                gameplay::EntityManager& entity_manager) override;

    void update_threaded(ThreadSafeEngineAccessor& engine,
                         const IMilisec tick,
                         gameplay::ThreadSafeGameWorldAccessor& game_world,
                         gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void shutdown(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) override;

   private:
    void on_window_resize(engine::Engine& engine, engine::event::Event& event);
};

class MODUS_ENGINE_EXPORT IsometricCameraSystem final : public engine::gameplay::System {
   private:
    struct NewValues {
        f32 width;
        f32 height;
    };
    engine::event::ListenerHandle m_window_resize_listener;
    Optional<NewValues> m_new_values;

   public:
    IsometricCameraSystem();

    Result<> initialize(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) override;

    void update(Engine&,
                const IMilisec,
                gameplay::GameWorld&,
                gameplay::EntityManager& entity_manager) override;

    void update_threaded(ThreadSafeEngineAccessor& engine,
                         const IMilisec,
                         gameplay::ThreadSafeGameWorldAccessor& game_world,
                         gameplay::ThreadSafeEntityManagerAccessor& entity_manager) override;

    void shutdown(Engine& engine, gameplay::GameWorld&, gameplay::EntityManager&) override;

   private:
    void on_window_resize(engine::Engine& engine, engine::event::Event& event);
};

}    // namespace modus::engine::graphics
