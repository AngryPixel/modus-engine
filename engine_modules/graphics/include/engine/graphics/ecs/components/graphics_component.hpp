/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/vector_storage.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/mesh.hpp>

namespace modus::engine::graphics {

namespace fbs::ecs {
struct GraphicsComponent;
}

class MaterialInstance;

enum class GraphicsComponentTag {
    None,
    SkyBox,
};

struct Mesh;
struct MODUS_ENGINE_EXPORT GraphicsComponent final : public gameplay::Component {
    MeshInstance mesh_instance;
    MaterialInstanceRef material_instance;
    math::bv::Sphere bounding_volume;
    GraphicsComponentTag tag = GraphicsComponentTag::None;
    Optional<f32> z_distance;
    animation::AnimatorHandle animator;
    bool visible = true;
    bool casts_shadow = false;

    void on_create(const gameplay::EntityId) override;
};

}    // namespace modus::engine::graphics

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<engine::graphics::GraphicsComponent> {
    using StorageType = VectorStorage<engine::graphics::GraphicsComponent>;
    using FlatbufferType = engine::graphics::fbs::ecs::GraphicsComponent;
};

template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<modus::engine::graphics::GraphicsComponent> {
    struct Intermediate {
        engine::graphics::MeshInstance mesh;
        Optional<f32> z_distance;
        bool visible = true;
        bool cast_shadow = false;
    };

    using ComponentType = modus::engine::graphics::GraphicsComponent;
    using IntermediateType = Intermediate;
    using StorageType = ComponentTraits<modus::engine::graphics::GraphicsComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&);
    static void extract_asset_paths(Vector<String>&, const StorageType&);
    static Result<> to_component(Engine&, ComponentType&, const IntermediateType&, const EntityId);
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.graphics();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(engine::graphics::GraphicsComponent)
