/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/hashmap_storage.hpp>
#include <engine/graphics/camera.hpp>

namespace modus::engine::graphics {

struct MODUS_ENGINE_EXPORT CameraComponent final : public gameplay::Component {
    Camera m_camera;
    glm::vec3 m_forward_vec;
    glm::vec3 m_up_vec;

    void on_create(const gameplay::EntityId) override {
        m_camera.reset();
        m_forward_vec = glm::vec3(0.f, 0.f, -1.0f);
        m_up_vec = glm::vec3(0.f, 1.f, 0.0f);
    }
};

}    // namespace modus::engine::graphics

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<engine::graphics::CameraComponent> {
    using StorageType = HashMapStorage<engine::graphics::CameraComponent>;
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(engine::graphics::CameraComponent)
