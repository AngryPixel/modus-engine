/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/graphics/camera.hpp>

#include "engine/graphics/light.hpp"

namespace modus::engine::graphics {

namespace fbs::ecs {
struct LightComponent;
}

struct MODUS_ENGINE_EXPORT LightComponent final : public gameplay::Component {
    Light light;
    bool enabled = true;

    void on_create(const gameplay::EntityId) override {
        enabled = true;
        light = Light();
    }
};

template <typename Vector>
void collect_visibile_lights(Vector& out,
                             const engine::gameplay::EntityManager& entity_manager,
                             const Camera& camera) {
    engine::graphics::FrustumEvaluator evaluator(camera);
    out.reserve(64);    // magic number!
    engine::gameplay::EntityManagerView<const LightComponent> view(entity_manager);
    view.for_each([&evaluator, &out](const gameplay::EntityId, const LightComponent& lc) {
        if (lc.enabled && lc.light.evaluate_visibility(evaluator)) {
            out.push_back(lc.light);
        }
    });
}

}    // namespace modus::engine::graphics

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<engine::graphics::LightComponent> {
    using StorageType = SparseStorage<engine::graphics::LightComponent>;
    using FlatbufferType = engine::graphics::fbs::ecs::LightComponent;
};
template <>
struct MODUS_ENGINE_EXPORT ComponentLoaderTraits<modus::engine::graphics::LightComponent> {
    using ComponentType = modus::engine::graphics::LightComponent;
    using IntermediateType = ComponentType;
    using StorageType = ComponentTraits<modus::engine::graphics::LightComponent>::FlatbufferType;
    static Result<IntermediateType> load(Engine&, const StorageType&);
    static void unload(Engine&, const IntermediateType&) {}
    static void extract_asset_paths(Vector<String>&, const StorageType&) {}
    static Result<> to_component(Engine&,
                                 ComponentType& out,
                                 const IntermediateType& in,
                                 const EntityId) {
        out = in;
        return Ok<>();
    }
    template <typename T>
    static const StorageType* from_collection(const T& collection) {
        return collection.light();
    }
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(engine::graphics::LightComponent)
