/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/gameplay/ecs/component_storage/sparse_storage.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/graphics/particles/particle_typesv2.hpp>

namespace modus::engine::graphics {

struct MODUS_ENGINE_EXPORT ParticleEmitterV2Component final : public gameplay::Component {
    ParticleEmitterV2Handle m_emitter;

    void on_create(const gameplay::EntityId) override { m_emitter = ParticleEmitterV2Handle(); }
};

}    // namespace modus::engine::graphics

namespace modus::engine::gameplay {
template <>
struct ComponentTraits<engine::graphics::ParticleEmitterV2Component> {
    using StorageType = SparseStorage<engine::graphics::ParticleEmitterV2Component>;
};
}    // namespace modus::engine::gameplay

MODUS_ENGINE_GAMEPLAY_COMPONENT_DECLARE(engine::graphics::ParticleEmitterV2Component)
