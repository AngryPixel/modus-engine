/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/guid.hpp>
#include <core/io/memory_stream.hpp>

namespace modus {
class ISeekableReader;
}

namespace modus::threed {
enum class TextureFormat : u8;
enum class TextureType : u8;
}    // namespace modus::threed

namespace modus::engine::graphics {

namespace fbs {
struct Image;
}

struct ImageMipMap {
    u32 width = 0;
    u32 height = 0;
    u32 depth = 0;
    u32 offset = 0;
    ByteSlice data;
};

class MODUS_ENGINE_EXPORT Image {
   private:
    GID m_guid;
    NotMyPtr<const fbs::Image> m_image;

   private:
    Image(const GID& guid, NotMyPtr<const fbs::Image> image);

   public:
    static Result<Image, void> from_bytes(const ByteSlice bytes);

    ~Image() = default;

    MODUS_CLASS_DISABLE_COPY(Image);
    MODUS_CLASS_DEFAULT_MOVE(Image);

    threed::TextureFormat texture_format() const;

    threed::TextureType texture_type() const;

    u32 width() const;

    u32 height() const;

    u32 depth() const;

    u32 mip_map_count() const;

    Optional<ImageMipMap> mip_map(const u32 index, const u32 depthLevel = 0) const;

    const GID& guid() const { return m_guid; }
};

}    // namespace modus::engine::graphics
