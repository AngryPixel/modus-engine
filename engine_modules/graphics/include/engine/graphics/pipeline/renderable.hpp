/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/material.hpp>
#include <math/transform.hpp>

namespace modus::threed {
struct Pass;
}
namespace modus::engine::gameplay {
struct TransformComponent;
}

namespace modus::engine {
class Engine;
}

namespace modus::engine::graphics {
struct FrustumEvaluator;
struct Camera;
class MeshDB;
struct GraphicsComponent;
class FrameGlobals;

struct MODUS_ENGINE_EXPORT Renderable {
    math::Transform world_transform;
    threed::DrawableHandle drawable;
    MaterialInstanceRef material_instance;
    f32 z_distance;
    animation::AnimatorHandle animator;
};

MODUS_ENGINE_EXPORT u32 component_to_renderable(graphics::Vector<Renderable>& out,
                                                const MeshDB& mesh_db,
                                                const gameplay::TransformComponent& tc,
                                                const Camera& camera,
                                                const GraphicsComponent& gc);

MODUS_ENGINE_EXPORT u32 component_to_renderable_shadow(graphics::Vector<Renderable>& out,
                                                       const MeshDB& mesh_db,
                                                       const gameplay::TransformComponent& tc,
                                                       const Camera& camera,
                                                       const GraphicsComponent& gc);

MODUS_ENGINE_EXPORT bool is_component_visible(const GraphicsComponent& gc,
                                              const graphics::FrustumEvaluator& evaluator);

MODUS_ENGINE_EXPORT void generate_commands(Engine& engine,
                                           NotMyPtr<const IPipeline> pipeline,
                                           threed::Pass& pass,
                                           const FrameGlobals& frame_globals,
                                           SliceMut<Renderable> items,
                                           const bool skip_sorting);
}    // namespace modus::engine::graphics
