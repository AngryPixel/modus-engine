/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::threed {
struct ProgramConstantInputParam;
}

namespace modus::engine::graphics {
struct Camera;

struct FrameGlobalsParams {
    const Camera& camera;
    u32 draw_width;
    u32 draw_height;
};

class MODUS_ENGINE_EXPORT FrameGlobals final {
   public:
    struct alignas(threed::kConstantBufferDataAlignment) ConstantBlock {
        glm::mat4 mat_projection;
        glm::mat4 mat_view;
        glm::mat4 mat_project_view;
        glm::mat4 mat_view_inverse;
        glm::mat4 mat_projection_view_inverse;
        f32 tick_sec;
        f32 elapsed_sec;
        i32 width;
        i32 height;
        glm::vec2 near_far;
    };

   private:
    struct ShaderSnippet;
    ConstantBlock m_globals;
    std::unique_ptr<ShaderSnippet> m_snippet;

   public:
    FrameGlobals();

    ~FrameGlobals();

    Result<> initialize(Engine& engine);

    Result<> shutdown(Engine& engine);

    Result<> update(Engine& engine, const FrameGlobalsParams& params);

    threed::ConstantBufferHandle constant_buffer() const;

    const FrameGlobals::ConstantBlock& constant_block() const { return m_globals; }

    static void fill_constant_input(threed::ProgramConstantInputParam& param);
};

}    // namespace modus::engine::graphics
