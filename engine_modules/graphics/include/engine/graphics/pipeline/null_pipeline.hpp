/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/pipeline/pipeline.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT NullPipeline final : public IPipeline {
   public:
    MODUS_ENGINE_DECLARE_PIPELINE_ID(999999);

    NullPipeline() : IPipeline(kPipelineId) {}

    Result<> initialize(Engine& engine);

    void shutdown(Engine& engine);

    Result<> build(Engine& engine, threed::PipelinePtr pipeline) override;

    void on_compositor_update(Engine& engine, const threed::Compositor& compositor) override;
};

}    // namespace modus::engine::graphics
