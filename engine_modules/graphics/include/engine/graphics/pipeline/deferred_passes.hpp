/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/pipeline/pipeline.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT GBufferPasses final {
   private:
    static constexpr u32 kBufferCount = 2;
    threed::FramebufferHandle m_gbuffer[kBufferCount];
    threed::TextureHandle m_depth_texture[kBufferCount];
    threed::TextureHandle m_color_texture[kBufferCount];
    threed::TextureHandle m_normal_texture[kBufferCount];
    threed::TextureHandle m_emissive_texture[kBufferCount];
    struct {
        struct {
            threed::EffectHandle effect;
            threed::ProgramHandle program;
        } directional;
        struct {
            threed::Pass pass;
            threed::EffectHandle effect;
            threed::EffectHandle null_effect;
            threed::ProgramHandle null_program;
            threed::ProgramHandle program;
            threed::DrawableHandle sphere_mesh;
        } point;
    } m_lighting;
    u32 m_active_buffer;

   public:
    GBufferPasses();

    Result<> initialize(Engine& engine);

    Result<> shutdown(Engine& engine);

    Result<> build(Engine& engine,
                   NotMyPtr<const IPipeline> graphics_pipeline,
                   threed::PipelinePtr pipeline,
                   const FrameGlobals& frame_globals,
                   SliceMut<Renderable> renderables,
                   threed::FramebufferHandle fbo,
                   const u32 w,
                   const u32 h);

    Result<> build_light_directional(Engine& engine,
                                     threed::PipelinePtr pipeline,
                                     const FrameGlobals& frame_globals,
                                     threed::FramebufferHandle fbo,
                                     const u32 w,
                                     const u32 h);

    Result<> build_light_point(Engine& engine,
                               threed::PipelinePtr pipeline,
                               const FrameGlobals& frame_globals,
                               threed::FramebufferHandle fbo,
                               const u32 w,
                               const u32 h);

    void on_compositor_update(Engine& engine, const threed::Compositor& compositor);

   private:
    Result<> build_gbuffer(Engine& engine, const u32 w, const u32 h);
};

}    // namespace modus::engine::graphics
