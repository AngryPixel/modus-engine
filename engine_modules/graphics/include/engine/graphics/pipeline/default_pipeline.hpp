/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/light.hpp>
#include <engine/graphics/particles/particle_render_command_generator.hpp>
#include <engine/graphics/pipeline/deferred_passes.hpp>
#include <engine/graphics/pipeline/pipeline.hpp>
#include <engine/graphics/shadow_manager.hpp>
#include <engine/graphics/tile_light_manager.hpp>
#include <threed/framebuffer_compositor.hpp>

namespace modus::engine::graphics {
/*
class MODUS_ENGINE_EXPORT DeferredPipeline final : public IPipeline {
   private:
    Vector<engine::graphics::Renderable> m_renderables;
    Vector<engine::graphics::Renderable> m_opaque;
    Vector<engine::graphics::Renderable> m_opaque_non_deferred;
    Vector<engine::graphics::Renderable> m_transparent;
    Vector<engine::graphics::Light> m_lights;
    Optional<engine::graphics::Renderable> m_skybox;
    engine::graphics::GBufferPasses m_gbuffer_pass;

   public:
    MODUS_ENGINE_DECLARE_PIPELINE_ID(1);

    DeferredPipeline() : IPipeline(kPipelineId) {}

    Result<> initialize(Engine& engine);

    void shutdown(Engine& engine);

    Result<> build(Engine& engine, threed::PipelinePtr pipeline) override;

    void on_compositor_update(Engine& engine, const threed::Compositor& compositor) override;
};*/

class MODUS_ENGINE_EXPORT ForwardPipeline final : public IPipeline {
   private:
    Vector<engine::graphics::Renderable> m_renderables;
    Vector<engine::graphics::Renderable> m_opaque;
    Vector<engine::graphics::Renderable> m_transparent;
    Vector<engine::graphics::Light> m_lights;
    Optional<engine::graphics::Renderable> m_skybox;
    graphics::TileLightManager m_light_manager;
    graphics::ShadowManager m_shadow_manager;
    ParticleRenderCommandGenerator m_particle_command_generator;

   public:
    MODUS_ENGINE_DECLARE_PIPELINE_ID(2);

    ForwardPipeline() : IPipeline(kPipelineId) {}

    Result<> initialize(Engine& engine);

    void shutdown(Engine& engine);

    Result<> build(Engine& engine, threed::PipelinePtr pipeline) override;

    void on_compositor_update(Engine& engine, const threed::Compositor& compositor) override;

    const graphics::TileLightManager& light_manager() const { return m_light_manager; }

    const graphics::ShadowManager& shadow_manager() const { return m_shadow_manager; }
};

}    // namespace modus::engine::graphics
