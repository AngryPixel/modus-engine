/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/material.hpp>
#include <engine/graphics/mesh.hpp>
#include <engine/graphics/pipeline/frame_globals.hpp>
#include <engine/graphics/pipeline/renderable.hpp>
#include <math/transform.hpp>
#include <threed/pipeline.hpp>

namespace modus::threed {
class Compositor;
class Device;
}    // namespace modus::threed

namespace modus::engine {
namespace gameplay {
struct TransformComponent;
}
class Engine;
}    // namespace modus::engine

namespace modus::engine::graphics {
struct Camera;
class MaterialInstance;
struct GraphicsComponent;
struct FrustumEvaluator;
class MeshDB;

class MODUS_ENGINE_EXPORT IPipeline {
   private:
    const u32 m_pipeline_id;

   public:
    virtual ~IPipeline() = default;

    virtual Result<> build(Engine& engine, threed::PipelinePtr pipeline) = 0;

    virtual void on_compositor_update(Engine& engine, const threed::Compositor& compositor) = 0;
    u32 pipeline_id() const { return m_pipeline_id; }

   protected:
    IPipeline(const u32 id) : m_pipeline_id(id) {}
};

#define MODUS_ENGINE_DECLARE_PIPELINE_ID(id) static constexpr u32 kPipelineId = id

template <typename T>
inline NotMyPtr<const T> pipeline_cast(NotMyPtr<const IPipeline> pipeline) {
    static_assert(std::is_base_of_v<IPipeline, T>);
#if !defined(MODUS_RTM)
    if (pipeline->pipeline_id() != T::kPipelineId) {
        modus_panic("Invalid pipeline cast");
    }
#endif
    return NotMyPtr<const T>(pipeline);
}

class EngineGlobals;
struct MODUS_ENGINE_EXPORT PassState {
    mutable threed::PipelinePtr pipeline;
    const FrameGlobals& engine_globals;
    const Camera& camera;
    struct {
        threed::FramebufferHandle handle;
        u32 width_px;
        u32 height_px;
    } framebuffer;
};

using ThreedPassPtr = NotMyPtr<const threed::Pass>;
class MODUS_ENGINE_EXPORT Pass {
   protected:
    bool m_enabled;

   protected:
    Pass() : m_enabled(true) {}

   public:
    virtual ~Pass() = default;

    virtual Result<> initialize(Engine& engine) = 0;

    virtual Result<> shutdown(Engine& engine) = 0;

    virtual Result<> build(Engine& engine, const PassState&) = 0;

    virtual void on_compositor_update(Engine&, const threed::Compositor&) {}

    bool is_enabled() const { return m_enabled; }

    void enable(const bool v) { m_enabled = v; }
};

}    // namespace modus::engine::graphics
