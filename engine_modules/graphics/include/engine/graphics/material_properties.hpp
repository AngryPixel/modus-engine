/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/material.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT BoolMaterialProperty final : public MaterialProperty {
   private:
    bool m_value;

   public:
    BoolMaterialProperty(const StringSlice name, const bool value);

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    bool value() const { return m_value; }
};

class MODUS_ENGINE_EXPORT U32MaterialProperty final : public MaterialProperty {
   private:
    u32 m_value;

   public:
    U32MaterialProperty(const StringSlice name, const u32 value);

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    u32 value() const { return m_value; }
};

class MODUS_ENGINE_EXPORT F32MaterialProperty final : public MaterialProperty {
   private:
    f32 m_value;

   public:
    F32MaterialProperty(const StringSlice name, const f32 value);

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    f32 value() const { return m_value; }
};

template <typename T>
class MODUS_ENGINE_EXPORT HandleMaterialProperty final : public MaterialProperty {
   private:
    static_assert(std::is_base_of_v<modus::StrongType<u32>, T>);
    static_assert(std::is_same_v<u32, threed::TextureHandle::value_type>);
    T m_value;

   public:
    HandleMaterialProperty(const StringSlice name, const T& value = T())
        : MaterialProperty(name), m_value(value) {}

    Result<> set(const PropertyValue& value) override {
        auto r_val = value.as_u32();
        if (!r_val) {
            return Error<>();
        }
        m_value = T(*r_val);
        return Ok<>();
    }

    PropertyValue get() const override { return PropertyValue(m_value); }

    T value() const { return m_value; };
};

using TextureHandleMaterialProperty = HandleMaterialProperty<threed::TextureHandle>;

class MODUS_ENGINE_EXPORT Vec2MaterialProperty final : public MaterialProperty {
   private:
    glm::vec2 m_value;

   public:
    Vec2MaterialProperty(const StringSlice name, const glm::vec2& value = glm::vec2(0.f));

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    const glm::vec2& value() const { return m_value; }
};

class MODUS_ENGINE_EXPORT Vec3MaterialProperty final : public MaterialProperty {
   private:
    glm::vec3 m_value;

   public:
    Vec3MaterialProperty(const StringSlice name, const glm::vec3& value = glm::vec3(0.f));

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    const glm::vec3& value() const { return m_value; }
};

class MODUS_ENGINE_EXPORT Vec4MaterialProperty final : public MaterialProperty {
   private:
    glm::vec4 m_value;

   public:
    Vec4MaterialProperty(const StringSlice name, const glm::vec4& value = glm::vec4(0.f));

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    const glm::vec4& value() const { return m_value; }
};

class MODUS_ENGINE_EXPORT RGBAMaterialProperty final : public MaterialProperty {
    glm::vec4 m_value;

   public:
    RGBAMaterialProperty(const StringSlice name,
                         const glm::vec4& value = glm::vec4(0.f, 0.f, 0.f, 1.f));

    Result<> set(const PropertyValue& value) override;

    PropertyValue get() const override;

    const glm::vec4& value() const { return m_value; }
};

}    // namespace modus::engine::graphics
