/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <math/frustum.hpp>
#include <math/transform.hpp>

namespace modus::engine::graphics {

struct MODUS_ENGINE_EXPORT Camera {
   private:
    math::Frustum m_frustum;
    math::CachedTransform m_transform;

   public:
    math::Frustum& frustum() { return m_frustum; }

    const math::Frustum& frustum() const { return m_frustum; }

    const math::CachedTransform& transform() const { return m_transform; }

    math::Transform to_world_transform() const {
        math::Transform t = m_transform.inverted();
        t.set_translation(position());
        return t;
    }

    void set_transform(const math::Transform& t) { m_transform.set_transform(t); }

    void reset() {
        m_transform.set_transform(math::Transform::kIdentity);
        m_frustum = math::Frustum();
    }

    void set_look_at(const glm::vec3& position, const glm::vec3& point, const glm::vec3& up) {
        m_transform.set_look_at(position, point, up);
    }

    glm::vec3 position() const { return -m_transform.translation() * m_transform.rotation(); }

    glm::vec2 point_to_screen_coord(const glm::vec3& point,
                                    const u32 screen_width,
                                    const u32 screen_height) const;

    const glm::mat4& projection_matrix() const { return m_frustum.matrix(); }

    const glm::mat4& view_matrix() const { return m_transform.to_matrix(); }

    glm::mat4 projection_view_matrix() const { return projection_matrix() * view_matrix(); }
};

struct MODUS_ENGINE_EXPORT FrustumEvaluator {
    math::FrustumPlanes frustum_planes;
    FrustumEvaluator(const Camera& camera) {
        const glm::mat4 vp = camera.projection_view_matrix();
        frustum_planes.extract(vp);
    }

    inline bool is_visible(const glm::vec3& point) const {
        return frustum_planes.is_visible(point);
    }

    inline bool is_visible(const math::bv::Sphere& sphere) const {
        return frustum_planes.is_visible(sphere);
    }

    inline bool is_visible(const math::bv::AABB& aabb) const {
        return frustum_planes.is_visible(aabb);
    }

    inline bool is_visible(const math::bv::Line& line) const {
        return frustum_planes.is_visible(line);
    }
};

}    // namespace modus::engine::graphics
