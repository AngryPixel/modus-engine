/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/guid.hpp>
#include <core/io/memory_stream.hpp>

namespace modus {
class ISeekableReader;
}

namespace modus::engine::graphics {

static constexpr u32 kMaxFontMetaData = 224;

namespace fbs {
struct Font;
}

struct FontAsciiGenParams {
    StringSlice text;
    f32 x_loc = 0.0f;
    f32 y_loc = 0.0f;
    f32 scale = 1.0f;
    f32 padding = 0.0f;
};

struct FontAsciiGenResult {
    u32 bytes_written;
    u32 array_count;
};

class MODUS_ENGINE_EXPORT Font {
   private:
    GID m_guid;
    NotMyPtr<const fbs::Font> m_font;
    RamStream<GraphicsAllocator> m_data;

   private:
    Font(const GID& guid, NotMyPtr<const fbs::Font> font, RamStream<GraphicsAllocator>&& data);

   public:
    static Result<Font, void> from_stream(ISeekableReader& stream);

    ~Font() = default;

    MODUS_CLASS_DISABLE_COPY(Font);
    MODUS_CLASS_DEFAULT_MOVE(Font);

    const GID& guid() const { return m_guid; }

    static constexpr usize buffer_size_for_text_size(const usize text_size) {
        return text_size * 24;
    }

    // Returns number of f32 values written into the buffer
    Result<FontAsciiGenResult, void> generate_ascii_string_v2(
        SliceMut<f32>& buffer,
        const FontAsciiGenParams& params) const;

    ByteSlice atlas_data() const;

    u32 atlas_width_px() const;

    u32 atlas_height_px() const;
};

}    // namespace modus::engine::graphics
