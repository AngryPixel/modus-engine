/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>
#include <threed/program_gen/default_inputs.hpp>

namespace modus::threed {
class Device;
}

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT AnimationCBufferInput final
    : public threed::dynamic::DefaultConstantBufferInput {
   public:
    AnimationCBufferInput(NotMyPtr<const threed::dynamic::InputBinder> binder);

    Result<threed::dynamic::BindableInputResult> generate_code_snippet(
        threed::dynamic::GenerateParams& params) const override;
};

class MODUS_ENGINE_EXPORT AnimationBufferCache {
   private:
    threed::ConstantBufferHandle m_identity_buffer;

   public:
    AnimationBufferCache() = default;

    ~AnimationBufferCache();

    Result<> initialize(threed::Device& device);

    void shutdown(threed::Device& device);

    Result<threed::ConstantBufferHandle, void> create_buffer(threed::Device& device);

    Result<> update_buffer(threed::Device& device,
                           threed::ConstantBufferHandle buffer,
                           const Slice<glm::mat4> data);

    // Use when the mesh does not have any active animations
    threed::ConstantBufferHandle identity_buffer() const;
};

}    // namespace modus::engine::graphics
