/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/fixed_vector.hpp>
#include <core/guid.hpp>
#include <engine/graphics/material.hpp>
#include <math/bounding_volumes.hpp>
#include <threed/types.hpp>

namespace modus::engine::graphics {

struct MODUS_ENGINE_EXPORT SubMesh {
    threed::DrawableHandle drawable;
    u32 material_index = 0;
};

struct MODUS_ENGINE_EXPORT Mesh {
    static constexpr u32 kMaxSubMeshCount = 8;
    GID mesh_guid;
    FixedVector<threed::BufferHandle, kMaxSubMeshCount> vertices;
    FixedVector<threed::BufferHandle, kMaxSubMeshCount> indices;
    math::bv::Sphere bounding_sphere;
    FixedVector<SubMesh, kMaxSubMeshCount> sub_meshes;
    animation::AnimationCatalogHandle animation_catlog;

    const GID& guid() const { return mesh_guid; }
};

MODUS_DECLARE_HANDLE_TYPE(MeshHandle, std::numeric_limits<u32>::max());

struct MODUS_ENGINE_EXPORT MeshInstance {
    MeshHandle mesh;
    math::bv::Sphere bounding_volume;
    FixedVector<MaterialInstanceRef, Mesh::kMaxSubMeshCount> materials;
    void reset();
};

class MODUS_ENGINE_EXPORT MeshDB : public GUIDHandleMap<Mesh, 32, MeshHandle, GraphicsAllocator> {
   public:
    MeshDB() = default;

    ~MeshDB() = default;

    Result<MeshInstance, void> new_instance(const GID& id) const;

    Result<MeshInstance, void> new_instance(const MeshHandle handle) const;
};

}    // namespace modus::engine::graphics
