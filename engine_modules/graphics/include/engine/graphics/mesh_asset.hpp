/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/animation/animation_types.hpp>
#include <engine/assets/asset_types.hpp>
#include <engine/graphics/mesh.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::graphics {

class MeshDB;

class MODUS_ENGINE_EXPORT MeshAsset final : public assets::AssetBase {
   public:
    Mesh m_mesh;
    MeshHandle m_mesh_handle;
    animation::AnimationCatalogHandle m_animation_catalog;

   public:
    MeshAsset(const Mesh& mesh,
              const MeshHandle mesh_handle,
              const animation::AnimationCatalogHandle animation_catalog);

    MODUS_CLASS_DISABLE_COPY_MOVE(MeshAsset);
};

class MODUS_ENGINE_EXPORT MeshAtlasAsset final : public assets::AssetBase {
   public:
    struct MeshEntry {
        String name;
        Mesh mesh;
        MeshHandle mesh_handle;
        animation::AnimationCatalogHandle animation_catalog;
    };
    Vector<MeshEntry> m_meshes;
    Vector<threed::BufferHandle> m_buffers;

   public:
    MeshAtlasAsset(Vector<MeshEntry>&& meshes, Vector<threed::BufferHandle>&& buffers);

    MODUS_CLASS_DISABLE_COPY_MOVE(MeshAtlasAsset);
};

class MODUS_ENGINE_EXPORT MeshAssetFactory final
    : public assets::PooledAssetFactory<MeshAsset, GraphicsAllocator> {
   private:
    MeshDB& m_mesh_db;

   public:
    MeshAssetFactory(MeshDB& mesh_db);

    MODUS_CLASS_DISABLE_COPY_MOVE(MeshAssetFactory);

    StringSlice file_extension() const override;

    std::unique_ptr<assets::AssetData> create_asset_data() override;

    Result<NotMyPtr<assets::AssetBase>> create(Engine&, assets::AssetData& data) override;

    Result<> destroy(Engine&, NotMyPtr<assets::AssetBase> asset) override;
};

class MODUS_ENGINE_EXPORT MeshAtlasAssetFactory final
    : public assets::PooledAssetFactory<MeshAtlasAsset, GraphicsAllocator> {
   private:
    MeshDB& m_mesh_db;

   public:
    MeshAtlasAssetFactory(MeshDB& mesh_db);

    MODUS_CLASS_DISABLE_COPY_MOVE(MeshAtlasAssetFactory);

    StringSlice file_extension() const override;

    std::unique_ptr<assets::AssetData> create_asset_data() override;

    Result<NotMyPtr<assets::AssetBase>> create(Engine&, assets::AssetData& data) override;

    Result<> destroy(Engine&, NotMyPtr<assets::AssetBase> asset) override;
};

}    // namespace modus::engine::graphics

MODUS_ENGINE_ASSET_TYPE_DECLARE(modus::engine::graphics::MeshAsset)
MODUS_ENGINE_ASSET_TYPE_DECLARE(modus::engine::graphics::MeshAtlasAsset)
