/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/fixed_vector.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::threed {
struct Pipeline;
}

namespace modus::engine::gameplay {
class GameWorld;
}

namespace modus::engine::graphics {

class Light;
class MODUS_ENGINE_EXPORT LightManager {
   public:
    static constexpr u32 kMaxPointLights = 128;
    static constexpr u32 kMaxDirectionalLights = 4;
    struct DirectionalLight {
        glm::vec3 diffuse_color;
        f32 diffuse_intensity;
        glm::vec3 direction;
        f32 padding__;
    };
    struct PointLight {
        glm::vec3 diffuse_color;
        f32 diffuse_intensity;
        glm::vec3 position;
        f32 radius;
    };

    // static f32 point_light_radius(const PointLight& light);

    struct alignas(threed::kConstantBufferDataAlignment) LightBlock {
        DirectionalLight dir_lights[kMaxDirectionalLights];
        PointLight point_lights[kMaxPointLights];
        glm::vec4 ambient;
        u32 num_dir_lights;
        u32 num_point_lights;
    };

   private:
    LightBlock m_block;
    threed::ConstantBufferHandle m_buffer;

   public:
    Result<> initialize(Engine& engine);

    Result<> shutdown(Engine& engine);

    Result<> update(Engine& engine, const Slice<Light>& lights);

    u32 num_directional_lights() const { return m_block.num_dir_lights; }

    u32 num_point_lights() const { return m_block.num_point_lights; }

    threed::ConstantBufferHandle constant_buffer() const { return m_buffer; }

    Slice<PointLight> point_lights() const {
        return Slice(m_block.point_lights, m_block.num_point_lights);
    }
};

}    // namespace modus::engine::graphics
