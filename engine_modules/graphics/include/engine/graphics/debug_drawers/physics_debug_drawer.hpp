/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/graphics/module_includes.hpp>

#include <physics/debug_drawer.hpp>

namespace modus::engine {
class Engine;
namespace graphics {
class DebugDrawer;
}
}    // namespace modus::engine

namespace modus::engine::graphics {

/// Class which implements the debug drawing interface for the physics engine.
/// NOTE: This class is not meant to be stored long time. Create an instance
/// on every use. References stored here are not meant to last longer than a
/// frame.
class MODUS_ENGINE_EXPORT PhysicsDebugDrawer final : public modus::physics::IDebugDrawer {
   private:
    graphics::DebugDrawer& m_drawer;

   public:
    PhysicsDebugDrawer(engine::Engine& engine);

    void draw_line(const glm::vec3& from,
                   const glm::vec3& to,
                   const glm::vec3& colour_from,
                   const glm::vec3& colour_to) override;

    void draw_text3d(const glm::vec3& pos, const StringSlice text) override;
};

}    // namespace modus::engine::graphics
