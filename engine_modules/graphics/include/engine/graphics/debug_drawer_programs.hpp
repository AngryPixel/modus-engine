/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>
#include <threed/program_gen/default_inputs.hpp>

namespace modus::engine {
class ModuleGraphics;
}

namespace modus::engine::graphics {

class DebugDrawerPrograms final {
   public:
    threed::ProgramHandle m_shape_program;
    threed::ProgramHandle m_line_program;
    threed::ProgramHandle m_font_program;

    threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_proj_input;
    threed::dynamic::TypedDefaultConstantInputBinder<glm::vec4> m_color_input;
    threed::dynamic::DefaultSamplerInputBinder m_font_atlas_input;
    threed::dynamic::RuntimeBinders m_shape_binders;
    threed::dynamic::RuntimeBinders m_line_binders;
    threed::dynamic::RuntimeBinders m_font_binders;

    Result<> initialize(NotMyPtr<ModuleGraphics> graphics);

    void shutdown(NotMyPtr<ModuleGraphics> graphics);
};

}    // namespace modus::engine::graphics
