/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/assets/asset_types.hpp>
#include <engine/graphics/camera.hpp>
#include <engine/graphics/pipeline/renderable.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <threed/program_gen/program_gen.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::threed {
struct Pipeline;
struct SamplerState;
}    // namespace modus::threed

namespace modus::engine::gameplay {
class GameWorld;
}

namespace modus::engine::graphics {

class Light;
struct ShadowMapParams {
    const gameplay::GameWorld& game_world;
    threed::Pipeline& pipeline;
    f32 near_plane;
    f32 far_plane;
};

class MODUS_ENGINE_EXPORT ShadowManager {
   private:
    struct ShaderSnippet;
    struct alignas(threed::kConstantBufferDataAlignment) DirShadowCasterBlock {
        glm::mat4 proj_view_matrix;
        glm::vec4 direction;
        u32 num_casters;
    };

    static constexpr u32 kBufferCount = 2;
    std::unique_ptr<ShaderSnippet> m_shader_snippet;
    threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_mvp_input;
    threed::dynamic::RuntimeBinders m_program_binders;
    threed::TextureHandle m_map[kBufferCount];
    threed::FramebufferHandle m_fbo[kBufferCount];
    threed::EffectHandle m_effect;
    threed::ProgramHandle m_program;
    threed::SamplerHandle m_sampler;
    threed::ConstantBufferHandle m_buffer;
    DirShadowCasterBlock m_block;
    Camera m_camera;
    Vector<Renderable> m_renderables;
    u32 m_active_buffer = 0;

   public:
    ShadowManager();

    ~ShadowManager();

    Result<> initialize(Engine& engine);

    Result<> shutdown(Engine& engine);

    Result<> update(Engine& engine, const Slice<Light>& lights);

    void build_pass(Engine& engine, ShadowMapParams& params);

    const glm::mat4& shadow_projection_view_matrix() const { return m_block.proj_view_matrix; }

    bool has_shadow_caster() const { return m_block.num_casters != 0; }
};

}    // namespace modus::engine::graphics
