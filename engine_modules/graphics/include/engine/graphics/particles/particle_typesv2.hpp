/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/handle_pool_shared.hpp>
#include <engine/graphics/module_includes.hpp>

namespace modus::engine::graphics {
MODUS_DECLARE_HANDLE_TYPE(ParticleSystemV2Handle, std::numeric_limits<u32>::max());
MODUS_DECLARE_HANDLE_TYPE(ParticleEmitterV2Handle, std::numeric_limits<u32>::max());
struct ParticleEmitterV2;
class ParticleGeneratorV2;
class ParticleSystemV2;
class ParticleRendererV2;

enum class ParticleRendererType { Point, Billboard, Total };
}    // namespace modus::engine::graphics