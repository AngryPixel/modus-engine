/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>
#include <engine/graphics/particles/particle_typesv2.hpp>
#include <threed/types.hpp>

namespace modus::threed {
class Device;
struct Pass;
}    // namespace modus::threed

namespace modus::engine::graphics {

class ParticleRegistryV2;
struct Camera;
class MODUS_ENGINE_EXPORT ParticleRenderCommandGenerator {
   private:
    struct ParticleSortData {
        NotMyPtr<const ParticleEmitterV2> emitter;
        f32 z_distance;
    };

    Vector<threed::BufferHandle> m_data_buffers;
    Vector<u8> m_staging_buffer;
    u32 m_current_buffer_idx;
    u32 m_buffer_size;
    Vector<ParticleSortData> m_sorted_particles;
    std::unique_ptr<ParticleRendererV2> m_renderers[u32(ParticleRendererType::Total)];

   public:
    ParticleRenderCommandGenerator();
    ~ParticleRenderCommandGenerator();
    MODUS_CLASS_DISABLE_COPY_MOVE(ParticleRenderCommandGenerator);

    Result<> initialize(threed::Device& device, const u32 buffer_size, const u32 alloc_count);

    void shutdown(threed::Device& device);

    void generate_commands(const ParticleRegistryV2& registry,
                           threed::Device& device,
                           threed::Pass& pass,
                           const Camera& camera);

   private:
    Result<threed::BufferHandle> alloc_buffer(threed::Device& device);
};
}    // namespace modus::engine::graphics