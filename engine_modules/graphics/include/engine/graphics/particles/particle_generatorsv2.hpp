/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>
#include <engine/graphics/particles/particlev2.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT NullPosParticleGenerator : public ParticleGeneratorV2 {
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT BoxPosParticleGenerator : public ParticleGeneratorV2 {
   public:
    glm::vec3 m_pos = glm::vec3(0.f);
    glm::vec3 m_max_start_offset = glm::vec3(1.f);

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT CirclePosParticleGenerator : public ParticleGeneratorV2 {
   public:
    glm::vec3 m_center = glm::vec3(0.f);
    f32 m_rad_x = 1.f;
    f32 m_rad_y = 1.f;

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT ColorChangeParticleGenerator : public ParticleGeneratorV2 {
   public:
    glm::vec4 m_start_color = glm::vec4(0.f);
    glm::vec4 m_end_color = glm::vec4(1.f);

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT BasicVelParticleGenerator : public ParticleGeneratorV2 {
   public:
    glm::vec3 m_min_velocity = glm::vec3(0.0f);
    glm::vec3 m_max_velocity = glm::vec3(0.0f);

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT BasicCircleVelParticleGenerator : public ParticleGeneratorV2 {
   public:
    f32 m_min_velocity = 0.f;
    f32 m_max_velocity = 1.f;

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT BasicTimeParticleGenerator : public ParticleGeneratorV2 {
   public:
    f32 m_min_time = 1.f;
    f32 m_max_time = 2.f;

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};

class MODUS_ENGINE_EXPORT NullUVParticleGenerator : public ParticleGeneratorV2 {
   public:
    glm::vec2 m_transform = glm::vec2(0.f);
    f32 m_depth = 0.f;

   public:
    void generate(const ParticleDTType dt,
                  ParticleData& particles,
                  const math::Transform& transform,
                  const usize start_index,
                  const usize end_index) override;
};
}    // namespace modus::engine::graphics
