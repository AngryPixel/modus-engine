/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>
#include <engine/graphics/particles/particlev2.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT BasicVelUpdater : public ParticleUpdaterV2 {
   public:
    void update(const ParticleDTType dt, ParticleData& particles) override;
};

class MODUS_ENGINE_EXPORT EulerUpdater : public ParticleUpdaterV2 {
   public:
    glm::vec3 m_global_acceleration = glm::vec3(0.f);
    void update(const ParticleDTType dt, ParticleData& particles) override;
};

class MODUS_ENGINE_EXPORT BasicColorUpdater : public ParticleUpdaterV2 {
   public:
    void update(const ParticleDTType dt, ParticleData& particles) override;
};

class MODUS_ENGINE_EXPORT BasicTimeUpdater : public ParticleUpdaterV2 {
   public:
    void update(const ParticleDTType dt, ParticleData& particles) override;
};

class MODUS_ENGINE_EXPORT AnimatedUVDepthUpdater : public ParticleUpdaterV2 {
   public:
    f32 m_max_depth = 1.0f;
    f32 m_update_interval = 0.5f;
    void update(const ParticleDTType dt, ParticleData& particles) override;
};

}    // namespace modus::engine::graphics
