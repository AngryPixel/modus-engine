/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/fixed_handle_pool.hpp>
#include <core/guid_handle_map.hpp>
#include <core/handle_pool.hpp>
#include <engine/graphics/particles/particle_typesv2.hpp>
#include <engine/graphics/particles/particlev2.hpp>

namespace modus::threed {
class Device;
struct Pass;
}    // namespace modus::threed

namespace modus::engine::graphics {
struct Camera;

class MODUS_ENGINE_EXPORT ParticleRegistryV2 {
   private:
    static constexpr u32 kNumParticleSystems = 16;

    using ParticleSystemMap = GUIDHandleMap<ParticleSystemV2,
                                            kNumParticleSystems,
                                            ParticleSystemV2Handle,
                                            GraphicsAllocator>;
    ParticleSystemMap m_particle_systems;
    HandlePool<ParticleEmitterV2, GraphicsAllocator, ParticleEmitterV2Handle> m_instances;

   public:
    ParticleRegistryV2();
    ~ParticleRegistryV2();
    MODUS_CLASS_DISABLE_COPY_MOVE(ParticleRegistryV2);

    Result<> initialize();

    void shutdown();

    Result<ParticleSystemV2Handle> register_system(const GID& guid, ParticleSystemV2&& system);

    Result<ParticleSystemV2Handle> system_handle_from_guid(const GID& guid) {
        return m_particle_systems.get_handle(guid);
    }

    Result<NotMyPtr<ParticleSystemV2>> system(const ParticleSystemV2Handle handle) {
        return m_particle_systems.get(handle);
    }

    Result<std::pair<ParticleEmitterV2Handle, NotMyPtr<ParticleEmitterV2>>> create_emitter(
        ParticleEmitterV2&& emitter);

    void destroy_emitter(const ParticleEmitterV2Handle handle);

    Result<> update_emitter_transform(const ParticleEmitterV2Handle handle,
                                      const math::Transform& transform);

    void update_emitters(const Engine& engine);

    template <typename T>
    void for_each(T&& fn) {
        m_instances.for_each(std::forward<T>(fn));
    }

    template <typename T>
    void for_each(T&& fn) const {
        m_instances.for_each(std::forward<T>(fn));
    }

    u32 instance_count() const { return m_instances.count(); }
};
}    // namespace modus::engine::graphics