/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/graphics/module_includes.hpp>

#include <engine/graphics/particles/particlev2.hpp>
#include <threed/program_gen/default_inputs.hpp>
#include <threed/program_gen/program_gen.hpp>
#include <threed/types.hpp>

namespace modus::threed {
class Device;
struct Pass;
}    // namespace modus::threed

namespace modus::engine::graphics {

struct Camera;

class MODUS_ENGINE_EXPORT PointParticleRenderer final : public ParticleRendererV2 {
   private:
    struct alignas(16) Data {
        glm::vec4 position;
        u32 color;
    };
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_vp_binder;
    threed::dynamic::RuntimeBinders m_binders;
    threed::ProgramHandle m_gpuprog;
    threed::DrawableHandle m_drawable;
    threed::EffectHandle m_effect;

   public:
    PointParticleRenderer();

    Result<> initialize(threed::Device& device) override;

    void shutdown(threed::Device& device) override;

    usize render_data_size(const ParticleData& data, const ParticleRenderData*) const override;

    void fill_buffer(ByteSliceMut buffer,
                     const ParticleData& data,
                     const math::Transform& transform,
                     const ParticleRenderData*,
                     const Camera&) const override;

    Result<NotMyPtr<threed::Command>> create_commands(const ParticleData& data,
                                                      const ParticleRenderData* render_data,
                                                      CommandData& command_data) const override;
};

struct MODUS_ENGINE_EXPORT BillboardParticleRendererData final : public ParticleRenderData {
    enum class AlignmentType : u8 { Camera, FixedAxis, TransformedAxis, Velocity };
    threed::TextureHandle texture;
    glm::vec3 axis = glm::vec3(0.f);
    AlignmentType alignment = AlignmentType::Camera;
};

class MODUS_ENGINE_EXPORT BillboardParticleRenderer final : public ParticleRendererV2 {
   private:
    struct alignas(16) Data {
        glm::vec4 position;
        u32 color;
        u32 axis;
        u64 uv;
    };
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_vp_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::vec3> m_camera_pos_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat3> m_view_transposed_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<u32> m_mode_binder;
    mutable threed::dynamic::DefaultSamplerInputBinder m_image_binder;
    threed::dynamic::RuntimeBinders m_binders;
    threed::ProgramHandle m_gpuprog;
    threed::DrawableHandle m_drawable;
    threed::EffectHandle m_effect;
    threed::SamplerHandle m_sampler;

   public:
    static std::unique_ptr<BillboardParticleRendererData> create_render_data() {
        return modus::make_unique<BillboardParticleRendererData>();
    }

    BillboardParticleRenderer();

    Result<> initialize(threed::Device& device) override;

    void shutdown(threed::Device& device) override;

    usize render_data_size(const ParticleData& data, const ParticleRenderData*) const override;

    void fill_buffer(ByteSliceMut buffer,
                     const ParticleData& data,
                     const math::Transform& transform,
                     const ParticleRenderData*,
                     const Camera&) const override;

    Result<NotMyPtr<threed::Command>> create_commands(const ParticleData& data,
                                                      const ParticleRenderData* render_data,
                                                      CommandData& command_data) const override;
};

}    // namespace modus::engine::graphics