/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/ref_pointer.hpp>
#include <engine/graphics/particles/particle_typesv2.hpp>
#include <math/transform.hpp>

namespace modus::threed {
struct Command;
class Device;
struct Pass;
}    // namespace modus::threed

namespace modus::engine::graphics {
struct Camera;
using ParticleDTType = FSeconds;
template <typename Allocator>
class MODUS_ENGINE_EXPORT ParticleDataV2 {
   private:
    Allocator m_allocator;

   public:
    glm::vec4* m_position = nullptr;
    glm::vec4* m_color = nullptr;
    glm::vec4* m_start_color = nullptr;
    glm::vec4* m_end_color = nullptr;
    glm::vec4* m_velocity = nullptr;
    glm::vec4* m_acceleration = nullptr;
    glm::vec4* m_uv = nullptr;      // {x,y} - Texture transform, z = depth , w = ?
    glm::vec4* m_time = nullptr;    // {x = remaining, y= total, z= elapsed [0-1], w= 1/x}
   private:
    bool* m_alive = nullptr;
    usize m_count = 0;
    usize m_count_alive = 0;

   public:
    using this_type = ParticleDataV2<Allocator>;
    ParticleDataV2(const Allocator& allocator = Allocator()) : m_allocator(allocator) {}
    MODUS_CLASS_TEMPLATE_DISABLE_COPY(ParticleDataV2, this_type);

    ParticleDataV2(this_type&& rhs) noexcept
        : m_allocator(rhs.m_allocator),
          m_position(rhs.m_position),
          m_color(rhs.m_color),
          m_start_color(rhs.m_start_color),
          m_end_color(rhs.m_end_color),
          m_velocity(rhs.m_velocity),
          m_acceleration(rhs.m_acceleration),
          m_uv(rhs.m_uv),
          m_time(rhs.m_time),
          m_alive(rhs.m_alive),
          m_count(rhs.m_count),
          m_count_alive(rhs.m_count_alive) {
        new (&rhs) this_type();
    }

    this_type& operator=(this_type&& rhs) noexcept {
        if (this != &rhs) {
            this->~ParticleDataV2();
            new (&rhs) this_type(std::forward<this_type>(rhs));
        }
        return *this;
    }

    ~ParticleDataV2() {
        if (m_position != nullptr) {
            m_allocator.deallocate(m_position);
        }
        if (m_color != nullptr) {
            m_allocator.deallocate(m_color);
        }
        if (m_start_color != nullptr) {
            m_allocator.deallocate(m_start_color);
        }
        if (m_end_color != nullptr) {
            m_allocator.deallocate(m_end_color);
        }
        if (m_velocity == nullptr) {
            m_allocator.deallocate(m_velocity);
        }
        if (m_acceleration != nullptr) {
            m_allocator.deallocate(m_acceleration);
        }
        if (m_uv != nullptr) {
            m_allocator.deallocate(m_uv);
        }
        if (m_alive != nullptr) {
            m_allocator.deallocate(m_alive);
        }
    }

    Result<> generate(const usize particle_count) {
        m_count = particle_count;
        m_alive = 0;
#define MODUS_PARTICLE_MEMBER_ALLOC(X)                                     \
    {                                                                      \
        void* ptr = m_allocator.realloc(X, sizeof(X[0]) * particle_count); \
        if (ptr == nullptr) {                                              \
            return Error<>();                                              \
        }                                                                  \
        X = reinterpret_cast<decltype(X)>(ptr);                            \
    }
        MODUS_PARTICLE_MEMBER_ALLOC(m_position);
        MODUS_PARTICLE_MEMBER_ALLOC(m_color);
        MODUS_PARTICLE_MEMBER_ALLOC(m_start_color);
        MODUS_PARTICLE_MEMBER_ALLOC(m_end_color);
        MODUS_PARTICLE_MEMBER_ALLOC(m_velocity);
        MODUS_PARTICLE_MEMBER_ALLOC(m_acceleration);
        MODUS_PARTICLE_MEMBER_ALLOC(m_uv);
        MODUS_PARTICLE_MEMBER_ALLOC(m_time);
        MODUS_PARTICLE_MEMBER_ALLOC(m_alive);
#undef MODUS_PARTICLE_MEMBER_ALLOC
        return Ok<>();
    }

    void kill(const usize index) {
        modus_assert(m_count_alive > 0);
        modus_assert(index < m_count);
        modus_assert(index < m_count_alive);
        m_alive[index] = false;
        m_count_alive--;
        swap_data(index, m_count_alive);
    }

    void wake(const usize index) {
        modus_assert(index < m_count);
        modus_assert(m_count_alive < m_count);
        m_alive[index] = true;
        m_count_alive++;
    }

    usize dead_start_index() const { return m_count_alive; }

    usize dead_end_index() const { return m_count; }

    constexpr usize alive_start_index() const { return 0; }

    usize alive_end_index() const { return m_count_alive; }

    usize count() const { return m_count; }

    void reset() { m_count_alive = 0; }

   private:
    void swap_data(const usize a, const usize b) {
        modus_assert(a < m_count);
        modus_assert(b < m_count);
        m_position[a] = m_position[b];
        m_color[a] = m_color[b];
        m_start_color[a] = m_start_color[b];
        m_end_color[a] = m_end_color[b];
        m_velocity[a] = m_velocity[b];
        m_acceleration[a] = m_acceleration[b];
        m_time[a] = m_time[b];
    }
};

using ParticleData = ParticleDataV2<GraphicsAllocator>;

/// Particle Generators modify one ore more attributes of a particle once a particle
/// has been spawned
class MODUS_ENGINE_EXPORT ParticleGeneratorV2 {
   public:
    ParticleGeneratorV2() = default;
    virtual ~ParticleGeneratorV2() = default;
    virtual void generate(const ParticleDTType dt,
                          ParticleData& particles,
                          const math::Transform& transform,
                          const usize start_index,
                          const usize end_index) = 0;
};
using ParticleGeneratorV2Ptr = RefPtr<ParticleGeneratorV2, GraphicsAllocator>;

template <typename T, typename... Args>
RefPtr<T> make_particle_generator(Args&&... args) {
    static_assert(std::is_base_of_v<ParticleGeneratorV2, T>);
    return make_refptr_with_allocator<T, GraphicsAllocator, Args...>(std::forward<Args>(args)...);
}

/// Updates particles attributes throughout the lifetime of the particle
class MODUS_ENGINE_EXPORT ParticleUpdaterV2 {
   public:
    ParticleUpdaterV2() = default;
    virtual ~ParticleUpdaterV2() = default;
    virtual void update(const ParticleDTType dt, ParticleData& particles) = 0;
};
using ParticleUpdaterV2Ptr = RefPtr<ParticleUpdaterV2, GraphicsAllocator>;

template <typename T, typename... Args>
RefPtr<T> make_particle_updater(Args&&... args) {
    static_assert(std::is_base_of_v<ParticleUpdaterV2, T>);
    return make_refptr_with_allocator<T, GraphicsAllocator, Args...>(std::forward<Args>(args)...);
}

class MODUS_ENGINE_EXPORT ParticleSystemV2 {
   private:
    Vector<ParticleGeneratorV2Ptr> m_generators;
    Vector<ParticleUpdaterV2Ptr> m_updaters;
    f32 m_emission_rate = 0.f;

   public:
    ParticleSystemV2() = default;
    MODUS_CLASS_DISABLE_COPY(ParticleSystemV2);
    MODUS_CLASS_DEFAULT_MOVE(ParticleSystemV2);

    void update(const ParticleDTType dt, ParticleData& data, const math::Transform& transform);
    void add_generator(ParticleGeneratorV2Ptr emitter) { m_generators.push_back(emitter); }
    void add_updater(ParticleUpdaterV2Ptr updater) { m_updaters.push_back(updater); }
    void set_emission_rate(const f32 emission_rate) { m_emission_rate = emission_rate; }
};

struct MODUS_ENGINE_EXPORT ParticleRenderData {
    virtual ~ParticleRenderData() = default;
};

class MODUS_ENGINE_EXPORT ParticleRendererV2 {
   public:
    ParticleRendererV2() = default;
    virtual ~ParticleRendererV2() = default;

    virtual Result<> initialize(threed::Device&) = 0;

    virtual void shutdown(threed::Device&) = 0;

    virtual usize render_data_size(const ParticleData& data, const ParticleRenderData*) const = 0;

    virtual void fill_buffer(ByteSliceMut buffer,
                             const ParticleData& data,
                             const math::Transform& transform,
                             const ParticleRenderData*,
                             const Camera& camera) const = 0;

    struct CommandData {
        threed::Device& device;
        threed::Pass& pass;
        const Camera& camera;
        threed::BufferHandle buffer;
        u32 buffer_offset = 0;
    };

    virtual Result<NotMyPtr<threed::Command>> create_commands(const ParticleData& data,
                                                              const ParticleRenderData* render_data,
                                                              CommandData& command_data) const = 0;
};

struct MODUS_ENGINE_EXPORT ParticleSystemInstanceV2 {
    ParticleData data;
    std::unique_ptr<ParticleRenderData> render_data;
    ParticleSystemV2Handle system;
    ParticleRendererType renderer_type;
    u32 particle_count = 0;
};

struct MODUS_ENGINE_EXPORT ParticleEmitterV2 {
    math::Transform transform = math::Transform::kIdentity;
    Vector<ParticleSystemInstanceV2> instances;
};

}    // namespace modus::engine::graphics