/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/assets/asset_types.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/material_properties.hpp>
#include <threed/program_gen/default_inputs.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT DefaultMaterialInstance final : public MaterialInstance {
   private:
    struct alignas(threed::kConstantBufferDataAlignment) Block {
        glm::mat4 model;
        glm::mat3 normal;
    };

    Vec4MaterialProperty m_prop_diffuse;
    TextureHandleMaterialProperty m_prop_diffuse_map;
    TextureHandleMaterialProperty m_prop_normal_map;
    TextureHandleMaterialProperty m_prop_specular_map;
    Vec3MaterialProperty m_prop_emissive;
    F32MaterialProperty m_prop_specular;
    Vec3MaterialProperty m_prop_diffuse_mix;

    mutable threed::BufferHandle m_instance_attribs;
    mutable u32 m_last_instance_buffer_size = 0;
    mutable graphics::Vector<Block> m_instance_blocks;

   public:
    static constexpr const char* kPropNameDiffuse = "diffuse";
    static constexpr const char* kPropNameDiffuseMap = "diffuse_map";
    static constexpr const char* kPropNameNormalMap = "normal_map";
    static constexpr const char* kPropNameSpecularMap = "specular_map";
    static constexpr const char* kPropNameSpecular = "specular";
    static constexpr const char* kPropNameEmissive = "emissive";
    static constexpr const char* kPropNameDiffuseMix = "diffuse_mix";

    DefaultMaterialInstance(const MaterialHandle h, const u32 flags);

    void apply(Engine& engine, MaterialApplyParams& params) const override;

    void apply_instanced(Engine& engine, MaterialInstancedApplyParams& params) const override;

   protected:
    Result<NotMyPtr<MaterialProperty>, void> find_property(const StringSlice name) override;
    Result<NotMyPtr<const MaterialProperty>, void> find_property(
        const StringSlice name) const override;
};

class MODUS_ENGINE_EXPORT DefaultMaterial final : public MaterialHelper<DefaultMaterialInstance> {
    friend class DefaultMaterialInstance;

   private:
    mutable threed::dynamic::DefaultSamplerInputBinder m_diffuse_map_binder;
    mutable threed::dynamic::DefaultSamplerInputBinder m_normal_map_binder;
    mutable threed::dynamic::DefaultSamplerInputBinder m_specular_map_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_mvp_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_model_matrix_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::vec4> m_diffuse_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::vec3> m_diffuse_mix_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::vec3> m_emissive_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<f32> m_specular_binder;
    mutable threed::dynamic::TypedDefaultConstantInputBinder<i32> m_flags_binder;
    threed::SamplerHandle m_sampler;

   public:
    static constexpr const char* kGUID = "61f0aec4-92f3-492e-b517-7f9e7e76ae6a";

    DefaultMaterial();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;
};

}    // namespace modus::engine::graphics
