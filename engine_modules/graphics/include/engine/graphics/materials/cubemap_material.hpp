/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/assets/asset_types.hpp>
#include <engine/graphics/material.hpp>
#include <engine/graphics/material_properties.hpp>
#include <threed/program_gen/default_inputs.hpp>

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT CubeMapMaterialInstance final : public MaterialInstance {
   private:
    TextureHandleMaterialProperty m_texture;

   public:
    static constexpr const char* kPropertyNameTexture = "texture";

    CubeMapMaterialInstance(const MaterialHandle h, const u32 flags);

    void apply(Engine& engine, MaterialApplyParams& params) const override;

   protected:
    Result<NotMyPtr<MaterialProperty>, void> find_property(const StringSlice name) override;
    Result<NotMyPtr<const MaterialProperty>, void> find_property(
        const StringSlice name) const override;
};

class MODUS_ENGINE_EXPORT CubeMapMaterial final : public MaterialHelper<CubeMapMaterialInstance> {
    friend class CubeMapMaterialInstance;

   private:
    mutable threed::dynamic::TypedDefaultConstantInputBinder<glm::mat4> m_matrix_binder;
    mutable threed::dynamic::DefaultSamplerInputBinder m_cubemap_binder;
    threed::dynamic::RuntimeBinders m_binders;
    threed::SamplerHandle m_sampler;

   public:
    CubeMapMaterial();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;
};

}    // namespace modus::engine::graphics
