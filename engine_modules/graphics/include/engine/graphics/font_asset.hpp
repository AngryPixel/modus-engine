/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <engine/assets/asset_types.hpp>
#include <engine/graphics/font.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT FontAsset final : public assets::AssetBase {
   public:
    Font m_font;
    threed::TextureHandle m_texture;

   public:
    FontAsset(Font&& font, const threed::TextureHandle handle);

    MODUS_CLASS_DISABLE_COPY_MOVE(FontAsset);
};

class MODUS_ENGINE_EXPORT FontAssetFactory final
    : public assets::PooledAssetFactory<FontAsset, GraphicsAllocator> {
   public:
    FontAssetFactory();

    MODUS_CLASS_DISABLE_COPY_MOVE(FontAssetFactory);

    StringSlice file_extension() const override;

    std::unique_ptr<assets::AssetData> create_asset_data() override;

    Result<NotMyPtr<assets::AssetBase>> create(Engine&, assets::AssetData& data) override;

    Result<> destroy(Engine&, NotMyPtr<assets::AssetBase> asset) override;
};

}    // namespace modus::engine::graphics

MODUS_ENGINE_ASSET_TYPE_DECLARE(modus::engine::graphics::FontAsset)
