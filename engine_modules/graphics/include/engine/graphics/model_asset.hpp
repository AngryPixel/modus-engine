/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/module_includes.hpp>

#include <core/fixed_vector.hpp>
#include <engine/assets/asset_types.hpp>
#include <engine/graphics/mesh.hpp>

namespace modus::engine {
class Engine;
}

namespace modus::engine::graphics {

class MODUS_ENGINE_EXPORT ModelAsset final : public assets::AssetBase {
   public:
    static constexpr u32 kMaxMaterialHandles = 8;
    using MaterialArray = FixedVector<assets::AssetHandleV2, kMaxMaterialHandles>;
    MaterialArray m_material_assets;
    assets::AssetHandleV2 m_mesh_asset;
    MeshHandle m_mesh_handle;
    assets::AssetHandleV2 m_collision_mesh_asset;

   public:
    ModelAsset(const assets::AssetHandleV2 mesh_asset,
               const MeshHandle mesh_handle,
               const MaterialArray& materials,
               Optional<assets::AssetHandleV2> collision_mesh_asset);

    MODUS_CLASS_DISABLE_COPY_MOVE(ModelAsset);
};

class MODUS_ENGINE_EXPORT ModelAssetFactory final
    : public assets::PooledAssetFactory<ModelAsset, GraphicsAllocator> {
   public:
    ModelAssetFactory();

    MODUS_CLASS_DISABLE_COPY_MOVE(ModelAssetFactory);

    StringSlice file_extension() const override;

    std::unique_ptr<assets::AssetData> create_asset_data() override;

    Result<NotMyPtr<assets::AssetBase>> create(Engine&, assets::AssetData& data) override;

    Result<> destroy(Engine&, NotMyPtr<assets::AssetBase> asset) override;
};

}    // namespace modus::engine::graphics

MODUS_ENGINE_ASSET_TYPE_DECLARE(modus::engine::graphics::ModelAsset)
