/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <os/os_pch.h>
#include <package/command.hpp>
#include <package/status_reporter.hpp>
// clang-format on
#include <os/path.hpp>

namespace modus::graphics {

static constexpr const char* kType = "font";

class FontCommandCreator;

class FontCommand final : public package::ICommand {
   public:
    std::vector<std::string> m_args;
    size_t m_input_index;

   public:
    const char* type() const override { return kType; }

    Result<> execute(package::StatusReporter& reporter,
                     const package::CommandContext&) const override {
        return package::detail::execute_process(m_args, "", reporter);
    }

    u64 command_hash(const package::CommandContext&) const override {
        Hasher64 hasher;
        for (const auto& arg : m_args) {
            hasher.update(arg.data(), arg.size());
        }
        return hasher.digest();
    }

    Vector<String> command_dependencies() const override { return {String(m_args[m_input_index])}; }
};

class FontCommandCreator final : public package::ICommandCreator {
   public:
    package::CommandPool<FontCommand, DefaultAllocator> m_commands;
    String m_mk_font_bin;

    FontCommandCreator() : m_commands(16) {}

    const char* type() const override { return kType; }

    Result<> initialize(package::StatusReporter& reporter) override {
        auto r_exec_dir = os::Path::executable_directory();
        if (!r_exec_dir) {
            package::ReportError(reporter, "Failed to get executable directory");
            return Error<>();
        }

#if defined(MODUS_OS_WIN32)
        constexpr const char* kExecName = "modus_mk_font.exe";
#else
        constexpr const char* kExecName = "modus_mk_font";
#endif
        String bin_file = os::Path::join(*r_exec_dir, kExecName);
        if (!os::Path::is_file(bin_file)) {
            package::ReportError(reporter, "Could not find '{}'", bin_file);
            return Error<>();
        }
        m_mk_font_bin = std::move(bin_file);
        return Ok<>();
    }

    Result<package::ICommand*> create(package::StatusReporter& reporter,
                                      const package::CommandCreateParams& params) override {
        static constexpr const char* kInputElem = "input";
        static constexpr const char* kFontSizeElem = "font_size";
        static constexpr const char* kFontPaddingElem = "font_padding";
        static constexpr const char* kAtlasDimElem = "atlas_dim";
        static constexpr const char* kSdfElem = "sdf";
        static constexpr const char* kSdfScaleElem = "sdf_scale";
        size_t input_index = 0;

        ReportDebug(reporter, "Creating Font Command instance");
        std::vector<std::string> args;
        args.reserve(16);
        args.push_back(m_mk_font_bin.c_str());
        args.push_back("-i");
        if (auto r = params.parser.read_string(kInputElem); !r) {
            package::ReportError(reporter, "FontCommandCreator: {}", r.error());
            return Error<>();
        } else {
            String file = os::Path::join(params.current_directory, *r);
            if (!os::Path::is_file(file)) {
                package::ReportError(reporter, "FontCommandCreator: File {} does note exist", file);
                return Error<>();
            }
            input_index = args.size();
            args.push_back(file.c_str());
        }

        if (auto r = params.parser.read_u32(kFontSizeElem); !r) {
            package::ReportError(reporter, "FontCommandCreator: {}", r.error());
            return Error<>();
        } else {
            args.push_back("--atlas-font-size-px");
            args.push_back(fmt::to_string(*r));
        }

        if (auto r = params.parser.read_u32(kFontPaddingElem); !r) {
            package::ReportError(reporter, "FontCommandCreator: {}", r.error());
            return Error<>();
        } else {
            args.push_back("--atlas-padding-px");
            args.push_back(fmt::to_string(*r));
        }

        if (auto r = params.parser.read_u32(kAtlasDimElem); !r) {
            package::ReportError(reporter, "FontCommandCreator: {}", r.error());
            return Error<>();
        } else {
            args.push_back("--atlas-dim-px");
            args.push_back(fmt::to_string(*r));
        }

        if (auto r = params.parser.read_opt_bool(kSdfElem); r && *r) {
            args.push_back("--sdf");
            if (auto s = params.parser.read_opt_f32(kSdfScaleElem); s && *s) {
                args.push_back("--sdf-scale");
                args.push_back(fmt::to_string(**s));
            } else if (!s) {
                package::ReportError(reporter, "FontCommandCreator: {}", s.error());
                return Error<>();
            }
        } else if (!r) {
            package::ReportError(reporter, "FontCommandCreator: {}", r.error());
            return Error<>();
        }

        args.push_back("-o");
        args.push_back(fmt::to_string(params.output_file));

        FontCommand* command = m_commands.create();
        command->m_args = std::move(args);
        command->m_input_index = input_index;
        return Ok<package::ICommand*>(command);
    }

    void shutdown() override { m_commands.clear(); }

    const char* help_string() const override {
        return "Font Command: Create Modus font files.\n"
               "Arguments:\n"
               "    input:str           Font input file(required)\n"
               "    font_size:u32       Font size in pixels (required)\n"
               "    font_padding:u32    Font padding size in pixels (required)\n"
               "    atlas_size:u32      Font atlas size in pixels (required)\n"
               "    sdf:bool Enable     sdf font generation\n"
               "    sdf_scale:f32       Sdf font scale\n";
    }
};

}    // namespace modus::graphics

extern "C" {
MODUS_PACKAGE_COMMAND_EXPORT modus::package::ICommandCreator*
MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME() {
    return new modus::graphics::FontCommandCreator();
}

MODUS_PACKAGE_COMMAND_EXPORT void MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME(
    modus::package::ICommandCreator* ptr) {
    delete ptr;
}
}
