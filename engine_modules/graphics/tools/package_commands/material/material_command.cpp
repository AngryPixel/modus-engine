/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <os/os_pch.h>
// clang-format on
#include <flatc_package_command.hpp>

namespace modus::graphics {

static constexpr const char* kType = "material";
static constexpr const char* kFbsPath = "engine/graphics/material.fbs";

class MaterialCommand final : public FlatcCommandBase {
   public:
    const char* type() const override { return kType; }
};

class MaterialCommandCreator final : public FlatcCommandCreatorBase {
   public:
    package::CommandPool<MaterialCommand, DefaultAllocator> m_commands;
    MaterialCommandCreator() : FlatcCommandCreatorBase(kFbsPath), m_commands(16) {}
    const char* type() const override { return kType; }

    void shutdown() override { m_commands.clear(); }

    const char* help_string() const override {
        return "Material Command: Convert modus materiall.json files.\n"
               "Arguments:\n"
               "    input:str   Material input file(required)\n";
    }

   protected:
    FlatcCommandBase* new_command() override { return m_commands.create(); }
};

}    // namespace modus::graphics

extern "C" {
MODUS_PACKAGE_COMMAND_EXPORT modus::package::ICommandCreator*
MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME() {
    return new modus::graphics::MaterialCommandCreator();
}

MODUS_PACKAGE_COMMAND_EXPORT void MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME(
    modus::package::ICommandCreator* ptr) {
    delete ptr;
}
}
