/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
// clang-format on
#include <core/getopt.hpp>
#include <core/io/byte_stream.hpp>
#include <core/io/memory_stream.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/guid_gen.hpp>

#include <engine/graphics/mesh_generated.h>
#include <engine/graphics/mesh_atlas_generated.h>

using namespace modus;
using namespace modus::engine::graphics;

[[nodiscard]] static bool dump_atlas_info(const StringSlice file_path) {
    auto r_file = os::FileBuilder().read().open(file_path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", file_path);
        return false;
    }

    RamStream<> ram_stream;
    auto r_mem_read = ram_stream.populate_from_stream(*r_file);
    if (!r_mem_read || r_mem_read.value() != r_file->size()) {
        std::cerr << "Failed to read stream into memory\n";
        return false;
    }

    const ByteSlice bytes = ram_stream.as_bytes();

    flatbuffers::Verifier v(bytes.data(), bytes.size());
    if (!fbs::VerifyMeshAtlasBuffer(v)) {
        std::cerr << fmt::format("File {} is not a mesh\n", file_path);
        return false;
    }

    auto atlas = fbs::GetMeshAtlas(bytes.data());
    if (atlas == nullptr) {
        return false;
    }

    std::cout << fmt::format("MeshAtlas: {:.2} MB \n", f64(bytes.size()) / (1024.0 * 1024.0));

    if (auto fbs_data_buffers = atlas->data_buffers(); fbs_data_buffers != nullptr) {
        std::cout << fmt::format("Data Buffers: #{:02}\n", fbs_data_buffers->size());
        for (usize i = 0; i < fbs_data_buffers->size(); ++i) {
            auto fbs_data_buffer = fbs_data_buffers->Get(i);
            if (fbs_data_buffer) {
                auto fbs_data = fbs_data_buffer->data();
                if (fbs_data != nullptr) {
                    std::cout << fmt::format("    [{:02}] size: {} ({:.2} MB)\n", i,
                                             fbs_data->size(),
                                             f64(fbs_data->size()) / (1024.0 * 1024.0));
                }
            }
        }
    }

    if (auto fbs_data_buffers = atlas->index_buffers(); fbs_data_buffers != nullptr) {
        std::cout << fmt::format("Index Buffers: #{:02}\n", fbs_data_buffers->size());
        for (usize i = 0; i < fbs_data_buffers->size(); ++i) {
            auto fbs_data_buffer = fbs_data_buffers->Get(i);
            if (fbs_data_buffer) {
                auto fbs_data = fbs_data_buffer->data();
                if (fbs_data != nullptr) {
                    std::cout << fmt::format("    [{:02}] size: {} ({:.2} MB)\n", i,
                                             fbs_data->size(),
                                             f64(fbs_data->size()) / (1024.0 * 1024.0));
                }
            }
        }
    }

    auto print_drawable_desc = [](const fbs::DrawableDesc& desc) {
        std::cout << fmt::format("       offset       : {:02}\n", desc.offset());
        std::cout << fmt::format("       data type    : {}\n",
                                 fbs::EnumNameDataType(desc.data_type()));
        std::cout << fmt::format("       buffer       : {:02}\n", desc.data_buffer_index());
        std::cout << fmt::format("       normalize?   : {}\n", desc.normalize());
    };

    const auto fbs_meshes = atlas->meshes();
    for (size_t m = 0; m < fbs_meshes->size(); m++) {
        const auto mesh = fbs_meshes->Get(m);
        std::cout << fmt::format("Mesh [{:02}] : {}\n", m, mesh->name()->c_str());
        if (const auto fbs_submeshes = mesh->submeshes(); fbs_submeshes != nullptr) {
            std::cout << fmt::format("  Submeshes: #{:02}\n", fbs_submeshes->size());
            for (usize i = 0; i < fbs_submeshes->size(); ++i) {
                const auto fbs_submesh = fbs_submeshes->Get(i);
                if (fbs_submesh != nullptr) {
                    std::cout << fmt::format("    [{:02}] --------------- \n", i);
                    std::cout << fmt::format("    index_type     : {}\n",
                                             fbs::EnumNameIndexType(fbs_submesh->index_type()));
                    std::cout << fmt::format("    index buffer   : {:02}\n",
                                             fbs_submesh->index_buffer_index());
                    std::cout << fmt::format("    index buf. Off.: {:02}\n",
                                             fbs_submesh->index_buffer_offset());
                    std::cout << fmt::format(
                        "    primitive type : {}\n",
                        fbs::EnumNamePrimitiveType(fbs_submesh->primitive_type()));
                    std::cout << fmt::format("    draw start     : {:02}\n",
                                             fbs_submesh->draw_start());
                    std::cout << fmt::format("    draw count     : {:02}\n",
                                             fbs_submesh->draw_count());
                    std::cout << fmt::format("    material index : {:02}\n",
                                             fbs_submesh->material_index());

                    if (const auto fbs_desc = fbs_submesh->vertex(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    vertex         : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->normals(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    normal         : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->texture(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    texture        : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->tangent(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    tangent        : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->binormal(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    binormal       : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->custom1(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    custom1        : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->custom2(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    custom2        : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->custom3(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    custom3        : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->custom4(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    custom4        : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->bone_indices(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    bone indices   : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                    if (const auto fbs_desc = fbs_submesh->bone_weights(); fbs_desc != nullptr) {
                        std::cout << fmt::format("    bone weights  : \n");
                        print_drawable_desc(*fbs_desc);
                    }
                }
            }
        }
        if (const auto fbs_anim_catalog = mesh->animation_catalog(); fbs_anim_catalog != nullptr) {
            std::cout << fmt::format("  Animation Catalog:\n");
            u32 skeleton_idx = 0;
            for (const auto& fbs_skeleton : *fbs_anim_catalog->skeletons()) {
                std::cout << fmt::format("    Skeleton       : {:02} \n", skeleton_idx);
                if (auto fbs_bones = fbs_skeleton->bones(); fbs_bones != nullptr) {
                    u32 bone_idx = 0;
                    for (const auto fbs_bone : *fbs_bones) {
                        std::cout << fmt::format("       [{:03}] :: {} : {:03}\n", bone_idx,
                                                 fbs_bone->name()->c_str(),
                                                 fbs_bone->parent_index());
                        bone_idx++;
                        if (const auto fbs_transform = fbs_bone->inv_bind_transform();
                            fbs_transform != nullptr) {
                            std::cout << fmt::format(
                                "          t: x:{} y:{} z:{} \n", fbs_transform->position().x(),
                                fbs_transform->position().y(), fbs_transform->position().z());
                            std::cout << fmt::format(
                                "          r: w:{} x:{} y:{} z:{} \n",
                                fbs_transform->rotation().w(), fbs_transform->rotation().x(),
                                fbs_transform->rotation().y(), fbs_transform->rotation().z());
                            std::cout << fmt::format("          s: {}\n", fbs_transform->scale());
                        }
                    }
                }
                ++skeleton_idx;
            }
            u32 anim_idx = 0;
            for (const auto& fbs_animation : *fbs_anim_catalog->skeleton_animations()) {
                std::cout << fmt::format("    Skeleton Animation [{:02}] {}\n", anim_idx,
                                         fbs_animation->name()->c_str());
                std::cout << fmt::format("      skeleton index : {:02} \n",
                                         fbs_animation->skeleton_index());
                std::cout << fmt::format("      duration sec   : {} \n",
                                         fbs_animation->duration_sec());
                if (const auto fbs_bone_keyframes = fbs_animation->bone_keyframes();
                    fbs_bone_keyframes != nullptr) {
                    std::cout << fmt::format("      bone transforms : {} \n",
                                             fbs_animation->bone_keyframes()->size());
                    for (usize j = 0; j < fbs_bone_keyframes->size(); ++j) {
                        if (const auto& fbs_bt = fbs_animation->bone_keyframes()->Get(j); fbs_bt) {
                            const auto fbs_transforms = fbs_bt->bone_transforms();
                            std::cout << fmt::format(
                                "          [{:02}] {:02}:{:02} \n", j, fbs_bt->bone_index(),
                                fbs_transforms != nullptr ? fbs_bt->bone_transforms()->size() : 0);
                        }
                    }
                }
                ++anim_idx;
            }
        }
    }
    return true;
}

struct InputMesh {
    fbs::MeshT mesh;
    String file_path;
};

static Result<InputMesh, void> load_mesh(StringSlice key, StringSlice path) {
    auto r_file = os::FileBuilder().read().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {}\n", path);
        return Error<>();
    }

    RamStream<> ram_stream;
    auto r_mem_read = ram_stream.populate_from_stream(*r_file);
    if (!r_mem_read || r_mem_read.value() != r_file->size()) {
        std::cerr << fmt::format("Failed to read {} into memory\n", path);
        return Error<>();
    }

    ByteSliceMut bytes = ram_stream.as_bytes();
    flatbuffers::Verifier v(bytes.data(), bytes.size());
    if (!fbs::VerifyMeshBuffer(v)) {
        std::cerr << fmt::format("File {} is not a mesh!\n", path);
        return Error<>();
    }

    fbs::Mesh* fbs_mesh = fbs::GetMutableMesh(bytes.data());
    const auto* submeshes = fbs_mesh->submeshes();
    if (submeshes == nullptr || submeshes->size() == 0) {
        std::cerr << fmt::format("No submeshes specified in mesh {}\n", path);
        return Error<>();
    }

    const auto fbs_data_buffers = fbs_mesh->data_buffers();
    const auto fbs_index_buffers = fbs_mesh->index_buffers();
    if (fbs_data_buffers == nullptr || fbs_data_buffers->size() > 1 ||
        fbs_index_buffers == nullptr || fbs_index_buffers->size() > 1) {
        std::cerr << fmt::format(
            "Currently this only works on meshes with one index and data buffer. "
            "Mesh:{} does not meet this requirement\n",
            path);
        return Error<>();
    }
    InputMesh result;
    result.file_path = key.to_str();
    fbs_mesh->UnPackTo(&result.mesh);
    return Ok<InputMesh>(std::move(result));
}

static void merge_data_buffers(fbs::BufferT& out, fbs::BufferT& in) {
    auto& out_buffer = out.data;
    const auto& in_buffer = in.data;
    out_buffer.reserve(out_buffer.size() + in_buffer.size());
    out_buffer.insert(out_buffer.end(), in_buffer.begin(), in_buffer.end());
}

/*static inline core::fbs::GUID to_fbs_guid(const GUID& guid) {
    core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }
    return fbs_guid;
}*/

/*
static Result<> merge_meshes(const StringSlice output) {
                             flatbuffers::FlatBufferBuilder fbs_builder;

    return Ok<>();
}*/

using FBSDataVec = std::vector<std::unique_ptr<fbs::BufferT>>;
struct VectorMergeState {
    FBSDataVec merged_data_buffers;
    FBSDataVec merged_index_buffers;
    Vector<size_t> data_buffer_offsets;
    Vector<size_t> index_buffer_offsets;

    void reserve_data(const size_t buffer_count) {
        const size_t old_size = merged_data_buffers.size();
        const size_t reserve_size = std::max(merged_data_buffers.size(), buffer_count);
        merged_data_buffers.resize(reserve_size);
        for (size_t i = old_size; i < reserve_size; i++) {
            modus_assert(!merged_data_buffers[i]);
            merged_data_buffers[i] = modus::make_unique<fbs::BufferT>();
        }
        data_buffer_offsets.resize(reserve_size, 0);
    }

    void reserve_indices(const size_t buffer_count) {
        const size_t old_size = merged_index_buffers.size();
        const size_t reserve_size = std::max(merged_index_buffers.size(), buffer_count);
        merged_index_buffers.resize(reserve_size);
        for (size_t i = old_size; i < reserve_size; i++) {
            modus_assert(!merged_index_buffers[i]);
            merged_index_buffers[i] = modus::make_unique<fbs::BufferT>();
        }
        index_buffer_offsets.resize(reserve_size, 0);
    }
};

static const StringSlice kIntro =
    "Combine different meshes into a mesh atlas for the Engine. All the meshes and submeshes"
    "will share one set off buffers\n"
    "modus_mk_mesh_atlas [options] --m key0:m0.mesh key1:m1.mesh ... keyN:mN.mesh";

int main(const int argc, const char** argv) {
    // NOTE: This tool assumes that all the meshes are written out with one
    // data buffer and one index buffer. Meshes with submeshes should be
    // prepared so that all the submeshes share the same buffer.
    CmdOptionString opt_output("-o", "--output", "Path for the generated output", "");
    CmdOptionString opt_dump("-d", "--dump", "Print contents of mesh atlas", "");
    CmdOptionStringMap opt_meshes("-m", "--meshes",
                                  "List of meshes with the format MeshKey:MeshPath.");

    CmdOptionParser opt_parser;
    opt_parser.add(opt_output).expect("Failed to add cmd parser option");
    opt_parser.add(opt_meshes).expect("Failed to add cmd parser option");
    opt_parser.add(opt_dump).expect("Failed to add cmd parser option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (opt_dump.parsed()) {
        if (!dump_atlas_info(opt_dump.value())) {
            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    }

    if (!opt_output.parsed()) {
        std::cerr << fmt::format("No output file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_meshes.value().size() < 2) {
        std::cerr << "At least two input files are required.\n";
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    modus::Vector<InputMesh> meshes;
    const auto& key_meshes = opt_meshes.value();
    meshes.reserve(key_meshes.size());
    // Read all input files
    for (const auto& [k, v] : key_meshes) {
        auto r_mesh = load_mesh(k, v);
        if (!r_mesh) {
            return EXIT_FAILURE;
        }
        meshes.push_back(r_mesh.release_value());
    }

    VectorMergeState merge_state;

    std::vector<std::unique_ptr<fbs::MeshEntryT>> fbs_mesh_entries;
    fbs_mesh_entries.reserve(meshes.size());

    for (auto& mesh_entry : meshes) {
        auto& mesh = mesh_entry.mesh;
        merge_state.reserve_data(mesh.data_buffers.size());
        merge_state.reserve_indices(mesh.index_buffers.size());

        // merge data buffers
        for (size_t i = 0; i < mesh.data_buffers.size(); i++) {
            merge_data_buffers(*merge_state.merged_data_buffers[i], *mesh.data_buffers[i]);
        }
        // merge index buffers
        for (size_t i = 0; i < mesh.index_buffers.size(); i++) {
            merge_data_buffers(*merge_state.merged_index_buffers[i], *mesh.index_buffers[i]);
        }

        // Update all buffer entries
        for (auto& sub_mesh : mesh.submeshes) {
            // Update index buffer
            if (sub_mesh->index_type != fbs::IndexType::None) {
                sub_mesh->index_buffer_offset +=
                    merge_state.index_buffer_offsets[sub_mesh->index_buffer_index];
            }

            // Update vertex data
            for (auto& desc : sub_mesh->data_buffers) {
                const u32 new_offset =
                    desc.offset() + merge_state.data_buffer_offsets[desc.data_buffer_index()];
                desc.mutate_offset(new_offset);
            }
        }
        // update buffer offsets;
        // merge data buffers
        for (size_t i = 0; i < mesh.data_buffers.size(); i++) {
            merge_state.data_buffer_offsets[i] += mesh.data_buffers[i]->data.size();
        }
        // merge index buffers
        for (size_t i = 0; i < mesh.index_buffers.size(); i++) {
            merge_state.index_buffer_offsets[i] += mesh.index_buffers[i]->data.size();
        }

        // Create MeshEntry
        auto fbs_mesh_entry = modus::make_unique<fbs::MeshEntryT>();
        fbs_mesh_entry->guid = std::move(mesh.guid);
        fbs_mesh_entry->name = mesh_entry.file_path.c_str();
        fbs_mesh_entry->submeshes = std::move(mesh.submeshes);
        fbs_mesh_entry->animation_catalog = std::move(mesh.animation_catalog);
        fbs_mesh_entry->bounding_volume = std::move(mesh.bounding_volume);
        fbs_mesh_entries.push_back(std::move(fbs_mesh_entry));
    }

    fbs::MeshAtlasT mesh_atlas;
    GID guid = os::guid::generate().value_or_panic("Failed to generate guid");
    mesh_atlas.guid = modus::make_unique<core::fbs::GUID>();
    memcpy(mesh_atlas.guid->mutable_value()->data(), guid.as_bytes().data(),
           guid.as_bytes().size());
    mesh_atlas.data_buffers = std::move(merge_state.merged_data_buffers);
    mesh_atlas.index_buffers = std::move(merge_state.merged_index_buffers);
    mesh_atlas.meshes = std::move(fbs_mesh_entries);

    // write to file

    flatbuffers::FlatBufferBuilder fbs_builder;
    auto fbs_mesh_atlas = fbs::CreateMeshAtlas(fbs_builder, &mesh_atlas);
    fbs_builder.Finish(fbs_mesh_atlas, "MHAT");

    auto r_file = os::FileBuilder().write().create().open(opt_output.value());
    if (!r_file) {
        std::cerr << fmt::format("Failed to open output file:{}\n", opt_output.value());
        return EXIT_FAILURE;
    }

    // Write data to file
    const ByteSlice builder_slice =
        ByteSlice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = r_file->write_exactly(builder_slice);
    if (!r_write) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", opt_output.value());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
