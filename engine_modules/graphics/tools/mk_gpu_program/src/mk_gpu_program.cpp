/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/graphics/gpuprogram_generated.h>

#include <core/getopt.hpp>
#include <core/io/memory_stream.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/guid_gen.hpp>
#include <threed/types.hpp>

#include "constants_json.hpp"

using namespace modus;
using namespace modus::engine;

static const StringSlice kIntro =
    "Generate a modus engine GPU Program file\n Usage: modus_mk_gpu_program -o "
    "output -t [type] --vert ... --frag ...";

static Result<RamStream<>, void> read_file_to_memory(const StringSlice path) {
    auto r_file = os::FileBuilder().read().binary().open(path);
    if (!r_file) {
        return Error<>();
    }

    RamStream<> stream;
    auto r_read = stream.populate_from_stream(r_file.value());
    if (!r_read || r_read.value() != r_file.value().size()) {
        return Error<>();
    }

    return Ok(std::move(stream));
}

static const StringSlice kApiTypeGLES3 = "gles3";
static const StringSlice kApiTypeVulkan = "vulkan";

static auto make_shader_source(flatbuffers::FlatBufferBuilder& builder, const RamStream<>& data) {
    const ByteSlice slice = data.as_bytes();
    return builder.CreateVector(slice.data(), slice.size());
}

int main(const int argc, const char** argv) {
    CmdOptionString opt_output("-o", "--output", "Path to the generated file [REQUIRED]", "");
    CmdOptionString opt_type("-t", "--type", "API type [gles3|vulkan] default = gles3",
                             kApiTypeGLES3);
    CmdOptionString opt_vert("", "--vert", "Path to vertex shader [REQUIRED]", "");
    CmdOptionString opt_frag("", "--frag", "Path to fragment shader", "");
    CmdOptionStringList opt_constants("-c", "--constants",
                                      "Path to json file(s) containing the constant(s) list(s)");
    CmdOptionString opt_guid("-g", "--guid", "Use this GUID instead of generating one", "");
    CmdOptionParser opt_parser;

    opt_parser.add(opt_output).expect("Failed to add option\n");
    opt_parser.add(opt_type).expect("Failed to add option\n");
    opt_parser.add(opt_vert).expect("Failed to add option\n");
    opt_parser.add(opt_frag).expect("Failed to add option\n");
    opt_parser.add(opt_guid).expect("Failed to add option\n");
    opt_parser.add(opt_constants).expect("Failed to add option\n");

    if (!opt_parser.parse(argc, argv) || !opt_vert.parsed() || !opt_output.parsed()) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (opt_type.value() != kApiTypeGLES3 && opt_type.value() != kApiTypeVulkan) {
        std::cerr << fmt::format("Unknown API type : {}\n", opt_type.value());
        return EXIT_FAILURE;
    }

    engine::graphics::fbs::APIType fbs_api_type = engine::graphics::fbs::APIType::GLES3;
    if (opt_type.value() == kApiTypeVulkan) {
        fbs_api_type = engine::graphics::fbs::APIType::VULKAN;
    }

    RamStream<> vert_shader;
    RamStream<> frag_shader;

    Vector<Constant> constants;
    Vector<Sampler> samplers;
    if (opt_constants.parsed()) {
        for (const auto& path : opt_constants.value()) {
            auto r = read_constant_file(path, constants, samplers);
            if (!r) {
                return EXIT_FAILURE;
            }
        }
    }

    // Read vertex shader
    {
        auto r_load = read_file_to_memory(opt_vert.value());
        if (!r_load) {
            std::cerr << fmt::format("Failed to read vertex shader: {}\n", opt_vert.value());
            return EXIT_FAILURE;
        }
        vert_shader = r_load.release_value();
    }

    // read fragment shader
    if (opt_frag.parsed()) {
        auto r_load = read_file_to_memory(opt_frag.value());
        if (!r_load) {
            std::cerr << fmt::format("Failed to read fragment shader: {}\n", opt_frag.value());
            return EXIT_FAILURE;
        }
        frag_shader = r_load.release_value();
    }

    GID guid;
    if (!opt_guid.parsed()) {
        auto r_guid = os::guid::generate();
        if (!r_guid) {
            std::cerr << "Failed to generate GUID\n";
            return EXIT_FAILURE;
        }
        guid = *r_guid;
    } else {
        auto r_guid = GID::from_string(opt_guid.value());
        if (!r_guid) {
            std::cerr << fmt::format("GUID '{}' is not a valid GUID type\n", opt_guid.value());
            return EXIT_FAILURE;
        };
        guid = r_guid.value();
    }

    // Fill up GUID
    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    flatbuffers::FlatBufferBuilder fbs_builder;

    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<engine::graphics::fbs::Constant>>>
        fbs_constants;
    flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<engine::graphics::fbs::Sampler>>>
        fbs_samplers;

    if (!constants.empty()) {
        Vector<flatbuffers::Offset<engine::graphics::fbs::Constant>> offset_vector;
        offset_vector.reserve(constants.size());
        for (const auto& constant : constants) {
            const StringSlice name_slice = constant.name.as_string_slice();
            auto fbs_name = fbs_builder.CreateString(name_slice.data(), name_slice.size());
            engine::graphics::fbs::ConstantBuilder builder(fbs_builder);
            builder.add_name(fbs_name);
            builder.add_is_buffer(constant.is_buffer);
            if (!constant.is_buffer) {
                builder.add_data_type(
                    static_cast<engine::graphics::fbs::DataType>(constant.data_type));
            }
            offset_vector.push_back(builder.Finish());
        }
        fbs_constants = fbs_builder.CreateVector(offset_vector.data(), offset_vector.size());
    }

    if (!samplers.empty()) {
        Vector<flatbuffers::Offset<engine::graphics::fbs::Sampler>> offset_vector;
        offset_vector.reserve(samplers.size());
        for (const auto& sampler : samplers) {
            const StringSlice name_slice = sampler.name.as_string_slice();
            auto fbs_name = fbs_builder.CreateString(name_slice.data(), name_slice.size());
            engine::graphics::fbs::SamplerBuilder builder(fbs_builder);
            builder.add_name(fbs_name);
            offset_vector.push_back(builder.Finish());
        }
        fbs_samplers = fbs_builder.CreateVector(offset_vector.data(), offset_vector.size());
    }

    auto fbs_vert_source = make_shader_source(fbs_builder, vert_shader);
    Optional<flatbuffers::Offset<flatbuffers::Vector<u8>>> fbs_frag_source;

    if (!frag_shader.as_bytes().is_empty()) {
        fbs_frag_source = make_shader_source(fbs_builder, frag_shader);
    }

    engine::graphics::fbs::GPUProgramBuilder builder(fbs_builder);
    builder.add_api(fbs_api_type);
    builder.add_samplers(fbs_samplers);
    builder.add_constants(fbs_constants);
    builder.add_vert(fbs_vert_source);
    if (fbs_frag_source.has_value()) {
        builder.add_frag(fbs_frag_source.value());
    }
    builder.add_guid(&fbs_guid);

    auto fbs_program = builder.Finish();
    fbs_builder.Finish(fbs_program, "MPRG");

    auto r_file = os::FileBuilder().write().create().binary().open(opt_output.value_cstr());
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {} for writing.\n", opt_output.value());
        return EXIT_FAILURE;
    }

    os::File file = r_file.release_value();

    const ByteSlice data_slice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = file.write(data_slice);
    if (!r_write || r_write.value() != data_slice.size()) {
        std::cerr << fmt::format("Failed to write data to {} .\n", opt_output.value());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
