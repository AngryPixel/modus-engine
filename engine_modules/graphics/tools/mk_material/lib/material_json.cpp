/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clangformat off

#include <core/core_pch.h>
#include <math/math_pch.h>
#include <os/os_pch.h>
#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <core/io/memory_stream.hpp>
#include <material_json.hpp>
#include <os/file.hpp>

using namespace rapidjson;

// clangformat on
namespace modus {

static constexpr const char* kKeyGroupColor = "color";
static constexpr const char* kKeyGroupMaps = "maps";
static constexpr const char* kKeyName = "name";
static constexpr const char* kKeyDisableCulling = "disable_culling";
static constexpr const char* kKeyIsTransparent = "transparent";
static constexpr const char* kKeyOpacity = "opacity";
static constexpr const char* kKeyDiffuse = "diffuse";
static constexpr const char* kKeySpecular = "specular";
static constexpr const char* kKeySpecularExp = "specular_exp";
static constexpr const char* kKeyEmissive = "emissive";
static constexpr const char* kKeyNormal = "normal";
static constexpr const char* kKeyGUID = "guid";

Result<Optional<f32>, String> read_float_opt(const Value& json_value, const char* name) {
    if (!json_value.HasMember(name)) {
        return Ok(Optional<f32>());
    }

    const auto& value = json_value[name];
    if (!value.IsFloat()) {
        return Error(modus::format("Element {} is not of type float", name));
    }
    return Ok(Optional<f32>(value.GetFloat()));
}

Result<f32, String> read_float(const Value& json_value, const char* name) {
    if (!json_value.HasMember(name)) {
        return Error(modus::format("Failed to locate elment {}", name));
    }

    const auto& value = json_value[name];
    if (!value.IsFloat()) {
        return Error(modus::format("Element {} is not of type float", name));
    }
    return Ok(value.GetFloat());
}

Result<Optional<Vector<String>>, String> read_string_list(const Value& json_value,
                                                          const char* name) {
    using ReturnType = Optional<Vector<String>>;
    if (!json_value.HasMember(name)) {
        return Ok(ReturnType());
    }

    const auto& value = json_value[name];
    if (!value.IsArray()) {
        return Error(modus::format("Element {} is not of type array", name));
    }

    Vector<String> list;
    u32 i = 0;
    for (auto it = value.Begin(); it != value.End(); ++it) {
        const auto& it_value = *it;
        if (!it_value.IsString()) {
            return Error(modus::format("Element [:02] in arry {} is not of type string", i, name));
        }
        list.push_back(it_value.GetString());
        ++i;
    }

    return Ok(ReturnType(std::move(list)));
}

Result<Optional<glm::vec3>, String> read_vec3(const Value& json_value, const char* name) {
    if (!json_value.HasMember(name)) {
        return Ok(Optional<glm::vec3>());
    }

    const auto& value = json_value[name];
    if (!value.IsObject()) {
        return Error(modus::format("Element {} is not of type object", name));
    }
    glm::vec3 vec;
    if (auto r = read_float(value, "x"); r) {
        vec.x = *r;
    } else {
        return Error(r.release_error());
    }
    if (auto r = read_float(value, "y"); r) {
        vec.y = *r;
    } else {
        return Error(r.release_error());
    }
    if (auto r = read_float(value, "z"); r) {
        vec.z = *r;
    } else {
        return Error(r.release_error());
    }

    return Ok(Optional<glm::vec3>(vec));
}

Result<MaterialJson, String> material_json_from_file(const MaterialJson& src,
                                                     const StringSlice file) {
    MODUS_UNUSED(file);
    MaterialJson material = src;

    RamStream<> m_json_data;

    {
        auto r_file = os::FileBuilder().read().open(file);
        if (!r_file) {
            return Error<String>(modus::format("Failed to open {}", file));
        }

        auto r_read = m_json_data.populate_from_stream(*r_file);
        if (!r_read || r_read.value() != r_file->size()) {
            return Error<String>(modus::format("Failed to read into memory"));
        }
    }
    const StringSlice doc_slice = m_json_data.as_string_slice();
    Document doc;
    doc.Parse(doc_slice.data(), doc_slice.size());

    if (doc.GetParseError() != ParseErrorCode::kParseErrorNone) {
        // Failed to parse document
        return Error(modus::format("Failed to parser json document"));
    }

    // Parse Name [required]
    if (doc.HasMember(kKeyName)) {
        if (const auto& value = doc[kKeyName]; value.IsString()) {
            material.name = value.GetString();
        } else {
            return Error<String>(modus::format("Element '{}' is not of type string", kKeyName));
        }
    }

    if (doc.HasMember(kKeyGUID)) {
        if (const auto& value = doc[kKeyGUID]; value.IsString()) {
            auto r_guid = GID::from_string(value.GetString());
            if (!r_guid) {
                return Error<String>(modus::format("'{}' is not a valid GUID", value.GetString()));
            }
            material.guid = *r_guid;
        } else {
            return Error<String>(modus::format("Element '{}' is not of type string", kKeyName));
        }
    }

    // Parse culling info
    if (doc.HasMember(kKeyDisableCulling)) {
        if (const auto& value = doc[kKeyDisableCulling]; value.IsBool()) {
            material.disable_culling = value.GetBool();
        } else {
            return Error(modus::format("Element '{}' is not of type bool", kKeyDisableCulling));
        }
    }

    // Parse Transparency
    if (doc.HasMember(kKeyIsTransparent)) {
        if (const auto& value = doc[kKeyIsTransparent]; value.IsBool()) {
            material.is_transparent = value.GetBool();
        } else {
            return Error(modus::format("Element '{}' is not of type bool", kKeyIsTransparent));
        }
    }

    // Parse colour group
    if (doc.HasMember(kKeyGroupColor)) {
        const auto& group = doc[kKeyGroupColor];
        if (!doc.IsObject()) {
            return Error(modus::format("Element '{}' is not of type object", kKeyGroupColor));
        }

        if (auto r = read_vec3(group, kKeyDiffuse); r) {
            if ((*r).has_value()) {
                material.color.diffuse = **r;
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_vec3(group, kKeySpecular); r) {
            if ((*r).has_value()) {
                material.color.specular = **r;
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_vec3(group, kKeyEmissive); r) {
            if ((*r).has_value()) {
                material.color.emissive = **r;
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_float_opt(group, kKeySpecularExp)) {
            if ((*r).has_value()) {
                material.color.specular_exponent = **r;
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_float_opt(group, kKeyOpacity)) {
            if ((*r).has_value()) {
                material.color.opacity = **r;
            }
        } else {
            return Error(r.release_error());
        }
    }

    // Parse
    if (doc.HasMember(kKeyGroupMaps)) {
        const auto& group = doc[kKeyGroupMaps];
        if (!doc.IsObject()) {
            return Error(modus::format("Element '{}' is not of type object", kKeyGroupMaps));
        }

        if (auto r = read_string_list(group, kKeyDiffuse)) {
            if ((*r).has_value()) {
                material.maps.diffuse = std::move(**r);
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_string_list(group, kKeySpecular)) {
            if ((*r).has_value()) {
                material.maps.specular = std::move(**r);
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_string_list(group, kKeyNormal)) {
            if ((*r).has_value()) {
                material.maps.normal = std::move(**r);
            }
        } else {
            return Error(r.release_error());
        }

        if (auto r = read_string_list(group, kKeyEmissive)) {
            if ((*r).has_value()) {
                material.maps.emissive = std::move(**r);
            }
        } else {
            return Error(r.release_error());
        }
    }

    return Ok(std::move(material));
}

Result<void, String> material_json_to_file(const MaterialJson& material, const StringSlice& file) {
    using StrRef = Value::StringRefType;
    MODUS_UNUSED(file);
    MODUS_UNUSED(material);
    Document doc;
    doc.SetObject();

    Value name;
    name.SetString(material.name.c_str(), doc.GetAllocator());

    Value disable_culling(material.disable_culling);
    Value is_transparent(material.is_transparent);

    const String guid_str = modus::format("{}", material.guid);
    Value guid;
    guid.SetString(guid_str.c_str(), doc.GetAllocator());

    doc.AddMember(StrRef(kKeyGUID), guid, doc.GetAllocator());
    doc.AddMember(StrRef(kKeyName), name, doc.GetAllocator());
    doc.AddMember(StrRef(kKeyDisableCulling), disable_culling, doc.GetAllocator());
    doc.AddMember(StrRef(kKeyIsTransparent), is_transparent, doc.GetAllocator());
    {
        auto write_vec3 = [&doc](Value& parent, const glm::vec3& vec,
                                 const char* var_name) -> void {
            Value v;
            v.SetObject();
            v.AddMember("x", vec.x, doc.GetAllocator());
            v.AddMember("y", vec.y, doc.GetAllocator());
            v.AddMember("z", vec.z, doc.GetAllocator());
            parent.AddMember(StrRef(var_name), v, doc.GetAllocator());
        };

        Value group;
        group.SetObject();
        write_vec3(group, material.color.diffuse, kKeyDiffuse);
        write_vec3(group, material.color.diffuse, kKeySpecular);
        write_vec3(group, material.color.diffuse, kKeyEmissive);
        group.AddMember(StrRef(kKeySpecularExp), material.color.specular_exponent,
                        doc.GetAllocator());
        group.AddMember(StrRef(kKeyOpacity), material.color.opacity, doc.GetAllocator());
        doc.AddMember(StrRef(kKeyGroupColor), group, doc.GetAllocator());
    }

    {
        auto write_list = [&doc](Value& parent, const Vector<String>& list,
                                 const char* var_name) -> void {
            if (list.empty()) {
                return;
            }
            Value array;
            array.SetArray();
            for (auto& str : list) {
                Value v;
                v.SetString(StrRef(str.c_str()));
                array.PushBack(v, doc.GetAllocator());
            }
            parent.AddMember(StrRef(var_name), array, doc.GetAllocator());
        };

        Value group;
        group.SetObject();
        write_list(group, material.maps.diffuse, kKeyDiffuse);
        write_list(group, material.maps.specular, kKeySpecular);
        write_list(group, material.maps.emissive, kKeyEmissive);
        write_list(group, material.maps.normal, kKeyNormal);
        doc.AddMember(StrRef(kKeyGroupMaps), group, doc.GetAllocator());
    }

    // Serialize out
    rapidjson::StringBuffer buffer(0, 65 * 1024 * 1024);
    rapidjson::PrettyWriter<rapidjson::StringBuffer> rjwriter(buffer);

    if (!doc.Accept(rjwriter)) {
        return Error(String("Failed to write json to memory buffer"));
    }

    auto r_file = os::FileBuilder().write().create().open(file);
    if (!r_file) {
        return Error(modus::format("Failed to open {} for writing", file));
    }

    const StringSlice generated = StringSlice(buffer.GetString(), buffer.GetSize());
    auto write_result = r_file->write(generated.as_bytes());
    if (!write_result || write_result.value() != generated.size()) {
        return Error(String("Failed to write data to file"));
    }
    return Ok<>();
}

}    // namespace modus
