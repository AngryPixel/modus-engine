/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/guid.hpp>

namespace modus {

struct MaterialJson {
    struct {
        glm::vec3 diffuse = glm::vec3(1.0f);
        glm::vec3 specular = glm::vec3(0.0f);
        glm::vec3 emissive = glm::vec3(0.0f);
        f32 specular_exponent = 0.0f;
        f32 opacity = 1.0f;
    } color;

    struct {
        modus::Vector<String> diffuse;
        modus::Vector<String> specular;
        modus::Vector<String> emissive;
        modus::Vector<String> normal;
    } maps;
    GID guid;
    String name;
    bool disable_culling = false;
    bool is_transparent = false;
};

Result<MaterialJson, modus::String> material_json_from_file(const MaterialJson& src,
                                                            const modus::StringSlice file);

Result<void, modus::String> material_json_to_file(const MaterialJson& material,
                                                  const modus::StringSlice& file);

}    // namespace modus
