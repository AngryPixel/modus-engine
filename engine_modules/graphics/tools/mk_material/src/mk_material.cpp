/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/graphics/material_generated.h>

#include <core/getopt.hpp>
#include <iostream>
#include <material_json.hpp>
#include <os/file.hpp>
#include <os/guid_gen.hpp>

using namespace modus;
using namespace modus::engine;
using namespace modus::engine::graphics;

static const StringSlice kIntro =
    "Generate material file for modus engine.\n"
    "\nUsage: modus_mk_material -i input.json -o output";
int main(const int argc, const char** argv) {
    CmdOptionString opt_input("-i", "--input", "Json input file describing the material", "");
    CmdOptionString opt_output("-o", "--output", "Path to the generated material file", "");
    CmdOptionString opt_overwrite(
        "-w", "--overwrite",
        "Path to json file which an be used to override partial values in the "
        "original material file.",
        "");
    CmdOptionParser opt_parser;

    opt_parser.add(opt_input).expect("failed to add option to parser");
    opt_parser.add(opt_output).expect("failed to add option to parser");
    opt_parser.add(opt_overwrite).expect("failed to add option to parser");

    if (!opt_parser.parse(argc, argv) || !opt_input.parsed() || !opt_output.parsed()) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    MaterialJson material;

    if (auto r_load = material_json_from_file(material, opt_input.value()); !r_load) {
        std::cerr << fmt::format("Failed to load file: {}\n", opt_input.value());
        std::cerr << fmt::format("   Error: {}\n", r_load.error());
        return EXIT_FAILURE;
    } else {
        material = r_load.release_value();
    }

    if (opt_overwrite.parsed()) {
        if (auto r_load = material_json_from_file(material, opt_overwrite.value()); !r_load) {
            std::cerr << fmt::format("Failed to load ovewrite file: {}\n", opt_overwrite.value());
            std::cerr << fmt::format("   Error: {}\n", r_load.error());
            return EXIT_FAILURE;
        } else {
            material = r_load.release_value();
        }
    }

    // Fill up GUID
    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = material.guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    flatbuffers::FlatBufferBuilder builder;

    auto make_color = [](const glm::vec3& vec) -> fbs::Color {
        fbs::Color c;
        c.mutate_r(vec.r);
        c.mutate_g(vec.g);
        c.mutate_b(vec.b);
        c.mutate_a(1.0f);
        return c;
    };

    fbs::MaterialColors fbs_colors;
    // build colors
    {
        fbs::Color color_diffuse = make_color(material.color.diffuse);
        color_diffuse.mutate_a(material.color.opacity);
        fbs::Color color_specular = make_color(material.color.diffuse);
        color_specular.mutate_a(material.color.specular_exponent);
        fbs::Color color_emissive = make_color(material.color.emissive);

        fbs_colors.mutable_diffuse() = color_diffuse;
        fbs_colors.mutable_emissive() = color_emissive;
        fbs_colors.mutable_specular() = color_specular;
    }

    auto make_list = [&builder](const modus::Vector<String>& list) -> auto {
        if (list.empty()) {
            return flatbuffers::Offset<
                flatbuffers::Vector<flatbuffers::Offset<flatbuffers::String>>>();
        }
        modus::Vector<flatbuffers::Offset<flatbuffers::String>> offsets;
        offsets.reserve(list.size());
        for (auto& str : list) {
            auto fbs_str = builder.CreateString(str.c_str());
            offsets.push_back(fbs_str);
        }
        return builder.CreateVector(offsets.data(), offsets.size());
    };

    auto fbs_diffuse_list = make_list(material.maps.diffuse);
    auto fbs_specular_list = make_list(material.maps.specular);
    auto fbs_emissive_list = make_list(material.maps.emissive);
    auto fbs_normal_list = make_list(material.maps.normal);

    fbs::MaterialMapsBuilder map_builder(builder);
    map_builder.add_diffuse(fbs_diffuse_list);
    map_builder.add_specular(fbs_specular_list);
    map_builder.add_emissive(fbs_emissive_list);
    map_builder.add_normal(fbs_normal_list);
    auto fbs_maps = map_builder.Finish();

    auto fbs_name_str = builder.CreateString(material.name.c_str());
    fbs::MaterialBuilder material_builder(builder);
    material_builder.add_guid(&fbs_guid);
    material_builder.add_transparent(material.is_transparent);
    material_builder.add_disable_culling(material.disable_culling);
    material_builder.add_name(fbs_name_str);
    material_builder.add_colors(&fbs_colors);
    material_builder.add_maps(fbs_maps);
    auto fbs_material = material_builder.Finish();
    builder.Finish(fbs_material, "MMAT");

    auto r_file = os::FileBuilder().write().create().binary().open(opt_output.value_cstr());
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {} for writing.\n", opt_output.value());
        return EXIT_FAILURE;
    }
    os::File file = r_file.release_value();

    const ByteSlice data_slice(builder.GetBufferPointer(), builder.GetSize());

    auto r_write = file.write(data_slice);
    if (!r_write || r_write.value() != data_slice.size()) {
        std::cerr << fmt::format("Failed to write data to {} .\n", opt_output.value());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
