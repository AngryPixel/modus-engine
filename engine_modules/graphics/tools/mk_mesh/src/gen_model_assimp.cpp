/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include "gen_model_assimp.hpp"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <engine/graphics/mesh.hpp>

#include <material_json.hpp>

using namespace modus;
using namespace modus::engine::graphics;

#define SKIP_BINORMAL
//#define USE_U16_FOR_BONE_WEIGHTS

static constexpr u32 kInvalidBoneIndex = std::numeric_limits<u32>::max();

struct BoneInfo {
    String name;
    math::Transform bind_transform_inv;
    math::Transform bone_transform;
    u32 bone_index = kInvalidBoneIndex;
    u32 bone_index_parent = kInvalidBoneIndex;
};

struct BoneTransform {
    f32 timestamp;
    math::Transform bone_transform;
};

struct BoneKeyframe {
    u32 bone_index = kInvalidBoneIndex;
    modus::Vector<BoneTransform> bone_transforms;
};

struct AnimationInfo {
    String name;
    f32 duration_ticks;
    f32 ticks_per_second;
    modus::Vector<BoneKeyframe> bone_keyframes;
    u32 skeleton_index;
};

struct AssimpMeshInfo {
    math::bv::AABB bv_aabb;
    math::bv::Sphere bv_sphere;
    fbs::IndexType index_type = fbs::IndexType::None;
    fbs::PrimitiveType primitive_type = fbs::PrimitiveType::Triangles;
    modus::Vector<f32> data;    // interleaved buffer
    modus::Vector<u32> indices_u32;
    modus::Vector<u16> indices_u16;
    modus::Vector<u8> indices_u8;
    modus::Vector<BoneInfo> bones;
    modus::Vector<AnimationInfo> animations;
    u32 stride_bytes = 0;
    u32 stride_elements = 0;
    u32 vertex_offset = 0;
    u32 normal_offset = 0;
    u32 tex_coord_offset = 0;
    u32 tangent_offset = 0;
    u32 binormal_offset = 0;
    u32 draw_count;
    u32 material_index = 0;
    u32 bone_index_offset = 0;
    u32 bone_weight_offset = 0;
    u32 skeleton_index = 1024;
};

using AssimpMaterial = modus::MaterialJson;

static inline core::fbs::GUID to_fbs_guid(const GID& guid) {
    core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }
    return fbs_guid;
}

inline glm::vec3 to_vec3(const aiColor3D& color) {
    return glm::vec3(color.r, color.g, color.b);
}

AssimpMaterial convert_material(const aiMaterial& material) {
    AssimpMaterial mat;
    aiString material_name;

    // material name
    if (aiGetMaterialString(&material, AI_MATKEY_NAME, &material_name) == AI_SUCCESS) {
        mat.name = material_name.C_Str();
    } else {
        mat.name = "unknown";
    }

    aiColor3D color;
    // diffuse color
    if (material.Get(AI_MATKEY_COLOR_DIFFUSE, color) == AI_SUCCESS) {
        mat.color.diffuse = to_vec3(color);
    }
    // specular color
    if (material.Get(AI_MATKEY_COLOR_SPECULAR, color) == AI_SUCCESS) {
        mat.color.specular = to_vec3(color);
    }
    // emissive color
    if (material.Get(AI_MATKEY_COLOR_EMISSIVE, color) == AI_SUCCESS) {
        mat.color.emissive = to_vec3(color);
    }

    // sepcular exponent
    if (f32 shininess = 0.0f; material.Get(AI_MATKEY_SHININESS, shininess) == AI_SUCCESS) {
        mat.color.specular_exponent = shininess;
    }

    // opacity
    if (f32 opacity = 1.0f; material.Get(AI_MATKEY_OPACITY, opacity) == AI_SUCCESS) {
        mat.disable_culling = true;
        mat.is_transparent = opacity < 1.0f;
        mat.color.opacity = 1.0f;
    }

    // culling enabled/disabled
    if (i32 culling = 0; material.Get(AI_MATKEY_TWOSIDED, culling) == AI_SUCCESS) {
        mat.disable_culling = culling == 1;
    }

    // Load Textures
    auto load_textures = [](const aiMaterial& m, const aiTextureType type,
                            modus::Vector<String>& samplers) {
        for (u32 i = 0; i < m.GetTextureCount(type); ++i) {
            aiString path;
            if (m.GetTexture(type, i, &path) == AI_SUCCESS) {
                samplers.push_back(path.C_Str());
            }
        }
    };

    // Diffuse maps
    load_textures(material, aiTextureType_DIFFUSE, mat.maps.diffuse);

    // Specular maps
    load_textures(material, aiTextureType_SPECULAR, mat.maps.specular);

    // Normal maps
    load_textures(material, aiTextureType_NORMALS, mat.maps.normal);

    // Emissive maps
    load_textures(material, aiTextureType_EMISSIVE, mat.maps.emissive);
    auto r_guid = os::guid::generate();
    if (!r_guid) {
        modus_panic("Failed to generate material GUID");
    }
    mat.guid = *r_guid;
    return mat;
}
inline glm::mat4 mat4_cast(const aiMatrix4x4& m) {
    return glm::transpose(glm::make_mat4(&m.a1));
}
inline glm::vec3 vec3_cast(const aiVector3D& v) {
    return glm::vec3(v.x, v.y, v.z);
}
inline glm::quat quat_cast(const aiQuaternion& q) {
    return glm::quat(q.w, q.x, q.y, q.z);
}

Result<> process_bones(AssimpMeshInfo& info,
                       u32& skeleton_counter,
                       const aiMesh& mesh,
                       const aiScene& scene,
                       const bool verbose) {
    // Collect bones
    info.bones.resize(mesh.mNumBones);
    for (u32 i = 0; i < mesh.mNumBones; ++i) {
        const aiBone* ai_bone = mesh.mBones[i];
        BoneInfo& bone_info = info.bones[i];
        bone_info.name = ai_bone->mName.C_Str();
        /*
        glm::mat4 mat = mat4_cast(ai_bone->mOffsetMatrix);
        glm::quat rot = glm::quat_cast(mat);
        glm::vec3 pos = mat[3];
        bone_info.offset_transform.set_scale(1.0f);
        bone_info.offset_transform.set_translation(pos);
        bone_info.offset_transform.set_rotation(glm::quat(rot));
        */
        aiVector3D scale, position;
        aiQuaternion rot;
        ai_bone->mOffsetMatrix.Decompose(scale, rot, position);
        math::Transform t;
        t.set_scale(std::max(std::max(scale.x, scale.y), scale.z));
        t.set_translation(vec3_cast(position));
        t.set_rotation(quat_cast(rot));
        bone_info.bind_transform_inv = t;
    }

    auto fn_find_bone = [&info](const StringSlice name) -> Optional<u32> {
        for (usize i = 0; i < info.bones.size(); ++i) {
            if (name == info.bones[i].name) {
                return u32(i);
            }
        }
        return Optional<u32>();
    };

    // Determine hierarchy
    MODUS_UNUSED(scene);
    struct NodeInfo {
        const aiNode* node;
        u32 parent_index;
    };

    struct SortedBoneInfo {
        u32 bone_index;
        u32 parent_index;
        math::Transform bone_transform;
    };

    modus::Vector<NodeInfo> node_queue;
    modus::Vector<SortedBoneInfo> sorted_bone_list;
    sorted_bone_list.reserve(info.bones.size());
    node_queue.reserve(32);
    // Invert in case of root node transform
    node_queue.push_back({scene.mRootNode, kInvalidBoneIndex});

    // Traverse node hiearchy and locate the bones and stored them in a sorted
    // array so we can determin which is the root bone and what node is the
    // parent
    // Note: Assimp requires use to keep track of the total transform of the
    // bone in the hierarchy. So the correct location of the bone is
    // TOTAL_TRANSFORM_TO_NODE + OFFSET_TRANSFORM
    while (!node_queue.empty()) {
        NodeInfo node_info = node_queue.front();
        node_queue.erase(node_queue.begin());
        u32 parent_index = node_info.parent_index;
        auto bone_index = fn_find_bone(node_info.node->mName.C_Str());
        if (bone_index) {
            aiVector3D scale, position;
            aiQuaternion rot;
            node_info.node->mTransformation.Decompose(scale, rot, position);
            math::Transform t;
            t.set_scale(std::max(std::max(scale.x, scale.y), scale.z));
            t.set_translation(vec3_cast(position));
            t.set_rotation(quat_cast(rot));
            parent_index = sorted_bone_list.size();
            sorted_bone_list.push_back({*bone_index, node_info.parent_index, t});
        }

        for (u32 i = 0; i < node_info.node->mNumChildren; ++i) {
            node_queue.push_back({node_info.node->mChildren[i], parent_index});
        }
    }

    if (verbose) {
        std::cout << "Bone Hiearchy:\n";
    }
    for (u32 i = 0; i < sorted_bone_list.size(); ++i) {
        const auto& bone_info = sorted_bone_list[i];
        if (verbose) {
            std::cout << fmt::format("[{:03}] :: [{:03}] {} : {:03}\n", i, bone_info.bone_index,
                                     info.bones[bone_info.bone_index].name, bone_info.parent_index);
        }
        info.bones[bone_info.bone_index].bone_index = i;
        info.bones[bone_info.bone_index].bone_index_parent = bone_info.parent_index;

        info.bones[bone_info.bone_index].bone_transform = bone_info.bone_transform;
    }
    info.skeleton_index = skeleton_counter++;
    return Ok<>();
}

static BoneKeyframe& get_or_create_bone_keyframe(AnimationInfo& info, const u32 bone_idx) {
    for (auto& bt : info.bone_keyframes) {
        if (bt.bone_index == bone_idx) {
            return bt;
        }
    }
    info.bone_keyframes.push_back(BoneKeyframe());
    BoneKeyframe& bt = info.bone_keyframes.back();
    bt.bone_index = bone_idx;
    return bt;
}

static BoneTransform& get_or_create_bone_transform(BoneKeyframe& key_frame, const f32 timestamp) {
    auto it = key_frame.bone_transforms.begin();
    for (; it != key_frame.bone_transforms.end(); ++it) {
        if (math::equal(it->timestamp, timestamp)) {
            return *it;
        }
        if (timestamp < it->timestamp) {
            break;
        }
    }

    math::Transform t = math::Transform::kIdentity;
    it = key_frame.bone_transforms.insert(it, BoneTransform{timestamp, t});
    return *it;
}

Result<> process_animations(SliceMut<AssimpMeshInfo> infos,
                            const aiScene& scene,
                            const bool verbose) {
    auto fn_find_bone = [](const AssimpMeshInfo& info, const StringSlice name) -> Optional<u32> {
        for (u32 i = 0; i < info.bones.size(); ++i) {
            if (name == info.bones[i].name) {
                return i;
            }
        }
        return Optional<u32>();
    };

    for (u32 i = 0; i < scene.mNumAnimations; ++i) {
        const aiAnimation* ai_animation = scene.mAnimations[i];

        AnimationInfo animation_info;
        animation_info.name = ai_animation->mName.C_Str();
        animation_info.duration_ticks = ai_animation->mDuration;
        animation_info.ticks_per_second = ai_animation->mTicksPerSecond;
        NotMyPtr<AssimpMeshInfo> submesh_ptr;

        if (ai_animation->mNumChannels == 0) {
            continue;
        }

        // Try to find submesh to which this animations belongs to
        {
            const aiNodeAnim* ai_node_anim = ai_animation->mChannels[0];
            for (u32 z = 0; z < infos.size(); ++z) {
                auto& info = infos[z];
                if (fn_find_bone(info, ai_node_anim->mNodeName.C_Str())) {
                    submesh_ptr = &info;
                    animation_info.skeleton_index = info.skeleton_index;
                    break;
                }
            }
            if (!submesh_ptr) {
                std::cerr << fmt::format(
                    "Failed to locate submesh for bone '{}' in animation "
                    "'{}'\n",
                    ai_node_anim->mNodeName.C_Str(), ai_animation->mName.C_Str());
                return Error<>();
            }
        }

        for (u32 j = 0; j < ai_animation->mNumChannels; ++j) {
            const aiNodeAnim* ai_node_anim = ai_animation->mChannels[j];

            auto bone_idx = fn_find_bone(*submesh_ptr, ai_node_anim->mNodeName.C_Str());
            if (!bone_idx) {
                std::cerr << fmt::format("Could not locate bone '{}' for animation '{}'\n",
                                         ai_node_anim->mNodeName.C_Str(),
                                         ai_animation->mName.C_Str());
                return Error<>();
            }

            const BoneInfo& bone_info = submesh_ptr->bones[*bone_idx];
            BoneKeyframe& keyframe =
                get_or_create_bone_keyframe(animation_info, bone_info.bone_index);

            for (u32 k = 0; k < ai_node_anim->mNumPositionKeys; ++k) {
                const aiVectorKey& key = ai_node_anim->mPositionKeys[k];
                BoneTransform& bt = get_or_create_bone_transform(keyframe, f32(key.mTime));
                bt.bone_transform.set_translation(vec3_cast(key.mValue));
            }

            if (ai_node_anim->mNumRotationKeys != ai_node_anim->mNumPositionKeys) {
                if (ai_node_anim->mNumRotationKeys == 1) {
                    const aiQuatKey& key = ai_node_anim->mRotationKeys[0];
                    for (auto& bt : keyframe.bone_transforms) {
                        bt.bone_transform.set_rotation(quat_cast(key.mValue));
                    }

                } else {
                    std::cerr << "Unhandled animation conversion case\n";
                    return Error<>();
                }
            } else {
                for (u32 k = 0; k < ai_node_anim->mNumRotationKeys; ++k) {
                    const aiQuatKey& key = ai_node_anim->mRotationKeys[k];
                    BoneTransform& bt = get_or_create_bone_transform(keyframe, f32(key.mTime));
                    bt.bone_transform.set_rotation(quat_cast(key.mValue));
                }
            }

            if (ai_node_anim->mNumScalingKeys != ai_node_anim->mNumPositionKeys) {
                if (ai_node_anim->mNumScalingKeys == 1) {
                    const aiVectorKey& key = ai_node_anim->mScalingKeys[0];
                    for (auto& bt : keyframe.bone_transforms) {
                        bt.bone_transform.set_scale(
                            std::max(std::max(key.mValue.x, key.mValue.y), key.mValue.z));
                    }

                } else {
                    std::cerr << "Unhandled animation conversion case\n";
                    return Error<>();
                }
            } else {
                for (u32 k = 0; k < ai_node_anim->mNumScalingKeys; ++k) {
                    const aiVectorKey& key = ai_node_anim->mScalingKeys[k];
                    BoneTransform& bt = get_or_create_bone_transform(keyframe, f32(key.mTime));
                    bt.bone_transform.set_scale(
                        std::max(std::max(key.mValue.x, key.mValue.y), key.mValue.z));
                }
            }
        }
        submesh_ptr->animations.push_back(std::move(animation_info));
    }

    for (auto& info : infos) {
        for (auto& animation : info.animations) {
            std::sort(animation.bone_keyframes.begin(), animation.bone_keyframes.end(),
                      [](const BoneKeyframe& bkf1, const BoneKeyframe& bkf2) {
                          return bkf1.bone_index < bkf2.bone_index;
                      });
        }
    }

    if (verbose) {
        for (usize s = 0; s < infos.size(); ++s) {
            const auto& info = infos[s];
            std::cout << fmt::format("Submesh {:02} Animations: {}\n", s, info.animations.size());
            for (usize i = 0; i < info.animations.size(); ++i) {
                const auto& animation = info.animations[i];
                std::cout << fmt::format("  [{:02}] {}\n", i, animation.name);
                std::cout << fmt::format("      ticks/sec      : {} \n",
                                         animation.ticks_per_second);
                std::cout << fmt::format("      duration ticks : {}\n", animation.duration_ticks);
                std::cout << fmt::format("      BoneTransforms : {} \n",
                                         animation.bone_keyframes.size());
                for (usize j = 0; j < animation.bone_keyframes.size(); ++j) {
                    const auto& bt = animation.bone_keyframes[j];
                    std::cout << fmt::format("          [{:02}] {:02}:{:02} \n", j, bt.bone_index,
                                             bt.bone_transforms.size());
                }
            }
        }
    }
    return Ok<>();
}

Result<> process_mesh(AssimpMeshInfo& info,
                      const aiMesh& mesh,
                      const PackOptions& pack_opt,
                      const bool skip_tangent_gen) {
    // Process bone info

    struct BoneIdxAndWeight {
        u32 index;
        f32 weight;
    };

    modus::Vector<modus::Vector<BoneIdxAndWeight>> bone_and_weights;

    modus::Vector<glm::u8vec4> bone_indices;
    modus::Vector<glm::vec4> bone_weights;
    bone_indices.resize(mesh.mNumVertices, glm::u8vec4(0));
    bone_weights.resize(mesh.mNumVertices, glm::vec4(0.f));
    bone_and_weights.resize(mesh.mNumVertices);
    constexpr u32 kWeightPerVertex = 4;
    for (u32 i = 0; i < mesh.mNumBones; ++i) {
        const aiBone* bone = mesh.mBones[i];

        for (u32 j = 0; j < bone->mNumWeights; ++j) {
            const aiVertexWeight& weight = bone->mWeights[j];
            bone_and_weights[weight.mVertexId].push_back(
                BoneIdxAndWeight{info.bones[i].bone_index, weight.mWeight});
        }
    }

    for (u32 i = 0; i < bone_and_weights.size(); ++i) {
        // sort by highest to lowest weight and pick the most influential
        modus::Vector<BoneIdxAndWeight>& list = bone_and_weights[i];
        std::sort(list.begin(), list.end(),
                  [](const BoneIdxAndWeight& v1, const BoneIdxAndWeight& v2) {
                      return v1.weight > v2.weight;
                  });

        glm::u8vec4& indices = bone_indices[i];
        glm::vec4& weights = bone_weights[i];

        f32 total_weight = 0.f;
        for (u32 j = 0; j < kWeightPerVertex && j < list.size(); ++j) {
            total_weight += list[j].weight;
        }

        // adjust weight based on total
        for (u32 j = 0; j < kWeightPerVertex && j < list.size(); ++j) {
            weights[j] = std::min(list[j].weight / total_weight, 1.0f);
            indices[j] = list[j].index;
        }
    }

    for (usize i = 0; i < bone_weights.size(); ++i) {
        f32 total_weight = 0.0f;
        const glm::vec4& weight = bone_weights[i];
        for (u32 j = 0; j < 4; ++j) {
            total_weight += weight[j];
        }
        if (total_weight > 1.001f) {
            std::cerr << fmt::format(
                "Bone weights for vertex {:04} has a combined weight higher "
                "than 1.0: Total Weight = {} \n",
                i, total_weight);
            return Error<>();
        }
    }

    modus_assert(mesh.HasPositions());
    {
        u32 stride_index = 3;
        u32 offset_counter = sizeof(glm::vec3);    // vertex;
        if (mesh.HasNormals()) {
            info.normal_offset = offset_counter;
            if (pack_opt.pack_normal) {
                offset_counter += sizeof(u32);
                stride_index += 1;
            } else {
                offset_counter += sizeof(glm::vec3);
                stride_index += 3;
            }
        }
        if (mesh.HasTextureCoords(0)) {
            info.tex_coord_offset = offset_counter;
            if (pack_opt.pack_tex) {
                offset_counter += sizeof(u32);
                stride_index += 1;
            } else {
                offset_counter += sizeof(glm::vec2);
                stride_index += 2;
            }
        }
        if (mesh.HasTangentsAndBitangents() && !skip_tangent_gen) {
            if (pack_opt.pack_tb) {
                info.tangent_offset = offset_counter;
                offset_counter += sizeof(u32);

#if !defined(SKIP_BINORMAL)
                info.binormal_offset = offset_counter;
                offset_counter += sizeof(u32);
                stride_index += 2
#else
                stride_index += 1;
#endif
            } else {
                info.tangent_offset = offset_counter;
                offset_counter += sizeof(glm::vec3);
#if !defined(SKIP_BINORMAL)
                info.binormal_offset = offset_counter;
                offset_counter += sizeof(glm::vec3);
                stride_index += 6;
#else
                stride_index += 3;
#endif
            }
        }
        if (!info.bones.empty()) {
            info.bone_index_offset = offset_counter;
            offset_counter += sizeof(u32);
            info.bone_weight_offset = offset_counter;
            stride_index += 1;
#if defined(USE_U16_FOR_BONE_WEIGHTS)
            offset_counter += sizeof(u32) * 2;
            stride_index += 2;
#else
            offset_counter += sizeof(u32);
            stride_index += 1;
#endif
        };

        // end
        info.stride_bytes = offset_counter;
        info.stride_elements = stride_index;
    }
    // reserve space
    info.data.resize((info.stride_bytes * mesh.mNumVertices / sizeof(f32)), 0.f);
    u32 data_idx = 0;
    for (u32 i = 0; i < mesh.mNumVertices; ++i) {
        modus_assert(data_idx < info.data.size());
        const u32 start_data_idx = data_idx;
        // copy vertex
        {
            const aiVector3D& v = mesh.mVertices[i];
            info.data[data_idx++] = v.x;
            info.data[data_idx++] = v.y;
            info.data[data_idx++] = v.z;
        }
        // copy normal
        if (mesh.HasNormals()) {
            modus_assert(((data_idx - start_data_idx) * sizeof(f32)) == info.normal_offset);
            const aiVector3D& v = mesh.mNormals[i];
            if (pack_opt.pack_normal) {
                const f32 packed = pack_vec3_as_f32(glm::vec3(v.x, v.y, v.z));
                info.data[data_idx++] = packed;
            } else {
                info.data[data_idx++] = v.x;
                info.data[data_idx++] = v.y;
                info.data[data_idx++] = v.z;
            }
        }
        // copy tex coord
        if (mesh.HasTextureCoords(0)) {
            modus_assert(((data_idx - start_data_idx) * sizeof(f32)) == info.tex_coord_offset);
            const aiVector3D& v = mesh.mTextureCoords[0][i];
            if (pack_opt.pack_tex) {
                const f32 packed = pack_vec2_as_f32(glm::vec2(v.x, v.y));
                info.data[data_idx++] = packed;
            } else {
                info.data[data_idx++] = v.x;
                info.data[data_idx++] = v.y;
            }
        }

        // copy Tangents and Bitangents
        if (mesh.HasTangentsAndBitangents()) {
            modus_assert(((data_idx - start_data_idx) * sizeof(f32)) == info.tangent_offset);
            const aiVector3D& t = mesh.mTangents[i];
            if (pack_opt.pack_tb) {
                const f32 packed = pack_vec3_as_f32(glm::vec3(t.x, t.y, t.z));
                info.data[data_idx++] = packed;
            } else {
                info.data[data_idx++] = t.x;
                info.data[data_idx++] = t.y;
                info.data[data_idx++] = t.z;
            }

#if !defined(SKIP_BINORMAL)
            modus_assert(((data_idx - start_data_idx) * sizeof(f32)) == info.binormal_offset);
            const aiVector3D& b = mesh.mBitangents[i];
            if (pack_opt.pack_tb) {
                const f32 packed = pack_vec3_as_f32(glm::vec3(b.x, b.y, b.z));
                info.data[data_idx++] = packed;
            } else {
                info.data[data_idx++] = b.x;
                info.data[data_idx++] = b.y;
                info.data[data_idx++] = b.z;
            }
#endif
            if (!info.bones.empty()) {
                union {
                    f32 value;
                    glm::u8vec4 vec;
                } transmute;
                transmute.vec = bone_indices[i];
                info.data[data_idx++] = transmute.value;

#if defined(USE_U16_FOR_BONE_WEIGHTS)
                union {
                    f32 f[2];
                    u64 u;
                } transmute_f64;
                transmute_f64.u = math::pack_unorm_16161616(bone_weights[i]);
                info.data[data_idx++] = transmute_f64.f[0];
                info.data[data_idx++] = transmute_f64.f[1];

#else
                union {
                    f32 f;
                    u32 u;
                } transmute_f32;
                transmute_f32.u = math::pack_unorm_8888(bone_weights[i]);
                info.data[data_idx++] = transmute_f32.f;
#endif
            }
        }
        MODUS_UNUSED(start_data_idx);
        modus_assert((data_idx - start_data_idx) == info.stride_elements);
    }

    modus_assert(mesh.HasFaces());
    if (mesh.HasFaces()) {
        info.draw_count = mesh.mNumFaces * 3;
        if (u32(mesh.mNumVertices) > std::numeric_limits<u16>::max()) {
            info.index_type = fbs::IndexType::U32;
            info.indices_u32.reserve(mesh.mNumFaces * 3);
            for (u32 i = 0; i < mesh.mNumFaces; ++i) {
                const aiFace& face = mesh.mFaces[i];
                if (face.mNumIndices == 3) {
                    for (u32 j = 0; j < face.mNumIndices; ++j) {
                        info.indices_u32.push_back(face.mIndices[j]);
                    }
                } else {
                    std::cerr << fmt::format("Mesh Face {} does not have 3 indices: nIndices={}\n",
                                             i, face.mNumIndices);
                    return Error<>();
                }
            }
        } else if (u32(mesh.mNumVertices) > std::numeric_limits<u8>::max()) {
            info.indices_u16.reserve(mesh.mNumFaces * 3);
            info.index_type = fbs::IndexType::U16;
            for (u32 i = 0; i < mesh.mNumFaces; ++i) {
                const aiFace& face = mesh.mFaces[i];
                if (face.mNumIndices == 3) {
                    for (u32 j = 0; j < face.mNumIndices; ++j) {
                        if (face.mIndices[j] > std::numeric_limits<u16>::max()) {
                            std::cerr << "Incorrect assumption about index type. "
                                         "Assuming max capacity as u16, but we found "
                                         "a value bigger than u16::max()\n";
                            return Error<>();
                        }
                        info.indices_u16.push_back(u16(face.mIndices[j]));
                    }
                } else {
                    std::cerr << fmt::format("Mesh Face {} does not have 3 indices: nIndices={}\n",
                                             i, face.mNumIndices);
                    return Error<>();
                }
            }
        } else {
            info.index_type = fbs::IndexType::U8;
            info.indices_u8.reserve(mesh.mNumFaces * 3);
            for (u32 i = 0; i < mesh.mNumFaces; ++i) {
                const aiFace& face = mesh.mFaces[i];
                if (face.mNumIndices == 3) {
                    for (u32 j = 0; j < face.mNumIndices; ++j) {
                        if (face.mIndices[j] > std::numeric_limits<u8>::max()) {
                            std::cerr << "Incorrect assumption about index type. "
                                         "Assuming max capacity as u8, but we found "
                                         "a value bigger than u8::max()\n";
                            return Error<>();
                        }
                        info.indices_u8.push_back(u8(face.mIndices[j]));
                    }
                } else {
                    std::cerr << fmt::format("Mesh Face {} does not have 3 indices: nIndices={}\n",
                                             i, face.mNumIndices);
                    return Error<>();
                }
            }
        }
    }

    // Bounding volume extraction;
    const glm::vec3 aabb_max = glm::vec3(mesh.mAABB.mMax.x, mesh.mAABB.mMax.y, mesh.mAABB.mMax.z);
    const glm::vec3 aabb_min = glm::vec3(mesh.mAABB.mMin.x, mesh.mAABB.mMin.y, mesh.mAABB.mMin.z);
    info.bv_aabb = math::bv::make_aabb(aabb_max, aabb_min);
    info.bv_sphere = math::bv::make_sphere(info.bv_aabb);

    /*std::cout << fmt::format("ai > max   : {} min   :{}\n", aabb_max,
    aabb_min); std::cout << fmt::format("box> center: {} extend:{}\n",
    info.bv_aabb.center, info.bv_aabb.extends); std::cout << fmt::format("sph>
    center: {} radius:{}\n", info.bv_sphere.center, info.bv_sphere.radius);*/

    info.material_index = mesh.mMaterialIndex;

    return Ok<>();
}

static void transform_to_fbs_tramsform(math::fbs::Transform& fbs_transform,
                                       const math::Transform& t) {
    fbs_transform.mutate_scale(t.scale());
    fbs_transform.mutable_position().mutate_x(t.translation().x);
    fbs_transform.mutable_position().mutate_y(t.translation().y);
    fbs_transform.mutable_position().mutate_z(t.translation().z);
    fbs_transform.mutable_rotation().mutate_w(t.rotation().w);
    fbs_transform.mutable_rotation().mutate_x(t.rotation().x);
    fbs_transform.mutable_rotation().mutate_y(t.rotation().y);
    fbs_transform.mutable_rotation().mutate_z(t.rotation().z);
}

static flatbuffers::Offset<fbs::Drawable> create_drawable(
    flatbuffers::FlatBufferBuilder& builder,
    const AssimpMeshInfo& info,
    const u32 buffer_index,
    const Slice<AssimpMaterial> materials,
    const u32 offset,
    Optional<fbs::IndexType> index_type_overwrite,
    const PackOptions& pack_opt) {
    fbs::DrawableDesc vertex_desc;
    fbs::DrawableDesc normal_desc;
    fbs::DrawableDesc texture_desc;
    fbs::DrawableDesc tangent_desc;
    fbs::DrawableDesc bone_indices_desc;
    fbs::DrawableDesc bone_weight_desc;
#if !defined(SKIP_BINORMAL)
    fbs::DrawableDesc binormal_desc;
#endif

    fbs::DrawableDataBufferDesc data_buffer_desc;
    data_buffer_desc.mutate_offset(0);
    data_buffer_desc.mutate_stride(info.stride_bytes);
    data_buffer_desc.mutate_data_buffer_index(buffer_index);
    auto fbs_data_buffer_desc_vec = builder.CreateVectorOfStructs(&data_buffer_desc, 1);

    // Vertex
    vertex_desc.mutate_offset(info.vertex_offset);
    vertex_desc.mutate_data_type(fbs::DataType::Vec3F32);
    vertex_desc.mutate_data_buffer_index(buffer_index);

    // Normal
    normal_desc.mutate_offset(info.normal_offset);
    if (pack_opt.pack_normal) {
        normal_desc.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
        normal_desc.mutate_normalize(true);
    } else {
        normal_desc.mutate_data_type(fbs::DataType::Vec3F32);
    }
    normal_desc.mutate_data_buffer_index(buffer_index);

    // Tex coords
    texture_desc.mutate_offset(info.tex_coord_offset);
    if (pack_opt.pack_tex) {
        texture_desc.mutate_data_type(fbs::DataType::Vec2U16);
        texture_desc.mutate_normalize(true);
    } else {
        texture_desc.mutate_data_type(fbs::DataType::Vec2F32);
    }
    texture_desc.mutate_data_buffer_index(buffer_index);

    // Tangent
    tangent_desc.mutate_offset(info.tangent_offset);
    if (pack_opt.pack_tb) {
        tangent_desc.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
        tangent_desc.mutate_normalize(true);
    } else {
        tangent_desc.mutate_data_type(fbs::DataType::Vec3F32);
    }

    tangent_desc.mutate_data_buffer_index(buffer_index);

#if !defined(SKIP_BINORMAL)
    // Binormal
    binormal_desc.mutate_offset(info.binormal_offset);
    binormal_desc.mutate_stride(info.stride_bytes);
    if (pack_opt.pack_tb) {
        binormal_desc.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
        binormal_desc.mutate_normalize(true);
    } else {
        binormal_desc.mutate_data_type(fbs::DataType::Vec3F32);
    }
    binormal_desc.mutate_data_buffer_index(buffer_index);
#endif

    // bone indices
    bone_indices_desc.mutate_offset(info.bone_index_offset);
    bone_indices_desc.mutate_data_type(fbs::DataType::Vec4U8);

    // bone weights
    bone_weight_desc.mutate_offset(info.bone_weight_offset);
#if defined(USE_U16_FOR_BONE_WEIGHTS)
    bone_weight_desc.mutate_data_type(fbs::DataType::Vec4U16);
#else
    bone_weight_desc.mutate_data_type(fbs::DataType::Vec4U8);
#endif
    bone_weight_desc.mutate_normalize(true);

    fbs::DrawableBuilder drawable_builder(builder);
    drawable_builder.add_data_buffers(fbs_data_buffer_desc_vec);
    drawable_builder.add_draw_count(info.draw_count);
    drawable_builder.add_draw_start(offset);
    drawable_builder.add_index_type(index_type_overwrite.value_or(info.index_type));
    drawable_builder.add_index_buffer_index(buffer_index);
    drawable_builder.add_primitive_type(info.primitive_type);
    drawable_builder.add_vertex(&vertex_desc);

    if (info.normal_offset != 0) {
        drawable_builder.add_normals(&normal_desc);
    }

    if (info.tex_coord_offset != 0) {
        drawable_builder.add_texture(&texture_desc);
    }

    if (info.tangent_offset != 0) {
        drawable_builder.add_tangent(&tangent_desc);
    }

    if (info.bone_index_offset != 0) {
        drawable_builder.add_bone_indices(&bone_indices_desc);
        drawable_builder.add_bone_weights(&bone_weight_desc);
    }

#if !defined(SKIP_BINORMAL)
    if (info.binormal_offset != 0) {
        drawable_builder.add_binormal(&binormal_desc);
    }
#endif

    drawable_builder.add_skeleton_index(info.skeleton_index);

    MODUS_UNUSED(materials);
    modus_assert(info.material_index < materials.size());
    // If we are not using assimp's default material, decrement the index
    // so that when we apply custom material the indices are in the correct
    // order.
    const u32 index = info.material_index > 0 ? info.material_index - 1 : 0;
    drawable_builder.add_material_index(index);
    return drawable_builder.Finish();
}

static flatbuffers::Offset<fbs::Buffer> create_data_buffer(flatbuffers::FlatBufferBuilder& builder,
                                                           const AssimpMeshInfo& info) {
    const auto data_slice = make_slice(info.data);
    const ByteSlice byte_slice = data_slice.as_bytes();
    auto fbs_vector = builder.CreateVector(byte_slice.data(), byte_slice.size());
    return fbs::CreateBuffer(builder, fbs_vector);
}

static u32 merge_data_buffer(modus::Vector<u8>& output, const AssimpMeshInfo& info) {
    const auto data_slice = make_slice(info.data);
    const ByteSlice byte_slice = data_slice.as_bytes();
    output.reserve(output.size() + byte_slice.size());
    output.insert(output.end(), byte_slice.begin(), byte_slice.end());
    modus_assert(byte_slice.size() % info.stride_bytes == 0);
    return byte_slice.size() / info.stride_bytes;
}

static flatbuffers::Offset<fbs::Buffer> create_index_buffer(flatbuffers::FlatBufferBuilder& builder,
                                                            const AssimpMeshInfo& info) {
    modus_assert(info.index_type != fbs::IndexType::None);
    ByteSlice byte_slice;
    if (info.index_type == fbs::IndexType::U32) {
        modus_assert(!info.indices_u32.empty());
        const auto data_slice = make_slice(info.indices_u32);
        byte_slice = data_slice.as_bytes();
    } else if (info.index_type == fbs::IndexType::U16) {
        modus_assert(!info.indices_u16.empty());
        const auto data_slice = make_slice(info.indices_u16);
        byte_slice = data_slice.as_bytes();
    } else if (info.index_type == fbs::IndexType::U8) {
        modus_assert(!info.indices_u8.empty());
        const auto data_slice = make_slice(info.indices_u8);
        byte_slice = data_slice.as_bytes();
    } else {
        modus_panic("Unhandled index type!");
    }
    auto fbs_vector = builder.CreateVector(byte_slice.data(), byte_slice.size());
    return fbs::CreateBuffer(builder, fbs_vector);
}

static u32 merge_index_buffer(modus::Vector<u8>& output,
                              const fbs::IndexType index_type,
                              const u32 offset,
                              const AssimpMeshInfo& info) {
    modus_assert(info.index_type != fbs::IndexType::None);
    modus_assert(index_type >= info.index_type);
    u32 index_byte_size = 0;
    if (index_type == fbs::IndexType::U32) {
        modus::Vector<u32> tmp;
        index_byte_size = 4;
        if (info.index_type == fbs::IndexType::U32) {
            tmp.reserve(info.indices_u32.size());
            for (u32 idx : info.indices_u32) {
                tmp.push_back(offset + idx);
            }
        } else if (info.index_type == fbs::IndexType::U16) {
            tmp.reserve(info.indices_u16.size());
            for (u16 idx : info.indices_u16) {
                tmp.push_back(offset + idx);
            }
        } else if (info.index_type == fbs::IndexType::U8) {
            tmp.reserve(info.indices_u8.size());
            for (u8 idx : info.indices_u8) {
                tmp.push_back(offset + idx);
            }
        } else {
            modus_panic("Should not be reached");
        }
        const ByteSlice bytes = make_slice(tmp).as_bytes();
        output.reserve(output.size() + bytes.size());
        output.insert(output.end(), bytes.begin(), bytes.end());
    } else if (index_type == fbs::IndexType::U16) {
        modus::Vector<u16> tmp;
        index_byte_size = 2;
        if (info.index_type == fbs::IndexType::U16) {
            tmp.reserve(info.indices_u16.size());
            for (u16 idx : info.indices_u16) {
                tmp.push_back(offset + idx);
            }
        } else if (info.index_type == fbs::IndexType::U8) {
            tmp.reserve(info.indices_u8.size());
            for (u8 idx : info.indices_u8) {
                tmp.push_back(offset + idx);
            }
        } else {
            modus_panic("Should not be reached");
        }
        const ByteSlice bytes = make_slice(tmp).as_bytes();
        output.reserve(output.size() + bytes.size());
        output.insert(output.end(), bytes.begin(), bytes.end());
    } else if (index_type == fbs::IndexType::U8) {
        modus::Vector<u8> tmp;
        index_byte_size = 1;
        if (info.index_type == fbs::IndexType::U8) {
            tmp.reserve(info.indices_u8.size());
            for (u8 idx : info.indices_u8) {
                tmp.push_back(offset + idx);
            }
        } else {
            modus_panic("Should not be reached");
        }
        const ByteSlice bytes = make_slice(tmp).as_bytes();
        output.reserve(output.size() + bytes.size());
        output.insert(output.end(), bytes.begin(), bytes.end());
    } else {
        modus_panic("Unhandled index type!");
    }
    return info.draw_count * index_byte_size;
}

void print_node_info(const aiNode* node, const u32 level, const u32 child_num) {
    const aiMatrix4x4 ai_transform = node->mTransformation;

    aiVector3D scaling;
    aiQuaternion rotation;
    aiVector3D position;
    ai_transform.Decompose(scaling, rotation, position);
    std::cout << fmt::format("{:2}-{:2}-scale: x:{} y:{} z:{}\n", level, child_num, scaling.x,
                             scaling.y, scaling.z);
    std::cout << fmt::format("{:2}-{:2}-rot  : x:{} y:{} z:{} w:{}\n", level, child_num, rotation.x,
                             rotation.y, rotation.z, rotation.w);
    std::cout << fmt::format("{:2}-{:2}-pos  : x:{} y:{} z:{}\n", level, child_num, position.x,
                             position.y, position.z);
    for (u32 i = 0; i < node->mNumChildren; ++i) {
        const aiNode* child = node->mChildren[i];
        print_node_info(child, level + 1, child_num + 1);
    }
}

static flatbuffers::Offset<engine::animation::fbs::Skeleton> build_fbs_skeleton(
    flatbuffers::FlatBufferBuilder& builder,
    const modus::Vector<BoneInfo>& bones) {
    modus::Vector<flatbuffers::Offset<engine::animation::fbs::Bone>> bone_vector;
    bone_vector.resize(bones.size());
    for (const auto& bone : bones) {
        auto fbs_string = builder.CreateString(bone.name.c_str(), bone.name.size());
        engine::animation::fbs::BoneBuilder bone_builder(builder);
        math::fbs::Transform fbs_inv_bind_transform;
        math::fbs::Transform fbs_bone_transform_total;
        transform_to_fbs_tramsform(fbs_inv_bind_transform, bone.bind_transform_inv);
        transform_to_fbs_tramsform(fbs_bone_transform_total, bone.bone_transform);
        bone_builder.add_name(fbs_string);
        bone_builder.add_parent_index(bone.bone_index_parent);
        bone_builder.add_inv_bind_transform(&fbs_inv_bind_transform);
        bone_builder.add_bone_transform(&fbs_bone_transform_total);
        auto fbs_bone = bone_builder.Finish();
        bone_vector[bone.bone_index] = fbs_bone;
    }
    auto fbs_vector = builder.CreateVector(bone_vector.data(), bone_vector.size());
    return engine::animation::fbs::CreateSkeleton(builder, fbs_vector);
}

static flatbuffers::Offset<engine::animation::fbs::SkeletonAnimation> build_fbs_animation(
    flatbuffers::FlatBufferBuilder& builder,
    const AnimationInfo& animation) {
    modus::Vector<flatbuffers::Offset<engine::animation::fbs::BoneKeyframe>> fbs_bone_keyframes;
    fbs_bone_keyframes.reserve(animation.bone_keyframes.size());
    for (auto& bkf : animation.bone_keyframes) {
        modus::Vector<flatbuffers::Offset<engine::animation::fbs::BoneTransform>>
            fbs_bone_transforms;
        fbs_bone_transforms.reserve(bkf.bone_transforms.size());
        for (auto& bt : bkf.bone_transforms) {
            engine::animation::fbs::BoneTransformBuilder bt_builder(builder);
            math::fbs::Transform fbs_transform;
            transform_to_fbs_tramsform(fbs_transform, bt.bone_transform);
            bt_builder.add_timestamp(bt.timestamp);
            bt_builder.add_bone_transform(&fbs_transform);
            auto fbs_bt = bt_builder.Finish();
            fbs_bone_transforms.push_back(fbs_bt);
        }

        auto fbs_bt_vec =
            builder.CreateVector(fbs_bone_transforms.data(), fbs_bone_transforms.size());
        engine::animation::fbs::BoneKeyframeBuilder fbs_bkf_builder(builder);
        fbs_bkf_builder.add_bone_index(bkf.bone_index);
        fbs_bkf_builder.add_bone_transforms(fbs_bt_vec);
        auto fbs_bkf = fbs_bkf_builder.Finish();
        fbs_bone_keyframes.push_back(fbs_bkf);
    }

    auto fbs_string = builder.CreateString(animation.name.c_str());
    auto fbs_bkf_vec = builder.CreateVector(fbs_bone_keyframes.data(), fbs_bone_keyframes.size());
    engine::animation::fbs::SkeletonAnimationBuilder anim_builder(builder);
    anim_builder.add_name(fbs_string);
    anim_builder.add_bone_keyframes(fbs_bkf_vec);
    anim_builder.add_duration_sec(animation.duration_ticks * animation.ticks_per_second);
    anim_builder.add_skeleton_index(animation.skeleton_index);
    return anim_builder.Finish();
}

static flatbuffers::Offset<engine::animation::fbs::AnimationCatalog> build_fbs_animation_catalog(
    flatbuffers::FlatBufferBuilder& builder,
    const Slice<AssimpMeshInfo> mesh_info,
    const u32 skeleton_count) {
    if (skeleton_count == 0) {
        return flatbuffers::Offset<engine::animation::fbs::AnimationCatalog>();
    }

    // create animations
    flatbuffers::Offset<
        flatbuffers::Vector<flatbuffers::Offset<engine::animation::fbs::SkeletonAnimation>>>
        fbs_anim_vec;
    modus::Vector<flatbuffers::Offset<engine::animation::fbs::SkeletonAnimation>> fbs_animations;
    modus::Vector<flatbuffers::Offset<engine::animation::fbs::Skeleton>> fbs_skeletons;
    fbs_skeletons.resize(skeleton_count);
    for (const auto& info : mesh_info) {
        if (info.bones.empty()) {
            continue;
        }

        auto fbs_skeleton = build_fbs_skeleton(builder, info.bones);
        fbs_skeletons[info.skeleton_index] = fbs_skeleton;
        for (const auto& animation : info.animations) {
            fbs_animations.push_back(build_fbs_animation(builder, animation));
        }
    }

    if (fbs_animations.empty()) {
        return flatbuffers::Offset<engine::animation::fbs::AnimationCatalog>();
    }
    fbs_anim_vec = builder.CreateVector(fbs_animations.data(), fbs_animations.size());

    auto fbs_skeleton_vec = builder.CreateVector(fbs_skeletons.data(), fbs_skeletons.size());

    engine::animation::fbs::AnimationCatalogBuilder catalog_builder(builder);
    catalog_builder.add_skeletons(fbs_skeleton_vec);
    catalog_builder.add_skeleton_animations(fbs_anim_vec);
    return catalog_builder.Finish();
}

Result<flatbuffers::Offset<fbs::Mesh>, void> gen_model_assimp(
    flatbuffers::FlatBufferBuilder& builder,
    const GID& guid,
    const f32 model_scale,
    const AssimpOptions& options) {
    MODUS_UNUSED(builder);
    MODUS_UNUSED(guid);
    modus_assert(options.input_file != nullptr);

    Assimp::Importer importer;

    // Apply scale to model
    importer.SetPropertyFloat(AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, model_scale);

    // Generate bounding box
    unsigned int assimp_flags = aiProcess_GenBoundingBoxes;

    // Apply global scale value
    assimp_flags |= aiProcess_GlobalScale;

    // Ensure we always get triangles to draw
    assimp_flags |= aiProcess_Triangulate;

    // Generate smooth normals if none are defined
    assimp_flags |= aiProcess_GenNormals;

    if (!options.skip_tangent_gen) {
        // Calculate TangentSpace
        assimp_flags |= aiProcess_CalcTangentSpace;
    }

    // Gen UV if missing
    assimp_flags |= aiProcess_GenUVCoords;

    if (!options.disable_optimizations) {
        // Reduce draw calls (indexing)
        assimp_flags |= aiProcess_JoinIdenticalVertices;

        // Exclude lines and points
        assimp_flags |= aiProcess_SortByPType;

        // Improve cache locality
        assimp_flags |= aiProcess_ImproveCacheLocality;

        // Fix invalid data
        assimp_flags |= aiProcess_FindInvalidData;

        // Reduce mesh duplication
        assimp_flags |= aiProcess_FindInstances;

        // Optimize mesh
        assimp_flags |= aiProcess_OptimizeMeshes;
    }

    if (!options.disable_optimizations && !options.disable_optimize_graph) {
        assimp_flags |= aiProcess_OptimizeGraph;
    }

    // Verify Model
    assimp_flags |= aiProcess_ValidateDataStructure;

    const aiScene* scene = importer.ReadFile(options.input_file, assimp_flags);
    if (scene == nullptr) {
        std::cerr << fmt::format("Failed to read model: {}\n", options.input_file);
        return Error<>();
    }

    if (scene->mNumMeshes > Mesh::kMaxSubMeshCount) {
        std::cerr << fmt::format(
            "Modus engine can only handle up to {} submesh(es) per model at "
            "the moment. {} were detected.\n",
            Mesh::kMaxSubMeshCount, scene->mNumMeshes);
        return Error<>();
    }

    // Process materials
    modus::Vector<AssimpMaterial> mesh_materials;
    std::set<u32> unique_material_indices;
    if (!options.material_file_output.is_empty()) {
        mesh_materials.resize(scene->mNumMaterials);
        for (u32 i = 0; i < scene->mNumMaterials; ++i) {
            mesh_materials[i] = convert_material(*scene->mMaterials[i]);
        }

        if (options.verbose) {
            std::cout << "\n";
            for (u32 i = 0; i < scene->mNumMaterials; ++i) {
                const AssimpMaterial& mat = mesh_materials[i];
                std::cout << fmt::format("Material [{:02}]\n", i);
                std::cout << fmt::format("    guid            : {}\n", mat.guid);
                std::cout << fmt::format("    name            : {}\n", mat.name);
                std::cout << fmt::format("    transparent     : {}\n", mat.is_transparent);
                std::cout << fmt::format("    disable_culling : {}\n", mat.disable_culling);
                std::cout << fmt::format("    color:\n");
                std::cout << fmt::format("        diffuse     : {}\n", mat.color.diffuse);
                std::cout << fmt::format("        specular    : {}\n", mat.color.specular);
                std::cout << fmt::format("        specular_exp: {}\n", mat.color.specular_exponent);
                std::cout << fmt::format("        emissive    : {}\n", mat.color.emissive);
                std::cout << fmt::format("        opacity     : {}\n", mat.color.opacity);
                std::cout << fmt::format("    maps:\n");

                auto print_textures = [](const StringSlice& header,
                                         const modus::Vector<String>& samplers) {
                    std::cout << fmt::format("        {}:\n", header);
                    for (usize t = 0; t < samplers.size(); ++t) {
                        std::cout << fmt::format("            [{:02}] {}\n", t, samplers[t]);
                    }
                };

                print_textures("diffuse ", mat.maps.diffuse);
                print_textures("specular", mat.maps.specular);
                print_textures("normal  ", mat.maps.normal);
                print_textures("emissive", mat.maps.emissive);
            }
            std::cout << "\n";
        }
    }

    // Generate data
    /* const aiNode* root_node = scene->mRootNode;
     if (root_node != nullptr) {
         print_node_info(root_node, 0, 0);
     }*/
    u32 skeleton_counter = 0;
    Array<AssimpMeshInfo, Mesh::kMaxSubMeshCount> mesh_info;
    for (u32 m = 0; m < scene->mNumMeshes; ++m) {
        const aiMesh* mesh = scene->mMeshes[m];

        for (u32 i = 1; i < AI_MAX_NUMBER_OF_TEXTURECOORDS; ++i) {
            if (mesh->HasTextureCoords(i)) {
                std::cerr << fmt::format(
                    "Mesh has more than one set of texture coordinates. This "
                    "is currently not supported\n");
                return Error<>();
            }
        }

        if (!process_bones(mesh_info[m], skeleton_counter, *mesh, *scene, options.verbose)) {
            std::cerr << fmt::format("Failed to process bones for sub mesh {}\n", m);
            return Error<>();
        }

        if (!process_mesh(mesh_info[m], *mesh, options.pack_opt, options.skip_tangent_gen)) {
            std::cerr << fmt::format("Failed to process sub mesh {}\n", m);
            return Error<>();
        }

        unique_material_indices.insert(mesh_info[m].material_index);
    }

    if (!process_animations(SliceMut(mesh_info.data(), scene->mNumMeshes), *scene,
                            options.verbose)) {
        std::cerr << fmt::format("Failed to process animations\n");
        return Error<>();
    }

    if (!options.material_file_output.is_empty()) {
        for (auto& idx : unique_material_indices) {
            const AssimpMaterial& mat = mesh_materials[idx];
            String output = options.material_file_output.to_str();
            os::Path::join_inplace(output, "material_");
            output += mat.name;
            output += ".json";

            if (options.verbose) {
                std::cout << fmt::format("Writing material [{:02}] to {}\n", idx, output);
            }

            if (!material_json_to_file(mat, output)) {
                std::cerr << fmt::format("Failed to write material file {}\n", output);
                return Error<>();
            }
        }
    }

    // Calculate BV
    math::bv::Sphere bv_sphere;
    math::bv::AABB bv_aabb;
    bv_sphere = mesh_info[0].bv_sphere;
    bv_aabb = mesh_info[0].bv_aabb;
    for (u32 m = 1; m < scene->mNumMeshes; ++m) {
        math::bv::merge(bv_sphere, mesh_info[m].bv_sphere);
        math::bv::merge(bv_aabb, mesh_info[m].bv_aabb);
    }

    // TODO: this probably breaks multi-mesh setups!!
    // Center model on y = 0;
    const f32 y_translation = bv_sphere.center.y > 0.0f ? -bv_sphere.center.y : bv_sphere.center.y;
    bv_sphere.center.y += y_translation;
    for (u32 m = 0; m < scene->mNumMeshes; ++m) {
        AssimpMeshInfo& info = mesh_info[m];
        usize index = 0;
        for (; index < info.data.size(); index += info.stride_elements) {
            // Apply translation to y component
            info.data[index + info.vertex_offset + 1] += y_translation;
        }
        modus_assert(index == info.data.size());
    }

    // Convert to flabuffer representation
    Array<flatbuffers::Offset<fbs::Drawable>, Mesh::kMaxSubMeshCount> fbs_drawables;
    Array<flatbuffers::Offset<fbs::Buffer>, Mesh::kMaxSubMeshCount> fbs_data_buffers;
    Array<flatbuffers::Offset<fbs::Buffer>, Mesh::kMaxSubMeshCount> fbs_index_buffers;
    modus::Vector<u8> combined_vertex_data;
    modus::Vector<u8> combined_index_data;

    u32 mesh_buffer_count = 0;
    if (!options.submeshes_share_buffer) {
        mesh_buffer_count = scene->mNumMeshes;
        for (u32 m = 0; m < scene->mNumMeshes; ++m) {
            fbs_data_buffers[m] = create_data_buffer(builder, mesh_info[m]);
            fbs_index_buffers[m] = create_index_buffer(builder, mesh_info[m]);
            fbs_drawables[m] = create_drawable(builder, mesh_info[m], m, make_slice(mesh_materials),
                                               0, Optional<fbs::IndexType>(), options.pack_opt);
        }
    } else {
        mesh_buffer_count = 1;
        fbs::IndexType index_type = fbs::IndexType::U8;
        u32 total_draw_count = 0;

        // determine highest index_type
        for (u32 m = 0; m < scene->mNumMeshes; ++m) {
            total_draw_count += mesh_info[m].draw_count;
        }

        if (total_draw_count <= std::numeric_limits<u8>::max()) {
            index_type = fbs::IndexType::U8;
        } else if (total_draw_count <= std::numeric_limits<u16>::max()) {
            index_type = fbs::IndexType::U16;
        } else {
            index_type = fbs::IndexType::U32;
        }

        u32 index_offset = 0;
        u32 buffer_offset = 0;
        for (u32 m = 0; m < scene->mNumMeshes; ++m) {
            const u32 element_count = merge_data_buffer(combined_vertex_data, mesh_info[m]);
            const u32 new_index_offset =
                merge_index_buffer(combined_index_data, index_type, buffer_offset, mesh_info[m]);
            fbs_drawables[m] = create_drawable(builder, mesh_info[m], 0, make_slice(mesh_materials),
                                               index_offset, index_type, options.pack_opt);
            index_offset += new_index_offset;
            buffer_offset += element_count;
        }
        {
            auto fbs_vector =
                builder.CreateVector(combined_vertex_data.data(), combined_vertex_data.size());
            fbs_data_buffers[0] = fbs::CreateBuffer(builder, fbs_vector);
        }
        {
            auto fbs_vector =
                builder.CreateVector(combined_index_data.data(), combined_index_data.size());
            fbs_index_buffers[0] = fbs::CreateBuffer(builder, fbs_vector);
        }
    }

    auto fbs_data_buffers_vec = builder.CreateVector(fbs_data_buffers.data(), mesh_buffer_count);
    auto fbs_index_buffers_vec = builder.CreateVector(fbs_index_buffers.data(), mesh_buffer_count);
    auto fbs_submeshes_vec = builder.CreateVector(fbs_drawables.data(), scene->mNumMeshes);

    math::fbs::Vec3 sphere_center(bv_sphere.center.x, bv_sphere.center.y, bv_sphere.center.z);
    math::fbs::Vec3 aabb_center(bv_aabb.center.x, bv_aabb.center.y, bv_aabb.center.z);
    math::fbs::Vec3 aabb_extend(bv_aabb.extends.x, bv_aabb.extends.y, bv_aabb.extends.z);
    fbs::BoundingVolumeSphere fbs_bv_sphere(sphere_center, bv_sphere.radius);
    fbs::BoundingVolumeAABB fbs_bv_aabb(aabb_center, aabb_extend);
    auto fbs_bv = fbs::BoundingVolume(fbs_bv_sphere, fbs_bv_aabb);

    modus::core::fbs::GUID fbs_guid = to_fbs_guid(guid);
    auto fbs_anim_catalog = build_fbs_animation_catalog(
        builder, Slice(mesh_info.data(), scene->mNumMeshes), skeleton_counter);

    fbs::MeshBuilder mesh_builder(builder);
    mesh_builder.add_guid(&fbs_guid);
    mesh_builder.add_bounding_volume(&fbs_bv);
    mesh_builder.add_data_buffers(fbs_data_buffers_vec);
    mesh_builder.add_index_buffers(fbs_index_buffers_vec);
    mesh_builder.add_submeshes(fbs_submeshes_vec);
    mesh_builder.add_animation_catalog(fbs_anim_catalog);

    // Finalize
    return Ok(mesh_builder.Finish());
}
