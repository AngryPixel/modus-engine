/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include "gen_model_sphere.hpp"

using namespace modus;
using namespace modus::engine::graphics;

flatbuffers::Offset<fbs::Mesh> gen_sphere(flatbuffers::FlatBufferBuilder& builder,
                                          const GID& guid,
                                          const f32 radius,
                                          const u32 sectors,
                                          const u32 stacks,
                                          const PackOptions& pack_opt) {
    const f32 pi = 3.1415926f;
    const u32 stack_count = std::max(u32(3), stacks);
    const u32 sector_count = std::max(u32(3), sectors);
    modus::Vector<VertexData> data;
    modus::Vector<u16> indices;

    // taken from https://www.songho.ca/opengl/gl_sphere.html
    // Gen sphere
    f32 x, y, z, xy;                              // vertex position
    f32 nx, ny, nz, lengthInv = 1.0f / radius;    // vertex normal
    f32 s, t;                                     // vertex texCoord

    const f32 sector_step = 2 * pi / sector_count;
    const f32 stack_step = pi / stack_count;
    f32 sector_angle, stack_angle;

    data.resize((stack_count + 1) * (sector_count + 1));
    auto vd_it = data.begin();
    for (u32 i = 0; i <= stack_count; ++i) {
        stack_angle = pi / 2.0f - f32(i) * stack_step;    // starting from pi/2 to -pi/2
        xy = radius * cosf(stack_angle);                  // r * cos(u)
        z = radius * sinf(stack_angle);                   // r * sin(u)

        // add (sector_count+1) vertices per stack
        // the first and last vertices have same position and normal, but
        // different tex coords
        for (u32 j = 0; j <= sector_count; ++j) {
            VertexData& vd = *vd_it;
            sector_angle = f32(j) * sector_step;    // starting from 0 to 2pi

            // vertex position (x, y, z)
            x = xy * cosf(sector_angle);    // r * cos(u) * cos(v)
            y = xy * sinf(sector_angle);    // r * cos(u) * sin(v)

            vd.vertex.x = x;
            vd.vertex.y = y;
            vd.vertex.z = z;

            // normalized vertex normal (nx, ny, nz)
            nx = x * lengthInv;
            ny = y * lengthInv;
            nz = z * lengthInv;

            vd.normal.x = nx;
            vd.normal.y = ny;
            vd.normal.z = nz;

            // vertex tex coord (s, t) range between [0, 1]
            s = f32(j) / sector_count;
            t = f32(i) / stack_count;

            vd.texture.x = s;
            vd.texture.y = t;
            ++vd_it;
        }
    }

    modus_assert(data.size() < usize(std::numeric_limits<u16>::max()));

    // gen indices
    indices.reserve(sector_count * stack_count * 6);
    u16 k1, k2;
    for (u16 i = 0; i < u16(stack_count); ++i) {
        k1 = i * u16(sector_count + 1);     // beginning of current stack
        k2 = k1 + u16(sector_count) + 1;    // beginning of next stack

        for (u16 j = 0; j < u16(sector_count); ++j, ++k1, ++k2) {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if (i != 0) {
                indices.push_back(k1);
                indices.push_back(k2);
                indices.push_back(k1 + 1);
            }
            // k1+1 => k2 => k2+1
            if (i != (stack_count - 1)) {
                indices.push_back(k1 + 1);
                indices.push_back(k2);
                indices.push_back(k2 + 1);
            }
        }
    }

    math::bv::AABB aabb;
    math::bv::Sphere sphere;
    sphere.center = glm::vec3(0.0f);
    sphere.radius = radius;
    aabb.center = glm::vec3(0.0f);
    aabb.extends = glm::vec3(radius);

    math::fbs::Vec3 sphere_center(sphere.center.x, sphere.center.y, sphere.center.z);
    math::fbs::Vec3 aabb_center(aabb.center.x, aabb.center.y, aabb.center.z);
    math::fbs::Vec3 aabb_extend(aabb.extends.x, aabb.extends.y, aabb.extends.z);
    fbs::BoundingVolumeSphere fbs_bv_sphere(sphere_center, sphere.radius);
    fbs::BoundingVolumeAABB fbs_bv_aabb(aabb_center, aabb_extend);
    auto fbs_bv = fbs::BoundingVolume(fbs_bv_sphere, fbs_bv_aabb);

    const fbs::IndexType index_type = fbs::IndexType::U16;
    const fbs::PrimitiveType primitive_type = fbs::PrimitiveType::Triangles;

    ByteSlice data_bytes;

    if (pack_opt.pack_normal && pack_opt.pack_tex) {
        auto packed_data = pack_all(make_slice(data));
        const auto data_slice = Slice<VertexDataPackedAll>(packed_data.data(), packed_data.size());
        data_bytes = data_slice.as_bytes();
    } else if (pack_opt.pack_tex) {
        auto packed_data = pack_texture(make_slice(data));
        const auto data_slice =
            Slice<VertexDataPackedTexture>(packed_data.data(), packed_data.size());
        data_bytes = data_slice.as_bytes();
    } else if (pack_opt.pack_normal) {
        auto packed_data = pack_normal(make_slice(data));
        const auto data_slice =
            Slice<VertexDataPackedNormal>(packed_data.data(), packed_data.size());
        data_bytes = data_slice.as_bytes();
    } else {
        const auto data_slice = Slice<VertexData>(data.data(), data.size());
        data_bytes = data_slice.as_bytes();
    }

    // Create Vertex buffer
    auto fbs_buffer_data = builder.CreateVector<u8>(data_bytes.data(), data_bytes.size());
    auto fbs_data_buffer = fbs::CreateBuffer(builder, fbs_buffer_data);

    // [Buffer]
    auto fbs_data_vector = builder.CreateVector(&fbs_data_buffer, 1);

    // Create Index Buffer
    const Slice<u16> index_slice = Slice<u16>(indices.data(), indices.size());
    const ByteSlice index_bytes = index_slice.as_bytes();

    auto fbs_index_data = builder.CreateVector<u8>(index_bytes.data(), index_bytes.size());
    auto fbs_index_buffer = fbs::CreateBuffer(builder, fbs_index_data);

    auto fbs_index_vector = builder.CreateVector(&fbs_index_buffer, 1);

    // Create Drawable info
    const u32 buffer_index = 0;
    u32 stride = 0;
    if (pack_opt.pack_normal && pack_opt.pack_tex) {
        stride = sizeof(f32) * 5;
    } else if (pack_opt.pack_tex) {
        stride = sizeof(f32) * 7;
    } else if (pack_opt.pack_normal) {
        stride = sizeof(f32) * 6;
    } else {
        stride = sizeof(f32) * 8;
    }

    fbs::DrawableDataBufferDesc data_buffer_desc;
    data_buffer_desc.mutate_offset(0);
    data_buffer_desc.mutate_stride(stride);
    data_buffer_desc.mutate_data_buffer_index(0);
    auto fbs_data_buffer_desc_vec = builder.CreateVectorOfStructs(&data_buffer_desc, 1);

    fbs::DrawableDesc vertex_info;
    vertex_info.mutate_offset(0);
    vertex_info.mutate_data_type(fbs::DataType::Vec3F32);
    vertex_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableDesc normal_info;
    normal_info.mutate_offset(sizeof(glm::vec3));
    if (!pack_opt.pack_normal) {
        normal_info.mutate_data_type(fbs::DataType::Vec3F32);
    } else {
        normal_info.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
        normal_info.mutate_normalize(true);
    }
    normal_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableDesc texture_info;
    if (!pack_opt.pack_normal) {
        texture_info.mutate_offset(sizeof(glm::vec3) * 2);
    } else {
        texture_info.mutate_offset(sizeof(f32) * 4);
    }
    if (!pack_opt.pack_tex) {
        texture_info.mutate_data_type(fbs::DataType::Vec2F32);
    } else {
        texture_info.mutate_data_type(fbs::DataType::Vec2U16);
        texture_info.mutate_normalize(true);
    }
    texture_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableBuilder drawable_builder(builder);
    drawable_builder.add_draw_count(indices.size());
    drawable_builder.add_data_buffers(fbs_data_buffer_desc_vec);
    drawable_builder.add_draw_start(0);
    drawable_builder.add_index_type(index_type);
    drawable_builder.add_primitive_type(primitive_type);
    drawable_builder.add_vertex(&vertex_info);
    drawable_builder.add_normals(&normal_info);
    drawable_builder.add_texture(&texture_info);

    auto fbs_drawable = drawable_builder.Finish();
    auto fbs_drawable_vec = builder.CreateVector(&fbs_drawable, 1);

    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    // Creat Mesh
    fbs::MeshBuilder mesh_builder(builder);
    mesh_builder.add_guid(&fbs_guid);
    mesh_builder.add_bounding_volume(&fbs_bv);
    mesh_builder.add_data_buffers(fbs_data_vector);
    mesh_builder.add_index_buffers(fbs_index_vector);
    mesh_builder.add_submeshes(fbs_drawable_vec);
    return mesh_builder.Finish();
}
