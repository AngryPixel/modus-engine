/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include "pack_utils.hpp"
#pragma once

struct AssimpOptions {
    const char* input_file = nullptr;
    bool disable_optimize_graph = false;
    bool disable_optimizations = false;
    bool verbose = false;
    bool submeshes_share_buffer = true;
    bool skip_tangent_gen = false;
    PackOptions pack_opt;
    StringSlice material_file_output;
};

Result<flatbuffers::Offset<fbs::Mesh>, void> gen_model_assimp(
    flatbuffers::FlatBufferBuilder& builder,
    const GID& guid,
    const f32 scale,
    const AssimpOptions& options);
