/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

struct GenPlaneParams {
    f32 xy_width = 1.0f;
    f32 xy_height = 1.0f;
    f32 xz_width = 1.0f;
    f32 xz_height = 1.0f;
    f32 yz_width = 1.0f;
    f32 yz_height = 1.0f;
    bool xy = false;
    bool xz = false;
    bool yz = false;
};

flatbuffers::Offset<fbs::Mesh> gen_plane(flatbuffers::FlatBufferBuilder& builder,
                                         const GID& guid,
                                         const GenPlaneParams& params);
