/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include "gen_model_cube.hpp"

flatbuffers::Offset<fbs::Mesh> gen_cube(flatbuffers::FlatBufferBuilder& builder,
                                        const GID& guid,
                                        const f32 radius,
                                        const PackOptions& pack_opt) {
    const float vertices[] = {
        // positions          // normals           // texture coords
        radius,  radius,  -radius, 0.0f,    0.0f,    -1.0f,   1.0f,    1.0f,    radius,  -radius,
        -radius, 0.0f,    0.0f,    -1.0f,   1.0f,    0.0f,    -radius, -radius, -radius, 0.0f,
        0.0f,    -1.0f,   0.0f,    0.0f,    -radius, -radius, -radius, 0.0f,    0.0f,    -1.0f,
        0.0f,    0.0f,    -radius, radius,  -radius, 0.0f,    0.0f,    -1.0f,   0.0f,    1.0f,
        radius,  radius,  -radius, 0.0f,    0.0f,    -1.0f,   1.0f,    1.0f,

        -radius, -radius, radius,  0.0f,    0.0f,    1.0f,    0.0f,    0.0f,    radius,  -radius,
        radius,  0.0f,    0.0f,    1.0f,    1.0f,    0.0f,    radius,  radius,  radius,  0.0f,
        0.0f,    1.0f,    1.0f,    1.0f,    radius,  radius,  radius,  0.0f,    0.0f,    1.0f,
        1.0f,    1.0f,    -radius, radius,  radius,  0.0f,    0.0f,    1.0f,    0.0f,    1.0f,
        -radius, -radius, radius,  0.0f,    0.0f,    1.0f,    0.0f,    0.0f,

        -radius, radius,  radius,  -1.0f,   0.0f,    0.0f,    1.0f,    0.0f,    -radius, radius,
        -radius, -1.0f,   0.0f,    0.0f,    1.0f,    1.0f,    -radius, -radius, -radius, -1.0f,
        0.0f,    0.0f,    0.0f,    1.0f,    -radius, -radius, -radius, -1.0f,   0.0f,    0.0f,
        0.0f,    1.0f,    -radius, -radius, radius,  -1.0f,   0.0f,    0.0f,    0.0f,    0.0f,
        -radius, radius,  radius,  -1.0f,   0.0f,    0.0f,    1.0f,    0.0f,

        radius,  -radius, -radius, 1.0f,    0.0f,    0.0f,    0.0f,    1.0f,    radius,  radius,
        -radius, 1.0f,    0.0f,    0.0f,    1.0f,    1.0f,    radius,  radius,  radius,  1.0f,
        0.0f,    0.0f,    1.0f,    0.0f,    radius,  radius,  radius,  1.0f,    0.0f,    0.0f,
        1.0f,    0.0f,    radius,  -radius, radius,  1.0f,    0.0f,    0.0f,    0.0f,    0.0f,
        radius,  -radius, -radius, 1.0f,    0.0f,    0.0f,    0.0f,    1.0f,

        -radius, -radius, -radius, 0.0f,    -1.0f,   0.0f,    0.0f,    1.0f,    radius,  -radius,
        -radius, 0.0f,    -1.0f,   0.0f,    1.0f,    1.0f,    radius,  -radius, radius,  0.0f,
        -1.0f,   0.0f,    1.0f,    0.0f,    radius,  -radius, radius,  0.0f,    -1.0f,   0.0f,
        1.0f,    0.0f,    -radius, -radius, radius,  0.0f,    -1.0f,   0.0f,    0.0f,    0.0f,
        -radius, -radius, -radius, 0.0f,    -1.0f,   0.0f,    0.0f,    1.0f,

        radius,  radius,  radius,  0.0f,    1.0f,    0.0f,    1.0f,    0.0f,    radius,  radius,
        -radius, 0.0f,    1.0f,    0.0f,    1.0f,    1.0f,    -radius, radius,  -radius, 0.0f,
        1.0f,    0.0f,    0.0f,    1.0f,    -radius, radius,  -radius, 0.0f,    1.0f,    0.0f,
        0.0f,    1.0f,    -radius, radius,  radius,  0.0f,    1.0f,    0.0f,    0.0f,    0.0f,
        radius,  radius,  radius,  0.0f,    1.0f,    0.0f,    1.0f,    0.0f,

    };

    math::bv::Sphere sphere;
    math::bv::AABB aabb;

    for (u32 i = 0; i < sizeof(vertices) / sizeof(f32); i += 8) {
        math::bv::merge(sphere, glm::vec3(vertices[i], vertices[i + 1], vertices[i + 2]));
        math::bv::merge(aabb, glm::vec3(vertices[i], vertices[i + 1], vertices[i + 2]));
    }

    modus::Vector<f32> packed;
    bool use_packed = false;
    if (pack_opt.pack_tex || pack_opt.pack_normal) {
        const usize array_size = modus::array_size(vertices);
        packed.reserve(array_size);
        for (usize i = 0; i < array_size;) {
            // vertices
            packed.push_back(vertices[i++]);
            packed.push_back(vertices[i++]);
            packed.push_back(vertices[i++]);

            if (pack_opt.pack_normal) {
                const f32 x = vertices[i++];
                const f32 y = vertices[i++];
                const f32 z = vertices[i++];
                packed.push_back(pack_vec3_as_f32(glm::vec3(x, y, z)));
            }

            if (pack_opt.pack_tex) {
                const f32 x = vertices[i++];
                const f32 y = vertices[i++];
                packed.push_back(pack_vec2_as_f32(glm::vec2(x, y)));
            }
        }
        use_packed = true;
    }

    math::fbs::Vec3 sphere_center(sphere.center.x, sphere.center.y, sphere.center.z);
    math::fbs::Vec3 aabb_center(aabb.center.x, aabb.center.y, aabb.center.z);
    math::fbs::Vec3 aabb_extend(aabb.extends.x, aabb.extends.y, aabb.extends.z);
    fbs::BoundingVolumeSphere fbs_bv_sphere(sphere_center, sphere.radius);
    fbs::BoundingVolumeAABB fbs_bv_aabb(aabb_center, aabb_extend);
    auto fbs_bv = fbs::BoundingVolume(fbs_bv_sphere, fbs_bv_aabb);

    const fbs::IndexType index_type = fbs::IndexType::None;
    const fbs::PrimitiveType primitive_type = fbs::PrimitiveType::Triangles;

    // Create Vertex buffer
    const Slice<f32> data_slice = use_packed ? make_slice(packed) : Slice<f32>(vertices);
    const ByteSlice data_bytes = data_slice.as_bytes();
    auto fbs_buffer_data = builder.CreateVector<u8>(data_bytes.data(), data_bytes.size());
    auto fbs_data_buffer = fbs::CreateBuffer(builder, fbs_buffer_data);

    // [Buffer]
    auto fbs_data_vector = builder.CreateVector(&fbs_data_buffer, 1);

    // Create Drawable info
    const u32 buffer_index = 0;
    u32 stride = 0;
    if (pack_opt.pack_normal && pack_opt.pack_tex) {
        stride = sizeof(f32) * 5;
    } else if (pack_opt.pack_tex) {
        stride = sizeof(f32) * 7;
    } else if (pack_opt.pack_normal) {
        stride = sizeof(f32) * 6;
    } else {
        stride = sizeof(f32) * 8;
    }

    fbs::DrawableDataBufferDesc data_buffer_desc;
    data_buffer_desc.mutate_offset(0);
    data_buffer_desc.mutate_stride(stride);
    data_buffer_desc.mutate_data_buffer_index(0);
    auto fbs_data_buffer_desc_vec = builder.CreateVectorOfStructs(&data_buffer_desc, 1);

    fbs::DrawableDesc vertex_info;
    vertex_info.mutate_offset(0);
    vertex_info.mutate_data_type(fbs::DataType::Vec3F32);
    vertex_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableDesc normal_info;
    normal_info.mutate_offset(sizeof(glm::vec3));
    if (!pack_opt.pack_normal) {
        normal_info.mutate_data_type(fbs::DataType::Vec3F32);
    } else {
        normal_info.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
        normal_info.mutate_normalize(true);
    }
    normal_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableDesc texture_info;
    if (!pack_opt.pack_normal) {
        texture_info.mutate_offset(sizeof(glm::vec3) * 2);
    } else {
        texture_info.mutate_offset(sizeof(f32) * 4);
    }
    if (!pack_opt.pack_tex) {
        texture_info.mutate_data_type(fbs::DataType::Vec2F32);
    } else {
        texture_info.mutate_data_type(fbs::DataType::Vec2U16);
        texture_info.mutate_normalize(true);
    }
    texture_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableBuilder drawable_builder(builder);
    drawable_builder.add_data_buffers(fbs_data_buffer_desc_vec);
    drawable_builder.add_draw_count(36);
    drawable_builder.add_draw_start(0);
    drawable_builder.add_index_type(index_type);
    drawable_builder.add_primitive_type(primitive_type);
    drawable_builder.add_vertex(&vertex_info);
    drawable_builder.add_normals(&normal_info);
    drawable_builder.add_texture(&texture_info);

    auto fbs_drawable = drawable_builder.Finish();
    auto fbs_drawable_vec = builder.CreateVector(&fbs_drawable, 1);

    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    // Creat Mesh
    fbs::MeshBuilder mesh_builder(builder);
    mesh_builder.add_guid(&fbs_guid);
    mesh_builder.add_bounding_volume(&fbs_bv);
    mesh_builder.add_data_buffers(fbs_data_vector);
    mesh_builder.add_submeshes(fbs_drawable_vec);
    return mesh_builder.Finish();
}
