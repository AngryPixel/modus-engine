/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include "gen_model_assimp.hpp"
#include "gen_model_cube.hpp"
#include "gen_model_icosahedron.hpp"
#include "gen_model_icosphere.hpp"
#include "gen_model_sphere.hpp"
#include "gen_plane.hpp"

using namespace modus;

static void dump_mesh_info(const ByteSlice ptr) {
    auto mesh = fbs::GetMesh(ptr.data());
    if (mesh == nullptr) {
        return;
    }

    std::cout << fmt::format("Mesh: {:.2} MB \n", f64(ptr.size()) / (1024.0 * 1024.0));

    if (auto fbs_data_buffers = mesh->data_buffers(); fbs_data_buffers != nullptr) {
        std::cout << fmt::format("Data Buffers: #{:02}\n", fbs_data_buffers->size());
        for (usize i = 0; i < fbs_data_buffers->size(); ++i) {
            auto fbs_data_buffer = fbs_data_buffers->Get(i);
            if (fbs_data_buffer) {
                auto fbs_data = fbs_data_buffer->data();
                if (fbs_data != nullptr) {
                    std::cout << fmt::format("    [{:02}] size: {} ({:.2} MB)\n", i,
                                             fbs_data->size(),
                                             f64(fbs_data->size()) / (1024.0 * 1024.0));
                }
            }
        }
    }

    if (auto fbs_data_buffers = mesh->index_buffers(); fbs_data_buffers != nullptr) {
        std::cout << fmt::format("Index Buffers: #{:02}\n", fbs_data_buffers->size());
        for (usize i = 0; i < fbs_data_buffers->size(); ++i) {
            auto fbs_data_buffer = fbs_data_buffers->Get(i);
            if (fbs_data_buffer) {
                auto fbs_data = fbs_data_buffer->data();
                if (fbs_data != nullptr) {
                    std::cout << fmt::format("    [{:02}] size: {} ({:.2} MB)\n", i,
                                             fbs_data->size(),
                                             f64(fbs_data->size()) / (1024.0 * 1024.0));
                }
            }
        }
    }

    auto print_drawable_desc = [](const fbs::DrawableDesc& desc) {
        std::cout << fmt::format("       offset       : {:02}\n", desc.offset());
        std::cout << fmt::format("       data type    : {}\n",
                                 fbs::EnumNameDataType(desc.data_type()));
        std::cout << fmt::format("       buffer       : {:02}\n", desc.data_buffer_index());
        std::cout << fmt::format("       normalize?   : {}\n", desc.normalize());
    };

    if (const auto fbs_submeshes = mesh->submeshes(); fbs_submeshes != nullptr) {
        std::cout << fmt::format("Submeshes: #{:02}\n", fbs_submeshes->size());
        for (usize i = 0; i < fbs_submeshes->size(); ++i) {
            const auto fbs_submesh = fbs_submeshes->Get(i);
            if (fbs_submesh != nullptr) {
                std::cout << fmt::format("    [{:02}] --------------- \n", i);
                std::cout << fmt::format("    index_type     : {}\n",
                                         fbs::EnumNameIndexType(fbs_submesh->index_type()));
                std::cout << fmt::format("    index buffer   : {:02}\n",
                                         fbs_submesh->index_buffer_index());
                std::cout << fmt::format("    index buf. Off.: {:02}\n",
                                         fbs_submesh->index_buffer_offset());
                std::cout << fmt::format("    primitive type : {}\n",
                                         fbs::EnumNamePrimitiveType(fbs_submesh->primitive_type()));
                std::cout << fmt::format("    draw start     : {:02}\n", fbs_submesh->draw_start());
                std::cout << fmt::format("    draw count     : {:02}\n", fbs_submesh->draw_count());
                std::cout << fmt::format("    material index : {:02}\n",
                                         fbs_submesh->material_index());

                if (const auto fbs_desc = fbs_submesh->vertex(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    vertex         : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->normals(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    normal         : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->texture(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    texture        : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->tangent(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    tangent        : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->binormal(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    binormal       : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->custom1(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    custom1        : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->custom2(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    custom2        : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->custom3(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    custom3        : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->custom4(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    custom4        : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->bone_indices(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    bone indices   : \n");
                    print_drawable_desc(*fbs_desc);
                }
                if (const auto fbs_desc = fbs_submesh->bone_weights(); fbs_desc != nullptr) {
                    std::cout << fmt::format("    bone weights  : \n");
                    print_drawable_desc(*fbs_desc);
                }
            }
        }
    }

    if (const auto fbs_anim_catalog = mesh->animation_catalog(); fbs_anim_catalog != nullptr) {
        std::cout << fmt::format("Animation Catalog:\n");
        u32 skeleton_idx = 0;
        for (const auto& fbs_skeleton : *fbs_anim_catalog->skeletons()) {
            std::cout << fmt::format("    Skeleton       : {:02} \n", skeleton_idx);
            if (auto fbs_bones = fbs_skeleton->bones(); fbs_bones != nullptr) {
                u32 bone_idx = 0;
                for (const auto fbs_bone : *fbs_bones) {
                    std::cout << fmt::format("       [{:03}] :: {} : {:03}\n", bone_idx,
                                             fbs_bone->name()->c_str(), fbs_bone->parent_index());
                    bone_idx++;
                    if (const auto fbs_transform = fbs_bone->inv_bind_transform();
                        fbs_transform != nullptr) {
                        std::cout << fmt::format(
                            "          t: x:{} y:{} z:{} \n", fbs_transform->position().x(),
                            fbs_transform->position().y(), fbs_transform->position().z());
                        std::cout << fmt::format(
                            "          r: w:{} x:{} y:{} z:{} \n", fbs_transform->rotation().w(),
                            fbs_transform->rotation().x(), fbs_transform->rotation().y(),
                            fbs_transform->rotation().z());
                        std::cout << fmt::format("          s: {}\n", fbs_transform->scale());
                    }
                }
            }
            ++skeleton_idx;
        }
        u32 anim_idx = 0;
        for (const auto& fbs_animation : *fbs_anim_catalog->skeleton_animations()) {
            std::cout << fmt::format("    Skeleton Animation [{:02}] {}\n", anim_idx,
                                     fbs_animation->name()->c_str());
            std::cout << fmt::format("      skeleton index : {:02} \n",
                                     fbs_animation->skeleton_index());
            std::cout << fmt::format("      duration sec   : {} \n", fbs_animation->duration_sec());
            if (const auto fbs_bone_keyframes = fbs_animation->bone_keyframes();
                fbs_bone_keyframes != nullptr) {
                std::cout << fmt::format("      bone transforms : {} \n",
                                         fbs_animation->bone_keyframes()->size());
                for (usize j = 0; j < fbs_bone_keyframes->size(); ++j) {
                    if (const auto& fbs_bt = fbs_animation->bone_keyframes()->Get(j); fbs_bt) {
                        const auto fbs_transforms = fbs_bt->bone_transforms();
                        std::cout << fmt::format(
                            "          [{:02}] {:02}:{:02} \n", j, fbs_bt->bone_index(),
                            fbs_transforms != nullptr ? fbs_bt->bone_transforms()->size() : 0);
                    }
                }
            }
            ++anim_idx;
        }
    }
}

static Result<> dump(const StringSlice path) {
    auto r_file = os::FileBuilder().read().open(path);
    if (r_file) {
        std::cerr << fmt::format("Failed to open {}\n", path);
    }

    RamStream<> ram_stream;
    auto r_mem_read = ram_stream.populate_from_stream(*r_file);
    if (!r_mem_read || r_mem_read.value() != r_file->size()) {
        std::cerr << "Failed to read stream into memory\n";
        return Error<>();
    }

    const ByteSlice bytes = ram_stream.as_bytes();

    flatbuffers::Verifier v(bytes.data(), bytes.size());
    if (!fbs::VerifyMeshBuffer(v)) {
        std::cerr << fmt::format("File {} is not a mesh\n", path);
        return Error<>();
    }

    dump_mesh_info(bytes);
    return Ok<>();
}

static const StringSlice kIntro = "Generate Meshes for the Engine.\n modus_mk_mesh [options] ...";
static const StringSlice kTypeCube = "cube";
static const StringSlice kTypeSphere = "sphere";
static const StringSlice kTypeIcosphere = "icosphere";
static const StringSlice kTypeIcosahedron = "icosahedron";
static const StringSlice kTypeAssimp = "assimp";
static const StringSlice kTypePlane = "plane";

int main(const int argc, const char** argv) {
    CmdOptionString opt_output("-o", "--output", "Path for the generated output", "");
    CmdOptionString opt_type(
        "-t", "--type", "Model type [cube|sphere|icosphere|icosahedron|assimp|plane].", kTypeCube);
    CmdOptionF32 opt_radius("-r", "--radius", "Generated model radius. Default = 1.0f", 1.0f);
    CmdOptionString opt_guid("-g", "--guid", "Use provided guid instead of generating one", "");

    // Sphere Specific Options
    CmdOptionU32 opt_sphere_sectors("", "--sphere-sectors", "Number of sphere sectors", 1);
    CmdOptionU32 opt_sphere_stacks("", "--sphere-stacks", "Number of sphere stacks", 1);

    // Icosphere Specific Options
    CmdOptionU32 opt_icosphere_iter_count("", "--icosphere-iter-count",
                                          "Iteration count for icosphere generation", 1);

    // Assimp options
    CmdOptionString opt_assimp_file("", "--assimp-file",
                                    "File to be loaded by the Assimp model convert", "");
    CmdOptionEmpty opt_assimp_disable_opt_graph("", "--assimp-disable-opt-graph",
                                                "Disable assimp optimize graph optimization");
    CmdOptionEmpty opt_assimp_disable_opt("", "--assimp-disable-opt",
                                          "Disable assimp optimizations");
    CmdOptionEmpty opt_assimp_skip_material("", "--assimp-skip-material",
                                            "Skip material information extraction");
    CmdOptionEmpty opt_assimp_skip_submesh_pack("", "--assimp-skip-submesh-pack",
                                                "Do not pack sub-meshes into one buffer");

    CmdOptionString opt_material_output("-m", "--matieral-ouput-dir",
                                        "Directory where to generate material files\n", "");

    CmdOptionEmpty opt_pack_texcoord("", "--pack-texcoord",
                                     "Pack texture coordinates as one unsigned integer");
    CmdOptionEmpty opt_pack_normal("", "--pack-normal",
                                   "Pack normals in 2_10_10_10_REV unsigned integer");
    CmdOptionEmpty opt_pack_tb("", "--pack-tb",
                               "Pack tangent and binormal in 2_10_10_10_REV unsigned integer");
    CmdOptionEmpty opt_pack_all(
        "", "--pack-all", "Apply all packing options (--pack-texcoord, --pack-normal, --pack-tb");

    CmdOptionEmpty opt_skip_tangent("", "--skip-tangent", "Do not generate tangent/binormal data");

    CmdOptionEmpty opt_plane_xy("", "--plane-xy", "Generate XY plane");
    CmdOptionEmpty opt_plane_xz("", "--plane-xz", "Generate XZ plane");
    CmdOptionEmpty opt_plane_yz("", "--plane-yz", "Generate YZ plane");
    CmdOptionF32 opt_plane_xy_width("", "--plane-xy-width", "Width of the plane", 1.0f);
    CmdOptionF32 opt_plane_xy_height("", "--plane-xy-height", "Height of the plane", 1.0f);
    CmdOptionF32 opt_plane_xz_width("", "--plane-xz-width", "Width of the plane", 1.0f);
    CmdOptionF32 opt_plane_xz_height("", "--plane-xz-height", "Height of the plane", 1.0f);
    CmdOptionF32 opt_plane_yz_width("", "--plane-yz-width", "Width of the plane", 1.0f);
    CmdOptionF32 opt_plane_yz_height("", "--plane-yz-height", "Height of the plane", 1.0f);

    CmdOptionString opt_dump("-d", "--dump",
                             "Print information about an existing mesh. E.g: -d test.mesh", "");
    CmdOptionEmpty opt_verbose("-v", "--verbose", "Verbose ouptput");

    CmdOptionParser opt_parser;

    opt_parser.add(opt_output).expect("Failed to add cmd parser option");
    opt_parser.add(opt_type).expect("Failed to add cmd parser option");
    opt_parser.add(opt_radius).expect("Failed to add cmd parser option");
    opt_parser.add(opt_guid).expect("Failed to add cmd parser option");

    opt_parser.add(opt_sphere_stacks).expect("Failed to add cmd parser option");
    opt_parser.add(opt_sphere_sectors).expect("Failed to add cmd parser option");

    opt_parser.add(opt_icosphere_iter_count).expect("Failed to add cmd parser option");

    opt_parser.add(opt_assimp_file).expect("Failed to add cmd parser option");
    opt_parser.add(opt_assimp_disable_opt_graph).expect("Failed to add cmd parser option");
    opt_parser.add(opt_assimp_disable_opt).expect("Failed to add cmd parser option");
    opt_parser.add(opt_assimp_skip_material).expect("Failed to add cmd parser option");
    opt_parser.add(opt_material_output).expect("Failed to add cmd parser option");
    opt_parser.add(opt_assimp_skip_submesh_pack).expect("Failed to add cmd parser option");

    opt_parser.add(opt_plane_xy).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_xz).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_yz).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_xy_width).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_xy_height).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_xz_width).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_xz_height).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_yz_width).expect("Failed to add cmd parser option");
    opt_parser.add(opt_plane_yz_height).expect("Failed to add cmd parser option");

    opt_parser.add(opt_pack_texcoord).expect("Failed to add cmd parser option");
    opt_parser.add(opt_pack_normal).expect("Failed to add cmd parser option");
    opt_parser.add(opt_pack_tb).expect("Failed to add cmd parser option");
    opt_parser.add(opt_pack_all).expect("Failed to add cmd parser option");
    opt_parser.add(opt_skip_tangent).expect("Failed to add cmd parser option");
    opt_parser.add(opt_dump).expect("Failed to add cmd parser option");
    opt_parser.add(opt_verbose).expect("Failed to add cmd parser option");

    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (opt_dump.parsed()) {
        auto r = dump(opt_dump.value());
        if (r) {
            return EXIT_SUCCESS;
        } else {
            return EXIT_FAILURE;
        }
    }

    if (!opt_output.parsed()) {
        std::cerr << fmt::format("No output file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    const StringSlice type = opt_type.value();

    if (type != kTypeCube && type != kTypeSphere && type != kTypeIcosphere &&
        type != kTypeIcosahedron && type != kTypeAssimp && type != kTypePlane) {
        std::cerr << fmt::format("Invalid model type: {}\n", type);
        return EXIT_FAILURE;
    }

    GID guid;
    if (!opt_guid.parsed()) {
        auto r_guid = os::guid::generate();
        if (!r_guid) {
            std::cerr << "Failed to generate GUID\n";
            return EXIT_FAILURE;
        }
        guid = *r_guid;
    } else {
        auto r_guid = GID::from_string(opt_guid.value());
        if (!r_guid) {
            std::cerr << fmt::format("GUID '{}' is not a valid GUID type\n", opt_guid.value());
            return EXIT_FAILURE;
        };
        guid = r_guid.value();
    }

    auto r_output = os::FileBuilder().write().create().open(opt_output.value());
    if (!r_output) {
        std::cerr << fmt::format("Failed to open output file: {}\n", opt_output.value());
        return EXIT_FAILURE;
    }

    PackOptions pack_opt;
    pack_opt.pack_tex = opt_pack_texcoord.parsed();
    pack_opt.pack_normal = opt_pack_normal.parsed();
    pack_opt.pack_tb = opt_pack_tb.parsed();
    if (opt_pack_all.parsed()) {
        pack_opt.pack_tex = true;
        pack_opt.pack_normal = true;
        pack_opt.pack_tb = true;
    }

    flatbuffers::FlatBufferBuilder builder;
    flatbuffers::Offset<fbs::Mesh> fbs_mesh;
    if (type == kTypeCube) {
        fbs_mesh = gen_cube(builder, guid, opt_radius.value(), pack_opt);
    } else if (type == kTypeSphere) {
        fbs_mesh = gen_sphere(builder, guid, opt_radius.value(), opt_sphere_sectors.value(),
                              opt_sphere_stacks.value(), pack_opt);
    } else if (type == kTypeIcosphere) {
        fbs_mesh = gen_icosphere(builder, guid, opt_radius.value(),
                                 opt_icosphere_iter_count.value(), pack_opt);
    } else if (type == kTypeIcosahedron) {
        fbs_mesh = gen_icosahedron(builder, guid, opt_radius.value(), pack_opt);
    } else if (type == kTypeAssimp) {
        if (!opt_assimp_file.parsed()) {
            std::cerr << "No input file specified. Please specify one using "
                         "--assimp-file\n";
            return EXIT_FAILURE;
        }

        String material_dir;
        if (opt_material_output.parsed()) {
            material_dir = opt_material_output.value_str();
        } else {
            material_dir = os::Path::get_path(opt_output.value()).to_str();
        }
        AssimpOptions options;
        options.skip_tangent_gen = opt_skip_tangent.parsed();
        options.input_file = opt_assimp_file.value_cstr();
        options.disable_optimize_graph = opt_assimp_disable_opt_graph.parsed();
        options.disable_optimizations = opt_assimp_disable_opt.parsed();
        options.verbose = opt_verbose.parsed();
        options.pack_opt = pack_opt;
        options.submeshes_share_buffer = !opt_assimp_skip_submesh_pack.parsed();
        if (!opt_assimp_skip_material.parsed()) {
            options.material_file_output = material_dir;
        }
        auto r_gen = gen_model_assimp(builder, guid, opt_radius.value(), options);
        if (!r_gen) {
            return EXIT_FAILURE;
        }
        fbs_mesh = *r_gen;
    } else if (type == kTypePlane) {
        GenPlaneParams params;
        params.xy = opt_plane_xy.parsed();
        params.xz = opt_plane_xz.parsed();
        params.yz = opt_plane_yz.parsed();
        params.xy_width = opt_plane_xy_width.value();
        params.xy_height = opt_plane_xy_height.value();
        params.xz_width = opt_plane_xz_width.value();
        params.xz_height = opt_plane_xz_height.value();
        params.yz_width = opt_plane_yz_width.value();
        params.yz_height = opt_plane_yz_height.value();
        fbs_mesh = gen_plane(builder, guid, params);
    } else {
        modus_panic("Unkown type");
    }

    // Finish build
    builder.Finish(fbs_mesh, "MESH");

    const ByteSlice builder_slice = ByteSlice(builder.GetBufferPointer(), builder.GetSize());

    auto r_write = r_output.value().write(builder_slice);
    if (!r_write || r_write.value() != builder_slice.size()) {
        std::cerr << fmt::format("Failed to write contents to output: {}\n", opt_output.value());
        return EXIT_FAILURE;
    }

    if (opt_verbose.parsed()) {
        dump_mesh_info(builder_slice);
    }
    return EXIT_SUCCESS;
}
