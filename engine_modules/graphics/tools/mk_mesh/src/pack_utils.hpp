/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

struct PackOptions {
    bool pack_tex = false;
    bool pack_normal = false;
    bool pack_tb = false;
};

inline modus::Vector<u16> pack_vec2s(const Slice<glm::vec2>& v) {
    modus::Vector<u16> result;
    result.reserve(v.size());

    for (auto& vec : v) {
        result.push_back(math::pack_snorm_1616(vec));
    }
    return result;
}

inline f32 pack_vec3_as_f32(const glm::vec3& normal) {
    union S {
        f32 f;
        u32 u;
    } v;
    v.u = math::pack_snorm_1010102_rev(glm::vec4(normal, 0.f));
    return v.f;
}

inline f32 pack_vec2_as_f32(const glm::vec2& vec) {
    union S {
        f32 f;
        u32 u;
    } v;
    v.u = math::pack_unorm_1616(vec);
    return v.f;
}

struct alignas(4) VertexData {
    glm::vec3 vertex;
    glm::vec3 normal;
    glm::vec2 texture;
};

struct alignas(4) VertexDataPackedNormal {
    glm::vec3 vertex;
    u32 normal;
    glm::vec2 texture;
};

struct alignas(4) VertexDataPackedTexture {
    glm::vec3 vertex;
    glm::vec3 normal;
    u32 texture;
};

struct alignas(4) VertexDataPackedAll {
    glm::vec3 vertex;
    u32 normal;
    u32 texture;
};

inline modus::Vector<VertexDataPackedNormal> pack_normal(const Slice<VertexData> input) {
    modus::Vector<VertexDataPackedNormal> output;
    output.resize(input.size());
    for (usize i = 0; i < input.size(); ++i) {
        output[i].vertex = input[i].vertex;
        output[i].normal = math::pack_snorm_1010102_rev(glm::vec4(input[i].normal, 0.f));
        output[i].texture = input[i].texture;
    }
    return output;
}

inline modus::Vector<VertexDataPackedTexture> pack_texture(const Slice<VertexData>& input) {
    modus::Vector<VertexDataPackedTexture> output;
    output.resize(input.size());
    for (usize i = 0; i < input.size(); ++i) {
        output[i].vertex = input[i].vertex;
        output[i].normal = input[i].normal;
        output[i].texture = math::pack_unorm_1616(input[i].texture);
    }
    return output;
}

inline modus::Vector<VertexDataPackedAll> pack_all(const Slice<VertexData>& input) {
    modus::Vector<VertexDataPackedAll> output;
    output.resize(input.size());
    for (usize i = 0; i < input.size(); ++i) {
        output[i].vertex = input[i].vertex;
        output[i].normal = math::pack_snorm_1010102_rev(glm::vec4(input[i].normal, 0.f));
        output[i].texture = math::pack_unorm_1616(input[i].texture);
    }
    return output;
}
