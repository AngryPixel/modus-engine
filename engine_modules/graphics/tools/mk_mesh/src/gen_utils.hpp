/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

inline modus::Vector<u16> convert_indices_to_u16(const Slice<u32> indices) {
    modus::Vector<u16> converted;
    bool convert_indices_to_u16 = true;
    for (auto& index : indices) {
        convert_indices_to_u16 =
            convert_indices_to_u16 && index < u32(std::numeric_limits<u16>::max());
    }

    if (convert_indices_to_u16) {
        converted.resize(indices.size());
        usize i = 0;
        for (auto& index : indices) {
            converted[i++] = u16(index);
        }
    }
    return converted;
}
