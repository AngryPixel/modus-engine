/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include "gen_plane.hpp"
#include "pack_utils.hpp"

flatbuffers::Offset<fbs::Mesh> gen_plane(flatbuffers::FlatBufferBuilder& builder,
                                         const GID& guid,
                                         const GenPlaneParams& params) {
    const glm::vec3 positions_xy[4] = {glm::vec3(-params.xy_width, -params.xy_height, 0.f),
                                       glm::vec3(+params.xy_width, -params.xy_height, 0.f),
                                       glm::vec3(-params.xy_width, +params.xy_height, 0.f),
                                       glm::vec3(+params.xy_width, +params.xy_height, 0.f)};

    const glm::vec3 normal_xy = glm::vec3(0.f, 0.f, 1.0f);

    const glm::vec3 positions_xz[4] = {glm::vec3(-params.xz_width, 0.f, -params.xz_height),
                                       glm::vec3(+params.xz_width, 0.f, -params.xz_height),
                                       glm::vec3(-params.xz_width, 0.f, +params.xz_height),
                                       glm::vec3(+params.xz_width, 0.f, +params.xz_height)};

    const glm::vec3 normal_xz = glm::vec3(0.f, 1.f, 0.0f);

    const glm::vec3 positions_yz[4] = {glm::vec3(0.f, -params.yz_width, -params.yz_height),
                                       glm::vec3(0.f, +params.yz_width, -params.yz_height),
                                       glm::vec3(0.f, -params.yz_width, +params.yz_height),
                                       glm::vec3(0.f, +params.yz_width, +params.yz_height)};

    const glm::vec3 normal_yz = glm::vec3(1.f, 0.f, 0.0f);

    const glm::vec2 tex_coords[4] = {glm::vec2(0, 0), glm::vec2(1, 0), glm::vec2(0, 1),
                                     glm::vec2(1, 1)};

    modus::Vector<f32> buffer;
    buffer.reserve(36 * 3);

    math::bv::Sphere sphere;
    math::bv::AABB aabb;

    u32 draw_count = 0;

    auto insert_fn = [&buffer, &sphere, &aabb](const glm::vec3 pos[4], const glm::vec3& normal,
                                               const glm::vec2 tex[4]) {
        const constexpr usize indices[] = {1, 0, 2, 2, 3, 1};
        for (usize i : indices) {
            buffer.push_back(pos[i].x);
            buffer.push_back(pos[i].y);
            buffer.push_back(pos[i].z);
            buffer.push_back(pack_vec3_as_f32(normal));
            buffer.push_back(pack_vec2_as_f32(tex[i]));

            math::bv::merge(sphere, pos[i]);
            math::bv::merge(aabb, pos[i]);
        }
    };

    if (params.xy) {
        insert_fn(positions_xy, normal_xy, tex_coords);
        draw_count += 6;
    }
    if (params.xz) {
        insert_fn(positions_xz, normal_xz, tex_coords);
        draw_count += 6;
    }
    if (params.yz) {
        insert_fn(positions_yz, normal_yz, tex_coords);
        draw_count += 6;
    }

    math::fbs::Vec3 sphere_center(sphere.center.x, sphere.center.y, sphere.center.z);
    math::fbs::Vec3 aabb_center(aabb.center.x, aabb.center.y, aabb.center.z);
    math::fbs::Vec3 aabb_extend(aabb.extends.x, aabb.extends.y, aabb.extends.z);
    fbs::BoundingVolumeSphere fbs_bv_sphere(sphere_center, sphere.radius);
    fbs::BoundingVolumeAABB fbs_bv_aabb(aabb_center, aabb_extend);
    auto fbs_bv = fbs::BoundingVolume(fbs_bv_sphere, fbs_bv_aabb);

    const fbs::IndexType index_type = fbs::IndexType::None;
    const fbs::PrimitiveType primitive_type = fbs::PrimitiveType::Triangles;

    // Create Vertex buffer
    const Slice<f32> data_slice = make_slice(buffer);
    const ByteSlice data_bytes = data_slice.as_bytes();
    auto fbs_buffer_data = builder.CreateVector<u8>(data_bytes.data(), data_bytes.size());
    auto fbs_data_buffer = fbs::CreateBuffer(builder, fbs_buffer_data);

    const u32 buffer_index = 0;
    u32 stride = 20;

    // [Buffer]
    auto fbs_data_vector = builder.CreateVector(&fbs_data_buffer, 1);
    fbs::DrawableDataBufferDesc data_buffer_desc;
    data_buffer_desc.mutate_offset(0);
    data_buffer_desc.mutate_stride(stride);
    data_buffer_desc.mutate_data_buffer_index(buffer_index);
    auto fbs_data_buffer_desc_vec = builder.CreateVectorOfStructs(&data_buffer_desc, 1);

    // Create Drawable info
    fbs::DrawableDesc vertex_info;
    vertex_info.mutate_offset(0);
    vertex_info.mutate_data_type(fbs::DataType::Vec3F32);
    vertex_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableDesc normal_info;
    normal_info.mutate_offset(sizeof(glm::vec3));
    normal_info.mutate_data_type(fbs::DataType::I32_2_3X10_REV);
    normal_info.mutate_normalize(true);
    normal_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableDesc texture_info;
    texture_info.mutate_offset(sizeof(glm::vec4));
    texture_info.mutate_data_type(fbs::DataType::Vec2U16);
    texture_info.mutate_normalize(true);
    texture_info.mutate_data_buffer_index(buffer_index);

    fbs::DrawableBuilder drawable_builder(builder);
    drawable_builder.add_data_buffers(fbs_data_buffer_desc_vec);
    drawable_builder.add_draw_count(draw_count);
    drawable_builder.add_draw_start(0);
    drawable_builder.add_index_type(index_type);
    drawable_builder.add_primitive_type(primitive_type);
    drawable_builder.add_vertex(&vertex_info);
    drawable_builder.add_normals(&normal_info);
    drawable_builder.add_texture(&texture_info);

    auto fbs_drawable = drawable_builder.Finish();
    auto fbs_drawable_vec = builder.CreateVector(&fbs_drawable, 1);

    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    // Creat Mesh
    fbs::MeshBuilder mesh_builder(builder);
    mesh_builder.add_guid(&fbs_guid);
    mesh_builder.add_bounding_volume(&fbs_bv);
    mesh_builder.add_data_buffers(fbs_data_vector);
    mesh_builder.add_submeshes(fbs_drawable_vec);
    return mesh_builder.Finish();
}
