/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <engine/engine_pch.h>
#include <engine/graphics/font_generated.h>
#include <freetype/ftglyph.h>
#include <ft2build.h>

#include <core/getopt.hpp>
#include <iostream>
#include <math/math.hpp>
#include <os/file.hpp>
#include <os/guid_gen.hpp>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

using namespace modus;
using namespace modus::engine;

struct RectScope {
    i32 x = 0;
    i32 y = 0;
    i32 w = 0;
    i32 h = 0;
    i32 dw = 0;
    i32 dh = 0;
};

// Get value inside the rect scope.
u8 rect_scope_get(const RectScope& rs, Slice<u8> data, const i32 x, const i32 y) {
    modus_assert(x < rs.w);
    modus_assert(y < rs.h);
    const i32 data_x = rs.x + x;
    const i32 data_y = rs.y + y;
    return data[data_y * rs.dw + data_x];
}

void rect_scope_set(const RectScope& rs, SliceMut<u8> data, const i32 x, const i32 y, const u8 v) {
    modus_assert(x < rs.w);
    modus_assert(y < rs.h);
    const i32 data_x = rs.x + x;
    const i32 data_y = rs.y + y;
    data[data_y * rs.dw + data_x] = v;
}

#if !defined(USE_SDF_GLYPH_FOUNDRY)

// TinySDF implemenation based of https://github.com/mapbox/tiny-sdf
// TODO: Font size/scale is not working properly ... sigh
class TinySDF final {
   private:
    std::vector<f64> m_gridOuter;
    std::vector<f64> m_gridInner;
    std::vector<f64> m_f;
    std::vector<f64> m_z;
    std::vector<f64> m_v;
    std::vector<u8> m_output;
    i32 m_size;

   public:
    TinySDF() : m_size(0) {}

    void process(const RectScope& scope,
                 const Slice<u8> data,
                 const f64 radius = 8.0,
                 const f64 cutoff = 0.25) {
        const i32 new_size = scope.w * scope.h;
        if (new_size > m_size) {
            m_gridOuter.resize(new_size);
            m_gridInner.resize(new_size);
            m_f.resize(new_size);
            m_z.resize(new_size);
            m_v.resize(new_size);
            m_output.resize(new_size);
        }

        m_size = new_size;

        constexpr f64 kInf = 1e20;

        // Set up inner and outer grid
        i32 i = 0;
        for (i32 y = 0; y < scope.h; y++) {
            for (i32 x = 0; x < scope.w; x++) {
                const u8 value = rect_scope_get(scope, data, x, y);
                const f32 alpha = f32(value) / 255.0f;
                m_gridOuter[i] = math::equal(alpha, 1.0f) ? 0.0
                                 : math::equal(alpha, 0.0f)
                                     ? kInf
                                     : f64(glm::pow(glm::max(0.0f, 0.5f - alpha), 2.0f));
                m_gridInner[i] = math::equal(alpha, 1.0f) ? kInf
                                 : math::equal(alpha, 0.0f)
                                     ? 0.0
                                     : f64(glm::pow(glm::max(0.0f, alpha - 0.5f), 2.0f));
                ++i;
            }
        }

        // 1D squared distance transform
        auto edt1d = [this, kInf](f64* grid, const i32 offset, const i32 stride,
                                  const i32 length) -> void {
            i32 q, k;
            f64 s, r;
            m_v[0] = 0.0;
            m_z[0] = -kInf;
            m_z[1] = kInf;

            for (q = 0; q < length; q++) {
                m_f[q] = grid[offset + q * stride];
            }

            k = 0;
            s = 0.0;
            for (q = 1; q < length; q++) {
                do {
                    r = m_v[k];
                    s = (m_f[q] - m_f[i64(r)] + f64(i64(q) * i64(q)) - r * r) / (f64(q) - r) / 2.0;
                } while (s <= m_z[k] && --k > -1);

                k++;
                m_v[k] = q;
                m_z[k] = s;
                m_z[k + 1] = kInf;
            }

            k = 0;
            for (q = 0; q < length; q++) {
                while (m_z[k + 1] < f64(q)) {
                    k++;
                }
                r = m_v[k];
                grid[offset + q * stride] = m_f[i64(r)] + (f64(q) - r) * (f64(q) - r);
            }
        };

        // 2D Euclidean squared distance transform by Felzenszwalb & Huttenlocher
        // https://cs.brown.edu/~pff/papers/dt-final.pdf
        auto edt = [&edt1d](f64* grid, const i32 width, const i32 height) {
            for (i32 x = 0; x < width; x++) {
                edt1d(grid, x, width, height);
            }
            for (i32 y = 0; y < height; y++) {
                edt1d(grid, y * width, 1, width);
            }
        };

        edt(m_gridOuter.data(), scope.w, scope.h);
        edt(m_gridInner.data(), scope.w, scope.h);

        // conversion to u8
        for (i = 0; i < m_size; i++) {
            const f64 d = glm::sqrt(m_gridOuter[i]) - glm::sqrt(m_gridInner[i]);
            const f64 d_norm = glm::round(255.0 - 255 * (d / radius + cutoff));
            const u8 out = static_cast<u8>(glm::clamp(d_norm, 0.0, 255.0));
            m_output[i] = out;
        }
    }

    void copy(const RectScope& scope, SliceMut<u8> data) {
        if (scope.w * scope.h < m_size) {
            modus_panic("Size does not match output dimensions");
        }
        i32 i = 0;
        for (i32 y = 0; y < scope.h; y++) {
            for (i32 x = 0; x < scope.w; x++) {
                rect_scope_set(scope, data, x, y, m_output[i]);
                i++;
            }
        }
    }
};

#endif

class FreeTypeScope {
   private:
    FT_Library& m_ft;

   public:
    FreeTypeScope(FT_Library& ft) : m_ft(ft) {}

    ~FreeTypeScope() { FT_Done_FreeType(m_ft); }
};

class FreeTypeFaceScope {
   private:
    FT_Face& m_ft;

   public:
    FreeTypeFaceScope(FT_Face& ft) : m_ft(ft) {}

    ~FreeTypeFaceScope() { FT_Done_Face(m_ft); }
};

struct FontOptions {
    i32 padding_px = 2;
    i32 font_size_px = 12;
};

static const StringSlice kIntro = "modus_mk_font -i input -o output";
int main(const int argc, const char** argv) {
    CmdOptionString opt_input("-i", "--input", "Input font file", "");
    CmdOptionString opt_output("-o", "--output", "Path to the generated file", "");
    CmdOptionEmpty opt_sdf("-s", "--sdf", "Generate signed distance field");
    CmdOptionU32 opt_sdf_scale("", "--sdf-scale", "Scale to apply to sdf data. Default = 12", 12);
    CmdOptionU32 opt_atlas_font_size("", "--atlas-font-size-px",
                                     "Font size in pixels. Default = 12", 12);
    CmdOptionU32 opt_atlas_padding("", "--atlas-padding-px",
                                   "Padding in pixels between glyphs on the atlas. Default = 2", 2);
    CmdOptionString opt_guid("-g", "--guid", "Use provided guid instead of generating one", "");
    CmdOptionU32 opt_atlas_dim("", "--atlas-dim-px", "Pow2 Size of atlas. Default = 512px", 512);
    CmdOptionString opt_debug_image(
        "", "--save-atlas-image", "Path to where the generate atlas image should be generated", "");
    CmdOptionParser opt_parser;

    opt_parser.add(opt_input).expect("failed to add option to parser");
    opt_parser.add(opt_output).expect("failed to add option to parser");
    opt_parser.add(opt_sdf).expect("failed to add option to parser");
    opt_parser.add(opt_sdf_scale).expect("failed to add option to parser");
    opt_parser.add(opt_atlas_font_size).expect("failed to add option to parser");
    opt_parser.add(opt_atlas_padding).expect("failed to add option to parser");
    opt_parser.add(opt_atlas_dim).expect("failed to add option to parser");
    opt_parser.add(opt_debug_image).expect("failed to add option to parser");
    opt_parser.add(opt_guid).expect("failed to add option to parser");

    if (!opt_parser.parse(argc, argv) || !opt_input.parsed() || !opt_output.parsed()) {
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    if (opt_atlas_dim.parsed()) {
        if (!math::is_pow2(opt_atlas_dim.value())) {
            std::cerr << fmt::format("Atlas dim {} is not a power of 2\n\n", opt_atlas_dim.value());
            std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
            return EXIT_FAILURE;
        }
    }

    GID guid;
    if (!opt_guid.parsed()) {
        auto r_guid = os::guid::generate();
        if (!r_guid) {
            std::cerr << "Failed to generate GUID\n";
            return EXIT_FAILURE;
        }
        guid = *r_guid;
    } else {
        auto r_guid = GID::from_string(opt_guid.value());
        if (!r_guid) {
            std::cerr << fmt::format("GUID '{}' is not a valid GUID type\n", opt_guid.value());
            return EXIT_FAILURE;
        };
        guid = r_guid.value();
    }

    // Init Freetype
    FT_Library ft;
    if (FT_Init_FreeType(&ft) != 0) {
        std::cerr << "Failed to initialize Freetype \n";
    }
    // Scope guard
    FreeTypeScope ft_scope(ft);

    // open font
    FT_Face face;
    if (FT_New_Face(ft, opt_input.value_cstr(), 0, &face) != 0) {
        std::cerr << fmt::format("Failed to open font {}\n", opt_input.value());
        return EXIT_FAILURE;
    }
    FreeTypeFaceScope face_scope(face);

    FontOptions font_options;
    font_options.font_size_px = opt_atlas_font_size.value();
    font_options.padding_px = opt_atlas_padding.value();

    struct GlypInfo {
        i32 index;                 // sorting index / asci character
        i32 width;                 // glyph width
        i32 height;                // glyph height
        i32 bearing_x;             // distance form origin left
        i32 bearing_y;             // height from the base line
        i32 advance_x;             // distance to next character
        f32 tex_top;               // Texture Rect  top
        f32 tex_left;              // Texture Rect left
        f32 tex_right;             // Texture Rect right
        f32 tex_bottom;            // Texture Rect bottom
        std::vector<u8> buffer;    // stored glyph images
    };
    modus::Vector<GlypInfo> glyph_infos;
    modus::Vector<RectScope> sdf_rects;

    // Set height in pixels for font
    if (!opt_sdf.parsed()) {
        FT_Set_Pixel_Sizes(face, 0, font_options.font_size_px);
    } else {
        FT_Set_Char_Size(face, 0, (FT_F26Dot6)(font_options.font_size_px * (1 << 6)), 0, 0);
    }

    u32 max_height = 0;
    u32 max_width = 0;
    const u32 line_height = (face->size->metrics.ascender - face->size->metrics.descender) >> 6;
    // Load all chars from 32 to 128
    glyph_infos.reserve(127 - 32);
    sdf_rects.reserve(127 - 32);

    FT_Int32 ft_load_flags = FT_LOAD_RENDER;
    if (opt_sdf.parsed()) {
        ft_load_flags |= FT_LOAD_NO_HINTING;
    }

    for (i32 i = 32; i < 127; ++i) {
        const FT_UInt char_index = FT_Get_Char_Index(face, i);
        if (char_index == 0) {
            std::cerr << fmt::format("Font does not have a valid glyphy for char: '{}':{:02}\n",
                                     char(i), i);
            return EXIT_FAILURE;
        }

        // draw glyph anti-aliased
        // FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
        if (FT_Load_Glyph(face, char_index, ft_load_flags) != 0) {
            return EXIT_FAILURE;
        }

        GlypInfo glyph_info;
        glyph_info.index = i;
        glyph_info.height = face->glyph->bitmap.rows;
        glyph_info.width = face->glyph->bitmap.width;
        glyph_info.bearing_x = face->glyph->bitmap_left;
        glyph_info.bearing_y = face->glyph->bitmap_top;
        glyph_info.advance_x = face->glyph->advance.x >>
                               6;    // advance.x is 1/64, needs to be dived to get the right value

        max_height = std::max(max_height, face->glyph->bitmap.rows);
        max_width = std::max(max_width, face->glyph->bitmap.width);
        // copy glyph data into memory because it seems to be overwritten/lost
        // later
        glyph_info.buffer.resize(glyph_info.width * glyph_info.height, '0');
        memcpy(glyph_info.buffer.data(), face->glyph->bitmap.buffer,
               face->glyph->bitmap.rows * face->glyph->bitmap.width);
        glyph_infos.push_back(std::move(glyph_info));
        /*        std::cout << fmt::format("'{}' :: width:{:04} height:{:04}
           bl:{:04} bt:{:04} advance:{:04}\n", char(i),
           face->glyph->bitmap.width, face->glyph->bitmap.rows,
                        face->glyph->bitmap_left,
                        face->glyph->bitmap_top,
                        face->glyph->advance.x >> 6);*/
    }

    // check if monospace
    bool is_monospace = true;
    for (size_t i = 0; i < glyph_infos.size() - 1 && is_monospace; ++i) {
        is_monospace = is_monospace && (glyph_infos[i].advance_x == glyph_infos[i + 1].advance_x);
    }

    // sort glyphs by height
    std::sort(glyph_infos.begin(), glyph_infos.end(),
              [](const GlypInfo& g1, const GlypInfo& g2) { return g1.height > g2.height; });

    const u32 atlas_buffer_size = opt_atlas_dim.value() * opt_atlas_dim.value();
    std::unique_ptr<u8> atlas_buffer_ptr(new u8[atlas_buffer_size]);
    ByteSliceMut atlas_buffer(atlas_buffer_ptr.get(), atlas_buffer_size);
    // Set everything to 0
    memset(atlas_buffer_ptr.get(), 0, atlas_buffer_size);

    // Generate singe row font atlas
    u32 current_x = 0;
    u32 current_y = 0;
    u32 current_height = glyph_infos[0].height;
    for (auto& glyph_info : glyph_infos) {
        const u32 width = glyph_info.width;
        const u32 height = glyph_info.height;

        // check if we need to move to the next row
        if (current_x + width + font_options.padding_px * 2 >= opt_atlas_dim.value()) {
            current_x = 0;
            current_y += font_options.padding_px * 2 + current_height;
            current_height = height;
        }

        // Calculate Glyph rect in buffer
        u32 left = current_x;
        u32 top = current_y;
        // if (!opt_sdf.parsed()) {
        left += font_options.padding_px;
        top += font_options.padding_px;
        //}
        u32 right = left + width;
        u32 bottom = top + height;
        /*if (opt_sdf.parsed()) {
            right += font_options.padding_px * 2;
            bottom += font_options.padding_px * 2;
        }*/

        if (right >= opt_atlas_dim.value() || bottom >= opt_atlas_dim.value()) {
            std::cerr << "With the current parameters not all characters fit on the font atlas\n";
            return EXIT_FAILURE;
        }

        // Covert to relative location for texture coordinates
        const f32 tex_left = f32(left) / f32(opt_atlas_dim.value());
        const f32 tex_right = f32(right) / f32(opt_atlas_dim.value());
        const f32 tex_top = f32(top) / f32(opt_atlas_dim.value());
        const f32 tex_bottom = f32(bottom) / f32(opt_atlas_dim.value());

        glyph_info.tex_top = 1 - tex_top;
        glyph_info.tex_bottom = 1 - tex_bottom;
        glyph_info.tex_right = tex_right;
        glyph_info.tex_left = tex_left;

        /*
        std::cout << fmt::format("'{}' :: l:{} r:{} t:{} b:{}\n",
                char(i),
                tex_left,
                tex_right,
                tex_top,
                tex_bottom);*/

        for (u32 y = 0; y < height; ++y) {
            for (u32 x = 0; x < width; ++x) {
                const u32 buffer_x = current_x + x + font_options.padding_px;
                const u32 buffer_y = current_y + y + font_options.padding_px;
                const u32 glyph_index = (y * width) + x;
                const u32 buffer_index = (buffer_y * opt_atlas_dim.value()) + buffer_x;
                atlas_buffer[buffer_index] = glyph_info.buffer[glyph_index];
            }
        }
        if (opt_sdf.parsed()) {
            RectScope rs;
            rs.x = current_x;
            rs.y = current_y;
            rs.w = width + font_options.padding_px * 2;
            rs.h = height + font_options.padding_px * 2;
            rs.dh = opt_atlas_dim.value();
            rs.dw = opt_atlas_dim.value();
            sdf_rects.push_back(rs);
        }
        current_x += width + font_options.padding_px * 2;
    }
    if (opt_sdf.parsed()) {
        // Generate SDF for every character including padding
        std::unique_ptr<u8> sdf_buffer(new u8[opt_atlas_dim.value() * opt_atlas_dim.value()]);
        SliceMut<u8> sdf_slice(sdf_buffer.get(), opt_atlas_dim.value() * opt_atlas_dim.value());

        TinySDF tsdf;
        for (const auto& sdf_rect : sdf_rects) {
            const f64 scale = opt_sdf_scale.parsed() ? opt_sdf_scale.value()
                                                     : f64(font_options.font_size_px) / 3.0;
            tsdf.process(sdf_rect, atlas_buffer, scale);
            tsdf.copy(sdf_rect, sdf_slice);
        }
        atlas_buffer_ptr = std::move(sdf_buffer);
        atlas_buffer = sdf_slice;
    }

    // Write debug image if requested
    if (opt_debug_image.parsed()) {
        // write png output
        if (!stbi_write_png(opt_debug_image.value_cstr(), opt_atlas_dim.value(),
                            opt_atlas_dim.value(), 1, atlas_buffer.data(), 0)) {
            std::cerr << fmt::format("Could not write atlas image to '{}'\n",
                                     opt_debug_image.value_cstr());
            return EXIT_SUCCESS;
        }
    }

    // flip image for Renderer
    const u32 width_in_bytes = opt_atlas_dim.value();
    const u32 half_height = opt_atlas_dim.value() / 2;

    for (u32 row = 0; row < half_height; row++) {
        u8* top = atlas_buffer.data() + row * width_in_bytes;
        u8* bottom = atlas_buffer.data() + (opt_atlas_dim.value() - row - 1) * width_in_bytes;
        for (u32 col = 0; col < width_in_bytes; col++) {
            std::swap(*top, *bottom);
            top++;
            bottom++;
        }
    }

    graphics::fbs::FontMetaData wrapper;
    auto fbs_render_info = wrapper.mutable_info();

    for (const auto& glyph_info : glyph_infos) {
        const u32 info_index = glyph_info.index - 32;
        auto render_info = fbs_render_info->GetMutablePointer(info_index);
        const auto& glyph_info_i = glyph_info;
        render_info->mutate_width(glyph_info_i.width);
        render_info->mutate_height(glyph_info_i.height);
        render_info->mutate_bearing_x(glyph_info_i.bearing_x);
        render_info->mutate_bearing_y(glyph_info_i.bearing_y);
        render_info->mutate_advance_x(glyph_info_i.advance_x);
        render_info->mutate_tex_top(glyph_info_i.tex_top);
        render_info->mutate_tex_bottom(glyph_info_i.tex_bottom);
        render_info->mutate_tex_left(glyph_info_i.tex_left);
        render_info->mutate_tex_right(glyph_info_i.tex_right);
    }

    // Prepare font header
    graphics::fbs::FontHeader fbs_font_header;
    fbs_font_header.mutate_font_size_px(font_options.font_size_px);
    fbs_font_header.mutate_atlas_width_px(opt_atlas_dim.value());
    fbs_font_header.mutate_atlas_height_px(opt_atlas_dim.value());
    fbs_font_header.mutate_max_width_px(max_width);
    fbs_font_header.mutate_max_height_px(max_height);
    fbs_font_header.mutate_line_height_px(line_height);
    fbs_font_header.mutate_is_monospace(true);
    if (opt_sdf.parsed()) {
        fbs_font_header.mutate_is_sdf(true);
    }

    // Fill up GUID
    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    flatbuffers::FlatBufferBuilder fbs_builder;
    auto fbs_data = fbs_builder.CreateVector(atlas_buffer.data(), atlas_buffer.size());
    auto fbs_font =
        graphics::fbs::CreateFont(fbs_builder, &fbs_guid, &fbs_font_header, &wrapper, fbs_data);

    fbs_builder.Finish(fbs_font, "MFNT");

    os::FileBuilder builder;
    auto r_file = builder.write().create().binary().open(opt_output.value_cstr());
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {} for writing.\n", opt_output.value());
        return EXIT_FAILURE;
    }

    os::File file = r_file.release_value();

    const ByteSlice data_slice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = file.write(data_slice);
    if (!r_write || r_write.value() != data_slice.size()) {
        std::cerr << fmt::format("Failed to write data to {} .\n", opt_output.value());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
