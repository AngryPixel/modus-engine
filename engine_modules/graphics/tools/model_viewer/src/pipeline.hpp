/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <engine/graphics/pipeline/pipeline.hpp>
class ShadedPass final : public graphics::Pass {
   private:
    threed::EffectHandle m_effect;
    assets::AssetHandle m_asset_program;
    Vector<threed::Command> m_commands;

    bool m_enable_diffuse_map = true;
    bool m_enable_normal_map = true;
    bool m_enable_light = true;

   public:
    ShadedPass();

    Result<> initialize(Engine& engine) override;

    Result<> shutdown(Engine& engine) override;

    Result<graphics::ThreedPassPtr, void> build(Engine& engine,
                                                const graphics::PassState& state) override;

    void toggle_normal_map() { m_enable_normal_map = !m_enable_normal_map; }

    void toggle_diffuse_map() { m_enable_diffuse_map = !m_enable_diffuse_map; }

    void toggle_light() { m_enable_light = !m_enable_light; }
};

class ModelViewerPipeline : public engine::graphics::IPipeline {
   private:
    ShadedPass m_pass_shaded;
    Vector<engine::graphics::ThreedPassPtr> m_pipeline;
    Vector<graphics::Renderable> m_renderables;

   public:
    Result<> initialize(engine::Engine& engine);

    Result<> shutdown(engine::Engine& engine);

    threed::Pipeline build(engine::Engine& engine) override;

    void on_compositor_update(engine::Engine& engine,
                              const threed::Compositor& compositor) override;

    void toggle_normal_map() { m_pass_shaded.toggle_normal_map(); }

    void toggle_diffuse_map() { m_pass_shaded.toggle_diffuse_map(); }

    void toggle_light() { m_pass_shaded.toggle_light(); }
};
