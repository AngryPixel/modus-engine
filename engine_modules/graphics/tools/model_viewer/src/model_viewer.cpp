﻿/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat on

#include <engine/event/engine_events.hpp>
#include <engine/gameplay/ecs/components/transform_component.hpp>
#include <engine/gameplay/ecs/entity_manager.hpp>
#include <engine/graphics/components/graphics_component.hpp>
#include <engine/input/input_manager.hpp>
#include <engine/input/input_mapper.hpp>
#include <engine/view.hpp>

#include "os/path.hpp"
#include "pipeline.hpp"

class ArcBall {
   private:
    i32 m_start_x = 0;
    i32 m_start_y = 0;
    i32 m_width = 0;
    i32 m_height = 0;
    glm::quat m_rotation = glm::quat(1.0f, glm::vec3(0.f));

   public:
    ArcBall() = default;

    void begin(const i32 x, const i32 y) {
        m_start_x = x;
        m_start_y = y;
    }

    void update(const i32 x,
                const i32 y,
                const math::Transform& camera,
                const math::Transform& object) {
        MODUS_UNUSED(camera);
        MODUS_UNUSED(object);
        if (m_start_x != x || m_start_y != y) {
            const glm::vec3 va = get_arcball_vector(m_start_x, m_start_y);
            const glm::vec3 vb = get_arcball_vector(x, y);
            const f32 angle = std::min(1.0f, glm::dot(va, vb));
            const glm::vec3 axis_in_camera_coord = glm::cross(va, vb);
            const glm::mat3 camera2object =
                glm::inverse(glm::mat3(camera.to_matrix()) * glm::mat3_cast(object.m_rotation));
            const glm::vec3 axis_in_object_coord = camera2object * axis_in_camera_coord;
            m_rotation *= glm::quat(angle, axis_in_object_coord);
            m_start_x = x;
            m_start_y = y;
        }
    }

    void set_screen_size(const i32 w, const i32 h) {
        m_width = w;
        m_height = h;
    }

    const glm::quat& rotation() const { return m_rotation; }

   private:
    glm::vec3 get_arcball_vector(const i32 x, const i32 y) {
        glm::vec3 arcball_point = glm::vec3(1.0f * f32(x) / f32(m_width) * 2.0f - 1.0f,
                                            1.0f * f32(y) / f32(m_height) * 2.0f - 1.0f, 0);
        arcball_point.y = -arcball_point.y;
        const f32 r = arcball_point.x * arcball_point.x + arcball_point.y * arcball_point.y;
        if (r <= 1.0f) {
            arcball_point.z = sqrtf(1.0f - r);
        } else {
            arcball_point = glm::normalize(arcball_point);
        }
        return arcball_point;
    }
};

enum ViewerInputActions {
    kActionZoom = 0,
    kActionXAxis = 1,
    kActionYAxis = 2,
    kActionDrag = 3,
};

void toggle_diffuse(Engine& engine);

void toggle_normal(Engine& engine);

void toggle_light(Engine& engine);

class ViewerInputContext final : public input::MappedInputContext {
   private:
    f32 m_zoom_distance = 10.0f;
    ArcBall m_arcball;
    math::Transform m_objec_transform;
    graphics::Camera& m_camera;
    bool m_arcball_track = false;

   private:
    void on_zoom(Engine& engine, const engine::input::AxisMap& map) {
        static constexpr f32 kZoomFactor = 5.f;
        m_zoom_distance += map.value_normalized() * kZoomFactor * (engine.tick_sec());
        m_zoom_distance = std::max(0.0f, m_zoom_distance);
        m_camera.transform.set_look_at(glm::vec3(0.f, 0.0f, m_zoom_distance), glm::vec3(0.f),
                                       glm::vec3(0.f, 1.f, 0.f));
    }

   public:
    ViewerInputContext(graphics::Camera& camera) : m_camera(camera) {}

    StringSlice name() const override { return "ViewerInput"; }

    bool handle_event(engine::Engine& engine, const app::InputEvent& event) override {
        const auto& settings = engine.modules().graphics().settings();
        m_arcball.set_screen_size(settings.window_width, settings.window_height);

        if (event.type == app::InputEventType::Keyboard) {
            if (event.keyboard.key == app::InputKey::KeyEscape) {
                engine.quit();
                return true;
            } else if (event.keyboard.key == app::InputKey::KeyD && event.keyboard.is_key_up()) {
                toggle_diffuse(engine);
                return true;
            } else if (event.keyboard.key == app::InputKey::KeyN && event.keyboard.is_key_up()) {
                toggle_normal(engine);
                return true;
            } else if (event.keyboard.key == app::InputKey::KeyL && event.keyboard.is_key_up()) {
                toggle_light(engine);
                return true;
            }

        } else if (event.type == app::InputEventType::MouseButton) {
            if (event.mouse_button.button == app::MouseButton::Left) {
                m_arcball_track = event.mouse_button.is_mouse_down();
                m_arcball.begin(event.mouse_button.x, event.mouse_button.y);
                return true;
            }
        } else if (event.type == app::InputEventType::MouseMove) {
            if (m_arcball_track) {
                m_arcball.update(event.mouse_move.x, event.mouse_move.y, m_camera.transform,
                                 m_objec_transform);
                m_objec_transform.set_rotation(m_arcball.rotation());
                return true;
            }
        }

        return MappedInputContext::handle_event(engine, event);
    }

    void update(modus::engine::Engine& engine) {
        MODUS_UNUSED(engine);

        m_input_mapper.update(engine);
    }

    const math::Transform& object_transform() const { return m_objec_transform; }
};

class ViewerWorld final : public modus::engine::gameplay::GameWorld {
   public:
    using EMType = engine::gameplay::EntityManager<engine::gameplay::TransformComponent,
                                                   engine::graphics::GraphicsComponent>;
    EMType m_entity_manager;
    engine::graphics::GraphicsSystem<EMType> m_graphics_system;
    engine::gameplay::TransformSystem<EMType> m_transform_system;
    engine::gameplay::EntityId m_model_entity;
    engine::graphics::Camera m_camera;
    engine::graphics::AmbientSettings m_ambient_settings;
    math::Transform m_camera_transform;
    engine::assets::AssetHandle m_model_asset;
    engine::graphics::MeshHandle m_model_mesh_handle;
    String m_model_path;
    ViewerInputContext m_input_context;
    event::ListenerHandle m_window_resize_listener;

    ViewerWorld() : m_input_context(m_camera) {}

    const engine::graphics::Camera& main_camera() const override { return m_camera; }

    const engine::graphics::AmbientSettings& ambient_settings() const override {
        return m_ambient_settings;
    }

    Result<void, void> load_assets(engine::Engine& engine) {
        {
            auto r_asset =
                engine.modules()
                    .assets()
                    .manager()
                    .load_immediate_typed<engine::graphics::ModelAsset>(engine, m_model_path);
            if (!r_asset) {
                MODUS_LOGE("Failed to load model\n");
                return Error<void>();
            }

            m_model_asset = r_asset.value()->asset_handle();
            m_model_mesh_handle = r_asset.value()->m_mesh_handle;
        }
        return Ok<void>();
    }

    void on_window_resize(Engine& engine, const event::Event& event) {
        MODUS_LOGI("Window Resize Event\n");
        modus_assert(event.event_handle() ==
                     engine.modules().event().engine_events().window_resized);
        MODUS_UNUSED(engine);
        const WindowResizeEvent& window_resize_event = static_cast<const WindowResizeEvent&>(event);
        m_camera.frustum.set_aspect_ratio(f32(window_resize_event.m_width) /
                                          f32(window_resize_event.m_height));
    }

    Result<void, void> initialize(engine::Engine& engine) override {
        if (!load_assets(engine)) {
            MODUS_LOGE("Failed to load meshes\n");
            return Error<void>();
        }

        m_window_resize_listener =
            engine.modules()
                .event()
                .manager()
                .register_event_listener(
                    engine.modules().event().engine_events().window_resized,
                    event::EventListenerBuilder::build<ViewerWorld, &ViewerWorld::on_window_resize>(
                        this))
                .value_or_panic("Failed to register event handler");

        // Create entities
        if (!m_entity_manager.initialize(engine, 4)) {
            return Error<void>();
        }

        auto entity_create_fn = [this](const StringSlice& name) -> engine::gameplay::EntityId {
            auto r_entity_create = m_entity_manager.create(name);
            if (!r_entity_create) {
                modus_panic("Failed to create entity");
            }

            auto r_graphics_component =
                m_entity_manager.add_component<engine::graphics::GraphicsComponent>(
                    r_entity_create.value());
            if (!r_graphics_component) {
                modus_panic("Failed to create graphics component");
            }

            auto r_transform_component =
                m_entity_manager.add_component<engine::gameplay::TransformComponent>(
                    r_entity_create.value());
            if (!r_transform_component) {
                modus_panic("Failed to create transform component");
            }

            return r_entity_create.value();
        };

        m_model_entity = entity_create_fn("Model");

        NotMyPtr<engine::graphics::GraphicsComponent> icosphere_g =
            m_entity_manager.component<engine::graphics::GraphicsComponent>(m_model_entity)
                .value_or_panic();

        const engine::graphics::MeshDB& mesh_db = engine.modules().graphics().mesh_db();

        const engine::graphics::MaterialDB material_db = engine.modules().graphics().material_db();

        // auto r_mesh_instance =
        auto r_mesh_instance = mesh_db.new_instance(m_model_mesh_handle);
        if (!r_mesh_instance) {
            return Error<void>();
        }
        icosphere_g->mesh_instance = r_mesh_instance.value();

        const auto& graphics_settings = engine.modules().graphics().settings();

        m_camera.frustum.set_aspect_ratio(graphics_settings.framebuffer_width /
                                          graphics_settings.framebuffer_height);

        m_ambient_settings.colour = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);

        if (!engine.modules().input().manager().add_context(m_input_context)) {
            MODUS_LOGE("Failed to add input context\n");
            return Error<void>();
        }

        return Ok<void>();
    }

    Result<void, void> shutdown(engine::Engine& engine) override {
        engine.modules().graphics().unset_pipeline();

        if (!engine.modules().input().manager().remove_context(m_input_context)) {
            MODUS_LOGE("Failed to remove input context\n");
            return Error<void>();
        }

        if (!engine.modules().event().manager().unregister_event_listner(
                engine.modules().event().engine_events().window_resized,
                m_window_resize_listener)) {
            MODUS_LOGW("Failed to unregister window resize event handler");
        }
        m_window_resize_listener = event::ListenerHandle();

        if (!engine.modules().assets().manager().destroy_immediate(engine, m_model_asset)) {
            MODUS_LOGE("Failed to unload mesh asset\n");
        }

        return Ok<void>();
    }

    void tick(engine::Engine& engine) override {
        m_input_context.update(engine);

        auto r_transform = m_entity_manager.component<gameplay::TransformComponent>(m_model_entity);
        if (r_transform) {
            (*r_transform)->set_transform(m_input_context.object_transform());
        }

        // Do entity add/removals from last frame
        m_entity_manager.update(engine);

        // Test transform update
        m_transform_system.update(engine, m_entity_manager, *this);

        // Update graphics components
        m_graphics_system.update(engine, m_entity_manager, *this);

        m_transform_system.clear_dirty_flags(m_entity_manager);
    }

    void tick_fixed(engine::Engine&) override {}

    void visible_objects(engine::graphics::Vector<engine::graphics::Renderable>& output,
                         const engine::graphics::Camera& camera) const override {
        output.reserve(m_entity_manager.entity_count());
        engine::graphics::FrustumEvaluator evaluator(camera);
        m_entity_manager.for_each_matching<engine::graphics::GraphicsComponent>(
            [&output, &evaluator](const EMType&, const engine::gameplay::EntityId,
                                  const engine::graphics::GraphicsComponent& graphics) {
                if (evaluator.is_visible(graphics.mesh_instance.bounding_volume_transformed)) {
                    output.emplace_back(engine::graphics::Renderable{
                        graphics.world_transform, graphics.mesh_instance,
                        graphics.material_instance, engine::graphics::MaterialInstanceRef()});
                }
            });
    }
};

class ModelViewer final : public engine::IGame {
   public:
    engine::NullView m_view;
    ViewerWorld m_world;
    ModelViewerPipeline m_pipeline;
    CmdOptionString m_opt_file;
    CmdOptionStringList m_opt_mount_paths;

   public:
    ModelViewer()
        : m_opt_file("", "--model", "Path to the model to load", ""),
          m_opt_mount_paths("", "--game-paths", "Paths to under game scope") {}

    StringSlice name() const override { return "ModelViewer"; }
    StringSlice name_preferences() const override { return "modus.tools.model_viewer"; }

    Result<void, void> add_cmd_options(modus::CmdOptionParser& parser) override {
        if (!parser.add(m_opt_file)) {
            return Error<void>();
        }
        if (!parser.add(m_opt_mount_paths)) {
            return Error<void>();
        }
        return Ok<void>();
    }

    Result<engine::View*, void> initialize(engine::Engine& engine) override {
        if (!m_opt_file.parsed()) {
            MODUS_LOGE("ModelViewer: No input file specified\n");
            return Error<void>();
        }

        auto& vfs = engine.modules().filesystem().vfs();
        m_world.m_model_path = m_opt_file.value_str();

        // Mount game paths
        for (auto& path : m_opt_mount_paths.value()) {
            vfs::VFSMountParams vfs_params;
            vfs_params.tag = "game";
            vfs_params.path = path;

            if (auto r_vfs = vfs.mount_path(vfs_params); !r_vfs) {
                MODUS_LOGE("Failed to mount game filesystem at: {}\n", path);
                MODUS_LOGE("    Error: {}\n", vfs::vfs_mount_error_to_string(r_vfs.error()));
                return Error<void>();
            } else {
                MODUS_LOGI("Mounted game path: {}\n", path);
            }
        }

        // mount tools shader dir
        {
            vfs::VFSMountParams vfs_params;
            vfs_params.tag = "gpuprog";
            vfs_params.path = MODUS_TOOL_SHADER_DIR;

            if (!vfs.mount_path(vfs_params)) {
                MODUS_LOGE("Failed to mount tool shader filesystem\n");
                return Error<void>();
            }
        }

        if (!m_pipeline.initialize(engine)) {
            MODUS_LOGE("Failed to init pipeline\n");
            return Error<void>();
        }

        engine.modules().graphics().set_pipeline(&m_pipeline);

        if (!m_world.initialize(engine)) {
            return Error<void>();
        }
        engine.modules().gameplay().set_world(engine, &m_world);
        return Ok<engine::View*>(&m_view);
    }

    Result<void, void> shutdown(engine::Engine& engine) override {
        (void)m_pipeline.shutdown(engine);

        return m_world.shutdown(engine);
    }

    void tick(engine::Engine& engine) override { MODUS_UNUSED(engine); }
};

int main(const int argc, const char** argv) {
    ModelViewer game;
    engine::Engine engine;

    auto r_parse = engine.parse_command_options(argc, argv, game);
    if (!r_parse) {
        return EXIT_FAILURE;
    } else if (r_parse.value()) {
        return EXIT_SUCCESS;
    }

    if (!engine.initialize(game)) {
        fprintf(stderr, "Failed to initialize engine\n");
        (void)engine.shutdown();
        return EXIT_FAILURE;
    }

    engine.run();

    if (!engine.shutdown()) {
        fprintf(stderr, "Failed to shutdown engine\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void toggle_diffuse(Engine& engine) {
    ModelViewer* game = static_cast<ModelViewer*>(engine.game());
    game->m_pipeline.toggle_diffuse_map();
}

void toggle_normal(Engine& engine) {
    ModelViewer* game = static_cast<ModelViewer*>(engine.game());
    game->m_pipeline.toggle_normal_map();
}

void toggle_light(Engine& engine) {
    ModelViewer* game = static_cast<ModelViewer*>(engine.game());
    game->m_pipeline.toggle_light();
}
