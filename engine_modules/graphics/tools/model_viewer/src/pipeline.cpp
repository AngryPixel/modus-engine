/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include "pch.h"
// clangformat on

#include <threed/effect.hpp>

#include "pipeline.hpp"

ShadedPass::ShadedPass() : graphics::Pass("ShadedPass") {}

static constexpr u32 kConstantSlotGlobalBlock = 0;
static constexpr u32 kConstantSlotModelMatrix = 1;
static constexpr u32 kConstantSlotNormalMatrix = 2;
static constexpr u32 kConstantSlotMaterialBlock = 3;
static constexpr u32 kConstantSlotMaterialIndex = 4;
static constexpr u32 kConstantSlotEnableNormal = 5;
static constexpr u32 kConstantSlotEnableDiffuse = 6;
static constexpr u32 kConstantSlotEnableLight = 7;

static constexpr u32 kTextureSlotDiffuse = 0;
static constexpr u32 kTextureSlotNormal = 1;

Result<void, void> ShadedPass::initialize(Engine& engine) {
    auto r_program =
        engine.modules().assets().manager().load_immediate_typed<graphics::GPUProgramAsset>(
            engine, "gpuprog:model_viewer/pass_shaded.gpuprog");
    if (!r_program) {
        MODUS_LOGE("Failed to load gpu program\n");
        return Error<void>();
    }

    m_asset_program = (*r_program)->asset_handle();

    threed::Device& device = engine.modules().graphics().device();

    // Texture input
    threed::ProgramTextureInputParam diffuse_map;
    diffuse_map.name = "map_diffuse";
    diffuse_map.texture_format = threed::TextureFormat::R8G8B8;
    diffuse_map.enabled = true;

    threed::ProgramTextureInputParam normal_map;
    normal_map.name = "map_normal";
    normal_map.texture_format = threed::TextureFormat::R8G8B8;
    normal_map.enabled = true;

    // Vertex input
    threed::ProgramDataInputParam data_input;
    data_input.enabled = true;
    data_input.data_type = threed::DataType::Vec3F32;

    threed::ProgramDataInputParam data_input2;
    data_input2.enabled = true;
    data_input2.data_type = threed::DataType::Vec3F32;

    // Constant input
    threed::ProgramConstantInputParam const_global_block;
    const_global_block.name = "Mat";
    const_global_block.enabled = true;
    const_global_block.type = threed::ProgramContanstInputType::Buffer;

    threed::ProgramConstantInputParam const_model_matrix;
    const_model_matrix.type = threed::ProgramContanstInputType::Constant;
    const_model_matrix.data_type = threed::DataType::Mat4F32;
    const_model_matrix.name = "model";
    const_model_matrix.enabled = true;

    threed::ProgramConstantInputParam const_material_block;
    const_material_block.type = threed::ProgramContanstInputType::Buffer;
    const_material_block.name = engine::graphics::kMaterialConstantBlockName;
    const_material_block.enabled = true;

    threed::ProgramConstantInputParam const_material_index;
    const_material_index.type = threed::ProgramContanstInputType::Constant;
    const_material_index.name = engine::graphics::kMaterialConstantIndexName;
    const_material_index.data_type = threed::DataType::I32;
    const_material_index.enabled = true;

    threed::ProgramConstantInputParam const_normal_matrix;
    const_normal_matrix.type = threed::ProgramContanstInputType::Constant;
    const_normal_matrix.data_type = threed::DataType::Mat4F32;
    const_normal_matrix.name = "normal_matrix";
    const_normal_matrix.enabled = true;

    threed::ProgramConstantInputParam const_enable_diffuse;
    const_enable_diffuse.type = threed::ProgramContanstInputType::Constant;
    const_enable_diffuse.data_type = threed::DataType::I32;
    const_enable_diffuse.name = "enable_diffuse";
    const_enable_diffuse.enabled = true;

    threed::ProgramConstantInputParam const_enable_normal;
    const_enable_normal.type = threed::ProgramContanstInputType::Constant;
    const_enable_normal.data_type = threed::DataType::I32;
    const_enable_normal.name = "enable_normal";
    const_enable_normal.enabled = true;

    threed::ProgramConstantInputParam const_enable_light;
    const_enable_light.type = threed::ProgramContanstInputType::Constant;
    const_enable_light.data_type = threed::DataType::I32;
    const_enable_light.name = "enable_light";
    const_enable_light.enabled = true;

    threed::SamplerState diffuse_sampler;
    diffuse_sampler.filter_near = threed::TextureFilter::LinearMipMapLinear;

    // Effect
    threed::EffectCreateParams effect_params;
    effect_params.program.handle = r_program.value()->m_program;
    effect_params.program.inputs.textures[kTextureSlotDiffuse] = diffuse_map;
    effect_params.program.inputs.textures[kTextureSlotNormal] = normal_map;
    effect_params.program.inputs.data[graphics::kVertexSlot] = data_input;
    effect_params.program.inputs.data[graphics::kNormalSlot] = data_input2;
    effect_params.program.inputs.constants[kConstantSlotGlobalBlock] = const_global_block;
    effect_params.program.inputs.constants[kConstantSlotModelMatrix] = const_model_matrix;
    effect_params.program.inputs.constants[kConstantSlotMaterialBlock] = const_material_block;
    effect_params.program.inputs.constants[kConstantSlotMaterialIndex] = const_material_index;
    effect_params.program.inputs.constants[kConstantSlotNormalMatrix] = const_normal_matrix;
    effect_params.program.inputs.constants[kConstantSlotEnableDiffuse] = const_enable_diffuse;
    effect_params.program.inputs.constants[kConstantSlotEnableNormal] = const_enable_normal;
    effect_params.program.inputs.constants[kConstantSlotEnableLight] = const_enable_light;
    effect_params.state.depth_stencil.depth.enabled = true;

    effect_params.state.raster.cull_mode = threed::CullMode::Back;
    effect_params.state.raster.cull_enabled = true;
    effect_params.state.raster.face_counter_clockwise = true;

    effect_params.state.sampler[kTextureSlotDiffuse] = diffuse_sampler;

    auto r_effect = device.create_effect(effect_params);
    if (!r_effect) {
        MODUS_LOGE("Failed to create effect!: {}\n", r_effect.error());
        return Error<void>();
    }
    m_effect = *r_effect;
    return Ok<void>();
}

Result<void, void> ShadedPass::shutdown(Engine& engine) {
    if (!engine.modules().assets().manager().destroy_immediate(engine, m_asset_program)) {
        return Error<void>();
    }

    return Ok<void>();
}

Result<graphics::ThreedPassPtr, void> ShadedPass::build(Engine& engine,
                                                        const graphics::PassState& state) {
    m_commands.clear();

    const graphics::MeshDB& mesh_db = engine.modules().graphics().mesh_db();
    const graphics::AmbientSettings& ambient_settings =
        engine.modules().gameplay().world()->ambient_settings();
    m_commands.clear();
    m_pass.frame_buffer = state.framebuffer.handle;
    m_pass.state.clear.clear_colour = true;
    m_pass.state.clear.colour = ambient_settings.colour;
    m_pass.viewport.width = state.framebuffer.width_px;
    m_pass.viewport.height = state.framebuffer.height_px;
    m_pass.state.clear.clear_depth_stencil = true;

    auto& device = engine.modules().graphics().device();

    auto const_enable_diffuse = device.upload_constant(i32(m_enable_diffuse_map));
    auto const_enable_normal = device.upload_constant(i32(m_enable_normal_map));

    auto const_enable_light = device.upload_constant(i32(m_enable_light));

    for (const graphics::Renderable& renderable : state.visible_list) {
        // Upload constant
        threed::ConstantHandle constant_model_matrix_handle =
            device.upload_constant(renderable.world_transform.to_matrix());

        const glm::mat4 normal_matrix =
            glm::transpose(glm::inverse(state.engine_globals.constant_block().mat_view *
                                        renderable.world_transform.to_matrix()));
        threed::ConstantHandle constant_normal_matrix = device.upload_constant(normal_matrix);

        auto r_mesh = mesh_db.get(renderable.mesh_instance.mesh);
        modus_assert(r_mesh);
        if (!r_mesh) {
            continue;
        }
        const graphics::Mesh& mesh = *(r_mesh.value());

        const auto& material_db = engine.modules().graphics().material_db();
        const auto material_block_handle = material_db.material_buffer();
        u32 index = 0;
        for (auto& submesh : mesh.sub_meshes) {
            if (submesh.enabled) {
                const engine::graphics::Material& material =
                    material_db.get_material_or_default(submesh.material_handle);
                threed::ConstantHandle model_material_index =
                    device.upload_constant(material.m_material_index);
                threed::Command command;
                command.effect = m_effect;
                command.inputs.textures[kTextureSlotDiffuse] = material.m_map.diffuse;
                command.inputs.textures[kTextureSlotNormal] = material.m_map.normal;
                command.inputs.constants[kConstantSlotGlobalBlock] =
                    state.engine_globals.constant_buffer();
                command.inputs.constants[kConstantSlotModelMatrix] = constant_model_matrix_handle;
                command.inputs.constants[kConstantSlotMaterialBlock] = material_block_handle;
                command.inputs.constants[kConstantSlotMaterialIndex] = model_material_index;
                command.inputs.constants[kConstantSlotNormalMatrix] = constant_normal_matrix;
                command.inputs.constants[kConstantSlotEnableNormal] = const_enable_normal;
                command.inputs.constants[kConstantSlotEnableDiffuse] = const_enable_diffuse;
                command.inputs.constants[kConstantSlotEnableLight] = const_enable_light;
                command.draw_state.primitive = mesh.sub_meshes[index].primitive;
                command.drawable = mesh.sub_meshes[index].drawable;
                m_commands.push_back(command);
                ++index;
            }
        }
    }

    m_pass.commands = make_slice(m_commands);
    return Ok<graphics::ThreedPassPtr>(&m_pass);
}

Result<void, void> ModelViewerPipeline::initialize(engine::Engine& engine) {
    if (!m_pass_shaded.initialize(engine)) {
        return Error<void>();
    }
    return Ok<void>();
}

Result<void, void> ModelViewerPipeline::shutdown(engine::Engine& engine) {
    (void)m_pass_shaded.shutdown(engine);
    return Ok<void>();
}

threed::Pipeline ModelViewerPipeline::build(engine::Engine& engine) {
    m_pipeline.clear();
    m_renderables.clear();

    const ModuleGameplay& gameplay = engine.modules().gameplay();
    NotMyPtr<const gameplay::GameWorld> world = gameplay.world();
    if (!world) {
        modus_panic("There is no game world!");
    }

    const graphics::Camera& world_camera = world->main_camera();
    world->visible_objects(m_renderables, world_camera);

    // Retrieve all visible objects from the game world

    const threed::Compositor& compositor = engine.modules().graphics().default_compositor();

    const auto frame_globals = engine.modules().graphics().frame_globals();
    if (!frame_globals) {
        MODUS_LOGE("No FrameGlobals avaialble\n");
        return threed::Pipeline();
    }
    // Prepare PassState
    const graphics::PassState state{
        make_slice(m_renderables),
        *this,
        **frame_globals,
        world_camera,
        {compositor.frame_buffer(), u32(compositor.width()), u32(compositor.height())}};

    if (auto r_pass = m_pass_shaded.build(engine, state); r_pass && *r_pass) {
        m_pipeline.push_back(*r_pass);
    }

    return make_slice(m_pipeline);
}

void ModelViewerPipeline::on_compositor_update(engine::Engine& engine,
                                               const threed::Compositor& compositor) {
    MODUS_UNUSED(engine);
    MODUS_UNUSED(compositor);
}
