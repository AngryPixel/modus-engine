/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clangformat off
#include <pch.h>
// clanformat on

#include "gen_image.hpp"
#include "loader_astc.hpp"
#include "loader_pkm.hpp"
#include "loader_stb.hpp"

/// Write one image to the file.
Result<> write_img_file(const RawImage& image, IByteWriter& writer) {
    const u8 padding_bytes[4] = {0, 0, 0, 0};
    const ByteSlice image_slice = ByteSlice(image.source->data(), image.size_input);
    usize offset = 0;

    if (!image.skip_row_by_row_writing) {
        while (offset < image.size_input) {
            auto r_bytes_written = writer.write(image_slice.sub_slice(offset, image.row_stride));
            if (!r_bytes_written || r_bytes_written.value() != image.row_stride) {
                return Error<>();
            }

            if (image.padding != 0) {
                auto r_bytes_written_padding =
                    writer.write(ByteSlice(padding_bytes, image.padding));
                if (!r_bytes_written_padding || r_bytes_written_padding.value() != image.padding) {
                    return Error<>();
                }
            }

            offset += image.row_stride;
        }
    } else {
        auto r_bytes_written = writer.write(image_slice);
        if (!r_bytes_written || r_bytes_written.value() != image_slice.size()) {
            return Error<>();
        }
    }
    return Ok<>();
}
struct T2ArrayValidationFailure {
    u32 mip_level;
    u32 input_idx;
};
static Result<void, T2ArrayValidationFailure> validate_inputs_as_texture_array(
    const Slice<RawImage> inputs,
    const u32 input_count) {
    const u32 mip_levels = inputs.size() / input_count;

    for (u32 i = 1; i < mip_levels; i++) {
        modus_assert(i < inputs.size());
        const RawImage& original = inputs[i];
        for (u32 c = 1; c < input_count; c++) {
            const u32 input_index = (mip_levels * c) + i;
            modus_assert(input_index < inputs.size());
            const RawImage& compare = inputs[input_index];

            if (!(original.height == compare.height && original.width == compare.width &&
                  original.depth == 1 && compare.depth == 1 && original.format == compare.format)) {
                return Error(T2ArrayValidationFailure{i, c});
            }
        }
    }
    return Ok<>();
}

static void update_texture_array_depths(SliceMut<RawImage> inputs,
                                        SliceMut<engine::graphics::fbs::ImageMipMapHeader> mips,
                                        const u32 input_count) {
    const u32 mip_levels = inputs.size() / input_count;
    for (u32 c = 0; c < input_count; c++) {
        for (u32 i = 0; i < mip_levels; i++) {
            const u32 input_index = (mip_levels * c) + i;
            modus_assert(input_index < inputs.size());
            RawImage& input = inputs[input_index];
            auto& mipmap_header = mips[input_index];
            input.depth = c;
            mipmap_header.mutate_depth(c);
        }
    }
}

Result<> generate_image(const GenImageOptions& options) {
    if (options.type == threed::TextureType::TCubeMap && options.args.size() != 6) {
        std::cerr << "Cubemap type needs exactly 6 images";
        return Error<>();
    }

    if (options.type == threed::TextureType::T2DArray &&
        options.args.size() % options.num_input_files != 0) {
        std::cerr << "Invalid number of files for texture array generation. The images have "
                     "different sizes or mip map levels";
        return Error<>();
    }

    Slice<String> arg_slice = options.args;

    if (options.verbose) {
        std::cout << fmt::format("Generating {} from files:\n", options.output);
        for (auto& arg : arg_slice) {
            std::cout << fmt::format("    {}\n", arg);
        }
    }

    os::FileBuilder builder;
    auto r_file = builder.write().create().binary().open(options.output);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open {} for writing.\n", options.output);
        return Error<>();
    }

    os::File file = r_file.release_value();

    flatbuffers::FlatBufferBuilder fbs_builder(2048);
    std::vector<RawImage> inputs;
    std::vector<engine::graphics::fbs::ImageMipMapHeader> mipmaps;
    RamStream ram_stream;

    usize offset = 0;
    // Load all images into memory
    mipmaps.resize(arg_slice.size());
    inputs.reserve(arg_slice.size());
    threed::TextureFormat format = threed::TextureFormat::R8G8B8;
    for (usize i = 0; i < arg_slice.size(); ++i) {
        const StringSlice path = arg_slice[i];
        Result<RawImage, void> r_load = Error<>();
        if (path.ends_with(".astc")) {
            r_load = load_image_astc(arg_slice[i].c_str(), options.srgb);
        } else if (path.ends_with(".pkm")) {
            r_load = load_image_pkm(arg_slice[i].c_str(), options.srgb);
        } else {
            r_load = load_image_stb(arg_slice[i].c_str(), options.srgb);
        }

        if (!r_load) {
            return Error<>();
        }
        const RawImage& image = r_load.value();

        // Generate mipmap header
        auto& mipmap_header = mipmaps[i];
        mipmap_header = engine::graphics::fbs::ImageMipMapHeader(
            image.width, image.height, image.depth, image.size_output, offset);
        if (i == 0) {
            format = image.format;
        } else {
            if (image.format != format) {
                std::cerr << fmt::format("Image {} does have the same format as image 0!\n", i);
                return Error<>();
            }

            if (options.type == threed::TextureType::TCubeMap) {
                const RawImage& input_0 = inputs[0];
                if (image.width != input_0.width || image.height != input_0.height ||
                    image.depth != input_0.depth) {
                    std::cerr << fmt::format(
                        "Image {} does not have the same size as image 0. This "
                        "is a requirment for generating a cubemap.\n",
                        i);
                    return Error<>();
                }
            }
        }
        offset += image.size_output;
        inputs.push_back(r_load.release_value());
    }

    // Valid image array
    if (options.type == threed::TextureType::T2DArray) {
        if (auto r = validate_inputs_as_texture_array(make_slice(inputs), options.num_input_files);
            !r) {
            std::cerr << fmt::format(
                "Input image {:02} at mip level {:02} does not have the same type as the first "
                "image at the same mip level\n",
                r.error().mip_level, r.error().input_idx);
            return Error<>();
        }
        update_texture_array_depths(make_slice_mut(inputs), make_slice_mut(mipmaps),
                                    options.num_input_files);
    }

    // write data to ram stream
    for (auto& mipmap : inputs) {
        auto r_write = write_img_file(mipmap, ram_stream);
        if (!r_write) {
            return Error<>();
        }
    }
    // NOLINTNEXTLINE
    auto mip_maps = fbs_builder.CreateVectorOfStructs(mipmaps);
    const ByteSlice image_data_slice = ram_stream.as_bytes();
    auto data = fbs_builder.CreateVector(image_data_slice.data(), image_data_slice.size());

    // Fill up GUID
    modus::core::fbs::GUID fbs_guid;
    auto fbs_guid_array = fbs_guid.mutable_value();
    const ByteSlice guid_slice = options.guid.as_bytes();
    for (usize i = 0; i < guid_slice.size(); ++i) {
        fbs_guid_array->Mutate(i, guid_slice[i]);
    }

    u32 mipmap_size = 0;
    u32 depth = 1;
    if (options.type == threed::TextureType::T2DArray) {
        mipmap_size = inputs.size() / options.num_input_files;
        depth = options.num_input_files;
    } else {
        mipmap_size = inputs.size();
    }

    engine::graphics::fbs::ImageBuilder image_builder(fbs_builder);
    image_builder.add_guid(&fbs_guid);
    image_builder.add_format(u8(format));
    image_builder.add_type(u8(options.type));
    image_builder.add_mip_map_count(u16(mipmap_size));
    image_builder.add_mips(mip_maps);
    image_builder.add_data(data);
    image_builder.add_depth(depth);
    image_builder.add_width(inputs[0].width);
    image_builder.add_height(inputs[0].height);
    auto fbs_image = image_builder.Finish();

    fbs_builder.Finish(fbs_image, "MIMG");

    const ByteSlice fbs_builder_slice =
        ByteSlice(fbs_builder.GetBufferPointer(), fbs_builder.GetSize());

    auto r_write = file.write(fbs_builder_slice);
    if (!r_write || r_write.value() != fbs_builder_slice.size()) {
        std::cerr << fmt::format("Failed to write flatbuffer structure to file: {}\n",
                                 options.output);
        return Error<>();
    }
    return Ok<>();
}
