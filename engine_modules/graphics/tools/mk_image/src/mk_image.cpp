/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include "compress_astc.hpp"
#include "compress_etc2.hpp"
#include "gen_image.hpp"
#include "resize_image.hpp"

using namespace modus;

static Result<> dump_info(const char* path) {
    auto r_file = os::FileBuilder().read().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Failed to open file '{}'\n", path);
        return Error<>();
    }

    RamStream ram_stream;
    auto r_read = ram_stream.populate_from_stream(r_file.value());
    if (!r_read) {
        std::cerr << fmt::format("Failed to read '{}'\n", path);
        return Error<>();
    }

    auto r_image = engine::graphics::Image::from_bytes(ram_stream.as_bytes());
    if (!r_image) {
        std::cerr << fmt::format("Failed to load '{}'\n", path);
        return Error<>();
    }

    const engine::graphics::Image& image = r_image.value();

    std::cout << fmt::format("Image {}:\n", path)
              << fmt::format("    guid     : {}\n", image.guid())
              << fmt::format("    type     : {}\n",
                             threed::texture_type_to_str(image.texture_type()))
              << fmt::format("    format   : {}\n",
                             threed::texture_format_to_str(image.texture_format()))
              << fmt::format("    width    : {}\n", image.width())
              << fmt::format("    height   : {}\n", image.height())
              << fmt::format("    depth    : {}\n", image.depth())
              << fmt::format("    #mipmaps : {}\n", image.mip_map_count());

    for (usize d = 0; d < image.depth(); ++d) {
        for (usize i = 0; i < image.mip_map_count(); ++i) {
            auto opt_mip_map = image.mip_map(i, d);
            if (!opt_mip_map) {
                std::cerr << fmt::format("Failed to access mip map number {} with depth {}\n", i,
                                         d);
                return Error<>();
            }
            const engine::graphics::ImageMipMap& mip_map = *opt_mip_map;
            std::cout << fmt::format("    d {:02} mipmap {:02}:\n", d, i)
                      << fmt::format("        width : {}\n", mip_map.width)
                      << fmt::format("        height: {}\n", mip_map.height)
                      << fmt::format("        depth : {}\n", mip_map.depth)
                      << fmt::format("        offset: {}\n", mip_map.offset)
                      << fmt::format("        size  : {}\n", mip_map.data.size());
        }
    }
    return Ok<>();
}

// NOTE: Astc cubemaps needs to be inverted before processing as the cubemaps
// do not require the images to be flipped to be displayed correctly.
// Alternatively we can also change the coordinates of the shader and
// the order in which the images are processed so this is not really required.
// I choose the latter
// This is the original order: kCubeMapOrder[kCubeMapFacesCount] = {
//    "right_", "left_", "top_", "bottom_", "front_", "back_"};
// And we just make adjustments for the flipping below
static constexpr usize kCubeMapFacesCount = 6;
static const char* kCubeMapOrder[kCubeMapFacesCount] = {"_right", "_left",  "_bottom",
                                                        "_top",   "_front", "_back"};
static const StringSlice kIntro = "modus_mk_image [options] -o output <FILES>";
static const StringSlice kType2D = "2d";
static const StringSlice kTypeCubeMap = "cube_map";
static const StringSlice kType2DArray = "2darray";
static const StringSlice kCompressionASTC = "astc";
static const StringSlice kCompressionETC2 = "etc2";

int main(const int argc, const char** argv) {
    // Init log
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::set(log::make_basic_stdout_logger());
#endif
    log::LogService::set_level(log::Level::Debug);

    CmdOptionEmpty opt_info("-d", "--dump-info", "Display information about the files.");
    CmdOptionString opt_output("-o", "--output", "Path for the generated output", "");
    CmdOptionString opt_type("-t", "--type", "Texture type [2d, cube_map, 2darray]", kType2D);
    CmdOptionString opt_compression("-c", "--compress",
                                    "Compress images using [astc|etc2] compressors", "");

    CmdOptionString opt_astc_bin("", "--astc-bin", "Override astcenc binary path", "");
    CmdOptionEmpty opt_astc_block_fix_mode(
        "", "--astc-block-fix", "Apply additional ASTC parameters to reduce block artifacts");
    CmdOptionEmpty opt_astc_fast("", "--astc-fast", "Use a faster but less thorough compression");

    CmdOptionU32 opt_astc_block_x("", "--astc-block-x", "Astc X block size. Default = 4.", 4);
    CmdOptionU32 opt_astc_block_y("", "--astc-block-y", "Astc Y block size. Default = 4.", 4);

    CmdOptionString opt_etcpack_bin("", "--etcpack-bin", "Override etcpack binary path", "");
    CmdOptionString opt_etcpack_format("", "--etcpack-format", "ECT2 texture format", "RGB");
    CmdOptionEmpty opt_etcpack_fast("", "--etcpack-fast",
                                    "Use a faster but less thorough compression");

    CmdOptionEmpty opt_normal("-n", "--normal-map",
                              "Enable special treatment for normal map conversion");

    CmdOptionEmpty opt_srgb("", "--srgb",
                            "Images are supposed to be treated as being in the SRGB color space.");

    CmdOptionEmpty opt_skip_mipmap("", "--skip-resize", "Do not generate mipmaps for image");

    CmdOptionString opt_guid("-g", "--guid", "Use provided guid instead of generating one", "");

    CmdOptionU32 opt_max_w("", "--max-width", "Maximum width in image resize operation", 0);
    CmdOptionU32 opt_max_h("", "--max-height", "Maximum height in image resize operation", 0);

    CmdOptionEmpty opt_verbose("-v", "--verbose", "Enable verbose options");
    CmdOptionEmpty opt_keep_tmp("-k", "--keep-tmp-dir",
                                "Do not remove temporary directory when finished.");

    CmdOptionParser opt_parser;

    opt_parser.add(opt_output).expect("Failed to add cmd option");
    opt_parser.add(opt_type).expect("Failed to add cmd option");
    opt_parser.add(opt_info).expect("Failed to add cmd option");
    opt_parser.add(opt_srgb).expect("Failed to add cmd option");
    opt_parser.add(opt_normal).expect("Failed to add cmd option");
    opt_parser.add(opt_max_w).expect("Failed to add cmd option");
    opt_parser.add(opt_max_h).expect("Failed to add cmd option");
    opt_parser.add(opt_compression).expect("Failed to add cmd option");
    // Astc
    opt_parser.add(opt_astc_bin).expect("Failed to add cmd option");
    opt_parser.add(opt_astc_block_fix_mode).expect("Failed to add cmd option");
    opt_parser.add(opt_astc_fast).expect("Failed to add cmd option");
    opt_parser.add(opt_astc_block_x).expect("Failed to add cmd option");
    opt_parser.add(opt_astc_block_y).expect("Failed to add cmd option");

    // Etc2
    opt_parser.add(opt_etcpack_bin).expect("Failed to add cmd option");
    opt_parser.add(opt_etcpack_format).expect("Failed to add cmd option");
    opt_parser.add(opt_etcpack_fast).expect("Failed to add cmd option");

    opt_parser.add(opt_skip_mipmap).expect("Failed to add cmd option");
    opt_parser.add(opt_guid).expect("Failed to add cmd option");
    opt_parser.add(opt_verbose).expect("Failed to add cmd option");
    opt_parser.add(opt_keep_tmp).expect("Failed to add cmd option");
    auto opt_result = opt_parser.parse(argc, argv);

    if (!opt_result) {
        std::cerr << fmt::format("{}\n", opt_result.error());
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_SUCCESS;
    }

    const auto remaining_args = opt_result.value();
    if (remaining_args.is_empty()) {
        std::cerr << fmt::format("No input file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (!opt_output.parsed() && !opt_info.parsed()) {
        std::cerr << fmt::format("No ouput file path specified.\n");
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_normal.parsed() && opt_srgb.parsed()) {
        std::cerr << "--normal is not compatible with --srgb\n";
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    threed::TextureType type;
    if (opt_type.value() == kType2D) {
        type = threed::TextureType::T2D;
    } else if (opt_type.value() == kTypeCubeMap) {
        type = threed::TextureType::TCubeMap;
    } else if (opt_type.value() == kType2DArray) {
        type = threed::TextureType::T2DArray;
        if (remaining_args.size() < 2) {
            std::cerr << "Texture arrays require at least 2 images\n";
            return EXIT_FAILURE;
        }
    } else {
        std::cerr << fmt::format("Unknown type specified '{}'.\n", opt_type.value());
        std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
        return EXIT_FAILURE;
    }

    if (opt_compression.parsed()) {
        if (opt_compression.value() != kCompressionASTC &&
            opt_compression.value() != kCompressionETC2) {
            std::cerr << fmt::format("Unknown compression type specified '{}'.\n",
                                     opt_compression.value());
            std::cerr << fmt::format("{}", opt_parser.help_string(kIntro));
            return EXIT_FAILURE;
        }
    }

    bool success = true;
    if (opt_info.parsed()) {
        for (const auto& input : remaining_args) {
            if (!dump_info(input)) {
                return EXIT_FAILURE;
            }
        }
    } else {
        GID guid;
        if (!opt_guid.parsed()) {
            auto r_guid = os::guid::generate();
            if (!r_guid) {
                std::cerr << "Failed to generate GUID\n";
                return EXIT_FAILURE;
            }
            guid = *r_guid;
        } else {
            auto r_guid = GID::from_string(opt_guid.value());
            if (!r_guid) {
                std::cerr << fmt::format("GUID '{}' is not a valid GUID type\n", opt_guid.value());
                return EXIT_FAILURE;
            };
            guid = r_guid.value();
        }

        String tmp_dir;
        {
            // Image generation time!!
            auto r_tmp_dir = os::Path::os_temp_path();
            if (!r_tmp_dir) {
                std::cerr << "Failed to get OS temporary directory\n";
                return EXIT_FAILURE;
            }
            auto r_guid = os::guid::generate();
            if (!r_guid) {
                std::cerr << "Failed to generate GUID\n";
                return EXIT_FAILURE;
            }
            GID g = *r_guid;

            tmp_dir = r_tmp_dir.release_value();
            os::Path::join_inplace(tmp_dir, "modus_mk_image");
            os::Path::join_inplace(tmp_dir, modus::format("{}", g));

            if (auto r = os::Path::make_directory(tmp_dir); !r) {
                std::cerr << fmt::format("Failed to create tmp dir: {}\n", tmp_dir);
                return EXIT_FAILURE;
            }
        }

        bool etc2_invert = false;
        Vector<String> resized_files;
        if (type == threed::TextureType::TCubeMap) {
            const StringSlice input = remaining_args[0];
            resized_files.reserve(6);
            const StringSlice path_component = os::Path::remove_extension(input);
            const StringSlice extension = os::Path::get_extension(input);
            for (auto& face : kCubeMapOrder) {
                String face_path = path_component.to_str();
                face_path += face;
                face_path += ".";
                face_path += extension.to_str();

                if (opt_skip_mipmap.parsed() && !opt_max_h.parsed() && !opt_max_w.parsed()) {
                    resized_files.push_back(std::move(face_path));
                    etc2_invert = true;
                } else {
                    GenMipMapOptions mipmap_options;
                    mipmap_options.file = face_path;
                    mipmap_options.tmp_dir = tmp_dir;
                    mipmap_options.srgb = opt_srgb.parsed();
                    mipmap_options.verbose = opt_verbose.parsed();
                    mipmap_options.max_mip_level = 0;
                    if (opt_max_w.parsed()) {
                        mipmap_options.max_width = opt_max_w.value();
                    }
                    if (opt_max_h.parsed()) {
                        mipmap_options.max_height = opt_max_h.value();
                    }

                    auto r_mipmap = generate_mipmaps(mipmap_options);
                    if (!r_mipmap) {
                        success = false;
                    }
                    resized_files.push_back(r_mipmap->at(0));
                }
            }
        } else if (opt_skip_mipmap.parsed()) {
            for (const auto& input : remaining_args) {
                resized_files.push_back(input);
            }
        } else {
            for (const auto& input : remaining_args) {
                GenMipMapOptions mipmap_options;
                mipmap_options.file = input;
                mipmap_options.tmp_dir = tmp_dir;
                mipmap_options.srgb = opt_srgb.parsed();
                mipmap_options.verbose = opt_verbose.parsed();
                if (opt_max_w.parsed()) {
                    mipmap_options.max_width = opt_max_w.value();
                }
                if (opt_max_h.parsed()) {
                    mipmap_options.max_height = opt_max_h.value();
                }

                auto r_mipmap = generate_mipmaps(mipmap_options);
                if (!r_mipmap) {
                    success = false;
                }
                resized_files = r_mipmap.release_value();
            }
        }

        Vector<String> compressed_files;
        if (success && opt_compression.parsed()) {
            compressed_files.reserve(resized_files.size());
            if (opt_compression.value() == kCompressionASTC) {
                for (auto& file : resized_files) {
                    CompressASTCOptions astc_options;
                    astc_options.tmp_dir = tmp_dir;
                    astc_options.input = file;
                    astc_options.srgb = opt_srgb.parsed();
                    astc_options.normal_map = opt_normal.parsed();
                    astc_options.verbose = opt_verbose.parsed();
                    astc_options.fast = opt_astc_fast.parsed();
                    astc_options.block_fix = opt_astc_block_fix_mode.parsed();
                    astc_options.blocks_x = opt_astc_block_x.value();
                    astc_options.blocks_y = opt_astc_block_y.value();
                    if (opt_astc_bin.parsed()) {
                        astc_options.astc_bin = opt_astc_bin.value();
                    }
                    auto r_compress = compress_astc(astc_options);
                    if (!r_compress) {
                        success = false;
                        break;
                    }
                    compressed_files.push_back(r_compress.release_value());
                }
            } else if (opt_compression.value() == kCompressionETC2) {
                for (auto& file : resized_files) {
                    CompressETC2Options etc2_options;
                    etc2_options.tmp_dir = tmp_dir;
                    etc2_options.input = file;
                    etc2_options.srgb = opt_srgb.parsed();
                    etc2_options.normal_map = opt_normal.parsed();
                    etc2_options.verbose = opt_verbose.parsed();
                    etc2_options.fast = opt_etcpack_fast.parsed();
                    etc2_options.invert = etc2_invert;
                    if (opt_etcpack_bin.parsed()) {
                        etc2_options.etcpack_bin = opt_etcpack_bin.value();
                    }
                    etc2_options.format = opt_etcpack_format.value();
                    auto r_compress = compress_etc2(etc2_options);
                    if (!r_compress) {
                        success = false;
                        break;
                    }
                    compressed_files.push_back(r_compress.release_value());
                }
            }
        } else {
            compressed_files = std::move(resized_files);
        }

        if (success) {
            GenImageOptions options;
            options.output = opt_output.value();
            options.guid = guid;
            options.srgb = opt_srgb.parsed();
            options.type = type;
            options.num_input_files =
                (type == threed::TextureType::T2DArray) ? remaining_args.size() : 1;
            options.args = make_slice(compressed_files);
            options.verbose = opt_verbose.parsed();
            auto r_gen = generate_image(options);
            if (!r_gen) {
                success = false;
            }
        }
        if (!tmp_dir.empty() && !opt_keep_tmp.parsed()) {
            if (auto r = os::Path::remove(tmp_dir); !r) {
                std::cerr << fmt::format("Failed to cleanup tmp dir: {}\n", tmp_dir);
            }
        }
    }
    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}
