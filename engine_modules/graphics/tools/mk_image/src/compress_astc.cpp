/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clangformat off
#include <pch.h>
// clangformat on

#include "compress_astc.hpp"

static constexpr const char* kAstcEnconderBin = "astcenc";

Result<String, void> compress_astc(const CompressASTCOptions& options) {
    String output_path;
    const StringSlice image_name = os::Path::get_basename(options.input);
    os::Path::join_inplace(output_path, options.tmp_dir, image_name);
    output_path += ".astc";

    std::string astc_block_format = fmt::format("{}x{}", options.blocks_x, options.blocks_y);
    std::vector<std::string> args;
    args.reserve(32);
    if (options.astc_bin.has_value() && !options.astc_bin->is_empty()) {
        args.push_back(options.astc_bin.value().to_str().c_str());
    } else {
        args.push_back(kAstcEnconderBin);
    }
    if (options.srgb) {
        args.push_back("-cs");
    } else {
        args.push_back("-c");
    }

    args.push_back(options.input.to_str().c_str());
    args.push_back(output_path.c_str());
    args.push_back(astc_block_format);
    if (options.block_fix) {
        args.push_back("-b");
        args.push_back("1.8");
        args.push_back("-v");
        args.push_back("2");
        args.push_back("1");
        args.push_back("1");
        args.push_back("0");
        args.push_back("25");
        args.push_back("0.1");
        args.push_back("-va");
        args.push_back("1");
        args.push_back("1");
        args.push_back("0");
        args.push_back("25");
        args.push_back("-dblimit");
        args.push_back("60");
    }
    args.push_back("-silentmode");
    if (!options.fast) {
        args.push_back("-thorough");
    } else {
        args.push_back("-fast");
    }

    if (options.normal_map) {
        args.push_back("-normal_percep");
    }

    if (options.verbose) {
        std::cout << fmt::format("Compressing {} as {} with args:\n", options.input, output_path);
        std::cout << args[1];
        for (usize i = 4; i < args.size(); ++i) {
            std::cout << " " << args[i];
        }
        std::cout << "\n";
    }

    std::string std_err_output;
    std::string std_out_output;
    TinyProcessLib::Process process(
        args, "",
        [&std_out_output](const char* bytes, size_t n) { std_out_output = std::string(bytes, n); },
        [&std_err_output](const char* bytes, size_t n) { std_err_output = std::string(bytes, n); });

    if (process.get_exit_status() != 0) {
        std::cerr << fmt::format("Failed to compress as astc: {}\n", options.input);
        std::cerr << "args: ";
        for (auto& arg : args) {
            std::cerr << " " << arg;
        }
        std::cerr << "\n";
        std::cerr << std_out_output;
        std::cerr << std_err_output;
        return Error<>();
    }

    return Ok(std::move(output_path));
}
