/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include "loader_pkm.hpp"

// From https://github.com/Ericsson/ETCPACK/blob/master/source/etcpack.cxx
enum PKMFormats {
    ETC1_RGB = 0,
    ETC2_RGB = 1,
    ETC2_RGBA_OLD = 2,
    ETC2_RGBA = 3,
    ETC2_RGBA1 = 4,
    ETC2_R = 5,
    ETC2_RG = 6,
    ETC2_R_SIGNED = 7,
    ETC2_RG_SIGNED = 8,
    ETC2_sRGB,
    ETC2_sRGBA,
    ETC2_sRGBA1
};

static Result<u16, IOError> read_big_endian_2bytes(modus::os::File& reader) {
    u8 bytes[2];
    u16 block;

    if (auto r = reader.read_exactly(SliceMut(bytes)); !r) {
        return Error(r.error());
    }

    block = 0;
    block |= bytes[0];
    block = block << 8;
    block |= bytes[1];
    return Ok(block);
}

PKMImageSource::PKMImageSource(RamStream<AllocatorLibC>&& stream) : m_stream(std::move(stream)) {}

const u8* PKMImageSource::data() const {
    return m_stream.as_bytes().data();
}

Result<RawImage, void> load_image_pkm(const char* path, const bool is_srgb) {
    MODUS_UNUSED(is_srgb);
    auto r_file = os::FileBuilder().read().binary().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Could not open {}\n", path);
        return Error<>();
    }

    u8 pkm_magic[4];
    u8 pkm_version[2];
    u16 pkm_texture_type;

    // Read & validate header
    if (!r_file->read_exactly(ByteSliceMut(pkm_magic))) {
        std::cerr << fmt::format("Failed to read pkm header");
        return Error<>();
    }
    if (!(pkm_magic[0] == 'P' && pkm_magic[1] == 'K' && pkm_magic[2] == 'M' &&
          pkm_magic[3] == ' ')) {
        std::cerr << fmt::format("file {} is not a valid pkm image\n", path);
        return Error<>();
    }

    // Read version
    if (!r_file->read_exactly(ByteSliceMut(pkm_version))) {
        std::cerr << fmt::format("Failed to read pkm version");
        return Error<>();
    }

    if (!(pkm_version[0] == '2' && pkm_version[1] == '0')) {
        std::cerr << fmt::format("Only pkm files with version 2.0 supported\n");
        return Error<>();
    }

    // Read texture type
    if (auto r = read_big_endian_2bytes(*r_file); !r) {
        std::cerr << fmt::format("Failed to read pkm texture format\n");
        return Error<>();
    } else {
        pkm_texture_type = *r;
    }

    if (pkm_texture_type == ETC1_RGB) {
        std::cerr << fmt::format("ETC1 format is not supported\n");
        return Error<>();
    }

    if (pkm_texture_type == ETC2_RGBA_OLD) {
        std::cerr << fmt::format("File {} is an old PKM format that is no longer supported\n");
        return Error<>();
    }

    // Validate type
    switch (pkm_texture_type) {
        case ETC2_RGB:
        case ETC2_RGBA:
        case ETC2_RGBA1:
        case ETC2_R:
        case ETC2_RG:
        case ETC2_R_SIGNED:
        case ETC2_RG_SIGNED:
        case ETC2_sRGB:
        case ETC2_sRGBA:
        case ETC2_sRGBA1:
            break;

        default:
            std::cerr << fmt::format("Invalid PKM format detected: {}\n", pkm_texture_type);
            return Error<>();
    }

    // Read dimensions
    u16 w, h;
    u16 actual_w, actual_h;

    if (auto r = read_big_endian_2bytes(*r_file); !r) {
        std::cerr << "Failed to read original image width\n";
        return Error<>();
    } else {
        w = *r;
    }

    if (auto r = read_big_endian_2bytes(*r_file); !r) {
        std::cerr << "Failed to read original image height\n";
        return Error<>();
    } else {
        h = *r;
    }

    if (auto r = read_big_endian_2bytes(*r_file); !r) {
        std::cerr << "Failed to read actual image width\n";
        return Error<>();
    } else {
        actual_w = *r;
    }

    if (auto r = read_big_endian_2bytes(*r_file); !r) {
        std::cerr << "Failed to read actual image height\n";
        return Error<>();
    } else {
        actual_h = *r;
    }

    RawImage image;
    image.depth = 1;
    image.width = actual_w;
    image.height = actual_h;
    image.size_input = 0;
    image.size_output = 0;

    bool format_is_srgb = false;

    switch (pkm_texture_type) {
        case ETC2_RGB:
            image.components = 3;
            image.format = modus::threed::TextureFormat::RGB_ETC2;
            break;
        case ETC2_RGBA:
            image.components = 4;
            image.format = modus::threed::TextureFormat::RGBA_ETC2;
            break;
        case ETC2_RGBA1:
            image.components = 4;
            image.format = modus::threed::TextureFormat::RGBA_ETC2_A1;
            break;
        case ETC2_R:
            image.components = 1;
            image.format = modus::threed::TextureFormat::R_EAC11;
            break;
        case ETC2_RG:
            image.components = 2;
            image.format = modus::threed::TextureFormat::RG_EAC11;
            break;
        case ETC2_R_SIGNED:
            image.components = 1;
            image.format = modus::threed::TextureFormat::R_EAC11_SIGNED;
            break;
        case ETC2_RG_SIGNED:
            image.format = modus::threed::TextureFormat::RG_EAC11_SIGNED;
            image.components = 2;
            break;
        case ETC2_sRGB:
            image.components = 3;
            image.format = modus::threed::TextureFormat::RGB_ETC2_srgb;
            break;
        case ETC2_sRGBA:
            image.format = modus::threed::TextureFormat::RGBA_ETC2_srgb;
            image.components = 4;
            break;
        case ETC2_sRGBA1:
            image.components = 4;
            image.format = modus::threed::TextureFormat::RGBA_ETC2_A1_srgb;
            break;
        default:
            std::cerr << fmt::format("Invalid PKM format detected\n");
            return Error<>();
    }
    if (format_is_srgb && !is_srgb) {
        std::cerr << fmt::format("File {} has srgb(a) format, but is flagged as not srgb\n", path);
        return Error<>();
    }
    if (!format_is_srgb && is_srgb) {
        std::cerr << fmt::format("File {} has an rgb(a) format, but is flagged as srgb\n", path);
        return Error<>();
    }

    auto r_position = r_file->position();
    if (!r_position) {
        std::cerr << fmt::format("Failed to get current file offset for {}\n", path);
        return Error<>();
    }

    const modus::threed::TextureFormatDesc desc = modus::threed::texture_format_desc(image.format);

    image.size_input = desc.block_size * u32(std::ceil(w / desc.block_width)) *
                       u32(std::ceil(h / desc.block_height));
    image.size_output = image.size_input;

    RamStream<AllocatorLibC> stream;
    auto r_read = stream.populate_from_stream(r_file.value());
    if (!r_read || r_read.value() != image.size_input) {
        std::cerr << fmt::format("Failed to read contents of etc2 image {} into memory\n", path);
        return Error<>();
    }
    modus_assert(image.size_input % 4 == 0);
    image.source = std::make_unique<PKMImageSource>(std::move(stream));
    image.skip_row_by_row_writing = true;
    return Ok(std::move(image));
}
