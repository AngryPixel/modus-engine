/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include "loader_astc.hpp"

AstcImageSource::AstcImageSource(RamStream<AllocatorLibC>&& stream) : m_stream(std::move(stream)) {}

const u8* AstcImageSource::data() const {
    return m_stream.as_bytes().data();
}

struct AstcHdr {
    u8 magic[4];
    u8 blockdim_x;
    u8 blockdim_y;
    u8 blockdim_z;
    u8 xsize[3]; /* x-size = xsize[0] + xsize[1] + xsize[2] */
    u8 ysize[3]; /* x-size, y-size and z-size are given in texels */
    u8 zsize[3]; /* block count is inferred */
};

static constexpr u8 kAstcMagic[4] = {0x13, 0xAB, 0xA1, 0x5C};

static bool is_valid_header(const u8 magic[4]) {
    return memcmp(magic, kAstcMagic, sizeof(kAstcMagic)) == 0;
}

static void get_astc_size(const AstcHdr& hdr, RawImage& info) {
    // Taken from the Mali OpenGL ES SDK
    /* Merge x,y,z-sizes from 3 chars into one integer value. */
    u32 xsize = hdr.xsize[0] + (hdr.xsize[1] << 8) + (hdr.xsize[2] << 16);
    u32 ysize = hdr.ysize[0] + (hdr.ysize[1] << 8) + (hdr.ysize[2] << 16);
    u32 zsize = hdr.zsize[0] + (hdr.zsize[1] << 8) + (hdr.zsize[2] << 16);

    /* Compute number of blocks in each direction. */
    u32 xblocks = (xsize + hdr.blockdim_x - 1) / hdr.blockdim_x;
    u32 yblocks = (ysize + hdr.blockdim_y - 1) / hdr.blockdim_y;
    u32 zblocks = (zsize + hdr.blockdim_z - 1) / hdr.blockdim_z;

    /* Each block is encoded on 16 bytes, so calculate total compressed image
     * data size. */
    info.size_input = xblocks * yblocks * zblocks << 4;
    info.size_output = info.size_input;
    info.width = xsize;
    info.height = ysize;
    info.depth = zsize;
}

Result<RawImage, void> load_image_astc(const char* path, const bool is_srgb) {
    MODUS_UNUSED(is_srgb);
    auto r_file = os::FileBuilder().read().binary().open(path);
    if (!r_file) {
        std::cerr << fmt::format("Could not open {}\n", path);
        return Error<>();
    }

    AstcHdr header;
    auto r_header_read = r_file.value().read(make_byte_slice_mut(header));
    if (!r_header_read || r_header_read.value() != sizeof(AstcHdr)) {
        std::cerr << fmt::format("Could not read astc header from {}\n", path);
        return Error<>();
    }

    if (!is_valid_header(header.magic)) {
        std::cerr << fmt::format("{} Is not a valid astc file\n", path);
        return Error<>();
    }

    RawImage image;
    get_astc_size(header, image);

    if (image.depth != 1) {
        std::cerr << fmt::format("{} Is not a 2d astc image\n", path);
        return Error<>();
    }

#define INVALID_BLOCK_DIMY                                                                        \
    std::cerr << fmt::format("Invalid block y dimension ({}) for {}\n", header.blockdim_x, path); \
    return Error<>()
    if (header.blockdim_x == 4) {
        if (header.blockdim_y == 4) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC4x4_srgb
                                   : threed::TextureFormat::RGBA_ASTC4x4;
        } else {
            INVALID_BLOCK_DIMY;
        }
    } else if (header.blockdim_x == 5) {
        if (header.blockdim_y == 4) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC5x4_srgb
                                   : threed::TextureFormat::RGBA_ASTC5x4;
        } else if (header.blockdim_y == 5) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC5x5_srgb
                                   : threed::TextureFormat::RGBA_ASTC5x5;
        } else {
            INVALID_BLOCK_DIMY;
        }
    } else if (header.blockdim_x == 6) {
        if (header.blockdim_y == 5) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC6x5_srgb
                                   : threed::TextureFormat::RGBA_ASTC6x5;
        } else if (header.blockdim_y == 6) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC6x6_srgb
                                   : threed::TextureFormat::RGBA_ASTC6x6;
        } else {
            INVALID_BLOCK_DIMY;
        }
    } else if (header.blockdim_x == 8) {
        if (header.blockdim_y == 5) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC8x5_srgb
                                   : threed::TextureFormat::RGBA_ASTC8x5;
        } else if (header.blockdim_y == 6) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC8x6_srgb
                                   : threed::TextureFormat::RGBA_ASTC8x6;
        } else if (header.blockdim_y == 8) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC8x8_srgb
                                   : threed::TextureFormat::RGBA_ASTC8x8;
        } else {
            INVALID_BLOCK_DIMY;
        }
    } else if (header.blockdim_x == 10) {
        if (header.blockdim_y == 5) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC10x5_srgb
                                   : threed::TextureFormat::RGBA_ASTC10x5;
        } else if (header.blockdim_y == 6) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC10x6_srgb
                                   : threed::TextureFormat::RGBA_ASTC10x6;
        } else if (header.blockdim_y == 8) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC10x8_srgb
                                   : threed::TextureFormat::RGBA_ASTC10x8;
        } else if (header.blockdim_y == 10) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC10x10_srgb
                                   : threed::TextureFormat::RGBA_ASTC10x10;
        } else {
            INVALID_BLOCK_DIMY;
        }
    } else if (header.blockdim_x == 12) {
        if (header.blockdim_y == 10) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC12x10_srgb
                                   : threed::TextureFormat::RGBA_ASTC12x10;
        } else if (header.blockdim_y == 12) {
            image.format = is_srgb ? threed::TextureFormat::RGBA_ASTC12x12_srgb
                                   : threed::TextureFormat::RGBA_ASTC12x12;
        } else {
            INVALID_BLOCK_DIMY;
        }
    } else {
        std::cerr << fmt::format("Invalid block x dimension ({}) for {}\n", header.blockdim_x,
                                 path);
        return Error<>();
    }
#undef INVALID_BLOCK_DIMY

    RamStream<AllocatorLibC> stream;
    auto r_read = stream.populate_from_stream(r_file.value());
    if (!r_read || r_read.value() != image.size_input) {
        std::cerr << fmt::format("Failed to read contents of astc image {} into memory\n", path);
        return Error<>();
    }
    modus_assert(image.size_input % 4 == 0);
    image.source = std::make_unique<AstcImageSource>(std::move(stream));
    image.skip_row_by_row_writing = true;
    return Ok(std::move(image));
}
