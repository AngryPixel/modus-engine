/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clangformat off
#include <pch.h>
// clanformat on

#include <iostream>

#include "loader_stb.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

Result<RawImage, void> load_image_stb(const char* path, const bool is_srgb) {
    MODUS_UNUSED(is_srgb);
    if (stbi_is_hdr(path)) {
        std::cerr << fmt::format("This tool does not support converting HDR images:{}\n", path);
        return Error<>();
    }

    // Make sure to flip the images when loading
    stbi_set_flip_vertically_on_load(1);

    i32 width, height, num_components;
    u8* img_data = stbi_load(path, &width, &height, &num_components, 0);

    if (img_data == nullptr) {
        std::cerr << fmt::format("Failed to load '{}': {}\n", path, stbi_failure_reason());
        return Error<>();
    }

    StbImagePtr image_ptr = StbImagePtr(img_data, stbi_image_free);

    if (num_components == 2 || num_components > 4) {
        std::cerr << fmt::format(
            "2 or more than 4 colour channels are currently not "
            "supported\n");
        return Error<>();
    }

    RawImage image;
    image.width = u32(width);
    image.height = u32(height);
    image.depth = 1;
    image.source = std::make_unique<StbImageSource>(std::move(image_ptr));
    if (num_components == 1) {
        image.format = threed::TextureFormat::R8;
    } else if (num_components == 3) {
        image.format = is_srgb ? threed::TextureFormat::R8G8B8_srgb : threed::TextureFormat::R8G8B8;
    } else {
        modus_assert(num_components == 4);
        image.format =
            is_srgb ? threed::TextureFormat::R8G8B8A8_srgb : threed::TextureFormat::R8G8B8A8;
    }

    const modus::threed::TextureFormatDesc& format_desc = threed::texture_format_desc(image.format);
    const u32 row_stride = format_desc.bytes_per_pixel * image.width;

    image.padding = 3 - ((row_stride + 4 - 1) % 4);
    image.row_stride = row_stride;
    image.size_output = (row_stride + image.padding) * image.height * image.depth;
    image.size_input = row_stride * image.height * image.depth;
    return Ok(std::move(image));
}
