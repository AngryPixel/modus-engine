/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
// clang-format off
#include "image_source.hpp"
// clang-format on

#include <core/io/memory_stream.hpp>

class AstcImageSource final : public IImageSource {
   private:
    RamStream<AllocatorLibC> m_stream;

   public:
    AstcImageSource(RamStream<AllocatorLibC>&& stream);

    const u8* data() const override;
};

Result<RawImage, void> load_image_astc(const char* path, const bool is_srgb);
