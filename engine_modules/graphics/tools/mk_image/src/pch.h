/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// clangformat off
#include <core/core_pch.h>
#include <engine/engine_pch.h>
#include <engine/graphics/image_generated.h>
#include <os/os_pch.h>
#include <stb_image.h>
#include <stb_image_resize.h>
#include <stb_image_write.h>

#include <core/getopt.hpp>
#include <core/io/byte_stream.hpp>
#include <core/io/memory_stream.hpp>
#include <engine/graphics/image.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/guid_gen.hpp>
#include <threed/texture.hpp>

#if defined(UNICODE)
#define RESTORE_UNICODE
#undef UNICODE
#endif
#include <process.hpp>
#if defined(RESTORE_UNICODE)
#define UNICODE
#undef RESTORE_UNICODE
#endif

#include "image_source.hpp"
// clangformat on

using namespace modus;
