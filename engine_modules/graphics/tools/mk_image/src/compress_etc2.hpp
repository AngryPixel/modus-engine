/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

struct CompressETC2Options {
    StringSlice tmp_dir;
    StringSlice input;
    StringSlice format;
    Optional<StringSlice> etcpack_bin;
    bool srgb = false;
    bool invert = false;
    bool normal_map = false;
    bool verbose = false;
    bool fast = false;
};

Result<String, void> compress_etc2(const CompressETC2Options& options);
