/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clangformat off
#include <pch.h>
// clangformat on

#include "compress_etc2.hpp"
// Implementation is in loader_stb.cpp
#include <stb_image.h>
// Implementation in resize_image.cpp
#include <stb_image_write.h>

static constexpr const char* kEtcpackBin = "etcpack";

// etcpack has no options to flip images, so we flip them here

static constexpr const char* kFlippedSuffixPNG = "_flipped.png";
static constexpr const char* kFlippedSuffixPKM = "_flipped.pkm";

Result<String, void> flip_image(const StringSlice output_base_name,
                                const char* path,
                                const bool flip) {
    if (stbi_is_hdr(path)) {
        std::cerr << fmt::format("This tool does not support converting HDR images:{}\n", path);
        return Error<>();
    }

    String output = output_base_name.to_str() + kFlippedSuffixPNG;

    // Make sure to flip the images when loading
    stbi_set_flip_vertically_on_load(flip);

    i32 width, height, num_components;
    u8* img_data = stbi_load(path, &width, &height, &num_components, 0);

    if (img_data == nullptr) {
        std::cerr << fmt::format("Failed to load '{}': {}\n", path, stbi_failure_reason());
        return Error<>();
    }

    const auto result = stbi_write_png(output.c_str(), width, height, num_components, img_data, 0);
    stbi_image_free(img_data);

    if (!result) {
        std::cerr << fmt::format("Failed to write flipped image: {}\n", output);
        return Error<>();
    }
    return Ok(std::move(output));
}

Result<String, void> compress_etc2(const CompressETC2Options& options) {
    String output_path;
    const StringSlice image_name = os::Path::get_basename(options.input);
    const StringSlice image_name_no_ext = os::Path::remove_extension(image_name);
    os::Path::join_inplace(output_path, options.tmp_dir);
    String output_name;
    os::Path::join_inplace(output_name, output_path, image_name_no_ext);

    std::vector<std::string> args;
    args.reserve(32);
    if (options.etcpack_bin.has_value() && !options.etcpack_bin->is_empty()) {
        args.push_back(options.etcpack_bin.value().to_str().c_str());
    } else {
        args.push_back(kEtcpackBin);
    }

    if (options.verbose) {
        std::cout << "Generating flipped image...\n";
    }
    auto r_flipped = flip_image(output_name, options.input.to_str().c_str(), options.invert);
    if (!r_flipped) {
        return Error<>();
    }
    if (options.verbose) {
        std::cout << fmt::format("Flipped image generated at: {}\n", *r_flipped);
    }
    output_name.append(kFlippedSuffixPKM);
    args.push_back(r_flipped->c_str());
    args.push_back(output_path.c_str());

    args.push_back("-s");
    if (options.fast) {
        args.push_back("fast");
    } else {
        args.push_back("slow");
    }

    args.push_back("-f");
    args.push_back(options.format.to_str().c_str());

    if (options.verbose) {
        std::cout << fmt::format("Compressing {} as {} with args:\n", options.input, output_path);
        for (usize i = 0; i < args.size(); ++i) {
            std::cout << " " << args[i];
        }
        std::cout << "\n";
    }

    auto r_cwd = os::Path::cwd();
    if (!r_cwd) {
        std::cerr << "Failed to get current working directory\n";
        return Error<>();
    }

    if (!os::Path::cd(options.tmp_dir)) {
        std::cerr << fmt::format("Failed to change directory to {}\n", options.tmp_dir);
        return Error<>();
    }

    std::string std_err_output;
    std::string std_out_output;
    TinyProcessLib::Process process(
        args, "",
        [&std_out_output](const char* bytes, size_t n) { std_out_output = std::string(bytes, n); },
        [&std_err_output](const char* bytes, size_t n) { std_err_output = std::string(bytes, n); });

    if (!os::Path::cd(*r_cwd)) {
        std::cerr << fmt::format("Failed to change directory to {}\n", *r_cwd);
        return Error<>();
    }

    if (process.get_exit_status() != 0) {
        std::cerr << fmt::format("Failed to compress as etc2: {}\n", options.input);
        std::cerr << "args: ";
        for (auto& arg : args) {
            std::cerr << " " << arg;
        }
        std::cerr << "\n";
        std::cerr << std_out_output;
        std::cerr << std_err_output;
        return Error<>();
    }

    return Ok(std::move(output_name));
}
