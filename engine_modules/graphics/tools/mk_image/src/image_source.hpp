/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <core/core_pch.h>
#include <threed/threed_pch.h>

#include <threed/texture.hpp>
#include <threed/types.hpp>

using namespace modus;

class IImageSource {
   public:
    virtual ~IImageSource() = default;
    virtual const u8* data() const = 0;
};

struct RawImage {
    RawImage() = default;
    MODUS_CLASS_DEFAULT_MOVE(RawImage);
    MODUS_CLASS_DISABLE_COPY(RawImage);
    std::unique_ptr<IImageSource> source;
    usize size_input = 0;
    usize size_output = 0;
    u32 width = 0;
    u32 height = 0;
    u32 depth = 0;
    u32 components = 0;
    u32 row_stride = 0;
    u32 padding = 0;
    threed::TextureFormat format = threed::TextureFormat::R8G8B8;
    bool skip_row_by_row_writing = false;
};
