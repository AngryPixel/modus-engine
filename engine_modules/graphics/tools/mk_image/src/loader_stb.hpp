/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

using StbImagePtr = std::unique_ptr<u8, void (*)(void*)>;

class StbImageSource final : public IImageSource {
   private:
    StbImagePtr m_image;

   public:
    StbImageSource(StbImagePtr&& image) : m_image(std::move(image)) {}

    const u8* data() const override { return m_image.get(); }
};

Result<RawImage, void> load_image_stb(const char* path, const bool is_srgb);
