/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */
// clangformat off
#include <pch.h>
// clangformat on
#define STBIR_ASSERT(x) modus_assert_message(x, "stb_image_resize_assert")

#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include <stb_image_resize.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include "loader_stb.hpp"
#include "resize_image.hpp"

struct ResizeOptions {
    NotMyPtr<const RawImage> image;
    String output_path;
    u32 width = 0;
    u32 height = 0;
    bool srgb = false;
};

static constexpr u32 kDivisionFactor = 2;

Result<> do_resize(const ResizeOptions& options) {
    const threed::TextureFormatDesc& format_desc =
        threed::texture_format_desc(options.image->format);
    if (format_desc.bytes_per_pixel > 32) {
        std::cerr << "Don't know how to resize image with more than 8 bytes "
                     "per channel\n";
        return Error<>();
    }

    const u32 row_stride = options.width * format_desc.bytes_per_pixel;
    const u32 image_size = row_stride * options.height;
    const ByteSlice image_data =
        ByteSlice(options.image->source->data(), options.image->size_input);
    std::unique_ptr<u8[]> bytes(new u8[image_size]);

    int result = 0;

    if (!options.srgb) {
        result = stbir_resize_uint8(image_data.data(), options.image->width, options.image->height,
                                    0, bytes.get(), options.width, options.height, 0,
                                    format_desc.n_channels);
    } else {
        // TODO: Handle cases where aplha channel is not the at the end!
        int alpha_flags = 0;
        int alpha_channel = 3;
        if (format_desc.n_bits_alpha == 0) {
            alpha_flags = STBIR_ALPHA_CHANNEL_NONE;
            alpha_channel = 0;
        }

        result = stbir_resize_uint8_srgb(
            image_data.data(), options.image->width, options.image->height, 0, bytes.get(),
            options.width, options.height, 0, format_desc.n_channels, alpha_channel, alpha_flags);
    }

    if (result == 0) {
        std::cerr << fmt::format("Failed to resize image: {}\n", options.output_path);
        return Error<>();
    }

    stbi_flip_vertically_on_write(1);

    // write data to disk
    if (stbi_write_png(options.output_path.c_str(), options.width, options.height,
                       format_desc.n_channels, bytes.get(), row_stride) == 0) {
        std::cerr << fmt::format("Failed to write image: {}\n", options.output_path);
        return Error<>();
    }

    return Ok<>();
}

Result<Vector<String>, void> generate_mipmaps(const GenMipMapOptions& options) {
    MODUS_UNUSED(options);

    auto r_image = load_image_stb(options.file.to_str().c_str(), true);

    if (!r_image) {
        std::cerr << fmt::format("Failed to load image: {}\n", options.file);
        return Error<>();
    }

    Vector<String> image_paths;
    Vector<ResizeOptions> resize_jobs;
    resize_jobs.reserve(20);

    const StringSlice image_name = os::Path::get_basename(options.file);

    const RawImage& image = *r_image;

    u32 width = image.width;
    u32 height = image.height;
    bool apply_max_resize = false;

    if (options.max_width.has_value() || options.max_height.has_value()) {
        while (true) {
            if ((options.max_width.has_value() && (width <= *options.max_width)) ||
                (options.max_height.has_value() && (height <= *options.max_width))) {
                break;
            }
            width /= kDivisionFactor;
            height /= kDivisionFactor;
            apply_max_resize = true;
        }
        if (apply_max_resize) {
            if (options.verbose) {
                std::cout << fmt::format("Limiting max widthxheight to {}x{}\n", width, height);
            }
            ResizeOptions opt;
            opt.image = &image;
            opt.width = width;
            opt.height = height;
            opt.srgb = options.srgb;
            os::Path::join_inplace(opt.output_path, options.tmp_dir,
                                   modus::format("{}-{:04}x{:04}.png", image_name, width, height));
            resize_jobs.emplace_back(opt);
        }
    }

    for (u32 i = 0; width > 32 && height > 32 && i < options.max_mip_level.value_or(i + 1); ++i) {
        width = width / kDivisionFactor;
        height = height / kDivisionFactor;
        ResizeOptions opt;
        opt.image = &image;
        opt.width = width;
        opt.height = height;
        opt.srgb = options.srgb;
        os::Path::join_inplace(opt.output_path, options.tmp_dir,
                               modus::format("{}-{:04}x{:04}.png", image_name, width, height));
        resize_jobs.emplace_back(opt);
    }

    image_paths.reserve(resize_jobs.size() + 1);
    if (!apply_max_resize) {
        image_paths.push_back(options.file.to_str());
    }

    for (auto& resize_job : resize_jobs) {
        if (options.verbose) {
            std::cout << fmt::format("Resizing {} to {:04}x{:04} as {}\n", options.file,
                                     resize_job.width, resize_job.height, resize_job.output_path);
        }
        if (!do_resize(resize_job)) {
            return Error<>();
        }
        image_paths.push_back(resize_job.output_path);
    }
    return Ok(std::move(image_paths));
}
