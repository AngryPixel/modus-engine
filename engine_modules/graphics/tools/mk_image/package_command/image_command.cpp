/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <core/core_pch.h>
#include <os/os_pch.h>
#include <package/command.hpp>
#include <package/status_reporter.hpp>
// clang-format on
#include <os/path.hpp>
#include <os/env.hpp>

namespace modus::graphics {

static constexpr const char* kType = "image";

class ImageCommandCreator;

class ImageCommand final : public package::ICommand {
   public:
    std::vector<std::string> m_args;
    Vector<String> m_dependencies;

   public:
    const char* type() const override { return kType; }

    Result<> execute(package::StatusReporter& reporter,
                     const package::CommandContext&) const override {
        return package::detail::execute_process(m_args, "", reporter);
    }

    u64 command_hash(const package::CommandContext&) const override {
        Hasher64 hasher;
        for (const auto& arg : m_args) {
            hasher.update(arg.data(), arg.size());
        }
        return hasher.digest();
    }

    Vector<String> command_dependencies() const override { return m_dependencies; }
};

static void generate_cubemap_dependency(Vector<String>& dependencies) {
    static const char* kCubeMapOrder[] = {"_right", "_left", "_bottom", "_top", "_front", "_back"};
    modus_assert(dependencies.size() == 1);
    const String input = std::move(dependencies[0]);
    dependencies.clear();
    const StringSlice path_component = os::Path::remove_extension(input);
    const StringSlice extension = os::Path::get_extension(input);
    String face_path;
    for (auto& face : kCubeMapOrder) {
        face_path = path_component.to_str();
        face_path += face;
        face_path += ".";
        face_path += extension.to_str();
        dependencies.push_back(face_path);
    }
}

class ImageCommandCreator final : public package::ICommandCreator {
   public:
    package::CommandPool<ImageCommand, DefaultAllocator> m_commands;
    String m_mk_image_bin;
    String m_etc_pack_bin;

    ImageCommandCreator() : m_commands(16) {}

    const char* type() const override { return kType; }

    Result<> initialize(package::StatusReporter& reporter) override {
        auto r_exec_dir = os::Path::executable_directory();
        if (!r_exec_dir) {
            package::ReportError(reporter, "Failed to get executable directory");
            return Error<>();
        }

#if defined(MODUS_OS_WIN32)
        constexpr const char* kExecName = "modus_mk_image.exe";
#else
        constexpr const char* kExecName = "modus_mk_image";
#endif

        String bin_file = os::Path::join(*r_exec_dir, kExecName);
        if (!os::Path::is_file(bin_file)) {
            package::ReportError(reporter, "Could not find '{}'", bin_file);
            return Error<>();
        }
        m_mk_image_bin = std::move(bin_file);

        auto r_find = os::find_executable("etcpack");
        if (!r_find) {
            package::ReportError(reporter, "Could not locate etcpack executable");
            return Error<>();
        }
        m_etc_pack_bin = r_find.release_value();
        package::ReportVerbose(reporter, "ImageCommandCreator: Found etcpack at {}",
                               m_etc_pack_bin);
        return Ok<>();
    }

    Result<package::ICommand*> create(package::StatusReporter& reporter,
                                      const package::CommandCreateParams& params) override {
        ReportDebug(reporter, "Creating Image Command instance");
        static constexpr const char* kInputElem = "inputs";
        static constexpr const char* kTypeElem = "type";
        static constexpr const char* kFormatElem = "format";
        static constexpr const char* kSRGBlem = "srgb";
        static constexpr const char* kMaxWElem = "max_width";
        static constexpr const char* kMaxHElem = "max_height";
        static constexpr const char* kSkipElem = "skip_mipmap";
        static constexpr const char* kCompression = "compress";

        Vector<String> m_dependencies;
        std::vector<std::string> args;
        args.reserve(16);
        args.push_back(m_mk_image_bin.c_str());

        if (auto r = params.parser.read_opt_string(kFormatElem); !r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        } else {
            args.push_back("--etcpack-format");
            if (*r) {
                if (**r == "RGBA8") {
                    args.push_back(fmt::to_string(**r));
                } else if (**r == "RGB") {
                    args.push_back("RGB");
                } else if (**r == "RG") {
                    args.push_back("RG");
                } else if (**r == "R") {
                    args.push_back("R");
                } else {
                    package::ReportError(reporter, "ImageCommandCreator: Invalid format {}", **r);
                    return Error<>();
                }
            } else {
                args.push_back("RGB");
            }
        }
        if (auto r = params.parser.read_opt_u32(kMaxWElem); r && *r) {
            args.push_back("--max-width");
            args.push_back(fmt::to_string(**r));
        } else if (!r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        }

        if (auto r = params.parser.read_opt_u32(kMaxHElem); r && *r) {
            args.push_back("--max-height");
            args.push_back(fmt::to_string(**r));
        } else if (!r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        }
        if (auto r = params.parser.read_opt_bool(kSkipElem); r && *r) {
            if (**r) {
                args.push_back("--skip-resize");
            }
        } else if (!r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        }

        if (auto r = params.parser.read_opt_bool(kSRGBlem); r && *r) {
            if (**r) {
                args.push_back("--srgb");
            }
        } else if (!r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        }

        if (auto r = params.parser.read_opt_bool(kCompression); r && *r) {
            if (**r) {
                args.push_back("-c");
                args.push_back("etc2");
                args.push_back("--etcpack-fast");
                args.push_back("--etcpack-bin");
                args.push_back(std::string(m_etc_pack_bin));
            }
        } else if (!r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        }

        bool is_cubemap = false;
        if (auto r = params.parser.read_string(kTypeElem); !r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        } else {
            args.push_back("-t");
            if (*r == "cubemap") {
                args.push_back("cube_map");
                is_cubemap = true;
            } else {
                args.push_back(fmt::to_string(*r));
            }
        }

        args.push_back("-o");
        args.push_back(fmt::to_string(params.output_file));

        if (auto r = params.parser.read_string_array(kInputElem); !r) {
            package::ReportError(reporter, "ImageCommandCreator: {}", r.error());
            return Error<>();
        } else {
            if ((*r).empty()) {
                package::ReportError(reporter, "ImageCommandCreator: No input files specified");
                return Error<>();
            }
            if (is_cubemap) {
                const StringSlice input = (*r)[0];
                const String file = os::Path::join(params.current_directory, input);
                m_dependencies.push_back(file);
                generate_cubemap_dependency(m_dependencies);
                args.push_back(fmt::to_string(file));
            } else {
                for (const auto& f : *r) {
                    const String file = os::Path::join(params.current_directory, f);
                    m_dependencies.push_back(file);
                    args.push_back(fmt::to_string(file));
                }
            }
        }

        ImageCommand* command = m_commands.create();
        command->m_args = std::move(args);
        command->m_dependencies = std::move(m_dependencies);
        return Ok<package::ICommand*>(command);
    }

    void shutdown() override { m_commands.clear(); }

    const char* help_string() const override {
        return "Image Command: Create Modus font files.\n"
               "Arguments:\n"
               "    inputs:[str]        Image input files(required)\n"
               "    type:string         Image type  (required)\n"
               "    format:string       Image format RGBA, RBG, RG, R. Default = RGB\n"
               "    srgb:bool           Is SRGB image\n"
               "    max_width:u32       Max Image width px\n"
               "    max_height:u32      Max Image atlas size in pixels (required)\n"
               "    skip_mipmap:bool    Do not generate mipmaps\n";
    }
};

}    // namespace modus::graphics

extern "C" {
MODUS_PACKAGE_COMMAND_EXPORT modus::package::ICommandCreator*
MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME() {
    return new modus::graphics::ImageCommandCreator();
}

MODUS_PACKAGE_COMMAND_EXPORT void MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME(
    modus::package::ICommandCreator* ptr) {
    delete ptr;
}
}
