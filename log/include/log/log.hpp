/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/string_slice.hpp>
#include <log/module_config.hpp>

#if defined(MODUS_LOG_USE_SPDLOG)
#include <spdlog/spdlog.h>
#endif

namespace modus::log {

enum class Level {
    Fatal,
    Error,
    Info,
    Warning,
    Verbose,
    Debug,
};

#if !defined(MODUS_LOG_USE_SPDLOG)
struct MODUS_LOG_EXPORT Logger {
    using LogFn = void (*)(void*, const Level, const fmt::string_view, fmt::format_args);
    void* state;
    LogFn log_fn;
};

template <typename T>
inline Logger make_logger(T& logger) {
    return Logger{&logger, [](void* state, const Level level, const fmt::string_view format_str,
                              fmt::format_args args) {
                      T* t = reinterpret_cast<T*>(state);
                      t->log(level, format_str, args);
                  }};
}

inline Logger make_logger_stateless(Logger::LogFn log_fn) {
    return Logger{nullptr, log_fn};
}

MODUS_LOG_EXPORT Logger make_basic_stdout_logger();
#endif
MODUS_LOG_EXPORT Result<Level, void> log_level_from_str(const StringSlice str);

class MODUS_CORE_EXPORT LogService {
   public:
    static void set_level(const Level l);

    static Level level();

#if !defined(MODUS_LOG_USE_SPDLOG)
    static Optional<Logger> set(Logger logger);

    static Optional<Logger> unset();

    static Optional<Logger> get();

    template <typename... Args>
    static void log(const Level level, const fmt::string_view format, const Args&... args) {
        auto logger = LogService::get();
        if (logger && level <= LogService::level()) {
            logger->log_fn(logger->state, level, format, fmt::make_format_args(args...));
        }
    }
#else
    static void set(const std::shared_ptr<spdlog::logger>& logger);

    static std::shared_ptr<spdlog::logger>& get();
#endif
};
}    // namespace modus::log

#if defined(MODUS_LOG_USE_SPDLOG)
#define MODUS_LOGI(...) modus::log::LogService::get()->info(__VA_ARGS__)
#define MODUS_LOGE(...) modus::log::LogService::get()->error(__VA_ARGS__)
#define MODUS_LOGW(...) modus::log::LogService::get()->warn(__VA_ARGS__)
#define MODUS_LOGD(...) modus::log::LogService::get()->debug(__VA_ARGS__)
#define MODUS_LOGV(...) modus::log::LogService::get()->debug(__VA_ARGS__)
#define MODUS_LOGF(...) modus::log::LogService::get()->critical(__VA_ARGS__)
#else
#define MODUS_LOGI(...) modus::log::LogService::log(modus::log::Level::Info, __VA_ARGS__)
#define MODUS_LOGE(...) modus::log::LogService::log(modus::log::Level::Error, __VA_ARGS__)
#define MODUS_LOGW(...) modus::log::LogService::log(modus::log::Level::Warning, __VA_ARGS__)
#define MODUS_LOGD(...) modus::log::LogService::log(modus::log::Level::Debug, __VA_ARGS__)
#define MODUS_LOGV(...) modus::log::LogService::log(modus::log::Level::Verbose, __VA_ARGS__)
#define MODUS_LOGF(...) modus::log::LogService::log(modus::log::Level::Fatal, __VA_ARGS__)
#endif
