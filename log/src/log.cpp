/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <ctime>
#include <log/log.hpp>

namespace modus::log {

#if !defined(MODUS_LOG_USE_SPDLOG)
static const fmt::string_view k_level_strings[] = {
    "[Fatal  ]: ", "[Error  ]: ", "[Info   ]: ", "[Warning]: ", "[Verbose]: ", "[Debug  ]: "};
#if defined(MODUS_OS_WIN32)
#define modus_localtime(time_t, tm) localtime_s(tm, time_t)
#else
#define modus_localtime(time_t, tm) localtime_r(time_t, tm)
#endif
struct BasicStdoutLogger {
    static void log(void*,
                    const Level level,
                    const fmt::string_view format,
                    fmt::format_args args) {
        modus_assert(level <= Level::Debug);
        fmt::memory_buffer buffer;
        time_t time_now;
        time(&time_now);
        tm time_info;
        modus_localtime(&time_now, &time_info);
        fmt::format_to(buffer, "[{:04}{:02}{:02}_{:02}{:02}{:02}]", 1900 + time_info.tm_year,
                       1 + time_info.tm_mon, time_info.tm_mday, time_info.tm_hour, time_info.tm_min,
                       time_info.tm_sec);

        fmt::format_to(buffer, k_level_strings[static_cast<u32>(level)]);
        fmt::vformat_to(buffer, format, args);
        fwrite(buffer.data(), 1, buffer.size(), stdout);
        if (level == Level::Fatal) {
            fflush(stdout);
            modus_panic("Fatal Log Output");
        }
    }
};

Logger make_basic_stdout_logger() {
    return make_logger_stateless(BasicStdoutLogger::log);
}
#endif

Result<Level, void> log_level_from_str(const StringSlice str) {
    if (str == "verbose") {
        return Ok(Level::Verbose);
    } else if (str == "error") {
        return Ok(Level::Error);
    } else if (str == "warning") {
        return Ok(Level::Warning);
    } else if (str == "info") {
        return Ok(Level::Info);
    } else if (str == "debug") {
        return Ok(Level::Debug);
    }
    return Error<>();
}

#if !defined(MODUS_LOG_USE_SPDLOG)
static Level k_logger_level = Level::Info;
static Optional<Logger> k_active_logger;

void LogService::set_level(const Level l) {
    k_logger_level = l;
}

Level LogService::level() {
    return k_logger_level;
}

Optional<Logger> LogService::set(Logger logger) {
    modus_assert(logger.log_fn != nullptr);
    auto previous_logger = k_active_logger;
    k_active_logger = Optional<Logger>(logger);
    return previous_logger;
}

Optional<Logger> LogService::unset() {
    auto previous_logger = k_active_logger;
    k_active_logger = Optional<Logger>();
    return previous_logger;
}

Optional<Logger> LogService::get() {
    return k_active_logger;
}
#else
static std::shared_ptr<spdlog::logger> g_active_logger = spdlog::default_logger();

void LogService::set(const std::shared_ptr<spdlog::logger>& logger) {
    modus_assert(logger);
    g_active_logger = logger;
}

std::shared_ptr<spdlog::logger>& LogService::get() {
    return g_active_logger;
}

void LogService::set_level(const Level l) {
    switch (l) {
        case Level::Verbose:
            [[fallthrough]];
        case Level::Debug:
            g_active_logger->set_level(spdlog::level::debug);
            break;
        case Level::Info:
            g_active_logger->set_level(spdlog::level::info);
            break;
        case Level::Error:
            g_active_logger->set_level(spdlog::level::err);
            break;
        case Level::Warning:
            g_active_logger->set_level(spdlog::level::warn);
            break;
        case Level::Fatal:
            g_active_logger->set_level(spdlog::level::critical);
            break;
        default:
            modus_panic("Unknown log level");
    }
}

Level LogService::level() {
    const auto log_level = g_active_logger->level();
    switch (log_level) {
        case spdlog::level::debug:
            return Level::Debug;
        case spdlog::level::err:
            return Level::Error;
        case spdlog::level::warn:
            return Level::Warning;
        case spdlog::level::critical:
            return Level::Fatal;
        case spdlog::level::info:
            [[fallthrough]];
        default:
            return Level::Info;
    };
}
#endif

}    // namespace modus::log
