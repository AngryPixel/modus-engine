/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>

namespace modus::package {

class AssetFilter {
   public:
    virtual ~AssetFilter() = default;
    virtual bool apply(const ConstAssetPtr asset) const = 0;
};

class IncudeAllFilter final : public AssetFilter {
   public:
    bool apply(const ConstAssetPtr asset) const override;
};

struct RunnerContext {
    const PackageContext& package_context;
    std::unique_ptr<AssetFilter> filter = modus::make_unique<IncudeAllFilter>();
};

struct RunnerResult {
    u64 num_updated_dependencies = 0;
    u64 num_commands_executed = 0;
};

class IRunner {
   protected:
    IRunner() = default;

   public:
    virtual ~IRunner() = default;

    virtual Result<RunnerResult> execute(StatusReporter& reporter,
                                         const RunnerContext& context) = 0;
};

/// Return Ok(true) if fhis depedency is newer and should be used
Result<bool> check_and_update_dependency_status(StatusReporter& reporter, DependencyPtr dep);

/// Returns Ok(true) if the command of this asset was run
Result<bool> check_and_execute_asset_command(StatusReporter& reporter,
                                             const RunnerContext& context,
                                             AssetPtr asset);

}    // namespace modus::package
