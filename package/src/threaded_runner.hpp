/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>
#include <runner.hpp>

namespace modus::package {

class ThreadedRunner final : public IRunner {
   private:
    const u32 m_num_threads;

   public:
    ThreadedRunner(const u32 num_threads);

    ~ThreadedRunner();

    Result<RunnerResult> execute(StatusReporter& reporter, const RunnerContext& context) override;
};

}    // namespace modus::package
