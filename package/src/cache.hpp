/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>
#include <core/allocator/allocator_libc.hpp>
#include <core/allocator/pool_allocator.hpp>

namespace modus {
class ISeekableReader;
}

namespace modus {
class IByteWriter;
}

namespace modus::package {

struct CacheInput {
    String m_input;
    u64 m_last_time_mod = 0;
    u64 m_hash = 0;
};

struct CacheEntry {
    String m_output;
    String m_type;
    Vector<CacheInput> m_inputs;
};

using CacheEntryPtr = NotMyPtr<CacheEntry>;
using ConstCacheEntryPtr = NotMyPtr<const CacheEntry>;
class Cache {
   private:
    PoolAllocatorTyped<CacheEntry, AllocatorLibC> m_allocator;
    HashMap<String, CacheEntryPtr> m_cache_elements;

   public:
    Cache();
    ~Cache();
    MODUS_CLASS_DISABLE_COPY_MOVE(Cache);

    Result<void, String> load(ISeekableReader& reader);

    Result<CacheEntryPtr> find(const String& output);

    Result<ConstCacheEntryPtr> find(const String& output) const;

    CacheEntryPtr find_or_create(const String& output);

    Result<> save(IByteWriter& writer) const;

    void clear();
};
}    // namespace modus::package
