/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <command_factory.hpp>
#include <package/command.hpp>
#include <commands/touch_command.hpp>
#include <commands/process_command.hpp>
#include <status_reporter_internal.hpp>
#include <rapidjson_utils.hpp>

namespace modus::package {

class NullCommand final : public ICommand {
    const char* type() const override { return "null"; }

    u64 command_hash(const CommandContext&) const override { return 0; }

    Result<void> execute(StatusReporter& reporter, const CommandContext& ctx) const override {
        ReportDebug(reporter, "Running null command for: {}", ctx.output);
        return Ok<>();
    }

    Vector<String> command_dependencies() const override { return {}; }
};

class NullCommandCreator final : public ICommandCreator {
   public:
    const char* type() const override { return "null"; }

    Result<> initialize(StatusReporter& reporter) override {
        ReportDebug(reporter, "Initializing null command");
        return Ok<>();
    }

    void shutdown() override {}

    Result<ICommand*> create(StatusReporter& reporter, const CommandCreateParams&) override {
        static NullCommand kCommand;
        ReportDebug(reporter, "Creating null command");
        return Ok<ICommand*>(&kCommand);
    }

    const char* help_string() const override {
        return "Null Command: Does nothing.\n"
               "Arguments: {}\n";
    }
};

CommandFactory::CommandFactory() {
    static NullCommandCreator kNullCommandCreator;
    m_creators.reserve(8);
    m_plugins.reserve(8);
    m_builtin_commands.reserve(8);
}

CommandFactory::~CommandFactory() {
    for (auto& p : m_plugins) {
        p.destroy_fn(p.creator.get());
        (void)p.lib.close();
    }
}

Result<> CommandFactory::initialize(StatusReporter& reporter, Slice<String> plugins) {
    ReportDebug(reporter, "CommandFactory: Loading builting commands");

    {
        auto creator = modus::make_unique<NullCommandCreator>();
        if (!creator->initialize(reporter)) {
            ReportError(reporter, "Failed to initialize builtin command: {}", creator->type());
            return Error<>();
        }
        m_creators.push_back(creator.get());
        m_builtin_commands.push_back(std::move(creator));
    }

    {
        auto creator = modus::make_unique<TouchCommandCreator>();
        if (!creator->initialize(reporter)) {
            ReportError(reporter, "Failed to initialize builtin command: {}", creator->type());
            return Error<>();
        }
        m_creators.push_back(creator.get());
        m_builtin_commands.push_back(std::move(creator));
    }

    {
        auto creator = modus::make_unique<ProcessCommandCreator>();
        if (!creator->initialize(reporter)) {
            ReportError(reporter, "Failed to initialize builtin command: {}", creator->type());
            return Error<>();
        }
        m_creators.push_back(creator.get());
        m_builtin_commands.push_back(std::move(creator));
    }

    ReportVerbose(reporter, "CommandFactory: Loading {} plugins", plugins.size());
    for (auto& plugin : plugins) {
        ReportDebug(reporter, "CommandFactory: Loading plugin '{}'", plugin);
        os::SharedLibrary lib;
        auto r = lib.open(plugin);
        if (!r) {
            ReportError(reporter, "Failed to load command plugin '{}': {}", plugin, r.error());
            return Error<>();
        }
        auto r_sym_create = lib.resolve(MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME_STR);
        if (!r_sym_create) {
            ReportError(
                reporter,
                "Failed to load command plugin '{}': Could not locate plugin create function",
                plugin);
            return Error<>();
        }

        auto r_sym_destroy = lib.resolve(MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME_STR);
        if (!r_sym_destroy) {
            ReportError(
                reporter,
                "Failed to load command plugin '{}': Could not locate plugin destroy function",
                plugin);
            return Error<>();
        }

        CommandCreatorPluginCreateFn fn_create =
            reinterpret_cast<CommandCreatorPluginCreateFn>(*r_sym_create);
        CommandCreatorPluginDestroyFn fn_destroy =
            reinterpret_cast<CommandCreatorPluginDestroyFn>(*r_sym_destroy);

        CommandCreatorPtr creator = fn_create();
        if (creator == nullptr) {
            ReportError(reporter,
                        "Failed to load plugin '{}': Could not create the command creator", plugin);
        }
        m_plugins.push_back({std::move(lib), creator, fn_create, fn_destroy});
        if (!creator->initialize(reporter)) {
            ReportError(reporter, "Failed to initialize plugin '{}'", plugin);
            return Error<>();
        }
        m_creators.push_back(creator);
    }
    return Ok<>();
}

void CommandFactory::shutdown(StatusReporter& reporter) {
    ReportDebug(reporter, "Shutting down command factory");
    for (auto& c : m_creators) {
        c->shutdown();
    }
    m_creators.clear();
    m_builtin_commands.clear();
    for (auto& p : m_plugins) {
        p.destroy_fn(p.creator.get());
        (void)p.lib.close();
    }
    m_plugins.clear();
}

struct JSONCommandValueParser final : public CommandValueParser {
    const rapidjson::Value::ConstObject& m_object;

    JSONCommandValueParser(const rapidjson::Value::ConstObject& object) : m_object(object) {}

    Result<String, String> read_string(const char* name) const override {
        String v;
        if (auto r = json_read_string(v, m_object, name); !r) {
            return Error(r.release_error());
        }
        return Ok(std::move(v));
    }

    Result<Vector<String>, String> read_string_array(const char* name) const override {
        if (!m_object.HasMember(name)) {
            return Error<String>(modus::format("Element {} not found", name));
        }

        const auto& array_value = m_object[name];
        if (!array_value.IsArray()) {
            return Error<String>(modus::format("Element {} is not an array", name));
        }

        const auto& array = array_value.GetArray();
        Vector<String> result;
        result.reserve(array.Size());

        for (u32 i = 0; i < array.Size(); ++i) {
            const auto& value = array[i];
            if (!value.IsString()) {
                return Error<String>(
                    modus::format("Array value {:02} of {} is not a string", i, name));
            }
            result.push_back(value.GetString());
        }
        return Ok(std::move(result));
    }

    Result<i32, String> read_i32(const char* name) const override {
        i32 v;
        if (auto r = json_read_i32(v, m_object, name); !r) {
            return Error(r.release_error());
        }
        return Ok(v);
    }

    Result<u32, String> read_u32(const char* name) const override {
        u32 v;
        if (auto r = json_read_u32(v, m_object, name); !r) {
            return Error(r.release_error());
        }
        return Ok(v);
    }

    Result<f32, String> read_f32(const char* name) const override {
        f32 v;
        if (auto r = json_read_f32(v, m_object, name); !r) {
            return Error(r.release_error());
        }
        return Ok(v);
    }

    Result<bool, String> read_bool(const char* name) const override {
        bool v;
        if (auto r = json_read_bool(v, m_object, name); !r) {
            return Error(r.release_error());
        }
        return Ok(v);
    }

    Result<Optional<String>, String> read_opt_string(const char* name) const override {
        return json_read_opt_string(m_object, name);
    }

    Result<Optional<i32>, String> read_opt_i32(const char* name) const override {
        return json_read_opt_i32(m_object, name);
    }

    Result<Optional<u32>, String> read_opt_u32(const char* name) const override {
        return json_read_opt_u32(m_object, name);
    }

    Result<Optional<f32>, String> read_opt_f32(const char* name) const override {
        return json_read_opt_f32(m_object, name);
    }

    Result<Optional<bool>, String> read_opt_bool(const char* name) const override {
        return json_read_opt_bool(m_object, name);
    }
};

Result<NotMyPtr<ICommand>> CommandFactory::create(StatusReporter& reporter,
                                                  const JSONObject& value,
                                                  const StringSlice output_directory,
                                                  const StringSlice current_directory,
                                                  const StringSlice output_file,
                                                  const StringSlice guid) {
    const auto& asset_object = value.object;
    CommandCreatorPtr creator;
    const char* type = nullptr;
    for (auto& c : m_creators) {
        type = c->type();
        if (asset_object.HasMember(type)) {
            const auto& cv = asset_object[type];
            if (cv.IsObject()) {
                creator = c;
                break;
            }
        }
    }

    if (!creator) {
        ReportError(reporter, "Command Factory: Could not find any valid command type");
        return Error<>();
    }

    const auto json_object = asset_object[type].GetObject();
    JSONCommandValueParser command_parser(json_object);

    CommandCreateParams command_params{command_parser, output_directory, current_directory,
                                       output_file, guid};
    auto r = creator->create(reporter, command_params);
    if (!r) {
        return Error<>();
    }
    return Ok<CommandPtr>(*r);
}

Result<> CommandFactory::add(CommandCreatorPtr creator) {
    if (auto r = find(creator->type()); r) {
        return Error<>();
    }
    m_creators.push_back(creator);
    return Ok<>();
}

Result<CommandCreatorPtr> CommandFactory::find(const StringSlice type) const {
    auto it = std::find_if(m_creators.begin(), m_creators.end(),
                           [type](const auto& ptr) { return type == ptr->type(); });
    if (it == m_creators.end()) {
        return Error<>();
    }
    return Ok(*it);
}

Vector<String> CommandFactory::list_command_types() const {
    Vector<String> types;
    types.reserve(m_creators.size());
    for (auto& c : m_creators) {
        types.push_back(c->type());
    }
    std::sort(types.begin(), types.end());
    return types;
}

}    // namespace modus::package
