/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>
#include <asset.hpp>
#include <command_factory.hpp>
#include <dependency.hpp>

namespace modus::package {

struct PackageContextParams {
    StringSlice command_plugin_dir;
    StringSlice dependency_cache_dir;
    StringSlice binary_dir;
    bool use_dependency_cache = true;
};

class PackageContext {
    friend class RecursivePackageLoader;

   private:
    DependencyDB m_dependency_db;
    AssetDB m_asset_db;
    CommandFactory m_cmd_factory;
    String m_dep_cache_file;
    String m_binary_dir;

   public:
    PackageContext();

    MODUS_CLASS_DISABLE_COPY_MOVE(PackageContext);

    static Result<> load_from_file(PackageContext& context,
                                   StatusReporter& reporter,
                                   const StringSlice file_path);

    Result<> initialize(StatusReporter& reporter, const PackageContextParams& params);

    Result<> shutdown(StatusReporter& reporter);

    Slice<AssetPtr> asset_list() const { return m_asset_db.list(); }

    Slice<DependencyPtr> dependency_list() const { return m_dependency_db.list(); }

    size_t asset_count() const { return m_asset_db.count(); }

    StringSlice binary_directory() const { return m_binary_dir; }

    const CommandFactory& command_factory() const { return m_cmd_factory; }
};

class PackageContextScope {
   private:
    StatusReporter& m_reporter;
    PackageContext& m_context;

   public:
    PackageContextScope(PackageContext& context, StatusReporter& reporter)
        : m_reporter(reporter), m_context(context) {}

    ~PackageContextScope() { (void)m_context.shutdown(m_reporter); }
};

}    // namespace modus::package
