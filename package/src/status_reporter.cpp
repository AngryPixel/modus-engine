/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <status_reporter_internal.hpp>
#include <iostream>

namespace modus::package {

StringSlice detail::reporter_format(const StringSlice fmt, fmt::format_args args) {
    static thread_local fmt::memory_buffer format_buffer;
    format_buffer.clear();
    fmt::vformat_to(format_buffer, fmt::string_view(fmt.data(), fmt.size()), args);
    return StringSlice(format_buffer.data(), format_buffer.size());
}

StatusReporter::StatusReporter() : m_level(Level::Info) {}

void StatusReporter::set_level(const Level l) {
    modus_assert(l < Level::Total);
    if (l < Level::Total) {
        m_level = l;
    }
}

void DefaultStatusReporter::report_verbose(const StringSlice msg) {
    std::cout << "[V] ";
    std::cout.write(msg.data(), msg.size());
    std::cout.put('\n');
}

void DefaultStatusReporter::report_error(const StringSlice msg) {
    std::cout << "[E] ";
    std::cout.write(msg.data(), msg.size());
    std::cout.put('\n');
}

void DefaultStatusReporter::report_warning(const StringSlice msg) {
    std::cout << "[W] ";
    std::cout.write(msg.data(), msg.size());
    std::cout.put('\n');
}

void DefaultStatusReporter::report_info(const StringSlice msg) {
    std::cout << "[I] ";
    std::cout.write(msg.data(), msg.size());
    std::cout.put('\n');
}

void DefaultStatusReporter::report_debug(const StringSlice msg) {
    std::cout << "[D] ";
    std::cout.write(msg.data(), msg.size());
    std::cout.put('\n');
}

void NullStatusReporter::report_verbose(const StringSlice) {}

void NullStatusReporter::report_error(const StringSlice) {}

void NullStatusReporter::report_warning(const StringSlice) {}

void NullStatusReporter::report_info(const StringSlice) {}

void NullStatusReporter::report_debug(const StringSlice) {}

std::unique_ptr<StatusReporter> StatusReporter::create_default_reporter() {
    return modus::make_unique<DefaultStatusReporter>();
}

std::unique_ptr<StatusReporter> StatusReporter::create_null_reporter() {
    return modus::make_unique<NullStatusReporter>();
}

}    // namespace modus::package
