/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <rapidjson/document.h>

namespace modus {
class ISeekableReader;
}

namespace modus {
class IByteWriter;
}

namespace modus::package {

struct JSONContext {
    rapidjson::Document m_doc;
};

struct JSONValue {
    rapidjson::Value& value;
};

struct JSONObject {
    rapidjson::Value::ConstObject object;
};

Result<void, String> load_json_file(rapidjson::Document& doc, ISeekableReader& reader);

Result<> write_json_file(rapidjson::Document& doc, IByteWriter& writer);

template <typename T>
inline Result<void, String> json_read_string(String& out, const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Error<String>(modus::format("value has no field named {}", field));
    }

    const auto& field_value = value[field];
    if (!field_value.IsString()) {
        return Error<String>(modus::format("field {} is not a string", field));
    }

    out = field_value.GetString();
    return Ok<>();
}

template <typename T>
inline Result<void, String> json_read_bool(bool& out, const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Error<String>(modus::format("value has no field named {}", field));
    }

    const auto& field_value = value[field];
    if (!field_value.IsBool()) {
        return Error<String>(modus::format("field {} is not a boolean", field));
    }
    out = field_value.GetBool();
    return Ok<>();
}

template <typename T>
inline Result<void, String> json_read_u64(u64& out, const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Error<String>(modus::format("value has no field named {}", field));
    }

    const auto& field_value = value[field];
    if (!field_value.IsUint64()) {
        return Error<String>(modus::format("field {} is not an u64", field));
    }

    out = field_value.GetUint64();
    return Ok<>();
}

template <typename T>
inline Result<void, String> json_read_i32(i32& out, const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Error<String>(modus::format("value has no field named {}", field));
    }

    const auto& field_value = value[field];
    if (!field_value.IsInt()) {
        return Error<String>(modus::format("field {} is not an i32", field));
    }

    out = field_value.GetInt();
    return Ok<>();
}

template <typename T>
inline Result<void, String> json_read_u32(u32& out, const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Error<String>(modus::format("value has no field named {}", field));
    }

    const auto& field_value = value[field];
    if (!field_value.IsUint()) {
        return Error<String>(modus::format("field {} is not an u32", field));
    }

    out = field_value.GetUint();
    return Ok<>();
}

template <typename T>
inline Result<void, String> json_read_f32(f32& out, const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Error<String>(modus::format("value has no field named {}", field));
    }

    const auto& field_value = value[field];
    if (!field_value.IsFloat()) {
        return Error<String>(modus::format("field {} is not an f32", field));
    }

    out = field_value.GetFloat();
    return Ok<>();
}

template <typename T>
inline Result<Optional<String>, String> json_read_opt_string(const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Ok(Optional<String>());
    }

    const auto& field_value = value[field];
    if (!field_value.IsString()) {
        return Error<String>(modus::format("field {} is not a string", field));
    }

    return Ok<Optional<String>>(field_value.GetString());
}

template <typename T>
inline Result<Optional<bool>, String> json_read_opt_bool(const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Ok(Optional<bool>());
    }

    const auto& field_value = value[field];
    if (!field_value.IsBool()) {
        return Error<String>(modus::format("field {} is not a boolean", field));
    }
    return Ok<Optional<bool>>(field_value.GetBool());
}

template <typename T>
inline Result<Optional<u64>, String> json_read_opt_u64(const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Ok(Optional<u64>());
    }

    const auto& field_value = value[field];
    if (!field_value.IsUint64()) {
        return Error<String>(modus::format("field {} is not an u64", field));
    }
    return Ok<Optional<u64>>(field_value.GetUint64());
}

template <typename T>
inline Result<Optional<i32>, String> json_read_opt_i32(const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Ok(Optional<i32>());
    }

    const auto& field_value = value[field];
    if (!field_value.IsInt()) {
        return Error<String>(modus::format("field {} is not an i32", field));
    }

    return Ok<Optional<i32>>(field_value.GetInt());
}

template <typename T>
inline Result<Optional<u32>, String> json_read_opt_u32(const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Ok(Optional<u32>());
    }

    const auto& field_value = value[field];
    if (!field_value.IsUint()) {
        return Error<String>(modus::format("field {} is not an u32", field));
    }

    return Ok<Optional<u32>>(field_value.GetUint());
}

template <typename T>
inline Result<Optional<f32>, String> json_read_opt_f32(const T& value, const char* field) {
    if (!value.HasMember(field)) {
        return Ok(Optional<f32>());
    }

    const auto& field_value = value[field];
    if (!field_value.IsFloat()) {
        return Error<String>(modus::format("field {} is not an f32", field));
    }

    return Ok<Optional<f32>>(field_value.GetFloat());
}
}    // namespace modus::package
