/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <os/path.hpp>

#include <asset.hpp>
#include <dependency.hpp>
#include <rapidjson_utils.hpp>
#include <command_factory.hpp>
#include <package/status_reporter.hpp>

namespace modus::package {

static constexpr u32 kElemPerPage = 32;
AssetDB::AssetDB(DependencyDB& dependency_db)
    : m_dependency_db(dependency_db), m_allocator(kElemPerPage) {
    m_list.reserve(kElemPerPage);
}

AssetDB::~AssetDB() {
    clear();
}

void AssetDB::clear() {
    for (auto& [k, v] : m_assets) {
        m_allocator.destroy(v.get());
    }
    m_assets.clear();
}

Result<AssetPtr> AssetDB::find(const String& output) {
    auto it = m_assets.find(output);
    if (it == m_assets.end()) {
        return Error<>();
    }
    return Ok(it->second);
}

Result<ConstAssetPtr> AssetDB::find(const String& output) const {
    auto it = m_assets.find(output);
    if (it == m_assets.end()) {
        return Error<>();
    }
    return Ok<ConstAssetPtr>(it->second);
}

static constexpr const char* kOuputElem = "output";
static constexpr const char* kDepsElem = "deps";
static constexpr const char* kPackElem = "pack_opt";
// static constexpr const char* kTypeElem = "type";
static constexpr const char* kPackArchiveElem = "archive";
static constexpr const char* kPackArchiveSkipElem = "skip_compression";

Result<AssetPtr> AssetDB::load(StatusReporter& reporter, const AssetLoadContext& context) {
    const auto& json_value = context.m_json_value.object;
    Asset asset;
    String asset_output_path;
    if (auto r = json_read_string(asset_output_path, json_value, kOuputElem); !r) {
        ReportError(reporter, "Asset {}", r.error());
        return Error<>();
    }

    // Get binary dir
    asset_output_path = os::Path::join(context.m_bin_dir, asset_output_path);
    if (m_assets.find(asset_output_path) != m_assets.end()) {
        ReportError(reporter, "Duplicate asset output detected '{}'", asset_output_path);
        return Error<>();
    }

    // Register output as a depenency so it can be referenced by other assets
    asset.m_output = m_dependency_db.find_or_create(asset_output_path);

    // Read Dependencies
    if (json_value.HasMember(kDepsElem)) {
        const auto& deps_value = json_value[kDepsElem];
        if (!deps_value.IsArray()) {
            ReportError(reporter, "{} element is not an array", kDepsElem);
            return Error<>();
        }

        const auto& deps_array = deps_value.GetArray();
        m_assets.reserve(deps_array.Size());
        for (const auto& dep_value : deps_array) {
            if (!dep_value.IsString()) {
                ReportError(reporter, "Dependency {:02} of Asset '{}' is not a string value",
                            asset.m_dependencies.size(), asset_output_path);
                return Error<>();
            }

            // Check if dependency is an asset
            AssetPtr asset_dep;
            const String bin_path = os::Path::join(context.m_bin_dir, dep_value.GetString());
            DependencyPtr dep_ptr;
            if (auto it = m_assets.find(bin_path); it != m_assets.end()) {
                ReportDebug(reporter,
                            "Asset '{}' dependency '{}' is the output of an existing asset",
                            asset_output_path, bin_path);
                asset_dep = it->second;
                dep_ptr = m_dependency_db.find_or_create(bin_path);
            } else {
                ReportDebug(reporter,
                            "Asset '{}' dependency '{}' is not the output of an existing asset",
                            asset_output_path, bin_path);
                // Otherwise look for the file
                const String src_path = os::Path::join(context.m_cwd, dep_value.GetString());
                if (!os::Path::is_file(src_path)) {
                    ReportError(
                        reporter,
                        "Dependency  {:02} of Asset '{}' is not a file or does not exist: {}",
                        asset.m_dependencies.size(), asset_output_path, src_path);
                    return Error<>();
                }
                dep_ptr = m_dependency_db.find_or_create(src_path);
            }
            modus_assert(dep_ptr);
            if (!dep_ptr->m_asset) {
                dep_ptr->m_asset = asset_dep;
            } else if (dep_ptr->m_asset != asset_dep) {
                ReportError(
                    reporter,
                    "Dependency {:02} of Asset '{}' already points to a different asset: {}",
                    asset.m_dependencies.size(), asset_output_path,
                    dep_ptr->m_asset->m_output->m_path);
                return Error<>();
            }
            asset.m_dependencies.push_back(dep_ptr);
        }
    }

    // Read pack opts
    if (json_value.HasMember(kPackElem)) {
        const auto& pack_value = json_value[kPackElem];
        if (!pack_value.IsObject()) {
            ReportError(reporter, "{} element of Asset '{}' is not an object", kPackElem,
                        asset_output_path);
            return Error<>();
        }
        const auto& pack_object = pack_value.GetObject();

        if (auto r =
                json_read_string(asset.m_archive_settings.archive, pack_object, kPackArchiveElem);
            !r) {
            ReportError(reporter, "Pack {} for Asset '{}'", r.error(), asset_output_path);
            return Error<>();
        }

        if (auto r = json_read_bool(asset.m_archive_settings.skip_compression, pack_object,
                                    kPackArchiveSkipElem);
            !r) {
            ReportError(reporter, "Pack {} for Asset '{}'", r.error(), asset_output_path);
            return Error<>();
        }
    }

    // Load command
    auto r_command = context.m_cmd_factory.create(reporter, context.m_json_value, context.m_bin_dir,
                                                  context.m_cwd, asset.m_output->m_path, "");
    if (!r_command) {
        ReportError(reporter, "Failed to create command for asset '{}'", asset_output_path);
        return Error<>();
    }
    const Vector<String> command_deps = (*r_command)->command_dependencies();
    u32 command_dep_index = 0;
    for (const auto& dep : command_deps) {
        // Check if dependency is an asset
        AssetPtr asset_dep;
        DependencyPtr dep_ptr;
        if (auto it = m_assets.find(dep); it != m_assets.end()) {
            ReportDebug(reporter,
                        "Asset '{}' command dependency '{}' is the output of an existing asset",
                        asset_output_path, dep);
            asset_dep = it->second;
            dep_ptr = m_dependency_db.find_or_create(dep);
        } else {
            ReportDebug(reporter,
                        "Asset '{}' command dependency '{}' is not the output of an existing asset",
                        asset_output_path, dep);
            // Otherwise look for the file
            if (!os::Path::is_file(dep)) {
                ReportError(
                    reporter,
                    "Command Dependency {:02} of Asset '{}' is not a file or does not exist: {}",
                    command_dep_index, asset_output_path, dep);
                return Error<>();
            }
            dep_ptr = m_dependency_db.find_or_create(dep);
        }
        modus_assert(dep_ptr);
        if (!dep_ptr->m_asset) {
            dep_ptr->m_asset = asset_dep;
        } else if (dep_ptr->m_asset != asset_dep) {
            ReportError(
                reporter,
                "Command Dependency {:02} of Asset '{}' already points to a different asset: {}",
                command_dep_index, asset_output_path, dep_ptr->m_asset->m_output->m_path);
            return Error<>();
        }
        asset.m_dependencies.push_back(dep_ptr);
        ++command_dep_index;
    }
    asset.m_command = *r_command;
    // Create asset
    ReportDebug(reporter, "Adding Asset with output '{}'", asset_output_path);
    AssetPtr ptr = m_allocator.construct(std::move(asset));
    if (ptr == nullptr) {
        modus_panic("ptr can't be null");
    }
    m_assets[ptr->m_output->m_path] = ptr;
    m_list.push_back(ptr);
    ptr->m_output->m_asset = ptr;
    return Ok(ptr);
}

}    // namespace modus::package
