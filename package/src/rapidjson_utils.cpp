/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <rapidjson_utils.hpp>
#include <core/io/memory_stream.hpp>
#include <os/file.hpp>

#include <rapidjson/stringbuffer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/cursorstreamwrapper.h>

namespace modus::package {

struct StringSliceReader {
    using Ch = char;

    const StringSlice m_stream;
    usize offset = 0;

    StringSliceReader(const StringSlice slice) : m_stream(slice) {}

    [[nodiscard]] Ch Peek() const {
        if (offset < m_stream.size()) {
            return m_stream[offset];
        }
        return '\0';
    }
    [[nodiscard]] Ch Take() {
        if (offset < m_stream.size()) {
            return m_stream[offset++];
        }
        return '\0';
    }
    [[nodiscard]] size_t Tell() const { return offset; }
    [[nodiscard]] Ch* PutBegin() {
        modus_assert(false);
        return nullptr;
    }
    void Put(Ch) { modus_assert(false); }
    void Flush() { modus_assert(false); }
    size_t PutEnd(Ch*) {
        modus_assert(false);
        return 0;
    }
};

Result<void, String> load_json_file(rapidjson::Document& doc, ISeekableReader& reader) {
    RamStream stream;
    if (!stream.reserve(512)) {
        return Error<String>("Failed to reserve memory to load json document");
    }
    auto read_result = stream.populate_from_stream(reader);
    if (!read_result) {
        return Error<String>("Failed to read json into memory");
    }

    const StringSlice doc_slice = stream.as_string_slice().sub_slice(0, read_result.value());
    StringSliceReader rjstream(doc_slice);
    rapidjson::CursorStreamWrapper cursor_stream(rjstream);
    const rapidjson::ParseResult parse_result = doc.ParseStream(cursor_stream);
    if (!parse_result) {
        return Error<String>(modus::format("Error parsing json file line:{} column:{}",
                                           cursor_stream.GetLine(), cursor_stream.GetColumn()));
    }
    return Ok<>();
}

Result<> write_json_file(rapidjson::Document& doc, IByteWriter& writer) {
    rapidjson::StringBuffer buffer(0, 65 * 1024 * 1024);
    rapidjson::PrettyWriter<rapidjson::StringBuffer> rjwriter(buffer);

    if (!doc.Accept(rjwriter)) {
        return Error<>();
    }
    const StringSlice generated = StringSlice(buffer.GetString(), buffer.GetSize());
    auto write_result = writer.write_exactly(generated.as_bytes());
    if (!write_result) {
        return Error<>();
    }
    return Ok<>();
}

}    // namespace modus::package
