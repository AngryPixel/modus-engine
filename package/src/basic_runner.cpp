/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <basic_runner.hpp>
#include <package_context.hpp>
#include <package/status_reporter.hpp>

#include <core/topo_sort.hpp>
#include <os/path.hpp>

namespace modus::package {

using SorterType = TopologicalSorter<const Dependency, DependencyPtr>;

Result<RunnerResult> BasicRunner::execute(StatusReporter& reporter, const RunnerContext& context) {
    SorterType sorter;
    Slice<AssetPtr> asset_list = context.package_context.asset_list();
    Slice<DependencyPtr> dependency_list = context.package_context.dependency_list();
    sorter.reserve(dependency_list.size());

    ReportDebug(reporter, "BasicRunner: Sorting {} dependencies", dependency_list.size());
    for (const auto& asset : asset_list) {
        if (!context.filter->apply(asset)) {
            continue;
        }
        // add each dependency as a leaf node, ignore duplicate entries
        for (auto& dep : asset->m_dependencies) {
            (void)sorter.add(dep, dep, Slice<DependencyPtr>());
        }
        auto r_add =
            sorter.add(asset->m_output, asset->m_output, make_slice(asset->m_dependencies));
        modus_assert(r_add == TopologicalSortError::None);
        MODUS_UNUSED(r_add);
    }

    // Sort Asset List
    Vector<ConstDependencyPtr> ordered_list;
    ordered_list.reserve(asset_list.size());
    const auto r_sort = sorter.evaluate(ordered_list);
    if (r_sort == TopologicalSortError::DependencyCycle) {
        const auto node_where = sorter.node_where_cycle_was_detected();
        const auto node_in_process = sorter.node_being_processed_when_cycle_was_detected();
        ReportError(reporter, "Dependency Cycle detected on '{}' while processing '{}'",
                    node_where ? node_where.value()->m_path : "Unknown",
                    node_in_process ? node_in_process.value()->m_path : "Uknown");
        return Error<>();
    } else if (r_sort == TopologicalSortError::DependencyNotFound) {
        auto missing_dep = sorter.missing_dependency();
        ReportError(reporter, "Dependency not found: {}\n",
                    missing_dep ? missing_dep.value()->m_path : "Uknown");
        return Error<>();
    } else if (r_sort != TopologicalSortError::None) {
        ReportError(reporter, "BasicRunner: Unknown Error ecountered during dependency sorting\n");
        return Error<>();
    }

    ReportVerbose(reporter, "BasicRunner: Checking which dependencies require updating");
    RunnerResult runner_result;
    // Check Dependency
    for (auto& dep : dependency_list) {
        if (auto r = check_and_update_dependency_status(reporter, dep); !r) {
            return Error<>();
        } else if (*r) {
            runner_result.num_updated_dependencies++;
        }
    }

    ReportVerbose(reporter, "BasicRunner: Starting asset command execution");
    // Run Commands
    for (auto& dep : ordered_list) {
        if (dep->m_asset != nullptr) {
            if (auto r = check_and_execute_asset_command(reporter, context, dep->m_asset); !r) {
                return Error<>();
            } else if (*r) {
                runner_result.num_commands_executed++;
            }
        }
    }
    ReportVerbose(reporter, "BasicRunner: Finished");
    return Ok(runner_result);
}

}    // namespace modus::package
