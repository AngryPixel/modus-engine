/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <commands/process_command.hpp>
#include <package/status_reporter.hpp>
#include <os/file.hpp>
#include <os/env.hpp>

namespace modus::package {

const char* ProcessCommand::type() const {
    return "process";
}

Result<void> ProcessCommand::execute(StatusReporter& reporter,
                                     const package::CommandContext&) const {
    return detail::execute_process(m_args, "", reporter);
}
u64 ProcessCommand::command_hash(const package::CommandContext&) const {
    modus::Hasher64 hasher;
    for (const auto& arg : m_args) {
        hasher.update(arg.data(), arg.size());
    }
    return hasher.digest();
}

Vector<String> ProcessCommand::command_dependencies() const {
    return {};
}

static constexpr usize kElementsPerBlock = 16;
ProcessCommandCreator::ProcessCommandCreator() : m_commands(kElementsPerBlock) {}

const char* ProcessCommandCreator::type() const {
    return "process";
}

Result<void> ProcessCommandCreator::initialize(package::StatusReporter& reporter) {
    ReportDebug(reporter, "Initializing Process Command Creator");
    return Ok<>();
}

void ProcessCommandCreator::shutdown() {
    m_commands.clear();
}

Result<package::ICommand*> ProcessCommandCreator::create(
    package::StatusReporter& reporter,
    const package::CommandCreateParams& params) {
    ReportDebug(reporter, "Creating Process command instance");
    static constexpr const char* kBinElem = "bin";
    static constexpr const char* kArgsElem = "args";

    std::vector<std::string> args;
    args.reserve(4);
    if (auto r = params.parser.read_string(kBinElem); !r) {
        package::ReportError(reporter, "ProcessCommandCreator: {}", r.error());
        return Error<>();
    } else {
        if (r->empty()) {
            package::ReportError(reporter, "ProcessCommandCreator: Empty Executable path");
            return Error<>();
        }

        auto r_exec_path = os::Path::executable_directory();
        if (!r_exec_path) {
            package::ReportError(reporter,
                                 "ProcessCommandCreator: Failed to get executable directory");
            return Error<>();
        }
        String local_path = os::Path::join(StringSlice(*r_exec_path), *r);
        if (os::Path::is_executable(local_path)) {
            args.push_back(std::string(local_path));
        } else if (!os::Path::is_absolute(*r)) {
            auto r_find = os::find_executable(*r);
            if (!r_find) {
                package::ReportError(reporter,
                                     "ProcessCommandCreator: Could not locate executable '{}'", *r);
                return Error<>();
            }
            args.push_back(std::string(*r_find));
        } else if (!os::Path::is_absolute(*r)) {
            if (!os::Path::is_executable(*r)) {
                package::ReportError(reporter,
                                     "ProcessCommandCreator: '{}' is not a valid executable", *r);
                return Error<>();
            }
            args.push_back(std::string(*r));
        } else if (args.empty()) {
            package::ReportError(reporter,
                                 "ProcessCommandCreator: Failed to extract the executable");
            return Error<>();
        }
    }

    if (auto r = params.parser.read_string_array(kArgsElem); !r) {
        package::ReportError(reporter, "ProcessCommandCreator: {}", r.error());
        return Error<>();
    } else {
        if (!r->empty()) {
            args.reserve(1 + r->size());
            for (const auto& arg : *r) {
                String replaced = StringSlice(arg).replace("%OUT%", params.output_file);
                replaced = StringSlice(replaced).replace("%BIN%", params.output_directory);
                replaced = StringSlice(replaced).replace("%SRC%", params.current_directory);
                args.push_back(std::string(replaced));
            }
        }
    }

    ProcessCommand* command = m_commands.create();
    command->m_args = std::move(args);
    return Ok<package::ICommand*>(command);
}

const char* ProcessCommandCreator::help_string() const {
    return "Process Command: Run an executable process to generate the specified output.\n"
           "Arguments: "
           "    bin:string      Process binary to execute (required)\n"
           "    args:[string]   Process arguments (required).\n"
           "                    %OUT% can be used reference the output file\n"
           "                    %SRC% can be used reference the source directory\n"
           "                    %BIN% can be used reference the output directory\n";
}

}    // namespace modus::package
