/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <commands/touch_command.hpp>
#include <package/status_reporter.hpp>
#include <os/file.hpp>
#include <os/env.hpp>

namespace modus::package {

const char* TouchCommand::type() const {
    return "touch";
}

Result<void> TouchCommand::execute(StatusReporter& reporter,
                                   const package::CommandContext& context) const {
#if !defined(MODUS_OS_WIN32)
    m_args.push_back(std::string(context.output.data(), context.output.size()));
    return detail::execute_process(m_args, "", reporter);
#else
    const bool exists = os::Path::is_file(context.output);
    if (exists) {
        auto r_file = os::FileBuilder().write().append().open(context.output);
        if (!r_file) {
            return Error<>();
        }
    } else {
        auto r_file = os::FileBuilder().write().create().open(context.output);
        if (!r_file) {
            return Error<>();
        }
    }
    return Ok<>();
#endif
}
u64 TouchCommand::command_hash(const package::CommandContext&) const {
    // Always return 0 as we want this command to always run
    return 0;
}

Vector<String> TouchCommand::command_dependencies() const {
    return {};
}

static constexpr usize kElementsPerBlock = 16;
TouchCommandCreator::TouchCommandCreator() : m_commands(kElementsPerBlock) {}

const char* TouchCommandCreator::type() const {
    return "touch";
}

Result<void> TouchCommandCreator::initialize(package::StatusReporter& reporter) {
    ReportDebug(reporter, "Initializing Touch Command Creator");
#if !defined(MODUS_OS_WIN32)
    auto r_touch = os::find_executable("touch");
    if (!r_touch) {
        ReportError(reporter, "Could not find touch executable");
        return Error<>();
    }
    ReportVerbose(reporter, "TouchCommandCreator: Found Touch Command at {}", *r_touch);
    m_touch_command = r_touch.release_value();
#endif
    return Ok<>();
}

void TouchCommandCreator::shutdown() {
    m_commands.clear();
}

Result<package::ICommand*> TouchCommandCreator::create(package::StatusReporter& reporter,
                                                       const package::CommandCreateParams&) {
    ReportDebug(reporter, "Creating Touch command instance");
    TouchCommand* command = m_commands.create();
#if !defined(MODUS_OS_WIN32)
    command->m_args.reserve(2);
    command->m_args.push_back(std::string(m_touch_command));
#endif
    return Ok<package::ICommand*>(command);
}

const char* TouchCommandCreator::help_string() const {
    return "Touch Command: Update last accessed time on a files sepecified by the output file.\n"
           "Arguments: {}\n";
}

}    // namespace modus::package
