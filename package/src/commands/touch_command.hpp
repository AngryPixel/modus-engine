/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package/command.hpp>

namespace modus::package {

class TouchCommand final : public ICommand {
   public:
    mutable std::vector<std::string> m_args;

   public:
    const char* type() const override;

    Result<void> execute(StatusReporter& reporter,
                         const package::CommandContext& context) const override;

    u64 command_hash(const package::CommandContext&) const override;

    Vector<String> command_dependencies() const override;
};

class TouchCommandCreator final : public ICommandCreator {
   private:
    CommandPool<TouchCommand, DefaultAllocator> m_commands;
    String m_touch_command;

   public:
    TouchCommandCreator();

    const char* type() const override;

    Result<void> initialize(StatusReporter& reporter) override;

    void shutdown() override;

    Result<package::ICommand*> create(StatusReporter& reporter,
                                      const CommandCreateParams&) override;

    const char* help_string() const override;
};

}    // namespace modus::package
