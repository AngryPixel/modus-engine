/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <dependency.hpp>
#include <asset.hpp>
#include <os/file.hpp>
#include <os/path.hpp>
#include <package/status_reporter.hpp>
#include <rapidjson_utils.hpp>

namespace modus::package {

static constexpr u32 kElementsPerPage = 128;
DependencyDB::DependencyDB() : m_allocator(kElementsPerPage) {
    m_list.reserve(kElementsPerPage);
}

DependencyDB::~DependencyDB() {
    clear();
}

Result<> DependencyDB::load_from_file(DependencyDB& db,
                                      StatusReporter& reporter,
                                      const StringSlice path) {
    ReportDebug(reporter, "Loading Dependency cache from path '{}'", path);
    if (!os::Path::exists(path)) {
        ReportInfo(reporter, "Dependency cache file '{}' doesn't exist, skipping load", path);
    } else {
        auto r_file = os::FileBuilder().read().open(path);
        ReportDebug(reporter, "Loading dependency cache file '{}'", path);
        if (!r_file) {
            ReportError(reporter, "Failed to open dependency cache file '{}': {}", path,
                        os::path_error_to_str(r_file.error()));
            return Error<>();
        }
        auto r_cache_load = db.load(reporter, *r_file);
        if (!r_cache_load) {
            ReportError(reporter, "Failed to open dependency cache file '{}'", path);
            return Error<>();
        }
    }
    ReportDebug(reporter, "Dependency Cache file '{}' successfully loaded", path);
    return Ok<>();
}

Result<> DependencyDB::save_to_file(const DependencyDB& db,
                                    StatusReporter& reporter,
                                    const StringSlice path) {
    const String tmp_file = path.to_str() + ".tmp";
    {
        ReportDebug(reporter, "Opening tmp dependency cache file '{}'", tmp_file);
        auto r_file = os::FileBuilder().create().write().open(tmp_file);
        if (!r_file) {
            ReportError(reporter, "Failed to open tmp dependency cache file '{}': {}", tmp_file,
                        os::path_error_to_str(r_file.error()));
            return Error<>();
        }

        ReportDebug(reporter, "Writing tmp dependency cache file '{}'", tmp_file);
        auto r_cache_save = db.save(reporter, *r_file);
        if (!r_cache_save) {
            ReportError(reporter, "Failed to write dependency cache to '{}'", tmp_file);
            return Error<>();
        }
        r_file->flush();
    }

    ReportDebug(reporter, "Attempting to move tmp dependency cache file");
    if (auto r = os::Path::rename(tmp_file, path); !r) {
        ReportError(reporter, "Failed to move tmp dependency cache from '{}' to '{}: {}", tmp_file,
                    path, os::path_error_to_str(r.error()));
        return Error<>();
    }
    return Ok<>();
}

static constexpr const char* kRootElem = "dependencies";
static constexpr const char* kVersionElem = "version";
static constexpr const char* kPathElem = "path";
static constexpr const char* kLastModElem = "last_mod";
static constexpr const char* kLastUsedElem = "last_used";
static constexpr const char* kHashElem = "hash";
static constexpr const char* kFromAssetElem = "from_asset";
static constexpr const char* kCommandHashElem = "cmd_hash";
static constexpr u32 kVersion = 1;

Result<> DependencyDB::load(StatusReporter& reporter, ISeekableReader& reader) {
    clear();

    rapidjson::Document doc;
    if (auto r_load = load_json_file(doc, reader); !r_load) {
        ReportError(reporter, "Failed to load cache file: {}", r_load.error());
        return Error<>();
    }

    if (!doc.HasMember(kRootElem)) {
        ReportError(reporter, "Could not locate element named {}", kRootElem);
        return Error<>();
    }

    if (!doc.HasMember(kVersionElem)) {
        ReportError(reporter, "Could not locate element named {}", kVersionElem);
        return Error<>();
    }

    // Check Version
    {
        const auto& value = doc[kVersionElem];
        if (!value.IsUint()) {
            ReportError(reporter, "{} element is not an unsigned integer", kVersionElem);
            return Error<>();
        }

        if (value.GetUint() != kVersion) {
            ReportError(reporter, "Invalid depenency cache version, current={} document={}",
                        kVersion, value.GetUint());
            return Error<>();
        }
    }

    // Load Cache Entries
    const auto& entries = doc[kRootElem];
    if (!entries.IsArray()) {
        ReportError(reporter, "{} element is not an array", kRootElem);
        return Error<>();
    }

    u32 entry_counter = 0;
    const auto& entries_array = entries.GetArray();
    m_dependencies.reserve(entries_array.Size());
    for (const auto& entry : entries.GetArray()) {
        if (!entry.IsObject()) {
            ReportError(reporter, "Dependency {:02} is not an object", entry_counter);
            return Error<>();
        }

        Dependency d;
        const auto& entry_object = entry.GetObject();

        if (auto r = json_read_string(d.m_path, entry_object, kPathElem); !r) {
            ReportError(reporter, "Dependency {:02} {}", entry_counter, r.error());
            return Error<>();
        }

        if (m_dependencies.find(d.m_path) != m_dependencies.end()) {
            ReportError(reporter, "Duplicate dependency: {}", d.m_path);
            return Error<>();
        }

        // If the dependency file can't be found do not add it to the list in order to avoid
        // false dependencies
        if (!os::Path::is_file(d.m_path)) {
            ReportWarning(reporter, "Could not locate dependency: {}", d.m_path);
            continue;
        }

        if (auto r = json_read_u64(d.m_last_time_mod, entry_object, kLastModElem); !r) {
            ReportError(reporter, "Dependency {:02} {}", entry_counter, r.error());
            return Error<>();
        }

        if (auto r = json_read_u64(d.m_hash, entry_object, kHashElem); !r) {
            ReportError(reporter, "Dependency {:02} {}", entry_counter, r.error());
            return Error<>();
        }

        if (auto r = json_read_u64(d.m_last_time_used, entry_object, kHashElem); !r) {
            ReportError(reporter, "Dependency {:02} {}", entry_counter, r.error());
            return Error<>();
        }

        if (auto r = json_read_bool(d.m_was_generated_from_asset, entry_object, kFromAssetElem);
            !r) {
            ReportError(reporter, "Dependency {:02} {}", entry_counter, r.error());
            return Error<>();
        }

        if (auto r = json_read_opt_u64(entry_object, kCommandHashElem); !r) {
            ReportError(reporter, "Dependency {:02} {}", entry_counter, r.error());
            return Error<>();
        } else if (*r) {
            d.m_cmd_hash = (*r).value_or(0);
        }

        DependencyPtr d_ptr = m_allocator.construct(std::move(d));
        m_dependencies[d_ptr->m_path] = d_ptr;
        m_list.push_back(d_ptr);
        entry_counter++;
    }
    return Ok<>();
}

Result<DependencyPtr> DependencyDB::find(const String& output) {
    auto it = m_dependencies.find(output);
    if (it == m_dependencies.end()) {
        return Error<>();
    }
    return Ok(it->second);
}

Result<ConstDependencyPtr> DependencyDB::find(const String& output) const {
    auto it = m_dependencies.find(output);
    if (it == m_dependencies.end()) {
        return Error<>();
    }
    return Ok<ConstDependencyPtr>(it->second.get());
}

DependencyPtr DependencyDB::find_or_create(const String& output) {
    auto it = m_dependencies.find(output);
    if (it == m_dependencies.end()) {
        DependencyPtr ptr = m_allocator.construct();
        if (ptr == nullptr) {
            modus_panic("Failed to allocate memory");
        }
        ptr->m_was_not_seen_before = true;
        ptr->m_path = output;
        m_dependencies[output] = ptr;
        m_list.push_back(ptr);
        return ptr;
    }
    return it->second;
}

Result<> DependencyDB::save(StatusReporter& reporter, IByteWriter& writer) const {
    using StrRef = rapidjson::Value::StringRefType;
    rapidjson::Document doc;
    doc.SetObject();

    // Add Version
    {
        rapidjson::Value value;
        value.SetUint(kVersion);
        doc.AddMember(StrRef(kVersionElem), value, doc.GetAllocator());
    }

    // create entries
    rapidjson::Value entry_array;
    entry_array.SetArray();

    auto add_string_value = [&doc](auto& value, const String& str, const StrRef field) -> void {
        rapidjson::Value v;
        v.SetString(StrRef(str.c_str()));
        value.AddMember(field, v, doc.GetAllocator());
    };

    auto add_u64_value = [&doc](auto& value, const u64 u, const StrRef field) -> void {
        rapidjson::Value v;
        v.SetUint64(u);
        value.AddMember(field, v, doc.GetAllocator());
    };

    auto add_bool_value = [&doc](auto& value, const bool b, const StrRef field) -> void {
        rapidjson::Value v;
        v.SetBool(b);
        value.AddMember(field, v, doc.GetAllocator());
    };

    for (const auto& [k, v] : m_dependencies) {
        // Do not store any entries in the cache for assests that were not successfully
        // executed when it's the first time we're seening/processing them
        if (v->m_asset != nullptr && v->m_was_not_seen_before && !v->m_successfully_executed) {
            ReportDebug(reporter,
                        "Skipping saving '{}' in cache as it failed and it's the first "
                        "time we are processing it",
                        v->m_path);
            continue;
        }
        rapidjson::Value entry_value;
        entry_value.SetObject();
        add_string_value(entry_value, v->m_path, StrRef(kPathElem));
        add_u64_value(entry_value, v->m_last_time_mod, StrRef(kLastModElem));
        add_u64_value(entry_value, v->m_last_time_used, StrRef(kLastUsedElem));
        add_u64_value(entry_value, v->m_hash, StrRef(kHashElem));
        add_bool_value(entry_value, v->m_asset != nullptr, StrRef(kFromAssetElem));
        add_u64_value(entry_value, v->m_cmd_hash, StrRef(kCommandHashElem));
        entry_array.PushBack(entry_value, doc.GetAllocator());
    }

    doc.AddMember(StrRef(kRootElem), entry_array, doc.GetAllocator());
    if (!write_json_file(doc, writer)) {
        ReportError(reporter, "Failed to write dependency cache to stream");
        return Error<>();
    }
    return Ok<>();
}

void DependencyDB::clear() {
    for (auto& [k, v] : m_dependencies) {
        m_allocator.destroy(v.get());
    }
    m_dependencies.clear();
}

void DependencyDB::erase_invalid() {
    auto it = m_list.begin();
    while (it != m_list.end()) {
        ConstDependencyPtr dep = *it;
        if (dep->m_was_generated_from_asset && !dep->m_asset) {
            m_dependencies.erase(dep->m_path);
            it = m_list.erase(it);
        } else {
            it++;
        }
    }
}

}    // namespace modus::package
