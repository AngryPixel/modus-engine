/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>
#include <core/allocator/allocator_libc.hpp>
#include <core/allocator/pool_allocator.hpp>

namespace modus {
class ISeekableReader;
}

namespace modus {
class IByteWriter;
}

namespace modus::package {

struct Dependency {
    friend class DependencyDB;

   private:
    bool m_successfully_executed = false;

   public:
    String m_path;
    u64 m_last_time_mod = 0;
    u64 m_hash = 0;
    u64 m_last_time_used = 0;
    u64 m_cmd_hash = 0;
    AssetPtr m_asset;
    Optional<u64> m_new_hash;
    Optional<u64> m_new_cmd_hash;
    Optional<u64> m_new_last_time_mod;
    bool m_updated = false;
    bool m_was_generated_from_asset = false;
    bool m_was_not_seen_before = false;

    void set_successfully_executed(const bool v) {
        if (v) {
            m_hash = m_new_hash.value_or(m_hash);
            m_cmd_hash = m_new_cmd_hash.value_or(m_cmd_hash);
            m_last_time_mod = m_new_last_time_mod.value_or(m_last_time_mod);
        }
        m_successfully_executed = v;
    }

    void set_last_time_mod(const u64 time) {
        if (m_asset) {
            m_new_last_time_mod = time;
        } else {
            m_last_time_mod = time;
        }
    }

    void set_new_hash(const u64 hash) {
        if (m_asset) {
            m_new_hash = hash;
        } else {
            m_hash = hash;
        }
    }
};

class DependencyDB {
   private:
    PoolAllocatorTyped<Dependency, AllocatorLibC> m_allocator;
    HashMap<String, DependencyPtr> m_dependencies;
    Vector<DependencyPtr> m_list;

   public:
    DependencyDB();

    ~DependencyDB();

    static Result<> load_from_file(DependencyDB& db,
                                   StatusReporter& reporter,
                                   const StringSlice path);

    static Result<> save_to_file(const DependencyDB& db,
                                 StatusReporter& reporter,
                                 const StringSlice path);

    MODUS_CLASS_DISABLE_COPY_MOVE(DependencyDB);

    Result<> load(StatusReporter& reporter, ISeekableReader& reader);

    Result<DependencyPtr> find(const String& output);

    Result<ConstDependencyPtr> find(const String& output) const;

    DependencyPtr find_or_create(const String& output);

    Result<> save(StatusReporter& reporter, IByteWriter& writer) const;

    Slice<DependencyPtr> list() const { return make_slice(m_list); }

    size_t count() const { return m_dependencies.size(); }

    void clear();

    // An invalid dependency is one generated from an asset file, but the asset file no longer
    // exists
    void erase_invalid();
};

}    // namespace modus::package
