/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <package_context.hpp>
#include <os/path.hpp>
#include <os/file.hpp>
#include <os/shared_library.hpp>
#include <core/fixed_stack.hpp>
#include <rapidjson_utils.hpp>
#include <package/status_reporter.hpp>

namespace modus::package {

constexpr const char* kDependencyFileName = "package_dependency_cache.json";
constexpr const char* kPackageFileName = "package.json";

PackageContext::PackageContext() : m_asset_db(m_dependency_db) {}

// static constexpr const char* kVersionElem = "version";
static constexpr const char* kAssetsElem = "assets";
static constexpr const char* kSubdirsElem = "subdirs";
// static constexpr const char* kSubDirsElem = "assets"; TODO?

class RecursivePackageLoader {
   private:
    struct StackEntry {
        Optional<rapidjson::Document> doc;
        String file_path;
        String directory_path;
    };
    static constexpr const u32 kMaxStackDepth = 32;
    PackageContext& m_context;
    FixedStack<StackEntry, kMaxStackDepth> m_stack;

   public:
    RecursivePackageLoader(PackageContext& context) : m_context(context) {}

    Result<> load(StatusReporter& reporter, const StringSlice file_path) {
        m_stack.clear();
        {
            StackEntry root_entry;
            root_entry.file_path = file_path.to_str();
            root_entry.directory_path = os::Path::get_path(file_path).to_str();
            m_stack.push(std::move(root_entry)).expect("Failed to move root entry to stack");
        }
        while (!m_stack.is_empty()) {
            StackEntry* entry = *m_stack.top();

            // First time parsing the file
            if (!entry->doc.has_value()) {
                ReportDebug(reporter, "First stage for package file '{}'", entry->file_path);
                ReportVerbose(reporter, "Opening package file '{}'", entry->file_path);
                auto r_file = os::FileBuilder().read().open(entry->file_path);
                if (!r_file) {
                    ReportError(reporter, "Failed to open package file {}: {}", entry->file_path,
                                os::path_error_to_str(r_file.error()));
                    return Error<>();
                }

                {
                    rapidjson::Document doc;
                    if (auto r_load = load_json_file(doc, *r_file); !r_load) {
                        ReportError(reporter, "Failed to load package file {}: {}",
                                    entry->file_path, r_load.error());
                        return Error<>();
                    }
                    entry->doc = std::move(doc);
                }

                // Load subdirs if any
                ReportDebug(reporter, "Checking subdirs for package file '{}'", entry->file_path);
                if (entry->doc->HasMember(kSubdirsElem)) {
                    rapidjson::Document& doc = *entry->doc;
                    const auto& subdirs_value = doc[kSubdirsElem];
                    if (!subdirs_value.IsArray()) {
                        ReportError(reporter, "{} element is not an array", kSubdirsElem);
                        ReportError(reporter, "Failed to load package file {}", entry->file_path);
                        return Error<>();
                    }

                    const auto& subdirs_array = subdirs_value.GetArray();
                    for (size_t i = 0; i < subdirs_array.Size(); ++i) {
                        // Reverse push the items on the stack
                        const size_t index = subdirs_array.Size() - i - 1;
                        const auto& subdir_value = subdirs_array[index];
                        if (!subdir_value.IsString()) {
                            ReportError(reporter, "Subdir {:02} of '{}' is not a string value",
                                        index, entry->file_path);
                            ReportError(reporter, "Failed to load package file {}",
                                        entry->file_path);
                            return Error<>();
                        }

                        String new_dir_path = os::Path::join(StringSlice(entry->directory_path),
                                                             subdir_value.GetString());
                        String new_file_path =
                            os::Path::join(StringSlice(new_dir_path), kPackageFileName);

                        ReportDebug(reporter, "Checking for subdir file '{}'", entry->file_path);
                        if (!os::Path::is_file(new_file_path)) {
                            ReportError(reporter,
                                        "Could not locate package file '{}' refereced in '{}'",
                                        new_file_path, entry->file_path);
                            return Error<>();
                        }

                        ReportDebug(reporter, "Pushing subdir file '{}' to the stack",
                                    entry->file_path);
                        StackEntry new_entry;
                        new_entry.file_path = std::move(new_file_path);
                        new_entry.directory_path = std::move(new_dir_path);
                        if (!m_stack.push(std::move(new_entry))) {
                            ReportError(
                                reporter,
                                "Recursive Stack depth exceeded, increase limit to continue");
                            return Error<>();
                        }
                    }
                }
            } else {
                ReportDebug(reporter, "Second stage for package file '{}'", entry->file_path);
                ReportDebug(reporter, "Loading assets from file '{}'", entry->file_path);
                // Second time, load assets, all subdirs have been processed by now
                if (entry->doc->HasMember(kAssetsElem)) {
                    rapidjson::Document& doc = *entry->doc;
                    const auto& assets_value = doc[kAssetsElem];
                    if (!assets_value.IsArray()) {
                        ReportError(reporter, "{} element is not an array", kAssetsElem);
                        ReportError(reporter, "Failed to load package file {}", entry->file_path);
                        return Error<>();
                    }

                    u32 asset_counter = 0;
                    for (const auto& asset : assets_value.GetArray()) {
                        if (!asset.IsObject()) {
                            ReportError(reporter, "Asset {:02} is not an object element",
                                        asset_counter);
                            ReportError(reporter, "Failed to load package file {}",
                                        entry->file_path);
                            return Error<>();
                        }

                        JSONObject json_object{asset.GetObject()};
                        AssetLoadContext asset_context{m_context.m_cmd_factory, json_object,
                                                       entry->directory_path,
                                                       m_context.m_binary_dir};
                        auto r_asset = m_context.m_asset_db.load(reporter, asset_context);
                        if (!r_asset) {
                            ReportError(reporter, "Failed to load Asset {:02} from '{}'",
                                        asset_counter, entry->file_path);
                            return Error<>();
                        }
                        asset_counter++;
                    }
                }
                ReportDebug(reporter, "Finished loading assets from file '{}'", entry->file_path);
                // Finished parsing this file pop from stack to complete
                m_stack.pop();
            }
        }
        return Ok<>();
    }
};

Result<> PackageContext::load_from_file(PackageContext& context,
                                        StatusReporter& reporter,
                                        const StringSlice file_path) {
    RecursivePackageLoader loader(context);
    auto r = loader.load(reporter, file_path);
    if (r) {
        context.m_dependency_db.erase_invalid();
    }
    return r;
}

struct PluginCollectorCallback : public os::Path::ListDirectoryCallback {
    Vector<String> m_plugins;
    PluginCollectorCallback() { m_plugins.reserve(8); }

    void on_list(const StringSlice path) override {
        if (os::SharedLibrary::is_valid_library_name(path) &&
            os::Path::remove_extension(path).ends_with("_command")) {
            m_plugins.push_back(path.to_str());
        }
    }
};

Result<> PackageContext::initialize(StatusReporter& reporter, const PackageContextParams& params) {
    ReportVerbose(reporter, "Initializing package context");
    ReportVerbose(reporter, "\tOutput Directory: {}", params.binary_dir);
    ReportVerbose(reporter, "\tCache Directory : {}", params.dependency_cache_dir);
    ReportVerbose(reporter, "\tPlugin Directory: {}", params.command_plugin_dir);
    ReportVerbose(reporter, "\tUse Dep. Cache  : {}", params.use_dependency_cache);

    m_dep_cache_file.clear();
    m_binary_dir.clear();
    if (params.use_dependency_cache) {
        ReportDebug(reporter, "Using dependency cache located at '{}'",
                    params.dependency_cache_dir);
        if (!os::Path::make_directory(params.dependency_cache_dir)) {
            ReportError(reporter, "Failed to create dependency cache directory: {}",
                        params.dependency_cache_dir);
            return Error<>();
        }
    }

    if (params.binary_dir.is_empty()) {
        ReportError(reporter, "No binary directory provided");
        return Error<>();
    }

    if (!os::Path::make_directory(params.binary_dir)) {
        ReportError(reporter, "Failed to craete binary directory '{}'", params.binary_dir);
        return Error<>();
    }
    m_binary_dir = params.binary_dir.to_str();

    // Collect plugins and init Command Factory
    {
        PluginCollectorCallback callback;
        if (!params.command_plugin_dir.is_empty()) {
            ReportDebug(reporter, "Scanning for command plugins at '{}'",
                        params.command_plugin_dir);
            auto r_list =
                os::Path::list_directory(callback, params.command_plugin_dir, false, false);
            if (!r_list) {
                ReportError(reporter, "Failed to scan plugin directory '{}': {}",
                            params.command_plugin_dir, os::path_error_to_str(r_list.error()));
                return Error<>();
            }
        }
        if (reporter.level() >= StatusReporter::Level::Debug) {
            for (const auto& p : callback.m_plugins) {
                ReportDebug(reporter, "\t{}", p);
            }
        }
        auto r_factory_init = m_cmd_factory.initialize(reporter, make_slice(callback.m_plugins));
        if (!r_factory_init) {
            return r_factory_init;
        }
    }

    if (params.use_dependency_cache) {
        m_dep_cache_file = os::Path::join(params.dependency_cache_dir, kDependencyFileName);
        if (auto r = DependencyDB::load_from_file(m_dependency_db, reporter, m_dep_cache_file);
            !r) {
            return Error<>();
        }
    }
    return Ok<>();
}

Result<> PackageContext::shutdown(StatusReporter& reporter) {
    ReportVerbose(reporter, "Shutting down package context");

    // shutdown cmd factory
    m_cmd_factory.shutdown(reporter);

    // shutdown dependency
    if (!m_dep_cache_file.empty()) {
        ReportVerbose(reporter, "Saving depedency cache to '{}'", m_dep_cache_file);
        auto r = DependencyDB::save_to_file(m_dependency_db, reporter, m_dep_cache_file);
        if (!r) {
            return Error<>();
        }
    }

    return Ok<>();
}

}    // namespace modus::package
