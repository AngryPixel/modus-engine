/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/status_reporter.hpp>

namespace modus::package {

class DefaultStatusReporter final : public StatusReporter {
   public:
    void report_verbose(const StringSlice) override;

    void report_error(const StringSlice) override;

    void report_warning(const StringSlice) override;

    void report_info(const StringSlice) override;

    void report_debug(const StringSlice) override;
};

class NullStatusReporter final : public StatusReporter {
   public:
    void report_verbose(const StringSlice) override;

    void report_error(const StringSlice) override;

    void report_warning(const StringSlice) override;

    void report_info(const StringSlice) override;

    void report_debug(const StringSlice) override;
};

}    // namespace modus::package
