/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <package/command.hpp>
#include <package/status_reporter.hpp>
#if defined(MODUS_OS_WIN32) && defined(UNICODE)
#define MODUS_RESTORE_UNICODE
#undef UNICODE
#endif
#include <process.hpp>
#if defined(MODUS_OS_WIN32) && defined(MODUS_RESTORE_UNICODE)
#define UNICODE
#undef MODUS_RESTORE_UNICODE
#endif

#if defined(MODUS_OS_WIN32)
#include <locale>
#include <codecvt>
#endif

namespace modus::package {

MODUS_PACKAGE_EXPORT
Result<> detail::execute_process(const std::vector<std::string>& args,
                                 const StringSlice path,
                                 StatusReporter& reporter) {
    const bool log_output = reporter.level() == StatusReporter::Level::Verbose;
    String command_str;
    command_str.reserve(128);
    for (auto& arg : args) {
        command_str += arg;
        command_str += ' ';
    }
    ReportVerbose(reporter, "Executing: {}", command_str);

#if defined(MODUS_OS_WIN32) && defined(TINYPROCESS_USE_UNICODE)
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::vector<std::wstring> wargs;
    wargs.reserve(args.size());
    for (auto& arg : args) {
        wargs.push_back(converter.from_bytes(arg));
    }
    const String str_path_utf8 = path.to_str();
    const std::wstring str_path = converter.from_bytes(str_path_utf8.c_str());
#else
    const std::string str_path(path.data(), path.size());
    const auto& wargs = args;
#endif

    String process_out;
    String process_err;
    TinyProcessLib::Process process(
        wargs, str_path,
        [&process_out, log_output](const char* bytes, size_t n) {
            if (log_output) {
                process_out.append(bytes, n);
            }
        },
        [&process_err](const char* bytes, size_t n) { process_err.append(bytes, n); });

    const int exit_status = process.get_exit_status();
    if (exit_status == 0) {
        if (log_output && !process_out.empty()) {
            ReportVerbose(reporter, "{}", process_out);
        }
        return Ok<>();
    }
    ReportError(reporter, "Failed to execute: {}\n\n{}", command_str, process_err);
    return Error<>();
}
}    // namespace modus::package
