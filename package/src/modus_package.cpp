/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <package_context.hpp>
#include <basic_runner.hpp>
#include <threaded_runner.hpp>
#include <package/status_reporter.hpp>

#include <os/path.hpp>
#include <os/time.hpp>
#include <core/getopt.hpp>

#include <iostream>

using namespace modus;

class TypeFilter final : public package::AssetFilter {
   private:
    String m_type;

   public:
    TypeFilter(StringSlice type) : m_type(type.to_str()) {}
    bool apply(package::ConstAssetPtr asset) const override {
        return asset->m_command->type() == m_type;
    }
};

class ExtFilter final : public package::AssetFilter {
   private:
    String m_ext;

   public:
    ExtFilter(StringSlice ext) : m_ext(ext.to_str()) {}
    bool apply(package::ConstAssetPtr asset) const override {
        return StringSlice(asset->m_output->m_path).ends_with(m_ext);
    }
};

Result<> clean_asset_files(const package::PackageContext& context,
                           package::StatusReporter& reporter) {
    auto assets = context.asset_list();
    package::ReportInfo(reporter, "Cleaning {} assets", assets.size());
    for (const auto& asset : assets) {
        const StringSlice asset_path = asset->m_output->m_path;
        if (os::Path::is_file(asset_path)) {
            package::ReportDebug(reporter, "Removing {}", asset_path);
            auto r_remove = os::Path::remove(asset_path);
            if (!r_remove) {
                package::ReportError(reporter, "Failed to remove asset '{}': {}", asset_path,
                                     os::path_error_to_str(r_remove.error()));
                return Error<>();
            }
        }
    }
    return Ok<>();
}

static String format_time(const IMilisec ms) {
    if (ms < IMilisec(1000)) {
        return modus::format("{} ms", ms.count());
    } else if (ms < IMilisec(60000)) {
        return modus::format("{:.2f} sec", FSeconds(ms).count());
    } else {
        return modus::format("{:.2f} min", f64(ms.count()) / 60000.0);
    }
}

static constexpr const char* kHelpStr =
    "Modus Package Tool - Generate Binary Assets and/or Package them.\n\n"
    "usage: modus_package [options] <root_package_[file|directory]>";

int main(const int argc, const char** argv) {
    CmdOptionString opt_out_path(
        "-o", "--output-dir",
        "Output directory for generated assets. If not specified a directory named "
        "'default_output' will be created next to the executable",
        "");
    CmdOptionU32 opt_jobs("-j", "--jobs", "Number of parallel jobs", 1);
    CmdOptionString opt_log_level(
        "-l", "--log-level", "Set logging level [debug|info|error|warning|verbose|silent]", "");
    CmdOptionEmpty opt_clean("-c", "--clean", "Clean all generated assets");
    CmdOptionString opt_cache_path("", "--cache-file",
                                   "Depedenecy cache path. If not specified a file will be created "
                                   "in the same directory as the binary directory",
                                   "");
    CmdOptionString opt_plugin_dir("", "--plugin-dir",
                                   "Directory from which to load plugins from. By default we look "
                                   "into the package_commands directory next to the executable.",
                                   "");
    CmdOptionString opt_command_filter(
        "", "--command-filter",
        "Only run asset commands and their dependencies if the type matches supplied type\n", "");
    CmdOptionString opt_ext_filter(
        "", "--ext-filter",
        "Only run asset commands and their dependencies if the output matches supplied extension\n",
        "");
    CmdOptionEmpty opt_skip_cache("", "--skip-cache", "Do not use cache file");
    CmdOptionEmpty opt_list_commands("", "--list-commands", "List available commands");
    CmdOptionString opt_command_help(
        "", "--command-help",
        "Display help for command type. Use --command-list to get available command types", "");

    CmdOptionParser opt_parser;
    opt_parser.add(opt_out_path).expect("Failed to add command option");
    opt_parser.add(opt_jobs).expect("Failed to add command option");
    opt_parser.add(opt_cache_path).expect("Failed to add command option");
    opt_parser.add(opt_skip_cache).expect("Failed to add command option");
    opt_parser.add(opt_plugin_dir).expect("Failed to add command option");
    opt_parser.add(opt_clean).expect("Failed to add command option");
    opt_parser.add(opt_log_level).expect("Failed to add command option");
    opt_parser.add(opt_list_commands).expect("Failed to add command option");
    opt_parser.add(opt_command_help).expect("Failed to add command option");
    opt_parser.add(opt_command_filter).expect("Failed to add command option");
    opt_parser.add(opt_ext_filter).expect("Failed to add command option");

    auto r_opt = opt_parser.parse(argc, argv);
    if (!r_opt) {
        std::cerr << fmt::format("{}\n", r_opt.error());
        std::cerr << fmt::format("{}", opt_parser.help_string(kHelpStr));
        return EXIT_FAILURE;
    }

    if (opt_parser.help_requested()) {
        std::cout << fmt::format("{}", opt_parser.help_string(kHelpStr));
        return EXIT_SUCCESS;
    }

    if (opt_command_filter.parsed() && opt_ext_filter.parsed()) {
        std::cerr << "Only one filter can be used at a time\n";
        return EXIT_FAILURE;
    }

    String root_file;
    if (!opt_command_help.parsed() && !opt_list_commands.parsed()) {
        if ((r_opt)->size() == 0) {
            std::cerr << "No input file specified\n";
            std::cerr << fmt::format("{}", opt_parser.help_string(kHelpStr));
            return EXIT_FAILURE;
        }
        root_file = r_opt->at(0);
        if (!os::Path::is_file(root_file)) {
            bool file_exists = false;
            if (os::Path::is_directory(root_file)) {
                os::Path::join_inplace(root_file, "package.json");
                file_exists = os::Path::is_file(root_file);
            }
            if (!file_exists) {
                std::cerr << fmt::format("Could not locate root file {}\n", root_file);
                return EXIT_FAILURE;
            }
        }
    }

    // Setup binary, cache and plugin directory
    String executable_path;
    String output_directory;
    String cache_directory;
    String plugin_directory;

    auto r_exec_path = os::Path::executable_path();
    if (!r_exec_path) {
        std::cerr << "Faild to retrieve executable path\n";
    }
    executable_path = os::Path::get_path(*r_exec_path).to_str();

    if (opt_out_path.parsed()) {
        output_directory = opt_out_path.value_str();
    } else {
        os::Path::join_inplace(output_directory, executable_path, "default_output");
    }

    if (opt_cache_path.parsed()) {
        cache_directory = opt_cache_path.value_str();
    } else {
        cache_directory = output_directory;
    }

    if (opt_plugin_dir.parsed()) {
        plugin_directory = opt_plugin_dir.value_str();
    } else {
        plugin_directory = os::Path::join(StringSlice(executable_path), "package_commands");
    }

    // Create Reporter
    auto reporter = package::StatusReporter::create_default_reporter();
    if (opt_log_level.parsed()) {
        if (opt_log_level.value() == "debug") {
            reporter->set_level(package::StatusReporter::Level::Debug);
        } else if (opt_log_level.value() == "info") {
            reporter->set_level(package::StatusReporter::Level::Info);
        } else if (opt_log_level.value() == "error") {
            reporter->set_level(package::StatusReporter::Level::Error);
        } else if (opt_log_level.value() == "warning") {
            reporter->set_level(package::StatusReporter::Level::Warning);
        } else if (opt_log_level.value() == "verbose") {
            reporter->set_level(package::StatusReporter::Level::Verbose);
        } else if (opt_log_level.value() == "silent") {
            reporter = package::StatusReporter::create_null_reporter();
        } else {
            std::cerr << fmt::format("Unknown value for --log-level: {}\n", opt_log_level.value());
            return EXIT_FAILURE;
        }
    }

    os::Timer timer;
    package::PackageContext context;
    package::PackageContextParams context_params;
    context_params.binary_dir = output_directory;
    context_params.dependency_cache_dir = cache_directory;
    context_params.command_plugin_dir = plugin_directory;
    context_params.use_dependency_cache = !opt_skip_cache.parsed();

    package::PackageContextScope package_scope(context, *reporter);

    if (!context.initialize(*reporter, context_params)) {
        return EXIT_FAILURE;
    }

    std::unique_ptr<package::AssetFilter> asset_filter;

    if (opt_command_filter.parsed()) {
        auto r = context.command_factory().find(opt_command_filter.value());
        if (!r) {
            std::cerr << fmt::format("Could not find any type named '{}'\n",
                                     opt_command_filter.value());
            return EXIT_FAILURE;
        }
        asset_filter = modus::make_unique<TypeFilter>(opt_command_filter.value());
    }
    if (opt_ext_filter.parsed()) {
        asset_filter = modus::make_unique<ExtFilter>(opt_ext_filter.value());
    }

    if (opt_list_commands.parsed()) {
        auto commands = context.command_factory().list_command_types();
        std::cout << "Available Commands:\n";
        for (auto& c : commands) {
            std::cout << fmt::format("\t{}\n", c);
        }
        return EXIT_SUCCESS;
    }

    if (opt_command_help.parsed()) {
        auto r_creator = context.command_factory().find(opt_command_help.value());
        if (!r_creator) {
            std::cerr << fmt::format("No command found for type '{}'\n", opt_command_help.value());
            return EXIT_FAILURE;
        }
        std::cout << (*r_creator)->help_string();
        std::cout << "\n";
        return EXIT_SUCCESS;
    }

    if (!package::PackageContext::load_from_file(context, *reporter, root_file)) {
        return EXIT_FAILURE;
    }

    package::ReportVerbose(*reporter, "Loaded {} assets in {}", context.asset_count(),
                           format_time(timer.elapsed_miliseconds()));

    if (opt_clean.parsed() && !clean_asset_files(context, *reporter)) {
        return EXIT_FAILURE;
    }

    package::RunnerContext runner_context{context};
    if (asset_filter) {
        runner_context.filter = std::move(asset_filter);
    }
    package::RunnerResult runner_result;
    if (opt_jobs.value() == 1) {
        timer.reset();
        package::BasicRunner basic_runner;
        auto r = basic_runner.execute(*reporter, runner_context);
        if (!r) {
            return EXIT_FAILURE;
        }
        runner_result = *r;
    } else {
        timer.reset();
        package::ThreadedRunner thread_runner(opt_jobs.value());
        auto r = thread_runner.execute(*reporter, runner_context);
        if (!r) {
            return EXIT_FAILURE;
        }
        runner_result = *r;
    }
    package::ReportInfo(*reporter, "Finished run with {} commands executed in {}",
                        runner_result.num_commands_executed,
                        format_time(timer.elapsed_miliseconds()));
    return EXIT_SUCCESS;
}
