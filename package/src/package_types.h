/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace modus::package {

struct Asset;
using AssetPtr = NotMyPtr<Asset>;
using ConstAssetPtr = NotMyPtr<const Asset>;

class AssetDB;

class ICommand;
using CommandPtr = NotMyPtr<ICommand>;
class ICommandCreator;
using CommandCreatorPtr = NotMyPtr<ICommandCreator>;
class CommandFactory;

struct Dependency;
using DependencyPtr = NotMyPtr<Dependency>;
using ConstDependencyPtr = NotMyPtr<const Dependency>;
class DependencyDB;

struct JSONValue;
struct JSONContext;
struct JSONObject;

class PackageContext;
class StatusReporter;

}    // namespace modus::package
