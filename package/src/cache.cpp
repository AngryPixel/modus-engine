/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <cache.hpp>
#include <rapidjson_utils.hpp>

namespace modus::package {

static constexpr u32 kElementsPerPage = 128;
Cache::Cache() : m_allocator(kElementsPerPage) {}

Cache::~Cache() {
    clear();
}

static constexpr const char* kCacheRootElem = "cache";
static constexpr const char* kVersionElem = "version";
static constexpr const char* kInputElem = "input";
static constexpr const char* kInputsElem = "inputs";
static constexpr const char* kOutputElem = "output";
static constexpr const char* kTypeElem = "type";
static constexpr const char* kLastModElem = "last_mod";
static constexpr const char* kHashElem = "hash";
static constexpr u32 kVersion = 1;

Result<void, String> Cache::load(ISeekableReader& reader) {
    clear();

    rapidjson::Document doc;
    if (auto r_load = load_json_file(doc, reader); !r_load) {
        return r_load;
    }

    if (!doc.HasMember(kCacheRootElem)) {
        return Error<String>("Could not locate root cache element");
    }

    if (!doc.HasMember(kVersionElem)) {
        return Error<String>("Could not locate version element");
    }

    // Check Version
    {
        const auto& value = doc[kVersionElem];
        if (!value.IsUint()) {
            return Error<String>("Expected uint value for version element");
        }

        if (value.GetUint() != kVersion) {
            return Error<String>(modus::format("Invalid cache version, current={} document={}",
                                               kVersion, value.GetUint()));
        }
    }

    // Load Cache Entries
    const auto& entries = doc[kCacheRootElem];
    if (!entries.IsArray()) {
        return Error<String>("Expected Array for cache element");
    }

    auto read_string = [](String& out, const auto& value,
                          const char* field) -> Result<void, String> {
        if (!value.HasMember(field)) {
            return Error<String>(modus::format("value has no field named {}", field));
        }

        const auto& field_value = value[field];
        if (!field_value.IsString()) {
            return Error<String>(modus::format("field{} is not a string", field));
        }

        out = field_value.GetString();
        return Ok<>();
    };

    auto read_u64 = [](u64& out, const auto& value, const char* field) -> Result<void, String> {
        if (!value.HasMember(field)) {
            return Error<String>(modus::format("value has no field named {}", field));
        }

        const auto& field_value = value[field];
        if (!field_value.IsUint64()) {
            return Error<String>(modus::format("field{} is not a u64", field));
        }

        out = field_value.GetUint64();
        return Ok<>();
    };

    u32 entry_counter = 0;
    const auto& entries_array = entries.GetArray();
    m_cache_elements.reserve(entries_array.Size());
    for (const auto& entry : entries.GetArray()) {
        if (!entry.IsObject()) {
            return Error<String>(
                modus::format("Cache Entry {:02} is not an object", entry_counter));
        }

        CacheEntry e;
        const auto& entry_object = entry.GetObject();

        if (auto r = read_string(e.m_output, entry_object, kOutputElem); !r) {
            return Error(modus::format("Cache Entry {:02} {}", entry_counter, r.error()));
        }

        if (m_cache_elements.find(e.m_output) != m_cache_elements.end()) {
            return Error<String>(modus::format("Duplicate cache element: {}", e.m_output));
        }

        if (!entry_object.HasMember(kInputsElem)) {
            return Error<String>(modus::format("Cache Entry {:02} has no field named {}",
                                               entry_counter, kInputElem));
        }

        const auto& input_elems = entry_object[kInputsElem];
        if (!input_elems.IsArray()) {
            return Error<String>(
                modus::format("Cache Entry {:02} {} is not an array", entry_counter, kInputsElem));
        }

        // Read input array
        u32 input_counter = 0;
        const auto& input_elems_array = input_elems.GetArray();
        e.m_inputs.reserve(input_elems_array.Size());
        for (const auto& input_elem : input_elems_array) {
            if (!input_elem.IsObject()) {
                return Error<String>(
                    modus::format("Cache Entry {:02} input element {:02} is not an object ",
                                  entry_counter, input_counter));
            }

            CacheInput ci;
            const auto& input_object = input_elem.GetObject();

            if (auto r = read_string(ci.m_input, input_object, kInputElem); !r) {
                return Error(modus::format("Cache Entry {:02} input {:02} {}", entry_counter,
                                           input_counter, r.error()));
            }

            if (auto r = read_u64(ci.m_last_time_mod, input_object, kLastModElem); !r) {
                return Error(modus::format("Cache Entry {:02} input {:02} {}", entry_counter,
                                           input_counter, r.error()));
            }

            if (auto r = read_u64(ci.m_hash, input_object, kHashElem); !r) {
                return Error(modus::format("Cache Entry {:02} input {:02} {}", entry_counter,
                                           input_counter, r.error()));
            }
            e.m_inputs.push_back(std::move(ci));
            input_counter++;
        }

        if (auto r = read_string(e.m_type, entry_object, kTypeElem); !r) {
            return Error(modus::format("Cache Entry {:02} {}", entry_counter, r.error()));
        }

        CacheEntryPtr e_ptr = m_allocator.construct(std::move(e));
        m_cache_elements[e_ptr->m_output] = e_ptr;
        entry_counter++;
    }
    return Ok<>();
}

Result<CacheEntryPtr> Cache::find(const String& output) {
    auto it = m_cache_elements.find(output);
    if (it == m_cache_elements.end()) {
        return Error<>();
    }
    return Ok(it->second);
}

Result<ConstCacheEntryPtr> Cache::find(const String& output) const {
    auto it = m_cache_elements.find(output);
    if (it == m_cache_elements.end()) {
        return Error<>();
    }
    return Ok<ConstCacheEntryPtr>(it->second.get());
}

CacheEntryPtr Cache::find_or_create(const String& output) {
    auto it = m_cache_elements.find(output);
    if (it == m_cache_elements.end()) {
        CacheEntryPtr ptr = m_allocator.construct();
        ptr->m_output = output;
        m_cache_elements[output] = ptr;
        return ptr;
    }
    return it->second;
}

Result<> Cache::save(IByteWriter& writer) const {
    using StrRef = rapidjson::Value::StringRefType;
    rapidjson::Document doc;
    doc.SetObject();

    // Add Version
    {
        rapidjson::Value value;
        value.SetUint(kVersion);
        doc.AddMember(StrRef(kVersionElem), value, doc.GetAllocator());
    }

    // create entries
    rapidjson::Value entry_array;
    entry_array.SetArray();

    auto add_string_value = [&doc](auto& value, const String& str, const StrRef field) -> void {
        rapidjson::Value v;
        v.SetString(StrRef(str.c_str()));
        value.AddMember(field, v, doc.GetAllocator());
    };

    auto add_u64_value = [&doc](auto& value, const u64 u, const StrRef field) -> void {
        rapidjson::Value v;
        v.SetUint64(u);
        value.AddMember(field, v, doc.GetAllocator());
    };

    for (const auto& [k, v] : m_cache_elements) {
        rapidjson::Value entry_value;
        entry_value.SetObject();

        rapidjson::Value inputs_values;
        inputs_values.SetArray();
        for (const auto& input : v->m_inputs) {
            rapidjson::Value object_value;
            object_value.SetObject();
            add_string_value(object_value, input.m_input, StrRef(kInputElem));
            add_u64_value(object_value, input.m_last_time_mod, StrRef(kLastModElem));
            add_u64_value(object_value, input.m_hash, StrRef(kHashElem));
            inputs_values.PushBack(object_value, doc.GetAllocator());
        }
        entry_value.AddMember(StrRef(kInputsElem), inputs_values, doc.GetAllocator());
        add_string_value(entry_value, v->m_output, StrRef(kOutputElem));
        add_string_value(entry_value, v->m_type, StrRef(kTypeElem));
        entry_array.PushBack(entry_value, doc.GetAllocator());
    }

    doc.AddMember(StrRef(kCacheRootElem), entry_array, doc.GetAllocator());

    return write_json_file(doc, writer);
}

void Cache::clear() {
    for (auto& [k, v] : m_cache_elements) {
        m_allocator.destroy(v.get());
    }
    m_cache_elements.clear();
}

}    // namespace modus::package
