/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>
#include <os/shared_library.hpp>
#include <package/command.hpp>

namespace modus::package {

class CommandFactory {
   private:
    struct Plugin {
        os::SharedLibrary lib;
        CommandCreatorPtr creator;
        CommandCreatorPluginCreateFn create_fn;
        CommandCreatorPluginDestroyFn destroy_fn;
    };

    Vector<CommandCreatorPtr> m_creators;
    Vector<Plugin> m_plugins;
    Vector<std::unique_ptr<ICommandCreator>> m_builtin_commands;

   public:
    CommandFactory();

    ~CommandFactory();

    MODUS_CLASS_DISABLE_COPY_MOVE(CommandFactory);

    Result<> initialize(StatusReporter& reporter, Slice<String> plugins);

    void shutdown(StatusReporter& reporter);

    Result<NotMyPtr<ICommand>> create(StatusReporter& reporter,
                                      const JSONObject& value,
                                      const modus::StringSlice output_directory,
                                      const modus::StringSlice current_directory,
                                      const modus::StringSlice output_file,
                                      const modus::StringSlice guid);

    Result<> add(CommandCreatorPtr creator);

    Result<CommandCreatorPtr> find(const StringSlice type) const;

    Vector<String> list_command_types() const;
};

}    // namespace modus::package
