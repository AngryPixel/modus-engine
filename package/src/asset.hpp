/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package_types.h>
#include <core/allocator/allocator_libc.hpp>
#include <core/allocator/pool_allocator.hpp>

namespace modus::package {

struct PackSettings {
    String archive;
    bool skip_compression;
};

struct Asset {
    DependencyPtr m_output;
    Vector<DependencyPtr> m_dependencies;
    PackSettings m_archive_settings;
    CommandPtr m_command;
};

struct AssetLoadContext {
    CommandFactory& m_cmd_factory;
    JSONObject& m_json_value;
    StringSlice m_cwd;
    StringSlice m_bin_dir;
};

class AssetDB {
   private:
    DependencyDB& m_dependency_db;
    PoolAllocatorTyped<Asset, AllocatorLibC> m_allocator;
    HashMap<String, AssetPtr> m_assets;
    Vector<AssetPtr> m_list;

   public:
    AssetDB(DependencyDB& dependency_db);

    ~AssetDB();

    MODUS_CLASS_DISABLE_COPY_MOVE(AssetDB);

    void clear();

    Result<AssetPtr> find(const String& output);

    Result<ConstAssetPtr> find(const String& output) const;

    Result<AssetPtr> load(modus::package::StatusReporter& reporter,
                          const AssetLoadContext& context);

    size_t count() const { return m_assets.size(); }

    Slice<AssetPtr> list() const { return make_slice(m_list); }
};

}    // namespace modus::package
