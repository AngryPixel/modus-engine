/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <threaded_runner.hpp>
#include <package_context.hpp>
#include <package/status_reporter.hpp>

#include <core/topo_sort.hpp>
#include <core/disjoint_set.hpp>
#include <os/path.hpp>
#include <os/thread.hpp>
#include <os/locks.hpp>
#include <core/delegate.hpp>

namespace modus::package {

using SorterType = TopologicalSorter<const Dependency, DependencyPtr>;
using UnionFind = DisjointSet<Dependency, DependencyPtr>;

class TaskControl {
   public:
    os::Mutex<StatusReporter*> m_reporter;
    StatusReporter::Level m_log_level;
    volatile bool m_did_error;

   public:
    TaskControl()
        : m_reporter(nullptr), m_log_level(StatusReporter::Level::Info), m_did_error(false) {}

    void signal_error() { m_did_error = true; }

    const volatile bool& error_state() const { return m_did_error; }

    bool finished_with_success() const { return !m_did_error; }
};

class DeferredStatusReporter final : public StatusReporter {
   private:
    struct Message {
        StatusReporter::Level level;
        String message;
    };
    os::Mutex<StatusReporter*>& m_reporter;
    Vector<Message> m_messages;

   public:
    DeferredStatusReporter(os::Mutex<StatusReporter*>& reporter, const StatusReporter::Level level)
        : m_reporter(reporter) {
        set_level(level);
        m_messages.reserve(16);
    }

    ~DeferredStatusReporter() { flush(); }

    void flush() {
        auto accessor = m_reporter.lock();
        for (const auto& m : m_messages) {
            switch (m.level) {
                case StatusReporter::Level::Verbose:
                    (*accessor)->report_verbose(m.message);
                    break;
                case StatusReporter::Level::Error:
                    (*accessor)->report_error(m.message);
                    break;
                case StatusReporter::Level::Warning:
                    (*accessor)->report_warning(m.message);
                    break;
                case StatusReporter::Level::Info:
                    (*accessor)->report_info(m.message);
                    break;
                case StatusReporter::Level::Debug:
                    (*accessor)->report_debug(m.message);
                    break;
                default:
                    ReportError(**accessor, "Unknown message type:{}", m.message);
                    break;
            }
        }
    }

    void report_verbose(const StringSlice msg) override {
        m_messages.push_back({StatusReporter::Level::Verbose, msg.to_str()});
    }
    void report_error(const StringSlice msg) override {
        m_messages.push_back({StatusReporter::Level::Error, msg.to_str()});
    }

    void report_warning(const StringSlice msg) override {
        m_messages.push_back({StatusReporter::Level::Warning, msg.to_str()});
    }

    void report_info(const StringSlice msg) override {
        m_messages.push_back({StatusReporter::Level::Info, msg.to_str()});
    }

    void report_debug(const StringSlice msg) override {
        m_messages.push_back({StatusReporter::Level::Debug, msg.to_str()});
    }
};
class TaskThread final : public os::Thread {
   public:
    TaskControl* m_control;
    RunnerResult m_result;
    const RunnerContext* m_context;
    Slice<Vector<DependencyPtr>> m_work_list;

   public:
    TaskThread() : TaskThread(nullptr) {}
    TaskThread(TaskControl* control) : m_control(control), m_result({0, 0}) {}

   protected:
    void run() override {
        DeferredStatusReporter reporter(m_control->m_reporter, m_control->m_log_level);
        ReportDebug(reporter, "[T{:02}] Startin/mg Dependency Update Scan",
                    os::Thread::current_thread_id());

        RunnerResult total_runner_result{0, 0};
        for (const auto& work_list : m_work_list) {
            RunnerResult runner_result;
            // Early exit on error from another task
            if (m_control->error_state()) {
                return;
            }

            // 1: check dependencies
            for (auto& dep : work_list) {
                if (dep->m_asset && !m_context->filter->apply(dep->m_asset)) {
                    continue;
                }
                if (auto r = check_and_update_dependency_status(reporter, dep); !r) {
                    m_control->signal_error();
                    return;
                } else {
                    runner_result.num_updated_dependencies++;
                }
            }

            // 2: generate update order if something requires updating
            if (runner_result.num_updated_dependencies == 0) {
                // No work to do, got to next item
                continue;
            }

            // Early exit on error from another task
            if (m_control->error_state()) {
                return;
            }

            SorterType sorter;
            for (const auto& dep : work_list) {
                if (dep->m_asset) {
                    if (!m_context->filter->apply(dep->m_asset)) {
                        continue;
                    }

                    for (auto& dependency : dep->m_asset->m_dependencies) {
                        (void)sorter.add(dependency, dependency, Slice<DependencyPtr>());
                    }

                    auto r_add = sorter.add(dep, dep, make_slice(dep->m_asset->m_dependencies));
                    modus_assert(r_add == TopologicalSortError::None);
                    MODUS_UNUSED(r_add);
                }
            }

            Vector<ConstDependencyPtr> ordered_list;
            ordered_list.reserve(work_list.size() / 2);
            const auto r_sort = sorter.evaluate(ordered_list);
            if (r_sort == TopologicalSortError::DependencyCycle) {
                const auto node_where = sorter.node_where_cycle_was_detected();
                const auto node_in_process = sorter.node_being_processed_when_cycle_was_detected();
                ReportError(reporter,
                            "[T{:02}] Dependency Cycle detected on '{}' while processing '{}'",
                            os::Thread::current_thread_id(),
                            node_where ? node_where.value()->m_path : "Unknown",
                            node_in_process ? node_in_process.value()->m_path : "Uknown");
                m_control->signal_error();
                return;
            } else if (r_sort == TopologicalSortError::DependencyNotFound) {
                auto missing_dep = sorter.missing_dependency();
                ReportError(reporter, "[T{:02}] Dependency not found: {}\n",
                            os::Thread::current_thread_id(),
                            missing_dep ? missing_dep.value()->m_path : "Uknown");
                m_control->signal_error();
                return;
            } else if (r_sort != TopologicalSortError::None) {
                ReportError(reporter,
                            "[T{:02}] Unknown Error ecountered during dependency sorting\n",
                            os::Thread::current_thread_id());
                m_control->signal_error();
                return;
            }

            // 3: run update command
            ReportDebug(reporter, "[T{:02}]: Starting command execution",
                        os::Thread::current_thread_id());
            for (auto& dep : ordered_list) {
                // Early exit on error from another task
                if (m_control->error_state()) {
                    return;
                }
                if (dep->m_asset != nullptr) {
                    if (auto r =
                            check_and_execute_asset_command(reporter, *m_context, dep->m_asset);
                        !r) {
                        m_control->signal_error();
                        return;
                    } else if (*r) {
                        runner_result.num_commands_executed++;
                    }
                }
            }

            total_runner_result.num_commands_executed += runner_result.num_commands_executed;
            total_runner_result.num_updated_dependencies += runner_result.num_updated_dependencies;
        }
        m_result = total_runner_result;
    }
};

struct ThreadState {
    Vector<TaskThread> m_threads;
    TaskControl m_task_control;

    ThreadState(const u32 num_threads) {
        m_threads.reserve(num_threads);
        for (u32 i = 0; i < num_threads; ++i) {
            m_threads.push_back(TaskThread(&m_task_control));
        }
    }

    ~ThreadState() {
        for (auto& t : m_threads) {
            t.join();
        }
    }

    Result<RunnerResult> schedule(StatusReporter& reporter,
                                  Slice<Vector<DependencyPtr>> data,
                                  const RunnerContext& context) {
        *m_task_control.m_reporter.lock() = &reporter;
        m_task_control.m_log_level = reporter.level();
        const u32 divisor = u32(std::ceil(f32(data.size()) / f32(m_threads.size())));
        // Less task than threads
        if (divisor == 1) {
            for (usize i = 0; i < data.size(); ++i) {
                m_threads[i].m_work_list = data.sub_slice(i, 1);
            }
        } else {
            usize thread_idx = 0;
            for (usize i = 0; i < data.size(); i += divisor) {
                const u32 start = i;
                const u32 size = divisor;
                m_threads[thread_idx].m_work_list = data.sub_slice(start, size);
                thread_idx++;
            }
        }

        for (auto& t : m_threads) {
            t.m_context = &context;
            if (!t.spawn()) {
                ReportError(reporter, "Failed to start task threads");
                return Error<void>();
            }
        }

        for (auto& t : m_threads) {
            t.join();
        }

        RunnerResult r;
        for (auto& t : m_threads) {
            r.num_commands_executed += t.m_result.num_commands_executed;
            r.num_updated_dependencies += t.m_result.num_updated_dependencies;
        }
        return Ok(r);
    }
};

ThreadedRunner::ThreadedRunner(const u32 num_threads) : m_num_threads(num_threads) {}

ThreadedRunner::~ThreadedRunner() {}

Result<RunnerResult> ThreadedRunner::execute(StatusReporter& reporter,
                                             const RunnerContext& context) {
    //  Initilize threads
    UnionFind grouper;
    Slice<AssetPtr> asset_list = context.package_context.asset_list();
    Slice<DependencyPtr> dependency_list = context.package_context.dependency_list();
    grouper.reserve(dependency_list.size());

    ReportVerbose(reporter, "ThreadedRunner: Grouping {} dependencies", dependency_list.size());
    for (const auto& asset : asset_list) {
        if (!context.filter->apply(asset)) {
            continue;
        }
        // add each dependency as a leaf node, ignore duplicate entries
        for (auto& dep : asset->m_dependencies) {
            (void)grouper.add(dep, dep, Slice<DependencyPtr>());
        }
        auto r_add =
            grouper.add(asset->m_output, asset->m_output, make_slice(asset->m_dependencies));
        modus_assert(r_add == DisjointSetError::Ok);
        MODUS_UNUSED(r_add);
    }
    grouper.evaluate();
    grouper.build_sets();
    auto job_sets = grouper.subsets();

    ThreadState thread_state(m_num_threads);

    ReportVerbose(reporter, "ThreadedRunner: Number of job sets {:03}", job_sets.size());
    if (reporter.level() == StatusReporter::Level::Debug) {
        for (usize i = 0; i < job_sets.size(); ++i) {
            for (const auto& dep : job_sets[i]) {
                ReportDebug(reporter, " [{:03}] {}", i, dep->m_path);
            }
        }
    }
    ReportInfo(reporter,
               "ThreadedRunner: Starting thread jobs, logging will be deferred from now on");
    auto r = thread_state.schedule(reporter, job_sets, context);
    ReportVerbose(reporter, "ThreadedRunner: Finished");
    return r;
}

}    // namespace modus::package
