/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <core/allocator/pool_allocator.hpp>
#include <package/module_config.hpp>

namespace modus::package {

class StatusReporter;

namespace detail {
MODUS_PACKAGE_EXPORT Result<> execute_process(const std::vector<std::string>& args,
                                              const StringSlice path,
                                              StatusReporter& reporter);
}

struct CommandContext {
    StringSlice output;
};

class ICommand {
   public:
    ICommand() = default;
    virtual ~ICommand() = default;
    virtual const char* type() const = 0;
    virtual Result<> execute(StatusReporter& reporter, const CommandContext& context) const = 0;
    /// Get a hash for the command arguments to check whether it needs to re-run should the
    /// arguments change
    virtual u64 command_hash(const CommandContext& context) const = 0;
    virtual Vector<String> command_dependencies() const = 0;
};

class CommandValueParser {
   public:
    virtual Result<String, String> read_string(const char* name) const = 0;

    virtual Result<Vector<String>, String> read_string_array(const char* name) const = 0;

    virtual Result<i32, String> read_i32(const char* name) const = 0;

    virtual Result<u32, String> read_u32(const char* name) const = 0;

    virtual Result<f32, String> read_f32(const char* name) const = 0;

    virtual Result<bool, String> read_bool(const char* name) const = 0;

    virtual Result<Optional<String>, String> read_opt_string(const char* name) const = 0;

    virtual Result<Optional<i32>, String> read_opt_i32(const char* name) const = 0;

    virtual Result<Optional<u32>, String> read_opt_u32(const char* name) const = 0;

    virtual Result<Optional<f32>, String> read_opt_f32(const char* name) const = 0;

    virtual Result<Optional<bool>, String> read_opt_bool(const char* name) const = 0;
};

struct CommandCreateParams {
    CommandValueParser& parser;
    StringSlice output_directory;
    StringSlice current_directory;
    StringSlice output_file;
    StringSlice guid;
};

class ICommandCreator {
   public:
    ICommandCreator() = default;
    virtual ~ICommandCreator() = default;

    virtual const char* type() const = 0;

    virtual Result<> initialize(StatusReporter& reporter) = 0;

    virtual void shutdown() = 0;

    virtual Result<ICommand*> create(StatusReporter& reporter,
                                     const CommandCreateParams& parser) = 0;

    virtual const char* help_string() const = 0;
};

using CommandCreatorPluginCreateFn = ICommandCreator* (*)();
using CommandCreatorPluginDestroyFn = void (*)(ICommandCreator*);

template <typename T, typename Allocator>
class CommandPool {
   private:
    PoolAllocatorTyped<T, Allocator> m_pool;
    Vector<T*, Allocator> m_commands;

   public:
    CommandPool(const size_t elements_per_block) : m_pool(elements_per_block), m_commands() {
        m_commands.reserve(elements_per_block);
    }

    template <typename... Args>
    T* create(Args&&... args) {
        return m_pool.construct(std::forward<Args>(args)...);
    }

    void clear() {
        for (auto& t : m_commands) {
            t->~T();
        }
        m_commands.clear();
    }
};

#define MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME modus_package_command_create_creator
#define MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME_STR "modus_package_command_create_creator"

#define MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME modus_package_command_destroy_creator
#define MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME_STR "modus_package_command_destroy_creator"

#if defined(MODUS_OS_UNIX)
#define MODUS_PACKAGE_COMMAND_EXPORT [[gnu::visibility("default")]]
#else
#define MODUS_PACKAGE_COMMAND_EXPORT __declspec(dllexport)
#endif
}    // namespace modus::package
