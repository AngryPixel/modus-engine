/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <package/package.hpp>

#include <package/module_config.hpp>

namespace modus::package {
class StatusReporter {
   public:
    enum class Level { Error, Info, Warning, Verbose, Debug, Total };

   private:
    Level m_level;

   protected:
    StatusReporter();

   public:
    static std::unique_ptr<StatusReporter> create_default_reporter();

    static std::unique_ptr<StatusReporter> create_null_reporter();

    virtual ~StatusReporter() = default;

    virtual void report_verbose(const StringSlice) = 0;

    virtual void report_error(const StringSlice) = 0;

    virtual void report_warning(const StringSlice) = 0;

    virtual void report_info(const StringSlice) = 0;

    virtual void report_debug(const StringSlice) = 0;

    void set_level(const Level l);

    Level level() const { return m_level; }
};

namespace detail {
MODUS_PACKAGE_EXPORT StringSlice reporter_format(const StringSlice fmt, fmt::format_args args);
}

template <typename... Args>
inline void ReportInfo(StatusReporter& r, const StringSlice fmt, const Args&... args) {
    if (r.level() >= StatusReporter::Level::Info) {
        r.report_info(detail::reporter_format(fmt, fmt::make_format_args(args...)));
    }
}

template <typename... Args>
inline void ReportWarning(StatusReporter& r, const StringSlice fmt, const Args&... args) {
    if (r.level() >= StatusReporter::Level::Warning) {
        r.report_warning(detail::reporter_format(fmt, fmt::make_format_args(args...)));
    }
}

template <typename... Args>
inline void ReportError(StatusReporter& r, const StringSlice fmt, const Args&... args) {
    if (r.level() >= StatusReporter::Level::Error) {
        r.report_error(detail::reporter_format(fmt, fmt::make_format_args(args...)));
    }
}

template <typename... Args>
inline void ReportVerbose(StatusReporter& r, const StringSlice fmt, const Args&... args) {
    if (r.level() >= StatusReporter::Level::Verbose) {
        r.report_verbose(detail::reporter_format(fmt, fmt::make_format_args(args...)));
    }
}
template <typename... Args>
inline void ReportDebug(StatusReporter& r, const StringSlice fmt, const Args&... args) {
    if (r.level() >= StatusReporter::Level::Debug) {
        r.report_debug(detail::reporter_format(fmt, fmt::make_format_args(args...)));
    }
}

}    // namespace modus::package
