
/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/core_pch.h>
#include <iostream>
#include <package/command.hpp>
#include <package/status_reporter.hpp>

using namespace modus;

class TestCommand final : public package::ICommand {
   private:
    String m_echo;

   public:
    TestCommand(StringSlice echo) : m_echo(echo.to_str()) {}

    const char* type() const override { return "test"; }

    Result<> execute(package::StatusReporter& reporter,
                     const package::CommandContext& context) const override {
        package::ReportInfo(reporter, "{} :: {}\n", context.output, m_echo);
        return Ok<>();
    }

    u64 command_hash(const package::CommandContext&) const override {
        return hash_data64(m_echo.data(), m_echo.size());
    }

    Vector<String> command_dependencies() const override { return {}; }
};

class TestCommandCreator final : public package::ICommandCreator {
   private:
    Vector<std::unique_ptr<TestCommand>> m_commands;

   public:
    const char* type() const override { return "test"; }

    Result<> initialize(package::StatusReporter& reporter) override {
        package::ReportDebug(reporter, "Initializing TestCommandCreator");
        return Ok<>();
    }

    void shutdown() override { m_commands.clear(); }

    Result<package::ICommand*> create(package::StatusReporter& reporter,
                                      const package::CommandCreateParams& params) override {
        auto r_str = params.parser.read_string("echo");
        if (!r_str) {
            package::ReportError(reporter, "Failed to load test command: {}", r_str.error());
            return Error<>();
        }
        m_commands.push_back(std::make_unique<TestCommand>(*r_str));
        return Ok<package::ICommand*>(m_commands.back().get());
    }

    const char* help_string() const override { return "Test Command"; }
};

extern "C" {

MODUS_PACKAGE_COMMAND_EXPORT modus::package::ICommandCreator*
MODUS_PACKAGE_COMMAND_PLUGIN_FN_CREATE_NAME() {
    return new TestCommandCreator();
}

MODUS_PACKAGE_COMMAND_EXPORT void MODUS_PACKAGE_COMMAND_PLUGIN_FN_DESTROY_NAME(
    modus::package::ICommandCreator* ptr) {
    delete ptr;
}
}
