/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// this test currently doesn't work on windows due to the symbol resolution issues
#if !defined(MODUS_OS_WIN32)
#include <pch.h>
#include <cache.hpp>
#include <catch2/catch.hpp>
#include <core/io/memory_stream.hpp>

#include <asset.hpp>
#include <command_factory.hpp>
#include <dependency.hpp>
#include <os/path.hpp>
#include <package/command.hpp>
#include <package/status_reporter.hpp>
#include <rapidjson_utils.hpp>

#include <iostream>

using namespace modus;

constexpr const char* kAssetJson =
    R"B(
{
  "asset": {
    "output": "foo.bin",
    "deps": [],
    "test": {
      "echo": "Foo"
    },
    "pack_opt": {
      "archive": "foo",
      "skip_compression": true
    }
  }
}
)B";

#if defined(MODUS_OS_UNIX)
static constexpr const char* kLibPath = "command_plugins/libtest_package_command.so";
#else
static constexpr const char* kLibPath = "command_plugins/test_package_command.dll";
#endif

TEST_CASE("Test Asset Load with test command", "[Asset]") {
    package::DependencyDB dependency_db;
    package::AssetDB asset_db(dependency_db);
    package::CommandFactory cmd_factory;

    auto exec_dir =
        os::Path::executable_directory().value_or_panic("failed to get executable directory");

    String plugin_file = os::Path::join(exec_dir, kLibPath);
    auto reporter = package::StatusReporter::create_default_reporter();
    reporter->set_level(package::StatusReporter::Level::Debug);

    auto r_factory = cmd_factory.initialize(*reporter, Slice<String>(&plugin_file, 1));
    REQUIRE(r_factory);

    rapidjson::Document doc;
    SliceStream stream(StringSlice(kAssetJson).as_bytes());
    REQUIRE(package::load_json_file(doc, stream));

    REQUIRE(doc.HasMember("asset"));
    const auto& asset_value = doc["asset"];
    REQUIRE(asset_value.IsObject());

    package::JSONObject json_object{asset_value.GetObject()};
    package::AssetLoadContext asset_context{cmd_factory, json_object, "", ""};
    REQUIRE(asset_db.load(*reporter, asset_context));

    auto r_asset = asset_db.find("foo.bin");
    REQUIRE(r_asset);
    package::AssetPtr asset = *r_asset;

    REQUIRE(asset->m_output->m_path == "foo.bin");
    REQUIRE(asset->m_archive_settings.archive == "foo");
    REQUIRE(asset->m_archive_settings.skip_compression);
    REQUIRE(asset->m_command);

    package::CommandContext cmd_ctx{"foo.bi"};
    auto r_cmd = asset->m_command->execute(*reporter, cmd_ctx);
    REQUIRE(r_cmd);
    cmd_factory.shutdown(*reporter);
}
#endif
