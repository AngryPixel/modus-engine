/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>
#include <cache.hpp>
#include <catch2/catch.hpp>
#include <core/io/memory_stream.hpp>
#include <iostream>

using namespace modus;

constexpr const char* kBasicCacheJson =
    R"B(
{
    "version":1,
    "cache":[
        {
            "inputs": [
                {
                    "input": "foo.cpp",
                    "last_mod": 0,
                    "hash": 0
                },
                {
                    "input": "bar.cpp",
                    "last_mod": 1024,
                    "hash": 2048
                }
             ],
            "output": "foobar",
            "type": "test"
        }
    ]
}
)B";

void validate_basic_cache(package::Cache& cache) {
    auto r_entry = cache.find("foobar");
    REQUIRE(r_entry);

    package::CacheEntryPtr ptr = *r_entry;

    REQUIRE(ptr->m_type == "test");
    REQUIRE(ptr->m_output == "foobar");
    const auto& inputs = ptr->m_inputs;

    REQUIRE(inputs.size() == 2);

    REQUIRE(inputs[0].m_hash == 0);
    REQUIRE(inputs[0].m_last_time_mod == 0);
    REQUIRE(inputs[0].m_input == "foo.cpp");

    REQUIRE(inputs[1].m_hash == 2048);
    REQUIRE(inputs[1].m_last_time_mod == 1024);
    REQUIRE(inputs[1].m_input == "bar.cpp");
}

TEST_CASE("Test Cache Load and Write", "[Cache]") {
    RamStream ram_stream;
    ram_stream.reserve(1024).expect("Failed to reserve memory for ram_stream");

    {
        SliceStream stream(StringSlice(kBasicCacheJson).as_bytes());
        package::Cache cache;
        auto r_cache = cache.load(stream);
        if (!r_cache) {
            std::cerr << r_cache.error() << '\n';
        }
        REQUIRE(r_cache);

        validate_basic_cache(cache);

        REQUIRE(cache.save(ram_stream));
    }

    ram_stream.seek(0).expect("Failed to seek 0 on ram_stream");
    package::Cache cache;
    auto r_cache = cache.load(ram_stream);
    if (!r_cache) {
        std::cerr << r_cache.error() << '\n';
        std::cerr << fmt::format("{}\n", ram_stream.as_string_slice());
    }
    REQUIRE(r_cache);
    validate_basic_cache(cache);
}
