/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>
#include <cache.hpp>
#include <catch2/catch.hpp>
#include <core/io/memory_stream.hpp>

#include <asset.hpp>
#include <basic_runner.hpp>
#include <iostream>
#include <os/file.hpp>
#include <os/path.hpp>
#include <os/time.hpp>
#include <package/status_reporter.hpp>
#include <package_context.hpp>

using namespace modus;

TEST_CASE("Load Null Asset", "[PackageContext]") {
    package::PackageContext context;
    auto reporter = package::StatusReporter::create_null_reporter();
    const String binary_dir = os::Path::join(MODUS_TEST_BINARY_DIR, "data");
    package::PackageContextParams context_params{
        "",
        "",
        binary_dir,
        false,
    };

    REQUIRE(context.initialize(*reporter, context_params));

    const String data_dir = os::Path::join(MODUS_TEST_SOURCE_DIR, "../data");
    const String package_file = os::Path::join(StringSlice(data_dir), "null_asset/package.json");
    auto r_load = package::PackageContext::load_from_file(context, *reporter, package_file);
    REQUIRE(context.asset_count() == 1);
    REQUIRE(r_load);
    REQUIRE(context.shutdown(*reporter));
}

TEST_CASE("Load Invalid Dep", "[PackageContext]") {
    package::PackageContext context;
    auto reporter = package::StatusReporter::create_null_reporter();
    const String binary_dir = os::Path::join(MODUS_TEST_BINARY_DIR, "data");
    package::PackageContextParams context_params{
        "",
        "",
        binary_dir,
        false,
    };

    REQUIRE(context.initialize(*reporter, context_params));

    const String data_dir = os::Path::join(MODUS_TEST_SOURCE_DIR, "../data");
    const String package_file =
        os::Path::join(StringSlice(data_dir), "invalid_dependency/package.json");
    auto r_load = package::PackageContext::load_from_file(context, *reporter, package_file);
    REQUIRE_FALSE(r_load);
    REQUIRE(context.shutdown(*reporter));
}

TEST_CASE("Package with bin dep", "[PackageContext]") {
    package::PackageContext context;
    auto reporter = package::StatusReporter::create_null_reporter();
    const String binary_dir = os::Path::join(MODUS_TEST_BINARY_DIR, "data");
    package::PackageContextParams context_params{
        "",
        "",
        binary_dir,
        false,
    };

    REQUIRE(context.initialize(*reporter, context_params));

    const String data_dir = os::Path::join(MODUS_TEST_SOURCE_DIR, "../data");
    const String package_file =
        os::Path::join(StringSlice(data_dir), "bin_dependency/package.json");
    auto r_load = package::PackageContext::load_from_file(context, *reporter, package_file);
    REQUIRE(context.asset_count() == 2);
    REQUIRE(r_load);
    REQUIRE(context.shutdown(*reporter));
}

TEST_CASE("Package with data dep", "[PackageContext]") {
    package::PackageContext context;
    auto reporter = package::StatusReporter::create_null_reporter();
    const String binary_dir = os::Path::join(MODUS_TEST_BINARY_DIR, "data");
    package::PackageContextParams context_params{
        "",
        "",
        binary_dir,
        false,
    };

    REQUIRE(context.initialize(*reporter, context_params));

    const String data_dir = os::Path::join(MODUS_TEST_SOURCE_DIR, "../data");
    const String package_file =
        os::Path::join(StringSlice(data_dir), "data_dependency/package.json");
    auto r_load = package::PackageContext::load_from_file(context, *reporter, package_file);
    REQUIRE(context.asset_count() == 1);
    REQUIRE(r_load);
    REQUIRE(context.shutdown(*reporter));
}

void execute_basic_runner(const bool clear_data, const bool use_cache_file) {
    const String data_dir = os::Path::join(MODUS_TEST_SOURCE_DIR, "../data");
    const String binary_dir = os::Path::join(MODUS_TEST_BINARY_DIR, "data");
    const String test_file = os::Path::join(data_dir, "package.json");

    // Cleanup previous data
    if (clear_data) {
        (void)os::Path::remove(binary_dir);
    }

    auto reporter = package::StatusReporter::create_default_reporter();
    reporter->set_level(package::StatusReporter::Level::Debug);
    package::PackageContext context;
    package::PackageContextParams context_params{"./command_plugins", binary_dir, binary_dir};

    auto r_context = context.initialize(*reporter, context_params);
    REQUIRE(r_context);
    auto r_load = package::PackageContext::load_from_file(context, *reporter, test_file);
    REQUIRE(r_load);
    REQUIRE(context.asset_count() == 2);

    package::RunnerContext runner_context{context};
    package::BasicRunner runner;
    auto r_runner = runner.execute(*reporter, runner_context);
    REQUIRE(r_runner);

    if (!use_cache_file) {
        // 3 dependencies, foo.bin, bar.bin and input.txt
        CHECK(r_runner->num_updated_dependencies == 3);
        CHECK(r_runner->num_commands_executed == 2);

        // check if files have been created
        const String foo_bin_path = os::Path::join(binary_dir, "out/foo.bin");
        const String bar_bin_path = os::Path::join(binary_dir, "out/bar.bin");
        REQUIRE(os::Path::is_file(foo_bin_path));
        REQUIRE(os::Path::is_file(bar_bin_path));

        // Run the runner again, nothing has changed, so nothing to do
        r_runner = runner.execute(*reporter, runner_context);
        REQUIRE(r_runner);
        CHECK(r_runner->num_updated_dependencies == 0);
        CHECK(r_runner->num_commands_executed == 0);

        // touch foo.bin
        {
            const String input_txt_path = os::Path::join(data_dir, "input.txt");
            auto r_touch = os::FileBuilder().create().write().open(input_txt_path);
            REQUIRE(r_touch);
            REQUIRE(r_touch->write_exactly(StringSlice(fmt::to_string(rand())).as_bytes()));
            r_touch->flush();
            // Sleep 2 seconds so that we can register the file update
            os::sleep_sec(ISeconds(2));
        }

        // Run the runner again, this time only one update should happen becaus of foo.bin
        r_runner = runner.execute(*reporter, runner_context);
        REQUIRE(r_runner);
        CHECK(r_runner->num_updated_dependencies == 1);
        CHECK(r_runner->num_commands_executed == 1);
    } else {
        // When using cache file from previous run, there's nothing to do
        CHECK(r_runner->num_updated_dependencies == 0);
        CHECK(r_runner->num_commands_executed == 0);
    }

    auto r_shudown = context.shutdown(*reporter);
    REQUIRE(r_shudown);
}

TEST_CASE("Load File Test and Execute", "[PackageContext]") {
    execute_basic_runner(true, false);
}

TEST_CASE("Load File Test and Execute With Cache Round 1", "[PackageContext]") {
    execute_basic_runner(true, false);
}
TEST_CASE("Load File Test and Execute With Cache Round 2", "[PackageContext]") {
    execute_basic_runner(false, true);
}

TEST_CASE("Recursive Package Load", "[PackageContext]") {
    const String data_dir = os::Path::join(MODUS_TEST_SOURCE_DIR, "../data/recursive");
    const String binary_dir = os::Path::join(MODUS_TEST_BINARY_DIR, "data/recursive");
    const String test_file = os::Path::join(data_dir, "package.json");

    auto reporter = package::StatusReporter::create_default_reporter();
    reporter->set_level(package::StatusReporter::Level::Debug);
    package::PackageContext context;
    package::PackageContextParams context_params{"./command_plugins", binary_dir, binary_dir};
    auto r_context = context.initialize(*reporter, context_params);
    REQUIRE(r_context);
    auto r_load = package::PackageContext::load_from_file(context, *reporter, test_file);
    REQUIRE(r_load);
    REQUIRE(context.asset_count() == 3);

    // Run basic runner
    package::RunnerContext runner_context{context};
    package::BasicRunner runner;
    auto r_runner = runner.execute(*reporter, runner_context);
    REQUIRE(r_runner);
    REQUIRE(r_runner->num_updated_dependencies == 4);
    REQUIRE(r_runner->num_commands_executed == 3);

    // check if files have been created
    const String foo_bin_path = os::Path::join(binary_dir, "out/foo.bin");
    const String bar_bin_path = os::Path::join(binary_dir, "out/bar.bin");
    const String z_bin_path = os::Path::join(binary_dir, "out/z.bin");
    REQUIRE(os::Path::is_file(foo_bin_path));
    REQUIRE(os::Path::is_file(bar_bin_path));
    REQUIRE(os::Path::is_file(z_bin_path));

    auto r_shudown = context.shutdown(*reporter);
    REQUIRE(r_shudown);
}
