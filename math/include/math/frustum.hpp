/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/containers.hpp>
#include <math/bounding_volumes.hpp>
#include <math/math.hpp>

namespace modus::math {

enum class FrustumMode : u8 { Perspective, Orthographic };

class MODUS_MATH_EXPORT Frustum {
   private:
    f32 m_near = 0.1f;
    f32 m_far = 100.0f;
    f32 m_fov_degrees = 75.0f;
    f32 m_aspect_ratio = 1.0f;
    f32 m_bottom = -1.0f;
    f32 m_top = 1.0f;
    f32 m_left = -1.0f;
    f32 m_right = 1.0f;
    mutable glm::mat4 m_matrix;
    FrustumMode m_mode = FrustumMode::Perspective;
    mutable bool m_dirty = true;

   public:
    inline f32 fov_degrees() const { return m_fov_degrees; }

    inline f32 near_range() const { return m_near; }

    inline f32 far_range() const { return m_far; }

    inline f32 bottom() const { return m_bottom; }

    inline f32 top() const { return m_top; }

    inline f32 left() const { return m_left; }

    inline f32 right() const { return m_right; }

    inline FrustumMode mode() const { return m_mode; }

    inline f32 aspect_ratio() const { return m_aspect_ratio; }

    inline bool is_perspective() const { return m_mode == FrustumMode::Perspective; }

    inline bool is_orthographic() const { return m_mode == FrustumMode::Orthographic; }

    inline void set_perspective(const f32 fov_degrees,
                                const f32 aspect_ratio,
                                const f32 near_range,
                                const f32 far_range) {
        m_dirty = true;
        m_mode = FrustumMode::Perspective;
        m_fov_degrees = fov_degrees;
        m_near = near_range;
        m_far = far_range;
        m_aspect_ratio = aspect_ratio;
    }

    inline void set_near_far(const f32 near_range, const f32 far_range) {
        m_near = near_range;
        m_far = far_range;
        m_dirty = true;
    }

    inline void set_aspect_ratio(const f32 aspect_ratio) {
        m_dirty = true;
        m_mode = FrustumMode::Perspective;
        m_aspect_ratio = aspect_ratio;
    }

    inline void set_fov_degrees(const f32 fov_degrees) {
        m_dirty = true;
        m_mode = FrustumMode::Perspective;
        m_fov_degrees = fov_degrees;
    }

    inline void set_orthograhic(const f32 near_range,
                                const f32 far_range,
                                const f32 top,
                                const f32 bottom,
                                const f32 left,
                                const f32 right) {
        m_far = far_range;
        m_near = near_range;
        m_top = top;
        m_bottom = bottom;
        m_left = left;
        m_right = right;
        m_mode = FrustumMode::Orthographic;
        m_dirty = true;
    }

    inline const glm::mat4& matrix() const {
        if (m_dirty) {
            if (m_mode == FrustumMode::Perspective) {
                m_matrix =
                    glm::perspective(glm::radians(m_fov_degrees), m_aspect_ratio, m_near, m_far);
            } else {
                m_matrix = glm::ortho(m_left, m_right, m_bottom, m_top, m_near, m_far);
            }

            m_dirty = false;
        }
        return m_matrix;
    }

    inline void set_ortographic_rect(const f32 top,
                                     const f32 bottom,
                                     const f32 left,
                                     const f32 right) {
        m_top = top;
        m_bottom = bottom;
        m_left = left;
        m_right = right;
        m_mode = FrustumMode::Orthographic;
        m_dirty = true;
    }
};

struct MODUS_MATH_EXPORT FrustumPlanes {
    static constexpr u32 kNear = 0;
    static constexpr u32 kFar = 1;
    static constexpr u32 kLeft = 2;
    static constexpr u32 kRight = 3;
    static constexpr u32 kTop = 4;
    static constexpr u32 kBottom = 5;
    bv::Plane planes[6];

    void extract(const glm::mat4& matrix);

    bool is_visible(const glm::vec3& point) const;
    bool is_visible(const bv::Sphere& sphere) const;
    bool is_visible(const bv::AABB& aabb) const;
    bool is_visible(const bv::Line& line) const;
};

static constexpr u32 kFrustumPointFarTopRight = 0;
static constexpr u32 kFrustumPointFarBottomRight = 1;
static constexpr u32 kFrustumPointFarBottomLeft = 2;
static constexpr u32 kFrustumPointFarTopLeft = 3;
static constexpr u32 kFrustumPointNearTopRight = 4;
static constexpr u32 kFrustumPointNearBottomRight = 5;
static constexpr u32 kFrustumPointNearBottomLeft = 6;
static constexpr u32 kFrustumPointNearTopLeft = 7;

using FrustumPoints = Array<glm::vec3, 8>;
MODUS_MATH_EXPORT void extract_frustum_points(FrustumPoints& points, const glm::mat4& vp);

}    // namespace modus::math
