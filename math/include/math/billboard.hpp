/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <math/math.hpp>

namespace modus::math::billboard {

/// Calculate rotation required to have a screen facing billboard
inline glm::quat screen_facing_rotation(const glm::mat4& view_matrix) {
    return glm::quat_cast(glm::transpose(glm::mat3(view_matrix)));
}

/// Calculate rotation required to have a billboard rotate around a given axis. Axis must be
/// normalized.
inline glm::quat axis_aligned_rotation(const glm::vec3& camera_position,
                                       const glm::vec3& world_position,
                                       const glm::vec3& axis) {
    // Optimization for Geometry Shader, based on calculated up vector:
    // https://gamedev.stackexchange.com/questions/153326/how-to-rotate-directional-billboard-particle-sprites-toward-the-direction-the-pa/153814#153814
    const glm::vec3 direction = glm::normalize(world_position - camera_position);
    const glm::vec3 right = glm::normalize(glm::cross(axis, direction));
    const glm::vec3 billboard_direction = glm::cross(right, axis);
    const glm::vec3 billboard_up_vec = glm::cross(billboard_direction, right);
    return glm::quatLookAt(billboard_direction, billboard_up_vec);
}

}    // namespace modus::math::billboard