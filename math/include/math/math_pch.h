/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <core/core_pch.h>
#include <cmath>

// GLM Math
#include <glm/exponential.hpp>
#include <glm/gtc/packing.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/matrix.hpp>
#include <glm/trigonometric.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

// ----------------------------------------------------------------------------
// Extensions to the GLM library
// lenghtSquared
// distanceSquared
// ----------------------------------------------------------------------------
namespace glm {

template <typename T, precision P>
GLM_FUNC_QUALIFIER T length_sqr(glm::tvec2<T, P> const& v) {
    GLM_STATIC_ASSERT(std::numeric_limits<T>::is_iec559,
                      "'length' only accept floating-point inputs");
    T sqr = v.x * v.x + v.y * v.y;
    return sqr;
}

template <typename T, precision P>
GLM_FUNC_QUALIFIER T length_sqr(glm::tvec3<T, P> const& v) {
    GLM_STATIC_ASSERT(std::numeric_limits<T>::is_iec559,
                      "'length' only accept floating-point inputs");
    T sqr = v.x * v.x + v.y * v.y + v.z * v.z;
    return sqr;
}

}    // namespace glm

// fmt formatters
template <>
struct fmt::formatter<glm::vec2> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const glm::vec2& vec, FormatContext& ctx) {
        return format_to(ctx.out(), "vec2{{ x:{}, y:{} }}", vec.x, vec.y);
    }
};
template <>
struct fmt::formatter<glm::vec3> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const glm::vec3& vec, FormatContext& ctx) {
        return format_to(ctx.out(), "vec3{{ x:{}, y:{}, z:{} }}", vec.x, vec.y, vec.z);
    }
};

template <>
struct fmt::formatter<glm::vec4> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const glm::vec4& vec, FormatContext& ctx) {
        return format_to(ctx.out(), "vec4{{ x:{}, y:{}, z:{}, w:{} }}", vec.x, vec.y, vec.z, vec.w);
    }
};

template <>
struct fmt::formatter<glm::quat> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const glm::quat& q, FormatContext& ctx) {
        return format_to(ctx.out(), "quat{{ w:{}, x:{}, y:{}, z:{} }}", q.w, q.x, q.y, q.z);
    }
};

template <>
struct fmt::formatter<glm::mat4> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const glm::mat4& m, FormatContext& ctx) {
        return format_to(ctx.out(), "mat{{ {}, {}, {}, {} }}", m[0], m[1], m[2], m[3]);
    }
};
