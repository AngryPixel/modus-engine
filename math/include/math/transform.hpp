/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <math/math.hpp>

namespace modus::math {

struct MODUS_MATH_EXPORT Transform {
    static const Transform kIdentity;

    glm::quat m_rotation;
    glm::vec3 m_position;
    f32 m_scale;

    Transform() = default;
    MODUS_CLASS_DEFAULT_COPY_MOVE(Transform);

    const glm::vec3& translation() const { return m_position; }
    const glm::quat& rotation() const { return m_rotation; }
    f32 scale() const { return m_scale; }

    void translate(const glm::vec3& translation);
    void translate(const f32 x, const f32 y, const f32 z);
    void set_translation(const glm::vec3& translation);
    void set_translation(const f32 x, const f32 y, const f32 z);
    Transform translated(const glm::vec3& translation) const;
    Transform translated(const f32 x, const f32 y, const f32 z) const;

    void rotate(const glm::quat& rotation);
    void rotate_prefix(const glm::quat& rotation);
    void set_rotation(const glm::quat& rotation);
    Transform rotated(const glm::quat& rotation) const;
    Transform rotated_prefix(const glm::quat& rotation) const;

    void scale(const f32 scale);
    void set_scale(const f32 scale);
    Transform scaled(const f32 scale) const;

    void invert();
    Transform inverted() const;

    glm::vec3 up() const;
    glm::vec3 forward() const;
    glm::vec3 right() const;

    glm::mat4 to_matrix() const;

    void transform(const Transform& other);
    void set_transform(const Transform& other);
    Transform transformed(const Transform& other) const;

    void set_look_at(const glm::vec3& eye, const glm::vec3& target, const glm::vec3& up);

    void set_identity();

    void reset();

    glm::vec3 transform(const glm::vec3& point) const;

    static Transform lerp(const Transform& t1, const Transform& t2, const f32 progression);
};

/// Cached Transform components.
///
/// m_matirx_update signals that this the cached matrix needs updating.
/// m_dirty signals that there was a change mad to this transform.
class MODUS_MATH_EXPORT CachedTransform : protected Transform {
   private:
    mutable glm::mat4 m_matrix = glm::mat4(1.0f);
    mutable bool m_dirty = false;

   public:
    CachedTransform() { static_cast<Transform&>(*this) = Transform::kIdentity; }

    inline const glm::mat4& to_matrix() const {
        if (m_dirty) {
            m_matrix = Transform::to_matrix();
            clear_dirty();
        }
        return m_matrix;
    }

    inline void override_matrix(const glm::mat4& mat) {
        clear_dirty();
        m_matrix = mat;
    }

    void clear_dirty() const { m_dirty = false; }
    void mark_dirty() const { m_dirty = true; }
    bool is_dirty() const { return m_dirty; }

    using Transform::rotation;
    using Transform::scale;
    using Transform::translation;

    void translate(const glm::vec3& translation) {
        Transform::translate(translation);
        m_dirty = true;
    }
    void translate(const f32 x, const f32 y, const f32 z) {
        Transform::translate(x, y, z);
        m_dirty = true;
    }
    void set_translation(const glm::vec3& translation) {
        Transform::set_translation(translation);
        m_dirty = true;
    }
    void set_translation(const f32 x, const f32 y, const f32 z) {
        Transform::set_translation(x, y, z);
        m_dirty = true;
    }
    using Transform::translated;

    void rotate(const glm::quat& rotation) {
        Transform::rotate(rotation);
        m_dirty = true;
    }
    void rotate_prefix(const glm::quat& rotation) {
        Transform::rotate_prefix(rotation);
        m_dirty = true;
    }
    void set_rotation(const glm::quat& rotation) {
        Transform::set_rotation(rotation);
        m_dirty = true;
    }
    using Transform::rotated;
    using Transform::rotated_prefix;

    void scale(const f32 scale) {
        Transform::scale(scale);
        m_dirty = true;
    }
    void set_scale(const f32 scale) {
        Transform::set_scale(scale);
        m_dirty = true;
    }
    using Transform::scaled;

    void invert() {
        Transform::invert();
        m_dirty = true;
    }
    using Transform::inverted;

    using Transform::forward;
    using Transform::right;
    using Transform::up;

    void transform(const Transform& other) {
        Transform::transform(other);
        m_dirty = true;
    }
    void set_transform(const Transform& other) {
        Transform::set_transform(other);
        m_dirty = true;
    }
    using Transform::transformed;
    void set_look_at(const glm::vec3& eye, const glm::vec3& target, const glm::vec3& up) {
        Transform::set_look_at(eye, target, up);
        m_dirty = true;
    }

    const Transform& as_transform() const { return *this; }

    void reset() {
        Transform::reset();
        m_dirty = true;
    }
};

}    // namespace modus::math

template <>
struct fmt::formatter<modus::math::Transform> {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx) {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const modus::math::Transform& t, FormatContext& ctx) {
        return format_to(ctx.out(), "Transform{{ t:{}, r:{}, s:{}}}", t.m_position, t.m_rotation,
                         t.m_scale);
    }
};
