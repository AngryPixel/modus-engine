/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <math/math.hpp>

namespace modus::math {
struct Transform;
}

namespace modus::math::bv {

// ----------------------------------------------------------------------------
// Ray
struct MODUS_MATH_EXPORT Ray {
    glm::vec3 origin = glm::vec3(0.0f);
    glm::vec3 direction = glm::vec3(0.0f, 0.0, 1.0f);
    f32 range = 1.0f;

    glm::vec3 point_at(const f32 distance) const { return origin + (direction * distance); }
};

inline Ray make_ray_infinite(const glm::vec3& origin, const glm::vec3& direction) {
    Ray r;
    r.origin = origin;
    r.direction = direction;
    r.range = std::numeric_limits<f32>::max();
    return r;
}

// ----------------------------------------------------------------------------
struct MODUS_MATH_EXPORT Line {
    glm::vec3 from = glm::vec3(0.f);
    glm::vec3 to = glm::vec3(1.f);

    glm::vec3 direction() const { return glm::normalize(to - from); }
};

// ----------------------------------------------------------------------------
// Plane

struct MODUS_MATH_EXPORT Plane {
    glm::vec3 direction = glm::vec3(1.0f, 0.0f, 0.0f);
    f32 offset = f32(1.0);

    inline glm::vec3 center() const { return direction * offset; }

    inline f32 test(const glm::vec3& point) const { return glm::dot(direction, point) + offset; }
};

MODUS_MATH_EXPORT Plane make_plane(const f32 a, const f32 b, const f32 c, const f32 d);

inline Plane make_plane(const glm::vec3& direction, const f32 offset) {
    return Plane{direction, offset};
}

inline Plane make_plane(const glm::vec3& normal, const glm::vec3& point) {
    Plane p;
    p.direction = normal;
    p.offset = glm::dot(normal, point);
    return p;
}

MODUS_MATH_EXPORT Plane make_plane(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3);

inline f32 distance(const Plane& plane, const glm::vec3& point) {
    return glm::abs(plane.test(point));
}

// ----------------------------------------------------------------------------
// AABB
struct MODUS_MATH_EXPORT AABB {
    glm::vec3 center = glm::vec3(0.0f);
    glm::vec3 extends = glm::vec3(1.0f);

    inline glm::vec3 max() const { return center + extends; }

    inline glm::vec3 min() const { return center - extends; }
};

inline AABB make_aabb(const glm::vec3& max, const glm::vec3 min) {
    AABB aabb;
    aabb.center = (max + min) * 0.5f;
    aabb.extends = glm::abs(max - min) * 0.5f;
    return aabb;
}

inline f32 distance(const AABB& aabb, const glm::vec3& point) {
    glm::vec3 dist(aabb.center - point);
    dist.x = glm::abs(dist.x) - aabb.extends.x;
    dist.y = glm::abs(dist.y) - aabb.extends.y;
    dist.z = glm::abs(dist.z) - aabb.extends.z;
    return std::max(std::min(std::min(dist.x, dist.y), dist.z), 1.0f);
}

// ----------------------------------------------------------------------------
// Sphere
struct MODUS_MATH_EXPORT Sphere {
    glm::vec3 center = glm::vec3(0.0f);
    f32 radius = 1.0f;
};

inline f32 distance(const Sphere& sphere, const glm::vec3& point) {
    return glm::distance(sphere.center, point) - sphere.radius;
}

inline Sphere make_sphere(const AABB& box) {
    Sphere s;
    s.center = box.center;
    s.radius = std::max(std::max(box.extends.x, box.extends.y), box.extends.z);
    return s;
}

// ----------------------------------------------------------------------------
// Contains
MODUS_MATH_EXPORT bool contains(const Sphere& src, const Sphere& dst);
MODUS_MATH_EXPORT bool contains(const Sphere& src, const AABB& dst);
MODUS_MATH_EXPORT bool contains(const Sphere& src, const glm::vec3& dst);

MODUS_MATH_EXPORT bool contains(const AABB& src, const Sphere& dst);
MODUS_MATH_EXPORT bool contains(const AABB& src, const AABB& dst);
MODUS_MATH_EXPORT bool contains(const AABB& src, const glm::vec3& dst);

// ----------------------------------------------------------------------------
// merge
MODUS_MATH_EXPORT void merge(Sphere& src, const Sphere& sphere);
MODUS_MATH_EXPORT void merge(Sphere& src, const glm::vec3& point);

MODUS_MATH_EXPORT void merge(AABB& src, const AABB& aabb);
MODUS_MATH_EXPORT void merge(AABB& src, const glm::vec3& point);

// ----------------------------------------------------------------------------
// Instersection

MODUS_MATH_EXPORT bool intersects(const Sphere& sphere1, const Sphere& sphere2);
MODUS_MATH_EXPORT bool intersects(const Sphere& sphere, const AABB& aabb);
MODUS_MATH_EXPORT bool intersects(const AABB& aabb1, const AABB& aabb2);
inline bool intersects(const AABB& aabb, const Sphere& sphere) {
    return intersects(sphere, aabb);
}
MODUS_MATH_EXPORT bool intersects(const Line& line, const Plane& plane);
MODUS_MATH_EXPORT bool intersects(const Ray& ray, const Plane& plane);

MODUS_MATH_EXPORT Result<f32, void> ray_intersercts(const Ray& ray, const Plane& plane);
MODUS_MATH_EXPORT Result<f32, void> ray_intersercts(const Ray& ray, const AABB& aabb);
MODUS_MATH_EXPORT Result<f32, void> ray_intersercts(const Ray& ray,
                                                    const Sphere& sphere,
                                                    const bool discard_interior = false);

// ----------------------------------------------------------------------------
// Classify
// If the Result is error, it means the primitive is not on the plane but a
// at a given positive (positive side) or negative (negative side) distane.
// If it returns Ok(), then we intersect with the plane.

MODUS_MATH_EXPORT Result<void, f32> classify(const Plane& plane, const AABB& aabb);
MODUS_MATH_EXPORT Result<void, f32> classify(const Plane& plane, const Sphere& sphere);
MODUS_MATH_EXPORT Result<void, f32> classify(const Plane& plane, const glm::vec3& point);

inline Result<void, f32> classify(const AABB& aabb, const Plane& plane) {
    return classify(plane, aabb);
}
inline Result<void, f32> classify(const Sphere& sphere, const Plane& plane) {
    return classify(plane, sphere);
}
// ----------------------------------------------------------------------------
// Transforms

MODUS_MATH_EXPORT Plane transform(const Transform& t, const Plane& plane);
MODUS_MATH_EXPORT Sphere transform(const Transform& t, const Sphere& plane);
MODUS_MATH_EXPORT AABB transform(const Transform& t, const AABB& aabb);

}    // namespace modus::math::bv
