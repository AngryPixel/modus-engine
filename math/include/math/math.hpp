/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// When not using pch, include the pch header here to ensure code completion engines based on
// clangd can find all the required headers.
#if !defined(MODUS_USE_PCH)
#include <math/math_pch.h>
#endif

// clang-format off
#include <core/core.hpp>
#include <math/module_config.hpp>
// clang-format on

// ----------------------------------------------------------------------------
// Float Equal
// ----------------------------------------------------------------------------
namespace modus::math {

static constexpr f32 kF32Epsilon = 1e-10f;    // std::numeric_limits<f32>::epsilon();
static constexpr f32 kPI = 3.1415926535897932384626433832795f;
static constexpr f32 kHalfPI = 1.5707963267948966192313216916398f;
static constexpr f32 kDoulbePI = 6.283185307179586476925286766559f;

/// Template equal function
template <typename T>
static inline bool equal(const T a, const T b) {
    return a == b;
}

/// Template iszero function
template <typename T>
static inline bool is_zero(const T a) {
    return a == T(0);
}

template <typename T>
static inline T squared(const T a) {
    return a * a;
}

/// Specialization of IsZero for floats
template <>
inline bool is_zero<f32>(const f32 a) {
    return fabsf(a) < kF32Epsilon;
}

/// Specialization of Equal for floats
template <>
inline bool equal<f32>(const f32 a, const f32 b) {
    return is_zero<f32>(a - b);
}

template <typename T>
inline bool is_infinitiy(const T a) {
    return a == std::numeric_limits<T>::infinity();
}

template <>
inline bool is_infinitiy<f32>(const f32 a) {
    return equal<f32>(a, std::numeric_limits<f32>::infinity());
}

inline u32 next_pow2(const u32 value) {
    modus_assert(value != 0);
    u32 v = value - 1;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
}

inline bool is_pow2(const u32 num) {
    return ((num != 0) && ((num & (num - 1)) == 0));
}

}    // namespace modus::math
