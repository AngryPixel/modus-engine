/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <math/math.hpp>

// Collection of normalized floating point packing utilities

namespace modus::math {

inline u32 pack_snorm_1010102_rev(const glm::vec4& v) {
    return glm::packSnorm3x10_1x2(v);
}

inline glm::vec4 unpack_snorm_1010102_rev(const u32 v) {
    return glm::unpackSnorm3x10_1x2(v);
}

inline u32 pack_snorm_1616(const glm::vec2& v) {
    return glm::packSnorm2x16(v);
}
inline glm::vec2 unpack_snorm_1616(const u32 v) {
    return glm::unpackSnorm2x16(v);
}

inline u32 pack_unorm_1616(const glm::vec2& v) {
    return glm::packUnorm2x16(v);
}
inline glm::vec2 unpack_unorm_1616(const u32 v) {
    return glm::unpackUnorm2x16(v);
}

inline u32 pack_snorm_8888(const glm::vec4& v) {
    return glm::packSnorm4x8(v);
}

inline u32 pack_unorm_8888(const glm::vec4& v) {
    return glm::packUnorm4x8(v);
}

inline u64 pack_unorm_16161616(const glm::vec4& v) {
    return glm::packUnorm4x16(v);
}

}    // namespace modus::math
