/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <math/math.hpp>

namespace modus::math {

template <class T>
T ease_bounce_out(f32 tick, const T& start, const T& end, const f32 duration) {
    const T diff = end - start;
    if ((tick /= duration) < (1 / 2.75f)) {
        return diff * (7.5625f * tick * tick) + start;
    } else if (tick < (2 / 2.75f)) {
        const f32 postFix = tick -= (1.5f / 2.75f);
        return diff * (7.5625f * (postFix)*tick + .75f) + start;
    } else if (tick < (2.5f / 2.75f)) {
        const f32 postFix = tick -= (2.25f / 2.75f);
        return diff * (7.5625f * (postFix)*tick + .9375f) + start;
    } else {
        const f32 postFix = tick -= (2.625f / 2.75f);
        return diff * (7.5625f * (postFix)*tick + .984375f) + start;
    }
}

template <class T>
T ease_elastic_in(f32 tick, const T& start, const T& end, const f32 duration) {
    if (is_zero(tick)) {
        return start;
    }

    const T diff = end - start;
    if ((tick /= duration) >= 1.0f) {
        return start + diff;
    }

    const f32 p = duration * .3f;
    const f32 s = p / 4.0f;
    const T postFix = diff * powf(2, 10 * (tick -= 1.0f));
    return -(postFix * std::sin((tick * duration - s) * (2.0f * kPI) / p)) + start;
}

template <class T>
T ease_elastic_out(f32 tick, const T& start, const T& end, const f32 duration) {
    if (is_zero(tick)) {
        return start;
    }

    const T diff = end - start;
    if ((tick /= duration) >= 1.0f) {
        return start + diff;
    }

    const f32 p = duration * 0.3f;
    const f32 s = p / 4.0f;
    return (diff * std::pow(2.0f, -10.0f * tick) *
                std::sin((tick * duration - s) * (2.0f * kPI) / p) +
            diff + start);
}

template <class T>
T ease_elastic_in_out(f32 tick, const T& start, const T& end, const f32 duration) {
    if (tick / duration < 0.5f) {
        return ease_elastic_in(tick, start, end, duration);
    } else {
        return ease_elastic_out(tick, start, end, duration);
    }
}

template <class T>
T ease_sin_out(f32 tick, const T& start, const T& diff, const f32 duration) {
    return diff * std::sin(tick / duration * (kPI / 2.0f)) + start;
    // return (t>=d) ? start+diff : diff * (-pow(2.0f, -10.0f* t/d) + 1.0f)
    // + start;
}

template <class T>
T ease_sin_in(f32 tick, const T& start, const T& diff, const f32 duration) {
    return -diff * std::cos(tick / duration * (kPI / 2.0f)) + diff + start;
}

}    // namespace modus::math
