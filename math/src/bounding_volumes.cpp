/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <math/bounding_volumes.hpp>
#include <math/transform.hpp>

namespace modus::math::bv {

Plane make_plane(const f32 a, const f32 b, const f32 c, const f32 d) {
    Plane p;
    // normalize for cheap distance checks
    const f32 lensq = a * a + b * b + c * c;

    // length of normal had better not be zero
    modus_assert_message(!modus::math::is_zero(lensq), "must not be zero");

    // recover gracefully
    if (modus::math::is_zero(lensq)) {
        p.direction = glm::vec3(1.0f, 0.0f, 0.0f);
        p.offset = 0.0f;
    } else {
        const f32 recip = 1.0f / glm::sqrt(lensq);
        p.direction = glm::vec3(a * recip, b * recip, c * recip);
        p.offset = d * recip;
    }
    return p;
}

Plane make_plane(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2) {
    Plane p;
    const glm::vec3 kU = p1 - p0;
    const glm::vec3 kV = p2 - p0;
    const glm::vec3 kW = glm::cross(kU, kV);

    // normalize for cheap distance checks
    const f32 lensq = kW.x * kW.x + kW.y * kW.y + kW.z * kW.z;

    // length of normal had better not be zero
    modus_assert_message(!modus::math::is_zero(lensq), "Must not be zero");

    // recover gracefully
    if (modus::math::is_zero(lensq)) {
        p.direction = glm::vec3(1.0f, 0.0f, 0.0f);
        p.offset = 0.0f;
    } else {
        const float recip = 1.0f / glm::sqrt(lensq);
        p.direction = glm::vec3(kW.x * recip, kW.y * recip, kW.z * recip);
        p.offset = -glm::dot(p0, p.direction);
    }
    return p;
}

// ----------------------------------------------------------------------------
// Contains
bool contains(const Sphere& src, const Sphere& dst) {
    const f32 dist_sqr = glm::length_sqr(src.center - dst.center);
    const f32 radius_sqr = squared(src.radius);
    const f32 other_radius_sqr = squared(dst.radius);
    return (dist_sqr <= radius_sqr) && (dist_sqr + other_radius_sqr <= radius_sqr);
}

bool contains(const Sphere& src, const AABB& dst) {
    const glm::vec3 dist(src.center - dst.center);
    return (glm::abs(dist.x) + dst.extends.x <= src.radius) &&
           (glm::abs(dist.y) + dst.extends.y <= src.radius) &&
           (glm::abs(dist.z) + dst.extends.z <= src.radius);
}

bool contains(const Sphere& src, const glm::vec3& dst) {
    return glm::length_sqr(src.center - dst) <= squared(src.radius);
}

bool contains(const AABB& src, const Sphere& dst) {
    const glm::vec3 dist(src.center - dst.center);
    return (glm::abs(dist.x) + dst.radius <= src.extends.x) &&
           (glm::abs(dist.y) + dst.radius <= src.extends.y) &&
           (glm::abs(dist.z) + dst.radius <= src.extends.z);
}

bool contains(const AABB& src, const AABB& dst) {
    const glm::vec3 dist(src.center - dst.center);
    return (glm::abs(dist.x) + dst.extends.x <= src.extends.x) &&
           (glm::abs(dist.y) + dst.extends.y <= src.extends.y) &&
           (glm::abs(dist.z) + dst.extends.z <= src.extends.z);
}

bool contains(const AABB& src, const glm::vec3& dst) {
    const glm::vec3 dist(src.center - dst);
    return (glm::abs(dist.x) <= src.extends.x) && (glm::abs(dist.y) <= src.extends.y) &&
           (glm::abs(dist.z) <= src.extends.z);
}

// ----------------------------------------------------------------------------
// Merge
void merge(Sphere& src, const Sphere& sphere) {
    const glm::vec3 diff = sphere.center - src.center;
    float length_sqr = glm::length_sqr(diff);
    float radius_diff = sphere.radius - src.radius;

    // Early-out
    if (squared(radius_diff) >= length_sqr) {
        // One fully contains the other
        if (radius_diff <= 0.0f) {
            return;    // no change
        } else {
            src.center = sphere.center;
            src.radius = sphere.radius;
            return;
        }
    }

    const f32 length = glm::sqrt(length_sqr);
    const f32 t = (length + radius_diff) / (2.0f * length);
    src.center = src.center + (diff * t);
    src.radius = 0.5f * (length + src.radius + sphere.radius);
}

void merge(Sphere& src, const glm::vec3& point) {
    const glm::vec3 diff = glm::abs(point - src.center);
    float radius_sqrd = squared(src.radius);
    float distance_sqrd = glm::length_sqr(diff);
    if (radius_sqrd < distance_sqrd) {
        src.radius = glm::sqrt(distance_sqrd);
    }
}

void merge(AABB& src, const AABB& aabb) {
    const glm::vec3 min = src.min();
    const glm::vec3 max = src.max();
    const glm::vec3 other_max = aabb.max();
    const glm::vec3 other_min = aabb.min();

    glm::vec3 new_max, new_min;

    new_max.x = std::max(max.x, other_max.x);
    new_max.y = std::max(max.y, other_max.y);
    new_max.z = std::max(max.z, other_max.z);

    new_min.x = std::min(min.x, other_min.x);
    new_min.y = std::min(min.y, other_min.y);
    new_min.z = std::min(max.z, other_min.z);

    if (!is_infinitiy(new_max.x) && !is_infinitiy(new_max.y) && !is_infinitiy(new_max.z)) {
        src.center = (new_max + new_min) * 0.5f;
        src.extends = (new_max - new_min) * 0.5f;
        return;
    }
    modus_assert_message(false, "inifinity detected");
}
void merge(AABB& src, const glm::vec3& point) {
    const glm::vec3 min = src.min();
    const glm::vec3 max = src.max();

    glm::vec3 new_max, new_min;
    new_max.x = std::max(max.x, point.x);
    new_max.y = std::max(max.y, point.y);
    new_max.z = std::max(max.z, point.z);

    new_min.x = std::min(min.x, point.x);
    new_min.y = std::min(min.y, point.y);
    new_min.z = std::min(max.z, point.z);

    if (!is_infinitiy(new_max.x) && !is_infinitiy(new_max.y) && !is_infinitiy(new_max.z)) {
        src.center = (new_max + new_min) * 0.5f;
        src.extends = (new_max - new_min) * 0.5f;
        return;
    }
    modus_assert_message(false, "inifinity detected");
}

// ----------------------------------------------------------------------------
// Intersects

bool intersects(const Sphere& sphere1, const Sphere& sphere2) {
    const glm::vec3 center_diff = sphere1.center - sphere2.center;
    const f32 radius_sum = sphere1.radius + sphere2.radius;
    return (glm::dot(center_diff, center_diff) <= squared(radius_sum));
}

bool intersects(const Sphere& sphere, const AABB& aabb) {
    modus_assert_message(false, "Needs revisiting, might not be accurate!!");
    // Use splitting planes
    const glm::vec3& center = sphere.center;
    const f32 radius = sphere.radius;
    const glm::vec3& min = aabb.min();
    const glm::vec3& max = aabb.max();

    // Arvo's algorithm
    f32 s, d = 0;
    for (int i = 0; i < 3; ++i) {
        if (center[i] < min[i]) {
            s = center[i] - min[i];
            d += s * s;
        } else if (center[i] > max[i]) {
            s = center[i] - max[i];
            d += s * s;
        }
    }
    return d <= radius * radius;
}

bool intersects(const AABB& aabb1, const AABB& aabb2) {
    const glm::vec3 dist(aabb1.center - aabb2.center);
    const glm::vec3 sumHalfSizes(aabb1.extends + aabb2.extends);
    return (glm::abs(dist.x) <= sumHalfSizes.x) && (glm::abs(dist.y) <= sumHalfSizes.y) &&
           (glm::abs(dist.z) <= sumHalfSizes.z);
}

bool intersects(const Line& line, const Plane& plane) {
    const glm::vec3 ab = line.to - line.from;
    const f32 denom = glm::dot(plane.direction, ab);
    if (is_zero(denom)) {
        return false;
    }
    const f32 t = (plane.offset - glm::dot(plane.direction, line.from)) / denom;

    if (t >= 0 && t <= 1.0f) {
        return true;
    }
    return false;
}

bool intersects(const Ray& ray, const Plane& plane) {
    auto r = ray_intersercts(ray, plane);
    if (!r) {
        return false;
    }
    return (*r <= ray.range);
}

Result<void, f32> classify(const Plane& plane, const glm::vec3& point) {
    const f32 d = plane.test(point);
    if (d < 0.0f || d > 0.0f) {
        return Error(d);
    }
    return Ok<>();
}

Result<void, f32> classify(const Plane& plane, const AABB& aabb) {
    modus_assert_message(false, "Needs revisiting, might not be accurate!!");
    const glm::vec3 max = aabb.max();
    const glm::vec3 min = aabb.min();
    glm::vec3 diag_max, diag_min;
    // set min/max values for x direction
    if (plane.direction.x >= 0.0f) {
        diag_min.x = min.x;
        diag_max.x = max.x;
    } else {
        diag_min.x = max.x;
        diag_max.x = min.x;
    }

    // set min/max values for y direction
    if (plane.direction.y >= 0.0f) {
        diag_min.y = min.y;
        diag_max.y = max.y;
    } else {
        diag_min.y = max.y;
        diag_max.y = min.y;
    }

    // set min/max values for z direction
    if (plane.direction.z >= 0.0f) {
        diag_min.z = min.z;
        diag_max.z = max.z;
    } else {
        diag_min.z = max.z;
        diag_max.z = min.z;
    }

    // minimum on positive side of plane, box on positive side
    float test = plane.test(diag_min);
    if (test > 0.0f) {
        return Error(test);
    }

    test = plane.test(diag_max);
    // min on non-positive side, max on non-negative side, intersection
    if (test >= 0.0f) {
        return Ok<>();
    }
    // max on negative side, box on negative side
    return Error(test);
}

Result<void, f32> classify(const Plane& plane, const Sphere& sphere) {
    const f32 distance = plane.test(sphere.center);
    const f32 dist_abs = glm::abs(distance);

    // If distance <= radius then the sphere is currently intersecting
    // the plane
    if (dist_abs <= sphere.radius) {
        return Ok<>();
    }

    if (distance > sphere.radius) {
        return Error(distance - sphere.radius);
    }
    // else (distance < sphere.radius) {
    return Error(distance + sphere.radius);
}

Result<f32, void> ray_intersercts(const Ray& ray, const Plane& plane) {
    const f32 denom = glm::dot(plane.direction, ray.direction);
    if (glm::abs(denom) < kF32Epsilon) {
        // Parallel
        return Error<>();
    }

    const f32 nom = glm::dot(plane.direction, ray.origin) + plane.offset;
    const f32 t = -(nom / denom);

    if (t >= 0.0f) {
        return Ok(t);
    }
    return Error<>();
}

Result<f32, void> ray_intersercts(const Ray& ray, const AABB& aabb) {
    f32 lowt = 0.0f;
    f32 t;
    bool hit = false;
    glm::vec3 hitpoint;
    const glm::vec3 min = aabb.min();
    const glm::vec3 max = aabb.max();
    const glm::vec3& rayorig = ray.origin;
    const glm::vec3& raydir = ray.direction;

    // Check origin inside first
    if (glm::all(glm::greaterThan(rayorig, min)) && glm::all(glm::lessThan(rayorig, max))) {
        return Ok(0.0f);
    }

    // Check each face in turn, only check closest 3
    // Min x
    if (rayorig.x <= min.x && raydir.x > 0) {
        t = (min.x - rayorig.x) / raydir.x;

        // Substitute t back into ray and check bounds and dist
        hitpoint = rayorig + raydir * t;
        if (hitpoint.y >= min.y && hitpoint.y <= max.y && hitpoint.z >= min.z &&
            hitpoint.z <= max.z && (!hit || t < lowt)) {
            hit = true;
            lowt = t;
        }
    }
    // Max x
    if (rayorig.x >= max.x && raydir.x < 0) {
        t = (max.x - rayorig.x) / raydir.x;

        // Substitute t back into ray and check bounds and dist
        hitpoint = rayorig + raydir * t;
        if (hitpoint.y >= min.y && hitpoint.y <= max.y && hitpoint.z >= min.z &&
            hitpoint.z <= max.z && (!hit || t < lowt)) {
            hit = true;
            lowt = t;
        }
    }
    // Min y
    if (rayorig.y <= min.y && raydir.y > 0) {
        t = (min.y - rayorig.y) / raydir.y;

        // Substitute t back into ray and check bounds and dist
        hitpoint = rayorig + raydir * t;
        if (hitpoint.x >= min.x && hitpoint.x <= max.x && hitpoint.z >= min.z &&
            hitpoint.z <= max.z && (!hit || t < lowt)) {
            hit = true;
            lowt = t;
        }
    }
    // Max y
    if (rayorig.y >= max.y && raydir.y < 0) {
        t = (max.y - rayorig.y) / raydir.y;

        // Substitute t back into ray and check bounds and dist
        hitpoint = rayorig + raydir * t;
        if (hitpoint.x >= min.x && hitpoint.x <= max.x && hitpoint.z >= min.z &&
            hitpoint.z <= max.z && (!hit || t < lowt)) {
            hit = true;
            lowt = t;
        }
    }
    // Min z
    if (rayorig.z <= min.z && raydir.z > 0) {
        t = (min.z - rayorig.z) / raydir.z;

        // Substitute t back into ray and check bounds and dist
        hitpoint = rayorig + raydir * t;
        if (hitpoint.x >= min.x && hitpoint.x <= max.x && hitpoint.y >= min.y &&
            hitpoint.y <= max.y && (!hit || t < lowt)) {
            hit = true;
            lowt = t;
        }
    }
    // Max z
    if (rayorig.z >= max.z && raydir.z < 0) {
        t = (max.z - rayorig.z) / raydir.z;

        // Substitute t back into ray and check bounds and dist
        hitpoint = rayorig + raydir * t;
        if (hitpoint.x >= min.x && hitpoint.x <= max.x && hitpoint.y >= min.y &&
            hitpoint.y <= max.y && (!hit || t < lowt)) {
            hit = true;
            lowt = t;
        }
    }
    if (!hit) {
        return Error<>();
    }

    return Ok(lowt);
}

Result<f32, void> ray_intersercts(const Ray& ray,
                                  const Sphere& sphere,
                                  const bool discard_interior) {
    const glm::vec3& raydir = ray.direction;
    // Adjust ray origin relative to sphere center
    const glm::vec3 rayorig = ray.origin - sphere.center;
    const float radius = sphere.radius;

    // Check origin inside first
    if (glm::length_sqr(rayorig) <= radius * radius && discard_interior) {
        return Ok(0.0f);
    }

    // Mmm, quadratics
    // Build coeffs which can be used with std quadratic solver
    // ie t = (-b +/- sqrt(b*b + 4ac)) / 2a
    float a = glm::dot(raydir, raydir);
    float b = 2 * glm::dot(rayorig, raydir);
    float c = glm::dot(rayorig, rayorig) - radius * radius;

    // Calc determinant
    float d = (b * b) - (4 * a * c);
    if (d < 0) {
        // No intersection
        return Error<>();
    }
    // BTW, if d=0 there is one intersection, if d > 0 there are 2
    // But we only want the closest one, so that's ok, just use the
    // '-' version of the solver
    f32 t = (-b - glm::sqrt(d)) / (2.0f * a);
    if (t < 0.0f) {
        t = (-b + glm::sqrt(d)) / (2.0f * a);
    }
    return Ok(t);
}

// ----------------------------------------------------------------------------
// Transform
Plane transform(const Transform& t, const Plane& plane) {
    glm::vec3 point = plane.center();
    glm::vec3 point_dir = point + plane.direction;
    point = t.transform(point);
    point_dir = t.transform(point_dir);

    Plane p;
    p.direction = point_dir - point;
    p.direction = glm::normalize(p.direction);
    p.offset = glm::dot(p.direction, point);
    return p;
}

Sphere transform(const Transform& t, const Sphere& sphere) {
    Sphere s;
    s.radius = sphere.radius * t.scale();
    s.center = t.transform(sphere.center);
    return s;
}

AABB transform(const Transform& t, const AABB& aabb) {
    // TODO: Improve
    const glm::mat4 mat = t.to_matrix();

    /* http://dev.theomader.com/transform-bounding-boxes/ */
    const glm::vec3 min = aabb.min();
    const glm::vec3 max = aabb.max();
    glm::vec3 tmin, tmax;
    for (int i = 0; i < 3; i++) {
        tmin[i] = tmax[i] = t.m_position[i];
        for (int j = 0; j < 3; j++) {
            f32 e = mat[i][j] * min[j];
            f32 f = mat[i][j] * max[j];
            if (e < f) {
                tmin[i] += e;
                tmax[i] += f;
            } else {
                tmin[i] += f;
                tmax[i] += e;
            }
        }
    }
    return make_aabb(max, min);
}

}    // namespace modus::math::bv
