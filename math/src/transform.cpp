﻿/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <math/transform.hpp>

namespace modus::math {

const Transform Transform::kIdentity =
    Transform{glm::quat(1.0, 0.f, 0.f, 0.f), glm::vec3(0.f), 1.0f};

void Transform::translate(const glm::vec3& translation) {
    m_position += translation;
}

void Transform::translate(const f32 x, const f32 y, const f32 z) {
    m_position += glm::vec3(x, y, z);
}

void Transform::set_translation(const glm::vec3& translation) {
    m_position = translation;
}

void Transform::set_translation(const f32 x, const f32 y, const f32 z) {
    m_position.x = x;
    m_position.y = y;
    m_position.z = z;
}

Transform Transform::translated(const glm::vec3& translation) const {
    Transform t = *this;
    t.translate(translation);
    return t;
}

Transform Transform::translated(const f32 x, const f32 y, const f32 z) const {
    Transform t = *this;
    t.translate(x, y, z);
    return t;
}

void Transform::rotate(const glm::quat& rotation) {
    m_rotation *= rotation;
}

void Transform::rotate_prefix(const glm::quat& rotation) {
    m_rotation = rotation * m_rotation;
}

void Transform::set_rotation(const glm::quat& rotation) {
    m_rotation = rotation;
}

Transform Transform::rotated(const glm::quat& rotation) const {
    Transform t = *this;
    t.rotate(rotation);
    return t;
}

Transform Transform::rotated_prefix(const glm::quat& rotation) const {
    Transform t = *this;
    t.rotate_prefix(rotation);
    return t;
}

void Transform::scale(const f32 scale) {
    m_scale *= scale;
}

void Transform::set_scale(const f32 scale) {
    m_scale = scale;
}

Transform Transform::scaled(const f32 scale) const {
    Transform t = *this;
    t.scale(scale);
    return t;
}

void Transform::invert() {
    m_rotation = glm::inverse(m_rotation);
    m_position = m_rotation * -m_position;
    m_scale = 1.0f / m_scale;
}

Transform Transform::inverted() const {
    Transform t = *this;
    t.invert();
    return t;
}

void Transform::set_transform(const Transform& other) {
    m_scale = other.m_scale;
    m_rotation = other.m_rotation;
    m_position = other.m_position;
}

void Transform::transform(const Transform& other) {
    m_scale *= other.m_scale;
    m_position += (m_rotation * other.m_position);
    m_rotation *= other.m_rotation;
}

Transform Transform::transformed(const Transform& other) const {
    Transform t = *this;
    t.transform(other);
    return t;
}

glm::vec3 Transform::up() const {
    return glm::normalize(m_rotation * glm::vec3(0.0f, 1.0f, 0.0f));
}

glm::vec3 Transform::forward() const {
    return glm::normalize(m_rotation * glm::vec3(0.0f, 0.0f, -1.0f));
}

glm::vec3 Transform::right() const {
    return glm::normalize(m_rotation * glm::vec3(1.0f, 0.0f, 0.0f));
}

void Transform::set_look_at(const glm::vec3& eye, const glm::vec3& target, const glm::vec3& up) {
    // From glm source code
    glm::vec3 f, u, s;
    f = glm::normalize(target - eye);
    s = glm::normalize(glm::cross(f, up));
    u = glm::cross(s, f);

    glm::mat3 mat3;

    mat3[0][0] = s.x;
    mat3[1][0] = s.y;
    mat3[2][0] = s.z;

    mat3[0][1] = u.x;
    mat3[1][1] = u.y;
    mat3[2][1] = u.z;

    mat3[0][2] = -f.x;
    mat3[1][2] = -f.y;
    mat3[2][2] = -f.z;

    m_rotation = glm::quat_cast(mat3);
    m_scale = 1.0f;
    m_position.x = -glm::dot(s, eye);
    m_position.y = -glm::dot(u, eye);
    m_position.z = glm::dot(f, eye);
}

glm::mat4 Transform::to_matrix() const {
    /*
    glm::mat4 result = glm::mat4_cast(m_rotation);
    result = glm::translate(result, m_position);
    result = glm::scale(result, m_scale); */
    glm::mat4 result = glm::mat4_cast(m_rotation);
    result[0][0] *= m_scale;
    result[1][0] *= m_scale;
    result[2][0] *= m_scale;
    result[0][1] *= m_scale;
    result[1][1] *= m_scale;
    result[2][1] *= m_scale;
    result[0][2] *= m_scale;
    result[1][2] *= m_scale;
    result[2][2] *= m_scale;
    result[3][0] = m_position.x;
    result[3][1] = m_position.y;
    result[3][2] = m_position.z;
    result[3][3] = 1.0f;

    return result;
}

void Transform::reset() {
    m_rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
    m_scale = 1.0f;
    m_position = glm::vec3(0.0f);
}

glm::vec3 Transform::transform(const glm::vec3& point) const {
    glm::vec3 result = point * m_scale;
    result = m_position + (m_rotation * result);
    return result;
}

Transform Transform::lerp(const Transform& t1, const Transform& t2, const f32 progression) {
    Transform t;
    auto fn_lerp = [](const glm::vec3& v1, const glm::vec3& v2, const f32 step) -> glm::vec3 {
        return v1 + (v2 - v1) * step;
    };
    auto fn_lerpf = [](const f32 v1, const f32 v2, const f32 step) -> f32 {
        return v1 + (v2 - v1) * step;
    };
    t.set_translation(fn_lerp(t1.translation(), t2.translation(), progression));
    t.set_rotation(glm::slerp(t1.rotation(), t2.rotation(), progression));
    t.set_scale(fn_lerpf(t1.scale(), t2.scale(), progression));
    return t;
}
void Transform::set_identity() {
    m_position = glm::vec3(0.f);
    m_scale = 1.0f;
    m_rotation = glm::quat(1.0f, 0.f, 0.f, 0.f);
}

}    // namespace modus::math
