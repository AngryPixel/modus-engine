/*
 * Copyright 2019-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <pch.h>

#include <math/frustum.hpp>

namespace modus::math {

void FrustumPlanes::extract(const glm::mat4& matrix) {
    // Taken from:
    // Fast Extraction of Viewing Frustum Planes from the World-
    // View-Projection Matrix

    // Planes are positive facing outward. Anything that lies on the inside
    // has a negative distance!
    f32 a = 0.0f, b = 0.0f, c = 0.0f, d = 0.0f;

    // Near
    a = matrix[0][3] + matrix[0][2];
    b = matrix[1][3] + matrix[1][2];
    c = matrix[2][3] + matrix[2][2];
    d = matrix[3][3] + matrix[3][2];
    planes[kNear] = bv::make_plane(-a, -b, -c, -d);

    // Far
    a = matrix[0][3] - matrix[0][2];
    b = matrix[1][3] - matrix[1][2];
    c = matrix[2][3] - matrix[2][2];
    d = matrix[3][3] - matrix[3][2];
    planes[kFar] = bv::make_plane(-a, -b, -c, -d);

    // Left
    a = matrix[0][3] + matrix[0][0];
    b = matrix[1][3] + matrix[1][0];
    c = matrix[2][3] + matrix[2][0];
    d = matrix[3][3] + matrix[3][0];
    planes[kLeft] = bv::make_plane(-a, -b, -c, -d);

    // Right
    a = matrix[0][3] - matrix[0][0];
    b = matrix[1][3] - matrix[1][0];
    c = matrix[2][3] - matrix[2][0];
    d = matrix[3][3] - matrix[3][0];
    planes[kRight] = bv::make_plane(-a, -b, -c, -d);

    // Top
    a = matrix[0][3] - matrix[0][1];
    b = matrix[1][3] - matrix[1][1];
    c = matrix[2][3] - matrix[2][1];
    d = matrix[3][3] - matrix[3][1];
    planes[kTop] = bv::make_plane(-a, -b, -c, -d);

    // Bottom
    a = matrix[0][3] + matrix[0][1];
    b = matrix[1][3] + matrix[1][1];
    c = matrix[2][3] + matrix[2][1];
    d = matrix[3][3] + matrix[3][1];
    planes[kBottom] = bv::make_plane(-a, -b, -c, -d);
}

bool FrustumPlanes::is_visible(const glm::vec3& point) const {
    for (auto& plane : planes) {
        auto result = bv::classify(plane, point);
        if (!result && result.error() > 0.0f) {
            return false;
        }
    }
    return true;
}

bool FrustumPlanes::is_visible(const bv::Sphere& sphere) const {
    for (auto& plane : planes) {
        auto result = bv::classify(plane, sphere);
        if (!result && result.error() > 0.0f) {
            return false;
        }
    }
    return true;
}

bool FrustumPlanes::is_visible(const bv::AABB& aabb) const {
    for (auto& plane : planes) {
        auto result = bv::classify(plane, aabb);
        if (!result && result.error() > 0.0f) {
            return false;
        }
    }
    return true;
}

bool FrustumPlanes::is_visible(const bv::Line& line) const {
    for (auto& plane : planes) {
        if (!bv::intersects(line, plane)) {
            auto r_from = bv::classify(plane, line.from);
            auto r_to = bv::classify(plane, line.to);
            if (!r_to && r_to.error() > 0.f && !r_from && r_from.error() > 0.f) {
                return false;
            }
        }
    }
    return true;
}

void extract_frustum_points(FrustumPoints& points, const glm::mat4& vp) {
    const glm::vec3 frustum_points[8] = {
        {1.0f, 1.0f, 1.0f},       // far top right
        {1.0f, -1.0f, 1.0f},      // far bottom right
        {-1.0f, -1.0f, 1.0f},     // far bottom left
        {-1.0f, 1.0f, 1.0f},      // far top left
        {1.0f, 1.0f, -1.0f},      // near top right
        {1.0f, -1.0f, -1.0f},     // near bottom right
        {-1.0f, -1.0f, -1.0f},    // near bottom left
        {-1.0f, 1.0f, -1.0f},     // near top left
    };

    const glm::mat4 inv = glm::inverse(vp);
    for (usize i = 0; i < points.size(); ++i) {
        glm::vec4 world = inv * glm::vec4(frustum_points[i], 1.0f);
        world *= 1.0f / world.w;
        points[i] = glm::vec3(world);
    }
}

}    // namespace modus::math
