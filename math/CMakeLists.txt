#
# Modus Math Module
#
# - Vector
# - Matrices
# - View Perspsective
# - Transform
# - ...
#

modus_add_module(math
    VERSION_MAJOR 0
    VERSION_MINOR 3
    VERSION_PATCH 0
)

target_sources(math PRIVATE
    include/math/math_pch.h
    include/math/transform.hpp
    include/math/bounding_volumes.hpp
    include/math/frustum.hpp
    include/math/easing_functions.hpp
    include/math/packing.hpp
    include/math/math.hpp
    include/math/billboard.hpp

    src/transform.cpp
    src/bounding_volumes.cpp
    src/frustum.cpp
)

target_link_libraries(math
    PUBLIC core modus_glm
)
