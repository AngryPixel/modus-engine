
/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <audio/codecs/wave_codec.hpp>

namespace modus::audio {

struct alignas(4) WaveHeader {
    char riff_id[4];      // 'RIFF'
    u32 size;             // make sure to read only 4 bits
    char riff_type[4];    // 'WAVE'
    char fmt_id[4];       // 'fmt'
    u32 fmt_size;
    u16 format_tag;
    u16 n_channels;
    u32 samples_per_sec;
    u32 n_avg_bytes_per_sec;
    u16 n_block_alingn;
    u16 n_bits_per_sample;
};

/* header of wave file */
struct alignas(4) ChunckHeader {
    char id[4];    // 'data' or 'fact'
    u32 size;
};

Result<> WaveCodec::initialize(ISeekableReader& reader) {
    m_data_offset = 0;
    m_data_size = 0;
    m_frequency = 0;
    m_bytes_read = 0;
    m_format = AudioFormat::Mono16;

    static constexpr const char* kRiffId = "RIFF";
    static constexpr const char* kWaveId = "WAVE";
    static constexpr const char* kFmtId = "fmt ";

    WaveHeader header;
    if (!reader.read_exactly(SliceMut(&header, 1).as_bytes())) {
        MODUS_LOGE("Failed to read WAVE header");
        return Error<>();
    }

    if (StringSlice(header.riff_id, 4) != kRiffId) {
        MODUS_LOGE("File is not a valid RIFF file");
        return Error<>();
    }

    if (StringSlice(header.riff_type, 4) != kWaveId) {
        MODUS_LOGE("File is not a valid WAVE file");
        return Error<>();
    }

    if (StringSlice(header.fmt_id, 4) != kFmtId) {
        MODUS_LOGE("File does not have a valid fmt id");
        return Error<>();
    }

    if (header.format_tag != 1) {
        MODUS_LOGE("WaveFile has invalid format tag {}, expected 1", header.format_tag);
        return Error<>();
    }

    if (header.n_bits_per_sample != 16 && header.n_bits_per_sample != 8) {
        MODUS_LOGE(
            "WaveFile has neither 8 or 16 bits per sample, but {} bits per "
            "sample",
            header.n_bits_per_sample);
        ;
        return Error<>();
    }

    if (header.n_channels > 2) {
        MODUS_LOGE("WaveFile has more than 2 channels");
        return Error<>();
    }

    ChunckHeader data_header;

    constexpr u32 kMaxTries = 20;
    static constexpr const char* kDataHeaderId = "data";

    for (u32 i = 0; i < kMaxTries; ++i) {
        if (!reader.read_exactly(SliceMut(&data_header, 1).as_bytes())) {
            MODUS_LOGE("Failed to read data chunk header");
            return Error<>();
        }

        if (StringSlice(data_header.id, 4) != kDataHeaderId) {
            static constexpr u32 kSkipBufferSize = 256;
            u8 skip_buffer[kSkipBufferSize];

            u32 size_left_to_skip = data_header.size;
            while (size_left_to_skip != 0) {
                const u32 size_to_skip = std::min(kSkipBufferSize, size_left_to_skip);
                if (!reader.read_exactly(SliceMut(skip_buffer, size_to_skip))) {
                    MODUS_LOGE("Failed to skip Wave chunk");
                    return Error<>();
                }
                modus_assert(size_to_skip <= size_left_to_skip);
                size_left_to_skip -= size_to_skip;
            }
            continue;
        }

        auto r_offset = reader.position();
        if (!r_offset) {
            MODUS_LOGE("WaveCodec: Failed to read stream position");
            return Error<>();
        }
        m_data_offset = *r_offset;
        m_data_size = data_header.size;
        break;
    }

    if (m_data_size == 0) {
        MODUS_LOGE("Failed to locate Wave data chunck");
        return Error<>();
    }

    if (header.n_channels == 1) {
        if (header.n_bits_per_sample == 8) {
            m_format = AudioFormat::Mono8;
        } else {
            m_format = AudioFormat::Mono16;
        }
    } else if (header.n_channels == 2) {
        if (header.n_bits_per_sample == 8) {
            m_format = AudioFormat::Stereo8;
        } else {
            m_format = AudioFormat::Stereo16;
        }
    }
    m_frequency = header.samples_per_sec;
    return Ok<>();
}

IOResult<usize> WaveCodec::decode(ISeekableReader& reader, ByteSliceMut slice) {
    const usize bytes_to_read = std::min(usize(m_data_size - m_bytes_read), slice.size());
    if (bytes_to_read == 0) {
        return Ok<usize>(0);
    }
    ByteSliceMut writeable_range = slice.sub_slice(bytes_to_read);
    auto r = reader.read(writeable_range);
    if (r) {
        m_bytes_read += u32(*r);
    }
    return r;
}

IOResult<void> WaveCodec::rewind(ISeekableReader& reader) {
    auto r_rewind = reader.seek(m_data_offset);
    if (!r_rewind) {
        return Error(r_rewind.error());
    }
    m_bytes_read = 0;
    return Ok<>();
}

u32 WaveCodec::data_size() const {
    return m_data_size;
}

i32 WaveCodec::audio_frequency() const {
    return m_frequency;
}

AudioFormat WaveCodec::audio_format() const {
    return m_format;
}

}    // namespace modus::audio
