
/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on
#include <audio/codecs/oggvorbis_codec.hpp>

#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

namespace modus::audio {

static usize ogg_read(void* dest, usize s1, usize s2, void* state) {
    ISeekableReader* reader = reinterpret_cast<ISeekableReader*>(state);
    ByteSliceMut slice(reinterpret_cast<u8*>(dest), s1 * s2);
    auto r_read = reader->read(slice);
    if (!r_read) {
        return 0;
    }
    return *r_read;
}

static int ogg_seek(void* state, i64 offset, int type) {
    ISeekableReader* reader = reinterpret_cast<ISeekableReader*>(state);
    if (type == SEEK_CUR) {
        auto r_pos = reader->position();
        if (!r_pos) {
            return -1;
        }
        return reader->seek((*r_pos) + offset) ? 0 : -1;
    } else if (type == SEEK_SET) {
        return reader->seek(offset) ? 0 : -1;
    } else if (type == SEEK_END) {
        if (offset != 0) {
            modus_panic("ogg/vorbis seek end with offset not available");
        }
        return reader->seek_end() ? 0 : -1;
    } else {
        modus_panic("Unknown seek operation for ogg/vorbis file");
    }

    return -1;
}

static long ogg_tell(void* state) {
    ISeekableReader* reader = reinterpret_cast<ISeekableReader*>(state);
    auto r_pos = reader->position();
    return r_pos ? static_cast<long>(*r_pos) : -1;
}

OggVorbisCodec::OggVorbisCodec() {}

OggVorbisCodec::~OggVorbisCodec() {
    if (m_file) {
        ov_clear(m_file.get());
    }
}

Result<> OggVorbisCodec::initialize(ISeekableReader& reader) {
    m_data_size = 0;
    m_frequency = 0;
    m_bytes_read = 0;
    m_format = AudioFormat::Mono16;
    m_vorbis_section = 0;

    auto file = modus::make_unique<OggVorbis_File>();

    ov_callbacks callbacks;
    callbacks.read_func = ogg_read;
    callbacks.seek_func = ogg_seek;
    callbacks.tell_func = ogg_tell;
    callbacks.close_func = nullptr;

    const int ov_result = ov_open_callbacks(&reader, file.get(), nullptr, 0, callbacks);
    if (ov_result != 0) {
        switch (ov_result) {
            case OV_EREAD:
                MODUS_LOGE("OggVorbisCodec: Error reading the stream");
                break;
            case OV_ENOTVORBIS:
                MODUS_LOGE("OggVorbisCodec: File is not ogg/vorbis file");
                break;
            case OV_EVERSION:
                MODUS_LOGE("OggVorbisCodec: File version mismatch");
                break;
            case OV_EBADHEADER:
                MODUS_LOGE("OggVorbisCodec: Bad ogg/vorbis header");
                break;
            case OV_EFAULT:
                MODUS_LOGE("OggVorbisCodec: Ogg/Vorbis internal codec error");
                break;
            default:
                MODUS_LOGE("OggVorbisCodec: Unknown error code {}", ov_result);
                break;
        }
        return Error<>();
    }

    const vorbis_info* info = ov_info(file.get(), -1);
    if (info == nullptr) {
        MODUS_LOGE("OggVorbisCodec: Failed to retrieve vorbis info");
        return Error<>();
    }

    if (info->channels > 2) {
        MODUS_LOGE("OggVorbisCodec: More than 2 channels currently not supported");
        return Error<>();
    }

    if (info->channels == 2) {
        m_format = AudioFormat::Stereo16;
    } else {
        m_format = AudioFormat::Mono16;
    }

    m_frequency = info->rate;
    const i64 ov_total = ov_pcm_total(file.get(), -1);
    if (ov_total == OV_EINVAL) {
        MODUS_LOGE("OggVorbisCodec: Failed to retrieve vorbis pcm size");
        return Error<>();
    }
    m_data_size = ov_total * info->channels * 2;

    m_file = std::move(file);
    return Ok<>();
}

IOResult<usize> OggVorbisCodec::decode(ISeekableReader&, ByteSliceMut slice) {
    i32 total_read = static_cast<i32>(slice.size());
    char* output = reinterpret_cast<char*>(slice.data());
    usize total_bytes_read = 0;
    do {
        const i32 bytes_to_read = std::min(total_read, 4096);
        const i32 result = ov_read(m_file.get(), output, bytes_to_read, 0, 2, 1, &m_vorbis_section);
        if (result == 0) {
            break;
        }

        if (result < 0) {
            return Error(IOError::Unknown);
        }

        total_bytes_read += result;
        output += result;
        total_read -= result;
    } while (total_read > 0);
    return Ok(total_bytes_read);
}

IOResult<void> OggVorbisCodec::rewind(ISeekableReader&) {
    const int result = ov_pcm_seek(m_file.get(), 0);
    if (result < 0) {
        return Error(IOError::Unknown);
    }
    return Ok<>();
}

u32 OggVorbisCodec::data_size() const {
    return m_data_size;
}

i32 OggVorbisCodec::audio_frequency() const {
    return m_frequency;
}

AudioFormat OggVorbisCodec::audio_format() const {
    return m_format;
}

}    // namespace modus::audio
