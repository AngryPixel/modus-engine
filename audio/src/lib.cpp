/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <audio/audio_instance.hpp>

#if defined(MODUS_AUDIO_USE_OPENAL)
#include <oal/oal_instance.hpp>
#endif

namespace modus::audio {

std::unique_ptr<AudioInstance> create_default_audio_instance() {
#if defined(MODUS_AUDIO_USE_OPENAL)
    return modus::make_unique<oal::OALInstance>();
#else
#error "No audio instance available"
#endif
}

}    // namespace modus::audio
