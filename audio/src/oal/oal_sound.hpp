
/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/codecs/codec.hpp>
#include <core/fixed_handle_pool.hpp>
#include <core/fixed_stack_string.hpp>
#include <core/fixed_vector.hpp>

namespace modus::audio {
struct SoundCreateParams;
}

namespace modus::audio::oal {

class OALAudioSource;
class OALSound {
    friend class OALSoundDB;
    static constexpr const u32 kStreamBufferCount = 4;
    static constexpr const u32 kStreamBufferSize = 1 << 16;
    static constexpr const u32 kNameSize = 64;

   private:
    SoundHandle m_handle;
    FixedStackString<kNameSize> m_name;
    FixedVector<ALuint, kStreamBufferCount> m_buffers;
    std::unique_ptr<ICodec> m_codec;
    std::unique_ptr<ISeekableReader> m_stream;
    u32 m_attachment_count = 0;
    UserId m_user_id;
    bool m_is_stream = false;

   public:
    OALSound() = default;

    ~OALSound();

    MODUS_CLASS_DISABLE_COPY(OALSound);

    OALSound(OALSound&& rhs) noexcept;

    OALSound& operator=(OALSound&& rhs) noexcept;

    Result<> initialize(SoundCreateParams& params);

    Result<> attach(OALAudioSource& source);

    void detach(OALAudioSource& source);

    Result<> update(OALAudioSource& source);

    Result<> rewind(OALAudioSource& source);

    bool is_stream() const { return m_is_stream; }

    UserId user_id() const { return m_user_id; }

    SoundHandle handle() const { return m_handle; }

   private:
    Result<usize, void> fill_buffer(ALuint al_buffer, const bool repeat);
};

class OALSoundDB {
   private:
    static constexpr u32 kMaxSoundItems = 64;
    using PoolType = FixedHandlePool<OALSound, kMaxSoundItems, SoundHandle>;
    PoolType m_sounds;
    using MapType = HashMapWithAllocator<UserId, SoundHandle, AudioAllocator>;
    MapType m_id_to_sound;

   public:
    OALSoundDB() = default;

    ~OALSoundDB() = default;

    Result<SoundHandle, void> create(SoundCreateParams& params);

    Result<> destroy(const SoundHandle handle);

    Result<NotMyPtr<OALSound>, void> get(const SoundHandle handle);

    Result<NotMyPtr<OALSound>, void> get(const UserId id);

    Result<SoundHandle, void> handle_from_user_id(const UserId id) const;
};

}    // namespace modus::audio::oal
