/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <oal/oal_api.hpp>
// clang-format on

#include <oal/oal_instance.hpp>
#include <oal/oal_device.hpp>

namespace modus::audio::oal {

static constexpr const char* kDefaultAudioDeviceName = "DefaultAudioDevice";

Result<> OALInstance::initialize() {
    m_audio_devices.reserve(8);
    m_audio_devices.push_back(AudioDeviceInfo{0, kDefaultAudioDeviceName});

    // Reset error stack
    (void)alGetError();

    // List all audio devices
    const ALboolean has_enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
    if (has_enumeration == AL_TRUE) {
        const ALCchar* devices = alcGetString(nullptr, ALC_DEVICE_SPECIFIER);
        if (devices == nullptr) {
            MODUS_LOGE("OALInstance: No audio devices available");
            return Error<>();
        }

        u32 index = 1;
        const ALCchar* device = devices;
        const ALCchar* next = devices + 1;
        while (device != nullptr && *device != '\0' && next != nullptr && *next != '\0') {
            String device_name = device;
            const usize len = device_name.size();
            m_audio_devices.push_back(AudioDeviceInfo{index++, std::move(device_name)});
            device += (len + 1);
            next += (len + 2);
        }
    }
    return Ok<>();
}

Result<> OALInstance::shutdown() {
    return Ok<>();
}

StringSlice OALInstance::name() const {
    return "OpenAL";
}

Slice<AudioDeviceInfo> OALInstance::list_audio_devices() const {
    return make_slice(m_audio_devices);
}

Result<std::unique_ptr<AudioDevice>, void> OALInstance::create_device(const AudioDeviceInfo& info) {
    ALCdevice* device = nullptr;
    if (info.name == kDefaultAudioDeviceName) {
        device = alcOpenDevice(nullptr);
    } else {
        device = alcOpenDevice(info.name.c_str());
    }
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    if (device == nullptr) {
        MODUS_LOGE("OALInstance: Failed to open device {}", info.name);
        return Error<>();
    }

    return Ok<std::unique_ptr<AudioDevice>>(modus::make_unique<OALAudioDevice>(device));
}

}    // namespace modus::audio::oal
