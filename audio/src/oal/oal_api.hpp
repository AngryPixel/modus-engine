/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/audio.hpp>

#if defined(MODUS_DEBUG)
#define MODUS_AUDIO_VALIDATE_OPENAL_CALLS
#endif

#define MODUS_DECLARE_OAL_HANDLE(Name)                                                     \
    MODUS_STRONG_TYPE_CLASS(Name, ALuint, std::numeric_limits<ALuint>::max())              \
    inline bool is_valid() const { return value() != std::numeric_limits<ALuint>::max(); } \
    inline operator bool() const { return is_valid(); }                                    \
    inline operator ALuint() const { return value(); }                                     \
    }

namespace modus::audio::oal {

const char* oal_error_to_cstr(const ALCenum error);

const char* oalc_error_to_cstr(const ALCenum error);

void log_oal_error(const char* filename, const int line);

void log_oalc_error(ALCdevice* device, const char* filename, const int line);

#if defined(MODUS_AUDIO_VALIDATE_OPENAL_CALLS)
#define MODUS_AUDIO_CHECK_OPENAL_ERROR log_oal_error(__FILE__, __LINE__);
#else
#define MODUS_AUDIO_CHECK_OPENAL_ERROR
#endif

#if defined(MODUS_AUDIO_VALIDATE_OPENAL_CALLS)
#define MODUS_AUDIO_CHECK_OPENALC_ERROR(device) log_oalc_error(device, __FILE__, __LINE__)
#else
#define MODUS_AUDIO_CHECK_OPENALC_ERROR(device)
#endif

MODUS_DECLARE_OAL_HANDLE(OALBufferId);
MODUS_DECLARE_OAL_HANDLE(OALSourceId);

static constexpr const ALenum kAudioFormaToALFormat[] = {AL_FORMAT_MONO8, AL_FORMAT_MONO16,
                                                         AL_FORMAT_STEREO8, AL_FORMAT_STEREO16};
static_assert(modus::array_size(kAudioFormaToALFormat) == u32(AudioFormat::Total));

}    // namespace modus::audio::oal
