/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <oal/oal_api.hpp>
// clang-format on

#include <oal/oal_audio_source.hpp>
#include <audio/audio_device.hpp>
#include <oal/oal_sound.hpp>

namespace modus::audio::oal {

OALAudioSource::OALAudioSource(const OALSourceId source) : m_source(source), m_looping(false) {}

OALAudioSource::~OALAudioSource() {
    if (m_source) {
        alDeleteSources(1, &m_source.value());
        MODUS_AUDIO_CHECK_OPENAL_ERROR
    }
}

OALAudioSource::OALAudioSource(OALAudioSource&& rhs) noexcept : m_source(rhs.m_source) {
    rhs.m_source = OALSourceId();
}

OALAudioSource& OALAudioSource::operator=(OALAudioSource&& rhs) noexcept {
    this->~OALAudioSource();
    MODUS_INPLACE_NEW(this) OALAudioSource(std::forward<OALAudioSource>(rhs));
    return *this;
}

void OALAudioSource::set_gain(const f32 gain) {
    const ALuint source = m_source.value();
    alSourcef(source, AL_GAIN, gain);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_pitch(const f32 pitch) {
    alSourcef(m_source.value(), AL_PITCH, pitch);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_rolloff_factor(const f32 factor) {
    alSourcef(m_source.value(), AL_ROLLOFF_FACTOR, factor);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_position(const glm::vec3& position) {
    alSourcefv(m_source.value(), AL_POSITION, &position.x);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_velocity(const glm::vec3& velocity) {
    alSourcefv(m_source.value(), AL_VELOCITY, &velocity.x);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_direction(const glm::vec3& direction) {
    alSourcefv(m_source.value(), AL_DIRECTION, &direction.x);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_sound(const SoundHandle handle, const bool is_stream) {
    m_sound = handle;
    if (is_stream) {
        apply_looping(false);
    } else {
        apply_looping(m_looping);
    }
}

void OALAudioSource::set_max_distance(const f32 distance) {
    // alSourcef(m_source, AL_REFERENCE_DISTANCE, distance);
    // MODUS_AUDIO_CHECK_OPENAL_ERROR
    alSourcef(m_source, AL_MAX_DISTANCE, distance);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::detach_buffers() {
    alSourcei(m_source, AL_BUFFER, 0);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::set_looping(const bool value) {
    m_looping = value;
    apply_looping(value);
}

bool OALAudioSource::is_playing() const {
    ALint state;
    alGetSourcei(m_source, AL_SOURCE_STATE, &state);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return state == AL_PLAYING;
}

bool OALAudioSource::is_looping() const {
    return m_looping;
}

void OALAudioSource::play() {
    alSourcePlay(m_source);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::stop() {
    alSourceStop(m_source.value());
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::pause() {
    alSourcePause(m_source.value());
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::resume() {
    alSourcePlay(m_source.value());
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

void OALAudioSource::rewind() {
    alSourceRewind(m_source);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

f32 OALAudioSource::offset_seconds() const {
    f32 result = 0.0f;
    alGetSourcef(m_source, AL_SEC_OFFSET, &result);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return result;
}

void OALAudioSource::apply_looping(const bool value) {
    alSourcei(m_source.value(), AL_LOOPING, value ? AL_TRUE : AL_FALSE);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
}

OALAudioSourceDB::~OALAudioSourceDB() {}

Result<AudioSourceHandle, void> OALAudioSourceDB::create(const AudioSourceCreateParams& params) {
    if (m_sources.is_full()) {
        modus_assert_message(false, "Max cacpity reached!");
        MODUS_LOGE(
            "OALAudioSourceDB: Max capcity reached, can't create more "
            "buffers");
        return Error<>();
    }
    ALuint al_source;
    alGenSources(1, &al_source);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    if (alGetError() != AL_NO_ERROR) {
        return Error<>();
    }

    auto r_add = m_sources.create(OALAudioSource(OALSourceId(al_source)));
    if (!r_add) {
        alDeleteSources(1, &al_source);
        MODUS_AUDIO_CHECK_OPENAL_ERROR
        MODUS_LOGE("OALAudioSourceDB: Failed to register audio source");
        return Error<>();
    }

    r_add->second->set_gain(params.gain);
    r_add->second->set_pitch(params.pitch);
    r_add->second->set_looping(params.looping);
    r_add->second->set_rolloff_factor(params.rolloff_factor);
    r_add->second->set_position(params.position);
    r_add->second->set_velocity(params.velocity);
    r_add->second->set_direction(params.direction);
    r_add->second->set_max_distance(params.max_distance);
    return Ok(r_add->first);
}

Result<> OALAudioSourceDB::destroy(const AudioSourceHandle handle, OALSoundDB& sound_db) {
    auto r_source = m_sources.get(handle);
    if (!r_source) {
        return Error<>();
    }

    if (auto r_sound = sound_db.get((*r_source)->sound()); r_sound) {
        (*r_sound)->detach(**r_source);
    }

    m_sources.erase(handle).expect("Failed to delete audio source");
    return Ok<>();
}

void OALAudioSourceDB::update(OALSoundDB& sound_db) {
    MODUS_PROFILE_AUDIO_SCOPE();
    auto update_fn = [&sound_db](OALAudioSource& source) {
        // Not a stream, continue
        if (!source.sound().is_valid()) {
            return;
        }

        auto r_sound = sound_db.get(source.sound());
        if (!r_sound) {
            MODUS_LOGE(
                "OALAudioSourceDB::Update: source does not have valid "
                "sound attached");
            return;
        }

        if (!(*r_sound)->update(source)) {
            MODUS_LOGE("OALAudioSourceDB::Update: Failed to update source stream");
        }
    };

    m_sources.for_each(update_fn);
}

Result<NotMyPtr<OALAudioSource>, void> OALAudioSourceDB::get(const AudioSourceHandle handle) {
    return m_sources.get(handle);
}

Result<NotMyPtr<const OALAudioSource>, void> OALAudioSourceDB::get(
    const AudioSourceHandle handle) const {
    return m_sources.get(handle);
}

}    // namespace modus::audio::oal
