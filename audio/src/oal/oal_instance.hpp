/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/audio_instance.hpp>

namespace modus::audio::oal {

class OALInstance final : public AudioInstance {
   private:
    Vector<AudioDeviceInfo> m_audio_devices;

   public:
    Result<> initialize() override;

    Result<> shutdown() override;

    StringSlice name() const override;

    Slice<AudioDeviceInfo> list_audio_devices() const override;

    Result<std::unique_ptr<AudioDevice>, void> create_device(const AudioDeviceInfo& info) override;
};

}    // namespace modus::audio::oal
