/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <oal/oal_api.hpp>
// clang-format on

#include <oal/oal_sound.hpp>
#include <audio/audio_device.hpp>
#include <oal/oal_audio_source.hpp>

namespace modus::audio::oal {

OALSound::~OALSound() {
    modus_assert_message(m_attachment_count == 0, "Buffers may be still attached to sources\n");
    if (m_buffers.size() != 0) {
        alDeleteBuffers(m_buffers.size(), m_buffers.data());
        MODUS_AUDIO_CHECK_OPENAL_ERROR
    }
}

OALSound::OALSound(OALSound&& rhs) noexcept
    : m_handle(rhs.m_handle),
      m_name(rhs.m_name),
      m_buffers(rhs.m_buffers),
      m_codec(std::move(rhs.m_codec)),
      m_stream(std::move(rhs.m_stream)),
      m_attachment_count(rhs.m_attachment_count),
      m_user_id(rhs.m_user_id),
      m_is_stream(rhs.m_is_stream) {
    rhs.m_handle = SoundHandle();
    rhs.m_buffers.clear();
    rhs.m_is_stream = false;
    rhs.m_user_id = UserId();
    rhs.m_name.clear();
}

OALSound& OALSound::operator=(OALSound&& rhs) noexcept {
    if (&rhs != this) {
        this->~OALSound();
        MODUS_INPLACE_NEW(this) OALSound(std::forward<OALSound>(rhs));
    }
    return *this;
}

Result<> OALSound::initialize(SoundCreateParams& params) {
    if (!params.byte_stream || !params.codec) {
        return Error<>();
    }

    if (!params.codec->initialize(*params.byte_stream)) {
        MODUS_LOGE("OALSound: Failed to initialize codec for:{}", params.name);
        return Error<>();
    }

    m_name = params.name;
    if (params.stream) {
        m_codec = std::move(params.codec);
        m_stream = std::move(params.byte_stream);

        for (u32 i = 0; i < kStreamBufferCount; ++i) {
            ALuint al_buffer;
            alGenBuffers(1, &al_buffer);
            MODUS_AUDIO_CHECK_OPENAL_ERROR
            if (alGetError() != AL_NO_ERROR) {
                return Error<>();
            }
            m_buffers.push_back(al_buffer).expect("Failed to push back buffer");
            auto r_fill = fill_buffer(al_buffer, false);
            if (!r_fill) {
                return Error<>();
            }
            if (*r_fill == 0) {
                break;
            }
        }
        if (m_buffers.size() == 0) {
            MODUS_LOGE("Failed to audio decoded for audio stream: {}", m_name);
            return Error<>();
        }
    } else {
        ALuint al_buffer;
        alGenBuffers(1, &al_buffer);
        MODUS_AUDIO_CHECK_OPENAL_ERROR
        if (alGetError() != AL_NO_ERROR) {
            return Error<>();
        }
        m_buffers.push_back(al_buffer).expect("Failed to insert buffer");
        StringWithAllocator<AudioAllocator> data;
        data.resize(params.codec->data_size());

        if (!params.codec->decode(*params.byte_stream, make_slice_mut(data).as_bytes())) {
            MODUS_LOGE("Failed to audio decoded for audio stream: {}", m_name);
            return Error<>();
        }

        alBufferData(al_buffer, kAudioFormaToALFormat[u32(params.codec->audio_format())],
                     data.data(), data.size(), params.codec->audio_frequency());
        MODUS_AUDIO_CHECK_OPENAL_ERROR
        if (alGetError() != AL_NO_ERROR) {
            MODUS_LOGE("OALSound: Failed to upload audio buffer data for {}", m_name);
            return Error<>();
        }
    }

    m_is_stream = params.stream;
    m_user_id = params.id;
    return Ok<>();
}

Result<> OALSound::attach(OALAudioSource& source) {
    const OALSourceId id = source.id();
    if (m_is_stream) {
        if (m_attachment_count == 1) {
            MODUS_LOGE("OALSound: Can't attach stream '{}' to more than on source", m_name);
            return Error<>();
        }

        alSourceQueueBuffers(id, m_buffers.size(), m_buffers.data());
        MODUS_AUDIO_CHECK_OPENAL_ERROR
    } else {
        modus_assert(m_buffers.size() == 1);
        alSourcei(id, AL_BUFFER, m_buffers[0]);
        MODUS_AUDIO_CHECK_OPENAL_ERROR
    }
    m_attachment_count++;
    source.set_sound(m_handle, m_is_stream);
    return Ok<>();
}

void OALSound::detach(OALAudioSource& source) {
    source.stop();
    source.detach_buffers();
    if (m_attachment_count > 0) {
        m_attachment_count--;
    }
    source.set_sound(SoundHandle(), false);
}

Result<> OALSound::update(OALAudioSource& source) {
    if (m_is_stream) {
        MODUS_PROFILE_AUDIO_BLOCK("Stream Update")
        const OALSourceId id = source.id();
        if (!source.is_playing()) {
            return Ok<>();
        }

        ALint n_buffers_processed;
        alGetSourcei(id, AL_BUFFERS_PROCESSED, &n_buffers_processed);
        MODUS_AUDIO_CHECK_OPENAL_ERROR

        while (n_buffers_processed--) {
            ALuint buffer_id;
            alSourceUnqueueBuffers(id, 1, &buffer_id);
            MODUS_AUDIO_CHECK_OPENAL_ERROR

            if (auto r_fill = fill_buffer(buffer_id, source.is_looping()); !r_fill) {
                MODUS_LOGE("Failed to update audio stream: {}", m_name);
                return Error<>();
            } else if (*r_fill != 0) {
                alSourceQueueBuffers(id, 1, &buffer_id);
                MODUS_AUDIO_CHECK_OPENAL_ERROR
            }
        }
    }
    return Ok<>();
}

Result<> OALSound::rewind(OALAudioSource& source) {
    if (m_is_stream) {
        detach(source);
        if (!m_codec->rewind(*m_stream)) {
            MODUS_LOGE("Failed to rewind audio codec");
            return Error<>();
        }

        // fill buffer
        for (u32 i = 0; i < m_buffers.size(); ++i) {
            auto r_fill = fill_buffer(m_buffers[i], false);
            if (!r_fill) {
                return Error<>();
            }
            if (*r_fill == 0) {
                break;
            }
        }
        return attach(source);
    }
    return Ok<>();
}

Result<usize, void> OALSound::fill_buffer(ALuint al_buffer, const bool repeat) {
    MODUS_PROFILE_AUDIO_SCOPE();
    u8 buffer[kStreamBufferSize];
    ByteSliceMut buffer_slice(buffer);
    auto r_read = m_codec->decode(*m_stream, buffer_slice);
    if (!r_read) {
        MODUS_LOGE("Failed to decode audio stream: {}", m_name);
        return Error<>();
    }

    if (*r_read == 0) {
        if (repeat) {
            // Rewind and read again
            auto r_rewind = m_codec->rewind(*m_stream);
            if (!r_rewind) {
                MODUS_LOGE("Failed to rewind audio codec for stream: {}", m_name);
                return Error<>();
            }

            r_read = m_codec->decode(*m_stream, buffer_slice);
            if (!r_read) {
                MODUS_LOGE("Failed to decode audio stream after rewind: {}", m_name);
                return Error<>();
            }
        } else {
            // no repeat, nothing to read
            return Ok(*r_read);
        }
    }

    alBufferData(al_buffer, kAudioFormaToALFormat[u32(m_codec->audio_format())],
                 buffer_slice.data(), *r_read, m_codec->audio_frequency());
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return Ok(*r_read);
}

Result<SoundHandle, void> OALSoundDB::create(SoundCreateParams& params) {
    if (!params.id.is_valid()) {
        MODUS_LOGE("OALSoundDB: Invalid user id supplied");
        return Error<>();
    }

    if (m_id_to_sound.find(params.id) != m_id_to_sound.end()) {
        MODUS_LOGE("OALSoundDB: UserId already exists");
        return Error<>();
    }

    if (m_sounds.is_full()) {
        modus_assert_message(false, "Max cacpity reached!");
        MODUS_LOGE("OALSoundDB: Max capcity reached, can't create more entries");
        return Error<>();
    }

    OALSound sound;
    if (!sound.initialize(params)) {
        return Error<>();
    }

    auto r_add = m_sounds.create(std::move(sound));
    if (!r_add) {
        MODUS_LOGE("OALSoundDB: Failed to register sound");
        return Error<>();
    }

    m_id_to_sound[params.id] = r_add->first;
    r_add->second->m_handle = r_add->first;
    return Ok(r_add->first);
}

Result<> OALSoundDB::destroy(const SoundHandle handle) {
    return m_sounds.erase(handle);
}

Result<NotMyPtr<OALSound>, void> OALSoundDB::get(const SoundHandle handle) {
    return m_sounds.get(handle);
}

Result<NotMyPtr<OALSound>, void> OALSoundDB::get(const UserId id) {
    auto it = m_id_to_sound.find(id);
    if (it == m_id_to_sound.end()) {
        return Error<>();
    }

    return m_sounds.get(it->second);
}

Result<SoundHandle, void> OALSoundDB::handle_from_user_id(const UserId id) const {
    auto it = m_id_to_sound.find(id);
    if (it == m_id_to_sound.end()) {
        return Error<>();
    }
    return Ok(it->second);
}

}    // namespace modus::audio::oal
