/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/audio.hpp>
#include <core/fixed_handle_pool.hpp>

namespace modus::audio {
struct AudioSourceCreateParams;
}
namespace modus::audio::oal {

class OALSoundDB;
class OALAudioSource {
   private:
    OALSourceId m_source;
    SoundHandle m_sound;
    bool m_looping;

   public:
    OALAudioSource(const OALSourceId source);

    ~OALAudioSource();

    MODUS_CLASS_DISABLE_COPY(OALAudioSource);

    OALAudioSource(OALAudioSource&& rhs) noexcept;

    OALAudioSource& operator=(OALAudioSource&& rhs) noexcept;

    const OALSourceId id() const { return m_source; }

    void set_gain(const f32 gain);

    void set_pitch(const f32 pitch);

    void set_rolloff_factor(const f32 factor);

    void set_position(const glm::vec3& position);

    void set_velocity(const glm::vec3& velocity);

    void set_direction(const glm::vec3& direction);

    void set_sound(const SoundHandle handle, const bool is_stream);

    SoundHandle sound() const { return m_sound; }

    void set_max_distance(const f32 distance);

    void detach_buffers();

    void set_looping(const bool value);

    bool is_looping() const;

    bool is_playing() const;

    void play();

    void stop();

    void pause();

    void resume();

    void rewind();

    f32 offset_seconds() const;

   private:
    void apply_looping(const bool value);
};

class OALAudioSourceDB {
   private:
    static constexpr u32 kMaxAudioSources = 64;
    using PoolType = FixedHandlePool<OALAudioSource, kMaxAudioSources, AudioSourceHandle>;
    PoolType m_sources;

   public:
    OALAudioSourceDB() = default;

    ~OALAudioSourceDB();

    Result<AudioSourceHandle, void> create(const AudioSourceCreateParams& params);

    void update(OALSoundDB& sound_db);

    Result<> destroy(const AudioSourceHandle handle, OALSoundDB& sound_db);

    Result<NotMyPtr<OALAudioSource>, void> get(const AudioSourceHandle handle);

    Result<NotMyPtr<const OALAudioSource>, void> get(const AudioSourceHandle handle) const;
};

}    // namespace modus::audio::oal
