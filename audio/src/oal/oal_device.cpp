
/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
#include <oal/oal_api.hpp>
// clang-format on

#include <oal/oal_device.hpp>

namespace modus::audio::oal {

OALAudioDevice::OALAudioDevice(ALCdevice* device) : m_device(device) {
    modus_assert(device != nullptr);
}

OALAudioDevice::~OALAudioDevice() {
    if (!alcCloseDevice(m_device)) {
        MODUS_LOGE("OALAudioDevice: Failed to close audio device");
    }
}

Result<std::unique_ptr<AudioContext>, void> OALAudioDevice::create_context() {
    ALCcontext* context = alcCreateContext(m_device, nullptr);
    MODUS_AUDIO_CHECK_OPENALC_ERROR(m_device);
    if (context == nullptr) {
        return Error<>();
    }

    return Ok<std::unique_ptr<AudioContext>>(modus::make_unique<OALAudioContext>(*this, context));
}

OALAudioContext::OALAudioContext(OALAudioDevice& device, ALCcontext* context)
    : m_device(device), m_context(context) {
    MODUS_UNUSED(m_device);
}

OALAudioContext::~OALAudioContext() {
    (void)unbind_from_current_thread();
    alcDestroyContext(m_context);
}

Result<> OALAudioContext::bind_to_current_thread() {
    const auto result = alcMakeContextCurrent(m_context);
    MODUS_AUDIO_CHECK_OPENALC_ERROR(m_device.m_device);
    if (result == AL_FALSE) {
        return Error<>();
    }
    alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return Ok<>();
}

Result<> OALAudioContext::unbind_from_current_thread() {
    if (alcGetCurrentContext() != nullptr) {
        const auto result = alcMakeContextCurrent(nullptr);
        MODUS_AUDIO_CHECK_OPENALC_ERROR(m_device.m_device);
        if (result == AL_FALSE) {
            return Error<>();
        }
    }
    return Ok<>();
}

void OALAudioContext::update() {
    m_audio_source_db.update(m_sound_db);
}

Result<AudioSourceHandle, void> OALAudioContext::create_source(
    const AudioSourceCreateParams& params) {
    return m_audio_source_db.create(params);
}

Result<> OALAudioContext::destroy_source(const AudioSourceHandle handle) {
    return m_audio_source_db.destroy(handle, m_sound_db);
}

Result<SoundHandle, void> OALAudioContext::create_sound(SoundCreateParams& params) {
    return m_sound_db.create(params);
}

Result<> OALAudioContext::destroy_sound(const SoundHandle handle) {
    return m_sound_db.destroy(handle);
}

Result<SoundHandle, void> OALAudioContext::sound_from_user_id(const UserId id) {
    return m_sound_db.handle_from_user_id(id);
}

Result<> OALAudioContext::set_listener_gain(const f32 gain) {
    alListenerf(AL_GAIN, gain);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return Ok<>();
}

Result<> OALAudioContext::set_listener_position(const glm::vec3& position) {
    alListenerfv(AL_POSITION, &position.x);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return Ok<>();
}

Result<> OALAudioContext::set_listener_velocity(const glm::vec3& velocity) {
    alListenerfv(AL_VELOCITY, &velocity.x);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return Ok<>();
}

Result<> OALAudioContext::set_listener_orientation(const glm::vec3& orientation,
                                                   const glm::vec3& up) {
    const f32 data[] = {orientation.x, orientation.y, orientation.z, up.x, up.y, up.z};
    alListenerfv(AL_ORIENTATION, data);
    MODUS_AUDIO_CHECK_OPENAL_ERROR
    return Ok<>();
}

Result<> OALAudioContext::play(const AudioSourceHandle source) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }

    if (!(*r_source)->sound().is_valid()) {
        MODUS_LOGE("Attempting to play audio source without any data");
        return Error<>();
    }

    (*r_source)->play();
    return Ok<>();
}

Result<> OALAudioContext::set_source_position(const AudioSourceHandle handle,
                                              const glm::vec3& position) {
    auto r_source = m_audio_source_db.get(handle);
    if (!r_source) {
        return Error<>();
    }
    (*r_source)->set_position(position);
    return Ok<>();
}

Result<> OALAudioContext::set_source_direction(const AudioSourceHandle handle,
                                               const glm::vec3& direction) {
    auto r_source = m_audio_source_db.get(handle);
    if (!r_source) {
        return Error<>();
    }
    (*r_source)->set_direction(direction);
    return Ok<>();
}

Result<> OALAudioContext::set_source_velocity(const AudioSourceHandle handle,
                                              const glm::vec3& velocity) {
    auto r_source = m_audio_source_db.get(handle);
    if (!r_source) {
        return Error<>();
    }
    (*r_source)->set_velocity(velocity);
    return Ok<>();
}

Result<> OALAudioContext::set_source_pitch(const AudioSourceHandle handle, const f32 pitch) {
    auto r_source = m_audio_source_db.get(handle);
    if (!r_source) {
        return Error<>();
    }
    (*r_source)->set_pitch(pitch);
    return Ok<>();
}

Result<> OALAudioContext::set_source_sound(const AudioSourceHandle source,
                                           const SoundHandle sound) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }
    auto r_sound = m_sound_db.get(sound);
    if (!r_sound) {
        return Error<>();
    }

    OALAudioSource& oal_source = **r_source;
    OALSound& oal_sound = **r_sound;
    const SoundHandle existing_sound = oal_source.sound();
    if (existing_sound.is_valid()) {
        if (auto r_existing = m_sound_db.get(existing_sound); r_existing) {
            (*r_existing)->detach(oal_source);
        }
        if (!oal_sound.attach(oal_source)) {
            return Error<>();
        }
    } else {
        if (!oal_sound.attach(oal_source)) {
            return Error<>();
        }
    }
    return Ok<>();
}

Result<> OALAudioContext::play(const AudioSourceHandle source, const SoundHandle sound) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }
    auto r_sound = m_sound_db.get(sound);
    if (!r_sound) {
        return Error<>();
    }

    OALAudioSource& oal_source = **r_source;
    OALSound& oal_sound = **r_sound;
    const SoundHandle existing_sound = oal_source.sound();
    if (existing_sound.is_valid()) {
        if (auto r_existing = m_sound_db.get(existing_sound); r_existing) {
            (*r_existing)->detach(oal_source);
        }
        if (!oal_sound.attach(oal_source)) {
            return Error<>();
        }
    } else {
        if (!oal_sound.attach(oal_source)) {
            return Error<>();
        }
    }

    oal_source.play();
    return Ok<>();
}

Result<> OALAudioContext::play(const AudioSourceHandle source, const UserId id) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }
    auto r_sound = m_sound_db.get(id);
    if (!r_sound) {
        return Error<>();
    }

    OALAudioSource& oal_source = **r_source;
    OALSound& oal_sound = **r_sound;
    const SoundHandle existing_sound = oal_source.sound();
    if (existing_sound.is_valid()) {
        if (auto r_existing = m_sound_db.get(existing_sound);
            r_existing && (*r_existing)->user_id() != id) {
            (*r_existing)->detach(oal_source);
        }
        if (!oal_sound.attach(oal_source)) {
            return Error<>();
        }
    } else {
        if (!oal_sound.attach(oal_source)) {
            return Error<>();
        }
    }

    oal_source.play();
    return Ok<>();
}

Result<> OALAudioContext::pause(const AudioSourceHandle source) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }
    (*r_source)->pause();
    return Ok<>();
}

Result<> OALAudioContext::stop(const AudioSourceHandle source) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }

    (*r_source)->stop();
    return Ok<>();
}

Result<> OALAudioContext::rewind(const AudioSourceHandle source) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }

    auto r_sound = m_sound_db.get((*r_source)->sound());
    if (!r_sound) {
        return Error<>();
    }

    if (!(*r_sound)->rewind(**r_source)) {
        return Error<>();
    }
    (*r_source)->rewind();
    return Ok<>();
}

Result<> OALAudioContext::rewind_and_play(const AudioSourceHandle source) {
    auto r_source = m_audio_source_db.get(source);
    if (!r_source) {
        return Error<>();
    }

    auto r_sound = m_sound_db.get((*r_source)->sound());
    if (!r_sound) {
        return Error<>();
    }

    if (!(*r_sound)->rewind(**r_source)) {
        return Error<>();
    }
    (*r_source)->rewind();
    (*r_source)->play();
    return Ok<>();
}

Result<bool, void> OALAudioContext::is_playing(const AudioSourceHandle handle) const {
    auto r_source = m_audio_source_db.get(handle);
    if (!handle) {
        return Error<>();
    }
    return Ok((*r_source)->is_playing());
}

Result<f32, void> OALAudioContext::source_offset_seconds(const AudioSourceHandle handle) const {
    auto r_source = m_audio_source_db.get(handle);
    if (!handle) {
        return Error<>();
    }
    return Ok((*r_source)->offset_seconds());
}

}    // namespace modus::audio::oal
