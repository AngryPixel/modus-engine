/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/audio_device.hpp>
#include <oal/oal_audio_source.hpp>
#include <oal/oal_sound.hpp>

namespace modus::audio::oal {

class OALAudioDevice final : public AudioDevice {
    friend class OALAudioContext;

   private:
    ALCdevice* m_device;

   public:
    OALAudioDevice(ALCdevice* device);

    ~OALAudioDevice();

    Result<std::unique_ptr<AudioContext>, void> create_context() override;
};

class OALAudioContext final : public AudioContext {
   private:
    OALAudioDevice& m_device;
    ALCcontext* m_context;
    OALAudioSourceDB m_audio_source_db;
    OALSoundDB m_sound_db;

   public:
    OALAudioContext(OALAudioDevice& device, ALCcontext* context);

    ~OALAudioContext();

    Result<> bind_to_current_thread() override;

    Result<> unbind_from_current_thread() override;

    void update() override;

    Result<AudioSourceHandle, void> create_source(const AudioSourceCreateParams& params) override;

    Result<> destroy_source(const AudioSourceHandle handle) override;

    Result<SoundHandle, void> create_sound(SoundCreateParams& params) override;

    Result<> destroy_sound(const SoundHandle handle) override;

    Result<SoundHandle, void> sound_from_user_id(const UserId id) override;

    Result<> set_listener_gain(const f32 gain) override;

    Result<> set_listener_position(const glm::vec3& position) override;

    Result<> set_listener_velocity(const glm::vec3& velocity) override;

    Result<> set_listener_orientation(const glm::vec3& orientation, const glm::vec3& up) override;

    Result<> set_source_position(const AudioSourceHandle handle,
                                 const glm::vec3& position) override;

    Result<> set_source_direction(const AudioSourceHandle handle,
                                  const glm::vec3& direction) override;

    Result<> set_source_velocity(const AudioSourceHandle handle,
                                 const glm::vec3& velocity) override;

    Result<> set_source_pitch(const AudioSourceHandle handle, const f32 pitch) override;

    Result<> set_source_sound(const AudioSourceHandle source, const SoundHandle sound) override;

    Result<> play(const AudioSourceHandle source, const SoundHandle h) override;

    Result<> play(const AudioSourceHandle source, const UserId id) override;

    Result<> play(const AudioSourceHandle source) override;

    Result<> pause(const AudioSourceHandle source) override;

    Result<> stop(const AudioSourceHandle source) override;

    Result<> rewind(const AudioSourceHandle source) override;

    Result<> rewind_and_play(const AudioSourceHandle source) override;

    Result<bool, void> is_playing(const AudioSourceHandle handle) const override;

    Result<f32, void> source_offset_seconds(const AudioSourceHandle h) const override;
};

}    // namespace modus::audio::oal
