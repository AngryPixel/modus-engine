/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

// clang-format off
#include <pch.h>
// clang-format on

#include <oal/oal_api.hpp>

namespace modus::audio::oal {

const char* oal_error_to_cstr(const ALCenum error) {
    switch (error) {
        case AL_NO_ERROR:
            return "No error";
        case AL_INVALID_NAME:
            return "Invalid name";
        case AL_INVALID_ENUM:
            return "Invalid enum";
        case AL_INVALID_VALUE:
            return "Invalid value";
        case AL_INVALID_OPERATION:
            return "Invalid operation";
        case AL_OUT_OF_MEMORY:
            return "Out of memory";
        default:
            return "Unknown error";
    }
}

const char* oalc_error_to_cstr(const ALCenum error) {
    switch (error) {
        case ALC_NO_ERROR:
            return "No error";
        case ALC_INVALID_DEVICE:
            return "Invalid device";
        case ALC_INVALID_CONTEXT:
            return "Invalid context";
        case ALC_INVALID_ENUM:
            return "Invalid enum";
        case ALC_INVALID_VALUE:
            return "Invalid value";
        case ALC_OUT_OF_MEMORY:
        default:
            return "Unknown error";
    }
}

void log_oal_error(const char* filename, const int line) {
    const auto al_error = alGetError();
    if (alcGetCurrentContext() != nullptr && al_error != AL_NO_ERROR) {
        MODUS_LOGE("OpenAL Error: {} at {}:{}", oal_error_to_cstr(al_error), filename, line);
    }
}

void log_oalc_error(ALCdevice* device, const char* filename, const int line) {
    const auto al_error = alcGetError(device);
    if (al_error != ALC_NO_ERROR) {
        MODUS_LOGE("OpenAL Context Error: {} at {}:{}", oalc_error_to_cstr(al_error), filename,
                   line);
    }
}

}    // namespace modus::audio::oal
