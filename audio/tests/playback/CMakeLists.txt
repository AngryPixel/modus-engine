#
# Context test
#

modus_add_test(test_audio_playback NAME AudioPlayback)

target_sources(test_audio_playback PRIVATE
    test_audio_playback.cpp
)

target_link_libraries(test_audio_playback PRIVATE audio)
