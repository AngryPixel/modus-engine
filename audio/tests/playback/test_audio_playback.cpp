/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <audio/audio_pch.h>

#include <os/os_pch.h>
#include <audio/audio_device.hpp>
#include <audio/audio_instance.hpp>

#include <audio/codecs/oggvorbis_codec.hpp>
#include <audio/codecs/wave_codec.hpp>
#include <catch2/catch.hpp>
#include <core/io/memory_stream.hpp>
#include <core/string_slice.hpp>
#include <log/log.hpp>
#include <os/file.hpp>
#include <os/time.hpp>

using namespace modus;

static void test_play(std::unique_ptr<audio::ICodec>&& codec,
                      const StringSlice path,
                      const bool stream) {
    // Load file
    const String test_file = os::Path::join(MODUS_TEST_SOURCE_DIR, path);
    auto r_file = os::FileBuilder().read().open(test_file);
    REQUIRE(r_file);

    audio::SoundCreateParams params{modus::audio::UserId(0), "Stream", std::move(codec),
                                    modus::make_unique<os::File>(r_file.release_value()), stream};

    // REQUIRE(params.codec->initialize(*params.stream));

    auto instance = audio::create_default_audio_instance();
    REQUIRE(instance);

    REQUIRE(instance->initialize());
    {
        auto devices = instance->list_audio_devices();
        REQUIRE(!devices.is_empty());

        auto r_device = instance->create_device(devices[0]);    // default device
        REQUIRE(r_device);

        auto device = r_device.release_value();

        auto r_context = device->create_context();
        REQUIRE(r_context);

        auto context = r_context.release_value();

        REQUIRE(context->bind_to_current_thread());

        REQUIRE(context->set_listener_gain(1.0f));
        REQUIRE(context->set_listener_position(glm::vec3(0.f, 0.f, 1.0f)));
        REQUIRE(context->set_listener_velocity(glm::vec3(0.f)));
        REQUIRE(context->set_listener_orientation(glm::vec3(0.f, 0.f, 1.0f),
                                                  glm::vec3(0.f, 1.0f, 0.f)));

        auto r_stream = context->create_sound(params);
        REQUIRE(r_stream);

        audio::AudioSourceCreateParams source_params;
        source_params.looping = stream;
        auto r_source = context->create_source(source_params);
        REQUIRE(r_source);

        auto r_play = context->play(*r_source, *r_stream);
        REQUIRE(r_play);

        os::Timer timer;
        while (timer.elapsed_seconds() < FSeconds(2.0)) {
            auto r_status = context->is_playing(*r_source);
            REQUIRE(r_status);
            if (!(*r_status)) {
                break;
            }
            context->update();
            os::sleep_ms(IMilisec(25));
        }
        REQUIRE(context->destroy_source(*r_source));
        REQUIRE(context->destroy_sound(*r_stream));
        REQUIRE(context->unbind_from_current_thread());
    }
    REQUIRE(instance->shutdown());
}

TEST_CASE("Play Ogg/Vorbis stream loop", "[AudioPlayback]") {
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::set(log::make_basic_stdout_logger());
#endif
    test_play(modus::make_unique<audio::OggVorbisCodec>(), "test.ogg", true);
}

TEST_CASE("Play Wave stream loop", "[AudioPlayback]") {
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::set(log::make_basic_stdout_logger());
#endif
    test_play(modus::make_unique<audio::WaveCodec>(), "test.wav", true);
}

TEST_CASE("Play OGG Vorbis buffer", "[AudioPlayback]") {
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::set(log::make_basic_stdout_logger());
#endif
    test_play(modus::make_unique<audio::OggVorbisCodec>(), "test.ogg", false);
}

TEST_CASE("PlayWave buffer", "[AudioPlayback]") {
#if !defined(MODUS_LOG_USE_SPDLOG)
    log::LogService::set(log::make_basic_stdout_logger());
#endif
    test_play(modus::make_unique<audio::WaveCodec>(), "test.wav", false);
}
