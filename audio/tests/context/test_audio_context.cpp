/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#include <audio/audio_pch.h>

#include <audio/audio_device.hpp>
#include <audio/audio_instance.hpp>
#include <catch2/catch.hpp>
#include <core/io/memory_stream.hpp>
#include <core/string_slice.hpp>

using namespace modus;

TEST_CASE("Create / Destroy context", "[AudioContext]") {
    auto instance = audio::create_default_audio_instance();
    REQUIRE(instance);

    REQUIRE(instance->initialize());

    {
        auto devices = instance->list_audio_devices();
        REQUIRE(!devices.is_empty());

        auto r_device = instance->create_device(devices[0]);    // default device
        REQUIRE(r_device);

        auto device = r_device.release_value();

        auto r_context = device->create_context();
        REQUIRE(r_context);

        auto context = r_context.release_value();

        REQUIRE(context->bind_to_current_thread());

        REQUIRE(context->unbind_from_current_thread());
    }
    REQUIRE(instance->shutdown());
}
