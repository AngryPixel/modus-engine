/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/audio.hpp>
#include <audio/codecs/codec.hpp>
#include <core/string_slice.hpp>

namespace modus::audio {

class AudioContext;

struct AudioSourceCreateParams {
    f32 pitch = 1.0f;
    f32 gain = 1.0f;
    f32 rolloff_factor = 1.0f;
    f32 max_distance = std::numeric_limits<f32>::max();
    glm::vec3 position = glm::vec3(0.f);
    glm::vec3 velocity = glm::vec3(0.f);
    glm::vec3 direction = glm::vec3(0.0f, 0.0f, 1.0f);
    bool looping = false;
};

struct SoundCreateParams {
    UserId id;
    StringSlice name;
    std::unique_ptr<ICodec> codec;
    std::unique_ptr<ISeekableReader> byte_stream;
    bool stream = false;
};

class MODUS_AUDIO_EXPORT AudioDevice {
   protected:
    AudioDevice() = default;

   public:
    virtual ~AudioDevice() = default;

    virtual Result<std::unique_ptr<AudioContext>, void> create_context() = 0;
};

class MODUS_AUDIO_EXPORT AudioContext {
   protected:
    AudioContext() = default;

   public:
    virtual ~AudioContext() = default;

    virtual Result<> bind_to_current_thread() = 0;

    virtual Result<> unbind_from_current_thread() = 0;

    virtual void update() = 0;

    virtual Result<AudioSourceHandle, void> create_source(
        const AudioSourceCreateParams& params) = 0;

    virtual Result<> destroy_source(const AudioSourceHandle handle) = 0;

    virtual Result<SoundHandle, void> create_sound(SoundCreateParams& params) = 0;

    virtual Result<> destroy_sound(const SoundHandle handle) = 0;

    virtual Result<SoundHandle, void> sound_from_user_id(const UserId id) = 0;

    virtual Result<> set_listener_gain(const f32 gain) = 0;

    virtual Result<> set_listener_position(const glm::vec3& position) = 0;

    virtual Result<> set_listener_velocity(const glm::vec3& velocity) = 0;

    virtual Result<> set_listener_orientation(const glm::vec3& orientation,
                                              const glm::vec3& up) = 0;

    virtual Result<> set_source_position(const AudioSourceHandle handle,
                                         const glm::vec3& position) = 0;

    virtual Result<> set_source_direction(const AudioSourceHandle handle,
                                          const glm::vec3& direction) = 0;

    virtual Result<> set_source_velocity(const AudioSourceHandle handle,
                                         const glm::vec3& velocity) = 0;

    virtual Result<> set_source_pitch(const AudioSourceHandle handle, const f32 pitch) = 0;

    virtual Result<> set_source_sound(const AudioSourceHandle source, const SoundHandle sound) = 0;

    virtual Result<> play(const AudioSourceHandle source, const SoundHandle sound) = 0;

    virtual Result<> play(const AudioSourceHandle source, const UserId id) = 0;

    virtual Result<> play(const AudioSourceHandle source) = 0;

    virtual Result<> pause(const AudioSourceHandle source) = 0;

    virtual Result<> stop(const AudioSourceHandle source) = 0;

    virtual Result<> rewind(const AudioSourceHandle source) = 0;

    virtual Result<> rewind_and_play(const AudioSourceHandle source) = 0;

    virtual Result<f32, void> source_offset_seconds(const AudioSourceHandle h) const = 0;

    virtual Result<bool, void> is_playing(const AudioSourceHandle handle) const = 0;
};

}    // namespace modus::audio
