/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/core.hpp>
#include <core/handle_pool_shared.hpp>

namespace modus::audio {

class AudioDevice;

#define MODUS_DECLARE_AUDIO_HANDLE(Name) \
    MODUS_DECLARE_HANDLE_TYPE(Name, std::numeric_limits<u32>::max())

MODUS_DECLARE_AUDIO_HANDLE(AudioSourceHandle);
MODUS_DECLARE_AUDIO_HANDLE(UserId);
MODUS_DECLARE_AUDIO_HANDLE(SoundHandle);

enum class AudioFormat : u8 { Mono8, Mono16, Stereo8, Stereo16, Total };

}    // namespace modus::audio

namespace modus {
template <>
struct hash<modus::audio::UserId> {
    std::size_t operator()(const modus::audio::UserId& id) const {
        return modus::hash<modus::audio::UserId::value_type>{}(id.value());
    }
};
}    // namespace modus
