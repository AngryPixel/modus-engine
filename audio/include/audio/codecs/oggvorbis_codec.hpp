/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <audio/codecs/codec.hpp>
struct OggVorbis_File;
namespace modus::audio {

class MODUS_AUDIO_EXPORT OggVorbisCodec final : public ICodec {
   private:
    std::unique_ptr<OggVorbis_File> m_file;
    u32 m_data_size;
    i32 m_frequency;
    u32 m_bytes_read;
    AudioFormat m_format;
    i32 m_vorbis_section;

   public:
    OggVorbisCodec();

    ~OggVorbisCodec();

    Result<> initialize(ISeekableReader& reader) override;

    IOResult<usize> decode(ISeekableReader& reader, ByteSliceMut slice) override;

    IOResult<void> rewind(ISeekableReader& reader) override;

    u32 data_size() const override;

    i32 audio_frequency() const override;

    AudioFormat audio_format() const override;
};

}    // namespace modus::audio
