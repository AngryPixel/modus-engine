/*
 * Copyright 2020-2021 by Leander Beernaert
 *
 * This file is part of Modus.
 *
 * Modus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Modus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Modus. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <audio/audio.hpp>
#include <core/io/byte_stream.hpp>

namespace modus::audio {

class MODUS_AUDIO_EXPORT ICodec {
   public:
    ICodec() = default;
    virtual ~ICodec() = default;

    virtual Result<> initialize(ISeekableReader& reader) = 0;

    virtual IOResult<usize> decode(ISeekableReader& reader, ByteSliceMut slice) = 0;

    virtual IOResult<void> rewind(ISeekableReader& reader) = 0;

    virtual u32 data_size() const = 0;

    virtual i32 audio_frequency() const = 0;

    virtual AudioFormat audio_format() const = 0;
};

}    // namespace modus::audio
